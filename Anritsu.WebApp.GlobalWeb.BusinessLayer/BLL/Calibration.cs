﻿using System.Data;
using DA = Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public static class Calibration
    {
        public static DataTable GetCertificatePricesForPart(string currentlang, string modelNumber)
        {
            return DA.Calibration.GetCertificatePricesForPart(currentlang,modelNumber);
        }

        public static DataTable GetProductNotesForPart(string modelNumber)
        {
            return DA.Calibration.GetProductNotesForPart(modelNumber);
        }

        public static void InsertCalCertRequestsLog(int userId, string modelNumber, string serialNumber, string clientIp)
        {
            DA.Calibration.InsertCalCertRequestsLog(userId, modelNumber, serialNumber, clientIp);
        }

        public static DataRow GetCalibrationCerttificateFile(string iUserId, string modelNumber, string serialNumber, string userIpAddress)
        {
            return DA.Calibration.GetCalibrationCerttificateFile(iUserId, modelNumber, serialNumber, userIpAddress);
        }

        public static DataTable GetAllCertificates(string modelNumber, string serialNumber)
        {
            return DA.Calibration.GetAllCertificates(modelNumber, serialNumber);
        }

        public static DataTable GetAllProductDetails(string  currentlang, string modelNumber)
        {
            return DA.Calibration.GetAllProductDetails(currentlang, modelNumber);
        }

    }
}
