﻿using DA = Anritsu.WebApp.GlobalWeb.DataAccess.Providers;
namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public static class Common
    {
        public static int GetCultureGroupIdForCultureCode(string cultureCode)
        {
            return DA.Common.GetCultureGroupIdForCultureCode(cultureCode);
        }
    }
}
