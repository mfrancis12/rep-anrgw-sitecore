﻿using System.Data;
using DA = Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public static class Country
    {
        public static DataRow GetCountryByIp(string ipAddress)
        {
            return DA.Country.GetCountryByIp(ipAddress);
        }
    }
}
