﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public class BllDownloads
    {
        public static void Download_AddLog(DownloadLog parm)
        {
            if (parm == null) return;
            DaDownload.Download_AddLog(parm);   
        }
    }
}
