﻿using DA = Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public static class DynamicForm
    {
        public static bool SaveSitecoreDynamiceFormDetails(string formName, string siteCoreFormId, int cultureGroupId, string userInformation, string additionalInfo)
        {
            return DA.DynamicForm.SaveSitecoreDynamiceFormDetails(formName, siteCoreFormId, cultureGroupId,
                userInformation, additionalInfo);
        }
    }
}
