﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using DAL = Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public static class Search
    {
        public const string GSEARCH_CACHE = "GSEARCHCACHE";
        public const string GSEARCH_DL_SUBCATEGORY_CACHE = "GSEARCH_CACHE_DLSUBCAT";
        public const string GSEARCH_CACHEKEY_LIST = "GSEARCHCACHE_KEYLIST";

        public static string GetCollectionName(string key, int cultureGroupId)
        {
            return GetSettings(cultureGroupId).ContainsKey(key) ? GetSettings(cultureGroupId)[key].Value : string.Empty;
        }

        public static Dictionary<string, GSetting> GetSettings(int cultureGroupId)
        {
            return GetSettings(false, cultureGroupId);
        }

        public static Dictionary<string, GSetting> GetSettings(bool refresh, int cultureGroupId)
        {
            var cacheKey = GSEARCH_CACHE + cultureGroupId;
            var context = HttpContext.Current;

            if (refresh)
                context.Cache.Remove(cacheKey);

            var lst =
                context.Cache.Get(cacheKey) as Dictionary<string, GSetting>;

            if (lst != null) return lst;
            DataTable tb = DAL.Search.GetRegionalSettingsList(cultureGroupId);
            lst = new Dictionary<string, GSetting>();
            if (tb == null)
                return lst;

            foreach (DataRow r in tb.Rows)
                lst.Add(r["Name"].ToString().ToLower(), new GSetting(r));

            context.Cache.Insert(cacheKey
                , lst
                , null
                , System.Web.Caching.Cache.NoAbsoluteExpiration
                , TimeSpan.FromMinutes(360));

            var cacheKeyList = context.Cache.Get(GSEARCH_CACHEKEY_LIST) as List<string> ?? new List<string>();

            if (!cacheKeyList.Contains(cacheKey))
                cacheKeyList.Add(cacheKey);

            context.Cache[GSEARCH_CACHEKEY_LIST] = cacheKeyList;

            return lst;
        }

        public static string GetDefaultCollection(int cultureGroupId)
        {
            return GetCollectionName(ConfigurationManager.AppSettings["GsaRootCaollection"], cultureGroupId);
        }
    }
}
