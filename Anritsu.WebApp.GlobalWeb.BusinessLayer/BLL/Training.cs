﻿using System;
using System.Data;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using DA = Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public static class Training
    {
        public static DataTable GetTrainingCategories(int cultureGroupId)
        {
            return DA.Training.GetTrainingCategories(cultureGroupId);
        }

        public static DataTable GetClassesForCategory(int cultureGroupId,int categoryId)
        {
            return DA.Training.GetClassesForCategory(cultureGroupId, categoryId);
        }

        public static DataTable GetClassesForCategoryByPageURL(int cultureGroupId, string pageUrl)
        {
            return DA.Training.GetClassesForCategoryByPageURL(cultureGroupId, pageUrl);
        }

        public static DataTable GetClassList(int cultureGroupId)
        {
            return DA.Training.GetClassList(cultureGroupId);
        }

        public static DataTable SearchRegistrations(int? cultureGroupId, int? statusId, int? classId, string firstName, string lastName, int? transactionId, int? registrationId, string partNumber, int? eventId, string location, string emailId)
        {
            return DA.Training.SearchRegistrations(cultureGroupId, statusId, classId, firstName, lastName, transactionId, registrationId, partNumber, eventId, location, emailId);
        }

        public static void UpdateRegistrationStatus(int registrationId, OrderStatusType status)
        {
            DA.Training.UpdateRegistrationStatus(registrationId, status);
        }

        public static void UpdateRegistrationAdditionalAmount(int registrationId, decimal additionalAmount)
        {
            DA.Training.UpdateRegistrationAdditionalAmount(registrationId, additionalAmount);
        }

        public static void CancelRegistration(int registrationId)
        {
            DA.Training.CancelRegistration(registrationId);
        }

        public static void CompleteRegistrationRefund(int registrationId)
        {
            DA.Training.CompleteRegistrationRefund(registrationId);
        }

        public static DataTable GetRegistrationStatusList()
        {
            return DA.Training.GetRegistrationStatusList();
        }

        public static DataTable GetRegisteredAttendees(int registrationId)
        {
            return DA.Training.GetRegisteredAttendees(registrationId);
        }

        public static DataTable GetRegisteredAttendee(int attendeeId)
        {
            return DA.Training.GetRegisteredAttendee(attendeeId);
        }

        public static void UpdateRegisteredAttendee(
            int attendeeId,
            int eventId,
            string firstName,
            string lastName,
            string phoneNumber,
            string phoneExtension,
            string email,
            string qadSalesOrder
            )
        {
            DA.Training.UpdateRegisteredAttendee(
                attendeeId,
                eventId,
                firstName,
                lastName,
                phoneNumber,
                phoneExtension,
                email,
                qadSalesOrder);
        }

        public static void DeleteRegisteredAttendee(int attendeeId)
        {
            DA.Training.DeleteRegisteredAttendee(attendeeId);
        }

        public static void InsertRegisteredAttendee(
            int registrationId,
            int eventId,
            string firstName,
            string lastName,
            string phoneNumber,
            string phoneExtension,
            string email,
            string qadSalesOrder
            )
        {
            DA.Training.InsertRegisteredAttendee(
                registrationId,
                eventId,
                firstName,
                lastName,
                phoneNumber,
                phoneExtension,
                email,
                qadSalesOrder);
        }

        public static DataTable GetClassEventsByClassID(int? cultureGroupId, int? classId)
        {
            return DA.Training.GetClassEventsByClassID(cultureGroupId, classId);
        }

        public static DataTable GetClassEventDetailsBetweenGivenDates(int cultureGroupId,string dateFrom)
        {
            return DA.Training.GetClassEventDetailsBetweenGivenDates(cultureGroupId, dateFrom);
        }

        public static DataTable GetClassScheduleDetails(int cultureGroupId, string classId)
        {
            return DA.Training.GetClassScheduleDetails(cultureGroupId, classId);
        }

        public static bool IsClassEventActive(int cultureGroupId, string classId)
        {
            return DA.Training.IsClassEventActive(cultureGroupId, classId);
        }

        public static DataTable GetClassEventDetailsById(int cultureGroupId, int eventId)
        {
            return DA.Training.GetClassEventDetailsById(cultureGroupId, eventId);
        }

        public static DataRow GetClassEventAvailableSeats(int cultureGroupId, int eventId)
        {
            return DA.Training.GetClassEventAvailableSeats(cultureGroupId, eventId);
        }

        public static DataTable GetClassEventDetailsByDate(int cultureGroupId, string eventDate)
        {
            return DA.Training.GetClassEventDetailsByDate(cultureGroupId, eventDate);
        }

        public static void DeleteShopCartAttendee(int attendeeId)
        {
            DA.Training.DeleteShopCartAttendee(attendeeId);
        }

        public static DataTable GetClassAttendeesInCart(string cartKey, int eventId)
        {
            return DA.Training.GetClassAttendeesInCart(cartKey, eventId);
        }

        public static DataTable GetCart(string cartKey)
        {
            return DA.Training.GetCart(cartKey);
        }

        public static void InsertClassAttendeesToShopCart(ShopCartAttendees attendeeObj)
        {
            DA.Training.InsertClassAttendeesToShopCart(attendeeObj);
        }

        public static DataTable GetItemsInCart(string cartKey,int cultureGroupId)
        {
            return DA.Training.GetItemsInCart(cartKey, cultureGroupId);
        }

        public static DataSet UpdateCartItemsDetails(int userId, int transactionId, int registrationId, string transactionKey,string cartKey)
        {
            return DA.Training.UpdateCartItemsDetails(userId, transactionId, registrationId,transactionKey,cartKey);
        }

        public static TraningRegistration GetRegistrationDetails(int registrationId)
        {
            return DA.Training.GetRegistrationDetails(registrationId);
        }

        public static void InsertRegistrationDetails(TraningRegistration registrationObj, out int registrationId)
        {
            DA.Training.InsertRegistrationDetails(registrationObj, out registrationId);
        }

        public static void DeleteShopCartItems(int eventId, string cartKey)
        {
            DA.Training.DeleteShopCartItems(eventId, cartKey);
        }
        public static void UpdateCartDetails(int userId, string cartKey)
        {
            UpdateCartDetails(userId, cartKey, null);
        }
        public static void UpdateCartDetails(int userId, string cartKey, string referredBy)
        {
            DA.Training.UpdateCartDetails(userId, cartKey, referredBy);
        }
        public static string InsertInquiryDetails(string userInfo)
        {
            return DA.Training.InsertInquiryDetails(userInfo);
        }

        public static string InsertTrainingInquiryDetails(string userInfo, string department, string comments)
        {
            return DA.Training.InsertTrainingInquiryDetails(userInfo, department, comments);
        }

        public static DataTable GetDistributorInfo(int eventId, int cultureGroupId)
        {
            return DA.Training.GetDistributorInfo(eventId, cultureGroupId);
        }

        public static Boolean Training_Payment_Transaction_IsExist(Guid transactionKey)
        {
            return DA.Training.Training_Payment_Transaction_Select(transactionKey) != null;
        }
    }
}
