﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DA = Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

using Anritsu.WebApp.GlobalWeb.DataAccess.Providers;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public static class TrainingAdminBL
    {
        public static int Register(TrainingAdminBO trainingadmin)
        {
            return TrainingAdminDA.ExecuteRegister(trainingadmin);
        }
    }
}
