﻿using System.Data;
using DA = Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public static class UniversalForm
    {
        public static DataRow GetEloquaInstanceById(int instanceId)
        {
            return DA.UniversalForm.GetEloquaInstanceById(instanceId);
        }
    }
}
