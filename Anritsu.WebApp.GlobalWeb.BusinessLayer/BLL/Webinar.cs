﻿using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using Anritsu.WebApp.GlobalWeb.DataAccess.Providers;

namespace Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL
{
    public class BllWebinar
    {
        public static void Webinar_AddLog(WebinarLog parm)
        {
            if (parm == null) return;
            DaWebinar.Webinar_AddLog(parm);
        }
        public static void VideoAccess_AddLog(VideoAccessLog parm)
        {
            if (parm == null) return;
            DaWebinar.VideoAccess_AddLog(parm);
        }
    }
}
