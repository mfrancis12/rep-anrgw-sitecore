﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects
{
    public class DownloadLog
    {
        public int DownloadId { get; set; }
        public string DownloadPageUrl { get; set; }
        public string DownloadFilePath { get; set; }
        public string UrlReferrer { get; set; }
        public string UserIpAddress { get; set; }
        public string UserPrimaryLlcc { get; set; }
        public string UserCountryCode { get; set; }
        public string BrowserAgent { get; set; }
        public string GwRegion { get; set; }
        public int GwUserId { get; set; }
        public bool IsLoginRequired { get; set; }
    }
}
