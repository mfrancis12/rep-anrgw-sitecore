﻿using System;
using System.Data;

namespace Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects
{
    public class GSetting
    {
        public int ID { get; set; }
        public int SettingID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string DisplayText { get; set; }
        public string Description { get; set; }

        public GSetting() { }

        public GSetting(DataRow row)
            : this()
        {
            if (row == null)
                return;

            ID = Convert.ToInt32(row["ID"]);
            SettingID = Convert.ToInt32(row["SettingID"]);
            Name = row["Name"].ToString();
            Value = row["Value"].ToString();
            DisplayText = row["DisplayText"].ToString();
            Description = row["Description"].ToString();
        }
    }
}
