﻿using System;

namespace Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects
{
    public enum OrderStatusType
    {
        Open = 1,
        Completed = 2,
        Canceled = 3,
        ChargeFailure = 4,
        PendingRefund = 5,
        Refunded = 6
    }

    [Serializable]
    public class ShopCartAttendees      
    {
        public int AttendeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EventId { get; set; }
        public string Phone { get; set; }
        public string Ext { get; set; }
        public string Email { get; set; }
        public int UserId {get;set;}
        public string  CartKey { get; set; }
    }

    [Serializable]
    public class TraningRegistration
    {
        public int RegistrationId { get; set; }
        public Guid TransactionKey { get; set; }
        public decimal Amount { get; set; }
        public decimal AdditionalAmount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string CountryName { get; set; }
        public string FaxNumber { get; set; }
        public string JobTitle { get; set; }
        public string PhoneNumber { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string ReferredBy { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }

    
}
