﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects
{
    public class TrainingAdminBO
    {
        public string CultureGroupId { get; set; }
        public string ClassId { get; set; }
        public string ClassName { get; set; }
        public string ClassDescription { get; set; }
        public string PageURL { get; set; }
        public string CreatedOnUTC { get; set; }
        public string ModifiedOnUTC { get; set; }
        public bool IsActive { get; set; }
        public string ContentTitle { get; set; }

        public string EventId { get; set; }
        //public int CultureGroupId { get; set; }
        //public int ClassId { get; set; }
        public string PartNumber { get; set; }
        public string DateFrom { get; set; }

        public string DateTo { get; set; }
        public string Location { get; set; }
        public string SeatsAllocated { get; set; }
        //public DateTime CreatedOnUTC { get; set; }
        //public DateTime ModifiedOnUTC { get; set; }
        public string DistributorId { get; set; }
        //public bool IsActive { get; set; }
        public string ExpiryDate { get; set; }
    }
}
