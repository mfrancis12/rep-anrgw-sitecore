﻿using System.Security.Policy;

namespace Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects
{
    public class WebinarLog
    {
        public string EventName { get; set; }
        public string Title { get; set; }
        public int CultureGroupId { get; set; }
        public int UserID { get; set; }
        public string ClientIP { get; set; }
        public string CreatedOnUtc { get; set; }
    }
    public class VideoAccessLog
    {
        public string VideoTitle { get; set; }
        public int CultureGroupId { get; set; }
        public int UserID { get; set; }
        public string ClientIP { get; set; }
        public string ViewDateOnUTC { get; set; }
    }

}
