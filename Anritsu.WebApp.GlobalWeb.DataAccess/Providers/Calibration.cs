﻿using System.Data;

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class Calibration
    {
        public static DataTable GetCertificatePricesForPart(string currentlang, string modelNumber)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn,
                    StoredProcedures.Calibration.Calibration_Certificate_Price_byPartNumber_Get);
                cmd.Parameters.AddWithValue("@PartNumber", modelNumber);
                cmd.Parameters.AddWithValue("@Language", currentlang);
                var tb = SqlProviderManager.FillInDataTable(cmd);
                return tb;
            }
        }

        public static DataTable GetProductNotesForPart(string modelNumber)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn,
                    StoredProcedures.Calibration.Calibration_Product_Notes_byPartNumber_Get);
                cmd.Parameters.AddWithValue("@PartNumber", modelNumber);
                var tb = SqlProviderManager.FillInDataTable(cmd);
                return tb;
            }
        }

        public static void InsertCalCertRequestsLog(int userId, string modelNumber, string serialNumber, string clientIp)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn,
                    StoredProcedures.Calibration.Calibration_Certificate_RequestLog_Insert);
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber);
                cmd.Parameters.AddWithValue("@ClientIP", clientIp);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataRow GetCalibrationCerttificateFile(string iUserId, string modelNumber, string serialNumber,
            string userIpAddress)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn,
                    StoredProcedures.Calibration.Calibration_Certificate_for_download_Get);
                cmd.Parameters.AddWithValue("@UserId", iUserId);
                cmd.Parameters.AddWithValue("@IPAddress", userIpAddress);
                cmd.Parameters.AddWithValue("@Model_Number", modelNumber);
                cmd.Parameters.AddWithValue("@Serial_Number", serialNumber);
                var dt = SqlProviderManager.FillInSingleRow(cmd);
                return dt;
            }
        }

        public static DataTable GetAllCertificates(string modelNumber, string serialNumber)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn,
                    StoredProcedures.Calibration.Calibration_Certificates_Search_Get);
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                var tb = SqlProviderManager.FillInDataTable(cmd);
                return tb;
            }
        }

        public static DataTable GetAllProductDetails(string currentlang,string modelNumber)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn,
                    StoredProcedures.Calibration.Calibration_Model_Search_Get);
                cmd.Parameters.AddWithValue("@Language", currentlang);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                var tb = SqlProviderManager.FillInDataTable(cmd);
                return tb;
            }
        }

    }
}
