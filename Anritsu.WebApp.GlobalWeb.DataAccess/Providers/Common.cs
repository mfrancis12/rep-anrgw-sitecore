﻿using System.Data.SqlClient;

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class Common
    {
        public static int GetCultureGroupIdForCultureCode(string cultureCode)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Common.GetCultureGroupID_Get);
                cmd.Parameters.AddWithValue("@CultureCode", cultureCode);
                int cultureGroupId;
                try
                {
                    cultureGroupId = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch
                {
                    cultureGroupId = 1;
                }
                return cultureGroupId;
            }
        }
    }
}
