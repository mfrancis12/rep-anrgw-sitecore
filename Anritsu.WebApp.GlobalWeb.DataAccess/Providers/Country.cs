﻿using System.Data;

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class Country
    {
        public static DataRow GetCountryByIp(string ipAddress)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetGeoIpConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn, "uSP_IPCountryLocation_GetByIP");
                cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
                return SqlProviderManager.FillInSingleRow(cmd);
            }
        }
    }
}
