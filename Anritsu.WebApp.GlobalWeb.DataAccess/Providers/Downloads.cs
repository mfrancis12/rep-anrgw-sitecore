﻿using System.Data;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class DaDownload
    {
        public static void Download_AddLog(DownloadLog parm)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.DownloadTrack.DownloadLogInsert);
                cmd.Parameters.AddWithValue("@DownloadId", parm.DownloadId);
                cmd.Parameters.AddWithValue("@LandingPageURL", parm.DownloadPageUrl);
                cmd.Parameters.AddWithValue("@DownloadFilePath", parm.DownloadFilePath);
                cmd.Parameters.AddWithValue("@UrlReferrer", parm.UrlReferrer);
                cmd.Parameters.AddWithValue("@ClientIP", parm.UserIpAddress);
                cmd.Parameters.AddWithValue("@UserPrimaryLLCC", parm.UserPrimaryLlcc);
                cmd.Parameters.AddWithValue("@UserCountryCode", parm.UserCountryCode);
                cmd.Parameters.AddWithValue("@UserBrowserAgent", parm.BrowserAgent);
                cmd.Parameters.AddWithValue("@GWRegion", parm.GwRegion);
                cmd.Parameters.AddWithValue("@GWUserId", parm.GwUserId);
                cmd.Parameters.AddWithValue("@IsLoginRequired", parm.IsLoginRequired);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
