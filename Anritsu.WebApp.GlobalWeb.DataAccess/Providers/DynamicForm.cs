﻿

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class DynamicForm
    {
        public static bool SaveSitecoreDynamiceFormDetails(string formName, string siteCoreFormId, int cultureGroupId, string userInformation, string additionalInfo)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.DynamicForm.SitecoreDynamicFormSave);
                cmd.Parameters.AddWithValue("@FormName", formName);
                cmd.Parameters.AddWithValue("@SireCoreFormId", siteCoreFormId);
                cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
                cmd.Parameters.AddWithValue("@UserInformation", userInformation);
                cmd.Parameters.AddWithValue("@AdditionalInfo", additionalInfo);
                cmd.ExecuteNonQuery();
            }
            return true;
        }
    }
}
