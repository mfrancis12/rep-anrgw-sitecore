﻿using System.Data;

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class Search
    {
        public static DataTable GetRegionalSettingsList(int? cultureGroupId)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Search.SEARCH_GETREGIONALSETTINGS_BYCULTUREGROUP);
                if (cultureGroupId.HasValue)
                    cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId.Value);

                var tb = SqlProviderManager.FillInDataTable(cmd);
                tb.PrimaryKey = new[] { tb.Columns[0] };
                return tb;
            }
        }
    }
}
