﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class Training
    {
        public static DataTable GetTrainingCategories(int cultureGroupId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.Categories_Get);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable GetClassesForCategory(int cultureGroupId, int categoryId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.Classes_For_Category_Get);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@CategoryID", categoryId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable GetClassesForCategoryByPageURL(int cultureGroupId, string pageUrl)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.Classes_For_Category_GetByEventPageURL);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@PageURL", pageUrl);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable GetClassList(int cultureGroupId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.GetClassList);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable SearchRegistrations(int? cultureGroupId, int? statusId, int? classId, string firstName, string lastName, int? transactionId, int? registrationId, string partNumber, int? eventId, string location, string emailId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.SearchRegistrations);

                if (cultureGroupId.HasValue)
                    cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId.Value);
                if (statusId.HasValue)
                    cmd.Parameters.AddWithValue("@StatusId", statusId.Value);
                if (classId.HasValue)
                    cmd.Parameters.AddWithValue("@ClassId", classId.Value);
                if (transactionId.HasValue)
                    cmd.Parameters.AddWithValue("@TransactionID", transactionId.Value);
                if (registrationId.HasValue)
                    cmd.Parameters.AddWithValue("@RegistrationId", registrationId.Value);
                if (eventId.HasValue)
                    cmd.Parameters.AddWithValue("@EventId", eventId.Value);

                if (!string.IsNullOrEmpty(firstName))
                    cmd.Parameters.AddWithValue("@FirstName", firstName);
                if (!string.IsNullOrEmpty(lastName))
                    cmd.Parameters.AddWithValue("@LastName", lastName);
                if (!string.IsNullOrEmpty(partNumber))
                    cmd.Parameters.AddWithValue("@PartNumber", partNumber);
                if (!string.IsNullOrEmpty(location))
                    cmd.Parameters.AddWithValue("@Location", location);
                if (!string.IsNullOrEmpty(emailId))
                    cmd.Parameters.AddWithValue("@EmailId", emailId);

                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);

                /*tb.Columns.Add("CompanyName", typeof(string));


                // obtain list of user ids in the results
                List<int> userIDs = new List<int>(tb.Rows.Count);
                foreach (DataRow row in tb.Rows)
                {
                    userIDs.Add((int)row["UserId"]);
                }

                // call sso service to get actual user information
                SSOSDK.AnritsuSsoClient ssoClient = new SSOSDK.AnritsuSsoClient();
                List<SSOSDK.SSOApiClient.GWSsoUserType> users = ssoClient.GetUserList(userIDs);


                // add the user data into the results datatable
                foreach (DataRow row in tb.Rows)
                {
                    // find the associated user record in the list
                    SSOSDK.SSOApiClient.GWSsoUserType ssoUser = users.Find(
                        delegate(SSOSDK.SSOApiClient.GWSsoUserType u)
                        {
                            return (u.UserId == (int)row["UserId"]);
                        });

                    if (ssoUser != null)
                        row["CompanyName"] = ssoUser.CompanyName;
                }
                */
                return tb;
            }
        }

        public static DataTable GetRegistrationStatusList()
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.GetRegistrationStatusList);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable GetClassEventsByClassID(int? cultureGroupId, int? classId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.GetClassEventsByClassID);

                if (cultureGroupId.HasValue)
                    cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId.Value);

                if (classId.HasValue)
                    cmd.Parameters.AddWithValue("@ClassID", classId.Value);

                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable GetClassEventDetailsBetweenGivenDates(int cultureGroupId, string dateFrom)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ClassEvents_Between_GivenDates_Get);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@FromDate", dateFrom);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable GetClassScheduleDetails(int cultureGroupId, string classId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ClassSchedule_Details_Get);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@ClassID", classId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static bool IsClassEventActive(int cultureGroupId, string classId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ClassEvent_IsActive);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@ClassID", classId);
                object returnValue = cmd.ExecuteScalar();
                bool bReturnValue = false;

                if (returnValue != null && returnValue is bool)
                    bReturnValue = (bool)returnValue;

                return bReturnValue;
            }
        }

        public static DataTable GetClassEventDetailsById(int cultureGroupId, int eventId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ClassEventDetailsByEventId_Get);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@EventID", eventId);
                cmd.CommandTimeout = 40;
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataRow GetClassEventAvailableSeats(int cultureGroupId, int eventId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ClassEvent_AvailableSeats);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@EventID", eventId);
                return SqlProviderManager.FillInSingleRow(cmd);
            }
        }

        public static DataTable GetClassEventDetailsByDate(int cultureGroupId, string eventDate)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ClassEventDetailsByGivenDate_Get);
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@eventDate", eventDate);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static void DeleteShopCartAttendee(int attendeeId)
        {

            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ShopCartAttendee_Delete);
                cmd.Parameters.AddWithValue("@AttendeeId", attendeeId);
                cmd.ExecuteNonQuery();
                SqlProviderManager.CloseSqlConnection(conn);

            }
        }

        public static DataTable GetClassAttendeesInCart(string cartKey, int eventId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ClassAttendeesInCart_Get);
                cmd.Parameters.AddWithValue("@CartKey", cartKey);
                cmd.Parameters.AddWithValue("@EventId", eventId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }
        public static void InsertClassAttendeesToShopCart(ShopCartAttendees attendeeObj)
        {

            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ClassAttendeesInShopCart_Insert);
                cmd.Parameters.AddWithValue("@CartKey", attendeeObj.CartKey);
                cmd.Parameters.AddWithValue("@UserId", attendeeObj.UserId);
                cmd.Parameters.AddWithValue("@EventId", attendeeObj.EventId);
                cmd.Parameters.AddWithValue("@FirstName", attendeeObj.FirstName);
                cmd.Parameters.AddWithValue("@LastName", attendeeObj.LastName);
                cmd.Parameters.AddWithValue("@PhoneNumber", attendeeObj.Phone);
                cmd.Parameters.AddWithValue("@EmailId", attendeeObj.Email);
                cmd.ExecuteNonQuery();
                SqlProviderManager.CloseSqlConnection(conn);
            }
        }

        public static DataTable GetItemsInCart(string cartKey, int cultureGroupId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ItemsInCart_Get);
                cmd.Parameters.AddWithValue("@CartKey", cartKey);
                cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable GetCart(string cartKey)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ShoppingCart_Get);
                cmd.Parameters.AddWithValue("@CartKey", cartKey);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataSet UpdateCartItemsDetails(int userId, int transactionId, int registrationId, string transactionKey, string cartKey)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.CartItemsDetails_Get);
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@TransactionId", transactionId);
                cmd.Parameters.AddWithValue("@RegistrationId", registrationId);
                cmd.Parameters.AddWithValue("@TransactionKey", transactionKey);
                cmd.Parameters.AddWithValue("@CartKey", cartKey);
                DataSet ds = SqlProviderManager.FillInDataSet(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return ds;
            }

        }

        public static DataRow Training_Payment_Transaction_Select(Guid transactionKey)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.Training_Payment_Transaction_Select);
                cmd.Parameters.AddWithValue("@TransactionKey", transactionKey);
                return SqlProviderManager.FillInSingleRow(cmd);
            }

        }

        public static TraningRegistration GetRegistrationDetails(int registrationId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.GetRegistrationDetails);
                cmd.Parameters.AddWithValue("@RegistrationID", registrationId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);

                if (tb != null && tb.Rows.Count == 1)
                {
                    DataRow registrationRow = tb.Rows[0];
                    var registration = new TraningRegistration
                    {
                        RegistrationId = registrationId,
                        TransactionKey = (Guid) registrationRow["TransactionKey"],
                        Amount = (decimal) registrationRow["Amount"],
                        Status = registrationRow["Status"] as string,
                        FirstName = registrationRow["FirstName"] as string,
                        LastName = registrationRow["LastName"] as string,
                        EmailId = registrationRow["EmailAddress"] as string,
                        ReferredBy = registrationRow["ReferredBy"] as string
                    };

                    if (!(registrationRow["AdditionalAmount"] is DBNull))
                        registration.AdditionalAmount = (decimal)registrationRow["AdditionalAmount"];

                    registration.Address1 = registrationRow["StreetAddress1"] as string;
                    registration.Address2 = registrationRow["StreetAddress2"] as string;
                    registration.City = registrationRow["City"] as string;
                    registration.Company = registrationRow["CompanyName"] as string;
                    registration.CountryName = registrationRow["CountryCode"] as string;
                    registration.FaxNumber = registrationRow["FaxNumber"] as string;
                    registration.JobTitle = registrationRow["JobTitle"] as string;
                    registration.PhoneNumber = registrationRow["PhoneNumber"] as string;
                    registration.PostalCode = registrationRow["ZipCode"] as string;
                    registration.State = registrationRow["State"] as string;


                    /*int userID = (int)registrationRow["UserId"];

                    // obtain the user information via SSO
                    SSOSDK.AnritsuSsoClient ssoClient = new SSOSDK.AnritsuSsoClient();
                    SSOSDK.SSOApiClient.GWSsoUserType ssoUser = ssoClient.GetUser(userID);

                    if (ssoUser != null)
                    {
                        registration.Address1 = ssoUser.StreetAddress1;
                        registration.Address2 = ssoUser.StreetAddress2;
                        registration.City = ssoUser.City;
                        registration.Company = ssoUser.CompanyName;
                        registration.CountryName = ssoUser.CountryName;
                        registration.FaxNumber = ssoUser.FaxNumber;
                        registration.JobTitle = ssoUser.JobTitle;
                        registration.PhoneNumber = ssoUser.PhoneNumber;
                        registration.PostalCode = ssoUser.PostalCode;
                        registration.State = ssoUser.State;
                    }*/

                    return registration;
                }
                return null;
            }
        }

        public static void UpdateRegistrationStatus(int registrationId, OrderStatusType status)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.UpdateRegistrationStatus);
                cmd.Parameters.AddWithValue("@RegistrationID", registrationId);
                cmd.Parameters.AddWithValue("@StatusID", status);
                cmd.ExecuteNonQuery();
                SqlProviderManager.CloseSqlConnection(conn);
            }
        }

        public static void UpdateRegistrationAdditionalAmount(int registrationId, decimal additionalAmount)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.UpdateRegistrationAdditionalAmount);
                cmd.Parameters.AddWithValue("@RegistrationID", registrationId);
                cmd.Parameters.AddWithValue("@AdditionalAmount", additionalAmount);
                cmd.ExecuteNonQuery();
                SqlProviderManager.CloseSqlConnection(conn);
            }
        }

        public static void CancelRegistration(int registrationId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.CancelRegistration);
                cmd.Parameters.AddWithValue("@RegistrationID", registrationId);
                cmd.ExecuteNonQuery();
                SqlProviderManager.CloseSqlConnection(conn);
            }
        }

        public static void CompleteRegistrationRefund(int registrationId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.CompleteRefund);
                cmd.Parameters.AddWithValue("@RegistrationID", registrationId);
                cmd.ExecuteNonQuery();
                SqlProviderManager.CloseSqlConnection(conn);
            }
        }

        public static DataTable GetRegisteredAttendees(int registrationId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.GetRegisteredAttendees);
                cmd.Parameters.AddWithValue("@RegistrationID", registrationId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static DataTable GetRegisteredAttendee(int attendeeId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.GetRegisteredAttendee);
                cmd.Parameters.AddWithValue("@AttendeeID", attendeeId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }

        public static void UpdateRegisteredAttendee(
            int attendeeId,
            int eventId,
            string firstName,
            string lastName,
            string phoneNumber,
            string phoneExtension,
            string email,
            string qadSalesOrder
            )
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.UpdateRegisteredAttendee);
                cmd.Parameters.AddWithValue("@AttendeeID", attendeeId);
                cmd.Parameters.AddWithValue("@EventID", eventId);
                cmd.Parameters.AddWithValue("@FirstName", firstName);
                cmd.Parameters.AddWithValue("@LastName", lastName);
                cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                cmd.Parameters.AddWithValue("@Extension", phoneExtension);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@QADSalesOrder", qadSalesOrder);
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertRegisteredAttendee(
            int registrationId,
            int eventId,
            string firstName,
            string lastName,
            string phoneNumber,
            string phoneExtension,
            string email,
            string qadSalesOrder
            )
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.InsertRegisteredAttendee);
                cmd.Parameters.AddWithValue("@RegistrationID", registrationId);
                cmd.Parameters.AddWithValue("@EventID", eventId);
                cmd.Parameters.AddWithValue("@FirstName", firstName);
                cmd.Parameters.AddWithValue("@LastName", lastName);
                cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                cmd.Parameters.AddWithValue("@Extension", phoneExtension);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@QADSalesOrder", qadSalesOrder);
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteRegisteredAttendee(int attendeeId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.DeleteRegisteredAttendee);
                cmd.Parameters.AddWithValue("@AttendeeID", attendeeId);
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertRegistrationDetails(TraningRegistration registrationObj, out int registrationId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.TrainingRegistration_Insert);
                cmd.Parameters.AddWithValue("@Amount", registrationObj.Amount);
                cmd.Parameters.AddWithValue("@FirstName", registrationObj.FirstName);
                cmd.Parameters.AddWithValue("@LastName", registrationObj.LastName);
                cmd.Parameters.AddWithValue("@EmailId", registrationObj.EmailId);
                cmd.Parameters.AddWithValue("@UserId", registrationObj.UserId);
                cmd.Parameters.AddWithValue("@ReferredBy", registrationObj.ReferredBy);

                cmd.Parameters.AddWithValue("@Company", registrationObj.Company);
                cmd.Parameters.AddWithValue("@JobTitle", registrationObj.JobTitle);
                cmd.Parameters.AddWithValue("@Address1", registrationObj.Address1);
                cmd.Parameters.AddWithValue("@Address2", registrationObj.Address2);
                cmd.Parameters.AddWithValue("@CountryName", registrationObj.CountryName);
                cmd.Parameters.AddWithValue("@City", registrationObj.City);
                cmd.Parameters.AddWithValue("@PhoneNumber", registrationObj.PhoneNumber);
                cmd.Parameters.AddWithValue("@State", registrationObj.State);
                cmd.Parameters.AddWithValue("@PostalCode", registrationObj.PostalCode);
                cmd.Parameters.AddWithValue("@FaxNumber", registrationObj.FaxNumber);


                var paramRegId = new SqlParameter
                {
                    ParameterName = "@RegistrationId",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Output
                };

                cmd.Parameters.Add(paramRegId);

                cmd.ExecuteNonQuery();
                registrationId = int.Parse(paramRegId.Value.ToString());
                SqlProviderManager.CloseSqlConnection(conn);
            }
        }

        public static void DeleteShopCartItems(int eventId, string cartKey)
        {

            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.ShopCartItems_Delete);
                cmd.Parameters.AddWithValue("@EventId", eventId);
                cmd.Parameters.AddWithValue("@CartKey", cartKey);
                cmd.ExecuteNonQuery();
                SqlProviderManager.CloseSqlConnection(conn);

            }
        }

        public static void UpdateCartDetails(int userId, string cartKey, string referredBy)
        {

            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.CartDetails_Update);
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@CartKey", cartKey);

                if (referredBy != null)
                    cmd.Parameters.AddWithValue("@ReferredBy", referredBy);

                cmd.ExecuteNonQuery();
                SqlProviderManager.CloseSqlConnection(conn);

            }
        }

        public static string InsertInquiryDetails(string userInfo)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var paramRefNum = new SqlParameter
                {
                    ParameterName = "@UserId",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Output
                };

                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.General.SubmitUserInfo_Insert);
                cmd.Parameters.AddWithValue("@UserInfo", userInfo);
                cmd.Parameters.Add(paramRefNum);

                cmd.ExecuteNonQuery();
                return paramRefNum.Value.ToString();
            }
        }

        public static string InsertTrainingInquiryDetails(string userInfo, string department, string comments)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var paramRefNum = new SqlParameter
                {
                    ParameterName = "@UserId",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Output
                };

                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.TrainingSubmitUserInfo);
                cmd.Parameters.AddWithValue("@UserInfo", userInfo);
                cmd.Parameters.AddWithValue("@Department", department);
                cmd.Parameters.AddWithValue("@Comments", comments);
                cmd.Parameters.Add(paramRefNum);

                cmd.ExecuteNonQuery();
                return paramRefNum.Value.ToString();
            }
        }


        public static DataTable GetDistributorInfo(int eventId, int cultureGroupId)
        {
            using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.DistributorInfo_Get);
                cmd.Parameters.AddWithValue("@EventId", eventId);
                cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
                DataTable tb = SqlProviderManager.FillInDataTable(cmd);
                SqlProviderManager.CloseSqlConnection(conn);
                return tb;
            }
        }
    }
}
