﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public class TrainingAdminDA
    {
        public static int ExecuteRegister(TrainingAdminBO trainingadmin)
        {
             using (SqlConnection conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                int res;
                SqlCommand cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.Training.Training_Admin);

                cmd.Parameters.AddWithValue("@ClassId", trainingadmin.ClassId);
                cmd.Parameters.AddWithValue("@ClassName", trainingadmin.ClassName);
                cmd.Parameters.AddWithValue("@CreatedOnUTC", trainingadmin.CreatedOnUTC);
                cmd.Parameters.AddWithValue("@ContentTitle", trainingadmin.ContentTitle);
                cmd.Parameters.AddWithValue("@CultureGroupId", trainingadmin.CultureGroupId);
                cmd.Parameters.AddWithValue("@DateFrom", trainingadmin.DateFrom);
                cmd.Parameters.AddWithValue("@DateTo", trainingadmin.DateTo);
                cmd.Parameters.AddWithValue("@DistributorId", trainingadmin.DistributorId);
                cmd.Parameters.AddWithValue("@EventId", trainingadmin.EventId);
                cmd.Parameters.AddWithValue("@ExpiryDate", trainingadmin.ExpiryDate);
                cmd.Parameters.AddWithValue("@IsActive", trainingadmin.IsActive);
                cmd.Parameters.AddWithValue("@ModifiedOnUTC", trainingadmin.ModifiedOnUTC);
                cmd.Parameters.AddWithValue("@PageURL", trainingadmin.PageURL);
                cmd.Parameters.AddWithValue("@PartNumber", trainingadmin.PartNumber);
                cmd.Parameters.AddWithValue("@SeatsAllocated", trainingadmin.SeatsAllocated);

                try
                {
                    return res=cmd.ExecuteNonQuery();
                }
                catch(Exception e) {
                    return res=0;
                }
                  //SqlParameter outPutParameter = new SqlParameter();
                  //  outPutParameter.ParameterName = “@res”;
                  //  outPutParameter.SqlDbType = System.Data.SqlDbType.Int;
                  //  outPutParameter.Direction = System.Data.ParameterDirection.Output;
                  //  cmd.Parameters.Add(outPutParameter);
                  //   string EmployeeId = outPutParameter.Value.ToString();
                  //  lblMessage.Text = “Employee Id = “ + EmployeeId;
                //SqlParameter ClassId = new SqlParameter("@ClassId", ta.ClassId);
                //SqlParameter ClassName = new SqlParameter("@ClassName", ta.ClassName);  //Registration of users
                //SqlParameter CreatedOnUTC = new SqlParameter("@CreatedOnUTC", ta.CreatedOnUTC);
                //SqlParameter ContentTitle = new SqlParameter("@ContentTitle", ta.ContentTitle);
                //SqlParameter CultureGroupId = new SqlParameter("@CultureGroupId", ta.CultureGroupId);
                //SqlParameter DateFrom = new SqlParameter("@DateFrom", ta.DateFrom);
                //SqlParameter DateTo = new SqlParameter("@DateTo", ta.DateTo);
                //SqlParameter DistributorId = new SqlParameter("@DistributorId", ta.DistributorId);  //Inserting user details in to Database.
                //SqlParameter EventId = new SqlParameter("@EventId", ta.EventId);
                //SqlParameter ExpiryDate = new SqlParameter("@ExpiryDate", ta.ExpiryDate);
                //SqlParameter IsActive = new SqlParameter("@IsActive", ta.IsActive);
                //SqlParameter ModifiedOnUTC = new SqlParameter("@ModifiedOnUTC", ta.ModifiedOnUTC);  //Inserting user details in to Database.
                //SqlParameter PageURL = new SqlParameter("@PageURL", ta.PageURL);
                //SqlParameter PartNumber = new SqlParameter("@PartNumber", ta.PartNumber);  //Inserting user details in to Database.
                //SqlParameter SeatsAllocated = new SqlParameter("@SeatsAllocated", ta.SeatsAllocated);
                //int res = 0;

                //return Convert.ToInt32(SqlProviderManager.GetResultsAsScalar(conn, CommandType.StoredProcedure, ReusableMethods.StoredProcedure.registeredUsers, fname, lname, gender, email, uname, password, num, image, isadmin));
             }
        }
    }
}
