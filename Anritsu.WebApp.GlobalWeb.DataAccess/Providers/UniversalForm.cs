﻿using System.Data;

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class UniversalForm
    {
        public static DataRow GetEloquaInstanceById(int instanceId)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.UniversalForm.UFA_GetEloquaInstanceById);
                cmd.Parameters.AddWithValue("@InstanceId", instanceId);
                return SqlProviderManager.FillInDataRow(cmd);
            }
        }
    }
}
