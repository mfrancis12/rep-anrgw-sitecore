﻿using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;

namespace Anritsu.WebApp.GlobalWeb.DataAccess.Providers
{
    public static class DaWebinar
    {
        public static void Webinar_AddLog(WebinarLog parm)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.TrackWebinar.WebinarDataCollection);
                cmd.Parameters.AddWithValue("@EventName", parm.EventName);
                cmd.Parameters.AddWithValue("@Title", parm.Title);
                cmd.Parameters.AddWithValue("@CultureGroupId", parm.CultureGroupId);
                cmd.Parameters.AddWithValue("@UserID", parm.UserID);
                cmd.Parameters.AddWithValue("@ClientIP", parm.ClientIP);
                cmd.Parameters.AddWithValue("@CreatedOnUTC", parm.CreatedOnUtc);
                cmd.ExecuteNonQuery();
            }
        }

        public static void VideoAccess_AddLog(VideoAccessLog parm)
        {
            using (var conn = SqlProviderManager.GetOpenedConnection(Utilities.GetConnectionString()))
            {
                var cmd = SqlProviderManager.MakeSpCommand(conn, StoredProcedures.TrackWebinar.VideoAccessDataCollection);
                cmd.Parameters.AddWithValue("@VideoTitle", parm.VideoTitle);
                cmd.Parameters.AddWithValue("@CultureGroupId", parm.CultureGroupId);
                cmd.Parameters.AddWithValue("@UserID", parm.UserID);
                cmd.Parameters.AddWithValue("@ClientIP", parm.ClientIP);
                cmd.Parameters.AddWithValue("@ViewDateOnUTC", parm.ViewDateOnUTC);
               
                cmd.ExecuteNonQuery();
            }
        }
    }
}
