﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Anritsu.WebApp.GlobalWeb.DataAccess

{
    public class SqlProviderManager
    {
        public static SqlConnection GetOpenedConnection(string connStr)
        {
            //if (StringUtils.IsNullOrEmptyString(connStr))
            //    return null;

            try
            {
                var conn = new SqlConnection(connStr);
                conn.Open();
                return conn;
            }
            catch (Exception)
            {
                //if (ConfigurationManager.AppSettings["NotifyDBDown"] != null &&
                //    ConfigurationManager.AppSettings["NotifyDBDown"].ToLower().Equals("true"))
                //{
                //    #region " db down, do notification "
                //    try
                //    {
                //        string recipient = "webmaster@anritsu.com";
                //        string sender = "WebMasterNoReply@anritsu.com";

                //        string subject = string.Format("DB error notification from {0}"
                //            , AppDomain.CurrentDomain.FriendlyName);

                //        #region email body
                //        StringBuilder sbBody = new StringBuilder();
                //        sbBody.Append(string.Format("Connection string key used : {0}", connStr));
                //        sbBody.Append("\n\r");
                //        sbBody.Append(string.Format("Server time : {0}", DateTime.Now.ToString()));
                //        sbBody.Append(string.Format("ERROR : {0}", ex.StackTrace));
                //        #endregion

                //        bool isBodyHtml = false;
                //        bool ignoreBadAddresses = true;

                //        //(new EmailUtil()).Send(recipient, sender, subject, sbBody.ToString());

                //        LegacyMail.SendMail(recipient, sender, subject, sbBody.ToString(), isBodyHtml, ignoreBadAddresses);

                //    }
                //    catch { }
                //    #endregion
                //}
                return null;
            }
        }


        public static void CloseSqlConnection(SqlConnection conn)
        {
            if (conn != null && conn.State == ConnectionState.Open)
                conn.Close();
        }

        public static SqlCommand MakeSpCommand(SqlConnection sqlConn, SqlTransaction sqlTran, string spName)
        {
            SqlCommand cmd = MakeSpCommand(sqlConn, spName);
            if (sqlTran != null)
                cmd.Transaction = sqlTran;
            return cmd;
        }

        public static SqlCommand MakeSpCommand(SqlConnection sqlConn, string sSpName)
        {
            var cmd = new SqlCommand(sSpName, sqlConn)
            {
                CommandText = sSpName,
                CommandType = CommandType.StoredProcedure
            };
            return cmd;
        }        

        public static SqlCommand MakeSpCommand(SqlConnection conn
            , SqlTransaction tran
            , string spSchema
            , string spName
            , int timeout)
        {
            if (string.IsNullOrEmpty(spSchema)) spSchema = "[dbo]";
            if (string.IsNullOrEmpty(spName)) throw new ArgumentNullException("spName");

            string sp = spSchema + "." + spName;
            SqlCommand cmd = MakeSpCommand(conn, tran, sp);
            if (timeout > 0) cmd.CommandTimeout = timeout;
            return cmd;
        }

        public static DataSet FillInDataSet(SqlCommand sqCmd)
        {
            var da = new SqlDataAdapter(sqCmd);
            var ds = new DataSet();
            da.Fill(ds);
            return ds;

        }

        public static DataSet FillInDataSet(SqlCommand sqCmd, string tbName)
        {
            var da = new SqlDataAdapter(sqCmd);
            var ds = new DataSet();
            da.Fill(ds, tbName);
            return ds;
        }

        public static DataSet FillMultipleTablesInDataSet(SqlDataAdapter sqDa)
        {
            if (sqDa == null || sqDa.SelectCommand == null)
                return null;

            var ds = new DataSet();
            sqDa.Fill(ds);
            return ds;

        }

        /// <summary>
        /// Call this to get DataTable from SqlCommand
        /// </summary>
        /// <param name="sqCmd"></param>
        /// <returns></returns>
        public static DataTable FillInDataTable(SqlCommand sqCmd)
        {
            DataSet ds = FillInDataSet(sqCmd);
            if (ds != null && ds.Tables.Count > 0)
                return ds.Tables[0];
            return null;

        }

        public static DataTable FillInDataTable(SqlCommand sqCmd, string tbName)
        {
            DataSet ds = FillInDataSet(sqCmd, tbName);
            if (ds != null && ds.Tables.Count > 0)
                return ds.Tables[0];
            return null;

        }

        public static DataRow FillInDataRow(SqlCommand sqCmd)
        {
            var tb = FillInDataTable(sqCmd);
            if (tb == null || tb.Rows.Count < 1) return null;
            return tb.Rows[0];
        }

        /// <summary>
        /// Call this to get Single row from SqlCommand
        /// </summary>
        /// <param name="sqCmd"></param>
        /// <returns></returns>
        public static DataRow FillInSingleRow(SqlCommand sqCmd)
        {
            DataSet ds = FillInDataSet(sqCmd);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                return ds.Tables[0].Rows[0];
            return null;
        }



        public static DataTable GetResultsAsDataTable(string cxnString, string sProc)
        {
            var sqlParams = new SqlParameter[0];

            return GetResultsAsDataTable(cxnString, sProc, sqlParams);
        }


        public static DataTable GetResultsAsDataTable(string cxnString,
                                                        string sProc, SqlParameter[] sqlParams)
        {
            DataTable results = null;

            using (var cxn = new SqlConnection(cxnString))
            {
                using (var cmd = new SqlCommand(sProc, cxn))
                {
                    #region Setup SqlCommand properties
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (sqlParams != null && sqlParams.Length > 0)
                    {
                        foreach (SqlParameter t in sqlParams)
                        {
                            cmd.Parameters.Add(t);
                        }
                    }
                    #endregion

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        #region Get the data
                        try
                        {
                            var ds = new DataSet();
                            da.Fill(ds);
                            results = new DataTable();
                            results = ds.Tables[0];
                        }
                        catch (Exception)
                        {
                            //ExceptionUtils.LogError(AnritsuAppsEnum.OTHER, err);
                        }
                        finally
                        {
                            #region clean up resources
                            da.Dispose();

                            cmd.Dispose();

                            if (cxn.State == ConnectionState.Open)
                            {
                                cxn.Close();
                            }

                            cxn.Dispose();

                            #endregion
                        }
                        #endregion
                    }
                }
            }

            return results;
        }


        public static object GetResultsAsScalar(string cxnString, string sProc)
        {
            var sqlParams = new SqlParameter[0];

            return GetResultsAsScalar(cxnString, sProc, sqlParams);
        }


        public static object GetResultsAsScalar(string cxnString,
                                                    string sProc, SqlParameter[] sqlParams)
        {
            object scalarResult = null;

            using (var cxn = new SqlConnection(cxnString))
            {
                using (var cmd = new SqlCommand(sProc, cxn))
                {
                    #region Setup SqlCommand properties
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (sqlParams != null)
                    {
                        foreach (SqlParameter t in sqlParams)
                        {
                            cmd.Parameters.Add(t);
                        }
                    }
                    #endregion

                    try
                    {
                        cxn.Open();

                        scalarResult = cmd.ExecuteScalar();
                    }
                    catch (Exception)
                    {
                        //ExceptionUtils.LogError(AnritsuAppsEnum.OTHER, err);
                    }
                    finally
                    {
                        #region clean up resources

                        cmd.Dispose();

                        if (cxn.State == ConnectionState.Open)
                        {
                            cxn.Close();
                        }

                        cxn.Dispose();

                        #endregion
                    }
                }
            }

            return scalarResult;
        }


        public static int ExecuteCommand(string cxnString, string sProc)
        {
            var sqlParams = new SqlParameter[0];

            return ExecuteCommand(cxnString, sProc, sqlParams);
        }


        public static int ExecuteCommand(string cxnString,
                                            string sProc, SqlParameter[] sqlParams)
        {
            int returnStatusCode = -999;

            using (var cxn = new SqlConnection(cxnString))
            {
                using (var cmd = new SqlCommand(sProc, cxn))
                {
                    #region Setup SqlCommand properties
                    cmd.CommandType = CommandType.StoredProcedure;

                    foreach (SqlParameter t in sqlParams)
                    {
                        cmd.Parameters.Add(t);
                    }

                    cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int);
                    cmd.Parameters["@RETURN_VALUE"].Direction = ParameterDirection.ReturnValue;
                    #endregion

                    try
                    {
                        cxn.Open();

                        cmd.ExecuteNonQuery();

                        returnStatusCode = (int)(cmd.Parameters["@RETURN_VALUE"]).Value;
                    }
                    catch (Exception)
                    {
                        //ExceptionUtils.LogError(AnritsuAppsEnum.OTHER, err);
                    }
                    finally
                    {
                        #region clean up resources

                        cmd.Dispose();

                        if (cxn.State == ConnectionState.Open)
                        {
                            cxn.Close();
                        }

                        cxn.Dispose();

                        #endregion
                    }
                }
            }

            return returnStatusCode;
        }
    }
}
