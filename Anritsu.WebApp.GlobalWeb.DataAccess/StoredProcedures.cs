﻿using System.Security.Policy;

namespace Anritsu.WebApp.GlobalWeb.DataAccess
{
    public static class StoredProcedures
    {

        #region Common

        public static class General
        {
            public const string SubmitUserInfo_Insert = "uSP_Anonymous_User_Information_Insert";
        }

        #endregion

        #region Common

        public static class Common
        {
            public const string GetCultureGroupID_Get = "uSP_AR_Resources_GetCultureGroupID";
        }

        #endregion

        #region Training - Support

        public static class Training
        {
            public const string SearchRegistrations = "uSP_Support_TrainingRegistration_Search";
            public const string GetRegistrationDetails = "uSP_Support_TrainingRegistration_Get";
            public const string UpdateRegistrationStatus = "uSP_Support_TrainingRegistration_UpdateStatus";

            public const string UpdateRegistrationAdditionalAmount =
                "uSP_Support_TrainingRegistration_UpdateAdditionalAmount";

            public const string CancelRegistration = "uSP_Support_TrainingRegistration_Cancel";
            public const string CompleteRefund = "uSP_Support_TrainingRegistration_CompleteRefund";
            public const string GetRegistrationStatusList = "uSP_Support_TrainingRegStatus_GetAll";
            public const string GetClassList = "uSP_Support_TrainingClass_GetAll";
            public const string GetRegisteredAttendees = "uSP_Support_TrainingRegisteredAttendees_GetAll";
            public const string GetRegisteredAttendee = "uSP_Support_TrainingRegisteredAttendee_ByAttendeeID";
            public const string UpdateRegisteredAttendee = "uSP_Support_TrainingRegisteredAttendee_Update";
            public const string InsertRegisteredAttendee = "uSP_Support_TrainingRegisteredAttendee_Insert";
            public const string DeleteRegisteredAttendee = "uSP_Support_TrainingRegisteredAttendee_Delete";

            public const string Categories_Get = "uSP_Support_Training_Category_Get";
            public const string Classes_For_Category_Get = "uSP_Support_Training_CategoryCourses_Get";

            public const string Classes_For_Category_GetByEventPageURL =
                "uSP_Support_Training_CategoryCourses_GetByEventPageURL";

            public const string ClassEvents_Between_GivenDates_Get = "uSP_Support_Training_ClassEventsBetweenDates_Get";
            public const string GetClassEventsByClassID = "uSP_Support_Training_Event_GetByClassID";
            public const string ClassSchedule_Details_Get = "uSP_Support_Training_ClassSchedule_Get"; //
            public const string ClassEvent_IsActive = "uSP_Support_Training_ClassEvent_IsActive"; //
            public const string ClassEvent_AvailableSeats = "uSP_Support_Training_ClassEventAvailableSeats_Get";

            public const string ClassEventDetailsByEventId_Get = "uSP_Support_Training_ClassEventDetailsByID_Get"; //
            public const string ClassEventDetailsByGivenDate_Get = "uSP_Support_Training_ClassEventsOnDate_Get";
            public const string ShopCartAttendee_Delete = "uSP_Training_ShopCart_Attendee_Delete"; //
            public const string ClassAttendeesInCart_Get = "uSP_Support_Training_ClassAttendeesInCart_Get"; //
            public const string ClassAttendeesInShopCart_Insert = "uSP_Training_ShopCart_Attendee_Insert"; //

            public const string ShoppingCart_Get = "uSP_Support_Training_ShoppingCart_Get"; //
            public const string ItemsInCart_Get = "uSP_Support_Training_ItemsInCart_Get";
            public const string CartItemsDetails_Get = "uSP_Support_Training_CartItemsDetails_Update";
            public const string CartDetails_Update = "uSP_Support_Training_CartDetails_Update"; //
            public const string TrainingRegistration_Insert = "uSP_Training_Registration_Insert";
            public const string ShopCartItems_Delete = "uSP_Support_Training_ItemsInCart_Delete";
            public const string DistributorInfo_Get = "uSP_Support_Training_Distributor_Get";
            public const string TrainingSubmitUserInfo = "uSP_Support_Training_SubmitUserInfo";

            public const string Training_Payment_Transaction_Select = "uSP_Support_Training_Payment_Transaction_Select";

            public const string Training_Admin = "uSP_TrainingAdmin_Events_Insert"; //
        }

        #endregion

        #region Calibration

        public static class Calibration
        {
            public const string Calibration_Model_Search_Get = "uSP_Repair_Calibration_Price_Model_Search_Get";
            public const string Calibration_Certificates_Search_Get = "uSP_Repair_Calibration_Certificates_Search_Get";

            public const string Calibration_Certificate_Price_byPartNumber_Get =
                "uSP_Repair_Calibration_Certificate_Price_byPartNumber_Get";

            public const string Calibration_Product_Notes_byPartNumber_Get =
                "uSP_Repair_Calibration_Product_Notes_byPartNumber_Get";

            public const string Calibration_Request_Service_Quote_Insert =
                "uSP_Repair_Calibration_Request_ServiceQuote_Insert";

            public const string Calibration_Certificate_Download =
                "uSP_Repair_Calibration_Certificates_DownloadCert_Get";

            public const string Calibration_RequestRetrofitQuote_Insert =
                "uSP_Repair_Calibration_RequestRetrofitQuote_Insert";

            public const string Calibration_Certificate_for_download_Get =
                "uSP_Repair_Calibration_Certificates_DownloadCert_Get";

            public const string Calibration_RequestPartQuote_Insert = "uSP_Repair_Calibration_RequestPartsQuote_Insert";

            public const string Calibration_Certificate_RequestLog_Insert =
                "uSP_Repair_Calibration_CertificateRequests_AddLog";
        }

        #endregion

        #region UniversalForm

        public static class UniversalForm
        {
            public const string UFA_GetEloquaInstanceById = "uSP_UFA_GetEloquaInstanceById";
        }

        #endregion

        #region Sitecore Synamic Form

        public static class DynamicForm
        {
            public const string SitecoreDynamicFormSave = "uSP_SitecoreDynamicFormDetails_Insert";
        }

        #endregion

        #region Search

        public static class Search
        {
            public const string SEARCH_GETREGIONALSETTINGS_BYCULTUREGROUP = "usp_Search_RegionalSettings_Select_ByCultureGroup";
        }

        #endregion

        #region Downloads
        public class DownloadTrack
        {
            public const string DownloadLogInsert = "[dbo].[uSP_Downloads_LandingPageLog_Insert]";
        }
        #endregion

        #region Webinars

        public class TrackWebinar
        {
            public const string WebinarDataCollection = "[dbo].[uSP_Webinars_DataCollection_Insert]";
            public const string VideoAccessDataCollection = "[dbo].[uSP_GW_VideoAccess_Log_Insert]";
        }
        #endregion
    }
}

