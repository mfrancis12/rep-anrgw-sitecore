﻿using System.Configuration;

namespace Anritsu.WebApp.GlobalWeb.DataAccess
{
    public class Utilities
    {
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString;
        }

        public static string GetGeoIpConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DB_GeoIP_ConnectionString"].ConnectionString;
        }
    }
}
