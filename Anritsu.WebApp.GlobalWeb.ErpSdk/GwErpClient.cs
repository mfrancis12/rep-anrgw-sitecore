﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Anritsu.WebApp.GlobalWeb.ErpSdk.ErpWS;
using System.Linq;
using System.Configuration;

namespace Anritsu.WebApp.GlobalWeb.ErpSdk
{
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Erp"), SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gw"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Gw")]
    public class GwErpClient
    {
        private Guid _serviceToken;

        private ErpService _service;
        internal ErpService Service
        {
            get
            {
                return _service ?? (_service = new ErpService
                {
                    GWApiHeader_ApiTypeValue = new GWApiHeader_ApiType { SToken = _serviceToken.ToString() },
                    Timeout = int.Parse(ConfigurationManager.AppSettings["GwErpSdk_TimeoutMilliSeconds"]),
                    Url = ConfigurationManager.AppSettings["GwErpSdk_ServiceEndpointURL"]
                    //Timeout = 100000,
                    //Url = "http://qa-webservices.anritsu.com/GWERPAPI/ErpService.asmx"
                });
            }
        }
        //public string SsoServiceEncKey{ get { return _EncKey; } }
        public GwErpClient()
        {
            _serviceToken = new Guid(ConfigurationManager.AppSettings["GwErpSdk_ServiceToken"]);
            //_serviceToken = new Guid("B814A12B-E153-4D86-BE46-596C38E7477B");
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Rp"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Rp"), SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "ll"), SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "e"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Cc")]
        public GetWarrantyInfoCall_RespType CheckWarrantyInfo(
            ErpSource_ApiType eRpSource
            , string modelNumber
            , string serialNumber
            , string llCc
            )
        {
            var parm = new GetWarrantyInfoCall_ReqType
            {
                ModelNumber = modelNumber.Trim(),
                SerialNumber = serialNumber.Trim(),
                LL_CC = llCc,
                ERPSource = eRpSource
            };
            var resp = Service.GetWarrantyInfoCall(parm);
            CheckResponse(resp);
            return resp;
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Rp"), SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "e"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Rp"), SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "ll"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Cc")]
        public GetWarrantyInfoCallByPartialModel_RespType CheckWarrantyInfoByPartialModel(
           ErpSource_ApiType eRpSource
           , string modelNumber
           , string serialNumber
           , string llCc
           )
        {
            var parm = new GetWarrantyInfoCall_ReqType
            {
                ModelNumber = modelNumber.Trim(),
                SerialNumber = serialNumber.Trim(),
                LL_CC = llCc,
                ERPSource = eRpSource
            };
            var resp = Service.GetWarrantyInfoByPartialModelCall(parm);
            CheckResponse(resp);
            return resp;
        }

        [SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper")]
        public RMASVC_FindServiceCenterCallRespType FindServiceCenter(
            string modelNumber
            , int pricingTypeId
            , string customerShipToState)
        {
            if (string.IsNullOrEmpty(modelNumber))
                throw new ArgumentException("Invalid model number.");
            if (string.IsNullOrEmpty(customerShipToState) || customerShipToState.Length != 2)
                throw new ArgumentException("Invalid SHIP-TO state code.");

            var parm = new RMASVC_FindServiceCenterCallReqType
            {
                ModelNumber = modelNumber.ToUpper(),
                PricingTypeId = pricingTypeId,
                CustomerShipToState = customerShipToState.ToUpper()
            };
            var resp = Service.RMASVC_FindServiceCenterCall(parm);
            CheckResponse(resp);
            return resp;
        }

        [SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public WtyCoverage_GetAllCall_RespType WarrantyCoverage_GetAll()
        {
            var resp = Service.WtyCoverage_GetAllCall();
            CheckResponse(resp);
            return resp;
        }

        [SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public void WarrantyCoverage_AddNew(int warrantyTypeId, string svcPricingTypeCode)
        {
            var parm = new WtyCoverage_AddNewCall_ReqType
            {
                WarrantyTypeId = warrantyTypeId,
                SvcPricingTypeCode = svcPricingTypeCode
            };
            var resp = Service.WtyCoverage_AddNewCall(parm);
            CheckResponse(resp);
        }

        [SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public void WarrantyCoverage_DeleteByRecordId(int recordId)
        {
            var resp = Service.WtyCoverage_DeleteByRecordIdCall(recordId);
            CheckResponse(resp);
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "erp"), SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores"), SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<WarrantyType_ApiType> WarrantyType_GetAll(ErpSource_ApiType erpSource)
        {
            var req =
                new GetWarrantyTypeCollectionCall_ReqType { ErpSource = erpSource };
            var resp =
                Service.GetWarrantyTypeCollectionCall(req);
            CheckResponse(resp);
            return resp.WarrantyTypeCollection == null ? null : resp.WarrantyTypeCollection.ToList();
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "erp"), SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores"), SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Desc")]
        public void WarrantyType_Update(ErpSource_ApiType erpSource
            , string internalCode, string warrantyTypeDesc)
        {
            if (string.IsNullOrEmpty(internalCode) || string.IsNullOrEmpty(warrantyTypeDesc))
                throw new ArgumentException("Invalid parameter to update warranty type.");

            var req =
                new UpdateWarrantyTypeCall_ReqType
                {
                    ErpSource = erpSource,
                    InternalCode = internalCode.Trim(),
                    WarrantyTypeDesc = warrantyTypeDesc.Trim()
                };
            var resp =
                Service.UpdateWarrantyTypeCall(req);
            CheckResponse(resp);
        }

        private void CheckResponse(ApiCallBase_RespType res)
        {
            if (res == null)
                throw new ArgumentException("Please try again later.  system is down.");
            if (res.CallStatus != CallStatus_ApiType.SUCCESS)
                throw new ArgumentException(res.ResponseMessage);
        }
    }
}
