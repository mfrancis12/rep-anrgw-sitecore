﻿using Anritsu.WebApp.GlobalWeb.ExternalWebApi.Interfaces;
using Anritsu.WebApp.GlobalWeb.ExternalWebApi.Models.Brightcove;
using Anritsu.WebApp.GlobalWeb.ExternalWebApi.Utilities;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.ExternalWebApi.Api
{
    public class BrightcoveWebApi : IBrightcoveWebApi
    {
        private static HttpClient HttpClient = new HttpClient()
        {
            Timeout = TimeSpan.FromMilliseconds(3000)
        };

        private const string PolicyKey = @"BCpkADawqM0pVZj1r7uRga7CqQOWM3B7SDzO3ATZ4yjoJLD775LiT3vqUZ4lExifmdgo0gesELEnt0fkm44_F7VYANhea8GAJO2LuyQE3BbQgWfz1rs7mulLFTt-HtjWxdTbx-DH8bUTlcw_";
        private const string PlaybackApiEndpoint = @"https://edge.api.brightcove.com/playback/v1";

        public async Task<GetVideoResponse> GetVideoInformation(string accountId, string videoId)
        {
            var url = $"{PlaybackApiEndpoint}/accounts/{accountId}/videos/{videoId}";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("ContentType", "application/json");
            request.Headers.Add("Accept", $"application/json;pk={PolicyKey}");
            var response = await HttpClient.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                    throw new FileNotFoundException($"Brightcove GetVideo API failed. Error_Code: {response.StatusCode}");
                else
                    // A result other than StatusCode:200 was returned
                    throw new Exception($"Brightcove GetVideo API failed. StatusCode: {response.StatusCode}");
            }

            // If HttpResponse Content parse successed, return parsed data.
            var json = await response.Content.ReadAsStringAsync();
            var videoInformation = new GetVideoResponse();
            if (JsonParser.TryParse<GetVideoResponse>(json, out videoInformation))
                return videoInformation;

            // If it cannot be parsed into ErrorType, unintended data is returned.
            throw new Exception($"Brightcove GetVideo API Content parse failed. Content: {json}");
        }
    }
}
