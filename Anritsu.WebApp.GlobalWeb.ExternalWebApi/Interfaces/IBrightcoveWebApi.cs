﻿using Anritsu.WebApp.GlobalWeb.ExternalWebApi.Models.Brightcove;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.ExternalWebApi.Interfaces
{
    public interface IBrightcoveWebApi
    {
        Task<GetVideoResponse> GetVideoInformation(string accountId, string videoId);
    }
}
