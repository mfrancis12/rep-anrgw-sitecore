﻿using System;

namespace Anritsu.WebApp.GlobalWeb.ExternalWebApi.Models.Brightcove
{
    public class GetVideoResponse
    {
        public bool IsRequestSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public Uri Poster { get; set; }

        public Uri Thumbnail { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public string Id { get; set; }
    }
}
