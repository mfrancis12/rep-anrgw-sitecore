﻿using Newtonsoft.Json;
using System;

namespace Anritsu.WebApp.GlobalWeb.ExternalWebApi.Utilities
{
    public static class JsonParser
    {
        /// <summary>
        /// TryPaseJson.
        /// Function to check whether Json can Parse with the type specified by <T>.
        /// If it can Parse, it returns true.
        /// </summary>
        /// <typeparam name="T">class</typeparam>
        /// <param name="json">json string</param>
        /// <param name="jsonObject">parsed object</param>
        /// <returns></returns>
        public static bool TryParse<T>(string json, out T jsonObject) where T : new()
        {
            try
            {
                jsonObject = JsonConvert.DeserializeObject<T>(json);
                return true;
            }
            catch (Exception)
            {
                jsonObject = new T();
                return false;
            }
        }
    }
}
