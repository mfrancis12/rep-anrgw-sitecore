﻿using System;
using System.Diagnostics.CodeAnalysis;
using Glass.Mapper.Pipelines.ObjectConstruction;
using Glass.Mapper.Sc;
using Sitecore.Data.Fields;
using Sitecore.SharedSource;
using Sitecore.Data.Items;
using Sitecore.SharedSource.PartialLanguageFallback.Extensions;

namespace Anritsu.WebApp.GlobalWeb.Models.App_Start
{
    public class FallbackCheckTask : IObjectConstructionTask
    {
        [SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.Compare(System.String,System.String,System.Boolean)"), SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.Compare(System.String,System.String,System.Boolean)")]
        public void Execute(ObjectConstructionArgs args)
        {
            if (args.Result == null)
            {
                var scContext = args.AbstractTypeCreationContext as SitecoreTypeCreationContext;
                if (scContext.Item == null)
                {
                    args.AbortPipeline();
                    return;
                }
                //this checks to see if the item was created by the fallback module
                //if (scContext.Item is Sitecore.Data.Managers.StubItem)
                //{

                //    return;
                //}

                // we could be trying to convert rendering parameters to a glass model, and if so, just return.
                if (String.Compare(scContext.Item.Paths.FullPath, "[orphan]/renderingParameters", true) == 0)
                {
                    return;
                }

                if (scContext.Item.Versions.Count == 0)
                {

                    var fallBackItem = CheckRecursivelyForFallbackItem(scContext.Item);

                    if (fallBackItem == null)

                        args.AbortPipeline();

                    else if (fallBackItem.Versions.Count == 0)

                        args.AbortPipeline();

                    return;

                }
            }
        }

        private Item CheckRecursivelyForFallbackItem(Item thisItem)
        {

            var fallBackItem = thisItem.GetFallbackItem();

            if (fallBackItem != null)
            {

                if (fallBackItem.Versions.Count == 0)

                    fallBackItem = CheckRecursivelyForFallbackItem(fallBackItem);

            }

            return fallBackItem;

        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "fld")]
        public static Item GetSitecoreFallbackItem(Item item, Field fld)
        {
            var checkValue = true;
            Item currentItem = item;
            try
            {

                if (item.Fields[fld.ID] != null)
                    checkValue = item.Fields[fld.ID].HasValue;

                if (checkValue)
                {
                    return item;
                }

                var fallbackItem = item.GetFallbackItem();
                if (fallbackItem != null)
                {
                    //Recursive call to get the item that has value for the particular field.
                    currentItem = GetSitecoreFallbackItem(fallbackItem, fld);
                }

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message + " " + ex.StackTrace, "ModelsFallbakHelper");
            }
            return currentItem;
        }
    }
}

