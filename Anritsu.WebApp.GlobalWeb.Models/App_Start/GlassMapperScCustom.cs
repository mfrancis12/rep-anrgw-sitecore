using Castle.Windsor;
using Glass.Mapper.Configuration;
using Glass.Mapper.Sc.CastleWindsor;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Configuration.Fluent;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Castle.MicroKernel.Registration;
using Glass.Mapper.Pipelines.ObjectConstruction;
using Glass.Mapper;
using Anritsu.WebApp.GlobalWeb.Models.FieldMappers;


namespace Anritsu.WebApp.GlobalWeb.Models.App_Start
{
    public static class GlassMapperScCustom
    {
        public static void CastleConfig(IWindsorContainer container)
        {
            var config = new Config();

            container.Register(
                 Component.For<IObjectConstructionTask>().ImplementedBy<FallbackCheckTask>().LifestyleTransient()
                //Component.For<IObjectConstructionTask>().ImplementedBy<SearchProxyWrapperTask>().LifestyleTransient(),
            );
            container.Register(Component.For<AbstractDataMapper>().ImplementedBy<LinkListDataHandler>().LifestyleTransient());
            container.Install(new SitecoreInstaller(config));
        }

        public static IConfigurationLoader[] GlassLoaders()
        {
            var attributes = new SitecoreAttributeConfigurationLoader("Glass.Mapper");

            var loader = new SitecoreFluentConfigurationLoader();
            var config = loader.Add<SearchItem>();
            config.Id(x => x.ItemId);
            config.Info(x => x.Language).InfoType(SitecoreInfoType.Language);
            config.Info(x => x.Version).InfoType(SitecoreInfoType.Version);
            config.Info(x => x.Url).InfoType(SitecoreInfoType.Url);
            config.Field(x => x.Name);
            config.Field(x => x.ShortDescription);
            config.Field(x => x.Video);
            config.Field(x => x.Thumbnail);

            //Downloads
            var downloads = loader.Add<DownloadSearchItem>();
            downloads.Id(x => x.ItemId);
            downloads.Info(x => x.Language).InfoType(SitecoreInfoType.Language);
            downloads.Info(x => x.Url).InfoType(SitecoreInfoType.Url);
            downloads.Field(x => x.TemplateId);
            downloads.Field(x => x.TemplateName);
            downloads.Field(x => x.ParentItem);
            downloads.Field(x => x.Title);
            downloads.Field(x => x.Description);
            downloads.Field(x => x.Version);
            downloads.Field(x => x.IsNewVersionAvailable);
            downloads.Field(x => x.ReplacementDownloadPath);
            downloads.Field(x => x.ReleaseDate);
            downloads.Field(x => x.Subcategory);
            downloads.Field(x => x.RelatedProducts);
            downloads.Field(x => x.OptionalLinks);
            downloads.Field(x => x.FilePath);
            downloads.Field(x => x.FileType);
            downloads.Field(x => x.Size);
            downloads.Field(x => x.LogOnRequired);
            downloads.Field(x => x.RedirectToMyAnritsu);
            downloads.Field(x => x.DownloadReleaseDate);

            //Downloads
            var frequentlyAskedQuestion = loader.Add<FrequentlyAskedQuestionSearchItem>();
            frequentlyAskedQuestion.Id(x => x.ItemId);
            frequentlyAskedQuestion.Info(x => x.Language).InfoType(SitecoreInfoType.Language);
            frequentlyAskedQuestion.Info(x => x.Url).InfoType(SitecoreInfoType.Url);
            frequentlyAskedQuestion.Field(x => x.TemplateId);
            frequentlyAskedQuestion.Field(x => x.TemplateName);
            frequentlyAskedQuestion.Field(x => x.Version);
            frequentlyAskedQuestion.Field(x => x.ParentItem);
            frequentlyAskedQuestion.Field(x => x.FaqType);
            frequentlyAskedQuestion.Field(x => x.Question);
            frequentlyAskedQuestion.Field(x => x.Answer);
            frequentlyAskedQuestion.Field(x => x.IsActive);
            frequentlyAskedQuestion.Field(x => x.RelatedProducts);


            var products = loader.Add<ProductSearchItem>();
            products.Id(x => x.ItemId);
            products.Info(x => x.Language).InfoType(SitecoreInfoType.Language);
            products.Info(x => x.Url).InfoType(SitecoreInfoType.Url);
            products.Field(x => x.TemplateId);
            products.Field(x => x.TemplateName);
            products.Field(x => x.ParentItem);
            products.Field(x => x.ChildItems);
            products.Field(x => x.SelectProduct);
            products.Field(x => x.RelatedProducts);
            products.Field(x => x.Videos);
            //products.Field(x => x.WhatSNew);
            products.Field(x => x.MenuTitle);
            products.Field(x => x.ShowInMainNavigation);

            //NewsDetail
            //var news = loader.Add<SearchNewsItem>();
            //news.Id(x => x.ItemId);
            //news.Field(x => x.Title);
            //news.Field(x => x.ShortDescription);
            //news.Field(x => x.Image);
            //news.Field(x => x.LongDescription);
            //news.Field(x => x.ReleaseDate);
            //news.Field(x => x.Link);
            //news.Field(x => x.Parent);
            //news.Field(x => x.ParentItem);
            //news.Field(x => x.TemplateId);

            //var announcements = loader.Add<SearchAnnouncementItem>();
            //announcements.Id(x => x.ItemId);
            //announcements.Field(x => x.Title);
            //announcements.Field(x => x.Text);
            //announcements.Field(x => x.ReleaseDate);
            //announcements.Field(x => x.Description);
            //announcements.Field(x => x.TemplateId);
            //announcements.Field(x => x.Parent);
            //announcements.Field(x => x.ParentItem);



            //var events = loader.Add<SearchEventItem>();
            //events.Id(x => x.ItemId);
            //events.Field(x => x.AdditionalLink);
            //events.Field(x => x.AdditionalTitle);
            //events.Field(x => x.Content);
            //events.Field(x => x.Parent);
            //events.Field(x => x.StartDate);
            //events.Field(x => x.EndDate);
            //events.Field(x => x.Title);
            //events.Field(x => x.EventBodyText);
            //events.Field(x => x.EventType);

            //events.Field(x => x.Image);
            //events.Field(x => x.ImageUrl);

            //var tradeshowEvents = loader.Add<SearchTradeshowItem>();
            //tradeshowEvents.Id(x => x.ItemId);
            //tradeshowEvents.Field(x => x.Image);
            //tradeshowEvents.Field(x => x.ImageUrl);

            //var webinarEvents = loader.Add<SearchOnlineWebinarItem>();
            //webinarEvents.Id(x => x.ItemId);
            //webinarEvents.Field(x => x.Image);
            //webinarEvents.Field(x => x.ImageUrl);

            //var tmEvents = loader.Add<SearchTMEventItem>();
            //tmEvents.Id(x => x.ItemId);
            //tmEvents.Field(x => x.RegisterOrLaunchTitle);
            //tmEvents.Field(x => x.RegisterOrLaunchLink);

            //var eventsGeneric = loader.Add<SearchEventItem>();
            //eventsGeneric.Id(x => x.ItemId);
            //eventsGeneric.Field(x => x.Title);
            //eventsGeneric.Field(x => x.StartDate);
            //eventsGeneric.Field(x => x.EndDate);
            //eventsGeneric.Field(x => x.StartTime);
            //eventsGeneric.Field(x => x.Location);
            //eventsGeneric.Field(x => x.ShortDescription);
            //eventsGeneric.Field(x => x.EventBodyText);
            //eventsGeneric.Field(x => x.AdditionalTitle);
            //eventsGeneric.Field(x => x.AdditionalLink);

            return new IConfigurationLoader[] { attributes, loader };
        }
        public static void PostLoad()
        {
            //Remove the comments to activate CodeFist
            /* CODE FIRST START
            var dbs = Sitecore.Configuration.Factory.GetDatabases();
            foreach (var db in dbs)
            {
                var provider = db.GetDataProviders().FirstOrDefault(x => x is GlassDataProvider) as GlassDataProvider;
                if (provider != null)
                {
                    using (new SecurityDisabler())
                    {
                        provider.Initialise(db);
                    }
                }
            }
             * CODE FIRST END
             */
        }
    }
}
