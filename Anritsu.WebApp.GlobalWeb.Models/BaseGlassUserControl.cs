﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using System.Linq.Expressions;
using System.Web.UI;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;

namespace Anritsu.WebApp.GlobalWeb.Models
{
    public class BaseGlassUserControl<T> : GlassUserControl<T> where T : class
    {
        //protected override void OnInit(EventArgs e)
        //{
        //    base.OnInit(e);

        //    if (Model == null)
        //    {
        //        SetErrorState();
        //        return;
        //    }

        //    // Invoke the custom item constructor
        //    var ctor = typeof(T).GetConstructor(new[] { typeof(Item) });
        //    Model = ctor.Invoke(new[] { DataSourceItem }) as T;

        //    // Verify the appropriate DataSource
        //    if (Model == null
        //        || !DataSourceItem.IsOfTemplate(Model.GetExpectedTemplateId(), 2))
        //    {
        //        SetErrorState();
        //        return;
        //    }

        //}

        //protected T Model { get; private set; }
        private bool DataSourceError { get; set; }

        protected void SetErrorState()
        {
            Visible = Sitecore.Context.PageMode.IsPageEditorEditing;
            DataSourceError = true;
        }

        protected override void Render(HtmlTextWriter writer)
        {

            if (DataSourceError)
            {
                WriteDataSourceError(writer);
            }
            else
            {
                //try
                //{
                    base.Render(writer);
                //}
                //catch (Exception ex)
                //{
                //    Visible = false;
                //    Sitecore.Diagnostics.Log.Error(ex.StackTrace, ex, this);
                //}
            }
        }

        /// <summary>
        /// Provides a simple error text block rendered in a div block element, with the
        /// style class set to "dataSourceError". Override this method to provide error
        /// HTML specific to the rendering.
        /// </summary>
        /// <param name="writer"></param>
        protected virtual void WriteDataSourceError(HtmlTextWriter writer)
        {
            writer.AddAttribute("class", "dataSourceError");
            writer.RenderBeginTag("div");
            writer.Write("The datasource for this control is not set or of the wrong type.");
            writer.RenderEndTag();
        }
    }
}
