﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Common
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Common/BaseTemplate
  /// Template ID : {5E8B8008-16FA-44C1-B8BF-DE6D7A2D8670}
  /// </summary>
  [SitecoreType(TemplateId = "{5E8B8008-16FA-44C1-B8BF-DE6D7A2D8670}")]
  public partial interface IBaseTemplate : IModelBase, IMetatag_template 
  {  

    /// <summary>
    /// Gets the Single-Line Text PageTitle. 
    /// </summary>
	[SitecoreField]
    string PageTitle { get; set; }

    [SitecoreField]
    string MetaTitle { get; set; }

  }
}
