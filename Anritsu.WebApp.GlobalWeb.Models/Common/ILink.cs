﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Common
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Common/Link
  /// Template ID : {F1569CF8-9216-4B2D-8604-429BCC4ED617}
  /// </summary>
  [SitecoreType(TemplateId = "{F1569CF8-9216-4B2D-8604-429BCC4ED617}")]
  public partial interface ILink : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the General Link Link. 
    /// </summary>
	[SitecoreField]
    Link Link { get; set; }

  }
}
