﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Common
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Common/Navigation
  /// Template ID : {2F97A15C-1D32-4FBB-A61F-ECA8BDAB2830}
  /// </summary>
  [SitecoreType(TemplateId = "{2F97A15C-1D32-4FBB-A61F-ECA8BDAB2830}")]
  public partial interface INavigation : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text MenuTitle. 
    /// </summary>
	[SitecoreField]
    string MenuTitle { get; set; }

    /// <summary>
    /// Gets the Checkbox ShowInMainNavigation. 
    /// </summary>
	[SitecoreField]
    bool ShowInMainNavigation { get; set; }

  }
}
