﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Common
{
    public partial interface IOptionalLinks : IModelBase 
    {
        /// <summary>
        /// Gets the FieldSuite OptionalLinks Field. 
        /// </summary>
        [SitecoreField]
        IList<Link> OptionalLinks { get;}
    }
}
