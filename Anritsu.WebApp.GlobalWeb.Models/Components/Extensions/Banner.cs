﻿using Anritsu.WebApp.GlobalWeb.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class Banner
    {
        public static string GetImagePath(this IBanner banner)
        {
            if (banner.BannerImage != null && !string.IsNullOrEmpty( banner.BannerImage.Src))
            {
                return banner.BannerImage.Src;
            }
            else
                return string.Empty;
        }

        public static string GetImageAltText(this IBanner banner)
        {
            return banner.BannerImage == null ? string.Empty :
            banner.BannerImage.Alt;
        }

        public static string GetTextColor(this IBanner banner)
        {
            return banner.TextColor == null ? string.Empty : banner.TextColor.Key;
        }
        
    }
}
