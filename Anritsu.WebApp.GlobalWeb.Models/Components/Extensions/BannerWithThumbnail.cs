﻿namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class BannerWithThumbnail
    {
        public static string GetTextColor(this IBannerWithThumbnail banner)
        {
            return banner.TextColor == null ? string.Empty : banner.TextColor.Value;
        }
    }
}
