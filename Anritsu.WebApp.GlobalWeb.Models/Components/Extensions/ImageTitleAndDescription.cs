﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class ImageTitleAndDescription
    {

        public static string GetTextColor(this IImageTitleAndDescription imageBlock)
        {
            return imageBlock.TextColor == null ? string.Empty : imageBlock.TextColor.Key;
        }
    }
}
