﻿namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class ImageWithTitle
    {
        public static string GetImagePath(this IImageWithTitle imageBlock)
        {
            return imageBlock.Image==null?string.Empty:
            imageBlock.Image.Src;
        }

        public static string GetImageAltText(this IImageWithTitle imageBlock)
        {
            return imageBlock.Image == null ? string.Empty :
            imageBlock.Image.Alt;
        }

    }
}
