﻿using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class LinkExtensions
    {
        /// Render a Glass Link property on it's own. (Mainly used on individual Links 
        /// in the LinkList field type).

        public static string RenderLink(this Link link)
        {
            NameValueCollection attributes = null;
            string contents = null;
            StringBuilder sb = new StringBuilder();
            using (TextWriter writer = new StringWriter(sb))
            {
                var result = GlassHtml.BeginRenderLink(link, attributes, contents, writer);
                result.Dispose();
            }
            return sb.ToString();
        }
    }
}
