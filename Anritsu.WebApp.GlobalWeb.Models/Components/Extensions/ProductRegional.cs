﻿using Anritsu.WebApp.GlobalWeb.Models.Pages;

namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class ProductRegional
    {
        public static string GetProductStatus(this IProductRegional product)
        {
            if (product != null && !product.SelectProduct.IsDiscontinued)
            {
                if (product.SelectProduct.Status != null && product.SelectProduct.Status.Image != null && !string.IsNullOrEmpty(product.SelectProduct.Status.Image.Src))
                {
                    return product.SelectProduct.Status.Image.Src;
                }
            }
            return string.Empty;
        }
    }
}
