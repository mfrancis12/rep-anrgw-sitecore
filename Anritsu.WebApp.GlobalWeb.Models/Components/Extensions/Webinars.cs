﻿using Anritsu.WebApp.GlobalWeb.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class Webinars
    {
        public static string GetCommaPrefixDuration(this IOnlineWebinars field)
        {
            return string.IsNullOrEmpty(field.Duration) ? string.Empty : " ," + field.Duration;
        }
    }
}
