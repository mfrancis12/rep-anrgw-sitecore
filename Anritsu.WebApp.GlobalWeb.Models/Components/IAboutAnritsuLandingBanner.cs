﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{    /// <summary>
    /// Code representation of item based on Sitecore template 	/sitecore/templates/Anritsu-WWW/Pages/AboutAnritsuLandingGlobal
    /// Template ID : {508A0923-FF10-4EC1-9B83-9DC5C1C1FB6C}
    /// </summary>
    [SitecoreType(TemplateId = "{067C3656-8902-4C6B-B0FD-7F2A6B976D9C}")]
    public partial interface IAboutAnritsuLandingBanner : IModelBase, INavigation, IBanner, IBaseTemplate
    {

    }
}
