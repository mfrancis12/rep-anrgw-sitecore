﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/AboutGallery
    /// Template ID : {C5405293-8111-47F9-B44C-279DBE87B3D4}
    /// </summary>
    [SitecoreType(TemplateId = "{C5405293-8111-47F9-B44C-279DBE87B3D4}")]

    public partial interface IAboutGallery: IModelBase, IStaticPage
    {
    }
}
