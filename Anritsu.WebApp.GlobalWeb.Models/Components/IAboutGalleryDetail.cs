﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/AboutGalleryDetail
    /// Template ID : {728049D8-D271-401A-A7D0-C72FD459CC8A}
    /// </summary>
    [SitecoreType(TemplateId = "{728049D8-D271-401A-A7D0-C72FD459CC8A}")]

    public partial interface IAboutGalleryDetail: IModelBase, IVideo
    {
        
    }
}
