﻿using Glass.Mapper.Sc.Configuration.Attributes;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Training/AccordionPanel
  /// Template ID : {67120DE4-2FB4-4C1B-9ADA-F7071015A1D7}
  /// </summary>
  [SitecoreType(TemplateId = "{67120DE4-2FB4-4C1B-9ADA-F7071015A1D7}")]
  public partial interface IAccordionPanel : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the Rich Text Description. 
    /// </summary>
	[SitecoreField]
    string Description { get; set; }

    /// <summary>
    /// Gets the Checkbox OpenByDefault. 
    /// </summary>
	[SitecoreField]
    bool OpenByDefault { get; set; }

  }
}
