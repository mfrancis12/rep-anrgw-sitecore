﻿using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/Address
    /// Template ID : {337A7927-78F9-4882-A22E-024ED6078A34}
    /// </summary>
    [SitecoreType(TemplateId = "{337A7927-78F9-4882-A22E-024ED6078A34}")]
     public partial interface IAddressDetails : IModelBase
    {
        /// <summary>
        /// Gets the Single-Line Text CompanyName. 
        /// </summary>
        [SitecoreField]
        string Title { get; set; }

        /// <summary>
        /// Gets the Rich Text AddressDetails. 
        /// </summary>
        [SitecoreField]
        string Details { get; set; }

        /// <summary>
        /// Gets the Multilist SelectCountry. 
        /// </summary>
        [SitecoreField]
        IEnumerable<IModelBase> SelectCountry { get; set; }
    }
}
