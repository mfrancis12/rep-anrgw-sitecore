﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/Banner
  /// Template ID : {7AA1604A-37C5-4D73-86D0-05855D3AF71D}
  /// </summary>
  [SitecoreType(TemplateId = "{7AA1604A-37C5-4D73-86D0-05855D3AF71D}")]
  public partial interface IBanner : IModelBase 
  {  

    /// <summary>
    /// Gets the Image BannerImage. 
    /// </summary>
	[SitecoreField]
    Image BannerImage { get; set; }

    /// <summary>
    /// Gets the Single-Line Text BannerTitle. 
    /// </summary>
	[SitecoreField]
    string BannerTitle { get; set; }

    /// <summary>
    /// Gets the Single-Line Text BannerSubtitle. 
    /// </summary>
	[SitecoreField]
    string BannerSubtitle { get; set; }

    /// <summary>
    /// Gets the Multi-Line Text BannerDescription. 
    /// </summary>
	[SitecoreField]
    string BannerDescription { get; set; }

    /// <summary>
    /// Gets the Droplink TextColor. Datasource: /sitecore/content/GlobalWeb/components/TextColor
    /// </summary>
    [SitecoreField]
    IDropListItem TextColor { get; set; }

  }
}
