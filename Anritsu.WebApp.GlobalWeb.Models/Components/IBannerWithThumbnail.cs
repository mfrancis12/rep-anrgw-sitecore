﻿using System;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Training/BannerWithThumbnail
  /// Template ID : {4B08F6E4-F887-4223-811E-FBD63A6E6346}
  /// </summary>
  [SitecoreType(TemplateId = "{4B08F6E4-F887-4223-811E-FBD63A6E6346}")]
  public partial interface IBannerWithThumbnail : IModelBase 
  {  

    /// <summary>
    /// Gets the Image BannerImage. Description: This image will be displayed as banner.. 
    /// </summary>
	[SitecoreField]
    Image BannerImage { get; set; }

    /// <summary>
    /// Gets the Droplink TextColor. Description: This is used for text color on the banner.. Datasource: {1DB619F9-101A-4FEA-928C-D4678048152B}
    /// </summary>
	[SitecoreField]
    IDropListItem TextColor { get; set; }

    /// <summary>
    /// Gets the Single-Line Text BannerTitle. Description: This will be displayed over the banner.. 
    /// </summary>
	[SitecoreField]
    string BannerTitle { get; set; }

    /// <summary>
    /// Gets the Rich Text BannerDescription. Description: This will be displayed as banner description. 
    /// </summary>
	[SitecoreField]
    string BannerDescription { get; set; }

    /// <summary>
    /// Gets the Image BannerThumbnail. Description: This will be displayed over banner. 
    /// </summary>
	[SitecoreField]
    Image BannerThumbnail { get; set; }

  }
}
