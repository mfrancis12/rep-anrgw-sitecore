﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/BannerWithTreeList
    /// Template ID : {508A0923-FF10-4EC1-9B83-9DC5C1C1FB6C}
    /// </summary>
    [SitecoreType(TemplateId = "{508A0923-FF10-4EC1-9B83-9DC5C1C1FB6C}")]
    public partial interface IBannerWithTreeList : IModelBase
    {
        /// <summary>
        /// Gets the the Treelist Content. Datasource: Datasource= Datasource=Datasource={E41AF5AA-ADC2-4E9B-A1B0-2B1F8EB9362E}&IncludeTemplatesForSelection=ImageBlockWithRichText&includetemplatesfordisplay=Folder,ImageBlockWithRichText
        /// </summary>
        [SitecoreField]
        IEnumerable<IImageBlockWithContent> Content { get; set; }

        /// <summary>
        /// Gets the Image BannerImage. 
        /// </summary>
        [SitecoreField]
        Image BannerImage { get; set; }

        /// <summary>
        /// Gets the Single-Line Text BannerTitle. 
        /// </summary>
        [SitecoreField]
        string BannerTitle { get; set; }

        /// <summary>
        /// Gets the Rich-Text Text BannerDescription. 
        /// </summary>
        [SitecoreField]
        string BannerDescription { get; set; }

        /// <summary>
        /// Gets the Droplink TextColor. Datasource: /sitecore/content/GlobalWeb/components/TextColor
        /// </summary>
        [SitecoreField]
        IDropListItem TextColor { get; set; }

    }
}
