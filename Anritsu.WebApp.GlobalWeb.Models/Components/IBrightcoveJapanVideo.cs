﻿using Glass.Mapper.Sc.Configuration.Attributes;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{992B8E3D-9A5A-45B9-9450-360D4DBE4A7A}")]
    public partial interface IBrightcoveJapanVideo : IModelBase
    {
        [SitecoreField]
        string AccountId { get; set; }

        [SitecoreField]
        string VideoId { get; set; }

        [SitecoreField]
        string VideoPortalUrl { get; set; }
    }
}