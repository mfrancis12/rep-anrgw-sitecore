﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/Carousel
  /// Template ID : {A0814F87-1F2E-47E7-A9D9-7662C47BE0F0}
  /// </summary>
  [SitecoreType(TemplateId = "{A0814F87-1F2E-47E7-A9D9-7662C47BE0F0}")]
  public partial interface ICarousel : IModelBase, IImageBlockWithSubtitle 
  {  

    /// <summary>
    /// Gets the Single-Line Text PanelName. 
    /// </summary>
	[SitecoreField]
    string PanelName { get; set; }

  }
}
