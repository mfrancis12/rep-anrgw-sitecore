﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{77C1A1F9-AEA4-42E2-9C13-F6BAADC81D65}")]
   public partial interface ICategory : IModelBase
    {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        string Description { get; set; }

        [SitecoreField]
        IEnumerable<IModelBase> SelectSubcategories { get; set; }
    }
}
