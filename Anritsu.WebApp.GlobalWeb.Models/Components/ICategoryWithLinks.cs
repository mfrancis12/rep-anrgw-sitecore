﻿using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{CA9C444F-F886-4C22-A663-B33763B72B33}")]
    public partial interface ICategoryWithLinks:IModelBase,IImageTitleAndDescription
    {
        [SitecoreField]
        string CategoryLinks { get; set; }
    }
}
