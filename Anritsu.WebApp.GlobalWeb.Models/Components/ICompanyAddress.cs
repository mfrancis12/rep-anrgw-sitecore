﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/CompanyAddress
    /// Template ID : {ED9A97A7-85A8-4C29-8EF0-F71B73A22784}
    /// </summary>
    [SitecoreType(TemplateId = "{ED9A97A7-85A8-4C29-8EF0-F71B73A22784}")]
    public partial interface ICompanyAddress : IModelBase
    {
        /// <summary>
        /// Gets the Single-Line Text CompanyName. 
        /// </summary>
        [SitecoreField]
        string Title { get; set; }

        /// <summary>
        /// Gets the General Link CompanyLink. 
        /// </summary>
        [SitecoreField]
        Link TitleLink { get; set; }

        /// <summary>
        /// Gets the Multi-Line Text ShortDescription. 
        /// </summary>
        [SitecoreField]
        string ShortDescription { get; set; }

        /// <summary>
        /// Gets the Rich Text AddressDetails. 
        /// </summary>
        [SitecoreField]
        string Details { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Longitude. 
        /// </summary>
        [SitecoreField]
        string Longitude { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Latitude. 
        /// </summary>
        [SitecoreField]
        string Latitude { get; set; }
    }
}
