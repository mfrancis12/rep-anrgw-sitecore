﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{FEC4A3CE-E389-4D4A-8FD5-5D608C0AE80A}")]
    public partial interface IContactBlock : IModelBase
    {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        string Description { get; set; }

        [SitecoreField]
        string LinkTitle { get; set; }

        [SitecoreField]
        Link Link { get; set; }

        [SitecoreField]
        string ContactLinks { get; set; }
    }
}
