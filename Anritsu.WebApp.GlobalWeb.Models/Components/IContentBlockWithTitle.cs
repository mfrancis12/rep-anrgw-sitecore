﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{9E1B0525-E57F-42BF-81C3-0A57E5A6CC7B}")]
    public partial interface IContentBlockWithTitle : IModelBase
    {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        string Description { get; set; }

        [SitecoreField]
        Link Link { get; set; }
    }
}
