﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ContentImageBlockWithLink
    /// Template ID : {590B26FD-0344-4C26-B620-2B32A46A52BF}
    /// </summary>
    [SitecoreType(TemplateId = "{590B26FD-0344-4C26-B620-2B32A46A52BF}")]
    public partial interface IContentImageBlockWithLink : IModelBase
    {
        /// <summary>
        /// Gets the Single-Line Text Title. 
        /// </summary>
        [SitecoreField]
        string Title { get; set; }

        /// <summary>
        /// Gets the Image Image. 
        /// </summary>
        [SitecoreField]
        Image Image { get; set; }

        /// <summary>
        /// Gets the Multi-Line Text Description. 
        /// </summary>
        [SitecoreField]
        string Description { get; set; }

        /// <summary>
        /// Gets the General Link Link. 
        /// </summary>
        [SitecoreField]
        Link Link { get; set; }
    }
}
