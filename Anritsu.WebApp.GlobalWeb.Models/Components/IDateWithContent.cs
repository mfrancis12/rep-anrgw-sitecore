﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/DateWithContent
  /// Template ID : {2F080FE3-BBDE-41A5-BD99-D86A97BEE2A8}
  /// </summary>
  [SitecoreType(TemplateId = "{2F080FE3-BBDE-41A5-BD99-D86A97BEE2A8}")]
  public partial interface IDateWithContent : IModelBase 
  {  

    /// <summary>
    /// Gets the Date ReleaseDate. 
    /// </summary>
	[SitecoreField]
    DateTime ReleaseDate { get; set; }

    /// <summary>
    /// Gets the Rich Text Description. 
    /// </summary>
	[SitecoreField]
    string Description { get; set; }

  }
}
