﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/DropListItem
  /// Template ID : {F547E7B1-B7B9-41AD-8958-4756BD7A5633}
  /// </summary>
  [SitecoreType(TemplateId = "{F547E7B1-B7B9-41AD-8958-4756BD7A5633}")]
  public partial interface IDropListItem : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Key. Shared. 
    /// </summary>
	[SitecoreField]
    string Key { get; set; }

    /// <summary>
    /// Gets the Single-Line Text Value. 
    /// </summary>
	[SitecoreField]
    string Value { get; set; }

  }
}
