﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{326B5B10-8E2B-4D81-91CA-A19622A03177}")]
    public partial interface IEcoProductDetails : IModelBase
    {
        [SitecoreField]
        string ModelName { get; set; }

        [SitecoreField]
        Link Link { get; set; }

        [SitecoreField]
        string ModelNumber { get; set; }

        [SitecoreField]
        string ShortText { get; set; }

        [SitecoreField]
        Image ProductThumbnail { get; set; }

        [SitecoreField]
        string RegisteredYear { get; set; }
    }
}
