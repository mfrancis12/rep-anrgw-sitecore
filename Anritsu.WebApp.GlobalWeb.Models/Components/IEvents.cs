﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glass.Mapper.Sc.DataMappers;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
   

   [SitecoreType(TemplateId = "{34232019-CF1D-4141-9592-8576DF511A12}")]
    public partial interface IEvents : IModelBase
    {

        [SitecoreField]
        IDropListItem EventType { get; set; }
       
       [SitecoreField]
       string Title { get; set; }

       [SitecoreField]
       DateTime StartDate { get; set; }

       [SitecoreField]
       DateTime EndDate { get; set; }
       
       [SitecoreField]
       string StartTime { get; set; }

       [SitecoreField]
       string Location { get; set; }

       [SitecoreField]
       string ShortDescription { get; set; }

       [SitecoreField]
       string EventBodyText { get; set; }

       [SitecoreField]
       string AdditionalTitle { get; set; }

       [SitecoreField]
       Link AdditionalLink { get; set; }

       [SitecoreField]
       bool ShowOnHome { get; set; }

       [SitecoreField]
       Link ExternalLandingLink { get; set; }
    }
}
