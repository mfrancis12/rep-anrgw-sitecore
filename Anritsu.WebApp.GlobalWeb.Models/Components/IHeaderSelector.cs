﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/HeaderSelector
  /// Template ID : {5E35ED8D-1643-4CD0-B011-A16E4C5040EF}
  /// </summary>
  [SitecoreType(TemplateId = "{5E35ED8D-1643-4CD0-B011-A16E4C5040EF}")]
  public partial interface IHeaderSelector : IModelBase 
  {  

    /// <summary>
    /// Gets the Checkbox ShowTagline. 
    /// </summary>
	[SitecoreField]
    bool ShowTagline { get; set; }

  }
}
