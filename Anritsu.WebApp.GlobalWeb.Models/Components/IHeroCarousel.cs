﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/HeroCarousel
    /// Template ID : {9ECF45E6-007E-4514-A4C1-FEB09B6A7082}
    /// </summary>
    [SitecoreType(TemplateId = "{9ECF45E6-007E-4514-A4C1-FEB09B6A7082}")]
    public partial interface IHeroCarousel:IModelBase
    {
        /// <summary>
        /// Gets the Treelist HeroCarousel. Datasource: Datasource={040D4679-AD23-46B3-B83F-DC156AB0BD78}
        /// </summary>
        [SitecoreField]
        IEnumerable<ICarousel> HeroCarousel { get; set; }

    }
}
