﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{2B7734E8-5BBB-4C81-AC8B-0AED370A29F5}")]
    public partial interface IHomeTicker : IModelBase
    {
        /// <summary>
        /// Gets the Single-Line Text Title. 
        /// </summary>
        [SitecoreField]
        string Title { get; set; }

        /// <summary>
        /// Gets the General Link Link. 
        /// </summary>
        [SitecoreField]
        Link Link { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Text. 
        /// </summary>
        [SitecoreField]
        string Text { get; set; }

    }
}
