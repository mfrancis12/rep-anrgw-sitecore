﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/IRNews
  /// Template ID : {B24A5B8D-564D-45AA-8A07-E82497A47904}
  /// </summary>
  [SitecoreType(TemplateId = "{B24A5B8D-564D-45AA-8A07-E82497A47904}")]
  public partial interface IIRNews : IModelBase
  {
    /// <summary>
    /// Gets the General Link Link. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

      [SitecoreField]
    DateTime ReleaseDate { get; set; }

      [SitecoreField]
    string ShortDescription { get; set; }

     [SitecoreField]
     Link Link { get; set; }

    

  }
}
