﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/IconLinkWithTitle
  /// Template ID : {5E8DF9D4-438F-4C43-9A8F-03BAA9E3DD82}
  /// </summary>
  [SitecoreType(TemplateId = "{5E8DF9D4-438F-4C43-9A8F-03BAA9E3DD82}")]
  public partial interface IIconLinkWithTitle : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the Droplink Icon. Datasource: /sitecore/content/GlobalWeb/components/Icons
    /// </summary>
	[SitecoreField]
    Guid Icon { get; set; }

    /// <summary>
    /// Gets the General Link Link. 
    /// </summary>
	[SitecoreField]
    Link Link { get; set; }

  }
}
