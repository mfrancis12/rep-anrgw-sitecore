﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ImageBlock
  /// Template ID : {3BBC764F-5A22-4AF0-9C6F-D730B5E66BA0}
  /// </summary>
  [SitecoreType(TemplateId = "{3BBC764F-5A22-4AF0-9C6F-D730B5E66BA0}")]
  public partial interface IImageBlock : IModelBase, IImageTitleAndDescription 
  {  

    /// <summary>
    /// Gets the General Link Link. 
    /// </summary>
	[SitecoreField]
    Link Link { get; set; }

  }
}
