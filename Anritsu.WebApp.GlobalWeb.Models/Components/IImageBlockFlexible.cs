﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ImageBlockFlexible
  /// Template ID : {B96026D6-D426-4D5E-8453-9EEEA8E70AF4}
  /// </summary>
  [SitecoreType(TemplateId = "{B96026D6-D426-4D5E-8453-9EEEA8E70AF4}")]
  public partial interface IImageBlockFlexible : IModelBase, IImageBlock 
  {  

    /// <summary>
    /// Gets the Droplink TextAlign. Datasource: /sitecore/content/GlobalWeb/components/TextAlign
    /// </summary>
	[SitecoreField]
    IDropListItem TextAlign { get; set; }

    /// <summary>
    /// Gets the Droplink ButtonAlign. Datasource: /sitecore/content/GlobalWeb/components/ButtonAlign
    /// </summary>
	[SitecoreField]
    IDropListItem ButtonAlign { get; set; }

  }
}
