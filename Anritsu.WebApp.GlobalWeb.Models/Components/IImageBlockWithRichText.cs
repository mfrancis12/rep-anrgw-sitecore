﻿using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;


namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ImageBlockWithRichText
    /// Template ID : {EC4239D3-43AA-4366-A6E0-9235209D23D3}
    /// </summary>
    [SitecoreType(TemplateId = "{EC4239D3-43AA-4366-A6E0-9235209D23D3}")]
    public partial interface IImageBlockWithRichText : IModelBase, IImageWithTitle, IRedirectLink
    {
        /// <summary>
        /// Gets the General Link Link. 
        /// </summary>
        [SitecoreField]
        string Description { get; set; }
    }
}
