﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ImageBlockWithSubtitle
  /// Template ID : {5B7FFB02-8448-4309-A33E-A4C69C30E531}
  /// </summary>
  [SitecoreType(TemplateId = "{5B7FFB02-8448-4309-A33E-A4C69C30E531}")]
  public partial interface IImageBlockWithSubtitle : IModelBase, IImageBlock 
  {  

    /// <summary>
    /// Gets the Single-Line Text Subtitle. 
    /// </summary>
	[SitecoreField]
    string Subtitle { get; set; }

  }
}
