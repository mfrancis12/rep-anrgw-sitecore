﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ImageTitleAndDescription
  /// Template ID : {35867DDF-AE7F-439C-A70D-00E8B76D2E0E}
  /// </summary>
  [SitecoreType(TemplateId = "{35867DDF-AE7F-439C-A70D-00E8B76D2E0E}")]
  public partial interface IImageTitleAndDescription : IModelBase, IImageWithTitle 
  {  

    /// <summary>
    /// Gets the Multi-Line Text Description. 
    /// </summary>
	[SitecoreField]
    string Description { get; set; }

    /// <summary>
    /// Gets the Droplink TextColor. Datasource: /sitecore/content/GlobalWeb/components/TextColor
    /// </summary>
    [SitecoreField]
    IDropListItem TextColor { get; set; }

  }
}
