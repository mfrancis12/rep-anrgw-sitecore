﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ImageTitleWithLink
    /// Template ID : {28BF246F-0EC8-4901-824F-F564F1D40AB3}
    /// </summary>
    [SitecoreType(TemplateId = "{28BF246F-0EC8-4901-824F-F564F1D40AB3}")]
    public partial interface IImageTitleWithLink: IModelBase
    {
        /// <summary>
        /// Gets the Image Image. 
        /// </summary>
        [SitecoreField]
        Image Image { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Title. 
        /// </summary>
        [SitecoreField]
        string Title { get; set; }

        /// <summary>
        /// Gets the General Link Link. 
        /// </summary>
        [SitecoreField]
        Link Link { get; set; }
    }
}
