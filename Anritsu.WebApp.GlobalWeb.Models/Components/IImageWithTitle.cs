﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ImageWithTitle
  /// Template ID : {BE42631B-441F-4941-A6A1-645770CE839D}
  /// </summary>
  [SitecoreType(TemplateId = "{BE42631B-441F-4941-A6A1-645770CE839D}")]
  public partial interface IImageWithTitle : IModelBase 
  {  

    /// <summary>
    /// Gets the Image Image. 
    /// </summary>
	[SitecoreField]
    Image Image { get; set; }

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

  }
}
