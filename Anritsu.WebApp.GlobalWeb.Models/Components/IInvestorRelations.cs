﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/InvestorRelations
    /// Template ID : {012DD425-2F2F-47C5-BBCC-E0A895A0DB29}
    /// </summary>
    [SitecoreType(TemplateId = "{012DD425-2F2F-47C5-BBCC-E0A895A0DB29}")]
    public partial interface IInvestorRelations:IModelBase,IBaseTemplate
    {
        /// <summary>
        /// Gets the Treelist HeroCarousel. Datasource: Datasource={2FC75C03-7759-4A2D-9ECD-0CC73531171E}
        /// </summary>
        [SitecoreField]
        IEnumerable<ICarousel> HeroCarousel { get; set; }
    }
}
