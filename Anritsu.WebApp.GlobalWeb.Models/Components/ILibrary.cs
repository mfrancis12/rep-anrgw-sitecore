﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/Download
  /// Template ID : {98432994-EB94-4E10-BEFC-A00129E96091}
  /// </summary>
  [SitecoreType(TemplateId = "{98432994-EB94-4E10-BEFC-A00129E96091}")]
  public partial interface ILibrary : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the Multi-Line Text Description. 
    /// </summary>
	[SitecoreField]
    string Description { get; set; }

    /// <remarks>
    /// There is no specific template for the field type Browse. This is a fallback field type template. 
    /// </remarks>
    /// <summary>
    /// Gets the Browse Link. 
    /// </summary>
	[SitecoreField]
    string Link { get; set; }

    /// <summary>
    /// Gets the Date ReleaseDate. 
    /// </summary>
	[SitecoreField]
    DateTime ReleaseDate { get; set; }

    /// <summary>
    /// Gets the Multilist SelectProducts. Datasource: /sitecore/content/GlobalWeb/home/products
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> SelectProducts { get; set; }

    /// <summary>
    /// Gets the Checkbox Active. 
    /// </summary>
	[SitecoreField]
    bool Active { get; set; }

  }
}
