﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{F1F3CCA8-3254-4DA1-B3CE-96CAA75AB430}")]
    public partial interface IOptionalLinks : IModelBase, INavigation
    {
        [SitecoreField]
        Glass.Mapper.Sc.Fields.Link Links { get; set; }
    }
}
