﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ProductDescriptionTab
    /// Template ID : {BD387961-DFA7-4B0F-8893-A952AC77E16F}
    /// </summary>
    [SitecoreType(TemplateId = "{BD387961-DFA7-4B0F-8893-A952AC77E16F}")]
    public partial interface IProductDescriptionTab : IModelBase, ITitleWithDescription
    {

    }
}
