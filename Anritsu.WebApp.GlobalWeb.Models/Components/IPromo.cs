﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/Promo
  /// Template ID : {35A874D1-EB0E-4DAD-9755-223FE9AB2DAF}
  /// </summary>
  [SitecoreType(TemplateId = "{35A874D1-EB0E-4DAD-9755-223FE9AB2DAF}")]
  public partial interface IPromo : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the General Link Link. 
    /// </summary>
	[SitecoreField]
    Link Link { get; set; }

    /// <summary>
    /// Gets the Image Image. 
    /// </summary>
	[SitecoreField]
    Image Image { get; set; }

  }
}
