﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/PromotionsFolder
  /// Template ID : {3910A213-BF4C-43F1-8AA7-3BACF8F9B0A9}
  /// </summary>
  [SitecoreType(TemplateId = "{3910A213-BF4C-43F1-8AA7-3BACF8F9B0A9}")]
  public partial interface IPromotionsFolder : IModelBase, IFolder 
  {  

  }
}
