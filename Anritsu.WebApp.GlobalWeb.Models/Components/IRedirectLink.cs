﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/RedirectLink
  /// Template ID : {2AD9DA10-21A3-4D7B-8DF3-CE39B10FFA31}
  /// </summary>
  [SitecoreType(TemplateId = "{2AD9DA10-21A3-4D7B-8DF3-CE39B10FFA31}")]
  public partial interface IRedirectLink : IModelBase, INavigation 
  {  

    /// <summary>
    /// Gets the General Link RedirectLink. 
    /// </summary>
	[SitecoreField]
    Link RedirectLink { get; set; }

  }
}
