﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
     [SitecoreType(TemplateId = "{FCB5C8F9-C748-46B9-920F-31439C2DE31F}")]
     public partial interface IStaticFreeformRegional:IModelBase, INavigation, IBaseTemplate
    {
        /// <summary>
        /// Gets the DropTree Content. Datasource: {A0056C28-8049-4708-B0C1-06F362FA2925}
        /// </summary>
        [SitecoreField]
        IStaticFreeformGlobal Content { get; set; }
    }
}
