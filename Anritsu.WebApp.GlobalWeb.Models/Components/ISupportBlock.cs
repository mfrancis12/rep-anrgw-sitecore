﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    [SitecoreType(TemplateId = "{6C3BDCF0-CBCF-4A11-BFE8-D3019E78B1EF}")]
    public partial interface ISupportBlock : IModelBase
    {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        string Description { get; set; }

        [SitecoreField]
        Link Link { get; set; }

        [SitecoreField]
        string OptionalLinks { get; set; }
    }
}
