﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/Sustainability
    /// Template ID : {B6DD69F9-9A20-4CA4-9401-22D9E0A2F6E8}
    /// </summary>
    [SitecoreType(TemplateId = "{B6DD69F9-9A20-4CA4-9401-22D9E0A2F6E8}")]
    public partial interface ISustainability:IModelBase,IBaseTemplate,ILink
    {
        /// <summary>
        /// Gets the Treelist HeroCarousel. Datasource: Datasource={3D5ACD03-8676-4F63-BBAD-C215A0BE3176}
        /// </summary>
        [SitecoreField]
        IEnumerable<ICarousel> HeroCarousel { get; set; }

    }
}
