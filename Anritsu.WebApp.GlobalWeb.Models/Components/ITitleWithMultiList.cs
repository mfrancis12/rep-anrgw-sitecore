﻿using System.Collections.Generic;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithMultiList
  /// Template ID : {B805C05C-1A0E-4DF3-AE2A-281E5D7AC0D1}
  /// </summary>
  [SitecoreType(TemplateId = "{B805C05C-1A0E-4DF3-AE2A-281E5D7AC0D1}")]
  public partial interface ITitleWithMultipleList : IModelBase, INavigation ,IBaseTemplate 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the Multilist Countries. Datasource: {CBE79E28-2912-4127-BE67-D469825EA027}
    /// </summary>
	[SitecoreField]
    IEnumerable<IISOCountry> Countries { get; set; }

  }
}
