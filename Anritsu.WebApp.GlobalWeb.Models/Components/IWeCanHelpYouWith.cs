﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/WeCanHelpYouWith
  /// Template ID : {BE06C00C-9971-48E8-9BE8-9C4EE2E4BC52}
  /// </summary>
  [SitecoreType(TemplateId = "{BE06C00C-9971-48E8-9BE8-9C4EE2E4BC52}")]
  public partial interface IWeCanHelpYouWith : IModelBase 
  {  

  }
}
