﻿using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.DataMappers;
using Glass.Mapper.Sc.Fields;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Anritsu.WebApp.GlobalWeb.Models.FieldMappers
{
    public class LinkListDataHandler : Glass.Mapper.Sc.DataMappers.AbstractSitecoreFieldMapper
    {
        SitecoreFieldLinkMapper _linkMapper = new SitecoreFieldLinkMapper();

        public LinkListDataHandler()
            : base(typeof(IList<Link>))
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "LinkList")]
        public override string SetFieldValue(object value, SitecoreFieldConfiguration config, SitecoreDataMappingContext context)
        {
            var links = value as IEnumerable<Link>;
            if (links == null)
            {
                return String.Empty;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("<links>");

            try
            {
                // in order to utilize the linkMapper, we need this link to be a field on an item, so we 
                // create a fake item with a fake field, and parse it that way.
                var fakeItem = Glass.Mapper.Sc.Utilities.CreateFakeItem(new HashDictionary<Guid, string>(),
                                                            ID.NewID,
                                                            context.Service.Database);
                using (new SecurityDisabler())
                {
                    var field = new Field(new ID(Guid.Empty), fakeItem);

                    fakeItem.Editing.BeginEdit();

                    foreach (var link in links)
                    {
                        _linkMapper.SetField(field, link, config, context);
                        sb.Append(field.Value);
                    }

                    fakeItem.Editing.CancelEdit();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Parsing of LinkList field failed.", ex);
            }

            sb.Append("</links>");
            return sb.ToString();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "LinkList")]
        public override object GetFieldValue(string fieldValue, SitecoreFieldConfiguration config, SitecoreDataMappingContext context)
        {
            var links = new List<Link>();
            if (String.IsNullOrWhiteSpace(fieldValue))
            {
                return links;
            }

            try
            {
                // parse the links from the xml.
                var xDocument = new XmlDocument();
                xDocument.LoadXml(fieldValue);

                var xmlLinks = xDocument.SelectNodes("/links/link");

                // in order to utilize the linkMapper, we need this link to be a field on an item, so we 
                // create a fake item with a fake field, and parse it that way.
                var fakeItem = Glass.Mapper.Sc.Utilities.CreateFakeItem(new HashDictionary<Guid, string>(),
                                                            ID.NewID,
                                                            context.Service.Database);

                using (new SecurityDisabler())
                {
                    Field field = new Field(new ID(Guid.Empty), fakeItem);
                    fakeItem.Editing.BeginEdit();

                    foreach (XmlNode xmlLink in xmlLinks)
                    {
                        field.Value = xmlLink.OuterXml;

                        var link = _linkMapper.GetField(field, config, context) as Link;
                        if (link != null)
                        {
                            if (xmlLink.Attributes["linktext"] != null)
                            {
                                if (!string.IsNullOrEmpty(xmlLink.Attributes["linktext"].Value))
                                {
                                    link.Text = xmlLink.Attributes["linktext"].Value;
                                }
                            }

                            if (xmlLink.Attributes["text"] != null)
                            {
                                if (!string.IsNullOrEmpty(xmlLink.Attributes["text"].Value))
                                {
                                    link.Text = xmlLink.Attributes["text"].Value;
                                }
                            }

                            links.Add(link);
                        }
                    }

                    fakeItem.Editing.CancelEdit();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Parsing of LinkList field failed.", ex);
            }

            return links;
        }

        public override void Setup(Glass.Mapper.Pipelines.DataMapperResolver.DataMapperResolverArgs args)
        {
            _linkMapper.Setup(args);
            base.Setup(args);
        }
    }
}
