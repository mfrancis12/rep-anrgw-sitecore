﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Global
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Global/ISOCountry
  /// Template ID : {0F8221F4-3662-43AD-B2CD-E410547EFAB5}
  /// </summary>
  [SitecoreType(TemplateId = "{0F8221F4-3662-43AD-B2CD-E410547EFAB5}")]
  public partial interface IISOCountry : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text CountryName. 
    /// </summary>
	[SitecoreField]
    string CountryName { get; set; }

    /// <summary>
    /// Gets the Single-Line Text CountryCode. Shared. 
    /// </summary>
	[SitecoreField]
    string CountryCode { get; set; }

    /// <summary>
    /// Gets the Droplink CultureGroup. Shared. Datasource: /sitecore/content/Global/CultureGroups
    /// </summary>
	[SitecoreField]
    Guid CultureGroup { get; set; }

    /// <summary>
    /// Gets the Checkbox ShowInRegionSelector. Shared. 
    /// </summary>
	[SitecoreField]
    bool ShowInRegionSelector { get; set; }

    /// <summary>
    /// Gets the Single-Line Text RegionId. Shared. 
    /// </summary>
	[SitecoreField]
    string RegionId { get; set; }

    /// <summary>
    /// Gets the Droplink GeographicRegion. Shared. Datasource: /sitecore/content/Global/geographic-regions
    /// </summary>
    [SitecoreField]
    IDropListItem GeographicRegion { get; set; }

  }
}
