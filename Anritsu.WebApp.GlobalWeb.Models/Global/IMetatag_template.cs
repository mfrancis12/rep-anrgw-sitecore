﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Global
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Global/Metatag template
  /// Template ID : {2757BDD1-D5BE-423E-8A42-DF199222A8C5}
  /// </summary>
  [SitecoreType(TemplateId = "{2757BDD1-D5BE-423E-8A42-DF199222A8C5}")]
  public partial interface IMetatag_template  
  {  

    /// <summary>
    /// Gets the lookup Metatags-Set. Title: Predefined set. Shared. Datasource: /sitecore/content/Global/Metatags/Sets
    /// </summary>
	[SitecoreField]
    Guid Metatags_Set { get; set; }

    /// <summary>
    /// Gets the Checklist Metatags-Metatags. Title: Metatags. Shared. Datasource: /sitecore/content/Global/Metatags/Metatags
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> Metatags_Metatags { get; set; }

    /// <summary>
    /// Gets the memo Metatags-Description. Title: Description. 
    /// </summary>
	[SitecoreField]
    string Metatags_Description { get; set; }

    /// <summary>
    /// Gets the Checklist Metatags-Predefined keywords. Title: Predefined keywords. Shared. Datasource: /sitecore/content/Global/Metatags/Keywords
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> Metatags_Predefined_keywords { get; set; }

    /// <summary>
    /// Gets the memo Metatags-Other keywords. Title: Other keywords. 
    /// </summary>
	[SitecoreField]
    string Metatags_Other_keywords { get; set; }

  }
}
