﻿using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models
{
    [SitecoreType]
    public interface IModelBase
    {
        [SitecoreId]
        Guid Id { get; set; }

        [SitecoreInfo(SitecoreInfoType.TemplateId)]
        Sitecore.Data.ID TemplateId { get; set; }

        [SitecoreInfo(SitecoreInfoType.TemplateName)]
        [SitecoreFieldAttribute(FieldName = "Title")]
        string TemplateName { get; set; }

        [SitecoreParent]
        IModelBase ParentItem { get; set; }

        [SitecoreChildren(IsLazy = true, InferType = true)]
        IEnumerable<IModelBase> ChildItems { get; set; }

        [SitecoreInfo(SitecoreInfoType.Url)]
        string Url { get; set; }
    }
}
