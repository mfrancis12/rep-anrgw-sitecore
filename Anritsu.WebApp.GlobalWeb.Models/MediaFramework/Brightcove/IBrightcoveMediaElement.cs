﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/MediaFramework/Brightcove/BrightcoveMediaElement
  /// Template ID : {C4EA24F3-C8BB-44CA-A224-DEF748ADF582}
  /// </summary>
  [SitecoreType(TemplateId = "{C4EA24F3-C8BB-44CA-A224-DEF748ADF582}")]
  public partial interface IBrightcoveMediaElement : IModelBase 
  {  

    /// <remarks>
    /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
    /// </remarks>
    /// <summary>
    /// Gets the ReadOnlyText ID. Title: ID. Shared. 
    /// </summary>
	[SitecoreField]
    string ID { get; set; }

    /// <summary>
    /// Gets the Single-Line Text Name. Title: Name. Shared. 
    /// </summary>
	[SitecoreField]
    string Name { get; set; }

    /// <summary>
    /// Gets the Single-Line Text ReferenceId. Title: Reference Id. Shared. 
    /// </summary>
	[SitecoreField]
    string ReferenceId { get; set; }

    /// <remarks>
    /// There is no specific template for the field type ImagePreview. This is a fallback field type template. 
    /// </remarks>
    /// <summary>
    /// Gets the ImagePreview ThumbnailURL. Title: Thumbnail URL. Shared. Datasource: Width=120
    /// </summary>
	[SitecoreField]
    string ThumbnailURL { get; set; }

    /// <summary>
    /// Gets the Single-Line Text ShortDescription. Title: Short Description. Shared. 
    /// </summary>
	[SitecoreField]
    string ShortDescription { get; set; }

    /// <summary>
    /// Gets the Multilist RelatedLinks. Datasource: /sitecore/content/GlobalWeb/home
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> RelatedLinks { get; set; }

  }
}
