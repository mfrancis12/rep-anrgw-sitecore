﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/MediaFramework/Brightcove/BrightcoveVideo
    /// Template ID : {6A5C6835-6E11-4602-A11D-B626E9255397}
    /// </summary>
    [SitecoreType(TemplateId = "{6A5C6835-6E11-4602-A11D-B626E9255397}")]
    public partial interface IBrightcoveVideo : IModelBase, IBrightcoveMediaElement
    {

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText CreationDate. Title: Creation Date. Shared. 
        /// </summary>
        [SitecoreField]
        string CreationDate { get; set; }

        /// <summary>
        /// Gets the Multi-Line Text LongDescription. Title: Long Description. Shared. 
        /// </summary>
        [SitecoreField]
        string LongDescription { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText PublishedDate. Title: Published Date. Shared. 
        /// </summary>
        [SitecoreField]
        string PublishedDate { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText LastModifiedDate. Title: Last Modified Date. Shared. 
        /// </summary>
        [SitecoreField]
        string LastModifiedDate { get; set; }

        /// <summary>
        /// Gets the Droplist Economics. Title: Economics. Shared. Datasource: {0222C45B-F1BF-47B1-A57A-8B8F224F9BEA}
        /// </summary>
        [SitecoreField]
        string Economics { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText LinkUrl. Title: Link URL. Shared. 
        /// </summary>
        [SitecoreField]
        string LinkUrl { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText LinkText. Title: Link Text. Shared. 
        /// </summary>
        [SitecoreField]
        string LinkText { get; set; }

        /// <summary>
        /// Gets the Multilist Tags. Title: Tags. Shared. Datasource: query:ancestor::*[@@templateid='{C657F8EF-4916-447F-9255-483ED2EEC562}']/Tags//*[@@templateid='{1F5CF62D-DEDC-4712-A8B6-CCC36E27A3F8}']
        /// </summary>
        [SitecoreField]
        IEnumerable<IModelBase> Tags { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ImagePreview. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ImagePreview VideoStillURL. Title: Video Still URL. Shared. Datasource: Width=480
        /// </summary>
        [SitecoreField]
        string VideoStillURL { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText Length. Title: Length. Shared. 
        /// </summary>
        [SitecoreField]
        string Length { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText PlaysTotal. Title: Plays Total. Shared. 
        /// </summary>
        [SitecoreField]
        string PlaysTotal { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText PlaysTrailingWeek. Title: Plays Trailing Week. Shared. 
        /// </summary>
        [SitecoreField]
        string PlaysTrailingWeek { get; set; }

        /// <remarks>
        /// There is no specific template for the field type Name Value List. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the Name Value List CustomFields. Title: Custom Fields. Shared. 
        /// </summary>
        [SitecoreField]
        string CustomFields { get; set; }

        /// <remarks>
        /// There is no specific template for the field type ReadOnlyText. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the ReadOnlyText Duration. Title: Duration. Shared. 
        /// </summary>
        [SitecoreField]
        string Duration { get; set; }

    }
}
