﻿using Glass.Mapper.Configuration.Attributes;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace Anritsu.WebApp.GlobalWeb.Models
{
    public static class ModelBaseExtension
    {
        public static IEnumerable<T> GetChildren<T>(this IModelBase modelBase, ISitecoreService service) where T : class
        {
            service = service ?? new SitecoreContext();
            if (modelBase==null||modelBase.ChildItems == null)
                return Enumerable.Empty<T>();
            var type = typeof(T);

            //Glass keeps an internal cache all the loaded types
            // we check to see if this is within the cache
            if (!service.GlassContext.TypeConfigurations.ContainsKey(type))
            {
                //if it isn't in the cache we manually load the type to add it to the cache
                service.GlassContext.Load(new AttributeTypeLoader(type));
            }

            //grab the config
            var config = service.GlassContext[type] as SitecoreTypeConfiguration;

            //do the mapping
            return modelBase.ChildItems
                .Where(x => config != null && x.TemplateId == config.TemplateId)
                .Select(x => service.GetItem<T>(x.Id));
        }

        public static IEnumerable<T> GetChildren<T>(this IModelBase modelBase) where T : class
        {
            return modelBase.GetChildren<T>(null);
        }

        public static IEnumerable<T> TryParseToGlass<T>(this IEnumerable<IModelBase> itemList, ISitecoreService service) where T : class
        {
            service = service ?? new SitecoreContext();
            if (itemList == null)
                return Enumerable.Empty<T>();
            var type = typeof(T);

            //Glass keeps an internal cache all the loaded types
            // we check to see if this is within the cache
            if (!service.GlassContext.TypeConfigurations.ContainsKey(type))
            {
                //if it isn't in the cache we manually load the type to add it to the cache
                service.GlassContext.Load(new AttributeTypeLoader(type));
            }

            //grab the config
            var config = service.GlassContext[type] as SitecoreTypeConfiguration;

            //do the mapping
            return itemList
                .Where(x => config != null && x.TemplateId == config.TemplateId)
                .Select(x => service.GetItem<T>(x.Id));
        }

        public static IEnumerable<T> TryParseToGlass<T>(this IEnumerable<IModelBase> itemList) where T : class
        {
            return itemList.TryParseToGlass<T>(null);
        }
    }
}
