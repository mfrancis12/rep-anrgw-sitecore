﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages.Extensions
{
    public static class ProductFamily
    {
        public static string GetTextColor(this IProductFamily family)
        {
            return family.TextColor == null ? string.Empty : family.TextColor.Key;
        }
    }
}
