﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{ 
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/AboutAnritsuLandingRegionalMap
    /// Template ID : {F160ABCC-0A99-4E92-B64F-9091906FC596}
    /// </summary>
    [SitecoreType(TemplateId = "{F160ABCC-0A99-4E92-B64F-9091906FC596}")]
    public partial interface IAboutAnritsuLanding : IModelBase
    {
        [SitecoreField]
        IAboutAnritsuLandingBanner Content { get; set; }
    }
}
