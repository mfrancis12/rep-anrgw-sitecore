﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/AboutGalleryMapping
    /// Template ID : {9F3E534A-6299-4891-BD5F-CB79044CF3F1}
    /// </summary>
    [SitecoreType(TemplateId = "{9F3E534A-6299-4891-BD5F-CB79044CF3F1}")]
    public partial interface IAboutGalleryMapping:IModelBase, INavigation
    {
        /// <summary>
        /// Gets the DropTree Content. Datasource: {A0056C28-8049-4708-B0C1-06F362FA2925}
        /// </summary>
        [SitecoreField]
        IAboutGallery Content { get; set; }
    }
}
