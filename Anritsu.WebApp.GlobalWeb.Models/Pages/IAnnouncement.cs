﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/Announcement
  /// Template ID : {B910F881-47A6-49C6-BB4F-0B5587E03CB0}
  /// </summary>
  [SitecoreType(TemplateId = "{B910F881-47A6-49C6-BB4F-0B5587E03CB0}")]
  public partial interface IAnnouncement : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the General Link Link. 
    /// </summary>
	[SitecoreField]
    Link Link { get; set; }

    /// <summary>
    /// Gets the Single-Line Text Text. 
    /// </summary>
	[SitecoreField]
    string Text { get; set; }

    [SitecoreField]
    DateTime ReleaseDate { get; set; }

    [SitecoreField]
    string Description { get; set; }


    [SitecoreField]
    bool ShowOnTicker { get; set; }

  }
}
