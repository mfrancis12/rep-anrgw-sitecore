﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
     /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ContactUsWorldwide
    /// Template ID : {72674378-4886-4A89-A2C7-2760293C3B74}
    /// </summary>
    [SitecoreType(TemplateId = "{72674378-4886-4A89-A2C7-2760293C3B74}")]
    public partial interface IAnritsuWorldwide : IModelBase
    {
        /// <summary>
        /// Gets the Single-Line Text Title. 
        /// </summary>
        [SitecoreField]
        string Title { get; set; }

        /// <summary>
        /// Gets the Multi-Line Text Description. 
        /// </summary>
        [SitecoreField]
        string Description { get; set; }

        /// <summary>
        /// Gets the Image Icon. 
        /// </summary>
        [SitecoreField]
        Image Icon { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Heading. 
        /// </summary>
        [SitecoreField]
        string Heading { get; set; }

        /// <summary>
        /// Gets the Single-Line Text ComapanyName. 
        /// </summary>
        [SitecoreField]
        string CompanyName { get; set; }

        /// <summary>
        /// Gets the Rich Text Location. 
        /// </summary>
        [SitecoreField]
        string Location { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Longitude. 
        /// </summary>
        [SitecoreField]
        string Longitude { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Latitude. 
        /// </summary>
        [SitecoreField]
        string Latitude { get; set; }

    }
}
