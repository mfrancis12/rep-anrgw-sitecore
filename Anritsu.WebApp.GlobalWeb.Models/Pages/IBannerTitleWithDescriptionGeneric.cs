﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/BannerTitleWithDescriptionGeneric
    /// Template ID : {CEDE160D-A236-45B5-B2E4-E0536DD75E39}
    /// </summary>
    [SitecoreType(TemplateId = "{CEDE160D-A236-45B5-B2E4-E0536DD75E39}")]
    public partial interface IBannerTitleWithDescriptionGeneric : IModelBase, IStaticPageWithBanner
    {

    }
}
