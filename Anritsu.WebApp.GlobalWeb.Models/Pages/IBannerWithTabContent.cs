﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;


namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/BannerWithTabContent
    /// Template ID : {9016423A-6D0F-459B-A6F3-09FF8CA526C2}
    /// </summary>
    [SitecoreType(TemplateId = "{9016423A-6D0F-459B-A6F3-09FF8CA526C2}")]
    public partial interface IBannerWithTabContent:IModelBase,INavigation,IBaseTemplate,IBanner,ILink
    {	
    }
}
