﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/BannerWithTreeListRegional
    /// Template ID : {82F213E7-584C-40A9-866B-8200619A0E47}
    /// </summary>
    [SitecoreType(TemplateId = "{82F213E7-584C-40A9-866B-8200619A0E47}")]
    public partial interface IBannerWithTreeListRegional: IModelBase, IBannerWithTreeList, INavigation, IBaseTemplate
    {
    }
}
