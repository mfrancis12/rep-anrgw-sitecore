﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{4D29DAAD-D6F3-4B50-B1CF-B279B47426CB}")]
   public partial interface IComponentsAccessoriesFamily : IModelBase , INavigation
    {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        Image BannerImage { get; set; }

        [SitecoreField]
        string Description { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        string OptionalLinks { get; set; }

        [SitecoreField]
        string RelatedLinks { get; set; }

        [SitecoreField]
        Image Thumbnail { get; set; }

        [SitecoreField]
        IEnumerable<IProductRegional> Products { get; set; }
    }
}
