﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ContactSupport
    /// Template ID : {6B159384-3FEE-475E-BEBF-3D09C9EFC7AD}
    /// </summary>
    [SitecoreType(TemplateId = "{6B159384-3FEE-475E-BEBF-3D09C9EFC7AD}")]
    public partial interface IContactSupport : IModelBase, INavigation, IBaseTemplate 
    {

        /// <summary>
        /// Gets the Rich Text WhatCanWeHelpYouWith. 
        /// </summary>
        [SitecoreField]
        string WhatCanWeHelpYouWith { get; set; }

        // <summary>
        /// Gets the General Link Link. 
        /// </summary>
        [SitecoreField]
        Link Link { get; set; }
    }
}
