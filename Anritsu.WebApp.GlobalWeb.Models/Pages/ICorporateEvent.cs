﻿using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{571B902F-D914-4961-9F9C-C0DE0118D389}")]
    public partial interface ICorporateEvent : IModelBase, IEvents
    {
        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        Link ImageUrl { get; set; }
    }
}
