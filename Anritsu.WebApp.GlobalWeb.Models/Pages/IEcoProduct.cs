﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
     [SitecoreType(TemplateId = "{7371DADA-06BB-49AC-A05C-0E0F539DD40B}")]
    public partial interface IEcoProduct : IModelBase, INavigation
    {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        Image BannerImage { get; set; }

        [SitecoreField]
        string Description { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        string Links { get; set; }

        
    }
}
