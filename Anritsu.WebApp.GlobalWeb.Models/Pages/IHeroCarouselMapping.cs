﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/HeroCarouselMapping
    /// Template ID : {E7977A74-A46C-4D94-AE71-9CB8A0CEAE7F}
    /// </summary>
    [SitecoreType(TemplateId = "{E7977A74-A46C-4D94-AE71-9CB8A0CEAE7F}")]
    public partial interface IHeroCarouselMapping: IModelBase,INavigation, IBaseTemplate, ILink
    {
        /// <summary>
        /// Gets the DropTree Content. Datasource: {A0056C28-8049-4708-B0C1-06F362FA2925}
        /// </summary>
        [SitecoreField]
        IHeroCarousel Content { get; set; }
    }
}
