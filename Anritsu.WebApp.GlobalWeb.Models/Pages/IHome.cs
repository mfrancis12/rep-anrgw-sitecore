﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Home
  /// Template ID : {9DE07373-5346-44ED-953A-E1E093EC5C17}
  /// </summary>
  [SitecoreType(TemplateId = "{9DE07373-5346-44ED-953A-E1E093EC5C17}")]
  public partial interface IHome : IModelBase, INavigation ,IBaseTemplate 
  {  

    /// <summary>
    /// Gets the Multilist HeroCarousel. Datasource: /sitecore/content/GlobalWeb/components/Carousel
    /// </summary>
	[SitecoreField]
    IEnumerable<IImageBlock> HeroCarousel { get; set; }

    
    /// <summary>
    /// Gets the Multilist Promos. Datasource: /sitecore/content/GlobalWeb/components/promotions
    /// </summary>
	[SitecoreField]
    IEnumerable<IPromo> Highlights { get; set; }

        [SitecoreField]
        IEnumerable<IHomeTicker> HomeTicker { get; set; }

    }
}
