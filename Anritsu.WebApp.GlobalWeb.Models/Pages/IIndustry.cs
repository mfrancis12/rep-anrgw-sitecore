﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{3A176CC8-B9D7-412B-8B40-F6D5FDD49CC0}")]
    public partial interface IIndustry : IModelBase,INavigation
    {
        [SitecoreField]
        Image BannerImage { get; set; }
       

        [SitecoreField]
        string BannerTitle { get; set; }


        [SitecoreField]
        string BannerDescription { get; set; }


        [SitecoreField]
        string IndustryName { get; set; }


        [SitecoreField]
        IEnumerable<IProductRegional> RelatedProducts { get; set; }


        [SitecoreField]
        Image Image { get; set; }


        [SitecoreField]
        string OptionalLinks { get; set; }

    }
}
