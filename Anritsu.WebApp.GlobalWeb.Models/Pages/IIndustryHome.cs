﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{8C6BC051-B964-49B3-8320-CE64003AC347}")]
    public partial interface IIndustryHome : IModelBase, IStaticPageWithBanner
    {
        [SitecoreField]
        IEnumerable<ICategory> SelectCategories { get; set; }
    }
}
