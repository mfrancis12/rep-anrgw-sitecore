﻿using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{FA075DCB-EBC8-4DFB-B319-75B83B51FD95}")]
    public partial interface IIndustrySubcategory : IModelBase,IRedirectLink,IImageTitleAndDescription
    {
        [SitecoreField]
        IEnumerable<IIndustry> Industries { get; set; }
    }
}
