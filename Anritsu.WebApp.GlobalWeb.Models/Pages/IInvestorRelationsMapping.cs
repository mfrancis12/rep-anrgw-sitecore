﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/InvestorRelationsMapping
    /// Template ID : {1CF53673-D160-4E9E-9EF2-435A89CC0336}
    /// </summary>
    [SitecoreType(TemplateId = "{1CF53673-D160-4E9E-9EF2-435A89CC0336}")]
    public partial interface IInvestorRelationsMapping:IModelBase,INavigation
    {
        /// <summary>
        /// Gets the DropTree Content. Datasource: {A0056C28-8049-4708-B0C1-06F362FA2925}
        /// </summary>
        [SitecoreField]
        IInvestorRelations Content { get; set; }
    }
}
