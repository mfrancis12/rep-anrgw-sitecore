﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/NewsRelease
    /// Template ID : {57A2D7A3-08D0-4A3B-971E-E22BB1D3F00C}
    /// </summary>
    [SitecoreType(TemplateId = "{57A2D7A3-08D0-4A3B-971E-E22BB1D3F00C}")]
    public partial interface INewsRelease : IModelBase
    {

        /// <summary>
        /// Gets the Single-Line Text Title. 
        /// </summary>
        [SitecoreField]
        string Title { get; set; }

        /// <summary>
        /// Gets the Date StartDate. 
        /// </summary>
        [SitecoreField]
        DateTime ReleaseDate { get; set; }

        [SitecoreField]
        Link Link { get; set; }

        [SitecoreField]
        string ShortDescription { get; set; }

        [SitecoreField]
        string LongDescription { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        bool ShowOnHome { get; set; }

        [SitecoreField]
        string LinksHeading { get; set; }

        [SitecoreField]
        string MakeImageResponsive { get; set; }
    }

}
