﻿using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{4BE1B2CA-2BD0-43C0-B6B5-6892CB9E5F79}")]
    public partial interface IOnlineWebinars : IModelBase, IEvents
    {
        [SitecoreField]
        string Duration { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        Link ImageUrl { get; set; }

        [SitecoreField]
        IDropListItem RegisterOrLaunchTitle { get; set; }

        [SitecoreField]
        Link RegisterOrLaunchLink { get; set; }
    }
}
