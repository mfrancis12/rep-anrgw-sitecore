﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ProductCategory
  /// Template ID : {8A045316-969A-4DAE-95BB-846CA22484F9}
  /// </summary>
  [SitecoreType(TemplateId = "{8A045316-969A-4DAE-95BB-846CA22484F9}")]
  public partial interface IProductCategory : IModelBase, INavigation ,ILink ,IBaseTemplate 
  {  

    /// <summary>
    /// Gets the Treelist HeroCarousel. Datasource: Datasource={A9EEAA97-816C-4B51-B4AF-613B37664FCE}
    /// </summary>
	[SitecoreField]
    IEnumerable<ICarousel> HeroCarousel { get; set; }
    [SitecoreField]
    IEnumerable<IProductRegional> NewProducts { get; set; }
    [SitecoreField]
    IEnumerable<IHomeTicker> HomeTicker { get; set; }
    [SitecoreField]
     IEnumerable<IProductSubcategory> ProductSubcategory { get; set; }
    }
}
