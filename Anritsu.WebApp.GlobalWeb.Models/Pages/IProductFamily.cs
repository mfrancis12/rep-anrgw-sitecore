﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ProductFamily
  /// Template ID : {0AC342A9-F49D-4B77-8032-5A2EF0A543ED}
  /// </summary>
  [SitecoreType(TemplateId = "{0AC342A9-F49D-4B77-8032-5A2EF0A543ED}")]
  public partial interface IProductFamily : IModelBase, INavigation ,IBaseTemplate 
  {  

    /// <summary>
    /// Gets the Image Image. 
    /// </summary>
	[SitecoreField]
    Image Image { get; set; }

    /// <summary>
    /// Gets the Rich Text Description. 
    /// </summary>
	[SitecoreField]
    string Description { get; set; }

    /// <summary>
    /// Gets the Multilist Specifications. Datasource: /sitecore/content/GlobalWeb/components/Specifications
    /// </summary>
	[SitecoreField]
    IEnumerable<IDropListItem> Specifications { get; set; }

    [SitecoreField]
    IEnumerable<IProductRegional> Products { get; set; }

    [SitecoreField]
    bool ShowReleaseDate { get; set; }

    /// <summary>
    /// Gets the Droplink TextColor. Datasource: /sitecore/content/GlobalWeb/components/TextColor
    /// </summary>
    [SitecoreField]
    IDropListItem TextColor { get; set; }
  }
}
