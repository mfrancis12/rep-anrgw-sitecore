﻿using System.Collections.Generic;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Glass.Mapper.Sc.Configuration.Attributes;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ProductRegional
  /// Template ID : {3FA9B0BB-E7FB-4DA3-9835-8EF8ED8BD652}
  /// </summary>
  [SitecoreType(TemplateId = "{3FA9B0BB-E7FB-4DA3-9835-8EF8ED8BD652}")]
    public partial interface IProductRegional : IModelBase, INavigation
  {  

    /// <summary>
    /// Gets the Droplink SelectProduct. Datasource: /sitecore/content/Global/Products
    /// </summary>
	[SitecoreField]
    IProductGlobal SelectProduct { get; set; }

    /// <summary>
    /// Gets the Multilist RelatedProducts. Datasource: /sitecore/content/GlobalWeb/home/products
    /// </summary>
    [SitecoreField]
    IEnumerable<IProductRegional> RelatedProducts { get; set; }

    /// <summary>
    /// Gets the Treelist Videos. Datasource: Datasource=/sitecore/media library/Media Framework/Accounts/AnritsuBrightcoveAccount/Media Content&includetemplatesforselection=BrightcoveVideo
    /// </summary>
    [SitecoreField]
    IEnumerable<IBrightcoveVideo> Videos { get; set; }

    /// <summary>
    /// Gets the Checkbox WhatSNew. 
    /// </summary>
    [SitecoreField]
    bool WhatSNew { get; set; }


        /// <summary>
        /// Gets the Treelist Brightcove Japan Videos. DataSource: Datasource={5FBFA1BA-5957-48D0-9539-518EEE6B2B7E}&includetemplatesforselection=BrightcoveJapanVideo&includetemplatesfordisplay=BrightcoveJapanVideo,Folder
        /// </summary>
        [SitecoreField]
        IEnumerable<IBrightcoveJapanVideo> JapanVideoPortals { get; set; }
    }
}
