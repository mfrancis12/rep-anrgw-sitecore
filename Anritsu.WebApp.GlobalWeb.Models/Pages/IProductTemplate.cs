﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ProductTemplate
  /// Template ID : {03D9986F-E552-4432-9A94-A7E97D386B7A}
  /// </summary>
  [SitecoreType(TemplateId = "{03D9986F-E552-4432-9A94-A7E97D386B7A}")]
  public partial interface IProductTemplate : IModelBase, IBaseTemplate 
  {  

  }
}
