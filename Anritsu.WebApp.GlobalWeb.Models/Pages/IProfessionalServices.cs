﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{9C3E61C2-7B90-4CEC-B199-C15F5FD5DC0B}")]
   public partial interface IProfessionalServices : IModelBase,INavigation,IBanner
    {
        [SitecoreField]
        IEnumerable<IContentBlockWithTitle> Services { get; set; }
    }
}
