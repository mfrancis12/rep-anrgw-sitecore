﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glass.Mapper.Sc.Fields;


namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/RepairCalibrationPricing
    /// Template ID : {AB86861A-6030-46C5-B394-E8F99E8B87DB}
    /// </summary>
    [SitecoreType(TemplateId = "{AB86861A-6030-46C5-B394-E8F99E8B87DB}")]
    public partial interface IRepairCalibrationPricing:IModelBase, INavigation, IBaseTemplate
    {
        [SitecoreField]
        string ShortDescription { get; set; }

        [SitecoreField]
        string LongDescription { get; set; }

        [SitecoreField]
        Image Image { get; set; }    

        [SitecoreField]
        string OptionalLinks { get; set; }
    }
}
