﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ServiceAssuranceHome
    /// Template ID : {9D52798D-911B-4F09-A313-40D9A7FF6D96}
    /// </summary>
    [SitecoreType(TemplateId = "{9D52798D-911B-4F09-A313-40D9A7FF6D96}")]
    public partial interface IServiceAssuranceHome : IModelBase, IBaseTemplate, INavigation
    {
        /// <summary>
        /// Gets the Treelist HeroCarousel. Datasource: Datasource={175867E2-FD4F-4750-AF59-B20799FA47E3}
        /// </summary>
        [SitecoreField]
        IEnumerable<ICarousel> HeroCarousel { get; set; }

        /// <summary>
        /// Gets the Treelist Announcements. Datasource: Datasource={A9FCA097-6439-4142-BECA-7C835AF85600}&IncludeTemplatesForSelection=Announcement&includetemplatesfordisplay=Folder,Announcement
        /// </summary>
        [SitecoreField]
        IEnumerable<IAnnouncement> Announcements { get; set; }

        /// <summary>
        /// Gets the Multilist RelatedProducts. Datasource: Datasource={94F7DD0F-F3CC-44A0-9B83-961DF255B1FC}
        /// </summary>
        [SitecoreField]
        IEnumerable<IContentImageBlockWithLink> RelatedProducts { get; set; }
    }
}
