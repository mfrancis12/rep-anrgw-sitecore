﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Anritsu.WebApp.GlobalWeb.Models.Pages

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{A1A3B315-9F36-40D7-8B18-DF27D3F616B4}")]

    public partial interface ISiteMap : IModelBase, INavigation, IBaseTemplate 
    {
        [SitecoreField]
       // NameValueLookup SiteMapLinks { get; set; }
       // IEnumerable<IModelBase> SiteMapLinks { get; set; }
        NameValueCollection SiteMapLinks { get; }
    }
}
