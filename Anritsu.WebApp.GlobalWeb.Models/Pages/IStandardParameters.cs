﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{5D905E78-E585-4DA3-A17A-B9449C4D3028}")]
    public partial interface IStandardParameters : IModelBase, INavigation, ILink, IBaseTemplate
    {
        [SitecoreField]
        bool UseDomainUrl { get; set; }
    }
}
