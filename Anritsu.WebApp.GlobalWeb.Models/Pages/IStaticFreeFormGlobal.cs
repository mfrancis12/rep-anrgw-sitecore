﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{D76CB34E-309D-4125-B0D0-CDEED557E49F}")]
  public partial interface IStaticFreeformGlobal : IModelBase
    {
        [SitecoreField]
        string Content { get; set; }

        [SitecoreField]
        string OptionalLinks { get; set; }

        [SitecoreField]
        Image Image { get; set; }

    }
}
