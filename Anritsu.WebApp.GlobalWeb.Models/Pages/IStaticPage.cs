﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/StaticPage
  /// Template ID : {65AD7682-3397-4E76-8643-440B9DD29648}
  /// </summary>
  [SitecoreType(TemplateId = "{65AD7682-3397-4E76-8643-440B9DD29648}")]
  public partial interface IStaticPage : IModelBase, INavigation ,IBaseTemplate 
  {  

  }
}
