﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/StaticPageWithBanner
  /// Template ID : {5B938778-BED7-44E9-9699-BDE8E500F267}
  /// </summary>
  [SitecoreType(TemplateId = "{5B938778-BED7-44E9-9699-BDE8E500F267}")]
  public partial interface IStaticPageWithBanner : IModelBase, INavigation ,IBanner ,IBaseTemplate 
  {  


  }
}
