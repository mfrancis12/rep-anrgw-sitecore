﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
     [SitecoreType(TemplateId = "{333352E0-8519-4AD9-AF47-6A697EF5132A}")]
    public partial interface IStaticPageWithBannerAndThumbnail : IModelBase, INavigation
    {
         [SitecoreField]
         Image BannerImage { get; set; }


         [SitecoreField]
         string BannerTitle { get; set; }

         
         [SitecoreField]
         string BannerDescription { get; set; }

         [SitecoreField]
         Image BannerThumbnail { get; set; }

         

         [SitecoreField]
         IDropListItem TextColor { get; set; }

    }
}
