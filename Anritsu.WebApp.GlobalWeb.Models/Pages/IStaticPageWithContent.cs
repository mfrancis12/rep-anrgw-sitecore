﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/StaticPageWithBanner
    /// Template ID : {0541CF27-2E2E-4662-922A-99CC010025FB}
    /// </summary>
    [SitecoreType(TemplateId = "{0541CF27-2E2E-4662-922A-99CC010025FB}")]
    public partial interface IStaticPageWithContent : IModelBase, INavigation
    {
        /// <remarks>
        /// There is no specific template for the field type Field Suite General Links. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the Field Suite General Links RelatedLinks. 
        /// </summary>
        [SitecoreField]
        string OptionalLinks { get; set; }

        /// <summary>
        /// Gets the html text from Rich Text
        /// </summary>
        [SitecoreField]
        string Content { get; set; }

        /// <summary>
        /// Gets the Image Image. 
        /// </summary>
        [SitecoreField]
        Image Image { get; set; }
    }
}
