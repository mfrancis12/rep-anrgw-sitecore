﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/StaticPageWithBanner
    /// Template ID : {94C9AFA0-F807-4D9C-9255-17EFA49E1527}
    /// </summary>
    [SitecoreType(TemplateId = "{94C9AFA0-F807-4D9C-9255-17EFA49E1527}")]
    public partial interface IStaticPageWithContentRegional : IModelBase, INavigation, IBaseTemplate
    {
        [SitecoreField]
        string Content { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        string OptionalLinks { get; set; }

    }
}
