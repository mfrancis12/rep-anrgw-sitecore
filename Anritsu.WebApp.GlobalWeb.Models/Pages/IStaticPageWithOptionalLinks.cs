﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{C2E3AC3A-D26B-4567-BC57-C5C47FE883B2}")]
    public partial interface IStaticPageWithOptionalLinks :IModelBase,INavigation
    {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        Image BannerImage { get; set; }

        [SitecoreField]
        string Description { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        string Links { get; set; }

        [SitecoreField]
        IEnumerable<IProductRegional> RelatedProducts { get; set; }
    }
}
