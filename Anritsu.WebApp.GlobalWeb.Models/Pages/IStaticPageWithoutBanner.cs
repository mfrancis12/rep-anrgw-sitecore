﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{46B1C488-3554-42C2-97A6-924BBC52CC38}")]
   public partial interface IStaticPageWithoutBanner : IModelBase, ITitleWithDescription , INavigation
    {
    }
}
