﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
     [SitecoreType(TemplateId = "{141A5B60-DFCB-488D-AD90-20BBEF380EA2}")]
   public partial interface IStaticText : IModelBase, IBaseTemplate
    {
         [SitecoreField]
         string Content { get; set; }

         [SitecoreField]
         IStaticPageForFreeform ContentMap { get; set; }

    }
}
