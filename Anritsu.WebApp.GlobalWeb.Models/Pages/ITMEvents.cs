﻿using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{

    [SitecoreType(TemplateId = "{508AE037-8CB8-4DA2-B425-0095E738395E}")]
    public partial interface ITMEvents : IModelBase, IEvents
    {
        [SitecoreField]
        IDropListItem RegisterOrLaunchTitle { get; set; }

        [SitecoreField]
        Link RegisterOrLaunchLink { get; set; }
    }
}
