﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
     [SitecoreType(TemplateId = "{D3E0E85D-956B-44D1-AB60-2BA1CDDA8148}")]
    public partial interface ITechnology : IModelBase,INavigation
    {
         [SitecoreField]
         Image BannerImage { get; set; }

         [SitecoreField]
         string Title { get; set; }

         [SitecoreField]
         string LongDescription { get; set; }

         [SitecoreField]
         string TechnologyName { get; set; }

         [SitecoreField]
         IEnumerable<IProductRegional> RelatedProducts { get; set; }

         [SitecoreField]
         Image Image { get; set; }

         [SitecoreField]
         string OptionalLinks { get; set; }

         [SitecoreField]
         string ShortDescription { get; set; }
    }
}
