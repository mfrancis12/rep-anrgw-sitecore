﻿using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    [SitecoreType(TemplateId = "{4A812CAD-1DF0-456E-B1DC-9C4BF8189352}")]
    public partial interface ITradeshows : IModelBase, IEvents
    {
        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        Link ImageUrl { get; set; }

       
    }
}
