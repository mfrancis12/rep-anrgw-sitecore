﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
     [SitecoreType(TemplateId = "{97B57224-BDE2-4210-9644-8CF822BFE2BD}")]
    public partial interface IWebinarVideo : IModelBase, INavigation,IBaseTemplate
    {
        [SitecoreField]
        string Video { get; set; }
    }
}
