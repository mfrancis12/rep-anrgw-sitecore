﻿using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    public class ProductSearchItem : SearchResultItem,IProductRegional
    {
        public Guid Id { get; set; }


        public Sitecore.Data.ID TemplateId { get; set; }


        public string TemplateName { get; set; }


        public IModelBase ParentItem { get; set; }


        public IEnumerable<IModelBase> ChildItems { get; set; }


        public string Url { get; set; }


        public IProductGlobal SelectProduct { get; set; }


        public IEnumerable<IProductRegional> RelatedProducts { get; set; }


        public IEnumerable<IBrightcoveVideo> Videos { get; set; }


        //public bool WhatSNew { get; set; }


        public string MenuTitle { get; set; }


        public bool ShowInMainNavigation { get; set; }

        [IndexField("ModelNumber")]
        public string ModelNumber { get; set; }

        [IndexField("IsDiscontinued")]
        public bool IsDiscontinued { get; set; }

        [IndexField("WhatSNew")]
        public bool WhatSNew { get; set; }

        public IEnumerable<IBrightcoveJapanVideo> JapanVideoPortals { get; set; }
    }
}
