﻿using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
   
    public class SearchItem : SearchResultItem, IVideo
    {

        public Guid Id { get; set; }


        public Sitecore.Data.ID TemplateId { get; set; }


        public string TemplateName { get; set; }


        public IModelBase ParentItem { get; set; }


        public IEnumerable<IModelBase> ChildItems { get; set; }


        public string Url { get; set; }

        public string Name { get; set; }

        public string ShortDescription { get; set; }

        public string LongDescription { get; set; }

        
        public string Video { get; set; }

        public Boolean LoginRequired { get; set; }

        public string RelatedLinks { get; set; }

       
        public IEnumerable<IVideo> RelatedVideos { get; set; }

        public Image Thumbnail { get; set; }

        public IBrightcoveVideo VideoItem
        {
            get
            {
                if (Video != null)
                {
                    var contextService = new SitecoreContext();
                    return contextService.GetItem<IBrightcoveVideo>(Video, LanguageManager.GetLanguage("en"));
                }
                return null;
            }

        }

        [IndexField("RelatedFamilies")]
        public IEnumerable<string> RelatedFamilies { get; set; }

        [IndexField("RelatedSubcategories")]
        public IEnumerable<string> RelatedSubcategories { get; set; }

        [IndexField("RelatedCategories")]
        public IEnumerable<string> RelatedCategories { get; set; }


    }
}
