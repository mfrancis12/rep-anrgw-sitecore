﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages.Training
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Training/TrainingCategory
  /// Template ID : {F4065695-2FB2-44CF-9151-70B4ABCC2E72}
  /// </summary>
  [SitecoreType(TemplateId = "{F4065695-2FB2-44CF-9151-70B4ABCC2E72}")]
  public interface ITrainingCategory : IModelBase, INavigation ,IBannerWithThumbnail 
  {  

  }
}
