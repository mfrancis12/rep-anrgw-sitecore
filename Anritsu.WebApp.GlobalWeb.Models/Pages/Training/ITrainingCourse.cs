﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages.Training
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Training/TrainingCourse
  /// Template ID : {F2C578CF-CA8E-44B0-970A-45F446775F81}
  /// </summary>
  [SitecoreType(TemplateId = "{F2C578CF-CA8E-44B0-970A-45F446775F81}")]
  public partial interface ITrainingCourse : IModelBase, INavigation 
  {  

    /// <summary>
    /// Gets the Single-Line Text CourseName. 
    /// </summary>
	[SitecoreField]
    string CourseName { get; set; }

    /// <summary>
    /// Gets the Multi-Line Text CourseShortDescription. 
    /// </summary>
	[SitecoreField]
    string CourseShortDescription { get; set; }

    /// <summary>
    /// Gets the Rich Text CourseDescription. 
    /// </summary>
	[SitecoreField]
    string CourseDescription { get; set; }

    /// <summary>
    /// Gets the Image CourseImage. 
    /// </summary>
	[SitecoreField]
    Image CourseImage { get; set; }

    /// <remarks>
    /// There is no specific template for the field type Field Suite Multilist. This is a fallback field type template. 
    /// </remarks>
    /// <summary>
    /// Gets the Field Suite Multilist AccordionPanels. 
    /// </summary>
	[SitecoreField]
    string AccordionPanels { get; set; }

    /// <remarks>
    /// There is no specific template for the field type Field Suite General Links. This is a fallback field type template. 
    /// </remarks>
    /// <summary>
    /// Gets the Field Suite General Links OptionalLinks. 
    /// </summary>
	[SitecoreField]
    string OptionalLinks { get; set; }

  }
}
