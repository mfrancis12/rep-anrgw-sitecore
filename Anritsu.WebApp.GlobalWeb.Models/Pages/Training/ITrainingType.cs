﻿using System.Collections.Generic;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages.Training
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Training/TrainingType
  /// Template ID : {B7064B8D-ED21-4477-BD0B-C037B5BFC971}
  /// </summary>
  [SitecoreType(TemplateId = "{B7064B8D-ED21-4477-BD0B-C037B5BFC971}")]
  public interface ITrainingType : IModelBase, IImageTitleAndDescription ,IRedirectLink
  {
      [SitecoreChildren(IsLazy = true, InferType = true)]
      IEnumerable<INavigation> Links { get; set; }

  }
}
