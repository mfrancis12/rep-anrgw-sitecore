﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anritsu.WebApp.GlobalWeb.UnitTest
{
    [TestClass]
    public class S3UtilityTest
    {
        [TestMethod]
        public void UploadFileTest()
        {
            SitecoreUtilities.Helpers.AmazonHelper.S3Utility.UploadFile("media-anritsuaws.com", "products/test.gif", "E:/Projects/Anritsu/CvsTrunk/WebApp/GlobalWeb/images/digitizing.gif");
        }
        [TestMethod]
        public void DeleteFileTest()
        {
            SitecoreUtilities.Helpers.AmazonHelper.S3Utility.DeleteFile("media-anritsuaws.com", "products/test.gif");
        }
    }
}
