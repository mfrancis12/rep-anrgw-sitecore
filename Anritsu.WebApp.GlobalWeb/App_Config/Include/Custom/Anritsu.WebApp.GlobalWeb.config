﻿<configuration xmlns:patch="http://www.sitecore.net/xmlconfig/">
  <sitecore>
    <commands>
      <command name="custom:applyversion" type="Anritsu.WebApp.SitecoreUtilities.Version.AddVersion, Anritsu.WebApp.SitecoreUtilities" />
      <command name="custom:exportapp" type="Anritsu.WebApp.SitecoreUtilities.Translations.ExportStringsApp,Anritsu.WebApp.SitecoreUtilities"/>
      <command name="custom:importapp" type="Anritsu.WebApp.SitecoreUtilities.Translations.ImportStringsApp,Anritsu.WebApp.SitecoreUtilities"/>
      <command name="custom:browseurl" type="Anritsu.WebApp.SitecoreUtilities.BrowseUrl.GetSelectedItemUrl, Anritsu.WebApp.SitecoreUtilities" />
    </commands>
    <mediaFramework>
      <playerMarkupGenerators>
        <add name="brightcove_video">
          <scriptUrl>https://players.brightcove.net/665003363001/BJveFUPkW_default/index.min.js</scriptUrl>
          <parameters>
            <param patch:after="param[@name=*]" name="secureConnections" value="true" />
            <param patch:after="param[@name=*]" name="secureHTMLConnections" value="true" />
          </parameters>
        </add>
        <add name="brightcove_playlist">
          <scriptUrl>https://players.brightcove.net/665003363001/BJveFUPkW_default/index.min.js</scriptUrl>
          <parameters>
            <param patch:after="param[@name=*]" name="secureConnections" value="true" />
            <param patch:after="param[@name=*]" name="secureHTMLConnections" value="true" />
          </parameters>
        </add>
      </playerMarkupGenerators>
    </mediaFramework>
    <pipelines>
      <httpRequestBegin>
        <processor type="Anritsu.WebApp.SitecoreUtilities.Pipelines.HttpRequestBegin.TransferRoutedRequest, Anritsu.WebApp.SitecoreUtilities" patch:after="processor[@type='Sitecore.Pipelines.HttpRequest.ItemResolver, Sitecore.Kernel']" />
        <processor type="Anritsu.WebApp.SitecoreUtilities.Pipelines.HttpRequestBegin.DisableVersionCountForGlass, Anritsu.WebApp.SitecoreUtilities" patch:after="processor[@type='Sitecore.Pipelines.HttpRequest.ItemResolver, Sitecore.Kernel']" />
        <processor type="Anritsu.WebApp.SitecoreUtilities.Pipelines.HttpRequestBegin.ExecuteRequest, Anritsu.WebApp.SitecoreUtilities" patch:after="processor[@type='Sitecore.Pipelines.HttpRequest.ExecuteRequest, Sitecore.Kernel']"/>
        <processor type="Sitecore.Pipelines.HttpRequest.ExecuteRequest, Sitecore.Kernel">
          <patch:delete />
        </processor>
      </httpRequestBegin>
      <initialize>
        <processor type="Anritsu.WebApp.SitecoreUtilities.Pipelines.Initialize.InitializeRoutes, Anritsu.WebApp.SitecoreUtilities" />
      </initialize>
      <publishItem>
        <processor
            patch:after="*[@type='Sitecore.Publishing.Pipelines.PublishItem.CheckSecurity, Sitecore.Kernel']"
            type="Anritsu.WebApp.SitecoreUtilities.Pipelines.PublishToCDN, Anritsu.WebApp.SitecoreUtilities">
          <ExcludeNodes hint="list:AddFilterItems">         
            <Node name="ItemId">{7436E92E-6060-4B00-A2A2-7F435DD0CF40}</Node>
          </ExcludeNodes>
        </processor>
      </publishItem>
      <convertToRuntimeHtml>
        <processor
            patch:after="*[@type='Sitecore.Pipelines.ConvertToRuntimeHtml.ShortenLinks, Sitecore.Kernel']"
            type="Anritsu.WebApp.SitecoreUtilities.Pipelines.ResolveBucketUrl, Anritsu.WebApp.SitecoreUtilities"/>
      </convertToRuntimeHtml>
      <!--<linkManager>
        <processor
            patch:after="*[@type='Sitecore.Pipelines.linkManager.ShortenLinks, Sitecore.Kernel']"
            type="Anritsu.WebApp.SitecoreUtilities.Pipelines.ResolveBucketUrl, Anritsu.WebApp.SitecoreUtilities"/>
      </linkManager>-->
      <renderLayout>
        <processor type="Sitecore.MediaFramework.Pipelines.RenderLayout.InsertHeadControls, Sitecore.MediaFramework">
          <filterSites hint="list:AddFilterSite">
            <site name="GlobalWeb">GlobalWeb</site>
          </filterSites>
        </processor>
        <processor type="Sitecore.MediaFramework.Pipelines.RenderLayout.InsertItemId, Sitecore.MediaFramework">
          <filterSites hint="list:AddFilterSite">
            <site name="GlobalWeb">GlobalWeb</site>
          </filterSites>
        </processor>
      </renderLayout>
      <getTranslation>
        <!-- Custom pipeline processor that will get the fallback language of the current language 
        and attempt to get the translation of that, as a final step in getTranslation -->
        <processor patch:after="*[@type='Sitecore.Pipelines.GetTranslation.TryGetFromCoreDatabase, Sitecore.Kernel']" type="Anritsu.WebApp.SitecoreUtilities.Pipelines.TryGetFromFallbackLanguage, Anritsu.WebApp.SitecoreUtilities" />
      </getTranslation>
    </pipelines>
    <linkManager>
      <patch:attribute name="defaultProvider" value="switcher" />
      <providers>
        <add name="switcher" fallback="sitecore" type="Anritsu.WebApp.SitecoreUtilities.Providers.SwitchingLinkProvider, Anritsu.WebApp.SitecoreUtilities" />
        <add name="GlobalWeb" type="Sitecore.Links.LinkProvider, Sitecore.Kernel" languageEmbedding="always"/>
        <!--<add patch:after="*[@type='Sitecore.Links.LinkProvider, Sitecore.Kernel']" name="custom" type="Anritsu.WebApp.SitecoreUtilities.Providers.CustomLinkProvider, Anritsu.WebApp.SitecoreUtilities" addAspxExtension="true" alwaysIncludeServerUrl="false" encodeNames="true" languageEmbedding="never" languageLocation="filePath" shortenUrls="true" useDisplayName="false" />-->
      </providers>
    </linkManager>
    <settings>
      <setting name="BucketConfiguration.DynamicBucketFolderPath" patch:instead="setting[@name='BucketConfiguration.DynamicBucketFolderPath']"
                  value="Anritsu.WebApp.SitecoreUtilities.Extensions.CustomBucketFolderPath,Anritsu.WebApp.SitecoreUtilities"/>
      <setting name="BucketTemplateTest" value="{B2959F2A-334C-44CF-B6A6-3AADF99FFADC}"/>
      <setting name="HtmlEditor.DefaultProfile" value="/sitecore/system/Settings/Html Editor Profiles/Rich Text Custom" />
      <setting name="IgnoreUrlPrefixes" value="/sitecore/default.aspx|/trace.axd|/webresource.axd|/sitecore/shell/Controls/Rich Text Editor/Telerik.Web.UI.DialogHandler.aspx|/sitecore/shell/applications/content manager/telerik.web.ui.dialoghandler.aspx|/sitecore/shell/Controls/Rich Text Editor/Telerik.Web.UI.SpellCheckHandler.axd|/Telerik.Web.UI.WebResource.axd|/sitecore/admin/upgrade/|/layouts/testing|/Subscription/|/Checkout/|/RRCS/|/OrderTracking/|/GlobalWeb/Services/CountryStateService.asmx" />
      <setting name="Publishing.PublishEmptyItems" value="true" />
      <setting name="Media.UploadAsFiles" value="true" />
      <setting name="PagePreview.SiteName">
        <patch:attribute name="value">GlobalWeb</patch:attribute>
      </setting>
      <setting name="Sitecore.Services.Heartbeat.ExcludeConnection" value="DB_NGW_ConnectionString|webadmin" />

      <setting name="Indexing.DisableDatabaseCaches" value="true"/>
      <setting name="Caching.DisableCacheSizeLimits" value="true"/>
      <setting name="ContentEditor.CheckHasChildrenOnTreeNodes" value="false" />
      <setting name="ContentEditor.RenderCollapsedSections" value="false" />
      <setting name="WebEdit.ShowNumberOfLockedItemsOnButton" value="false" />

      <setting name="LinksAdmin.Site.GlobalWeb" value="1"/>
      <setting name="AnrSso.GWSp.DefaultReturnUrl" value="/{0}"/>
      <setting name="SSOExternalClientHosts" value="yaung01,www1.anritsu.co.jp,as37.cr.anritsu.co.jp,66.105.41.163"/>
      <setting name="WhitelistURL" value="local-www.anritsu.com"/>
      <setting name="DefaultLocale" value="en-US"/>
      <setting name="AnrSso.GWSp.Binding" value="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"/>
      <setting name="AnrSso.GWSp.Cert" value="x509_106.pfx"/>
      <setting name="AnrSso.GWSp.CertPwd" value="14755"/>


      <setting name="ControlDateFormat" value="{0:d}"/>
      <setting name="RMA_ERPSource_US" value="US"/>
      <setting name="RMA_ERPSource_JP" value="JP"/>
      <setting name="RMA_ERPSource" value="US"/>
      <setting name="RMA_WrtyCheck" value="/rrcs/warranty-check-status.aspx"/>
      <setting name="HtmlEditor.RemoveScripts" value="false" />

      <setting name="Query.MaxItems">
        <patch:attribute name="value">500</patch:attribute>
      </setting>

      <setting name="Counters.Enabled">
        <patch:attribute name="value">false</patch:attribute>
      </setting>

      <!--	WEBDAV FEATURE ENABLED
			Determines if WebDAV feature should be enabled at all.
			Specify 'true' to enable WebDAV and 'false' to disable the feature.
			Default value: true
			-->
      <setting name="WebDAV.Enabled">
        <patch:attribute name="value">false</patch:attribute>
      </setting>
      <!-- ENABLE BUCKET SPECIFIC DEBUG IN LOGS
				   If enabled, every query is written to the log file. Only have this turned on while you are debugging, switch it off in your production environment.
			-->
      <setting name="BucketConfiguration.EnableBucketDebug">
        <patch:attribute name="value">false</patch:attribute>
      </setting>
      <!-- Security Hardening Settings.	-->
      <setting name="Login.DisableAutoComplete" value="true" />
      <setting name="Login.DisableRememberMe" value="true" />
      <setting name="Media.RequestProtection.SharedSecret" value="AnrTechS1t3c0r3Aspectitsu" />
      <setting name="Indexing.UpdateInterval" value="23:50:00" />

      <setting name="ErrorPage" value="/page-not-found" />
      <setting name="ItemNotFoundUrl" value="/page-not-found" />
      <setting name="LayoutNotFoundUrl" value="/page-not-found" />
      <setting name="LinkItemNotFoundUrl" value="/page-not-found" />
      <setting name="NoAccessUrl" value="/page-not-found" />
      <setting name="NoLicenseUrl" value="/sitecoreservice/nolicense.aspx" />
    </settings>
    <events>
      <event name="item:created">
        <handler type="Anritsu.WebApp.SitecoreUtilities.Events.LimitNumberOfItems, Anritsu.WebApp.SitecoreUtilities" method="OnItemCreated" >
          <Item id="{DB7E5EF3-0A02-4535-90EF-148DB4DB7594}">
            <ItemCount>2</ItemCount>
          </Item>
        </handler>
      </event>
      <event name="item:saved">
        <handler type="Anritsu.WebApp.SitecoreUtilities.Events.GetS3MediaInfo, Anritsu.WebApp.SitecoreUtilities" method="OnItemSaved" >
        </handler>
      </event>
      <event name="publish:end">
        <handler type="Sitecore.Publishing.HtmlCacheClearer, Sitecore.Kernel" method="ClearCache">
          <sites hint="list">
            <site>website</site>
            <site>GlobalWeb</site>
          </sites>
        </handler>
        <handler type="Anritsu.WebApp.SitecoreUtilities.Events.ClearCache, Anritsu.WebApp.SitecoreUtilities" method="ClearItemCache">
          <Databases hint="list">
            <database>web</database>
          </Databases>
        </handler>
      </event>
      <event name="publish:end:remote">
        <handler type="Sitecore.Publishing.HtmlCacheClearer, Sitecore.Kernel" method="ClearCache">
          <sites hint="list">
            <site>website</site>
            <site>GlobalWeb</site>
          </sites>
        </handler>
        <handler type="Anritsu.WebApp.SitecoreUtilities.Events.ClearCache, Anritsu.WebApp.SitecoreUtilities" method="ClearItemCache">
          <Databases hint="list">
            <database>web</database>
          </Databases>
        </handler>
      </event>
      <event name="indexing:end">
        <handler type="Anritsu.WebApp.SitecoreUtilities.Events.ClearCache, Anritsu.WebApp.SitecoreUtilities" method="ClearItemCache">
          <Databases hint="list">
            <database>web</database>
          </Databases>
        </handler>
      </event>
      <event name="indexing:end:remote">
        <handler type="Anritsu.WebApp.SitecoreUtilities.Events.ClearCache, Anritsu.WebApp.SitecoreUtilities" method="ClearItemCache">
          <Databases hint="list">
            <database>web</database>
          </Databases>
        </handler>
      </event>
    </events>
    <!-- DATABASES -->
    <databases>
      <!-- web -->
      <database id="web" singleInstance="true" type="Sitecore.Data.Database, Sitecore.Kernel">
        <cacheSizes hint="setting">
          <data>200MB</data>
          <items>200MB</items>
          <paths>2500KB</paths>
          <itempaths>50MB</itempaths>
          <standardValues>2500KB</standardValues>
        </cacheSizes>
      </database>
    </databases>
    <sites>
      <site name="shell">
        <patch:attribute name="htmlCacheSize">100MB</patch:attribute>
      </site>
      <site name="website">
        <patch:attribute name="formsRoot">{96566513-E18E-4861-BD22-5CAFA66092EC}</patch:attribute>
      </site>
    </sites>
    <hooks>
      <hook type="Sitecore.Diagnostics.MemoryMonitorHook, Sitecore.Kernel">
        <patch:delete/>
      </hook>
    </hooks>
  </sitecore>
</configuration>