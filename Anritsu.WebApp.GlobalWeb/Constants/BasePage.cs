﻿using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Xml.Linq;
using Utils = Anritsu.WebApp.SitecoreUtilities.Utilities;

namespace Anritsu.WebApp.GlobalWeb.Constants
{
    public class BasePage : System.Web.UI.Page
    {
        private const string _SSOCLIENTLISTCACHEKEY = "SsoClientList";
        private const string _WHITELISTCACHEKEY = "WhiteList";

        public BasePage()
        {
            RequireSSL = false;
        }

        public static String GetRedirectUrl(string url, string defaultUrl)
        {
            if (string.IsNullOrEmpty(url)) return defaultUrl;
            url = HttpUtility.UrlDecode(url.Contains(",") ? url.Split(',')[0] : url);
            return !IsWhiteListUrl(url) ? defaultUrl : url;
        }

        public static bool IsWhiteListUrl(string url)
        {
            return IsWhiteListUrl(url, true);
        }

        public static bool IsWhiteListUrl(string url, bool isExternalConfig)
        {
            var isWhiteListUrl = true;
            if (string.IsNullOrEmpty(url)) return false;
            if ((url.StartsWith("/") || url.StartsWith("~/")) && !url.StartsWith("//")) return true;
            url = url.StartsWith("http://") || url.StartsWith("https://") || url.StartsWith("//") ? url : "http://" + url;
            Uri uri = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri) || null == uri)
            {
                //Invalid URL
                return false;
            }
            if (!IsWhiteListHost(new Uri(url).Host, isExternalConfig))
                isWhiteListUrl = false;

            return isWhiteListUrl;
        }

        public static bool IsWhiteListHost(string hostName, bool isExternalconfig)
        {
            var isWhiteListUrl = false;
            if (isExternalconfig)
            {
                try
                {
                    var hostlist = new BasePage().GetWhiteListfromCache();
                    if (hostlist != null && hostlist.Count > 0)
                    {
                        isWhiteListUrl = hostlist.Contains(hostName);
                    }
                }
                catch (Exception)
                {
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Sitecore.Configuration.Settings.GetSetting("WhitelistURL")))
                    return false;
                var hostList = new List<string>(Sitecore.Configuration.Settings.GetSetting("WhitelistURL").Split(','));
                isWhiteListUrl = hostList.Contains(hostName.ToLower());
            }
            return isWhiteListUrl;
        }

        public static bool IsSsoClientHost(string hostName, bool isExternalconfig)
        {
            var isssoClient = false;
            if (isExternalconfig)
            {
                try
                {
                    var hostlist = GetSsoClientListfromCache();
                    if (hostlist != null && hostlist.Count > 0)
                        isssoClient = hostlist.Contains(hostName);
                }
                catch (Exception)
                {
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Sitecore.Configuration.Settings.GetSetting("SSOExternalClientHosts")))
                    return false;

                var hostList = new List<string>(Sitecore.Configuration.Settings.GetSetting("SSOExternalClientHosts").Split(','));
                isssoClient = hostList.Contains(hostName.ToLower());
            }
            return isssoClient;
        }

        public static List<string> GetSsoClientListfromCache()
        {
            const string cachefile = "~/App_Data/ClientList/SSOExternalClientHosts.config";
            var hostlist = new List<string>();
            var cache = HttpContext.Current.Cache;
            try
            {
                if (cache[_SSOCLIENTLISTCACHEKEY] == null)
                {
                    var elements = XElement.Load(HttpContext.Current.Server.MapPath(cachefile));
                    hostlist = (from c in elements.Elements("add")
                                where (string)c.Attribute("Isactive") == "true"
                                select c.Attribute("url").Value).ToList<string>();
                    cache.Insert(_SSOCLIENTLISTCACHEKEY, hostlist,
                        new CacheDependency(HttpContext.Current.Server.MapPath(cachefile))
                        , Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                }
                else
                {
                    hostlist = (List<string>)cache[_SSOCLIENTLISTCACHEKEY];
                }
            }
            catch (Exception)
            {
            }
            return hostlist;
        }

        private List<string> GetWhiteListfromCache()
        {
            const string cachefile = "~/App_Data/ClientList/WhitelistUrl.config";
            var hostlist = new List<string>();
            var cache = HttpContext.Current.Cache;

            try
            {
                if (cache[_WHITELISTCACHEKEY] == null)
                {
                    var elements = XElement.Load(HttpContext.Current.Server.MapPath(cachefile));
                    hostlist = (from c in elements.Elements("add")
                                where (string)c.Attribute("Isactive") == "true"
                                select c.Attribute("url").Value).ToList<string>();
                    cache.Insert(_WHITELISTCACHEKEY, hostlist,
                        new CacheDependency(HttpContext.Current.Server.MapPath(cachefile)),
                        Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                }
                else
                {
                    hostlist = (List<string>)cache[_WHITELISTCACHEKEY];
                }
            }
            catch (Exception)
            {
            }
            return hostlist;
        }

        public static string GetCurrentLanguage()
        {
            //if (HttpContext.Current != null && HttpContext.Current.Session != null &&
            //    HttpContext.Current.Session[SSOUtilities.SessionVariables.Language] != null)
            //    return HttpContext.Current.Session[SSOUtilities.SessionVariables.Language].ToString();
            //else
            //{
            //    string lang = new BasePage().CurrentLanguage;
            //    if (HttpContext.Current != null)
            //        HttpContext.Current.Session[SSOUtilities.SessionVariables.Language] = lang;
            //    return lang;
            //}
            return Sitecore.Context.Language.Name;
        }

        private string CurrentLanguage
        {
            get
            {
                var language = Sitecore.Context.Language.Name;
               
                if (string.IsNullOrEmpty(language))
                {
                  language = Sitecore.Configuration.Settings.GetSetting("DefaultLocale");
                }
                return language;
            }
        }


        [Browsable(true)]
        [Description("Switching port between 443 <--> 80")]
        public virtual bool RequireSSL { get; set; }

        public static GWSsoUserType LoggedInUser()
        {
            var usr = HttpContext.Current.Session[Constants.SSOUtilities.SessionVariables.UserLoggedIn]
                as GWSsoUserType;

            if (usr == null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                usr = RefreshUserSession();
            }
            return usr;
        }

        public static GWSsoUserType RefreshUserSession()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;

            var redirectLoginUrl = string.Format("/Sign-In.aspx?ReturnURL={0}"
              , HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
            // HttpContext.Current.Response.Redirect(redirectLoginUrl);

            var decryptedIek =
                SecurityUtilities.ClientCacheEncrypter.DecryptString(HttpContext.Current.User.Identity.Name);
            if (string.IsNullOrEmpty(decryptedIek)) return null;
            var splittedIek = decryptedIek.Split('|');
            var aClUserId = 0;
            if (splittedIek.Length != 2 || !int.TryParse(splittedIek[0], out aClUserId))
                HttpContext.Current.Response.Redirect(redirectLoginUrl);

            var secretKey = splittedIek[1];
            var ssoClient = new Anritsu.Library.SSOSDK.AnritsuSsoClient();
            var usr = ssoClient.GetUserByGWUserId(aClUserId, secretKey);
            HttpContext.Current.Session[Constants.SSOUtilities.SessionVariables.UserLoggedIn] = usr;
            return usr;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public static CultureInfo GetCurrentCultureInfo()
        {
            return new CultureInfo(Sitecore.Context.Language.ToString());
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public static CultureInfo GetCurrentUICultureInfo()
        {
            return new CultureInfo(Sitecore.Context.Language.ToString());
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public static string GetCurrentCulture()
        {
            return Sitecore.Context.Language.ToString();
        }

        public static int GetCultureGroupId(string code)
        {
            var cultureCode = 1;
            switch (code)
            {

                case "en-US":
                    cultureCode = 1;
                    break;

                case "ja-JP":
                    cultureCode = 2;
                    break;
                case "en-GB":
                    cultureCode = 3;
                    break;

                case "en-AU":
                    cultureCode = 4;
                    break;

                case "zh-CN":
                    cultureCode = 6;
                    break;

                case "ko-KR":
                    cultureCode = 7;
                    break;

                case "zh-TW":
                    cultureCode = 10;
                    break;
                case "ru-RU":
                    cultureCode = 14;
                    break;
                case "en-IN":
                    cultureCode = 15;
                    break;

            }
            return cultureCode;
        }


        protected override void OnLoad(EventArgs e)
        {
            SwitchSSL();
            base.OnLoad(e);
        }

        private void SwitchSSL()
        {
            if ((RequireSSL && !Request.IsSecureConnection) || (!RequireSSL && Request.IsSecureConnection))
            {
                //The old code was breaking the rewritten URLs
                // no longer need this because we are using css versioning and abs urls. HttpResponse.RemoveOutputCacheItem("/App_Themes/AnritsuDefault/GlobalStyles.css");

                var ubHttp = new UriBuilder(Request.Url);

                #region " HTTP_X_REWRITE_URL "

                if (Request.ServerVariables["HTTP_X_REWRITE_URL"] != null)
                {
                    string rewrittenUrl = Request.ServerVariables["HTTP_X_REWRITE_URL"];
                    if (!string.IsNullOrEmpty(rewrittenUrl))
                    {
                        var ubBaseUri = new UriBuilder(ubHttp.Scheme, ubHttp.Host);
                        var rwRewrittenUri = new Uri(ubBaseUri.Uri, rewrittenUrl);
                        ubHttp = new UriBuilder(rwRewrittenUri);
                    }
                }

                #endregion

                #region " TA's special code."

                if (Request.Url.ToString().Contains("/Contact-Us/Web-Master.aspx") &&
                    string.IsNullOrEmpty(Request.QueryString["Referal"]))
                {
                    ubHttp.Query = "Referal=" + Request.UrlReferrer;
                }

                #endregion

                if (RequireSSL && !Request.IsSecureConnection)
                {
                    ubHttp.Scheme = Uri.UriSchemeHttps;
                }
                else if (!RequireSSL && Request.IsSecureConnection)
                {
                    ubHttp.Scheme = Uri.UriSchemeHttp;
                }

                ubHttp.Port = -1; // use default value for protocol          
                Response.Redirect(ubHttp.ToString(), false);
            }
        }

    }
}