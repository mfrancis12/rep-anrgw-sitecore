﻿using System;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using BLL = Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;

namespace Anritsu.WebApp.GlobalWeb.Constants
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gsa")]
    public static class GsaHelpers
    {
        public static string PerformHeaderSearch()
        {
            //var keywords = HttpUtility.UrlEncode(RemoveBadChars(searchTerm.Trim()));
            var site = GetCurrentDefaultCollection(BasePage.GetCultureGroupId(Sitecore.Context.Language.ToString()));
            var sbQuery = new StringBuilder();

            sbQuery.Append(string.Format(ConfigurationManager.AppSettings["SearchDefaultUrl"], Sitecore.Context.Language));

            sbQuery.AppendFormat("{0}={1}", QueryStringVariables.GSA.Site, site);
            //sbQuery.AppendFormat("&{0}={1}", QueryStringVariables.GSA.Query, keywords);
            sbQuery.AppendFormat("&{0}={1}", QueryStringVariables.GSA.Filter, QueryStringVariables.GSA.FilterValue);
            //sbQuery.AppendFormat("&{0}={1}", QueryStringVariables.CountryName, HttpContext.Current.Request.Cookies[CookieVariables.UserSelectedCountry] != null 
                //? HttpContext.Current.Request.Cookies[CookieVariables.UserSelectedCountry].Value : string.Empty);
            sbQuery.AppendFormat("&{0}={1}", QueryStringVariables.IsLoggedIn, HttpContext.Current.Session[SessionVariables.UserLoggedIn] == null ? "false" : "true");

            //HttpContext.Current.Response.Clear();
            //HttpContext.Current.Response.Redirect(sbQuery.ToString());
            return sbQuery.ToString();
        }

        public static string RemoveBadChars(string searchKeyword)
        {
            try
            {
                var pattern = new Regex(Convert.ToString(ConfigurationManager.AppSettings["SearchKeywordRegexFilter"]));
                return pattern.Replace(searchKeyword, "");
            }
            catch { return searchKeyword; }
        }

        public static string GetCurrentDefaultCollection(int cultureGroupId)
        {
            var collectionKey = ConfigurationManager.AppSettings["GsaRootCaollection"]; 
            string settingName = BLL.Search.GetCollectionName(collectionKey, cultureGroupId);
            if (string.IsNullOrEmpty(settingName))
                settingName = BLL.Search.GetDefaultCollection(cultureGroupId);

            return settingName;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gsa")]
        public static bool IsGsaCrawler()
        {
            return HttpContext.Current.Request.UserAgent != null && HttpContext.Current.Request.UserAgent.Contains("gsa-crawler");
        }
    }
}