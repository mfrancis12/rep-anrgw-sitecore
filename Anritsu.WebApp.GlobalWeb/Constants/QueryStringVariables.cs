﻿using System.Configuration;
namespace Anritsu.WebApp.GlobalWeb.Constants
{
    public static class QueryStringVariables
    {
        public const string EventId = "eventID";
        public const string ReturnUrl = "ReturnURL";
        public const string Mode = "Mode";
        public const string ModelId = "ModelID";
        public const string SerialNumber = "SerialNumber";
        public const string TransactionKey = "trtk";
        public const string RegistrationId = "RegistrationId";
        public const string CountryName = "CountryName";
        public const string IsLoggedIn = "ili";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "GSA")]
        public static class GSA
        {
            public const string Site = "site";
            public const string Query = "q";
            public const string Filter = "filter";
            public static readonly string FilterValue = ConfigurationManager.AppSettings["FilterMode"];
        }
    }
}