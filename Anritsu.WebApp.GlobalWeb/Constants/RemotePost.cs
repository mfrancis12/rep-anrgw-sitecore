﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Anritsu.WebApp.SitecoreUtilities;

namespace Anritsu.WebApp.GlobalWeb.Constants
{
    public class RemotePost
    {
        private readonly NameValueCollection _InputValues = new NameValueCollection();

        public string Url = string.Empty;
        public string Method = "post";
        public string FormName = "form1";

        public void Add(string name, string valueString)
        {
            _InputValues.Add(name, valueString);
        }

        public void Post()
        {
            HttpContext.Current.Response.Clear();
            var formToPost = new StringBuilder();
            formToPost.Append("<html><head>");
            formToPost.AppendFormat("</head><body onload=\"document.{0}.submit()\">", FormName);
            formToPost.AppendFormat("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >",
                FormName, Method, Url);
            for (int i = 0; i < _InputValues.Keys.Count; i++)
            {
                formToPost.AppendFormat("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">"
                    , _InputValues.Keys[i], _InputValues[_InputValues.Keys[i]]);
            }
            formToPost.Append("</form>");
            formToPost.Append("</body></html>");
            HttpContext.Current.Response.Write(formToPost.ToString());
            HttpContext.Current.Response.End();
        }

        public string GetPostString()
        {
            var formToPost = new StringBuilder();
            formToPost.Append("<html><head>");
            formToPost.AppendFormat("</head><body onload=\"document.{0}.submit()\">", FormName);
            formToPost.AppendFormat("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >",
                FormName, Method, Url);
            for (int i = 0; i < _InputValues.Keys.Count; i++)
            {
                formToPost.AppendFormat("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">"
                    , _InputValues.Keys[i], _InputValues[_InputValues.Keys[i]]);
            }
            formToPost.Append("</form>");
            formToPost.Append("</body></html>");
            return formToPost.ToString();
        }

        public void SmartPost()
        {
            var currentPage = (Page)HttpContext.Current.CurrentHandler;
            ScriptManager currentScriptManager = ScriptManager.GetCurrent(currentPage);

            var formToPost = new StringBuilder();
            formToPost.Append("var submitForm = document.createElement(\"FORM\");");
            formToPost.Append("document.body.appendChild(submitForm);");
            formToPost.Append("submitForm.method = \"POST\";");
            formToPost.AppendFormat("submitForm.name = '{0}';", FormName);
            formToPost.Append("var newElement;");
            for (int i = 0; i < _InputValues.Keys.Count; i++)
            {
                formToPost.Append("newElement = document.createElement('input');");
                formToPost.AppendFormat("newElement.setAttribute('name', '{0}');", _InputValues.Keys[i]);
                formToPost.Append("newElement.setAttribute('type', 'hidden');");
                formToPost.AppendFormat("newElement.value = \"{0}\";",
                    HttpUtility.HtmlEncode(_InputValues[_InputValues.Keys[i]].ConvertNullToEmptyString()));
                formToPost.Append("submitForm.appendChild(newElement);");

            }
            formToPost.AppendFormat("submitForm.action = '{0}';", Url);
            formToPost.Append("submitForm.submit();");

            if (currentScriptManager != null && currentScriptManager.IsInAsyncPostBack)
            {
                #region Came from AJAX request
                ScriptManager.RegisterStartupScript(currentPage, typeof(string), "submitFormAjax", formToPost.ToString(), true);
                #endregion
            }
            else
            {
                #region Came from normal request
                currentPage.ClientScript.RegisterStartupScript(typeof(string), "submitFormNoAjax", formToPost.ToString(), true);
                #endregion
            }
        }


    } 
}