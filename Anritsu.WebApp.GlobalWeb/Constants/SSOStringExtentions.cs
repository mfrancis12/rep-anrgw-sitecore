﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.Constants
{
    public static class SSOStringExtensions
    {
        public static string ConvertNullToEmptyString(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }
            return input;
        }
    }
}