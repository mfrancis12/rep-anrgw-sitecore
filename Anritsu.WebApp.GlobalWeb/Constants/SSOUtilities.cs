﻿#region Assembly Atp.Saml.dll, v5.0.40.5183
// F:\AnritsuSVN\rep-anrgw-dev\branches\BR_Dev_Venu\CodeBase\AssemblyReference\Atp.UltimateSaml\Atp.Saml.dll
#endregion

using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.Constants.SsoSamlUtility.Utils;
using Atp.Saml;
using Atp.Saml2;
using Atp.Saml2.Binding;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Security;
using Attribute = Atp.Saml2.Attribute;

namespace Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities
{    
    public static class QSKeys
    {
        public const String ReturnURL = "ReturnURL";
        public const String Binding = "binding";
        public const String Wreply = "wreply";
        public const String Sptkn = "sptkn";
        public const String MarketLocale = "mkt"; //region code to overide for customizations
        public const String SiteLocale = "lang"; // content locale
        public const String Idplgo = "idplgo";
        public const String IsNew = "isNew";
    }

    public static class ConfigUtility
    {

        public static string GetStringProperty(string key, string defaultValue)
        {

            return GetStringSettingFromContext(key, defaultValue);
        }

        private static string GetStringSettingFromContext(string key, string defaultValue)
        {
            if (HttpContext.Current == null)
            {
                return GetStringPropertyFromConfig(key, defaultValue);
            }

            string setting;
            if (HttpContext.Current.Items[key] == null)
            {
                setting = GetStringPropertyFromConfig(key, defaultValue);
                HttpContext.Current.Items[key] = setting;
            }
            else
            {
                setting = HttpContext.Current.Items[key].ToString();
            }
            return setting;


        }

        private static string GetStringPropertyFromConfig(string key, string defaultValue)
        {
            if (Sitecore.Configuration.Settings.GetSetting(key) == null) return defaultValue;
            return Sitecore.Configuration.Settings.GetSetting(key);
        }

        public static string AppSettingGetValue(string configKey)
        {
            try
            {
                if (string.IsNullOrEmpty(Sitecore.Configuration.Settings.GetSetting(configKey))) return string.Empty;
                return Sitecore.Configuration.Settings.GetSetting(configKey);
            }
            catch { return string.Empty; }
        }
    }

    public static class WebUtility
    {
        public static string GetAbsoluteUrl(string relativeUrl)
        {
            if (HttpContext.Current == null) return String.Empty;
            if (relativeUrl.StartsWith("http", StringComparison.InvariantCultureIgnoreCase)) return relativeUrl;//not relative url
            String relUrl = relativeUrl.Replace("~/", "/");
            return new Uri(HttpContext.Current.Request.Url, relUrl).ToString();

        }
        public static void ForceSecureConnection()
        {
            //if (!HttpContext.Current.Request.IsSecureConnection && HttpContext.Current.Request.Url.Host != "localhost")

            // it is necessary to use AbsoluteUrl in lieu of Url due to ReturnUrl encoding
            HttpContext context = HttpContext.Current;
            if (context == null) return;
            if (!context.Request.IsSecureConnection)
            {
                context.Response.Redirect(
                    context.Request.Url.AbsoluteUri.Replace("http://", "https://"));
                context.Response.End();
            }
        }
    }

    internal class GwSsoSamlUtility : SsoSamlUtility
    {

        public static GWSsoUserType InitiateGwSsoUserObject(Response samlRes, out bool rememberme)
        {
            rememberme = false;
            if (samlRes == null) return null;
            var samlAssertion = samlRes.Assertions[0] as Assertion;
            if (samlAssertion == null) return null;
            var usersecKey = ConvertUtility.ConvertToGuid(samlAssertion.Subject.NameId.NameIdentifier, Guid.Empty);
            if (usersecKey.IsNullOrEmptyGuid()) return null;
            var gwUserId = ConvertUtility.ConvertToInt32(FindSamlAttributeValue("gwuserid", samlAssertion), 0);
            var emailAddress = FindSamlAttributeValue("emailaddress", samlAssertion);


            bool.TryParse(FindSamlAttributeValue("rememberme", samlAssertion), out rememberme);

            var ssoWsUser = new GWSsoUserType
            {
                UserId = gwUserId,
                SecToken = usersecKey.ToString(),
                EmailAddress = emailAddress,
                LoginName = FindSamlAttributeValue("loginname", samlAssertion),
                FirstName = FindSamlAttributeValue("firstname", samlAssertion),
                MiddleName = FindSamlAttributeValue("middlename", samlAssertion),
                LastName = FindSamlAttributeValue("lastname", samlAssertion),
                PrimaryCultureCode = FindSamlAttributeValue("primaryculturecode", samlAssertion),
                JobTitle = FindSamlAttributeValue("jobtitle", samlAssertion),
                CompanyName = FindSamlAttributeValue("companyname", samlAssertion),
                StreetAddress1 = FindSamlAttributeValue("streetaddress1", samlAssertion),
                StreetAddress2 = FindSamlAttributeValue("streetaddress2", samlAssertion),
                City = FindSamlAttributeValue("city", samlAssertion),
                State = FindSamlAttributeValue("state", samlAssertion),
                PostalCode = FindSamlAttributeValue("zipcode", samlAssertion),
                CountryCode = FindSamlAttributeValue("countrycode", samlAssertion),
                PhoneNumber = FindSamlAttributeValue("phonenumber", samlAssertion),
                FaxNumber = FindSamlAttributeValue("faxnumber", samlAssertion),
                RegisteredFromURL = FindSamlAttributeValue("registeredfromurl", samlAssertion),
                IsTempPassword = false,
                EncryptedTempPassword = String.Empty,
                CreatedUTCDate = FindSamlAttributeValue("createdutcdate", samlAssertion),
                MotifiedUTCDate = FindSamlAttributeValue("modifiedutcdate", samlAssertion),
                CountryName = FindSamlAttributeValue("countryname", samlAssertion)
            };
            var userStatusId = ConvertUtility.ConvertToInt32(FindSamlAttributeValue("userstatusid", samlAssertion), 0);
            if (userStatusId > 0)
            {
                ssoWsUser.UserStatus = new GWSsoUserStatusType();
                ssoWsUser.UserStatus.UserStatusId = userStatusId;
                ssoWsUser.UserStatus.UserStatus = FindSamlAttributeValue("userstatustitle", samlAssertion);
            }
            var userTypeId = ConvertUtility.ConvertToInt32(FindSamlAttributeValue("usertypeid", samlAssertion), 0);
            if (userTypeId > 0)
            {
                ssoWsUser.UserType = new GWSsoUserTypeType
                {
                    UserTypeId = userTypeId,
                    UserType = FindSamlAttributeValue("usertypename", samlAssertion),
                    IsUserProfile =
                        ConvertUtility.ConvertToBoolean(FindSamlAttributeValue("usertypeisuserprofile", samlAssertion),
                            false),
                    Description = String.Empty
                };
            }
            var loginHistories = InitLoginHistory(samlAssertion);
            if (loginHistories != null) ssoWsUser.LoginHistoryCollection = loginHistories.ToArray();

            var roles = InitUserRolesAndPermissions(samlAssertion);
            if (roles != null) ssoWsUser.UserRoleCollection = roles.ToArray();

            HttpContext.Current.Session[SessionVariables.UserLoggedIn] = ssoWsUser;

            return ssoWsUser;
        }

        private static List<GWSsoLoginHistoryType> InitLoginHistory(Assertion samlAssertion)
        {
            if (samlAssertion == null) return null;
            var loginHistories = FindSamlAttributes("loginhistory", samlAssertion);
            if (loginHistories == null) return null;
            var lst = new List<GWSsoLoginHistoryType>();
            foreach (var att in loginHistories)
            {
                if (att.Values == null) continue;
                var lht = new GWSsoLoginHistoryType();
                foreach (var dataStr in att.Values.Select(av => av.Data.ToString()))
                {
                    if (dataStr.StartsWith("IP~")) lht.IPAddress = dataStr.Replace("IP~", String.Empty);
                    if (dataStr.StartsWith("LoggedInDate~")) lht.LoginUTCDate = dataStr.Replace("LoggedInDate~", String.Empty);
                }
                lst.Add(lht);
            }
            return lst;
        }

        private static List<GWSsoUserRoleType> InitUserRolesAndPermissions(Assertion samlAssertion)
        {
            if (samlAssertion == null) return null;
            var rolesPers = FindSamlAttributes("gwroleperm", samlAssertion);
            if (rolesPers == null) return null;
            var lst = new List<GWSsoUserRoleType>();
            foreach (var att in rolesPers)
            {
                if (att.Values == null) continue;
                var roleObj = new GWSsoUserRoleType();
                var permissions = new List<GWSsoPermissionTokenType>();
                foreach (var dataStr in att.Values.Select(av => av.Data.ToString()))
                {
                    if (dataStr.StartsWith("GWROLE~")) roleObj.RoleId = dataStr.Replace("GWROLE~", String.Empty);
                    if (dataStr.StartsWith("GWROLETITLE~")) roleObj.RoleTitle = dataStr.Replace("GWROLETITLE~", String.Empty);
                    if (!dataStr.StartsWith("GWPERTOKEN~")) continue;
                    var rolePerArr = dataStr.Split('|');
                    if (rolePerArr.Length < 1) continue;
                    GWSsoPermissionTokenType per = null;
                    foreach (var rolePerStr in rolePerArr)
                    {
                        if (rolePerStr.StartsWith("GWPERTOKEN~"))
                        {
                            var perToken = ConvertUtility.ConvertToGuid(rolePerStr.Replace("GWPERTOKEN~", String.Empty), Guid.Empty);
                            if (perToken.IsNullOrEmptyGuid()) break;
                            per = new GWSsoPermissionTokenType { PermissionToken = perToken };
                        }
                        else if (rolePerStr.StartsWith("PERTITLE~")) per.TokenTitle = rolePerStr.Replace("PERTITLE~", String.Empty);
                        else if (rolePerStr.StartsWith("PERDESC~")) per.TokenDescription = rolePerStr.Replace("PERDESC~", String.Empty);
                        else if (rolePerStr.StartsWith("PERCREATEDATE~")) per.CreatedUTCDate = rolePerStr.Replace("PERCREATEDATE~", String.Empty);
                    }
                    if (per != null) permissions.Add(per);
                }
                roleObj.PermissionCollection = permissions.ToArray();
                lst.Add(roleObj);
            }
            return lst;
        }

        #region Certificate methods

        public static X509Certificate2 GetSpCertificate()
        {
            var fileName =Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.Cert");
            var certPwd = Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.CertPwd");
            var certFilePath = Path.Combine(Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.CertStore"), fileName);
            return string.IsNullOrEmpty(certFilePath) ? null : CertificateUtilities.LoadCertificate(certFilePath, certPwd);
        }

        public static X509Certificate2 GetIdpCertificate()
        {
            var fileName = Sitecore.Configuration.Settings.GetSetting("AnrSso.GWIdp.Cert");
            if (string.IsNullOrEmpty(fileName)) return null;
            var certFilePath = Path.Combine(Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.CertStore"), fileName);
            return CertificateUtilities.LoadCertificate(certFilePath, null);
        }

        #endregion

        public static void HandleRedirect(String resourceUrl)
        {
            var context = HttpContext.Current;
            if (context == null) return;
            if (resourceUrl.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
            {
                #region " handle http resource/return URL "
                if (BasePage.IsSsoClientHost(new Uri(resourceUrl).Host, true))
                {
                    System.Diagnostics.Trace.Write("InIsSsoClientHost:" + resourceUrl);
                    context.Response.Redirect(String.Format("/GlobalWeb/sso/gateway.aspx?ReturnURL={0}", HttpUtility.UrlEncode(resourceUrl)));
                }
                else if (BasePage.IsWhiteListHost(new Uri(resourceUrl).Host, true)) //check white list here
                {
                    //this will not handle attacks without protocol like attack.bla.com
                    System.Diagnostics.Trace.Write("InWhiteListHost:" + resourceUrl);
                    context.Response.Redirect(resourceUrl);
                }
                else
                {
                    System.Diagnostics.Trace.Write("DefaultReturnUrl");
                    context.Response.Redirect(string.Format(Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.DefaultReturnUrl", "/{0}/Home.aspx"), BasePage.GetCurrentLanguage()));
                }
                #endregion
            }
            else
            {
                context.Response.Redirect(resourceUrl);
            }
        }

     


    }

    public class CertificateUtilities
    {
        public static X509Certificate2 LoadCertificate(string certFilePath, string password)
        {
            var cert = new X509Certificate2(certFilePath, password, X509KeyStorageFlags.MachineKeySet);
            return cert;
        }
    }

    public abstract class SsoSamlUtility
    {
        public static void SamlLoginRequest_SendHttpPost(string spResourceUrl, string acsUrl, string idpLoginurl, string localAndmkt, X509Certificate2 sPx509Certificate, string spTokenId, bool isNew)
        {
            HttpContext context = HttpContext.Current;
            if (context == null) return;

            string relayState = Guid.NewGuid().ToString();
            SamlSettings.CacheProvider.Insert(relayState, spResourceUrl, new TimeSpan(1, 0, 0));
            AuthnRequest authnRequest = SamlLoginRequest_CreateAuthnRequest(spResourceUrl, acsUrl, idpLoginurl, sPx509Certificate);
            String idpUrl;
            if (isNew)
            {
                idpUrl = string.Format("{0}?" +QSKeys.Sptkn + "={1}&" + QSKeys.Wreply + "={2}&" + QSKeys.MarketLocale + "={3}&" + QSKeys.SiteLocale + "={3}&" + QSKeys.IsNew + "={4}",
                  idpLoginurl,
                  spTokenId,
                  HttpUtility.UrlEncode(spResourceUrl), localAndmkt, "True"
                  );
            }
            else
            {
                idpUrl = string.Format("{0}?" +QSKeys.Sptkn + "={1}&" + QSKeys.Wreply + "={2}&" + QSKeys.MarketLocale + "={3}&" + QSKeys.SiteLocale + "={3}",
                idpLoginurl,
                spTokenId,
                HttpUtility.UrlEncode(spResourceUrl), localAndmkt
                );
            }
            authnRequest.SendHttpPost(context.Response, idpUrl, relayState);
            //causing ThreadAbortException Occurs If You Use Response.End, Response.Redirect, or Server.Transfer
            //context.Response.End();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        public static void SamlLogoutRequest_SendHttpPost(string spResourceUrl, string idpLogout, string spTokenId, string localeAndmkt, X509Certificate2 sPx509Certificate)
        {
            HttpContext context = HttpContext.Current;
            if (context == null) return;
            // Create a logout request.
            var logoutRequest = new LogoutRequest
            {
                Issuer = new Issuer(WebUtility.GetAbsoluteUrl("~/")),
                NameId = new NameId(context.User.Identity.Name)
            };

            var idpUrl = string.Format("{0}?" + QSKeys.Sptkn + "={1}&" + QSKeys.Wreply + "={2}&" + QSKeys.SiteLocale + "={3}&" + QSKeys.MarketLocale + "={3}",
             idpLogout,
              spTokenId,
              HttpUtility.UrlEncode(spResourceUrl), localeAndmkt
              );
            logoutRequest.Redirect(context.Response, idpUrl, null, sPx509Certificate.PrivateKey);
            //causing ThreadAbortException Occurs If You Use Response.End, Response.Redirect, or Server.Transfer
            //context.Response.End();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        public static AuthnRequest SamlLoginRequest_CreateAuthnRequest(string spResourceUrl, string acsUrl, string idpLoginurl, X509Certificate2 x509Certificate)
        {
            var issuerUrl = WebUtility.GetAbsoluteUrl("~/");
            var acsBaseUrl = WebUtility.GetAbsoluteUrl(acsUrl);
            string assertionConsumerServiceUrl = string.Format("{0}?" + SSOUtilities.QSKeys.Wreply + "={1}",
                acsBaseUrl,
                HttpUtility.UrlEncode(spResourceUrl)
                );

            // Create the authentication request.
            var authnRequest = new AuthnRequest
            {
                Destination = idpLoginurl,
                Issuer = new Issuer(issuerUrl),
                ForceAuthn = false,
                NameIdPolicy = new NameIdPolicy(null, null, true),
                ProtocolBinding = SamlBindingUri.HttpPost,
                AssertionConsumerServiceUrl = assertionConsumerServiceUrl
            };
            // X509Certificate2 x509Certificate = GetSpCertificate();
            authnRequest.Sign(x509Certificate);
            return authnRequest;
        }

        public static Response BuildACSResponse(out string relayState, string artifactUrl, X509Certificate2 idpx509Certificate, string bindingType)
        {
            Response samlResponse = null;
            relayState = null;
            HttpContext context = HttpContext.Current;
            if (context == null) return null;
            //default binding.
            if (string.IsNullOrEmpty(bindingType ))
                bindingType = SamlBindingUri.HttpPost;
            switch (bindingType)
            {
                case SamlBindingUri.HttpPost:
                    samlResponse = Response.Create(context.Request);
                    relayState = samlResponse.RelayState;
                    break;

                case SamlBindingUri.HttpArtifact:
                    Saml2ArtifactType0004 httpArtifact = Saml2ArtifactType0004.CreateFromHttpArtifactHttpForm(context.Request);

                    // Create an artifact resolve request.
                    var artifactResolve = new ArtifactResolve
                    {
                        Issuer = new Issuer(WebUtility.GetAbsoluteUrl("~/")),
                        Artifact = new Artifact(httpArtifact.ToString())
                    };

                    // Send the artifact resolve request and receive the artifact response.
                    string spArtifactResponderUrl = artifactUrl;

                    ArtifactResponse artifactResponse = ArtifactResponse.SendSamlMessageReceiveAftifactResponse(spArtifactResponderUrl, artifactResolve);
                    samlResponse = new Response(artifactResponse.Message);
                    relayState = httpArtifact.RelayState;
                    break;
            }

            // Verify the response's signature.
            if (samlResponse != null && samlResponse.Validate(idpx509Certificate))
                return samlResponse;
            return null;
        }

        public static void DoDefaultSignout(string redirectUrl)
        {
            HttpContext context = HttpContext.Current;
            if (context == null) return;
            if (context.Session != null) context.Session.Abandon();
            FormsAuthentication.SignOut();
            redirectUrl = (string.IsNullOrEmpty(redirectUrl)) ? FormsAuthentication.DefaultUrl : redirectUrl;
            context.Response.Redirect(redirectUrl, true);
        }

        public static String FindSamlAttributeValue(string attributeName, Assertion samlAssertion)
        {
            if (samlAssertion == null) return null;
            String attNameUrnToFind = String.Format("urn:oid:anrsso:{0}", attributeName);
            var attFound = (from attstmt in samlAssertion.AttributeStatements where attstmt.Attributes != null select attstmt.Attributes.Cast<Atp.Saml2.Attribute>().ToList() into genatt select (from a in (IList<Attribute>)genatt where a.Name.Equals(attNameUrnToFind, StringComparison.InvariantCultureIgnoreCase) select a) into sa where sa != null && sa.Count() == 1 select sa.First()).FirstOrDefault();
            if (attFound != null && attFound.Values != null && attFound.Values.Count == 1)
                return ConvertUtility.ConvertNullToEmptyString(attFound.Values[0].Data);
            return String.Empty;
        }

        public static List<Attribute> FindSamlAttributes(string attributeName, Assertion samlAssertion)
        {
            if (samlAssertion == null) return null;
            String attributeNameUrnToFind = String.Format("urn:oid:anrsso:{0}", attributeName);
            List<Attribute> lst = null;
            foreach (AttributeStatement attstmt in samlAssertion.AttributeStatements)
            {
                if (attstmt.Attributes != null)
                {
                    IList<Attribute> genatt = attstmt.Attributes.Cast<Attribute>().ToList();
                    var sa = from a in genatt
                             where a.Name.Equals(attributeNameUrnToFind, StringComparison.InvariantCultureIgnoreCase)
                             select a;
                    if (lst == null) lst = new List<Attribute>();
                    lst.AddRange(sa.ToArray());
                }

            }
            return lst;
        }
       
    }  

    public static class SessionVariables
    {
        public const string Language = "Language";
        public const string UserLoggedIn = "User";
    }

    public static class MethodExt
    {
        public static bool IsNullOrEmptyString(this string input)
        {
            return string.IsNullOrEmpty(input);
        }
        public static bool IsNullOrEmptyGuid(this Guid input)
        {
            if (input == Guid.Empty)
                return true;
            return false;
        }
    }

    public static class SecurityUtilities
    {
        public class ClientCacheEncrypter
        {
            private const string Enckey = "4038C253-85C4-436A-8775-C413DDBBE05C";//don't change this
            public static string EncryptString(string input)
            {
                var enc = new AnritsuEncrypter(Enckey);
                return enc.EncryptData(input);
            }
            public static string DecryptString(string encryptedValue)
            {
                var enc = new AnritsuEncrypter(Enckey);
                return enc.DecryptData(encryptedValue);
            }
        }

        public class AnritsuEncrypter
        {
            private const int MinKeyLength = 9;
            static private readonly byte[] Key = new Byte[8];
            static private readonly byte[] Iv = new Byte[8];
            static private string _keystring = string.Empty;//enter key //"E2BE0601-0AFE-4306-8FE3-FDC60CB3FD6C";

            public AnritsuEncrypter(string key)
            {
                _keystring = key;
            }
            public void SetEncryptionKeyByIntId(int valueId)
            {
                string segment = "_" + valueId;
                string key = segment;
                while (key.Length < MinKeyLength)
                {
                    key += segment;
                }
                _keystring = key;
            }
            /// <summary>
            /// Function to encrypt data
            /// string Length cannot exceed 92160 bytes.
            /// </summary>
            /// <param name="valueToEncrypt"></param>
            /// <returns></returns>
            public string EncryptData(string valueToEncrypt)
            {
                string mResultString;//Return Result
                //1. string Length cannot exceed 92160 bytes.
                //Otherwise, buffer will overflow.
                //See point 3 for reasons
                if (valueToEncrypt.Length > 92160)
                {
                    mResultString = "Error. Data string too large. Keep within 90Kb.";
                    return mResultString;
                }
                //2. Generate the Keys
                if (!InitKey())
                {
                    mResultString = "Error. Fail to generate key for encryption";
                    return mResultString;
                }
                //3. Prepare the string
                //The first 5 character of the string is formatted to store
                //the actual length of the data.
                //This is the simplest way to remember to original length of the data,
                //without resorting to complicated computations.
                valueToEncrypt = string.Format("{0,5:00000}" + valueToEncrypt,
                         valueToEncrypt.Length);
                //4. Encrypt the Data
                var mDataBytes = new byte[valueToEncrypt.Length];
                var mAsciiEnc = new ASCIIEncoding();
                //UTF8Encoding aEnc = new UTF8Encoding();
                mAsciiEnc.GetBytes(valueToEncrypt, 0, valueToEncrypt.Length, mDataBytes, 0);
                var mDesProvider = new DESCryptoServiceProvider();
                ICryptoTransform mDesEncrypt = mDesProvider.CreateEncryptor(Key, Iv);
                //5. Perpare the streams
                var mInputStream = new MemoryStream(mDataBytes);
                var mTransformStream = new CryptoStream(mInputStream,
                    mDesEncrypt, CryptoStreamMode.Read);
                var mOutputStream = new MemoryStream();
                //6. Start performing the encryption
                int mBytesRead;
                var mOutputBytes = new byte[1024];
                do
                {
                    mBytesRead = mTransformStream.Read(mOutputBytes, 0, 1024);
                    if (mBytesRead != 0)
                        mOutputStream.Write(mOutputBytes, 0, mBytesRead);
                }
                while (mBytesRead > 0);
                //7. Returns the encrypted result after it is base64 encoded
                //In this case, the actual result is converted to base64 so that
                //it can be transported over the HTTP protocol without deformation.
                if (mOutputStream.Length == 0)
                    mResultString = "";
                else
                    mResultString = Convert.ToBase64String(mOutputStream.GetBuffer(),
                        0, (int)mOutputStream.Length);
                return mResultString;
            }
            /// <summary>
            /// Function to decrypt data
            /// </summary>
            /// <param name="sStringToEncrypt"></param>
            /// <returns></returns>
            public string DecryptData(string valueToEncrypt)
            {
                string mResultString;
                //1. Generate the Key used for decrypting
                if (!InitKey())
                {
                    mResultString = "Error. Fail to generate key for decryption.";
                    return mResultString;
                }
                //2. Initialize the service provider
                var mDesProvider = new DESCryptoServiceProvider();
                ICryptoTransform mDesDecrypt = mDesProvider.CreateDecryptor(Key, Iv);
                //3. Prepare the streams
                var mOutputStream = new MemoryStream();
                var mTransformStream = new CryptoStream(mOutputStream, mDesDecrypt,
                    CryptoStreamMode.Write);
                //4. Remember to revert the base64 encoding into a byte array
                //to restore the original encrypted data stream
                byte[] mPlainBytes;
                try
                {
                    mPlainBytes = Convert.FromBase64CharArray(valueToEncrypt.ToCharArray(),
                        0, valueToEncrypt.Length);
                }
                catch (Exception)
                {
                    mResultString = "Error. Input Data is not base64 encoded.";
                    return mResultString;
                }
                long mRead = 0;
                long mTotal = valueToEncrypt.Length;
                try
                {
                    //5. Perform the actual decryption
                    while (mTotal >= mRead)
                    {
                        mTransformStream.Write(mPlainBytes, 0, mPlainBytes.Length);
                        //descsp.BlockSize=64
                        mRead = mOutputStream.Length + Convert.ToUInt32(
                            ((mPlainBytes.Length / mDesProvider.BlockSize) *
                            mDesProvider.BlockSize));
                    }
                    var mAsciiEnc = new ASCIIEncoding();
                    mResultString = mAsciiEnc.GetString(mOutputStream.GetBuffer(),
                        0, (int)mOutputStream.Length);
                    //6. Trim the string to return only the meaningful data
                    //Remember that in the encrypt function, the first 5 character
                    //holds the length of the actual data. This is the simplest way
                    //to remember to original length of the data, without resorting
                    //to complicated computations.
                    string mStringLength = mResultString.Substring(0, 5);
                    int mLen = Convert.ToInt32(mStringLength);
                    mResultString = mResultString.Substring(5, mLen);
                    //strResult = strResult.Remove(0,5);
                    return mResultString;
                }
                catch
                {
                    mResultString = "Error. Decryption Failed. Possibly due to incorrect Key"
                        + " or corrputed data";
                    return mResultString;
                }
            }
            /// <summary>
            /// Private function to generate the keys into member variables
            /// </summary>
            /// <returns></returns>
            static private bool InitKey()
            {
                try
                {
                    // Convert Key to byte array
                    var mKeyBytes = new byte[_keystring.Length];
                    var mAsciiEnc = new ASCIIEncoding();
                    mAsciiEnc.GetBytes(_keystring, 0, _keystring.Length, mKeyBytes, 0);
                    //Hash the key using SHA1
                    var mShaProvider = new SHA1CryptoServiceProvider();
                    byte[] mHashBytes = mShaProvider.ComputeHash(mKeyBytes);
                    int i;
                    // use the low 64-bits for the key value
                    for (i = 0; i < 8; i++)
                        Key[i] = mHashBytes[i];
                    for (i = 8; i < 16; i++)
                        Iv[i - 8] = mHashBytes[i];
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}

namespace Anritsu.WebApp.GlobalWeb.Constants.SsoSamlUtility.Utils
{    
        public static class ConvertUtility
        {
            public static String ConvertToString(object input, String defaultValue)
            {
                if (input == null || input == DBNull.Value) return defaultValue;
                try { return input.ToString(); }
                catch { return defaultValue; }
            }

            public static string ConvertNullToEmptyString(object input)
            {
                if (input == null || input == DBNull.Value || string.IsNullOrEmpty(input.ToString()))
                    return string.Empty;
                return input.ToString();
            }

            public static bool ConvertToBoolean(object input, bool defaultValue)
            {
                if (input == null || input == DBNull.Value) return defaultValue;
                try { return bool.Parse(input.ToString()); }
                catch { return defaultValue; }
            }

            public static Int32 ConvertToInt32(object input, Int32 defaultValue)
            {
                if (input == null || input == DBNull.Value) return defaultValue;
                try { return Convert.ToInt32(input); }
                catch { return defaultValue; }
            }

            public static Int64 ConvertToInt64(object input, Int64 defaultValue)
            {
                if (input == null || input == DBNull.Value) return defaultValue;
                try { return Convert.ToInt64(input); }
                catch { return defaultValue; }
            }

            public static Guid ConvertToGuid(object input, Guid defaultValue)
            {
                if (input == null || input == DBNull.Value) return defaultValue;
                try { return new Guid(input.ToString()); }
                catch { return defaultValue; }
            }

            public static DateTime ConvertToDateTime(object input, DateTime defaultValue)
            {
                if (input == null || input == DBNull.Value) return defaultValue;
                try { return Convert.ToDateTime(input.ToString()); }
                catch { return defaultValue; }
            }

            public static Byte[] ConvertToBlob(object input, Byte[] defaultValue)
            {
                if (input == null || input == DBNull.Value) return defaultValue;
                try { return (Byte[])input; }
                catch { return defaultValue; }
            }

            public static string ConvertToHtmlEncodedString(object input)
            {
                if (input == null || input == DBNull.Value) return string.Empty;
                return System.Web.HttpUtility.HtmlEncode(Convert.ToString(input));
            }

            public static object ConvertNullToDBNull(object input)
            {
                if (input == null) return DBNull.Value;
                return input;
            }

            public static String ConvertToQueryString(NameValueCollection nvc)
            {
                if (nvc == null || nvc.Count < 1) return string.Empty;
                var items = (from string key in nvc select String.Concat(key, "=", System.Web.HttpUtility.UrlEncode(nvc[key]))).ToList();
                if (items.Count < 1) return string.Empty;
                return "?" + String.Join("&", items.ToArray());
            }

            public static String ConvertToDelimitedString(List<String> lst, string delimiter)
            {
                if (lst == null || !lst.Any() || string.IsNullOrEmpty(delimiter)) return string.Empty;
                var sb = new StringBuilder();
                foreach (String s in lst) sb.AppendFormat("{0}{1}", s, delimiter);
                return sb.Remove(sb.Length - 1, 1).ToString();
            }
            public static String ConvertToFileName(object input, String defaultValue)
            {
                if (input == null || input == DBNull.Value) return defaultValue;
                try
                {
                    return System.IO.Path.GetFileName(input.ToString()).ToLower();
                }
                catch { return defaultValue; }
            }
        }   
}