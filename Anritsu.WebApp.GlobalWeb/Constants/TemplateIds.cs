﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.Constants
{
    public static class TemplateIds
    {
        public const string Product = "{03D9986F-E552-4432-9A94-A7E97D386B7A}";
        public const string LinkTemplate = "{002D8DBE-7058-4F1B-9787-7420A6A5A23E}";
        public const string TeaserTemplate = "{497FAD2E-BE97-46CF-9CD3-DDBC888D01B3}";
        public const string BucketTemplate = "{B2959F2A-334C-44CF-B6A6-3AADF99FFADC}";
        public const string HomeTemplate = "{9DE07373-5346-44ED-953A-E1E093EC5C17}";
        public const string NewsRelease = "{57A2D7A3-08D0-4A3B-971E-E22BB1D3F00C}";
        public const string IRNews = "{B24A5B8D-564D-45AA-8A07-E82497A47904}";
        public const string Events = "{34232019-CF1D-4141-9592-8576DF511A12}";
        public const string Library = "{98432994-EB94-4E10-BEFC-A00129E96091}"; 
        public const string ProductCategory="{8A045316-969A-4DAE-95BB-846CA22484F9}";
        public const string ProductSubcategory = "{CA06D3D1-8C1C-42A8-88DE-EBA404E7A723}";//{2AD9DA10-21A3-4D7B-8DF3-CE39B10FFA31}";
        public const string ProductFamily = "{0AC342A9-F49D-4B77-8032-5A2EF0A543ED}";
        public const string ProductRegion = "{3FA9B0BB-E7FB-4DA3-9835-8EF8ED8BD652}";
        public const string RedirectLink = "{2AD9DA10-21A3-4D7B-8DF3-CE39B10FFA31}";
        public const string ProductGlobal = "{94757055-E1B4-4B0D-B0E7-7BDBC26861D8}";
        public const string Video = "{77CE87EC-C909-4A42-9B05-6455F7976D39}";
        public const string News = "{57A2D7A3-08D0-4A3B-971E-E22BB1D3F00C}";
        public const string Announcement = "{B910F881-47A6-49C6-BB4F-0B5587E03CB0}";
        public const string YearFolder = "{3CAF38BE-A8DA-4073-884E-1CDB1F4A5F0E}";
        public const string Tradeshow = "{4A812CAD-1DF0-456E-B1DC-9C4BF8189352}";
        public const string Webinar = "{4BE1B2CA-2BD0-43C0-B6B5-6892CB9E5F79}";
        public const string TMEvent = "{508AE037-8CB8-4DA2-B425-0095E738395E}";
        public const string CorporateEvent = "{571B902F-D914-4961-9F9C-C0DE0118D389}";
        public const string Link = "{F1569CF8-9216-4B2D-8604-429BCC4ED617}";
        public const string ComponenetsAccessories = "{4D29DAAD-D6F3-4B50-B1CF-B279B47426CB}";
        public const string Downloads = "{0A13323A-10F2-404A-9AA2-7E0BD4D0EF78}";
        public const string SupportQuestions = "{B7BDD7DC-0624-4526-83AB-4702B625E71E}";
        public const string FrequentlyAskedQuestion = "{B7BDD7DC-0624-4526-83AB-4702B625E71E}";
        public const string TrainingCategory = "{F4065695-2FB2-44CF-9151-70B4ABCC2E72}";
        public const string TrainingCourse = "{F2C578CF-CA8E-44B0-970A-45F446775F81}";
        public const string DropListItem = "{F547E7B1-B7B9-41AD-8958-4756BD7A5633}";
        public const string Forms = "{EDB90620-2658-46BF-8C52-4DA2BF9AE056}";

        //For search metatags
        public const string ContactSupport = "{6B159384-3FEE-475E-BEBF-3D09C9EFC7AD}";
        public const string Script = "{48EFC40C-0979-4F0D-BD17-D759370820D8}";

        public const string TitleWithDescription = "{E280A6CF-E094-498A-A96E-E7032EB8FC9F}";
        public const string ProductDescriptionTab = "{BD387961-DFA7-4B0F-8893-A952AC77E16F}";

        //infivis
        public const string InfivisVideo = "{21FA5AFE-8B06-4E46-A4CB-C917C2B79DFD}";
        public const string InfivisProductFamilyStatic = "{3C47A664-F04F-4294-8416-931B5023597A}";
        public const string InfivisProductDetailStatic = "{A649E727-507F-48D3-A6F2-187931D2528C}";
        public const string InfivisAccordionPlusGridStatic= "{977AC54C-803F-46EF-A14F-68DEDCE6B16A}";
        public const string InfivisProductFamilyFeatureStatic = "{60486DA0-749C-4063-9F1A-584E334EEE1B}";
        public const string InfivisProductDetailStaticC1 = "{FCC3D75C-09A4-4A3B-B9A8-04697DD096CC}";

        public const string CategoryLink = "{204C570F-8BA4-45A7-83EC-0B7A299330E4}";
        public const string MegaMenuColumn = "{F547E7B1-B7B9-41AD-8958-4756BD7A5633}";
        public const string SubCategoryLink = "{F06A06AF-96D8-4666-BF4A-9C2E861F3635}";
        public const string GroupHeader = "{66D73396-7F7D-4348-A194-0C8474043809}";
    }
}