﻿using Sitecore.ContentSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch.Base
{
    public class BaseRepository
    {
        /// <summary>
        /// 
        /// </summary>
        protected IProviderSearchContext NewsAndEventsSearchContext
        {
            get { return ContentSearchManager.GetIndex("anritsu_news_and_events").CreateSearchContext(); }
        }

        /// <summary>
        /// 
        /// </summary>
        protected IProviderSearchContext DownloadsSearchContext
        {
            get { return ContentSearchManager.GetIndex("anritsu_downloads").CreateSearchContext(); }
        }

                /// <summary>
        /// 
        /// </summary>
        protected IProviderSearchContext FAQSearchContext
        {
            get { return ContentSearchManager.GetIndex("anritsu_faqs").CreateSearchContext(); }
        }
    }
}