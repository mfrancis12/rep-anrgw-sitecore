﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch.ComputedFields
{
    public class DynamicComputedField : AbstractComputedIndexField
    {
 
        private readonly XmlNode _configuration;

        public DynamicComputedField(XmlNode p_Configuration)
        {
            _configuration = p_Configuration;
        }


        /// <summary>
        /// entry point
        /// </summary>
        /// <param name="indexable"></param>
        /// <returns></returns>
        public override object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;
            if (item == null)
            {
                return null;
            }


            //this is the field that we are going to get our inital value from.
            string fieldName = GetConfigurationValue("sitecorefieldName");

            //this is the type of field we are going to cater for.
            string returnType = GetConfigurationValue("fieldReturnType");

            //optional property, if field value if empty then it will search this language for it.
            string fallbackLanguage = GetConfigurationValue("fallbackLanguage");

            //check that we have field name and return type
            if (string.IsNullOrEmpty(fieldName) || string.IsNullOrEmpty(returnType))
            {
                return null;
            }

            //if we are returning a string
            if (returnType.Equals("string", StringComparison.OrdinalIgnoreCase))
            {
                //simply return rawValue;
                string value = GetRawFieldValueOrFallback(item, fieldName, fallbackLanguage);

                if (!string.IsNullOrEmpty(value))
                {
                    return value;
                }
            }
            else if (returnType.Equals("date", StringComparison.OrdinalIgnoreCase))
            {
                var date = GetDateFieldValueOrFallback(item, fieldName, fallbackLanguage);

                if (date != null && !date.Equals(DateTime.MinValue) && !date.Equals(DateTime.MaxValue))
                {
                    return date;
                }
            }
            else if (returnType.Equals("boolean", StringComparison.OrdinalIgnoreCase))
            {
                var value = GetBooleanFieldValueOrFallback(item, fieldName, fallbackLanguage);

                if (value != null)
                {
                    return value;
                }
            }

            return null;
        }

        /// <summary>
        /// returns configuration attribute values
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <returns></returns>
        protected string GetConfigurationValue(string configurationKey)
        {
            string configurationValue = null;
            if (_configuration != null && _configuration.Attributes != null)
            {
                XmlAttribute configurationAttribute = _configuration.Attributes[configurationKey];
                if (configurationAttribute != null)
                {
                    configurationValue = configurationAttribute.Value;
                }
            }
            return configurationValue;
        }

        /// <summary>
        /// method will return the raw value of fieldName from the item if is empty will try and get the fieldName value from the item in the fallbackLangauge
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fieldName"></param>
        /// <param name="fallbackLanguage"></param>
        /// <returns></returns>
        private string GetRawFieldValueOrFallback(Item item, string fieldName, string fallbackLanguage)
        {
            string value = string.Empty;

            if (item == null || string.IsNullOrEmpty(fieldName))
            {
                return string.Empty;
            }

            value = item[fieldName];

            Language language;
            if (string.IsNullOrEmpty(value) && Sitecore.Globalization.Language.TryParse(fallbackLanguage, out language))
            {
                Item fallbackItem = item.Database.GetItem(item.ID, language);

                if (fallbackItem != null)
                {
                    value = fallbackItem[fieldName];
                }
            }

            return value;
        }


        /// <summary>
        /// method will return the date value of fieldName from the item if is empty will try and get the fieldName date value from the item in the fallbackLangauge
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fieldName"></param>
        /// <param name="fallbackLanguage"></param>
        /// <returns></returns>
        private DateTime GetDateFieldValueOrFallback(Item item, string fieldName, string fallbackLanguage)
        {
            DateTime date = DateTime.MinValue;

            if (item == null || string.IsNullOrEmpty(fieldName))
            {
                return DateTime.MinValue;
            }

            var field = ((Sitecore.Data.Fields.DateField)item.Fields[fieldName]);

            if (field == null)
            {
                return DateTime.MinValue;
            }

            date = field.DateTime;
            if (date != null && !date.Equals(DateTime.MinValue) && !date.Equals(DateTime.MaxValue))
            {
                return date;
            }

            Language language;
            if (Sitecore.Globalization.Language.TryParse(fallbackLanguage, out language))
            {
                Item fallbackItem = item.Database.GetItem(item.ID, language);

                if (fallbackItem != null)
                {
                    var fallbackField = ((Sitecore.Data.Fields.DateField)fallbackItem.Fields[fieldName]);

                    if (fallbackField != null)
                    {
                        return fallbackField.DateTime;
                    }
                }
            }

            return DateTime.MinValue;
        }

        /// <summary>
        /// method will return the date value of fieldName from the item if is empty will try and get the fieldName date value from the item in the fallbackLangauge
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fieldName"></param>
        /// <param name="fallbackLanguage"></param>
        /// <returns></returns>
        private bool? GetBooleanFieldValueOrFallback(Item item, string fieldName, string fallbackLanguage)
        {
            //if we have an item
            if (item == null || string.IsNullOrEmpty(fieldName))
            {
                return false;
            }

            //try get field
            var field = ((Sitecore.Data.Fields.CheckboxField)item.Fields[fieldName]);

            //check if field exsits
            if (field == null)
            {
                //if it does not return null (returning null should not put the field in the index) 
                return null;
            }

            //if field is checked
            if (field.Checked)
            {
                return field.Checked;
            }

            //we are here because the field is not checked
            //try parse fallback language
            Language language;
            if (Sitecore.Globalization.Language.TryParse(fallbackLanguage, out language))
            {
                //try get fall back item
                Item fallbackItem = item.Database.GetItem(item.ID, language);

                //if we have fallback item
                if (fallbackItem != null)
                {
                    //try and get field from item
                    var fallbackField = ((Sitecore.Data.Fields.CheckboxField)fallbackItem.Fields[fieldName]);

                    //check we have field
                    if (fallbackField != null)
                    {
                        return fallbackField.Checked;
                    }
                }
            }

            return null;
        }
    }
}
