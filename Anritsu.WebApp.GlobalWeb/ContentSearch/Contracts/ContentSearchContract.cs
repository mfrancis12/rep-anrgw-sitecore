﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts
{
    public class ContentSearchContract
    {
        public ContentSearchContract()
        {
            this.TemplateRestrictions = new List<String>();
            this.RootRestrictions = new List<ID>();
            this.CheckShowOnHome = false;
        }

        public List<String> TemplateRestrictions { get; set; }

        public List<ID> RootRestrictions { get; set; }

        public bool CheckShowOnHome { get; set; }

    }
}