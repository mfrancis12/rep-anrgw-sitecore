﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts
{
    public class EventsSearchContract : ContentSearchContract
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}