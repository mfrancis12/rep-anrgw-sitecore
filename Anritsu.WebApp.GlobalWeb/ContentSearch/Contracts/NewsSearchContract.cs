﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts
{
    public class NewsSearchContract : ContentSearchContract
    {
        public DateTime StartReleaseDate { get; set; }

        public DateTime EndReleaseDate { get; set; }
    }
}