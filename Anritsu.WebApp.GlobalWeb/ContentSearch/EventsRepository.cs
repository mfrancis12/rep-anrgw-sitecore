﻿using Anritsu.WebApp.GlobalWeb.ContentSearch.Base;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Anritsu.WebApp.GlobalWeb.ContentSearch.ResultItems;
using Sitecore;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch
{
    public class EventsRepository : BaseRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        public SearchResults<EventsResultItem> SearchEventsByContract(EventsSearchContract contract, int take = 0, int skip = 0)
        {
            var predicate = PredicateBuilder.True<EventsResultItem>();

            //will restrict results by provided tempalte types
            if (contract.TemplateRestrictions.Any())
            {
                var templates = PredicateBuilder.False<EventsResultItem>();

                foreach (var t in contract.TemplateRestrictions)
                {
                    ID id;
                    if (ID.TryParse(t, out id))
                    {
                        templates = templates.Or(r => r.TemplateId == id);
                    }
                }

                predicate = predicate.And(templates);
            }

            //will restrict results by provided root ids
            if (contract.RootRestrictions.Any())
            {
                var rootRestrictions = PredicateBuilder.False<EventsResultItem>();

                foreach (var rr in contract.RootRestrictions)
                {
                    if (!ID.IsNullOrEmpty(rr))
                    {
                        rootRestrictions = rootRestrictions.Or(r => r.Paths.Contains(rr));
                    }
                }

                predicate = predicate.And(rootRestrictions);
            }

            if (contract.StartDate != DateTime.MinValue && contract.EndDate != DateTime.MinValue)
            {
                //AW only start date for events. no end date
                predicate = predicate.And(d => d.StartDate >= contract.StartDate.ToUniversalTime() && d.StartDate <= contract.EndDate.ToUniversalTime());
            }

            if (contract.CheckShowOnHome)
            {
                predicate = predicate.And(x => x.ShowOnHome);
            }

            predicate = predicate.And(l => l.Language == Sitecore.Context.Language.Name);

            predicate = predicate.And(v => v.LatestVersion);

            if (take > 0)
            {
                return NewsAndEventsSearchContext
                     .GetQueryable<EventsResultItem>()
                     .Where(predicate)
                     .OrderByDescending(r => r.StartDate)
                     .Skip(skip)
                     .Take(take)
                     .GetResults();
            }
            else
            {
                return NewsAndEventsSearchContext
                     .GetQueryable<EventsResultItem>()
                     .Where(predicate)
                     .OrderByDescending(r => r.StartDate)
                     .GetResults();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        public SearchResults<EventsResultItem> SearchEventsByContractForTab(EventsSearchContract contract, int take = 0, int skip = 0)
        {
            var predicate = PredicateBuilder.True<EventsResultItem>();

            //will restrict results by provided tempalte types
            if (contract.TemplateRestrictions.Any())
            {
                var templates = PredicateBuilder.False<EventsResultItem>();

                foreach (var t in contract.TemplateRestrictions)
                {
                    ID id;
                    if (ID.TryParse(t, out id))
                    {
                        templates = templates.Or(r => r.TemplateId == id);
                    }
                }

                predicate = predicate.And(templates);
            }

            //will restrict results by provided root ids
            if (contract.RootRestrictions.Any())
            {
                var rootRestrictions = PredicateBuilder.False<EventsResultItem>();

                foreach (var rr in contract.RootRestrictions)
                {
                    if (!ID.IsNullOrEmpty(rr))
                    {
                        rootRestrictions = rootRestrictions.Or(r => r.Paths.Contains(rr));
                    }
                }

                predicate = predicate.And(rootRestrictions);
            }

            if (contract.CheckShowOnHome)
            {
                predicate = predicate.And(x => x.ShowOnHome);
            }
            
            
            predicate = predicate.And(x => x.EndDate >= DateTime.Now.Date.ToUniversalTime());

            predicate = predicate.And(l => l.Language == Sitecore.Context.Language.Name);

            predicate = predicate.And(v => v.LatestVersion);

            if (take > 0)
            {
                return NewsAndEventsSearchContext
                     .GetQueryable<EventsResultItem>()
                     .Where(predicate)
                     .OrderBy(r => r.StartDate)
                     .Skip(skip)
                     .Take(take)
                     .GetResults();
            }
            else
            {
                return NewsAndEventsSearchContext
                     .GetQueryable<EventsResultItem>()
                     .Where(predicate)
                     .OrderBy(r => r.StartDate)
                     .GetResults();
            }

        }

    }
}