﻿using Anritsu.WebApp.GlobalWeb.ContentSearch.Base;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Anritsu.WebApp.GlobalWeb.ContentSearch.ResultItems.Base;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch
{
    public class GeneralRepository : BaseRepository
    {
        public SearchResults<GeneralResultItem> SearchByContract(ContentSearchContract contract)
        {
            var predicate = PredicateBuilder.True<GeneralResultItem>();

            //will restrict results by provided tempalte types
            if (contract.TemplateRestrictions.Any())
            {
                var templates = PredicateBuilder.False<GeneralResultItem>();

                foreach (var t in contract.TemplateRestrictions)
                {
                    ID id;
                    if (ID.TryParse(t, out id))
                    {
                        templates = templates.Or(r => r.TemplateId == id);
                    }
                }

                predicate = predicate.And(templates);
            }

            //will restrict results by provided root ids
            if (contract.RootRestrictions.Any())
            {
                var rootRestrictions = PredicateBuilder.False<GeneralResultItem>();

                foreach (var rr in contract.RootRestrictions)
                {
                    if (!ID.IsNullOrEmpty(rr))
                    {
                        rootRestrictions = rootRestrictions.Or(r => r.Paths.Contains(rr));
                    }
                }

                predicate = predicate.And(rootRestrictions);
            }

            predicate = predicate.And(l => l.Language == Sitecore.Context.Language.Name);

            predicate = predicate.And(v => v.LatestVersion);
            //predicate = predicate.And(i=>i.)

            return NewsAndEventsSearchContext
                        .GetQueryable<GeneralResultItem>()
                        .Where(predicate)
                        .GetResults();
        }

        public SearchResults<GeneralResultItem> SearchDownloadsByContract(ContentSearchContract contract)
        {
            var predicate = PredicateBuilder.True<GeneralResultItem>();

            //will restrict results by provided tempalte types
            if (contract.TemplateRestrictions.Any())
            {
                var templates = PredicateBuilder.False<GeneralResultItem>();

                foreach (var t in contract.TemplateRestrictions)
                {
                    ID id;
                    if (ID.TryParse(t, out id))
                    {
                        templates = templates.Or(r => r.TemplateId == id);
                    }
                }

                predicate = predicate.And(templates);
            }

            //will restrict results by provided root ids
            if (contract.RootRestrictions.Any())
            {
                var rootRestrictions = PredicateBuilder.False<GeneralResultItem>();

                foreach (var rr in contract.RootRestrictions)
                {
                    if (!ID.IsNullOrEmpty(rr))
                    {
                        rootRestrictions = rootRestrictions.Or(r => r.Paths.Contains(rr));
                    }
                }

                predicate = predicate.And(rootRestrictions);
            }

            predicate = predicate.And(l => l.Language == Sitecore.Context.Language.Name);

            predicate = predicate.And(v => v.LatestVersion);
            //predicate = predicate.And(i=>i.)

            return DownloadsSearchContext
                        .GetQueryable<GeneralResultItem>()
                        .Where(predicate)
                        .GetResults();
        }

        public SearchResults<GeneralResultItem> SearchFAQsByContract(ContentSearchContract contract)
        {
            var predicate = PredicateBuilder.True<GeneralResultItem>();

            //will restrict results by provided tempalte types
            if (contract.TemplateRestrictions.Any())
            {
                var templates = PredicateBuilder.False<GeneralResultItem>();

                foreach (var t in contract.TemplateRestrictions)
                {
                    ID id;
                    if (ID.TryParse(t, out id))
                    {
                        templates = templates.Or(r => r.TemplateId == id);
                    }
                }

                predicate = predicate.And(templates);
            }

            //will restrict results by provided root ids
            if (contract.RootRestrictions.Any())
            {
                var rootRestrictions = PredicateBuilder.False<GeneralResultItem>();

                foreach (var rr in contract.RootRestrictions)
                {
                    if (!ID.IsNullOrEmpty(rr))
                    {
                        rootRestrictions = rootRestrictions.Or(r => r.Paths.Contains(rr));
                    }
                }

                predicate = predicate.And(rootRestrictions);
            }

            if (contract.CheckShowOnHome)
            {
                predicate = predicate.And(x => x.ShowOnHome);
            }

            predicate = predicate.And(l => l.Language == Sitecore.Context.Language.Name);

            predicate = predicate.And(v => v.LatestVersion);
            //predicate = predicate.And(i=>i.)

            return DownloadsSearchContext
                        .GetQueryable<GeneralResultItem>()
                        .Where(predicate)
                        .GetResults();
        }
    }
}