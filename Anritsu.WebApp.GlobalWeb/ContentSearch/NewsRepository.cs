﻿using Anritsu.WebApp.GlobalWeb.ContentSearch.Base;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Anritsu.WebApp.GlobalWeb.ContentSearch.ResultItems;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch
{
    public class NewsRepository : BaseRepository
    {
        public SearchResults<NewsResultItem> SearchNewsByContract(NewsSearchContract contract, int take = 0, int skip = 0, bool searchContextLanguage = true)
        {
            var predicate = PredicateBuilder.True<NewsResultItem>();

            //will restrict results by provided tempalte types
            if (contract.TemplateRestrictions.Any())
            {
                var templates = PredicateBuilder.False<NewsResultItem>();

                foreach (var t in contract.TemplateRestrictions)
                {
                    ID id;
                    if (ID.TryParse(t, out id))
                    {
                        templates = templates.Or(r => r.TemplateId == id);
                    }
                }

                predicate = predicate.And(templates);
            }

            //will restrict results by provided root ids
            if (contract.RootRestrictions.Any())
            {
                var rootRestrictions = PredicateBuilder.False<NewsResultItem>();

                foreach (var rr in contract.RootRestrictions)
                {
                    if (!ID.IsNullOrEmpty(rr))
                    {
                        rootRestrictions = rootRestrictions.Or(r => r.Paths.Contains(rr));
                    }
                }

                predicate = predicate.And(rootRestrictions);
            }

            if (contract.StartReleaseDate != DateTime.MinValue && contract.EndReleaseDate != DateTime.MinValue)
            {
                predicate = predicate.And(d => d.ReleaseDate >= contract.StartReleaseDate.ToUniversalTime() && d.ReleaseDate <= contract.EndReleaseDate.ToUniversalTime());
            }

            if (contract.CheckShowOnHome)
            {
                predicate = predicate.And(x => x.ShowOnHome);
            }

            if (searchContextLanguage)
            {
                predicate = predicate.And(l => l.Language == Sitecore.Context.Language.Name);
            }

            predicate = predicate.And(v => v.LatestVersion);

            if (take > 0)
            {
                return NewsAndEventsSearchContext
                     .GetQueryable<NewsResultItem>()
                     .Where(predicate)
                     .OrderByDescending(r => r.ReleaseDate)
                     .Skip(skip)
                     .Take(take)
                     .GetResults();
            }
            else
            {
                return NewsAndEventsSearchContext
                     .GetQueryable<NewsResultItem>()
                     .Where(predicate)
                     .OrderByDescending(r => r.ReleaseDate)
                     .GetResults();
            }


        }
    }
}