﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch.ResultItems.Base
{
    public class GeneralResultItem : SearchResultItem
    {
        [IndexField("_latestversion")]
        public virtual bool LatestVersion { get; set; }

        [IndexField("showonhome_computed")]
        public virtual bool ShowOnHome { get; set; }
    }
}