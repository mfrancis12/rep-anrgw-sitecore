﻿using Anritsu.WebApp.GlobalWeb.ContentSearch.ResultItems.Base;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch.ResultItems
{
    public class EventsResultItem : GeneralResultItem
    {
        [IndexField("start_date_computed")]
        public virtual DateTime StartDate { get; set; }

        [IndexField("end_date_computed")]
        public virtual DateTime EndDate { get; set; }
    }
}