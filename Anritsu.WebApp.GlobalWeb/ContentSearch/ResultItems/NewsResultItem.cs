﻿using Anritsu.WebApp.GlobalWeb.ContentSearch.ResultItems.Base;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.ContentSearch.ResultItems
{
    public class NewsResultItem : GeneralResultItem
    {
        [IndexField("release_date_computed")]
        public virtual DateTime ReleaseDate { get; set; }
    }
}