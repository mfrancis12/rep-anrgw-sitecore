﻿using Sitecore.Diagnostics;
using Sitecore.Shell.Framework.Commands;

namespace Anritsu.WebApp.GlobalWeb.CustomApps.LinkManagement
{
    /// <summary>
    /// Command class to manage links
    /// </summary>
    public class ManageLinks : Command
    {
        /// <summary>
        /// Overriding Execute method to start the dialog
        /// </summary>
         ///<param name="context"></param>
        public override void Execute(CommandContext context)
        {
            Assert.ArgumentNotNull(context, "CommandContext");

            if (context.Items.Length == 1)
            {
                var item = context.Items[0];

                var parameters = new System.Collections.Specialized.NameValueCollection();

                parameters["id"] = item.ID.ToString();

                Sitecore.Context.ClientPage.Start(this, "Run", parameters);
            }
        }

        /// <summary>
        /// Method to trigger the dialog
        /// </summary>
         ///<param name="args"></param>
        protected void Run(Sitecore.Web.UI.Sheer.ClientPipelineArgs args)
        {
            Sitecore.Text.UrlString url = new Sitecore.Text.UrlString("/sitecore/client/Your Apps/Speak UI/LinkManagement/LinkDialog");
            url.Append("id", args.Parameters["id"]);
            Sitecore.Context.ClientPage.ClientResponse.ShowModalDialog(url.ToString());
        }
    }
}