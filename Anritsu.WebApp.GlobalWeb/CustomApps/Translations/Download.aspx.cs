﻿using System;
using System.Net;
using System.Web;
using System.IO;

namespace Anritsu.WebApp.GlobalWeb.CustomApps.Translations
{
    /// <summary>
    /// Which is used to create and download the file in zip.
    /// </summary>
    public partial class Download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// Download translated zip file
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">event</param>
        protected void Download_Click(object sender, EventArgs e)
        {
            string fileName = Request.QueryString["fname"];
            if (!string.IsNullOrEmpty(fileName))
            {
                string path = Server.MapPath("~/TranslatableFiles/") + fileName + ".zip";
                if (File.Exists(path))
                    {
                        DownloadFile(fileName);
                    }
            }
        }
        public void DownloadFile(string fileName)
        {
            using (WebClient request = new WebClient())
            {
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + ".zip");
                byte[] data = request.DownloadData(Server.MapPath("~/TranslatableFiles/") + fileName + ".zip");
                response.BinaryWrite(data);
                response.End();
            }
        }
    }
}