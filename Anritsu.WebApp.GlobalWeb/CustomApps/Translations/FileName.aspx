﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileName.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.CustomApps.Translations.FileName" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="shortcut icon" href="/sitecore/images/favicon.ico" />
    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Default.css" />

    <%--    <link rel="stylesheet" href="/sitecore/shell/controls/Lib/Flexie/flex.css" />

    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Shell.css" />

    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Startbar.css" />--%>
</head>
<body>
    <form id="form1" runat="server">

         

        <div class="scStretch">
            <div class="scWizardPageContainer">
                <div id="Languages" class="scStretch scFlexColumnContainerWithoutFlexie" style="">
                    <div class="scWizardHeader">
                        <img border="0" alt="" src="/temp/IconCache/Network/32x32/Earth_Location.png" />
                        <div>
                            <div class="scWizardTitle">Enter File Name</div>
                            <div class="scWizardText">
                                Enter the name of the  file to export. Click Next to continue.<br />
                            </div>
                        </div>
                    </div>
                    <div class="scBottomEdge"></div>
                    <div class="scTopEdge"></div>
                    <div class="scWizardFormContentForOldIE scFlexContentWithoutFlexie">
                        <div class="scStretchAbsolute" style="padding: 8px 8px 8px 32px">
                            
                            <div style="padding: 0px 0px 2px 0px;">
                                Enter the name of the file:
                            </div>
                            <div style="padding: 0px 0px 16px 0px;">
                                <asp:TextBox ID="EnterFileName" runat="server"></asp:TextBox>
                            </div>
                            <asp:Label ID="Message" runat="server" Style="color: red;"></asp:Label><br />
                        </div>
                    </div>
                </div>

            </div>
            <div class="scWizardButtons">
                <div class="scBottomEdge"></div>
                <div class="scTopEdge"></div>
                <asp:Button ID="Next" runat="server" CssClass="scButton" Text="Next" OnClick="Next_Click" Width="100" />

            </div>
        </div>


    </form>
</body>
</html>
