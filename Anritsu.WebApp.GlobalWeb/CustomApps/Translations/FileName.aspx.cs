﻿using System;
using Anritsu.WebApp.SitecoreUtilities.Translations;
using System.IO.Compression;
using Sitecore.Data.Items;
using System.Collections.ObjectModel;
using System.Xml;


namespace Anritsu.WebApp.GlobalWeb.CustomApps.Translations
{
    public partial class FileName : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            SelectLanguageItems selectedItems = (SelectLanguageItems)base.Session["SetLanguagesandItems"];
            CreateFile(EnterFileName.Text, selectedItems);
        }
        private void CreateFile(string fileName, SelectLanguageItems selectedItems)
        {                     
            if (!string.IsNullOrEmpty(fileName))
            {               
                if (selectedItems.TargetLanguages.Count > 0 && selectedItems.SelectedItems.Count > 0)
                {
                    //foreach (Item itm in selectedItems.SelectedItems)
                    //{
                    //    foreach (string targetLanguage in selectedItems.TargetLanguages)
                    //    {
                    //        Collection<string> sourceandTarget = new Collection<string>();
                    //        sourceandTarget.Add(selectedItems.SourceLanguage);
                    //        sourceandTarget.Add(targetLanguage);
                    //        XmlDocument doc = ExportItems.GetItemsAsXml(itm, sourceandTarget, selectedItems.SourceLanguage);
                    //        System.IO.Directory.CreateDirectory(Server.MapPath("~/TranslatableFiles/") + fileName);
                    //        string filePath = Server.MapPath("~/TranslatableFiles/") + fileName;
                    //        doc.Save(filePath + "/" + itm.Name + "_" + targetLanguage + ".");
                    //    }
                    //}
                    //foreach (Item itm in selectedItems.SelectedItems)
                    //{
                    foreach (string targetLanguage in selectedItems.TargetLanguages)
                    {
                        Collection<string> sourceandTarget = new Collection<string>();
                        sourceandTarget.Add(selectedItems.SourceLanguage);
                        sourceandTarget.Add(targetLanguage);
                        XmlDocument doc = ExportItems.GetItemsAsXml(selectedItems.SelectedItems, sourceandTarget, selectedItems.SourceLanguage);
                        System.IO.Directory.CreateDirectory(Server.MapPath("~/TranslatableFiles/") + fileName);
                        string filePath = Server.MapPath("~/TranslatableFiles/") + fileName;
                        doc.Save(filePath + "/" + fileName+"_" + targetLanguage + ".");
                    }
                    //}
                    ZipFile.CreateFromDirectory(Server.MapPath("~/TranslatableFiles/") + fileName, Server.MapPath("~/TranslatableFiles/") + fileName + ".Zip");
                    System.IO.Directory.Delete(Server.MapPath("~/TranslatableFiles/") + fileName, true);
                }
                Response.Redirect("/GlobalWeb/CustomApps/Translations/Download.aspx?fname=" + fileName);
            }
            else
            {
                Message.Text = "Please enter file name.";
            }
        }
    }
}