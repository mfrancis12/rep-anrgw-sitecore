﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Xml;
using Sitecore;
using Sitecore.Jobs;
using Sitecore.Shell.Applications.Globalization.ImportLanguage;
using Sitecore.Xml;

namespace Anritsu.WebApp.GlobalWeb.CustomApps.Translations
{
    public partial class Import : System.Web.UI.Page
    {
        private readonly Sitecore.Data.Database _master = Sitecore.Configuration.Factory.GetDatabase("master");
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            var sb = new StringBuilder();
            if (fileUpload.HasFile)
            {
                try
                {
                    sb.AppendFormat("Uploading file: {0}", fileUpload.FileName);
                    //// saving the file
                    fileUpload.SaveAs(Server.MapPath("~/TranslatableFiles/") + fileUpload.FileName);
                    //// Showing the file information
                    sb.AppendFormat("<br/> Save As: {0}", fileUpload.PostedFile.FileName);
                    sb.AppendFormat("<br/> File type: {0}", fileUpload.PostedFile.ContentType);
                    sb.AppendFormat("<br/> File length: {0}", fileUpload.PostedFile.ContentLength);
                    sb.AppendFormat("<br/> File name: {0}", fileUpload.PostedFile.FileName);
                    FileName.Text = Path.GetFileNameWithoutExtension(fileUpload.PostedFile.FileName);
                }
                catch (Exception ex)
                {
                    sb.Append("<br/> Error <br/>");
                    sb.AppendFormat("Unable to save file <br/> {0}", ex.Message);
                }
            }
            else
            {
                Message.Text = sb.ToString();
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(FileName.Text))
                {
                    if (Directory.Exists(Server.MapPath("~/TranslatableFiles/") + FileName.Text))
                    {
                        Directory.Delete(Server.MapPath("~/TranslatableFiles/") + FileName.Text, true);
                    }

                    ZipFile.ExtractToDirectory(Server.MapPath("~/TranslatableFiles/") + FileName.Text + ".zip", Server.MapPath("~/TranslatableFiles/") + FileName.Text);
                    string[] filesPath = Directory.GetFiles(Server.MapPath("~/TranslatableFiles/") + FileName.Text);
                    foreach (string path in filesPath)
                    {
                        var selectedLanguageNames = RenderLanguages(path);
                        if (selectedLanguageNames.Count > 0)
                        {
                            var jobOptions = new JobOptions("ImportLanguage", "ImportLanguage", Client.Site.Name, new ImportLanguageForm.Importer(_master.Name, path, selectedLanguageNames), "Import")
                            {
                                ContextUser = Sitecore.Context.User
                            };
                            jobOptions.AfterLife = TimeSpan.FromMinutes(1.0);
                            JobManager.Start(jobOptions);
                        }
                    }
                }
                else
                {
                    Message.Text = "Please select a file to import.";
                }

                Response.Redirect("~/GlobalWeb/CustomApps/Translations/Success.aspx");
            }
            catch (Exception)
            {
                Message.Text = "Error:While uploading file.";
            }
        }

        private List<string> RenderLanguages(string filename)
        {
            var langugeNodes = new List<string>();
            XmlDocument xmlDocument;
            try
            {
                xmlDocument = XmlUtil.LoadXmlFile(filename);
            }
            catch (XmlException)
            {
                return langugeNodes;
            }
            
            XmlNodeList xmlNodeList = xmlDocument.SelectNodes("/sitecore/phrase");
            if (xmlNodeList == null)
            {
                return langugeNodes;
            }
          
            XmlNode xmlNode = xmlNodeList.Cast<XmlNode>().FirstOrDefault(xmlNode2 => xmlNode2.HasChildNodes);

            if (xmlNode == null)
            {
                return langugeNodes;
            }

            langugeNodes.AddRange(xmlNode.ChildNodes.Cast<XmlNode>().Select(xmlNode3 => xmlNode3.LocalName).Where(localName => !string.IsNullOrEmpty(localName)));

            langugeNodes.RemoveAt(0);
            return langugeNodes;
        }
    }
}