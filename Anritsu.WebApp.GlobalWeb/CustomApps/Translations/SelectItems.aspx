﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectItems.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.CustomApps.Translations.SelectItems" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="shortcut icon" href="/sitecore/images/favicon.ico" />
    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Default.css" />

    <link rel="stylesheet" href="/sitecore/shell/controls/Lib/Flexie/flex.css" />

    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Shell.css" />

    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Startbar.css" />
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        
        <div class="scStretch">
            <div class="scWizardPageContainer">
                <div id="Languages" class="scStretch scFlexColumnContainerWithoutFlexie" style="">
                    <div class="scWizardHeader">
                        <img border="0" alt="" src="/temp/IconCache/Network/32x32/Earth_Location.png" />
                        <div>
                            <div class="scWizardTitle">Select Items</div>
                            <div class="scWizardText">Select the items to export. Click Next to continue.<br /></div>
                        </div>
                    </div>
                    <div class="scBottomEdge"></div>
                    <div class="scTopEdge"></div>
                    Enter Template ID:<asp:TextBox ID="textTemplateID" runat="server"></asp:TextBox>
                    <div class="scWizardFormContentForOldIE scFlexContentWithoutFlexie">
                        <div class="scStretchAbsolute" style="padding: 8px 8px 8px 32px">
                            <fieldset id="LanguagesPanel" style="margin: 0px 0px 16px 0px;">
                                <legend id="LanguagesPanellegend">Select Items</legend>
                               <%-- <div id="LanguageList" style="padding: 4px;">--%>
                                <asp:CheckBox ID="SelectAll" runat="server" Text="Select All" />
                                     <asp:TreeView ID="contentTreeList" runat="server" ShowCheckBoxes="All"></asp:TreeView>
                                <%--</div>--%>
                            </fieldset>
                        
                           <asp:Label ID="Message" runat="server" Style="color: red;"></asp:Label><br />

                        </div>
                    </div>
                </div>
            </div>
            <div class="scWizardButtons">
                <div class="scBottomEdge"></div>
                <div class="scTopEdge"></div>
                <asp:Button ID="Next" runat="server" CssClass="scButton" Text="Next" OnClick="Next_Click" Width="100" />
                
            </div>
        </div>
    </form>
    <script>
        $(function () {
            $("#SelectAll").on("click", function () {
                var  $contentTreeCheckBoxList = $("#contentTreeList").find("input:checkbox");
                if ($(this).is(":checked")) {
                    $contentTreeCheckBoxList.prop("checked",true);
                } else {
                    $contentTreeCheckBoxList.prop("checked",false);
                }
            });
        });
    </script>
</body>
</html>
