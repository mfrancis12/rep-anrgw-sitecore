﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Anritsu.WebApp.SitecoreUtilities.Translations;
using System.Collections.Generic;


namespace Anritsu.WebApp.GlobalWeb.CustomApps.Translations
{
    public partial class SelectItems : System.Web.UI.Page
    {
        private readonly Sitecore.Data.Database _master = Sitecore.Configuration.Factory.GetDatabase("master");

        private Collection<Item> GetSelectedItems
        {
            get
            {
                var listItems = new Collection<Item>();
                foreach (TreeNode node in contentTreeList.CheckedNodes)
                {
                    listItems.Add(_master.GetItem(node.Value));
                }

                return listItems;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    BindTreeViewControl(_master.GetItem(Request.QueryString["id"]));
                }
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            var selectedItems = (SelectLanguageItems)base.Session["SetLanguagesandItems"];
            selectedItems.SelectedItems = new Collection<Item>();
            if (GetSelectedItems.Count > 0)
            {
                foreach (var itm in GetSelectedItems)
                {
                    if (itm.TemplateID.ToString().Equals(textTemplateID.Text.Trim()))
                    {
                        selectedItems.SelectedItems.Add(itm);
                    }
                }
                //selectedItems.SelectedItems=selectedItems.SelectedItems.Where(x => x.TemplateID.ToString().Equals("SelectedTemplateID"));
                base.Session["SetLanguagesandItems"] = selectedItems;
                Response.Redirect("/GlobalWeb/CustomApps/Translations/FileName.aspx");
            }
            else
            {
                Message.Text = "Select at least one item.";
            }
        }

        private void BindTreeViewControl(Item item)
        {
            var root = new TreeNode(item.Name);
            root.SelectAction = TreeNodeSelectAction.None;
            root.Value = item.ID.ToString();
            root.Collapse();
            contentTreeList.Nodes.Add(root);

            var lstItems = item.Children.ToList();

            foreach (var childItem in lstItems)
            {
                TreeNode croot = new TreeNode(childItem.Name);
                croot.SelectAction = TreeNodeSelectAction.None;
                croot.Value = childItem.ID.ToString();
                croot.Collapse();
                if (childItem.HasChildren)
                {
                    CreateNode(croot, childItem);
                }

                root.ChildNodes.Add(croot);
            }
        }

        private void CreateNode(TreeNode node, Item childItem)
        {
            List<Item> listItems = childItem.Children.ToList();
            foreach (Item itm in listItems)
            {
                TreeNode childnode = new TreeNode(!string.IsNullOrEmpty(Convert.ToString(itm.Fields[Sitecore.FieldIDs.DisplayName])) ? Convert.ToString(itm.Fields[Sitecore.FieldIDs.DisplayName]) : itm.Name);
                childnode.Value = itm.ID.ToString();
                childnode.SelectAction = TreeNodeSelectAction.None;
                childnode.Collapse();
                node.ChildNodes.Add(childnode);
                if (itm.HasChildren)
                {
                    CreateNode(childnode, itm);
                }
            }
        }

        //protected void SelectAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (SelectAll.Checked)
        //    {
        //        foreach (TreeNode treenodes in contentTreeList.Nodes)
        //        {
        //            treenodes.Checked = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (TreeNode treenodes in contentTreeList.Nodes)
        //        {
        //            treenodes.Checked = false;
        //        }
        //    }
        //}        
    }
}