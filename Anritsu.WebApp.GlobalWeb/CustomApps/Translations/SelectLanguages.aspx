﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectLanguages.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.CustomApps.Translations.SelectLanguages" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="shortcut icon" href="/sitecore/images/favicon.ico" />
    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Default.css" />

    <link rel="stylesheet" href="/sitecore/shell/controls/Lib/Flexie/flex.css" />

    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Shell.css" />

    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Startbar.css" />


</head>
<body>
    <form id="form1" runat="server">
        <div class="scStretch">
            <div class="scWizardPageContainer">
                <div id="Languages" class="scStretch scFlexColumnContainerWithoutFlexie" style="">
                    <div class="scWizardHeader">
                        <img border="0" alt="" src="/temp/IconCache/Network/32x32/Earth_Location.png" />
                        <div>
                            <div class="scWizardTitle">Select Languages</div>
                            <div class="scWizardText">Select the languages you wish to export. Click Next to continue.</div>
                        </div>
                    </div>
                    <div class="scBottomEdge"></div>
                    <div class="scTopEdge"></div>
                    <div class="scWizardFormContentForOldIE scFlexContentWithoutFlexie">
                        <div class="scStretchAbsolute" style="padding: 8px 8px 8px 32px">
                            <fieldset id="LanguagesPanel" style="margin: 0px 0px 16px 0px;">
                                <legend id="LanguagesPanellegend">Select Source Language</legend>
                                <div id="LanguageList" style="padding: 4px;">
                                    <asp:DropDownList ID="SourceLanguages" runat="server" OnSelectedIndexChanged="SourceLanguages_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Select Target Languages</legend>
                                <asp:Repeater ID="TargetLanguages" runat="server">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="TargetLanguage" runat="server" Text="<%#Container.DataItem %>" />
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </fieldset>
                            <asp:Label ID="Message" runat="server" Style="color: red"></asp:Label><br />

                        </div>
                    </div>
                </div>
            </div>
            <div class="scWizardButtons">
                <div class="scBottomEdge"></div>
                <div class="scTopEdge"></div>
                <asp:Button ID="Next" runat="server" CssClass="scButton" Text="Next" OnClick="Next_Click" Width="100" />
            </div>
        </div>
    </form>
</body>
</html>
