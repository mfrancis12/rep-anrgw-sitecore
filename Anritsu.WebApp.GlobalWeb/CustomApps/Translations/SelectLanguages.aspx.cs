﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.UI.WebControls;
using Anritsu.WebApp.SitecoreUtilities.Translations;


namespace Anritsu.WebApp.GlobalWeb.CustomApps.Translations
{
    public partial class SelectLanguages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    SourceLanguages.DataSource = ExportItems.GetLanguageCodes;
                    SourceLanguages.DataBind();
                    TargetLanguages.DataSource = ExportItems.GetLanguageCodes.Where(x => !x.Equals(SourceLanguages.SelectedItem.Text));
                    TargetLanguages.DataBind();
                }
            }
        }

        protected void SourceLanguages_SelectedIndexChanged(object sender, EventArgs e)
        {
            TargetLanguages.DataSource = ExportItems.GetLanguageCodes.Where(x => !x.Equals(SourceLanguages.SelectedItem.Text));
            TargetLanguages.DataBind();
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            var selectedLanguages = new SelectLanguageItems();
            selectedLanguages.SourceLanguage = SourceLanguages.SelectedItem.Text;
            selectedLanguages.TargetLanguages = new Collection<string>();
            foreach (RepeaterItem chkBox in TargetLanguages.Items)
            {
                var languages = (CheckBox)chkBox.FindControl("TargetLanguage");
                if (languages.Checked && !languages.Text.Equals(SourceLanguages.SelectedItem.Text))
                {
                    selectedLanguages.TargetLanguages.Add(languages.Text);
                }
            }

            if (selectedLanguages.TargetLanguages.Count > 0)
            {
                base.Session["SetLanguagesandItems"] = selectedLanguages;
                Response.Redirect("/GlobalWeb/CustomApps/Translations/SelectItems.aspx?" + Request.QueryString);
            }
            else
            {
                Message.Text = "Select at least one language";
            }
        }
    }
}