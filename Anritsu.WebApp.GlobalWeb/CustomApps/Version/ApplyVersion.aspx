﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplyVersion.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.CustomApps.Version.ApplyVersion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Apply Version</title>
    <link rel="stylesheet" href="/sitecore/shell/themes/standard/default/Default.css" />
       <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <style type="text/css">
          span#lblCount {
            margin-left: 70px;
            border-bottom: black;
            border-bottom-style: dotted;
            border-bottom-width: thin;
            border-bottom-color: blue;
        }

        .scStretchAbsolute {
            position: relative !important;
            margin-left:20px;
        }

        .divleft {
            margin-left: 20px;
        }
        .pullright {
            text-align:right;
        }
        .pulltopbottom {
            margin-top:10px;
            margin-bottom:10px;
        }
      
    </style>
</head>
<body style="width: 95%;">
    <form id="form1" runat="server">
        <div class="divleft pulltopbottom ">
           <%-- <div>
                <b>Item Name:</b><asp:Label ID="Name" runat="server" ForeColor="Blue"></asp:Label>
            </div>--%>
            <div>
                Select Source Language:<asp:DropDownList ID="SourceLanguages" runat="server" Width="200px"></asp:DropDownList>
            </div>
            <div>
                Select Target Language:<asp:DropDownList ID="TargetLanguages" runat="server" Width="200px"></asp:DropDownList>
            </div>
             <div>
                <asp:Label ID="Count" runat="server" ForeColor="Black"></asp:Label>
            </div>
        </div>
        <div>
            <div class="scWizardFormContentForOldIE scFlexContentWithoutFlexie">
                <div class="scStretchAbsolute">
                    <fieldset id="LanguagesPanel" style="margin: 0px 0px 16px 0px;">
                        <legend id="LanguagesPanellegend">Select Items</legend>

                        <asp:CheckBox ID="SelectAll" runat="server" Text="Select All" />
                        <asp:TreeView ID="contentTreeList" runat="server" ShowCheckBoxes="All"></asp:TreeView>

                    </fieldset>

                    <asp:Label ID="Message" runat="server" Style="color: red;"></asp:Label><br />

                </div>
            </div>
        </div>
        <div class="divleft">
            <div class="pullright">
                <asp:Button ID="Add" runat="server" Text="Add" OnClick="Add_Click" Width="80px" /></div>
           
        </div>

    </form>
    <script>
        $(function () {
            $("#SelectAll").on("click", function () {
                var $contentTreeCheckBoxList = $("#contentTreeList").find("input:checkbox");
                if ($(this).is(":checked")) {
                    $contentTreeCheckBoxList.prop("checked", true);
                } else {
                    $contentTreeCheckBoxList.prop("checked", false);
                }
            });
        });
    </script>
</body>
</html>
