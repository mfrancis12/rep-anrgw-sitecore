﻿using Sitecore.Collections;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.CustomApps.Version
{
    public partial class ApplyVersion : System.Web.UI.Page
    {
        Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Name.Text = master.GetItem(Request.QueryString["id"]).Name;
                BindLanguages();               
                if (Request.QueryString["id"] != null)
                {
                    this.BindTreeViewControl(this.master.GetItem(Request.QueryString["id"]));
                }                
            }
        }
        private void BindLanguages()
        {
            ChildList LstChildren = master.GetItem("{64C4F646-A3FA-4205-B98E-4DE2C609B60F}").GetChildren(); //Sitecore.Context.Database.GetItem("{64C4F646-A3FA-4205-B98E-4DE2C609B60F}").GetChildren();
            ListItemCollection lst = new ListItemCollection();
            foreach (Item child in LstChildren)
            {
                ListItem lstItm = new ListItem();
                lstItm.Text = child.Name;
                lstItm.Value = child.ID.ToString();
                lst.Add(lstItm);
            }

            SourceLanguages.DataSource = lst;
            SourceLanguages.DataTextField = "Text";
            SourceLanguages.DataValueField = "Value";
            SourceLanguages.DataBind();

            TargetLanguages.DataSource = lst;
            TargetLanguages.DataTextField = "Text";
            TargetLanguages.DataValueField = "Value";
            TargetLanguages.DataBind();

        }

        static int cont = 0;
        protected void Add_Click(object sender, EventArgs e)
        {
            cont = 0;
            if (Request.QueryString["id"] != null)
            {
                bool status = true;
                Language sourceLanguage = Language.Parse(SourceLanguages.SelectedItem.Text);
                Language targetLanguage = Language.Parse(TargetLanguages.SelectedItem.Text);
                //Check Main item
                status = CheckLanguageVersion(master.GetItem(Request.QueryString["id"]), targetLanguage, sourceLanguage);
                if (!status)
                {
                    Count.Text = @"Internal error occurred. Please try again.";
                    Count.ForeColor = System.Drawing.Color.Red;
                    return;
                }
                //Get Parent Item descendants              
                //var childItms = master.GetItem(Request.QueryString["id"]).Axes.GetDescendants();

                if (this.GetSelectedItems.Count() > 0)
                {
                    foreach (Item child in this.GetSelectedItems)
                    {
                        status = CheckLanguageVersion(child, targetLanguage, sourceLanguage);
                        if (!status)
                        {
                            Count.Text = "Internal error occurred. Please try again.";
                            Count.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                }
            }

            if (cont > 0)
                Count.Text = "<b>" + TargetLanguages.SelectedItem.Text + "</b>" + " version added to the item(s)";
            else
                Count.Text = "<b>" + TargetLanguages.SelectedItem.Text + "</b>" + " version already added to the item(s)";

        }


        //Query: Before adding version to Item, do we need to check permissions?
        // 1. before editing need to check item workflow
        // 2. Write access
        // 3. Edit only with Admin privillages
        // 4. CanLock/HasLock 
        private bool CheckLanguageVersion(Item itmId, Language targetLang, Language sourceLang)
        {
            Item localizedItm = master.GetItem(itmId.ID, targetLang);
            if (localizedItm.Versions.Count == 0)
            {
                //try
                //{
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    localizedItm.Editing.BeginEdit();
                    localizedItm.Versions.AddVersion();
                    localizedItm.Editing.EndEdit();

                    //Sitecore.Globalization.Language globalLang = Sitecore.Globalization.Language.Parse("en");
                    Item globalItem = master.GetItem(localizedItm.ID, sourceLang);

                    if (globalItem != null && globalItem.Versions.Count > 0)
                    {
                        using (new Sitecore.Data.Items.EditContext(localizedItm))
                        {
                            globalItem.Fields.ReadAll();
                            localizedItm.Fields.ReadAll();
                            foreach (Field field in globalItem.Fields)
                            {
                                if (!field.Name.Contains("__") && !field.Shared)
                                {
                                    localizedItm[field.ID] = globalItem.Fields[field.ID].Value;
                                }
                            }
                        }
                    }
                }


                    cont++;
                //}
                //catch
                //{
                //    return false;
                //}
            }
            else
            {
                Item globalItem = master.GetItem(localizedItm.ID, sourceLang);

                if (globalItem != null && globalItem.Versions.Count > 0)
                {
                    using (new Sitecore.Data.Items.EditContext(localizedItm))
                    {
                        globalItem.Fields.ReadAll();
                        localizedItm.Fields.ReadAll();
                        foreach (Field field in globalItem.Fields)
                        {
                            if (!field.Name.Contains("__") && !field.Shared)
                            {
                                localizedItm[field.ID] = globalItem.Fields[field.ID].Value;
                            }
                        }
                    }
                }
            }
            return true;
        }

        private Collection<Item> GetSelectedItems
        {
            get
            {
                Collection<Item> listItems = new Collection<Item>();
                foreach (TreeNode node in contentTreeList.CheckedNodes)
                {
                    listItems.Add(this.master.GetItem(node.Value));
                }

                return listItems;
            }
        }

        private void BindTreeViewControl(Item item)
        {
            try
            {
                TreeNode root = new TreeNode(item.Name);
                root.SelectAction = TreeNodeSelectAction.None;
                root.Value = item.ID.ToString();
                root.Collapse();
                contentTreeList.Nodes.Add(root);

                List<Item> lstItems = item.Children.ToList();

                foreach (Item childItem in lstItems)
                {
                    TreeNode croot = new TreeNode(childItem.Name);
                    croot.SelectAction = TreeNodeSelectAction.None;
                    croot.Value = childItem.ID.ToString();
                    croot.Collapse();
                    if (childItem.HasChildren)
                    {
                        this.CreateNode(croot, childItem);
                    }

                    root.ChildNodes.Add(croot);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CreateNode(TreeNode node, Item childItem)
        {
            List<Item> listItems = childItem.Children.ToList();
            foreach (Item itm in listItems)
            {
                TreeNode childnode = new TreeNode(!string.IsNullOrEmpty(Convert.ToString(itm.Fields[Sitecore.FieldIDs.DisplayName])) ? Convert.ToString(itm.Fields[Sitecore.FieldIDs.DisplayName]) : itm.Name);
                childnode.Value = itm.ID.ToString();
                childnode.SelectAction = TreeNodeSelectAction.None;
                childnode.Collapse();
                node.ChildNodes.Add(childnode);
                if (itm.HasChildren)
                {
                    this.CreateNode(childnode, itm);
                }
            }
        }
    }
}