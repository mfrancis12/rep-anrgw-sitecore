﻿using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Security.Accounts;
using Sitecore.Workflows.Simple;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Anritsu.WebApp.GlobalWeb.CustomApps.Workflow
{
    public partial class SelectUsers : System.Web.UI.Page
    {
        Database db = Sitecore.Configuration.Factory.GetDatabase("master");
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
               // BindRoles();
                GetEmailsForUsers();
            }
            
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            //var db = Sitecore.Configuration.Factory.GetDatabase("master");
            List<string> emails = new List<string>();
            foreach (RadListBoxItem item in Destination.Items)
            {
                emails.Add(item.Value);
            }

            Item innerItem = db.GetItem(Request.QueryString["innerItemID"]);
            Item contentItem = db.GetItem(Request.QueryString["contentItemID"]);
            string fullPath = innerItem.Paths.FullPath;
            SendMail(emails, innerItem, contentItem, fullPath);
        }
        

        //Returns unique email addresses of users that correspond to the selected list of users/roles  
        private void GetEmailsForUsers()
        {
            
            List<UsersAndMails> useremaillist = new List<UsersAndMails>();           
            //var users = System.Web.Security.Roles.GetUsersInRole(RolesList.SelectedItem.Text);
            //const string domain = "ANRGW";
            string domain = Sitecore.Configuration.Settings.GetSetting("domain");
            if (!string.IsNullOrEmpty(domain) && Sitecore.Security.Domains.Domain.GetDomain(domain) != null)
            {
               var users = Sitecore.Security.Domains.Domain.GetDomain(domain).GetUsers();
               
                if (users != null)
                {
                    string dataItemID = string.Empty;
                    Item dataItem = db.GetItem(Request.QueryString["contentItemID"]);
                    if (dataItem != null)
                    {
                        dataItemID = Request.QueryString["contentItemID"];
                        string nextState = Request.QueryString["nextState"];
                        foreach (var user in users)
                        {
                            if (CheckReadAccess(dataItemID, user.Name) && CheckReadAccess(nextState, user.Name))
                            {
                                UsersAndMails userandemail = new UsersAndMails();
                                if (user == null) continue;
                                var membershipUser = System.Web.Security.Membership.GetUser(user.Name);
                                if (membershipUser.Email != null && !string.IsNullOrEmpty(membershipUser.Email.Trim()))
                                {
                                    userandemail.UserName = membershipUser.UserName;
                                    userandemail.Email = membershipUser.Email;
                                }
                                useremaillist.Add(userandemail);
                            }
                        }
                    }

                    Source.DataSource = useremaillist;
                    Source.DataTextField = "UserName";
                    Source.DataValueField = "Email";
                    Source.DataBind();
                }
            }

           
        }
        private void SendMail(List<string> emails, Item innerItem, Item dataItem, string fullPath)
        {            
            foreach (var email in emails)
            {

                //var membershipUser = System.Web.Security.Membership.GetUser(user);
                //email = membershipUser.Email;
                // use this email to send the message to that user
                string mailFrom = Sitecore.Context.User.Profile.Email;
                string mailTo = email;// this.GetText(innerItem, "to", args);
                string mailServer = GetText(innerItem, "mail server", dataItem);
                string mailSubject = GetText(innerItem, "subject", dataItem);
                string mailBody = GetText(innerItem, "message", dataItem);

                Sitecore.Diagnostics.Error.Assert(mailTo.Length > 0, "The 'To' field is not specified in the mail action item: " + fullPath);
                Sitecore.Diagnostics.Error.Assert(mailFrom.Length > 0, "The 'From' field is not specified in the mail action item: " + fullPath);
                Sitecore.Diagnostics.Error.Assert(mailSubject.Length > 0, "The 'Subject' field is not specified in the mail action item: " + fullPath);
                Sitecore.Diagnostics.Error.Assert(mailServer.Length > 0, "The 'Mail server' field is not specified in the mail action item: " + fullPath);
                //System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage(text, text2);
                using (MailMessage mailMessage = new System.Net.Mail.MailMessage(mailFrom, mailTo))
                {
                    mailMessage.Subject = mailSubject;
                    mailMessage.Body = mailBody;
                    //System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(text3);
                    using (SmtpClient smtpClient = new System.Net.Mail.SmtpClient(mailServer))
                    {
                        smtpClient.Send(mailMessage);
                    }
                }
            }
        }
        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <param name="commandItem">The command item.</param>
        /// <param name="field">The field.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private string GetText(Item commandItem, string field, Item dataItem)
        {
            string text = commandItem[field];
            if (text.Length > 0)
            {
                return ReplaceVariables(text, dataItem);
            }
            return string.Empty;
        }
        /// <summary>
        /// Replaces the variables.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private static string ReplaceVariables(string text, Item dataItem)
        {
            text = text.Replace("$itemPath$", dataItem.Paths.FullPath);
            text = text.Replace("$itemLanguage$", dataItem.Language.ToString());
            text = text.Replace("$itemVersion$", dataItem.Version.ToString());           
            return text;
        }
        public bool CheckReadAccess(string itemId, string domainUser)
        {
            bool ReadAccess = false;

            if (Sitecore.Data.ID.IsID(itemId))
            {
                Item item = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(itemId),Sitecore.Context.Language);
                if (item != null)
                {                   
                    if (Sitecore.Security.Accounts.User.Exists(domainUser))
                    {
                        Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(domainUser, false);
                        // UserSwitcher allows below code to run under a specific user 
                        using (new Sitecore.Security.Accounts.UserSwitcher(user))
                        {
                            ReadAccess = item.Access.CanRead();
                        }
                    }
                }
            }
            return ReadAccess;
        }
        //private void BindRoles()
        //{
        //    //var roles = Sitecore.Context.User.Roles.Where(role => role.Domain != null && role.Domain.Equals(domain)).ToList();
        //    //RolesList.DataSource = roles;
        //    //RolesList.DataBind();
        //    const string domain = "ANRGW";
        //    if (Sitecore.Security.Domains.Domain.GetDomain(domain) != null)
        //    {
        //        RolesList.DataSource = Sitecore.Security.Domains.Domain.GetDomain(domain).GetRoles();
        //        RolesList.DataBind();
        //    }

        //}

        //protected void RolesList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    GetEmailsForUsers();
        //}

        protected void Cancel_Click(object sender, EventArgs e)
        {
            Sitecore.Context.ClientPage.ClientResponse.CloseWindow();
            //StringBuilder sb = new StringBuilder();
            //sb.AppendLine(@"<script language=""javascript"" type=""text/javascript"">");
            //sb.AppendLine(@"  window.returnValue = ""Cancel""");
            //sb.AppendLine(@"  window.close();");
            //sb.AppendLine(@"</script>");
            //ClientScript.RegisterClientScriptBlock(GetType(), "close", sb.ToString());
        }
    }
    public class UsersAndMails
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}