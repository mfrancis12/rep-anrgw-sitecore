﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.CustomFields
{
    public class BrowseFile : Sitecore.Web.UI.HtmlControls.Control
    {
        protected override void OnLoad(EventArgs e)
        {
            if (!Sitecore.Context.ClientPage.IsEvent)
            {
                //create the textbox controls
                var textFirstName = new Sitecore.Shell.Applications.ContentEditor.Text();
                textFirstName.ID = GetID("textFirstName");
                var textLastName = new Sitecore.Shell.Applications.ContentEditor.Text();
                textLastName.ID = GetID("textLastName");
                //
                //add the textbox controls as children
                this.Controls.Add(textFirstName);
                this.Controls.Add(textLastName);
            }
            else
            {
            }
            base.OnLoad(e);
        }
    }
}