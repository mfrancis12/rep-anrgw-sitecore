﻿using System;
using System.Linq;
using Sitecore.Data.Items;
using System.Web.UI;
using Sitecore.Diagnostics;
using Sitecore.Resources;
using System.Collections.Generic;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.Shell.Applications.ContentEditor;


namespace Anritsu.WebApp.GlobalWeb.CustomFields
{
    public class LanguageFilteredItems : Sitecore.Shell.Applications.ContentEditor.MultilistEx
    {
        //public string ItemLanguage
        //{
        //    get;
        //    set;
        //}
        string strItemPath = string.Empty;
        private Dictionary<string, string> SelectedItems;
        private Dictionary<string, string> UnSelectedItems;

        public void AssignItemPath(string item)
        {
            strItemPath = item;
        }
        /// <summary>
        /// Return here your unselected items. First value is the ID you will store into your field, the second one is the display text.
        /// </summary>
        /// <returns>The unselected items</returns>
        private IEnumerable<KeyValuePair<string, string>> GetNonSelectedItems()
        {
            return UnSelectedItems;
        }


        /// <summary>
        /// Return here your selected items. First value is the ID you will store into your field, the second one is the display text.
        /// </summary>
        /// <returns>The selected items</returns>
        private IEnumerable<KeyValuePair<string, string>> GetSelectedItems()
        {
            return SelectedItems;
        }

        /// <summary>
        /// By overideing this method, you can initialise some variables here.
        /// </summary>
        private void InitRendering()
        {
            //Return here your unselected items. First value is the ID you will store into your field, the second one is the display text.
            SelectedItems = new Dictionary<string, string>();
            UnSelectedItems = new Dictionary<string, string>();



            List<Item> items = new List<Item>();
            Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

            string itemPath = master.GetItem(this.ItemID).Paths.FullPath;
            Item itmforfield = master.GetItem(this.ItemID);

            itemPath = this.Source;
            Item itmChek = master.GetItem(itemPath);
            if (itmChek != null)
            {
                items = master.GetItem(itemPath).GetChildren().ToList();
                items = items.Where(item =>
                {
                    string lang = ItemLanguage;//context langauge
                    if (!string.IsNullOrEmpty(lang))
                    {
                        var versions = Sitecore.Data.Managers.ItemManager.GetVersions(item, Sitecore.Data.Managers.LanguageManager.GetLanguage(lang, item.Database));
                        return versions.Count > 0;
                    }
                    return false;
                }
                 ).ToList();
            }

            foreach (Item item in items)
            {
                if (this.Value.Split('|').Contains(item.ID.ToString()))
                {
                    SelectedItems.Add(item.ID.ToString(), item.DisplayName);
                }
                else
                {
                    UnSelectedItems.Add(item.ID.ToString(), item.DisplayName);
                }
            }
        }



        protected override void DoRender(System.Web.UI.HtmlTextWriter output)
        {
            Assert.ArgumentNotNull(output, "output");

            base.ServerProperties["ID"] = this.ID;


            InitRendering();
            string text = string.Empty;
            if (this.ReadOnly)
            {
                text = " disabled=\"disabled\"";
            }
            output.Write(string.Concat(new string[]
                            {
                                "<input id=\"", 
                                this.ID, 
                                "_Value\" type=\"hidden\" value=\"", 
                                StringUtil.EscapeQuote(this.Value), 
                                "\" />"
                            }));
            output.Write("<table" + this.GetControlAttributes() + ">");
            output.Write("<tr>");
            output.Write("<td class=\"scContentControlMultilistCaption\" width=\"50%\">" + Translate.Text("All") + "</td>");
            output.Write("<td width=\"20\">" + Images.GetSpacer(20, 1) + "</td>");
            output.Write("<td class=\"scContentControlMultilistCaption\" width=\"50%\">" + Translate.Text("Selected") + "</td>");
            output.Write("<td width=\"20\">" + Images.GetSpacer(20, 1) + "</td>");
            output.Write("</tr>");
            output.Write("<tr>");
            output.Write("<td valign=\"top\" height=\"100%\">");
            output.Write(string.Concat(new string[]
                            {
                                "<select id=\"", 
                                this.ID, 
                                "_unselected\" class=\"scContentControlMultilistBox\" multiple=\"multiple\" size=\"10\"", 
                                text, 
                                " ondblclick=\"javascript:scContent.multilistMoveRight('", 
                                this.ID, 
                                "')\" onchange=\"javascript:document.getElementById('", 
                                this.ID, 
                                "_all_help').innerHTML=this.selectedIndex>=0?this.options[this.selectedIndex].innerHTML:''\" >"
                            }));

            //Bind the selected values?
            foreach (KeyValuePair<string, string> field in GetNonSelectedItems())
            {
                //Item item2 = dictionaryEntry.Value as Item;
                //if (item2 != null)
                //{
                output.Write(string.Concat(new string[]
                {
                    "<option value=\"", 
                    field.Key, 
                    "\">", 
                    field.Value, 
                    "</option>"
                }));
                //}
            }

            output.Write("</select>");
            output.Write("</td>");
            output.Write("<td valign=\"top\">");
            this.RenderButton(output, "Core/16x16/arrow_blue_right.png", "javascript:scContent.multilistMoveRight('" + this.ID + "')");
            output.Write("<br />");
            this.RenderButton(output, "Core/16x16/arrow_blue_left.png", "javascript:scContent.multilistMoveLeft('" + this.ID + "')");
            output.Write("</td>");
            output.Write("<td valign=\"top\" height=\"100%\">");
            output.Write(string.Concat(new string[]
                        {
                            "<select id=\"", 
                            this.ID, 
                            "_selected\" class=\"scContentControlMultilistBox\" multiple=\"multiple\" size=\"10\"", 
                            text, 
                            " ondblclick=\"javascript:scContent.multilistMoveLeft('", 
                            this.ID, 
                            "')\" onchange=\"javascript:document.getElementById('", 
                            this.ID, 
                            "_selected_help').innerHTML=this.selectedIndex>=0?this.options[this.selectedIndex].innerHTML:''\">"
                        }));

            //Bind the available items list
            foreach (KeyValuePair<string, string> field in GetSelectedItems())
            {
                output.Write(string.Concat(new string[]
                {
                    "<option value=\"", 
                    field.Key, 
                    "\">", 
                    field.Value, 
                    "</option>"
                }));

            }
            output.Write("</select>");
            output.Write("</td>");
            output.Write("<td valign=\"top\">");
            this.RenderButton(output, "Core/16x16/arrow_blue_up.png", "javascript:scContent.multilistMoveUp('" + this.ID + "')");
            output.Write("<br />");
            this.RenderButton(output, "Core/16x16/arrow_blue_down.png", "javascript:scContent.multilistMoveDown('" + this.ID + "')");
            output.Write("</td>");
            output.Write("</tr>");
            output.Write("<tr>");
            output.Write("<td valign=\"top\">");
            output.Write("<div style=\"border:1px solid #999999;font:8pt tahoma;padding:2px;margin:4px 0px 4px 0px;height:14px\" id=\"" + this.ID + "_all_help\"></div>");
            output.Write("</td>");
            output.Write("<td></td>");
            output.Write("<td valign=\"top\">");
            output.Write("<div style=\"border:1px solid #999999;font:8pt tahoma;padding:2px;margin:4px 0px 4px 0px;height:14px\" id=\"" + this.ID + "_selected_help\"></div>");
            output.Write("</td>");
            output.Write("<td></td>");
            output.Write("</tr>");
            output.Write("</table>");

        }

        private void RenderButton(HtmlTextWriter output, string icon, string click)
        {
            Assert.ArgumentNotNull(output, "output");
            Assert.ArgumentNotNull(icon, "icon");
            Assert.ArgumentNotNull(click, "click");
            ImageBuilder imageBuilder = new ImageBuilder();
            imageBuilder.Src = icon;
            imageBuilder.Width = 16;
            imageBuilder.Height = 16;
            imageBuilder.Margin = "2px";
            if (!this.ReadOnly)
            {
                imageBuilder.OnClick = click;
            }
            output.Write(imageBuilder.ToString());
        }
    }
}