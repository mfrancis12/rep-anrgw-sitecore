﻿using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Anritsu.WebApp.GlobalWeb.CustomFields.MediaBrowser
{
    public class BrowseMedia : Sitecore.Web.UI.HtmlControls.Control
    {
        public string ItemId { get; set; }

        /// <summary>
        /// Name html control style
        /// </summary>
        protected virtual string ButtonStyle
        {
            get
            {
                return "width:10%";
            }
        }

        /// <summary>
        /// Name html control style
        /// </summary>
        protected virtual string InputStyle
        {
            get
            {
                return "width:100%;margin-top:20px;";
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (Sitecore.Context.ClientPage.IsEvent)
            {
                this.LoadValue();
                return;
            }
            this.BuildControl();
        }
        /// <summary>
        /// Builds the control.
        /// </summary>
        private void BuildControl()
        {
            Item itm = Sitecore.Context.ContentDatabase.GetItem(new Sitecore.Data.ID(ItemId));

            StringBuilder sb = new StringBuilder();
            sb.Append("<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\">");

            string uniqueButtonID = GetUniqueID(this.ID + "SelectMediaButton");
            string uniqueInputID = GetUniqueID(this.ID + "SelectMediaInput");

            string isDisabled = this.Disabled ? " disabled=\"disabled\"" : string.Empty;

            string arg1 = string.Format("<a id=\"{0}\" {1} style=\"{2}\" href=\"javascript:void(0);\" class=\"scContentButton\"  onclick=\"javascript:mediaButtonEvent(" + uniqueInputID + ")\">Insert link</a>",
                uniqueButtonID,
                isDisabled,
                this.ButtonStyle
                );

            string arg2 = string.Format("<input id=\"{0}\" type=\"text\"{1} style=\"{3}\" value=\"{2}\" class=\"browse-input-path\"/>",
                uniqueInputID,
                isDisabled,
                StringUtil.EscapeQuote(Value),
                this.InputStyle);

            sb.Append("<tr><td>" + arg1 + "</td></tr>");
            sb.Append("<tr><td>" + arg2 + "</td></tr>");

            sb.Append("</table>");
            this.Controls.Add(new LiteralControl(sb.ToString()));
        }

        /// <summary>
        /// Loads the post data.
        /// </summary>
        private void LoadValue()
        {
            if (Disabled)
            {
                return;
            }
            Page page = HttpContext.Current.Handler as System.Web.UI.Page;
            var nameValueCollection = page != null ? page.Request.Form : new NameValueCollection();

            string controlValue = string.Empty;
            //int intCount = 0;

            foreach (string key in nameValueCollection.Keys)
            {
                if (!string.IsNullOrEmpty(nameValueCollection[key]) && key.StartsWith(this.ID + "SelectMediaInput", StringComparison.OrdinalIgnoreCase))
                {
                    controlValue = nameValueCollection[key];
                    break;
                }
            }

            if (Value == controlValue)
            {
                return;
            }
            Value = controlValue;
        }
    }
}