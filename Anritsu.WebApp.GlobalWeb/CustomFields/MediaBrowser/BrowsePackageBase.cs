﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;

namespace Anritsu.WebApp.GlobalWeb.CustomFields.MediaBrowser
{
    public class BrowsePackageBase : Page
    {
        public String BasePath { get; set; }

        public String DLActiveRegion { get; set; }

        public String DownloadRegion { get; set; }

        public TreeNode AddNodes(string path, TreeNode parentNode, bool usePrefix = true)
        {
            if (path.EndsWith("/",StringComparison.OrdinalIgnoreCase))
                path = path.Substring(0, path.Length - 1);
            var commonPrefixes = new List<string>();
            var objs3Objects = GetS3Objects(path, "/", ref commonPrefixes, usePrefix);
            var node = new TreeNode(path, path);

            if (parentNode.ChildNodes.Count == 0)
            {
                //bind s3 folders
                foreach (var objChildNode in from folder in commonPrefixes
                                             let key = (string.IsNullOrEmpty(path)) ? folder
                                            : folder.Replace(path, "").Replace("/", "")
                                             let value = (string.IsNullOrEmpty(path))
                                                ? folder
                                             : path + "/" + folder.Replace(path, "").Replace("/", "")
                                             select new TreeNode(key, value)
                                             {
                                                 PopulateOnDemand = true,
                                                 Target = "_blank"
                                             })
                {
                    parentNode.ChildNodes.Add(objChildNode);
                }
                //bind s3 files
                foreach (var s3Obj in objs3Objects)
                {
                    var key = (string.IsNullOrEmpty(path))
                        ? s3Obj.Key
                        : s3Obj.Key.Replace(path, "").Replace("/", "");
                    var value = (string.IsNullOrEmpty(path))
                        ? s3Obj.Key
                        : path + "/" + s3Obj.Key.Replace(path, "").Replace("/", "");

                    if (path.EndsWith("/",StringComparison.OrdinalIgnoreCase))
                        path = path.Substring(0, path.Length - 1);
                    var objChildNode = new TreeNode(key, string.Format("{0}", value))
                    {
                        PopulateOnDemand = true,
                        Target = "_blank"
                    };

                    var extension = GetS3ObjectExtension(s3Obj.Key);
                    if (!extension.IsNullOrEmptyString())
                    {
                        switch (extension)
                        {
                            case ".pdf":
                                objChildNode.ImageUrl = "../Images/Adobe-PDF-Document-icon.png";
                                objChildNode.Expanded = true;
                                break;
                            case ".docx":
                            case ".doc":
                                objChildNode.Expanded = true;
                                objChildNode.ImageUrl = "../Images/Word-icon.png";
                                break;
                            case ".zip":
                                objChildNode.Expanded = true;
                                objChildNode.ImageUrl = "../Images/zip-icon.png";
                                break;
                            case ".png":
                            case ".jpg":
                            case ".jpeg":
                            case ".gif":
                                objChildNode.Expanded = true;
                                objChildNode.ImageUrl = "../Images/gnome_image_x_generic.png";
                                break;
                            case ".htm":
                            case ".html":
                                objChildNode.Expanded = true;
                                objChildNode.ImageUrl = "../Images/html.png";
                                break;
                            default:
                                objChildNode.Expanded = true;
                                break;
                        }
                    }
                    parentNode.ChildNodes.Add(objChildNode);
                }

            }
            return node;
        }

        private AmazonS3Client GetS3Client()
        {

            AmazonS3Client s3Client;
            RegionEndpoint regionEndpoint = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["AWSEndPoint"]);
            s3Client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSAccessKey"], ConfigurationManager.AppSettings["AWSSecretKey"], regionEndpoint);//, regionEndpoint
            return s3Client;
        }

        public S3ObjectsWithPrefixes GetS3ObjectsWithPrefixes(String prefix, String delimiter)
        {

            List<S3Object> content = new List<S3Object>();
            List<string> prefixes = new List<string>();

            if (!prefix.EndsWith("/", StringComparison.OrdinalIgnoreCase))
                prefix = prefix + "/";
            ListObjectsRequest request = new ListObjectsRequest();

            request.BucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            request.Delimiter = delimiter;
            request.Prefix = prefix;//.Replace("&", "amp;");
            request.MaxKeys = 10000;

            do
            {
                //CommonPrefixes
                AmazonS3Client s3Client = GetS3Client();

                ListObjectsResponse response = s3Client.ListObjects(request);
                content.AddRange(response.S3Objects);
                prefixes.AddRange(response.S3Objects.Select(t => t.Key));
                prefixes.AddRange(response.CommonPrefixes);

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);
            S3ObjectsWithPrefixes s3objwithPrefixes = new S3ObjectsWithPrefixes() { CommonPrefixes = prefixes, Content = content };
            return s3objwithPrefixes;
        }

        public List<S3Object> GetS3Objects(String prefix, String delimeter, ref List<String> commonPrefixes, bool usePrefix = true)
        {
            var resources = new List<S3Object>();
            commonPrefixes = new List<String>();
            string bucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            var s3Client = GetS3Client();

            var request = new ListObjectsRequest { BucketName = bucketName };
            if (!String.IsNullOrEmpty(delimeter))
                request.Delimiter = "/";
            request.Prefix = usePrefix ? prefix + "/" : String.Empty;
            request.MaxKeys = 10000;
            do
            {
                var response = s3Client.ListObjects(request);
                resources.AddRange(response.S3Objects.Where(s3obj => s3obj.Key != request.Prefix));
                commonPrefixes.AddRange(response.CommonPrefixes);
                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);
            return resources;
        }

        private static string GetS3ObjectExtension(string key)
        {
            if (string.IsNullOrEmpty(key) || key.LastIndexOf(".", StringComparison.Ordinal) < 0) return string.Empty;
            return key.Substring(key.LastIndexOf(".", StringComparison.Ordinal));

        }
    }



    public class S3ObjectsWithPrefixes
    {
        public List<string> CommonPrefixes { get; set; }
        public List<S3Object> Content { get; set; }
    }

    public class AnritsuUtilityS3
    {
        private String _AWSS3_AccessKey;
        private String _AWSS3_SecretKey;

        private String AWSS3_AccessKey
        {
            get
            {
                return _AWSS3_AccessKey;
            }
        }

        private String AWSS3_SecretKey
        {
            get
            {
                return _AWSS3_SecretKey;
            }
        }

        public AnritsuUtilityS3()
        {
            InitConfig();
        }

        private void InitConfig()
        {
            _AWSS3_AccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            _AWSS3_SecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
        }
    }
}