﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectMediaItems.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.CustomFields.MediaBrowser.SelectMediaItems" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        .toolbar {
            padding:5px 10px;
        }
        .field-wrapper {
            margin:30px 0 0 30px;
        }
        .field-wrapper .content {
                margin-bottom:5px;
        }
        .btn-primary {
            background-image: linear-gradient(to bottom, #289bc8 0%, #207da2 100%);
            background-repeat: repeat-x;
            border-color: #175973;
            box-shadow: 0 1px #5dbadf inset;
            background-color: #289bc8;
            border-color: #175973;
            color: #ffffff;
            border-radius: 6px;
            cursor: pointer;
            display: inline-block;
            font-size: 13px;
            font-weight: normal;
            line-height: 1.42857;
            padding:5px 10px;
        }
	 </style>
     <script type="text/javascript">
         function SelectAndClose() {
             txtValue = document.getElementById("txtPackagePath");
             window.top.mediaDialogReturnValue = txtValue.value;
             window.top.dialogClose();
             return false;
         }
        </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="field-wrapper">
            <div class="content">
                <asp:TextBox ID="txtPackagePath" ClientIDMode="Static" runat="server" CssClass="toolbar" Width="300px" />
                <span  onclick="SelectAndClose()" class="btn-primary">Select</span>
                
            </div>
            <asp:TreeView ID="BrowsePackages" runat="server" Height="500px" ImageSet="XPFileExplorer" NodeIndent="15" Width="292px">
                <ParentNodeStyle Font-Bold="False" />
                <RootNodeStyle ImageUrl="//static.cdn-anritsu.com/apps/connect/img/folder.gif" />
                <HoverNodeStyle Font-Underline="True" ForeColor="#6666AA" />
                <SelectedNodeStyle BackColor="#B5B5B5" Font-Underline="False" HorizontalPadding="0px"
                    VerticalPadding="0px" />
                <NodeStyle Font-Names="Tahoma" Font-Size="8pt" ForeColor="Black" HorizontalPadding="2px"
                    NodeSpacing="0px" VerticalPadding="2px" />
                <LeafNodeStyle ImageUrl="//static.cdn-anritsu.com/apps/connect/img/folder.gif" />
            </asp:TreeView>
        </div>
    </form>
</body>
</html>

