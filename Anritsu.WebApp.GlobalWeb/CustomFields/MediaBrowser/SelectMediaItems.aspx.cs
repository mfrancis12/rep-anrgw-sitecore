﻿using System;
using System.Configuration;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.CustomFields.MediaBrowser
{
    public partial class SelectMediaItems : BrowsePackageBase
    {
        public SelectMediaItems()
        {
            BasePath = ConfigurationManager.AppSettings["AWSBucketName"];
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Context.SetActiveSite("shell");
            if (!IsPostBack)
            {
                var onjParent = new TreeNode(BasePath, "") { PopulateOnDemand = true };
                BrowsePackages.Nodes.Add(onjParent);
                BrowsePackages.CollapseAll();
            }
            BrowsePackages.TreeNodeExpanded += BrowsePackages_TreeNodeExpanded;
            BrowsePackages.SelectedNodeChanged += BrowsePackages_SelectedNodeChanged;
        }

        protected void BrowsePackages_SelectedNodeChanged(object sender, EventArgs e)
        {
            txtPackagePath.Text = BrowsePackages.SelectedValue;
        }

        protected void BrowsePackages_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Node.Value))
                AddNodes(e.Node.Value, e.Node, false);
            else
                AddNodes(e.Node.Value, e.Node);
        }
    }
}