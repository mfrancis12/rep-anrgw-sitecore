﻿using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Text;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.HtmlControls.Data;
using Sitecore.Web.UI.Sheer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Anritsu.WebApp.GlobalWeb.CustomFields
{
    public class NameLookupValueExtension : Input
    {
        /// <summary>
        /// Gets or sets the name of the field.
        /// </summary>
        /// <value>The name of the field.</value>
        /// <contract>
        ///   <requires name="value" condition="not null" />
        ///   <ensures condition="nullable" />
        /// </contract>
        public string FieldName
        {
            get
            {
                return GetViewStateString("FieldName");
            }
            set
            {
                Assert.ArgumentNotNull(value, "value");
                SetViewStateString("FieldName", value);
            }
        }
        /// <summary>
        /// Gets or sets the item ID.
        /// </summary>
        /// <value>The item ID.</value>
        /// <contract>
        ///   <requires name="value" condition="not null" />
        ///   <ensures condition="nullable" />
        /// </contract>
        public string ItemId
        {
            get
            {
                return GetViewStateString("ItemID");
            }
            set
            {
                Assert.ArgumentNotNull(value, "value");
                SetViewStateString("ItemID", value);
            }
        }
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        /// <contract>
        ///   <requires name="value" condition="not null" />
        ///   <ensures condition="nullable" />
        /// </contract>
        public string Source
        {
            get
            {
                return GetViewStateString("Source");
            }
            set
            {
                Assert.ArgumentNotNull(value, "value");
                SetViewStateString("Source", value);
            }
        }
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <param name="current">The current.</param>
        /// <returns>The items.</returns>
        private IEnumerable<Item> GetItems(Item current)
        {
            Assert.ArgumentNotNull(current, "current");
            return LookupSources.GetItems(current, Source);
        }
        /// <summary>
        /// Gets the item header.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The item header.</returns>
        /// <contract>
        ///   <requires name="item" condition="not null" />
        ///   <ensures condition="not null" />
        /// </contract>
        private string GetItemHeader(Item item)
        {
            Assert.ArgumentNotNull(item, "item");
            var @string = StringUtil.GetString(new[]
			{
				FieldName
			});
            string result;
            if (@string.StartsWith("@", StringComparison.OrdinalIgnoreCase))
            {
                result = item[@string.Substring(1)];
            }
            else
            {
                result = @string.Length > 0 ? item[FieldName] : item.DisplayName;
            }
            return result;
        }
        /// <summary>
        /// Gets the item value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        /// <contract>
        ///   <requires name="item" condition="not null" />
        ///   <ensures condition="not null" />
        /// </contract>
        private string GetItemValue(Item item)
        {
            Assert.ArgumentNotNull(item, "item");
            return item.ID.ToString();
        }
        /// <summary>
        /// Determines whether the specified item is selected.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// 	<c>true</c> if the specified item is selected; otherwise, <c>false</c>.
        /// </returns>
        private bool IsSelected(Item item)
        {
            Assert.ArgumentNotNull(item, "item");
            return Value == item.ID.ToString() || Value == item.Paths.LongID;
        }
        /// <summary>
        /// Gets value html control.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="value">The value.</param>
        /// <returns>The formatted value html control.</returns>
        private string GetValueHtmlControl(string id, string value)
        {
            var htmlTextWriter = new HtmlTextWriter(new StringWriter());
            var item = Sitecore.Context.ContentDatabase.GetItem(ItemId);
            var items = GetItems(item);
            htmlTextWriter.Write(string.Concat(new[]
			{
				"<select id=\"",
				id,
				"_value\" name=\"",
				id,
				"_value\"",
				GetControlAttributes(),
				">"
			}));
            htmlTextWriter.Write("<option" + (string.IsNullOrEmpty(value) ? " selected=\"selected\"" : string.Empty) + " value=\"\"></option>");
            var array = items;
            foreach (Item item2 in array)
            {
                var itemHeader = GetItemHeader(item2);
                var flag = item2.ID.ToString() == value;
                htmlTextWriter.Write(string.Concat(new[]
			    {
			        "<option value=\"",
			        GetItemValue(item2),
			        "\"",
			        flag ? " selected=\"selected\"" : string.Empty,
			        ">",
			        itemHeader,
			        "</option>"
			    }));
            }
            htmlTextWriter.Write("</select>");
            return htmlTextWriter.InnerWriter.ToString();
        }

        /// <summary>
        /// Name html control style
        /// </summary>
        private string NameStyle
        {
            get
            {
                return "width:100%;background-color:lightgrey'";
            }
        }
        /// <summary>
        /// Is control vertical
        /// </summary>
        private bool IsVertical
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Sitecore.Shell.Applications.ContentEditor.NameValue" /> class.
        /// </summary>
        public NameLookupValueExtension()
        {
            Class = "scContentControl";
            Activation = true;
        }
        /// <summary>
        /// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"></see> object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="output">
        /// The <see cref="T:System.Web.UI.HtmlTextWriter"></see> object that receives the server control content.
        /// </param>
        protected override void DoRender(HtmlTextWriter output)
        {
            Assert.ArgumentNotNull(output, "output");
            SetWidthAndHeightStyle();
            output.Write("<div" + ControlAttributes + ">");
            RenderChildren(output);
            output.Write("</div>");
        }
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">
        /// The <see cref="T:System.EventArgs"></see> object that contains the event data.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnLoad(e);
            if (Sitecore.Context.ClientPage.IsEvent)
            {
                LoadValue();
                return;
            }
            BuildControl();
        }
        /// <summary>
        /// Parameters the change.
        /// </summary>
        [UsedImplicitly]
        private void ParameterChange()
        {
            ClientPage clientPage = Sitecore.Context.ClientPage;
            if (clientPage.ClientRequest.Source == StringUtil.GetString(clientPage.ServerProperties[ID + "_LastParameterID"]))
            {
                string value = clientPage.ClientRequest.Form[clientPage.ClientRequest.Source];
                if (!string.IsNullOrEmpty(value))
                {
                    string value2 = BuildParameterKeyValue(string.Empty, string.Empty);
                    clientPage.ClientResponse.Insert(ID, "beforeEnd", value2);
                }
            }
            NameValueCollection nameValueCollection = null;
            var page = HttpContext.Current.Handler as System.Web.UI.Page;
            if (page != null)
            {
                nameValueCollection = page.Request.Form;
            }
            if (nameValueCollection == null)
            {
                return;
            }
            if (Validate(nameValueCollection))
            {
                clientPage.ClientResponse.SetReturnValue(true);
            }
        }
        /// <summary>
        /// Sets the modified flag.
        /// </summary>
        protected override void SetModified()
        {
            base.SetModified();
            if (TrackModified)
            {
                Sitecore.Context.ClientPage.Modified = true;
            }
        }
        /// <summary>
        /// Builds the control.
        /// </summary>
        private void BuildControl()
        {
            var urlString = new UrlString(Value);
            foreach (string text in urlString.Parameters.Keys)
            {
                if (text.Length > 0)
                {
                    Controls.Add(new LiteralControl(BuildParameterKeyValue(urlString.Parameters[text], HttpUtility.UrlDecode(text))));
                }
            }
            Controls.Add(new LiteralControl(BuildParameterKeyValue(string.Empty, string.Empty)));
        }
        /// <summary>
        /// Builds the parameter key value.
        /// </summary>
        /// <param name="key">
        /// The parameter key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The parameter key value.
        /// </returns>
        /// <contract><requires name="key" condition="not null" /><requires name="value" condition="not null" /><ensures condition="not null" /></contract>
        private string BuildParameterKeyValue(string key, string value)
        {
            Assert.ArgumentNotNull(key, "key");
            Assert.ArgumentNotNull(value, "value");
            string uniqueID = GetUniqueID(ID + "_Param");
            Sitecore.Context.ClientPage.ServerProperties[ID + "_LastParameterID"] = uniqueID;
            string clientEvent = Sitecore.Context.ClientPage.GetClientEvent(ID + ".ParameterChange");
            string text = ReadOnly ? " readonly=\"readonly\"" : string.Empty;
            string text2 = Disabled ? " disabled=\"disabled\"" : string.Empty;
            string arg = IsVertical ? "</tr><tr>" : string.Empty;
            string arg2 = string.Format("<input id=\"{0}\" name=\"{1}\" type=\"text\"{2}{3} style=\"{6}\" value=\"{4}\" onchange=\"{5}\"/>", new object[]
			{
				uniqueID,
				uniqueID,
				text,
				text2,
				StringUtil.EscapeQuote(HttpUtility.UrlDecode(key)),
				clientEvent,
				NameStyle
			});
            string valueHtmlControl = GetValueHtmlControl(uniqueID, StringUtil.EscapeQuote(HttpUtility.UrlDecode(value)));
            return string.Format("<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\"><tr><td width=\"20%\">{0}</td>{2}<td width=\"80%\">{1}</td></tr></table>", valueHtmlControl, arg2, arg);
        }
        /// <summary>
        /// Loads the post data.
        /// </summary>
        private void LoadValue()
        {
            if (ReadOnly || Disabled)
            {
                return;
            }
            var page = HttpContext.Current.Handler as System.Web.UI.Page;
            var nameValueCollection = page != null ? page.Request.Form : new NameValueCollection();
            var urlString = new UrlString();
            foreach (string text in nameValueCollection.Keys)
            {
                if (!string.IsNullOrEmpty(text) && text.StartsWith(ID + "_Param", StringComparison.OrdinalIgnoreCase) && !text.EndsWith("_value", StringComparison.OrdinalIgnoreCase))
                {
                    string text3 = nameValueCollection[text];
                    string text2 = HttpUtility.UrlEncode(nameValueCollection[text + "_value"]);
                    if (!string.IsNullOrEmpty(text2))
                    {
                        urlString[text2] = (text3 ?? string.Empty);
                    }
                }
            }
            string text4 = urlString.ToString();
            if (Value == text4)
            {
                return;
            }
            Value = text4;
            SetModified();
        }
        /// <summary>
        /// Validates the specified client page.
        /// </summary>
        /// <param name="form">The form.</param>
        /// <returns>The result of the validation.</returns>
        private bool Validate(NameValueCollection form)
        {
            Assert.ArgumentNotNull(form, "form");
            return true;
        }
    }
}