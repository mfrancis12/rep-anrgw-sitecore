﻿using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.SiteSearch;

namespace Anritsu.WebApp.GlobalWeb.DownloadSearch
{
    # region ProductFamily Computed Field

    public class ProductFamily : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;                        

            return GetProductFamilies(item);
        }

        private IEnumerable<string> GetProductFamilies(Item frequentlyAskedQuestionItem)
        {
            var regionalProducts = new List<Item>();

            using (new Sitecore.Globalization.LanguageSwitcher(frequentlyAskedQuestionItem.Language))
            {
                regionalProducts.AddRange(((MultilistField)frequentlyAskedQuestionItem.Fields["RelatedProducts"]).GetItems().ToList());

                var globalProducts = regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null).Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem).ToList();

                var families = new List<Item>();
                foreach (var product in globalProducts)
                {
                    families.AddRange(((MultilistField)product.Fields["ProductFamily"]).GetItems().Where(x => x != null).Distinct(new ItemEqualityComparer()).ToList());
                }

                families = families.Distinct(new ItemEqualityComparer()).ToList();

                if (families.Any())
                    return families.Select(x => x.ID.ToString()).ToList();
                else return null;
            }
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    #endregion

    #region ProductSubCategory Computed Field

    public class ProductSubcategory : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            return GetSubcategories(item);
        }

        private IEnumerable<string> GetSubcategories(Item frequentlyAskedQuestionItem)
        {
            var regionalProducts = new List<Item>();

            using (new Sitecore.Globalization.LanguageSwitcher(frequentlyAskedQuestionItem.Language))
            {
                regionalProducts.AddRange(((MultilistField)frequentlyAskedQuestionItem.Fields["RelatedProducts"]).GetItems().ToList());

                var globalProducts = regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null).Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem).ToList();
                var families = new List<Item>();
                foreach (var product in globalProducts)
                {
                    families.AddRange(((MultilistField)product.Fields["ProductFamily"]).GetItems().Where(x => x != null).Distinct(new ItemEqualityComparer()).ToList());
                }
                families = families.Distinct(new ItemEqualityComparer()).ToList();
                var subcategories = families.Select(x => x.ParentID.ToString()).Distinct();

                if (subcategories.Any())
                    return subcategories.Distinct();
                else return null;
            }
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    #endregion

    #region ProductCategory Computed Field

    public class ProductCategory : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (item.TemplateID.ToString() != TemplateIds.FrequentlyAskedQuestion)
                return null;

            return GetCategories(item);
        }

        private IEnumerable<string> GetCategories(Item frequentlyAskedQuestionItem)
        {
            var regionalProducts = new List<Item>();

            using (new Sitecore.Globalization.LanguageSwitcher(frequentlyAskedQuestionItem.Language))
            {

                regionalProducts.AddRange(((MultilistField)frequentlyAskedQuestionItem.Fields["RelatedProducts"]).GetItems().ToList());

                var globalProducts = regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null).Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem).ToList();

                var families = new List<Item>();
                foreach (var product in globalProducts)
                {
                    families.AddRange(((MultilistField)product.Fields["ProductFamily"]).GetItems().Where(x => x != null).Distinct(new ItemEqualityComparer()).ToList());
                }

                families = families.Distinct(new ItemEqualityComparer()).ToList();

                var subcategories = families.Select(x => x.Parent).Distinct(new ItemEqualityComparer()).ToList();

                var categories = subcategories.Select(x => x.ParentID.ToString());

                if (categories.Any())
                    return categories.Distinct();
                else return null;
            }
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }


    #endregion    

    #region ProductModel Number Computed Field

    public class ProductModelNumberField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            return GetModelNumbers(item);
        }

        private List<string> GetModelNumbers(Item downloadItem)
        {
            var regionalProducts = new List<Item>();

            using (new Sitecore.Globalization.LanguageSwitcher(downloadItem.Language))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(downloadItem, downloadItem.Fields["RelatedProducts"]);
                regionalProducts.AddRange(((MultilistField)fallbackItem.Fields["RelatedProducts"]).GetItems().ToList());

                var globalProducts = regionalProducts.Where(x => ((ReferenceField)(SiteSearchHelper.GetSitecoreFallbackItem(x, x.Fields["SelectProduct"]).Fields["SelectProduct"])).TargetItem != null).Select(x => ((ReferenceField)(SiteSearchHelper.GetSitecoreFallbackItem(x, x.Fields["SelectProduct"]).Fields["SelectProduct"])).TargetItem).ToList();

                if (globalProducts.Any())
                    return globalProducts.Select(x => (SiteSearchHelper.GetSitecoreFallbackItem(x, x.Fields["ModelNumber"])["ModelNumber"]).Replace("<br>","/")).Distinct().ToList();
                else return null;
            }
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }

    #endregion

    #region DownloadCategory Computed Field

    public class DownloadCategoryField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            return GetDownloadCategory(item);
        }

        private string GetDownloadCategory(Item downloadItem)
        {
            string downloadCategory = null;

            using (new Sitecore.Globalization.LanguageSwitcher(downloadItem.Language))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(downloadItem, downloadItem.Fields["Subcategory"]);
                if (fallbackItem.Fields["Subcategory"] != null && ((ReferenceField)fallbackItem.Fields["Subcategory"]).TargetItem != null)
                {
                    var downloadCategoryItem = ((ReferenceField)fallbackItem.Fields["Subcategory"]).TargetItem;
                    downloadCategory = SiteSearchHelper.GetSitecoreFallbackItem(downloadCategoryItem, downloadCategoryItem.Fields["Key"])["Key"];
                }
            }
            return downloadCategory;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }

    #endregion

    #region DownloadCategoryId Computed Field

    public class DownloadCategoryIdField : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            return GetDownloadCategory(item);
        }

        private string GetDownloadCategory(Item downloadItem)
        {
            string downloadCategory = null;

            using (new Sitecore.Globalization.LanguageSwitcher(downloadItem.Language))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(downloadItem, downloadItem.Fields["Subcategory"]);
                if (fallbackItem.Fields["Subcategory"] != null && ((ReferenceField)fallbackItem.Fields["Subcategory"]).TargetItem != null)
                {
                    downloadCategory = ((ReferenceField)fallbackItem.Fields["Subcategory"]).TargetID.ToString();
                }
            }
            return downloadCategory;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }

    #endregion

    #region Download ReleaseDate Computed Field

    public class DownloadReleaseDate : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            return GetDownloadReleaseDate(item);
        }

        private string GetDownloadReleaseDate(Item downloadItem)
        {
            string downloadReleaseDate = null;

            using (new Sitecore.Globalization.LanguageSwitcher(downloadItem.Language))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(downloadItem, downloadItem.Fields["ReleaseDate"]);
                if (fallbackItem.Fields["ReleaseDate"] != null)
                {
                    downloadReleaseDate = fallbackItem["ReleaseDate"];
                }
            }
            return downloadReleaseDate;
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }

    #endregion
}