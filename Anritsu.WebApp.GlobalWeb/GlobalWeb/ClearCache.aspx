﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<script runat="server">
    protected void EventsCache_Click(object sender, EventArgs e)
    {
        var cacheKey = "events-";
        var enumerator = HttpContext.Current.Cache.GetEnumerator();
        while (enumerator.MoveNext())
        {
            if (enumerator.Key.ToString().StartsWith(cacheKey))
            {
                HttpContext.Current.Cache.Remove(enumerator.Key.ToString());
            }
        }
        lblStatus.Visible = true;
        lblStatus.Text = "Events cache has been cleared.";
    }

    protected void IrNewsCache_Click(object sender, EventArgs e)
    {
        var cacheKey = "irnews-";
        var enumerator = HttpContext.Current.Cache.GetEnumerator();
        while (enumerator.MoveNext())
        {
            if (enumerator.Key.ToString().StartsWith(cacheKey))
            {
                HttpContext.Current.Cache.Remove(enumerator.Key.ToString());
            }
        }
        lblStatus.Visible = true;
        lblStatus.Text = "IrNews cache has been cleared.";
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td style="height: 100px">
                        <asp:Button ID="EventsCache" runat="server" Text="Clear Events Cache" OnClick="EventsCache_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="IrNewsCache" runat="server" Text="Clear IrNews Cache" OnClick="IrNewsCache_Click" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 20px">
                        <asp:Label ID="lblStatus" runat="server" Visible="False" ForeColor="#339966"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>