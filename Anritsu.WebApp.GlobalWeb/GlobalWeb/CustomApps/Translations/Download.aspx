﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Download.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.CustomApps.Translations.Download" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
 <div>
       <strong>The wizard has completed. Click Finish to close the wizard.</strong><br /><br />
        To download the file, click Download <br /><br />

    <asp:Button ID="Downloads" runat="server" Text="Download" OnClick="Download_Click"/>
    <asp:Button ID="Close" runat="server" Text="Close" OnClientClick="return Closewinwdow();" />
    </div>
    </form>
</body>
     <script type="text/javascript">
         function Closewinwdow() {
             window.top.dialogClose();
         }
    </script>
</html>
