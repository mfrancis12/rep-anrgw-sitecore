﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Import.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.CustomApps.Translations.Import" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   <div class="scStretch">
            <div class="scWizardPageContainer">
                <div id="File" class="scStretch scFlexColumnContainerWithoutFlexie" style="">
                    <div class="scWizardHeader">
                        <img border="0" alt="" src="/temp/IconCache/Network/32x32/Earth_Location.png" />
                        <div>
                            <div class="scWizardTitle">Select Import File</div>
                            <div class="scWizardText">Select the file to import. Click Next to continue.</div>
                        </div>
                    </div>
                    <div class="scBottomEdge"></div>
                    <div class="scTopEdge"></div>

                    <div class="scWizardFormContentForOldIE scFlexContentWithoutFlexie">
                        <div style="padding: 8px 8px 8px 32px;" class="scStretchAbsolute">
                            <asp:TextBox ID="FileName" runat="server"></asp:TextBox><asp:FileUpload ID="fileUpload" runat="server" />
                            <br />
                            <br />
                              <asp:Button ID="Upload" runat="server"
                                OnClick="Upload_Click" Text="Upload"
                                Style="width: 85px" />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                          
                           
                            <div class="scWizardButtons">
                                <div class="scBottomEdge"></div>
                                <div class="scTopEdge"></div>
                                 <asp:Button ID="Next" runat="server" Text="Next" OnClick="Next_Click" Width="100" />
                                <asp:Label ID="Message" runat="server" /><br />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
