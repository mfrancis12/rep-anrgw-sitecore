﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectUsers.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.CustomApps.Workflow.SelectUsers" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div>
          <%--  <div style="margin-top: 10px;margin-bottom:10px;">
                Select Role:<asp:DropDownList ID="RolesList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RolesList_SelectedIndexChanged" DataTextField="Name" DataValueField="Name">
                </asp:DropDownList>
            </div>--%>
            <div>
                Select Users:
            <div>
                <telerik:RadListBox ID="Source" runat="server" AllowTransfer="true" TransferToID="Destination" EmptyMessage="No users are available" Height="270px" Width="240px">
                </telerik:RadListBox>
                <telerik:RadListBox ID="Destination" runat="server" Height="270px" Width="210px">
                </telerik:RadListBox>
            </div>
            </div>
            <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" /><asp:Button ID="Cancel" runat="server" Text="Cancel" OnClick="Cancel_Click"  />
        </div>
        <asp:Label ID="message" runat="server"></asp:Label>
    </form>
</body>
</html>
