﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HeaderLayout.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Layouts.HeaderLayout" %>

<%@ Register TagPrefix="asp" Namespace="Anritsu.WebApp.GlobalWeb.CustomFields" Assembly="Anritsu.WebApp.GlobalWeb" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="mt" Namespace="Sitecore.WebControls" Assembly="Sitecore.MetaTags" %>

<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8"> <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9"> <![endif]-->
<!--[if gt IE 9]>  <html class="ie gte9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-ie">
<!--<![endif]-->
<head>
    <title></title>
    <mt:metatags runat="server" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="/GlobalWeb/Resources/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="/GlobalWeb/Resources/css/style.css">
    <link rel="stylesheet" href="/GlobalWeb/Resources/img/icon/icons.css">
    <link rel="stylesheet" href="/GlobalWeb/Resources/css/fixes.css">
    <style type="text/css">
        .out-container { height:300px;
        }
    </style>
</head>
<body class="<%=Sitecore.Context.Language.Equals(Sitecore.Globalization.Language.Parse("ja-JP"))?"japan":""%>">
    <form runat="server" id="mainForm">
      
        <div class="skipnav"><a href="#maincontent">Skip to main content </a></div>
        <div class="out-container">
            <sc:placeholder runat="server" key="Menu" />
            <div class="rightContent">
                <div class="wrap">
                    <sc:placeholder runat="server" key="Header" />
                    <div id="maincontent">
                        <sc:placeholder runat="server" key="Content" />
                    </div>
                    <%--<div class="footer">
                        <sc:placeholder runat="server" key="Footer" />
                    </div>--%>
                </div>
            </div>
            <div class="search-popup">
                <div id="search-bar" class="search-bar hide">
                    <input type="text" placeholder="Type" autocomplete="off"><i class="icon icon-search"></i><span runat="server">
                        <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "clear") %>
                    </span><a class="icon icon-close closeLink"></a>
                </div>
            </div>
            <div class="country-popup">
                <div class="hd">
                    <div class="current">
                        <sc:Text ID="CountryName" Field="CountryName" runat="server"></sc:Text>
                    </div>
                    <a class="icon icon-close closeLink" href="#"></a>
                </div>
                <div class="bd">
                    <div class="heading">
                        <span runat="server"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "VisitAnotherCountry") %></span>
                    </div>
                    <div class="country-list">
                        <asp:Repeater ID="CountryList" runat="server" ItemType="Sitecore.Data.Items.Item">
                            <ItemTemplate>
                                <a href='/<%# GetCultureCode(Item,"CultureGroup") %>' data-country-code="<%# Item["CountryCode"] %>"><%# Item["CountryName"] %></a>
                            </ItemTemplate>
                            <FooterTemplate>
                                <a href="#"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "More") %> <i class="icon icon-next"></i></a>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div runat="server">
                <div class='country-confirm <%= ConfirmCountryPopup %>'>
                    <div class="hd">
                        <div class="current">
                            <sc:Text ID="confirmCountry" Field="Phrase" runat="server"></sc:Text>
                        </div>
                        <div class="closeLinkDiv"><a class="icon icon-close closeLink" href="#"></a></div>
                    </div>
                    <div class="bd">
                        <div class="select-box custom-select">
                            <asp:DropDownListAttributes runat="server" ID="ConfirmCountriesDropDown">
                            </asp:DropDownListAttributes>
                        </div>
                        <asp:Button ID="ConfirmButton" runat="server" CssClass="button" />
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="/GlobalWeb/Resources/js/jquery.min.js"></script>
    <script src="/GlobalWeb/Resources/js/jquery.transit.js"></script>
    <script src="/GlobalWeb/Resources/js/select.js"></script>
    <script src="/GlobalWeb/Resources/js/slick.min.js"></script>
    <script src="/GlobalWeb/Resources/js/jquery.autocomplete.min.js"></script>
    <script src="/GlobalWeb/Resources/js/jquery.pjax.js"></script>
    <script src="/GlobalWeb/Resources/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="/GlobalWeb/Resources/js/app.js"></script>
    <script src="/GlobalWeb/Resources/js/page.js"></script>
    <script src="/GlobalWeb/Resources/js/fixes.js"></script>
    <script src="https://s7.addthis.com/js/250/addthis_widget.js?pub=xa-4a8b926248dc2b0b"></script>

</body>
</html>