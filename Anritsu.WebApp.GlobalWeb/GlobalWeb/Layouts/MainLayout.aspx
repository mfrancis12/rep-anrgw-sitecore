﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainLayout.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Layouts.MainLayout" %>

<%@ Register TagPrefix="asp" Namespace="Anritsu.WebApp.GlobalWeb.CustomFields" Assembly="Anritsu.WebApp.GlobalWeb" %>
<%@ Register TagPrefix="mt" Namespace="Sitecore.WebControls" Assembly="Sitecore.MetaTags" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Constants" %>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8"> <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9"> <![endif]-->
<!--[if gt IE 9]>  <html class="ie gte9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<%=Region%>" class="no-ie">
<!--<![endif]-->
<head>
    <link rel="icon" href="<%=CDNPath %>/appfiles/img/icons/favicon.ico" type="image/png">
    <asp:PlaceHolder id="TitlePlaceHolder" runat="server" />
    

    <mt:metatags runat="server" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="content-language" content="<%=Region%>">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="msapplication-config" content="none"/>
    
    <% if (Sitecore.Context.Language.Name.Equals("zh-cn", StringComparison.OrdinalIgnoreCase) || Sitecore.Context.Language.Name.Equals("zh-tw", StringComparison.OrdinalIgnoreCase))
       { %>
    <meta name="baidu-site-verification" content="thTwrGcIwb" />
    <% } %>

    <link rel="apple-touch-icon" href="<%=CDNPath %>/appfiles/img/icons/favicon.ico">
    <link href="<%= CDNPath %>/appfiles/css/jquery-ui-min.css" rel="Stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="<%=CDNPath %>/appfiles/css/globalmin.css?v=20201211">
	
	 <% if (Sitecore.Context.Language.Name.Equals("ja-jp", StringComparison.OrdinalIgnoreCase))
       { %>
     <link rel="stylesheet" href="<%=CDNPath %>/ja-jp/about-anritsu/appfiles/css/navigation.css?v=20210331">
    <% } else {%>
	 <link rel="stylesheet" href="<%=CDNPath %>/appfiles/css/navigation.css?v=20210331">
	 <% } %>
   
	
    <sc:VisitorIdentification id="VisitorIdentification" runat="server" />
    <asp:PlaceHolder id="GsaMetaPlaceHolder" runat="server" />
	<script src="<%=CDNPath %>/appfiles/js/jquery.min.js" type="text/javascript"></script>

   

</head>
<body class="global-web <%=Region.ToLower()%>">
     

     <input type="hidden" id="hdnSearchPath" value="<%=SearchPath%>" />
    <!--googleoff: all--><div class="skipnav"><a href="#maincontent">Skip to main content </a></div><!--googleon: all-->
    <div class="out-container">
          <div class="header-area">
 <sc:placeholder runat="server" key="Header" />
 </div>
        <div class="mainForm">		
  <!--googleoff: all-->
        <form runat="server" id="mainForm">
            <div class="rightContent">
                <div class="wrap">
                   
                    <div id="maincontent">
					   <sc:placeholder runat="server" key="BreadCrumb" />
                        <sc:placeholder runat="server" key="Content" />
                    </div>
                    <div class="footer">
                        <sc:placeholder runat="server" key="Footer" />
                    </div>
                </div>
            </div>
           
            <div class="country-popup country-popup-container">
                <div class="hd">
				 <div class="heading">
                   <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "VisitAnotherCountry") %></span>
                   <asp:HiddenField id="hdnCountryName" runat="server"></asp:HiddenField>
                 </div>      
                 <a class="icon icon-close-new closeLink" href="#"></a>
                </div>
                <div class="bd">                    
                  <div class="country-list">
					<ul>
                        <asp:Repeater ID="CountryList" runat="server" ItemType="Sitecore.Data.Items.Item" OnItemDataBound="CountryList_ItemDataBound">
                            <ItemTemplate>
                               <li>  <a href=' <%# "/"+GetCultureCode(Item,"CultureGroup") %>' class="country" runat="server" id="countryLnk"
                                   data-country-code='<%# Item["CountryCode"] %>'><%# Item["CountryName"] %></a>    </li>                             
                            </ItemTemplate>
                            <FooterTemplate>
                                <li> <a href="<%# Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(ItemIds.ChangeRegion)) %>" class="more">
                                    <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "More") %> <i class="icon icon-next-new"></i></a> </li>
                            </FooterTemplate>
                        </asp:Repeater>
						</ul>
                    </div>
                </div>
            </div>
            <div runat="server">
                <div class='country-confirm <%= ConfirmCountryPopup %>'>
                    <div class="hd">
                        <div class="current">
                            <sc:Text ID="confirmCountry" Field="Phrase" runat="server"></sc:Text>
                        </div>
                        <div class="closeLinkDiv"><a class="icon icon-close-new closeLink" href="#"></a></div>
                    </div>
                    <div class="bd">
                        <div class="select-box custom-select">
                            <asp:DropDownListAttributes runat="server" ID="ConfirmCountriesDropDown">
                            </asp:DropDownListAttributes>
                        </div>
                        <input type="button" id="btnConfirmCountry" class="button" runat="server" />
                    </div>
                </div>
            </div>
            <!--googleon: all-->
        </form>		
       
            </div>
    </div>
     <div class="loading">
        <div class="sk-fading-circle">
          <div class="sk-circle1 sk-circle"></div>
          <div class="sk-circle2 sk-circle"></div>
          <div class="sk-circle3 sk-circle"></div>
          <div class="sk-circle4 sk-circle"></div>
          <div class="sk-circle5 sk-circle"></div>
          <div class="sk-circle6 sk-circle"></div>
          <div class="sk-circle7 sk-circle"></div>
          <div class="sk-circle8 sk-circle"></div>
          <div class="sk-circle9 sk-circle"></div>
          <div class="sk-circle10 sk-circle"></div>
          <div class="sk-circle11 sk-circle"></div>
          <div class="sk-circle12 sk-circle"></div>
        </div>
      </div>

     <script src="<%=CDNPath %>/appfiles/js/global.min.js?v=20191122" type="text/javascript"></script>  
	 <script src="<%=CDNPath %>/appfiles/js/navigation.js?v=20200526" type="text/javascript"></script>  
     <script  src="<%= CDNPath %>/appfiles/js/jquery-ui.min.js" type="text/javascript"></script>

     <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M6V4SVH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
    'gtm.start':
    new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-M6V4SVH');</script>
    <!-- End Google Tag Manager -->

     <% if (Sitecore.Context.Language.Name.Equals("zh-cn", StringComparison.OrdinalIgnoreCase) == false)
       { %>

	<%--Added for coveo search: Begin--%>

	<link rel="stylesheet" href="https://static.cloud.coveo.com/searchui/v1.2537/css/CoveoFullSearchNewDesign.css"/>
    <script src="https://static.cloud.coveo.com/searchui/v1.2537/js/CoveoJsSearch.Searchbox.min.js"></script>
    <script src="https://static.cloud.coveo.com/searchui/v1.2537/js/CoveoJsSearch.min.js"></script>
    <script type="text/javascript" src="https://static.cloud.coveo.com/searchui/v1.2537/js/cultures/<%= !Request.QueryString.HasKeys()||(Request.QueryString.HasKeys()&&Request.QueryString["cgzid"]==null)?"en":Request.QueryString["cgzid"].ToLower()=="zh-cn"?"zh-cn":Request.QueryString["cgzid"].ToLower()=="zh-tw"?"zh-tw":Request.QueryString["cgzid"].ToLower().Substring(0,2) %>.js"></script>


	<%--Added for coveo search: End--%>

      <script>
        function isCurrentPageSearchPage() {
            return (document.querySelector("#search") !== null);
        };

        function CoveoInitialize() {
          var root = document.body;

            Coveo.SearchEndpoint.endpoints['default'] = new Coveo.SearchEndpoint({
                restUri: 'https://platform.cloud.coveo.com/rest/search',
                accessToken: '<%= System.Configuration.ConfigurationManager.AppSettings["CoveoToken"] %>'
            });

            var searchroot = Coveo.$$(document).find("#coveosearchbox");
            Coveo.initSearchbox(searchroot, "/search/<%= Sitecore.Context.Language.Name%>/default", {
            });


            var group = $("#content_0_hdnGroupHeader").val();
            if ($("#header_0_hdnGroupHeader").val()) {
                group = $("#header_0_hdnGroupHeader").val()
            } else {
                if ($("#content_0_hdnGroupHeader").val()) {
                    group = $("#content_0_hdnGroupHeader").val()
                }
            }

            if (group && group.toLowerCase() != "corporate") {
                Coveo.state(searchroot, "hq", "@groupheader=\"" + group + "\"", {
                    silent: true
                });
            }


            //$(".icon-search-new").click(function () {
            //    //Coveo.init(root);
            //});

            //$(".menu-item").click(function () {
            //    //Coveo.init(root);
            //});
        }

        if (document.readyState === 'complete') {
            console.log('document is already ready, just execute code here');
            CoveoInitialize();
        } else {
            document.addEventListener('DOMContentLoaded', function () {
                console.log('document was not ready, place code here');
                CoveoInitialize();
            });
        }

    </script>
     <!--googleoff: all-->
            <div class="search-popup search-popup-container">
                <%--<div id="search-bar" class="search-bar hide">
                    <input type="text" id="searchbox" autocomplete="off" class="ui-autocomplete-input" maxlength="<%= MaxSearchTermLength %>"/>
                    <i id="searchbarbutton" class="icon icon-search-new"></i>                                                               
                    <a class="icon icon-close-new closeLink"></a>
                </div>--%>

            	<%--Added for coveo search: Start--%>
                 <div class="search-close"><a class="icon icon-close-new closeLink"></a></div>
                <div id="coveosearchbox">
                      <div class="coveo-search-section">
                            <div
                                id="coveosearch" 
                                class="CoveoSearchbox" 
                                data-enable-omnibox="true" 
                                data-add-search-button="true" 
                                data-enable-query-suggest-addon="true"
                                data-enable-reveal-query-suggest-addon="true"
                                data-enable-partial-match="true" 
                                data-partial-match-keywords="3"
                                data-partial-match-threshold="35%"
                                data-enable-query-syntax="false">
                            </div>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "en-us") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionenus" data-number-of-suggestions="20"></div>
                            <% } %>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "en-au") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionenau" data-number-of-suggestions="20"></div>
                            <% } %>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "en-gb") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionengb" data-number-of-suggestions="20"></div>
                            <% } %>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "en-in") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionenin" data-number-of-suggestions="20"></div>
                            <% } %>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "ja-jp") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionjajp" data-number-of-suggestions="20"></div>
                            <% } %>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "ko-kr") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionkokr" data-number-of-suggestions="20"></div>
                            <% } %>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "ru-ru") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionruru" data-number-of-suggestions="20"></div>
                            <% } %>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "zh-cn") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionzhcn" data-number-of-suggestions="20"></div>
                            <% } %>
                            <% if (Sitecore.Context.Language.Name.ToLower() == "zh-tw") {%>
                                <div class="CoveoFieldSuggestions" data-header-title="Suggestions" data-field="@querysuggestionzhtw" data-number-of-suggestions="20"></div>
                            <% } %>
                            <%--<div class="CoveoAnalyticsSuggestions" data-header-title="Suggestions" data-number-of-suggestions="20"></div>--%>
                            
                      </div>
                </div>
                <div class="CoveoAnalytics"></div>
            	<%--Added for coveo search: End--%>

            </div>
       <!--googleon: all-->
   <% } %>
    <sc:placeholder runat="server" key="ShanonScript" />  
</body>
</html>
