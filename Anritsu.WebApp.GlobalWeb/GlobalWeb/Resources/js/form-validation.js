$(document).ready(function () {
	$(".form-submit").on("mousedown", function(e){
		$(".form-submit").data("mouseDown", true);
	});

	$(".form-submit").on("mouseup", function(e){
		$(".form-submit").data("mouseDown", false);
	});
    var inCompleteForm = true;
    $(document).on("click", ".form-submit", function (e) {
        checkValidation($(this));
		if ($(".email-confirm").length == 1) {
                confirmEmail($(".email-confirm"));
		}
		if ($(".confirmPassword").length == 1) {
			confirmPassword($(".confirmPassword"));
		}
        if (!isValidForm()) {
            e.preventDefault();
        }
		
    });
    $(document).on("blur",".required input", function (e) {
		if($(".form-submit").data("mouseDown") != true || e.which == 13){
			fireValidation($(this).closest(".required"));
		}
    });
    $(document).on("blur", ".required textarea", function (e) {
		if($(".form-submit").data("mouseDown") != true || e.which == 13){
			fireValidation($(this).closest(".required"));
		}
    });
    $(document).on("blur", ".email-confirm", function (e) {
		if($(".form-submit").data("mouseDown") != true || e.which == 13){
			confirmEmail($(this));
		}
    });
    $(document).on("blur", ".confirmPassword", function (e) {
		if($(".form-submit").data("mouseDown") != true || e.which == 13){
			confirmPassword($(this));
		}
    });
	
	
    
    $(document).on("change", ".required input[type='checkbox']", function (e) {
		if($(".form-submit").data("mouseDown") != true || e.which == 13){
			fireValidation($(this).closest(".required"));
		}
    });
	/* state require */
	$(document).on("blur", ".selectRequired", function (e) {
		if($(".form-submit").data("mouseDown") != true || e.which == 13){
			selectrequire($(this));
		}
    });

    function checkValidation($that) {
        $("body").find(".required").each(function () {
            fireValidation($(this));

        });
        if ($(".error-message:visible").length == 0 && $(".custom-error-messgae:visible").length == 0 && $(".custom-error-message:visible").length == 0) {
            inCompleteForm = false;
            //$(".form-submit").click();
        }
    }
	function isValidForm(){
	if ($(".error-message:visible").length == 0 && $(".custom-error-messgae:visible").length == 0 && $(".custom-error-message:visible").length == 0) {
		return true;
	}else{
		return false;
	}
	}
    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
    function validatePhoneNumber(phoneNumber) {
        var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
        return phoneNumberPattern.test(phoneNumber);
    }
	
	function validateNumber(number) {
        var phoneNumberPattern = /^\d*$/;
        return phoneNumberPattern.test(number);
    }
	
	function validatePageName(pageName) {
        var pageNamePattertn = /^[A-Za-z0-9_-]*$/;
        return pageNamePattertn.test(pageName);
    }
	
    function validateZipcode(zipcode, regex) {
        return regex.test(zipcode);
    }

    function emailFieldValidation($that) {
        if (validateEmail($that.find(".email").val())) {
            $that.find("input").removeClass("input-error");
            $that.find(".custom-error-message").hide();
        }
        else {
            $that.find("input").addClass("input-error");
            $that.find(".custom-error-message").show();
        }
    }

    function phoneNumberValidation($that) {
        if (validatePhoneNumber($that.find(".phone-number").val())) {
            $that.find("input").removeClass("input-error");
            $that.find(".custom-error-message").hide();
        }
        else {
            $that.find("input").addClass("input-error");
            $that.find(".custom-error-message").show();
        }
    }
	
	function NumberValidation($that) {
        if (validateNumber($that.find(".number").val())) {
            $that.find("input").removeClass("input-error");
            $that.find(".custom-error-message").hide();
        }
        else {
            $that.find("input").addClass("input-error");
            $that.find(".custom-error-message").show();
        }
    }
	
	function pageNameValidation($that) {
        if (validatePageName($that.find(".page-name").val())) {
            $that.find("input").removeClass("input-error");
            $that.find(".custom-error-message").hide();
        }
        else {
            $that.find("input").addClass("input-error");
            $that.find(".custom-error-message").show();
        }
    }

    function zipcodeValidation($that, regex) {
        if (validateZipcode($that.find(".zipcode").val(), regex)) {
            $that.find("input").removeClass("input-error");
            $that.find(".custom-error-message").hide();
        }
        else {
            $that.find("input").addClass("input-error");
            $that.find(".custom-error-message").show();
        }
    }

    function fireValidation($that) {
        if ($that.hasClass("input-text")) {
            if ($that.find("input").val() == "") {
                $that.find("input").addClass("input-error");
                $that.find(".custom-error-message").hide();
                $that.find(".error-message").show();
            }
            else {
                $that.find("input").removeClass("input-error");
                $that.closest(".group").find(".error-message").hide();
                if ($that.find(".email").length == 1) {
                    emailFieldValidation($that);
                }
                if ($that.find(".phone-number").length == 1) {
                    phoneNumberValidation($that);
                }
				if ($that.find(".number").length == 1) {
                    NumberValidation($that);
                }
                if ($that.find(".zipcode").length == 1) {
                    var regex = new RegExp($that.find(".zipcode").attr("data-regex"));
                    zipcodeValidation($that, regex);
                }
				if($that.find(".page-name").length == 1){
					pageNameValidation($that);
				}
            }
        }
        if ($that.hasClass("input-textarea")) {
            if ($that.find("textarea").val() == "") {
                $that.find("textarea").addClass("input-error");
                $that.find(".error-message").show();
            }
            else {
                $that.find("textarea").removeClass("input-error");
                $that.find(".error-message").hide();
            }
        }

        if ($that.hasClass("input-password")) {
            if ($that.find("input").val() == "") {
                $that.find("input").addClass("input-error");
                $that.find(".error-message").show();
            }
            else {
                $that.find("input").removeClass("input-error");
                $that.find(".error-message").hide();
                if ($that.closest(".group").find(".strength").text() == "Weak") {
                    $that.find("input").addClass("input-error");
                    $that.find(".custom-error-message").show();
                }
                else {
                    $that.find("input").removeClass("input-error");
                    $that.find(".custom-error-message").hide();
                }
            }
            if ($(".confirmPassword").length == 1) {
                if (($(".confirmPassword").val() != "") && (($(".confirmPassword").val() != $(".newPassword").val()) || $(".newPassword").closest(".group").find(".error-message:visible").length == 1 || $(".newPassword").closest(".group").find(".custom-error-message:visible").length == 1)) {
                    $(".confirmPassword").closest(".group").find(".custom-error-message").show();
                    $(".confirmPassword").closest(".group").find("input").addClass("input-error");
                }
                else {
                    $(".confirmPassword").closest(".group").find(".custom-error-message").hide();
                    $(".confirmPassword").closest(".group").find("input").removeClass("input-error");
                    if ($(".confirmPassword").val() == "") {
                        $(".confirmPassword").closest(".group").find("input").addClass("input-error");
                    }
                }
            }
        }

        if ($that.hasClass("input-select")) {
            if ($that.find("select").val() == "") {
                $that.find("select").addClass("input-error");
                $that.find(".error-message").show();
            }
            else {
                $that.find("input").removeClass("input-error");
                $that.find(".error-message").hide();
            }
        }

        if ($that.hasClass("input-checkbox")) {
            if ($that.find("input").is(":checked") == false) {
                $that.find("input").addClass("input-error");
                $that.find(".error-message").show();
            }
            else {
                $that.find("input").removeClass("input-error");
                $that.find(".error-message").hide();
            }
        }

    }
    function confirmEmail($that) {
        if (($that.val() != "") && (($that.val() != $(".email-new").val()) || $(".email-new").closest(".group").find(".error-message:visible").length == 1 || $(".email-new").closest(".group").find(".custom-error-messgae:visible").length == 1)) {
            $that.closest(".group").find(".custom-error-message").show();
            $that.closest(".group").find("input").addClass("input-error");
        }
        else {
            $that.closest(".group").find(".custom-error-message").hide();
            $that.closest(".group").find("input").removeClass("input-error");
            if ($that.val() == "") {
                $that.closest(".group").find("input").addClass("input-error");
            }
        }
    }
	
	function selectrequire($that) {
		if($(".selectRequired option:selected" ).text() == " Select one"){
			$(".selectRequired").closest(".group").find(".error-message").show();
			}	
	}
    function confirmPassword($that) {
        if (($that.val() != "") && (($that.val() != $(".newPassword").val()) || $(".newPassword").closest(".group").find(".error-message:visible").length == 1 || $(".newPassword").closest(".group").find(".custom-error-message:visible").length == 1)) {
            $that.closest(".group").find(".custom-error-message").show();
            $that.closest(".group").find("input").addClass("input-error");
        }
        else {
            $that.closest(".group").find(".custom-error-message").hide();
            $that.closest(".group").find("input").removeClass("input-error");
            if ($that.val() == "") {
                $that.closest(".group").find("input").addClass("input-error");
            }
        }
    }
});

