﻿$(document).ready(function () {
    $('.scfSectionBorderAsFieldSet').parent('div').addClass('form-section');
    $('.scfSectionLegend').addClass('form-sub-head');


    $('.scfSingleLineTextBox,.scfMultipleLineTextBox').on('blur', function () {
        if ($(this).parent().parent().find('.scfValidatorRequired').css('display') == "inline")
            $(this).css('border', '2px solid #f5a623');
        else
            $(this).css('border', '1px solid #979797');
    });
    if ($('.scfForm').find('.scfSectionLegend').length > 0) {
        $('.scfSubmitButtonBorder').css('margin-left', '10px');
        $('.scfIntroBorder').css('margin-left', '10px');
        $('.scfForm h1').css('margin-left', '10px');
    }
    /*global params */
    var culture = window.location.href.split('/')[3];
    var region = '';
    if (culture.indexOf('-') > 0) {
        region = culture.split('-')[1];
    };
    $('.scfValidatorRequired').css('color', '#f5a623');
    $('.scfValidator').css('color', '#f5a623');
    //var countryDropdown = $('.scfCountrySelector').find('.scfDropList');
    //var stateDropdown = $('.scfStateSelector').find('.scfDropList');
    /*global params */

    function showStatesForUs() {
        $('.scfStateSelector-us').show();
        $('.scfStateSelector-jp').hide();
        $('.scfStateSingleLineText').hide();
    }
    function showStatesForJp() {
        $('.scfStateSelector-us').hide();
        $('.scfStateSelector-jp').show();
        $('.scfStateSingleLineText').hide();
    }
    function showSingleLineStateText() {
        $('.scfStateSelector-us').hide();
        $('.scfStateSelector-jp').hide();
        $('.scfStateSingleLineText').show();
    }

    function showControlsByReggion(currentRegion) {
        if (currentRegion.toLowerCase() === "us" ) {
            showStatesForUs();
        } else if (currentRegion.toLowerCase() === "jp" &&
            $('.scfCountrySelector').length > 0 && $('.scfCountrySelector').find('.scfDropList').val().toLowerCase() === "jp") {
            showStatesForJp();
        } else {
            showSingleLineStateText();
        }

        if ($('.scfCountrySelector').length>0 && $('.scfCountrySelector').find('.scfDropList').val().toLowerCase() === "us") {
            showStatesForUs();
        }
        else if ($('.scfCountrySelector').length > 0 && $('.scfCountrySelector').find('.scfDropList').val().toLowerCase() === "jp") {
            showStatesForJp();
        } else {
            showSingleLineStateText();
        }
    }

    showControlsByReggion(region);

    //bind states dropdown only if the country selected is US or Japan
    $('.scfCountrySelector').on('change', function () {
        var country = $('.scfCountrySelector').find('.scfDropList').val();
        $('.scfStateSingleLineText').find('.scfSingleLineTextBox').val("");
        //$(".scfCountrySelector select").find('option').removeAttr("selected");
        //var countrySelected = $('.scfCountrySelector').find('.scfDropList');
        //countrySelected.options[countrySelected.options.selectedIndex].setAttribute("selected", "selected");
        //$(".scfCountrySelector select option[value='AF']").attr("selected","selected");

        showControlsByReggion(country);
    });

    $('.scfValidator').each(function () {
        var msg = $(this).text();
        msg = msg.replace("*", "").replace(":", "");
        $(this).text(msg);
    });

    $('.scfRequestType input:radio').on('click', function () {
        var $this = $(this);
        $('.scfSectionContent input:text').each(function (i, elem) {
            $elem = $(elem);
            var thisLabel = $elem.closest('.scfSingleLineTextBorder').find('label'),
                isRequired = false;
            if ($elem.attr('validvalues')) {
                thisLabel.text(thisLabel.text().replace('*', ''));
                var validValues = $elem.attr('validvalues').split('|');
                for (var i = 0; i < validValues.length; i++) {

                    if ($this.val() == validValues[i]) {
                        isRequired = true;
                        thisLabel.text(thisLabel.text() + '*');
                        continue;
                    }
                }
            }
            if (!isRequired) {
                thisLabel.text(thisLabel.text().replace('*', ''));
            }
        });
    });

    $(".scfSubmitButton").on("click", function () {
        $(".scfValidatorRequired").each(function () {
            var validationMsg = $(this).text();
            validationMsg = validationMsg.replace("*", "").replace(":", "");
            $(this).text(validationMsg);

            if ($(this).css('display') === "inline") {
                $(this).closest("div").find("input[type=\"text\"],textarea").css("border", "2px solid #f5a623");
            } else {
                $(this).closest("div").find("input[type=\"text\"],textarea").css("border", "1px solid #979797");
            }
        });
        //if ($('.scfRequestType').val() != 'undefined') {
        //    $('.scfSingleLineTextBox').parent().each(function () {
        //        if ($(this).find('span:eq(3)').css('visibility') == 'visible') {
        //            $(this).find('.scfSingleLineTextBox').css('border', '2px solid #f5a623')
        //        } else {
        //            $(this).find('.scfSingleLineTextBox').css('border', '2px solid #979797')
        //        }
        //    })
        //}
        var isValid = true;
        $(".scfDropListBorder,.scfDroplistBorder").find("span[class='scfRequired']").each(function () {
            var selectedVal = $(this).closest("div").find(".scfDropList").val().toLowerCase();
            if (selectedVal === "select" || selectedVal === "-select-" || selectedVal === "--select--") {
                $(this).next().css("display", "block"); isValid = false;
            } else
                $(this).next().css("display", "none");
        });

        $(".scfProductsDdl").find("span[class='scfRequired']").each(function () {
            var selectedVal = $(this).closest("div").find(".scfDropList").val().toLowerCase();
            var enteredVal = $(".scfProductsSL").closest("div").find(".scfSingleLineTextBox").val();
            if ((selectedVal === "select" || selectedVal === "-select-" || selectedVal === "--select--") && $.trim(enteredVal).length === 0) {
                $(this).next().css("display", "block"); isValid = false;
            } else
                $(this).next().css("display", "none");
        });

        var $countryDd = $(".scfCountrySelector").find(".scfDropList"),
            selectedCountry = "";
        if ($countryDd.length > 0) {
            selectedCountry = $(".scfCountrySelector").find(".scfDropList").val().toLowerCase();
        }

        if (selectedCountry === "us") {
            $(".scfStateSelector-us").find("span[class='scfRequired']").each(function () {
                var selectedVal = $(this).closest("div").find(".scfDropList").val().toLowerCase();
                if (selectedVal === "select" || selectedVal === "-select-" || selectedVal === "--select--") {
                    $(this).next().css("display", "block"); isValid = false;
                } else
                    $(this).next().css("display", "none");
            });
        }
        if (selectedCountry === "jp") {
            $('.scfStateSelector-jp').find("span[class='scfRequired']").each(function () {
                var selectedVal = $(this).closest("div").find(".scfDropList").val().toLowerCase();
                if (selectedVal === "select" || selectedVal === "-select-" || selectedVal === "--select--") {
                    $(this).next().css("display", "block"); isValid = false;
                } else
                    $(this).next().css("display", "none");
            });
        }

        if (isValid === false)
            return false;
        return true;
    });
});

