$(document).ready(function(){
  initPage()
})

function initPage(){

  $(".tab-interface-2").each(function(i,item){
    var tab = $(item).children("ul");
    var tabContent = $(item).children(".section")
    $(tab).find("li a").each(function(index,el){
      $(el).click(function(e){
        e.stopPropagation()
        e.preventDefault()
        $(tab).find("li a").removeClass("active")
        $(el).addClass("active")
        tabContent.removeClass("active")
        tabContent.eq(index).addClass("active")
          if ($(".ie8").length > 0 && $('.product-downloads').length > 0) {
              $(".tab-interface.inner-tab.product-downloads > ul > li > a").eq(0).trigger("click");
          }
      })
    })
    $(tabContent).find(".section-title").each(function(index,el){
      $(el).click(function(e){
        e.stopPropagation()
        e.preventDefault()
        $(tab).find("li a").removeClass("active")
        $(tab).find("li a").eq(index).addClass("active")
        $(tabContent).removeClass("active")
        $(tabContent).eq(index).addClass("active")
        var toTop = $(el).offset().top
        $(document).scrollTop(toTop)
      })
    })
  })

}
