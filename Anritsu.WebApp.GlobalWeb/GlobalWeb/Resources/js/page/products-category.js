$(document).ready(function(){
  initPage()
})

function initPage(){
  window.categorySlider = $(".category-slider").slick({
      "dots": true,
      "autoplay": true,
      "autoplaySpeed": 5000,
      "speed": 500
  });
  categorySlider.on('afterChange',function(slick,slider){
    $(".category-slider-tab .item").removeClass("active")
    $(".category-slider-tab .item").eq(slider.currentSlide).addClass("active")
  })
  $(".category-slider-tab .item").each(function(i,item){
    $(item).click(function(e){
      e.stopPropagation();
      e.preventDefault();
      categorySlider.slick('slickGoTo',i)
    })
  })
  $(".products-info .action a").click(function(e){

    e.stopPropagation();
    e.preventDefault();
    var target = $(this).attr("href");
    $("body, html").animate({
      scrollTop:$(target).offset().top
    },500)
  })

  window.newProductsSlider = $('.new-products-slider').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 568,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  
}

