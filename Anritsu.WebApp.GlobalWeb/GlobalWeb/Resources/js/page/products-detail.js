$(document).ready(function(){
  initPage()
})

function initPage(){
  window.productImageSlider = $(".product-image-slider").slick({
      "dots": false,
      "slidesToShow": 3,
      "variableWidth": true,
      "infinite": true,
      "speed": 500
  });
  window.productVideoSlider = $(".product-video-slider").slick({
      "dots": false,
      "slidesToShow": 3,
      "variableWidth": true,
      "infinite": true,
      "speed": 500
  });


  window.productRelatedSlider = $(".product-related-slider").slick({
      "dots": false,
      "slidesToShow": 3,
      "variableWidth": true,
      "infinite": true,
      "speed": 500
  });
  $(".tab-interface-2").each(function(i,item){
    var tab = $(item).children("ul");
    var tabContent = $(item).children(".section")
    $(tab).find("li a").each(function(index,el){
      $(el).click(function(e){
        e.stopPropagation()
        e.preventDefault()
        $(tab).find("li a").removeClass("active")
        $(el).addClass("active")
        tabContent.removeClass("active")
        tabContent.eq(index).addClass("active")
        if ($(".ie8").length > 0 && $('.product-downloads').length > 0) {
            $(".tab-interface.inner-tab.product-downloads > ul > li > a").eq(0).trigger("click");
        }
      })
    })
    $(tabContent).find(".section-title").each(function(index,el){
      $(el).click(function(e){
        e.stopPropagation()
        e.preventDefault()
        $(tab).find("li a").removeClass("active")
        $(tab).find("li a").eq(index).addClass("active")
        $(tabContent).removeClass("active")
        $(tabContent).eq(index).addClass("active")
        var toTop = $(el).offset().top
        $(document).scrollTop(toTop)
      })
    })
  })


  var popupTitle = $(".product-detail h1.cate").html()
  $('.product-image-slider .slider:not(.slick-cloned) a').magnificPopup({
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
      titleSrc: function(item) {
        return popupTitle;
      }
    }
  });
  $(".select-mobile select").change(function(){
    console.log(this.value)
    $(this).parent().parent().find("ul li a").eq(this.value).click()
  })


}
