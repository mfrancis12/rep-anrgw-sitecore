﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutLandingBanner.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.AboutLandingBanner" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Constants" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>

<% if (Model.Content.BannerImage!=null && !string.IsNullOrEmpty(Model.Content.BannerImage.Src))
                     { %>
<div class="full-container multiProCls">
    <div class="hero-banner-3" style="background-image: url('<%=Model.Content.BannerImage.Src%>')">
        <div class="hero-content">
            <h1 class="<%=Editable(x=>x.Content.GetTextColor()) %>"><%=Editable(x=>x.Content.BannerTitle) %></h1>
            <p class="<%=Editable(x=>x.Content.GetTextColor()) %>"><%=Editable(x=>x.Content.BannerDescription) %></p> 
        </div>
    </div>
</div>
<% } %>
<%else 
  {%>
<div class="full-container container">
    <div class="products-heading">
        <div class="products-title">
          <h1 style="color: #289b7d;"><%=Editable(x=>x.Content.BannerTitle) %></h1></div>
            <div class="products-description"><p><%=Editable(x=>x.Content.BannerDescription) %></p> </div>
        </div>
    </div>
 <% }
   %>
