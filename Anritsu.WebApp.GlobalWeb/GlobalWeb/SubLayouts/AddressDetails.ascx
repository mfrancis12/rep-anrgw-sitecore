﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressDetails.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.AddressDetails" %>
<div class="tab-interface contact-tab">
    <ul>
        <asp:Repeater ID="AddressTypes" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
            <ItemTemplate>
                <li><a href="#section<%#Item.Id.ToString()%>" class="<%#Container.ItemIndex==0?"active":"" %>"><span><%#Editable(Item,x=>x.Value) %></span></a></li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <asp:Repeater ID="Addresses" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
        <ItemTemplate>
            <div id="section<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?"active":"" %>">
                <div class="contact-item">
                    <asp:Repeater ID="ContactDetails" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IAddressDetails" DataSource="<%#GetContactDetails(Item) %>">
                        <ItemTemplate>
                            <p class="name"><%#Editable(Item,x=>x.Title) %></p>
                            <p><%#Editable(Item,x=>x.Details) %></p>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
