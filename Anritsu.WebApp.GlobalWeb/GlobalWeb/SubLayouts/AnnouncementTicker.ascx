﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnnouncementTicker.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.AnnouncementTicker" %>
<%@ Import Namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>
    <div id="ticker" class="announcement" runat="server">
        <asp:Repeater ID="pimticker" runat="server" >
            <ItemTemplate>
            
                 <div>
                   
					   <a href="<%# Eval("Link").ToString() %>" class="text" title="<%# Eval("Title").ToString() %>">
                        <b> <%# Eval("Title").ToString() %>: </b><%# Eval("Text").ToString() %>
                     </a>
           
                 </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

