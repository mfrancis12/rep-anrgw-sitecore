﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerWithThumbnail.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Banners.BannerWithThumbnail" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<div class="full-container tech-home">
    <div style="background-image: url('<%=Model.BannerImage.Src%>')" class="hero-banner-2">
        <div class="hero-content">
            <p>
                <%=RenderImage(x=>x.BannerThumbnail) %>
            </p>
            <h1 class="<%=Model.GetTextColor()%>"><%=Editable(x=> x.BannerTitle)%></h1>
            <p class="<%=Model.GetTextColor()%>">
                <%=Editable(x=> x.BannerDescription)%>
            </p>
        </div>
    </div>
</div>
