﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Carousel.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Carousel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<div class="full-container nogap">
    <div class="home-slider slick">
        <asp:Repeater runat="server" ID="RepeaterCarousel" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IImageBlock">
            <ItemTemplate>
                <div style="background-image: url('<%#Item.Image.Src %>')" class="slider">
                    <div class="inner-table">
                        <div class="table-cell">
                            <h2 class="<%#Editable(Item,x=>x.GetTextColor()) %>"><%#Editable(Item, x=>x.Title) %></h2>
                            <p class="description <%#Editable(Item,x=>x.GetTextColor()) %>"><%#Editable(Item, x=>x.Description) %></p>
                            <div class="link"><%# RenderLink(Item,x=>x.Link,new { @class = "button"}, true)%></div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
