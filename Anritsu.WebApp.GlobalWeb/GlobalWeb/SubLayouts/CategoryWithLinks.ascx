﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryWithLinks.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.CategoryWithLinks" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="products-tech">
    <asp:Repeater ID="Categories" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICategoryWithLinks">
        <ItemTemplate>
            <div class="item">
                <div class="inner">
                    <h3 class="title"><%#Editable(Item,x=>x.Title) %></h3>
                    <div class="content">
                        <div class="text">
                            <div class="img">
                                <%#Editable(Item,x=>x.Image,new {@class="img100"} ) %>
                            </div>
                            <p class="more">
                                <%#Editable(Item,x=>x.Description)%>
                            </p>
                        </div>
                        <%--<% if (Item.CategoryLinks!=null && Item.CategoryLinks.Count()>0)--%>
                       <%-- <% if (Editable(Item,x=>x.CategoryLinks)!=null && Editable(Item,x=>x.CategoryLinks).Count()>0)
               { %>--%>
                        <div class="device">
                            <asp:Repeater ID="Categorylinks" runat="server" OnItemDataBound="CategoryLinks_ItemDataBound1" >
                                <ItemTemplate>
                                    <a id="CategoryLink" runat="server"></a>
                                </ItemTemplate>
                            </asp:Repeater>

                        </div>
                       <%-- <% } %>--%>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
