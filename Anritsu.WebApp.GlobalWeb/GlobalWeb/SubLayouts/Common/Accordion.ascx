﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Accordion.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Common.Accordion" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<asp:Repeater ID="RepeaterAccordion" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IAccordionPanel">
    <HeaderTemplate>
        <div class="accordion">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="accordion-item <%#Item.GetPanelClass() %>">
            <div class="accordion-title">
                <%#Editable(Item,x=>x.Title) %>
                <div class="accordion-icon">
                    <div class="icon <%#Item.GetPanelIconClass() %>"></div>
                </div>
            </div>
            <div class="accordion-content">
                <%#Editable(Item,x=>x.Description) %>   
            </div>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>
