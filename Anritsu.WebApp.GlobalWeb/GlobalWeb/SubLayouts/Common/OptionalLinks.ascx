﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OptionalLinks.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Common.OptionalLinks" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>

<% if (Model.OptionalLinks != null && Model.OptionalLinks.Any() && Model.OptionalLinks.Count() != 0)
   { %>
    <h3><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
    <% foreach (Glass.Mapper.Sc.Fields.Link link in Model.OptionalLinks)
        { %>
        <p>
            <%= link.RenderLink() %>
            <i class="icon icon-arrow-blue"></i>
        </p>
     <% } %>
 <% } %>
