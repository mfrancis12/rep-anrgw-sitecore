﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComponentsAccessoriesFamily.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ComponentsAccessoriesFamily" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>


<div class="container">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.Title)%></h1>
            <% if (!string.IsNullOrEmpty(Model.BannerImage.Src))
               { %>
            <p>
                <img src="<%=Model.BannerImage.Src%>" class="img100">
            </p>
            <% } %>
            <%=Editable(x=>x.Description) %>
            

            <% if (Model.Products.Count()!=0)
               { %>
             <h2 class="margin-top50"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "products") %></h2>
            <div class="products-intro">
                <asp:Repeater ID="Products" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IProductRegional">
                    <ItemTemplate>
                        <div class="products-item">
                              <a href="<%#Item.Url%>">
                                <img src="<%#Editable(Item, x=>x.SelectProduct.Thumbnail.Src) %>">
                            <h3><%#Editable(Item, x=>x.SelectProduct.ModelNumber) %></h3>
                             <p><%#Editable(Item, x=>x.SelectProduct.ProductName) %></p></a>
                            <p><%#Editable(Item, x=>x.SelectProduct.ProductDescriptor) %></p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>            
             <% } %>      

        </div>

        
        <div class="content-aside">
            <p class="mobile">
                <img src="<%= Model.Image.Src%>">
            </p>
            <% if (Model.OptionalLinks.Count()!=0)
               { %>
            <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
            <asp:Repeater ID="optionalLinks" runat="server" OnItemDataBound="optionalLinks_ItemDataBound">
                <ItemTemplate>
                    <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
            <% } %>

             <% if (Model.RelatedLinks.Count()!=0)
               { %>
            <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "RelatedLinks") %></h3>

            <asp:Repeater ID="relatedLinks" runat="server" OnItemDataBound="relatedLinks_ItemDataBound">
                <ItemTemplate>
                    <p><a id="RelatedLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
            <% } %>

        </div>
        
    </div>
</div>
