﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComponentsAccessoriesLanding.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ComponentsAccessoriesLanding" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="container">
    <div class="landing-items">
        <asp:Repeater runat="server" ID="ComponentsAndAccessories" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IComponentsAccessoriesFamily">
            <ItemTemplate>
            <div class="item">
                <div class="inner">

                    <div class="hd">

                        <h2><a href="<%# GetLink(Item) %>"><%# Editable(Item,x=>x.Title) %></a></h2>
                    </div>
                    <div class="img">
                        <a href="<%# GetLink(Item) %>">
                            <%--<img src="static/img/landing/landing-item-1.jpg">--%>
                            <%# RenderImage(Item, x=>x.Thumbnail,isEditable:true) %>
                        </a>
                    </div>
                    <div class="bd">
                        <p><%# Editable(Item, x=>x.Description) %> </p>
                        <p class="link"><a href="<%# GetLink(Item) %>"><%# Editable(Item,x=>x.Title) %><i class="icon icon-arrow-blue"></i></a></p>
                    </div>
                </div>
            </div>
                </ItemTemplate>
        </asp:Repeater>
    </div>
     <sc:placeholder runat="server" key="RightRail" />
</div>
