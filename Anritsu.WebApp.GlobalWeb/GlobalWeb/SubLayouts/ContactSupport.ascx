﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactSupport.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ContactSupport" %>
<div style="margin-bottom: 0px;" class="container">
    <div class="contact-us-container">
        <div class="contact-us">
            <div class="hd">
                <h1><%=Editable(x=>x.PageTitle) %></h1>              
                <p id="region" runat="server"><asp:Label ID="labelCountryName" runat="server"></asp:Label> <a  href="<%=SitecoreContext.GetItem<Anritsu.WebApp.GlobalWeb.Models.IModelBase>(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.ChangeRegion).Url+"?ReturnUrl="+Model.Url %>"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","ChangeRegion") %></a></p>
            </div>
            <div class="bd">
                <div class="left">
                    <sc:Placeholder Key="AddressDetails" runat="server" />
                </div>
                <div class="right">
                    <div class="item">
                        <div class="buttonWrap">
                           <%=RenderLink(x=>x.Link, new { @class = "button"}, isEditable: true, contents:Editable(x=>x.Link.Text))%>                          
                        </div>
                    </div>
                    <%=Editable(x=>x.WhatCanWeHelpYouWith) %>                    
                </div>
            </div>
        </div>
    </div>
</div>