﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentBlocks.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ContentBlocks" %>
<%@ Register Assembly="SitecoreExperienced.SitecoreControls" Namespace="SitecoreExperienced.SitecoreControls" TagPrefix="scexp" %>

<div class="container">
    <div class="products-list">
        <div class="row">
            <div class="item">
                <scexp:WFDynamicPlaceholder id="ContentBlockLeft" key="ContentBlockLeft" runat="server" />
            </div>
            <div class="item">
                <scexp:WFDynamicPlaceholder id="ContentBlockRight" key="ContentBlockRight" runat="server" />
            </div>
        </div>
    </div>
</div>
