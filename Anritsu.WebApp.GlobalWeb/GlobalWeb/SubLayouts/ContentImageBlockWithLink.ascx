﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentImageBlockWithLink.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.BuyHome" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="container">
    <div class="landing-items">
        <asp:Repeater ID="BuyContentBlocks" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IContentImageBlockWithLink">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <div class="hd">
                         
                           <h2><a href="<%#GetLink(Item)%>" class="<%# GetTitleLinkClass(Item)%>"><%# Editable(Item,x=>x.Title) %></a></h2>
                        </div>
                        <div class="img">
                            <a href="<%#GetLink(Item)%>">
                             <%#RenderImage(Item,x=>x.Image,isEditable:true) %>
                            </a>
                        </div>
                        <div class="bd">
                            <p><%# Editable(Item,x=>x.Description) %></p>
                            <p class="link">
                                <%# RenderLink(Item,x=>x.Link, isEditable: true,contents: Editable(Item,x=>x.Link.Text)+ "<i class='icon icon-arrow-blue'></i>")%>
                            </p>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
     <sc:placeholder runat="server" key="RightRail" />
</div>
