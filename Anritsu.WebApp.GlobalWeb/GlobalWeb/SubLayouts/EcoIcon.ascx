﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EcoIcon.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.EcoIcon" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Constants" %>

<div class="eco">
<% if (Model.Image!=null&&!string.IsNullOrEmpty(Model.Image.Src))
                     { %>
 <a href="<%=Editable(Model,x=>x.Link.Url) %>">
    <img src="<%=Editable(Model,x=>x.Image.Src) %>"/>
</a>
<% } %>
    </div>
