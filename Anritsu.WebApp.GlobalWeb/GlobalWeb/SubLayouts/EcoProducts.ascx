﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EcoProducts.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.EcoProducts" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>


<div class="container">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.Title)%></h1>
            <% if (!string.IsNullOrEmpty(Model.BannerImage.Src))
               { %>
            <p>
                <img src="<%=Model.BannerImage.Src%>" class="img100">
            </p>
            <% } %>
            <%=Editable(x=>x.Description) %>
                       
             <h2 class="margin-top50"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ecoproducts") %></h2>

            <div class="products-intro">               
                <asp:Repeater ID="ecoProducts" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IEcoProductDetails">
                    <ItemTemplate>
                        <div class="products-item">
                              <a href="<%#Editable(Item, x=>x.Link.Url) %>">
                                <img src="<%#Editable(Item, x=>x.ProductThumbnail.Src) %>"></a>
                            <h3><%#Editable(Item, x=>x.ModelName) %></h3>
                            <p><%#Editable(Item, x=>x.ModelNumber) %></p>
                             <p><%#Editable(Item, x=>x.ShortText) %></p>
                            <p><%#Editable(Item, x=>x.RegisteredYear) %></p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            

       
 </div>
        
        <div class="content-aside">
            <p class="mobile">
                <img src="<%= Model.Image.Src%>">
            </p>
            <% if (Model.Links.Count()!=0)
               { %>
            <h3><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
            <asp:Repeater ID="optionalLinks" runat="server" OnItemDataBound="optionalLinks_ItemDataBound">
                <ItemTemplate>
                    <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
            <% } %>
        </div>
       
   
</div>
    </div>
    
