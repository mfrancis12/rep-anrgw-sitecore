﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ErrorPage" %>
<div id="divContent"  runat="server">

   <h1><%= Sitecore.Context.Item.Fields["PageTitle"] %></h1>
    <div  >
        <asp:Literal ID="ltrLndngPgBody" runat="server"></asp:Literal>
    </div>
</div>