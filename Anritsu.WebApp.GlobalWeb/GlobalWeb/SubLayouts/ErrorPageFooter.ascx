﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorPageFooter.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ErrorPageFooter" %>


 <div class="min-container site-info" style="padding: 0; border: 0px;">
    <div class="copy-right" style="position: static; width: auto">
	    <div>
            <div class="icon icon-logo-small"></div>
            <br>
            <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Copyright")%> <%=DateTime.Now.ToString("yyyy") %>
            <br>
		</div>
    </div>
</div>