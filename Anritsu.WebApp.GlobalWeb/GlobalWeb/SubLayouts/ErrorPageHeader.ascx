﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorPageHeader.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ErrorPageHeader" %>

 <header id="global-header">
   <h1 class="header__logo">
    <a href="/home"><img src="<% =CDNPath %>/appfiles/img/icons/angw-h-nav-sprite-homelogo.png">
    </a>
   </h1>
</header>
<!--googleon: all-->
