﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FAQ.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.FAQ" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<div class="container">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%= Translate.TextByDomain("GlobalDictionary", "anritsufaqs") %></h1>
            <% if (FrequentlyAskedQuestion != null)
               {%>
            <%=Editable(FrequentlyAskedQuestion,x=>x.Question)%>
            <input type="hidden" id="hdnQuestion" runat="server"/>
            <input type="hidden" id="hdnFaqId" runat="server"/>
            <div class="accordion faq-accordion">
                <div class="accordion-item open">
                    <div class="accordion-title">
                        <%= Translate.TextByDomain("GlobalDictionary", "answer") %>
                        <div class="accordion-icon">
                            <div class="icon icon-minus-grey"></div>
                        </div>
                    </div>
                    <div class="accordion-content linkcolor">
                        <%=Editable(FrequentlyAskedQuestion,x=>x.Answer)%>
                        <input type="hidden" id="hdnAnswer" runat="server"/>
                    </div>
                </div>

                <asp:Repeater ID="DownloadCategoryTypes" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                    <ItemTemplate>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <%# Editable(Item, x=>x.Value) %>
                                <div class="accordion-icon">
                                    <div class="icon icon-plus-grey"></div>
                                </div>
                            </div>
                            <div class="accordion-content section-height">
                                <ul>
                                    <asp:Repeater ID="DownloadCategories" runat="server" DataSource="<%# BindDownloads(Item.Key) %>" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                                        <ItemTemplate>
                                            <li>
                                                <h4 class="padding20"><%# Editable(Item, x=>x.Value) %></h4>
                                                <ul class="accordion-content-list no-left-padding">
                                                    <asp:Repeater ID="DownloadList" runat="server" DataSource="<%# BindDownloadList(Item.Key) %>" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.DownloadSearchItem">
                                                        <ItemTemplate>
                                                            <li>
                                                                <p>
                                                                    <asp:HyperLink runat="server" id="LibraryFileDownloadLink" Target="_blank" NavigateUrl="<%# Item.GetItemUrl()%>" ><%# Editable(Item,x=>x.Title) %></asp:HyperLink>
                                                                    <asp:HyperLink runat="server" id="LibraryFileNewVersionDownloadLink" Target="_blank" CssClass="new-version" Visible="<%# Item.IsNewVersionAvailable %>" NavigateUrl="<%# Item.GetReplacementItemUrl()%>" ><%=Translate.TextByDomain("GlobalDictionary", "newerversionavailable") %></asp:HyperLink>
                                                                </p>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div runat="server" class="accordion-item" id="relatedDocument">
                    <div class="accordion-title">
                        <%= Translate.TextByDomain("GlobalDictionary", "otherrelateddocuments") %>
                        <div class="accordion-icon">
                            <div class="icon icon-plus-grey"></div>
                        </div>
                    </div>

                    <div class="accordion-content section-height">
                        <ul>
                            <asp:Repeater ID="OtherDownloadCategoryTypes" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                                <ItemTemplate>
                                    <asp:Repeater ID="OtherCategories" runat="server" DataSource="<%# BindDownloads(Item.Key) %>" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                                        <ItemTemplate>
                                            <li>
                                                <h4 class="padding20"><%# Editable(Item, x=>x.Value) %></h4>
                                                <ul class="accordion-content-list no-left-padding">
                                                    <asp:Repeater ID="OtherDownloadList" runat="server" DataSource="<%# BindDownloadList(Item.Key) %>" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.DownloadSearchItem">
                                                        <ItemTemplate>
                                                            <li>
                                                                <p>
                                                                    <asp:HyperLink runat="server" id="OtherCategoryLibraryFileDownloadLink" Target="_blank" NavigateUrl="<%# Item.GetItemUrl()%>" ><%# Editable(Item,x=>x.Title) %></asp:HyperLink>
                                                                    <asp:HyperLink runat="server" id="OtherCategoryLibraryFileNewVersionDownloadLink" Target="_blank" CssClass="new-version" Visible="<%# Item.IsNewVersionAvailable %>" NavigateUrl="<%# Item.GetReplacementItemUrl()%>" ><%=Translate.TextByDomain("GlobalDictionary", "newerversionavailable") %></asp:HyperLink>
                                                                </p>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
        <div class="content-aside <%= HideOptionalLinks %>">
            <p class="mobile">
                <img src="./static/img/content-text/optional_links.png">
            </p>
            <h3><%= Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
            <asp:Repeater ID="FaqOptionalLinks" runat="server" OnItemDataBound="FaqOptionalLinks_ItemDataBound">
                <ItemTemplate>
                    <p><a id="otherLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
