﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs"  Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Header" %>

<%@ Register Assembly="Anritsu.WebApp.GlobalWeb" Namespace="Anritsu.WebApp.GlobalWeb.WebControls"
    TagPrefix="anritsu" %>

<!--googleoff: all-->

 <header id="global-header">
   <h1 class="header__logo">
    <a href="<%= GetHomeItem %>"><img src="<% =CDNPath %>/appfiles/img/icons/anritsu-logo.svg">
    </a>
       
</h1>
    
    <button class="header__menu-trigger">
      <span class="menu-trigger__hamburger"></span>
      <span class="menu-trigger__label">Menu</span>
    </button>
     <input type="hidden" id="hdnIsCorporate" runat="server" />
            <input type="hidden" id="hdnGroupHeader" runat="server" />
    <ul class="util-links">
      <li class="util__search" title="<%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Search") %>">
        <button class="util__search__btn">
          <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Search") %></span>
        </button>
        <span class="util__search__tooltip"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Search") %></span>
        <div class="util__search__query-entry">
              <input type="hidden" id="hdnsearchplaceholder" value="<%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Searchboxplaceholder") %>" />
        </div>
		
      </li>

     <li id="myanritsumenu" runat="server" class="util__my-anritsu--is-active"  title="My Anritsu">
         <button class="util__my-anritsu__btn">
         <img   src="<% =CDNPath %>/appfiles/img/icons/angw-h-nav-sprite-myanritsu-logout.png" width="23" height="23" id="myanritsuicon" runat="server"       >
            <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "myanritsu") %></span>
        </button>
        
        <div class="util__my-anritsu__wrapper">
          <p class="util__my-anritsu__title"><%=GetLoggedInUserName()%></p>
          <button class="util__my-anritsu__close"><span class="btn--close"></span></button>
		  <%=buildmyanritsumenu()%>
            
        </div>
    
     </li>
    

      <li class="util__contact-us" title=<%= Sitecore.Globalization.Translate.TextByDomain("contactus", "ContactUs") %>>
        <a id="linkContactUsDesktop" runat="server" href="#" class=util__contact-us__btn>
          <span><%= Sitecore.Globalization.Translate.TextByDomain("contactus", "ContactUs") %></span>
        </a>
        <span class="util__contact-us__tooltip"><%= Sitecore.Globalization.Translate.TextByDomain("contactus", "ContactUs") %></span>
      </li>
      <li class="util__change-region" title="<%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "VisitAnotherCountry") %>">
        <button class="util__change-region__btn">
          <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "VisitAnotherCountry") %></span>
        </button>
        <span class="util__change-region__tooltip"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "VisitAnotherCountry") %></span>
        <p class="util__change-region__current"><sc:Text ID="SelectedCountryName" Field="CountryName" runat="server"></sc:Text></p>
       
      </li>
    </ul>
       <sc:placeholder runat="server" key="Menu" />
    </header>

<!--googleon: all-->


