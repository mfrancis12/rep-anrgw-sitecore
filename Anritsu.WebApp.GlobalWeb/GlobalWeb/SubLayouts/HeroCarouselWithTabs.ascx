﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeroCarouselWithTabs.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.HeroCarouselWithTabs" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Common" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Pages" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div id="carouselheader" runat="server" class="full-container nogap">
    <div class="page-heading">
        <div class="inner">
            <div class="heading-text">
                <h1 class="title"><%=Editable(SitecoreContext.GetCurrentItem<IHeroCarouselMapping>(),x=>x.PageTitle) %></h1>
            </div>
            <div class="heading-button">
               <a href=" <%=Editable(SitecoreContext.GetCurrentItem<ILink>(),x=>x.Link.Url) %>">
                   <%=Editable(SitecoreContext.GetCurrentItem<ILink>(),x=>x.Title) %>
               </a>
            </div>
        </div>
    </div>
</div>
<div class="full-container nogap">
    <div class="category-slider slick">
        <asp:Repeater ID="sliders" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICarousel">
            <ItemTemplate>
                <div class="slider">
                    <div class="product-image">
                        <%#RenderImage(Item,x=>x.Image,isEditable:true) %>
                    </div>
                    <div class="text">
                        <div class="inner">
                            <div class="name <%#Editable(Item,x=>x.GetTextColor()) %>"><%#Editable(Item, x=>x.Title) %></div>
                            <div class="sub-title <%#Editable(Item,x=>x.GetTextColor()) %>"><%#Editable(Item, x=>x.Subtitle) %></div>
                            <div class="description <%#Editable(Item,x=>x.GetTextColor()) %>">
                                <%#Editable(Item, x=>x.Description) %>
                            </div>
                            <div class="link">
                                <%# RenderLink(Item, x=>x.Link,new { @class ="button"}, true,Editable(Item, x=>x.Link.Text))%>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
     <div class="category-slider-tab">
        <asp:Repeater ID="categories" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICarousel">
            <ItemTemplate>
                <div class="item <%#Container.ItemIndex==0?"active":"" %>">
                    <a href="javascript:void(0)"><%#Editable(Item, x=>x.PanelName) %></a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
<div class="container">
    <div class="tab-interface-2 product-detail-tab">
        <ul>
            <asp:Repeater ID="Tabs" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
                <ItemTemplate>
                    <li><a href="#<%#Item.Id.ToString()%>" class="<%#Container.ItemIndex==0?"active":""%>"><span><%#Editable(Item,x=>x.Title)%></span></a></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <asp:Repeater ID="TabsDescription" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
            <ItemTemplate>
                <div id="<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?"active":""%>">
                    <div class="section-title"><span><%#Editable(Item,x=>x.Title) %></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
                    <div class="section-content detail-table">
                        <%#Editable(Item,x=>x.Description) %>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
