﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IconsWithLinks.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.IconsWithLinks" %>
<div class="container">
    <div class="landing-help">
        <asp:Repeater ID="rptLandingHelp" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IIconLinkWithTitle">
            <ItemTemplate>
                <div class="item">
                    <i class="icon  <%#GetCssClass(Editable(Item, x=>x.Icon)) %>"></i>
                    <%#RenderLink(Item,x => x.Link, new System.Collections.Specialized.NameValueCollection {{"class","button"}}) %>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
