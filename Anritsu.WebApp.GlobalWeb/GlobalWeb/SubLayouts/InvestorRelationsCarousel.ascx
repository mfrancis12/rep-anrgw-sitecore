﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvestorRelationsCarousel.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.InvestorRelationsCarousel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="full-container nogap">
    <div class="page-heading">
        <div class="inner">
            <div class="heading-text">
                <h1 class="title"><%=Editable(x=> x.Content.PageTitle) %></h1>
            </div>
            <div class="heading-button">
                <asp:Repeater ID="buttons" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.ILink">
                    <ItemTemplate>
                        <a href=" <%#Editable(Item, x=>x.Link.Url) %>" class="button"><%#Editable(Item, x=>x.Title) %></a>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            </div>
    </div>
</div>
<div class="full-container nogap">
    <div class="category-slider slick">
        <asp:Repeater ID="sliders" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICarousel">
            <ItemTemplate>
                <div class="slider">
                    <div class="product-image">
                        <%#RenderImage(Item,x=>x.Image,isEditable:true) %>
                    </div>
                    <div class="text">
                        <div class="inner">
                            <div class="name"><%#Editable(Item, x=>x.Title) %></div>
                            <div class="sub-title"><%#Editable(Item, x=>x.Subtitle) %></div>
                            <div class="description">
                                <%#Editable(Item, x=>x.Description) %>
                            </div>
                             </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>