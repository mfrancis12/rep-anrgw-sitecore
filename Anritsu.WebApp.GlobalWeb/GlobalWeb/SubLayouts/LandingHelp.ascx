﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandingHelp.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.LandingHelp" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Constants" %>

     <div class="landing-help">
        <asp:Repeater ID="RepeaterHelpIcons" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IIconLinkWithTitle">
            <ItemTemplate>              
              <div class="item ">
                  <i class="icon <%#GetClass(Item.Icon.ToString()) %>"></i>
                 
                   <%# RenderLink(Item, x=>x.Link,new { @class = "button" }, isEditable: true, contents:Editable(Item,x=>x.Title))%>
               
              </div>
              </ItemTemplate>
        </asp:Repeater>
    </div>

    