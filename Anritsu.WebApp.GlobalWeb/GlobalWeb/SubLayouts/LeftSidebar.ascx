﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftSidebar.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.LeftSidebar" %>
<%@ Register tagPrefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel"  %>

<div class="leftside">
    <asp:Repeater ID="Teasers" runat="server">
        <ItemTemplate>
            <div class="bannerlink">
                <sc:Link runat="server" Field="Link" Item="<%#(Container.DataItem as Sitecore.Data.Items.Item) %>">
                    <sc:Image  runat="server" Field="Image" Item="<%#(Container.DataItem as Sitecore.Data.Items.Item) %>"></sc:Image>
                </sc:Link>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>