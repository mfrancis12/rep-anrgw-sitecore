﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LinkManagerPromotions.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.LinkManagerPromotions" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="container">
   <div class="hero-menu">
        <asp:Repeater ID="RepeaterLinks" runat="server">
            <ItemTemplate>
                <div class="item"><a href="<%#Eval("TargetURL") %>"><img src="<%#GetImageUrl(Convert.ToString(Eval("ImageURL"))) %>" alt="<%#Eval("AltText") %>"/></a>
                <h2 class="link"><a href="<%#Eval("TargetURL") %>"><%#Eval("LinkName") %><i class="icon icon-arrow-grey"></i></a></h2>
              </div>             
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>