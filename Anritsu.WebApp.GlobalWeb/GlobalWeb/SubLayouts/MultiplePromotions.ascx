﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiplePromotions.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.MultiplePromotions" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Constants" %>

<div class="container">
    <div class="promo-items" id="promo">
        <asp:Repeater ID="PromotionsList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IImageBlockWithContent">
            <ItemTemplate>
               <div class="<%# !string.IsNullOrEmpty(Item.Image.Src)?"item":"item-noimg" %>">                        
                        <div class="<%# !string.IsNullOrEmpty(Item.Image.Src)?"img":"hiddentxt" %>">
                             <a href="<%#GetLink(Item)%>" class="<%# GetTitleLinkClass(Item)%>">
                            <img src="<%#Editable(Item, x=>x.Image.Src) %>"></a>
                    </div>
                    <div class="text">
                         <a href="<%#GetLink(Item)%>" class="<%# GetTitleLinkClass(Item)%>">                                               
                        <h2><%#Editable(Item,x=> x.Title) %></h2></a>
                        <p class="more"><%#Editable(Item, x=> x.Description) %></p>
                        <p class="<%#GetLinkClass(Item)%>"> 
                            <a href="<%#GetLink(Item)%>" class="<%# GetTitleLinkClass(Item)%>">
                                <b><%#GetLinkText(Item)%></b>
                                <%--<i class="icon icon-arrow-blue"></i>--%>
                                <i class="<%#GetLinkIcon(Item)%>"></i>
                            </a>
                        </p>
                      
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <sc:placeholder runat="server" key="LandingHelp" />
</div>
