﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewProducts.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.NewProducts" %>
<div id="section2" class="section <%=Sitecore.Context.Item.TemplateID.ToString().Equals(Anritsu.WebApp.GlobalWeb.Constants.TemplateIds.ProductCategory)?"active":""%>">
    <div class="new-products-slider slick">
        <asp:Repeater ID="ProductList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IProductRegional">
            <ItemTemplate>
                <div class="slider">
                    <h2 class="image"><a href="<%#Item.Url %>"><%#RenderImage(Item,x=>x.SelectProduct.Thumbnail) %></h2>
                    <h2 class="title"><a href="<%#Item.Url %>"><%#Item.SelectProduct.ProductName%></a></h2>
                    <div class="sub-title"><a href="<%#Item.Url %>"><%#Item.SelectProduct.ModelNumber%></a></div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
