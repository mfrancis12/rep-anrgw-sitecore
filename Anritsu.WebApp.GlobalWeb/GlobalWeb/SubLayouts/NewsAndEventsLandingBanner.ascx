﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsAndEventsLandingBanner.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.NewsAndEventsLandingBanner" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<div class="full-container multiProCls">
    <div class="multi-promo-filter-hero-banner" style="background-image: url('<%=Model.GetImagePath()%>')">
        <div class="hero-content">
            <h1 class="multy-promo-filter-title <%=Model.GetTextColor() %>"><%=Editable(x=>x.BannerTitle) %></h1>
            <p class="<%=Model.GetTextColor() %>"><%=Editable(x=>x.BannerDescription) %></p>

        </div>
    </div>
</div>
