﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnlineWebinarVideos.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.OnlineWebinarVideos" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="full-container video-container nogap">
    <sc:Sublayout runat="server" Id="BrightcoveMediaControl" Path="/layouts/MediaFramework/Sublayouts/EmbedMediaPlayer.ascx"></sc:Sublayout>
</div>
<div class="left-content">
            <h1>
                <%=Editable(Model,x=>x.PageTitle)%>
            </h1>
    <br />
    </div>