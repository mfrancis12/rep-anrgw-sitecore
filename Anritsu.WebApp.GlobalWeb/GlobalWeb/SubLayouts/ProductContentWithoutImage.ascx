﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductContentWithoutImage.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ProductContentWithoutImage" %>
<h2 class="title">
    <a href="<%=Editable(x=> x.Link.Url) %>">
        <%=Editable(x=> x.Title) %>
    </a>
</h2>
<div class="text">
    <p>
        <b>
            <%=Editable(x=> x.Subtitle) %>
        </b>
        <%=Editable(x=> x.Description) %>
    </p>
    <p class="align-right">
        <a href="<%=Editable(x=> x.Link.Url) %>" class="learnmore"><b><%=Editable(x=> x.Link.Text) %></b><i class="icon icon-arrow-blue"></i></a>
    </p>
</div>



