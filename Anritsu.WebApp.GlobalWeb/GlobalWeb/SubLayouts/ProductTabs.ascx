﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductTabs.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ProductTabs" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="container">
    <div class="tab-interface-2 <%= GetTabClass %>">
        <ul>
            <li><%=GetOverViewTab()%></li>
            <asp:Repeater ID="ProductTabsList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
                <ItemTemplate>
                    <li><a href="#<%#Item.Id.ToString()%>" <%# string.IsNullOrEmpty(GetOverViewTab()) && Container.ItemIndex == 0 ? "class=\"active\"" : "" %>><span><%#Editable(Item,x=>x.Title)%></span></a></li>
                </ItemTemplate>
            </asp:Repeater>
            <li class="<%=ShowHideLibrary %>"><a href="#Library"><span><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "library") %></span></a></li>
        </ul>
        <asp:Repeater ID="OverviewItems" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
            <ItemTemplate>
                <div id="<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?"active":"" %>">
                    <div class="section-title"><span><%#Editable(Item,x=>x.Title)%></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
                    <div class="section-content detailSection">
                        <div class="tab-interface inner-tab">
                            <ul>
                                <asp:Repeater ID="OverviewTabs" runat="server" DataSource="<%#GetOverviewContent(Item) %>">
                                    <ItemTemplate>
                                        <li><a href="#section1-item<%#Container.ItemIndex+1%>" <%# Container.ItemIndex==0? " class=\"active\"" : ""  %>><%#GetTitle(Container.DataItem)%> </a></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <%-- <li><a href="#section1-item-desc"><%#GetDescriptionTab() %></a></li>--%>
                                <% if (HasTechnologies)
                                   {%>
                                <li><a href="#section1-item-tech"><%#GetTechnologyTab() %></a></li>
                                <% } %>
                            </ul>
                            <div class="select-mobile">
                                <select>
                                    <asp:Repeater ID="OverviewTabsForMobile" runat="server"  DataSource="<%#GetOverviewContent(Item) %>">
                                        <ItemTemplate>
                                            <option value="<%#Container.ItemIndex%>"><%#GetTitle(Container.DataItem)%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <%--<option value="<%#GetOverviewContent(Item).Count() %>"><%#GetDescriptionTab() %></option>--%>
                                    <%if (HasTechnologies)
                                      { %>
                                    <option value="<%#GetOverviewContent(Item).Count()+1 %>"><%#GetTechnologyTab() %></option>
                                    <% } %>
                                </select>
                            </div>
                            <asp:Repeater ID="OverviewTabsContent" runat="server" DataSource="<%#GetOverviewContent(Item) %>">
                                <ItemTemplate>
                                    <div id="section1-item<%#Container.ItemIndex+1%>" <%# Container.ItemIndex==0? " class=\"section active\"" : "class=\"section\""  %>>
                                        <%#GetDescription(Container.DataItem)%>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <%--<div id="section1-item-desc" class="section">
                                <%#GetDescriptionTabContent(Item) %>
                            </div>--%>
                            <%if (HasTechnologies)
                              { %>
                            <div id="section1-item-tech" class="section">
                                <ul>
                                    <% foreach (var tech in Model.SelectProduct.RelatedTechnologies.ToList<Anritsu.WebApp.GlobalWeb.Models.Pages.ITechnology>())
                                       {%>
                                    <li><a href="<%= tech.Url%>"><%= tech.TechnologyName %></a></li>
                                    <% } %>
                                </ul>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Repeater ID="ProductTabsDescription" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
            <ItemTemplate>
                <div id="<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?GetCssClass:""%>">
                    <div class="section-title"><span><%#Editable(Item,x=>x.Title) %></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
                    <div class="section-content">
                        <div class="<%# Sitecore.Context.Database.GetItem(Item.Id.ToString()).HasChildren? "width70 floatLft":"width100" %>">
                            <%#Editable(Item,x=>x.Description) %>
                        </div>
                        <div class="<%# HideRelatedLinks(Item) %>">
                            <div class="link-title"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "RelatedLinks") %></div>
                            <asp:Repeater ID="RelatedLinks" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.ILink" DataSource="<%#GetRelatedLinks(Item) %>">
                                <ItemTemplate>
                                    <div class="links">
                                        <%# RenderLink(Item, x=>x.Link, isEditable: true, contents:Editable(Item,x=>x.Title) +"&nbsp;"+ "<i class='icon icon-arrow-blue'></i>")%>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>

            </ItemTemplate>
        </asp:Repeater>

        <div id="Library" class="section <%=ShowHideLibrary %>">
            <div class="section-title"><span><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "library") %></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
            <div class="section-content detailSection">
                <div class="tab-interface inner-tab product-downloads products-table">
                    <ul>
                        <asp:Repeater ID="productDownloads" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                            <ItemTemplate>
                                <li>
                                    <a class="<%# Container.ItemIndex==0? "active" : ""  %>" href="#<%# Editable(Item,x=>x.Key) %>"><%# Editable(Item,x=>x.Value) %> </a>
                                    <table class="section detail-table active" id="<%# Editable(Item,x=>x.Key) %>">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div><%# Editable(Item,x=>x.Value) %></div>
                                                </th>
                                                <th>
                                                    <div><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "releasedate") %></div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <asp:Repeater ID="productDownloadLibrary" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.DownloadSearchItem" DataSource="<%# GetProductLibraryContent(Item.Key) %>">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="border">
                                                        <b>
                                                            <asp:HyperLink runat="server" ID="LibraryFileDownloadLink" Target="_blank" NavigateUrl="<%# Item.GetItemUrl()%>" >
                                                                <%# Editable(Item,x=>x.Title) %>
                                                            </asp:HyperLink>
                                                            <asp:HyperLink runat="server" ID="LibraryFileNewVersionDownloadLink" Target="_blank" CssClass="new-version" Visible="<%# Item.IsNewVersionAvailable %>" NavigateUrl="<%# Item.GetReplacementItemUrl()%>" >
                                                                <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "newerversionavailable") %>
                                                            </asp:HyperLink>
                                                        </b>
                                                    </td>
                                                    <td><b><%# Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToFormattedDate()) %></b></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </table>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="select-mobile">
                        <select id="product-download-category">
                            <asp:Repeater ID="ProductsDownloadsMobile" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                                <ItemTemplate>
                                    <option value="<%#Editable(Item,x=>x.Key)%>"><%#Editable(Item,x=>x.Value) %></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
