﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfessionalServices.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ProfessionalServices" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Pages" %>


<div class="full-container tech-home">
            <%--<div style="background-image:url('<%=Model.BannerImage.Src%>')" class="hero-banner-2"> --%>   
    <div style="background-image:url('<%=Editable(SitecoreContext.GetCurrentItem<IStaticPageWithBannerAndThumbnail>(),x=> x.BannerImage.Src)%>')" class="hero-banner-2">             
              <div class="hero-content <%=Model.TextColor!=null?Editable(x=>x.TextColor.Key):"" %>">
                  <% if (!string.IsNullOrEmpty(Editable(SitecoreContext.GetCurrentItem<IStaticPageWithBannerAndThumbnail>(),x=> x.BannerThumbnail.Src) ))
                     { %>
                <p><%=RenderImage(SitecoreContext.GetCurrentItem<IStaticPageWithBannerAndThumbnail>(), x=>x.BannerThumbnail, isEditable: true)%></p>
                  <% } %>
                <h1><%=Editable(SitecoreContext.GetCurrentItem<IStaticPageWithBannerAndThumbnail>(),x=> x.BannerTitle)%></h1>
                <p>
                    <%=Editable(SitecoreContext.GetCurrentItem<IStaticPageWithBannerAndThumbnail>(),x=> x.BannerDescription)%> 
                </p>
              </div>                    
            </div>
          </div>

 
<div class="container">
    <div class="landing-tech">
        <asp:Repeater ID="contentBlock" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IContentBlockTitleWithLink">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <h2><%#Editable(Item,x=> x.Title)%></h2>
                        <%#Editable(Item,x=> x.Description)%>
                        <p><%#RenderLink(Item, x=>x.Link, isEditable: true, contents: Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ReadMore")+"<i class='" + "icon icon-arrow-blue" + "'></i>")%></p>        
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>