﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReprintCalibrationCertificate.ascx.cs"
    Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ReprintCalibrationCertificate" %>


<script src='<%=CdnPath %>/appfiles/js/calibration_certificate.js'></script>
<script src='<%=CdnPath %>/appfiles/js/GlobalScriptMethods.js'></script>


<div class="container">
    <div class="your-info">
        <div class="form-container">
            <!-- Main Body to search the Certificates -->
            <div id="divMainBody" runat="server">
                <div class="contact-heading">
                    <h1>
                        <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "calibrationcertsearchtitle") %>
                       <%-- Calibration Certificate Search--%>
                    </h1>
                    <p class="contact-intro">
                        <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "calibrationcertsearchtext") %>
                        <%--Search by Model and Serial Number to access your calibration certification--%>
                    </p>
                    <%--<p class="contact-intro"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "calibrationcertsearchdesc") %></p>--%>
                    <asp:HiddenField ID="hfCmp" runat="server" />
                </div>
                <h3 class="form-title">
                    <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "calibrationcertsearchdesc") %>
                    <%--Enter your model number (eg., 30396) and serial number (e.g., 062111) Enter the exact Serial number as it appears on the label--%> 
                </h3>
                <div class="bd">
                    <div class="group input-text required">
                        <label for="txtMdlNum">
                            <%=Sitecore.Globalization.Translate.TextByDomain("Products", "ModelNumber") %> :
                        </label>
                        <p class="error-message">
                            <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgCannotBeEmpty") %>
                        </p>
                        <p class="custom-error-message">
                            <asp:RegularExpressionValidator ID="revInvalidModel" Visible="False"
                                ValidationExpression="^\S{3,}$" ControlToValidate="txtMdlNum" SetFocusOnError="true"
                                Display="None" runat="server">
                            </asp:RegularExpressionValidator>
                            <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgModelNumber") %>
                        </p>
                        <asp:TextBox ID="txtMdlNum" runat="server" MaxLength="50" CssClass="min-three-char"></asp:TextBox>
                    </div>
                    <div class="group input-text required">
                        <label for="txtSlNum">
                            <%=Sitecore.Globalization.Translate.TextByDomain("MyAnritsu", "SerialNumber") %> :
                        </label>
                        <p class="error-message">
                            <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgCannotBeEmpty") %>
                        </p>
                        <asp:TextBox ID="txtSlNum" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="group">
                        <asp:Button ID="bttSearch" runat="server"
                            OnClick="bttSearch_Click" class="button single-form-submit"></asp:Button>
                    </div>
                </div>
                <div id="divSearchNoResults" runat="server" visible="false" style="padding-top: 20px;">
                    <b>
                        <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "CalibrationCertificateSearchNotFound") %>
                    </b>
                    <a href="javascript:void(0)" onclick="ShowCertificateRequest('<%= divMainBody.ClientID %>','divRequestCert')" class="anchorColor">
                        <%=Sitecore.Globalization.Translate.TextByDomain("CommonFormStrings", "ClickThisLink") %>
                    </a>
                </div>
            </div>

        </div>
        <div class="form-container">
            <!-- Certificate Request & User Details -->
            <div id="divRequestCert" style="display: none;">
                <h2>
                    <asp:Label ID="lblCalCertReq" runat="server" Text="Calibration Certificate Request "></asp:Label>
                </h2>

                <div class="group input-text required">
                    <label for="txtUserName">
                        User Name
                    </label>
                    <p class="error-message">
                        <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgCannotBeEmpty") %>
                    </p>
                    <asp:TextBox ID="txtUserName" runat="server" MaxLength="50"></asp:TextBox>
                </div>
                <div class="group input-text required">
                    <label for="txtUserLogin">
                        User Login
                    </label>
                    <p class="error-message">
                        <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgCannotBeEmpty") %>
                    </p>
                    <p class="custom-error-message">
                        <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgInvalidEmail") %>
                    </p>
                    <asp:TextBox ID="txtUserLogin" runat="server" MaxLength="50" CssClass="email"></asp:TextBox>
                </div>
                <div class="group input-text required">
                    <label for="txtUserPhNum">
                        User Phone Number 
                    </label>
                    <p class="error-message">
                        <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgCannotBeEmpty") %>
                    </p>
                    <asp:TextBox ID="txtUserPhNum" runat="server" MaxLength="50"></asp:TextBox>
                </div>
                <div class="group input-text required">
                    <label for="txtMdlNum1">
                        <%=Sitecore.Globalization.Translate.TextByDomain("Products", "ModelNumber") %>
                    </label>
                    <p class="error-message">
                        <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgCannotBeEmpty") %>
                    </p>
                    <asp:TextBox ID="txtMdlNum1" runat="server" MaxLength="50"></asp:TextBox>
                </div>
                <div class="group input-text required">
                    <label for="txtSlNum1">
                        <%=Sitecore.Globalization.Translate.TextByDomain("MyAnritsu", "SerialNumber") %>
                    </label>
                    <p class="error-message">
                        <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgCannotBeEmpty") %>
                    </p>
                    <asp:TextBox ID="txtSlNum1" runat="server" MaxLength="50"></asp:TextBox>
                </div>
                <div class="group input-textarea required">
                    <label for="txtComments1">
                        Comments :(Maximum 2000 characters) 
                    </label>
                    <p class="custom-error-message">
                        <%=Sitecore.Globalization.Translate.TextByDomain("ErrorMessage", "ErrMsgCharactersLimit") %>
                    </p>
                    <asp:TextBox ID="txtComments1" runat="server" MaxLength="2000"
                        TextMode="MultiLine" CssClass="max-two-thousand-char"></asp:TextBox>
                </div>
                <div class="group">
                    <asp:Button ID="btnCertReqSubmit" runat="server" OnClick="btnCertReqSubmit_Click" class="button single-form-submit" />
                </div>
            </div>
        </div>

        <!-- Search Results -->
        <div id="divSearchResults" runat="server" visible="false">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <h2 class="certificate-heading">
                            <asp:Label ID="lblRes" runat="server" Text="Calibration Certificate "></asp:Label></h2>
                        <br />
                        <a href="javascript:void(0)" class="anchorColor"
                            onclick="DefaultCalCertsDivs('<%= divMainBody.ClientID %>','<%= divSearchNoResults.ClientID %>','<%= divSearchResults.ClientID %>')">
                            <asp:Label ID="lblAnotherSearch" runat="server" Text="Perform Another Search "></asp:Label></a>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="detail-table">
                            <asp:GridView ID="dgvCertificates" PageSize="10" AllowPaging="True" runat="server" border="0"
                                AutoGenerateColumns="False" OnRowCreated="dgvCertificates_RowCreated">
                                <PagerSettings Position="TopAndBottom" />
                                <Columns>
                                    <asp:BoundField DataField="ModelNumber" />
                                    <asp:BoundField DataField="SerialNumber" />
                                    <asp:TemplateField HeaderText="Calibration Cert " ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbttDownloadCert" CommandName="DownloadCommand" CommandArgument='<%# Eval("ModelNumber") + "," + Eval("SerialNumber") %>'
                                                runat="server" Text='<%# Bind("Cal_Pdf", "{0}.pdf") %>' OnClick="lbttDownloadCert_Click" CssClass="anchorColor">
                                            </asp:LinkButton>
                                            <img alt="" src="//dl.cdn-anritsu.com/appfiles/img/icons/pdf_small_new.png" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Calibration Cert Date ">
                                          <ItemTemplate>
                                                <asp:Label ID="lblCAlCertDate" runat="server" Text='<%# GetFormattedDate(Eval("Cal_Date")) %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>

                                    <%--<asp:BoundField HeaderText="Calibration Cert Date " DataField="Cal_Date" HtmlEncode="true" DataFormatString="{0:d}" />--%>
                                    <asp:TemplateField HeaderText="Calibration Due Date ">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtSubDueDate" runat="server" Text='<%# GetFormattedDate(Eval("Cal_Due_Date")) %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                           <asp:Label ID="lblSugDueDatelnk" runat="server" Text='<%# GetFormattedDate(Eval("Cal_Due_Date")) %>'></asp:Label>
                                            -
                                    <asp:HyperLink ID="hlServiceRequest" NavigateUrl="<%# GetURL() %>" runat="server" CssClass="anchorColor">
                                        <asp:Literal ID="Literal1" runat="server" Text="Request Service Calibration"></asp:Literal>
                                    </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White"></PagerStyle>--%>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="right">
                        <br />
                        <br />
                        <div id="divReader">
                            <asp:Label ID="lblPdfDownloadText" runat="server" Text="Adobe Reader is not installed? click here to download "></asp:Label>
                            <asp:HyperLink ID="hlDownloadAdobeReader" runat="server" NavigateUrl="http://www.adobe.com/products/acrobat/readstep2.html"
                                ImageUrl="//dl.cdn-anritsu.com/appfiles/img/icons/pdficon.gif" Target="_blank"></asp:HyperLink>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>

        <div id="divThankyou" runat="server" style="display: none;">
            <div style="text-align: center;"><b>Thank you for your request. </b></div>
            <asp:Label ID="lblThankYou" runat="server"></asp:Label>
        </div>
    </div>
</div>



