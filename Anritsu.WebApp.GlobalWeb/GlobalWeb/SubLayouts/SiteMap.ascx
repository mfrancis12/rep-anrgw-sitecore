﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteMap.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.SiteMap" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="container">
<div class="leftCol">
<asp:Repeater ID="LeftRepeater" runat="server" OnItemDataBound="LeftRepeater_ItemDataBound">
    <ItemTemplate>
        <asp:TreeView ID="LeftTree" runat="server"  CssClass="sitemapCSS">
            </asp:TreeView>
    </ItemTemplate>
</asp:Repeater>
    </div>
<div class="rightCol">
<asp:Repeater ID="RightRepeater" runat="server" OnItemDataBound="RightRepeater_ItemDataBound">
    <ItemTemplate>
        <asp:TreeView ID="RightTree" runat="server"  CssClass="sitemapCSS">
            </asp:TreeView>
    </ItemTemplate>
</asp:Repeater>
    </div>
</div>
