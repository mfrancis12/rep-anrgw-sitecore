﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaticBanner.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.StaticBanner" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<% if (!string.IsNullOrEmpty(Model.BannerImage.Src))
                     { %>
<div class="full-container multiProCls">
    <div class="hero-banner-3" style="background-image: url('<%=Model.BannerImage.Src%>')">
        <div class="hero-content <%=Editable(x=>x.GetTextColor()) %>">
            <h1><%=Editable(x=>x.BannerTitle) %></h1>
            <p><%=Editable(x=>x.BannerDescription) %></p> 
        </div>
    </div>
</div>
<% } %>
<%else 
  {%>
<div class="full-container container">
    <div class="products-heading">
        <div class="products-title">
          <h1 style="color: #289b7d;"><%=Editable(x=>x.BannerTitle) %></h1></div>
            <div class="products-description"><p><%=Editable(x=>x.BannerDescription) %></p> </div>
        </div>
    </div>
 <% }
   %>