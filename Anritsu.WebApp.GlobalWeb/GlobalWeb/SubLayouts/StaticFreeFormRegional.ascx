﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaticFreeFormRegional.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.StaticFreeformRegional" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="container">
    <div class="content-form">
        <div class="content-detail">
            <%--<%= Model.Content.Content %>--%>
            <%= Model.Content!=null ? Editable(x=>x.Content.Content):"" %>
        </div>
    
    <div class="content-aside">
            <p class="desktop"> <%if(!string.IsNullOrEmpty(ImageUrl )){ %><img class="img100" src="<%= ImageUrl %>"><%} %></p>
            <p class="mobile"> <%if (!string.IsNullOrEmpty(ImageUrl))
                                 { %><img class="img100" src="<%= ImageUrl %>"><%} %></p>
                <asp:Repeater ID="OptionalLinks" runat="server" OnItemDataBound="OptionalLinks_ItemDataBound" OnPreRender="OptionalLinks_PreRender">
                    <HeaderTemplate> <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3></HeaderTemplate>
                    <ItemTemplate>
                        <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                    </ItemTemplate>
            </asp:Repeater>
        </div>
        </div>
</div>