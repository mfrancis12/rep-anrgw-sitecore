﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaticPageWithContent.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.StaticPageWithContent" %>

<div class="container">
    <div class="content-form">
        <div class="content-detail">
            <%=GetContent()%>
        </div>
        <div class="content-aside">
            <p class="desktop"> <%if(!string.IsNullOrEmpty(ImageSrc )){ %><img class="img100" src="<%= ImageSrc %>"><%} %></p>
            <p class="mobile"> <%if(!string.IsNullOrEmpty(ImageSrc )){ %><img class="img100" src="<%= ImageSrc %>"><%} %></p>
                <asp:Repeater ID="OptionalLinks" runat="server" OnItemDataBound="OptionalLinks_ItemDataBound" OnPreRender="OptionalLinks_PreRender">
                    <HeaderTemplate> <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3></HeaderTemplate>
                    <ItemTemplate>
                        <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                    </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>










