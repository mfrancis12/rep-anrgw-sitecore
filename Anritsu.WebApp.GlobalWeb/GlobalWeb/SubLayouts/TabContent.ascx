﻿<%@ control language="C#" autoeventwireup="true" codebehind="TabContent.ascx.cs" inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.TabContent" %>
<%@ import namespace="Anritsu.WebApp.GlobalWeb.Models.Pages" %>
<div class="container marginTop">
    <div class="tab-interface hero-tab marginTop20">
        <ul>   
            <li><a href="#section1" class="<%=Sitecore.Context.Item.ID.ToString().Equals(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.Home)?"active":""%>"> <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","NewsRelease") %></span></a></li>
            <li><a href="#section2" class="<%=Sitecore.Context.Item.TemplateID.ToString().Equals(Anritsu.WebApp.GlobalWeb.Constants.TemplateIds.ProductCategory)?"active":""%>"> <span><%=GetTabTitle %></span></a></li>
            <li><a href="#section3"> <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","Events") %></span></a></li>                   
        </ul>
         <sc:placeholder runat="server" key="TabContent" />
    </div>
</div>
