﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabsRenderer.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.TabsRenderer" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="container">
    <div class="tab-interface-2 product-detail-tab">
        <ul>
            <asp:Repeater ID="Tabs" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
                <ItemTemplate>
                    <li><a href="#<%#Item.Id.ToString()%>" class="<%#Container.ItemIndex==0?"active":""%>"><span><%#Editable(Item,x=>x.Title)%></span></a></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <asp:Repeater ID="TabsDescription" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
            <ItemTemplate>
                <div id="<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?"active":""%>">
                    <div class="section-title"><span><%#Editable(Item,x=>x.Title) %></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
                    <div class="section-content detail-table">
                        <%#Editable(Item,x=>x.Description) %>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
