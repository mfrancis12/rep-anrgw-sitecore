﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TechnologyDetail.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.TechnologyDetail" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>


<div class="container accordianclr">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.Title)%></h1>
            <p>
                <%= RenderImage(x => x.BannerImage,new {@class="img100", Width="730",Height="270"}, isEditable: true)%>
            </p>
            <%=Editable(x=>x.LongDescription) %>

            <% if (itemCount > 0)
               { %>
            <div class="accordion">
                <asp:Repeater ID="tabsDescription" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
                    <ItemTemplate>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <span><%#Editable(Item,x=>x.Title) %></span>
                                <div class="accordion-icon">
                                    <div class="icon icon-plus-grey"></div>
                                </div>
                            </div>
                            <div class="accordion-content">
                                <%#Editable(Item,x=>x.Description) %>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <% } %>


            <% if (Model.RelatedProducts.Any())
               { %>
            <h2 class="margin-top50"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Products") %></h2>
            <div class="products-intro">
                <asp:Repeater ID="productList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IProductRegional">
                    <ItemTemplate>
                        <div class="products-item productItemHeight" style="background-image: url('<%# Item.GetProductStatus() %>'); background-repeat: no-repeat;">
                            <a href="<%#Item.Url%>">
                                <%#RenderImage(Item,x=>x.SelectProduct.Thumbnail, isEditable: true)%>

                                <h3><%#Editable(Item, x=>x.SelectProduct.ModelNumber) %></h3>
                            </a>
                            <p><%#Editable(Item, x=>x.SelectProduct.ProductDescriptor) %></p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <% } %>
        </div>

        <div class="content-aside">
            <p class="mobile">
                <%=RenderImage(x=>x.Image, isEditable: true)%>
            </p>
            <asp:Repeater ID="optionalLinks" runat="server" OnItemDataBound="OptionalLinks_ItemDataBound" OnPreRender="OptionalLinks_PreRender">
                <HeaderTemplate>
                    <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
                </HeaderTemplate>
                <ItemTemplate>
                    <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
