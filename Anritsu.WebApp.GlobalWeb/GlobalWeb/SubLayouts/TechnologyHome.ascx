﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TechnologyHome.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.TechnologyHome" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="full-container tech-home">
    <div style="background-image: url('<%=Model.BannerImage!=null?Model.BannerImage.Src:""%>')" class="hero-banner-2">
        <div class="hero-content <%=Model.TextColor!=null?Editable(x=>x.TextColor.Key):"" %>">
            <p><%= RenderImage(x=>x.BannerThumbnail) %></p>
            <h1><%=Editable(x=> x.BannerTitle)%></h1>
            <p><%=Editable(x=> x.BannerDescription)%></p>
        </div>
    </div>
</div>
<div class="container">
    <div class="landing-tech">
        <asp:Repeater ID="contentBlock" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.ITechnology">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <h2><%#Editable(Item,x=> x.TechnologyName)%></h2>
                        <%#Editable(Item,x=> x.ShortDescription)%>
                        <p><a href="<%#Item.Url %>"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ReadMore") %><i class="icon icon-arrow-blue"></i></a></p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
