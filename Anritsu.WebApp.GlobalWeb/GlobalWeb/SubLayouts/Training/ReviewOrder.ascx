﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReviewOrder.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.ReviewOrder" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<div class="container accordianclr">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.PageTitle) %></h1>
            <asp:Label ID="LabelMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            <asp:Panel runat="server" ID="pnlConfirmationText">
                <p><%=Editable(x=>x.Content) %></p>
                <br />
                <asp:Literal ID="ltrText" runat="server"></asp:Literal>
                <br />
                <%=Translate.TextByDomain("Training","MoreInfo")%>&nbsp;
                <asp:Literal ID="ltrUserEmailId" runat="server"></asp:Literal>
                <br />
                <br />
                <%=Translate.TextByDomain("Training","RegistrationDetails")%>.
                <%=Translate.TextByDomain("Training","Please")%>
                <asp:HyperLink ID="lnkPrint" runat="server" Target="_blank"><%=Translate.TextByDomain("Training","Print")%></asp:HyperLink>
                <%=Translate.TextByDomain("Training","ForRecords")%>
            </asp:Panel>
            <div class="location">
                <div class="location-title"><%=Translate.TextByDomain("CommonFormStrings","Contact Information")%></div>
                <div class="location-description">
                    <div class="location-description-block">
                        <table width="100%">
                            <tr>
                                <td style="font-weight: bold" width="100px">
                                    <%=Translate.TextByDomain("CommonFormStrings","FirstName") %>:</td>
                                <td width="250px">
                                    <asp:Label ID="txtFirstName" runat="server" />
                                </td>
                                <td style="font-weight: bold" width="100px">
                                    <%=Translate.TextByDomain("CommonFormStrings","Lastname")%>:</td>
                                <td>
                                    <asp:Label ID="txtLastName" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","Address")%>:</td>
                                <td>
                                    <asp:Label ID="txtAddress1" runat="server" />
                                </td>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","Address2")%>:</td>
                                <td>
                                    <asp:Label ID="txtAddress2" runat="server" /></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","Company")%>:</td>
                                <td>
                                    <asp:Label ID="txtCompany" runat="server" /></td>
                                <td width="85px" style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","JobTitle")%>:</td>
                                <td width="300px">
                                    <asp:Label ID="txtTitle" runat="server" /></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","Email")%>:</td>
                                <td>
                                    <asp:Label ID="txtEmail" runat="server" /></td>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","City")%>:</td>
                                <td>
                                    <asp:Label ID="txtCity" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","Phone")%>:</td>
                                <td>
                                    <asp:Label ID="txtPhone" runat="server" /></td>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","Country")%>:</td>
                                <td>
                                    <asp:Label ID="ddlCountry" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","FaxNumber")%>:</td>
                                <td>
                                    <asp:Label ID="txtFax" runat="server" /></td>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","State")%>:</td>
                                <td>
                                    <asp:Label ID="cntrlState" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <%=Translate.TextByDomain("CommonFormStrings","PostalCode")%>:</td>
                                <td>
                                    <asp:Label ID="txtPostalCode" runat="server" />
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlBillingInfo" runat="server">
                <div class="location">
                    <div class="location-title"><%=Translate.TextByDomain("CommonFormStrings","BillingInformation")%> </div>
                    <div class="location-description">
                        <div class="location-description-block">

                            <table width="100%">
                                <tr>
                                    <td width="85px" style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","JobTitle")%>:</td>
                                    <td width="300px">
                                        <asp:Label ID="lblBillingTitle" runat="server" /></td>
                                    <td width="85px" style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","Address1")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingAddress1" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","Name")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingName" runat="server" /></td>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","Address2")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingAddress2" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","Company")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingCompany" runat="server" /></td>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","Country")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingCountry" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","Email")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingEmail" runat="server" /></td>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","City")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingCity" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","Phone")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingPhone" runat="server" /></td>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","State")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingState" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","FaxNumber")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingFax" runat="server" /></td>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("CommonFormStrings","PostalCode")%>:</td>
                                    <td>
                                        <asp:Label ID="lblBillingPostalCode" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <asp:Label ID="lblPO_VoucherNumberCaption" runat="server" /></td>
                                    <td>
                                        <asp:Label ID="lblPO_VoucherNumber" runat="server" /></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("Training", "Amount") %>:</td>
                                    <td>
                                        <asp:Label ID="lblAmountDue" runat="server" /></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlConfirmation" Visible="false">
                        <div class="location">
                            <div class="location-title">
                                <%=Translate.TextByDomain("Training", "ClassRegInformation") %>
                            </div>
                <asp:ListView ID="lstvRegistrationDetails" runat="server">
                    <LayoutTemplate>
                            <div class="location-description">
                                <div class="location-description-block">
                                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                                </div>
                            </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div style="font-weight: bold">
                            <%=Translate.TextByDomain("Training", "Class") %>
                            <%#Eval("Rank") %>:
                        </div>
                        <div>
                            <table style="width: 100%">
                                <tr style="vertical-align: top;">
                                    <td width="95px" style="font-weight: bold">
                                        <%=Translate.TextByDomain("Training","PartNumber")%>:
                                    </td>
                                    <td width="250px">
                                        <%#Eval("PartNumber") %>
                                    </td>
                                    <td width="85px" style="font-weight: bold">
                                        <%=Translate.TextByDomain("Training","ClassName")%>:
                                    </td>
                                    <td>
                                        <%# Eval("ClassName") %>
                                    </td>
                                </tr>
                                <tr style="vertical-align: top;">
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("Glossary", "Date") %>:
                                    </td>
                                    <td>
                                        <%#GetScheduleDate() %>
                                    </td>
                                    <td style="font-weight: bold">
                                        <%=Translate.TextByDomain("Glossary", "Location") %>:
                                    </td>
                                    <td style="text-align: justify;">
                                        <%#Eval("Location") %>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="font-weight: bold">
                            <%=Translate.TextByDomain("Training", "Attendees") %>
                        </div>
                        <asp:ListView ID="lstvAttendees" runat="server" DataSource='<%#GetAttendees() %>'>
                            <LayoutTemplate>
                                <table style="width: 100%">
                                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td width="15px">&nbsp;&nbsp;&nbsp;<%#(int) DataBinder.Eval(Container, "DisplayIndex")+1%>.
                                    </td>
                                    <td width="110px">
                                        <%#Eval("FirstName") %>
                                    </td>
                                    <td width="110px">
                                        <%#Eval("LastName") %>
                                    </td>
                                    <td width="90px">
                                        <%#Eval("PhoneNumber") %>
                                    </td>
                                    <td>
                                        <%#Eval("EmailID") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:ListView>
                        </div>
            </asp:Panel>

            <asp:Button ID="btnTrainingCart" runat="server" Text="Return to Cart" OnClick="btnTrainingCart_Click" Visible="false" />
        </div>
    </div>
    <div class="content-aside">
    </div>
</div>
