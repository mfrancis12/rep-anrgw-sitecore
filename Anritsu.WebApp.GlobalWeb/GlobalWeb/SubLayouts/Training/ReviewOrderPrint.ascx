﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReviewOrderPrint.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.ReviewOrderPrint" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<h2><%=Translate.TextByDomain("Training","ThankYouOrder")%></h2>
<p><%=Translate.TextByDomain("Training","Confirmation")%></p>
<asp:Panel runat="server" ID="pnlConfirmation">
    <asp:ListView ID="lstvRegistrationDetails" runat="server">
        <LayoutTemplate>
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <div style="font-weight: bold">
                <%=Translate.TextByDomain("Training", "Class") %>
                <%#Eval("Rank") %>:
            </div>
            <div>
                <table style="width: 100%">
                    <tr style="vertical-align: top;">
                        <td width="95px" style="font-weight: bold">
                            <%=Translate.TextByDomain("Training","PartNumber")%>:
                        </td>
                        <td width="250px">
                            <%#Eval("PartNumber") %>
                        </td>
                        <td width="85px" style="font-weight: bold">
                            <%=Translate.TextByDomain("Training","ClassName")%>:
                        </td>
                        <td>
                            <%# Eval("ClassName") %>
                        </td>
                    </tr>
                    <tr style="vertical-align: top;">
                        <td style="font-weight: bold">
                            <%=Translate.TextByDomain("Glossary", "Date") %>:
                        </td>
                        <td>
                            <%#GetScheduleDate() %>
                        </td>
                        <td style="font-weight: bold">
                            <%=Translate.TextByDomain("Glossary", "Location") %>:
                        </td>
                        <td style="text-align: justify;">
                            <%#Eval("Location") %>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="font-weight: bold">
                <%=Translate.TextByDomain("Training", "Attendees") %>
            </div>
            <asp:ListView ID="lstvAttendees" runat="server" DataSource='<%#GetAttendees() %>'>
                <LayoutTemplate>
                    <table style="width: 100%">
                        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td width="15px">&nbsp;&nbsp;&nbsp;<%#(int) DataBinder.Eval(Container, "DisplayIndex")+1%>.
                        </td>
                        <td width="110px">
                            <%#Eval("FirstName") %>
                        </td>
                        <td width="110px">
                            <%#Eval("LastName") %>
                        </td>
                        <td width="90px">
                            <%#Eval("PhoneNumber") %>
                        </td>
                        <td>
                            <%#Eval("EmailID") %>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>


