﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingCourseDetail.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.TrainingCourseDetail" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="container accordianclr">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.CourseName) %></h1>
            <p><%=RenderImage(x => x.CourseImage, new { @class = "img100", Width = 730, Height = 285})%></p>
            <%=Editable(x=>x.CourseDescription) %>
            <sc:placeholder runat="server" Key="TrainingCourse" />
        </div>
        <div class="content-aside">
            <sc:placeholder runat="server" key="RightRail" />
        </div>
    </div>
</div>
