﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrainingAdmin.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.TrainingAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ul>
            <li>
    <asp:Label ID="LabelClassName" runat="server" Text="ClassName :" ></asp:Label>
        <asp:TextBox ID="ClassName" runat="server" ></asp:TextBox>
</li>
              <li>
        <asp:Label ID="LabelClassDescription" runat="server" Text="ClassDescription :" ></asp:Label>
        <asp:TextBox ID="ClassDescription" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelPageURL" runat="server" Text="PageURL :" ></asp:Label>
        <asp:TextBox ID="PageURL" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelCreatedOnUTC" runat="server" Text="CreatedOnUTC :" ></asp:Label>
        <asp:TextBox ID="CreatedOnUTC" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelModifiedOnUTC" runat="server" Text="ModifiedOnUTC :" ></asp:Label>
        <asp:TextBox ID="ModifiedOnUTC" runat="server" ></asp:TextBox>
                    </li>
                    <li>
        <asp:Label ID="LabelIsActive" runat="server" Text="IsActive :" ></asp:Label>
       <asp:CheckBox ID="IsActive" runat="server" />
            </li>
              <li>
        <asp:Label ID="LabelContentTitle" runat="server" Text="ContentTitle :" ></asp:Label>
        <asp:TextBox ID="ContentTitle" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelEventId" runat="server" Text="EventId :" ></asp:Label>
        <asp:TextBox ID="EventId" runat="server" ></asp:TextBox>
                    </li>
                          <li>
        <asp:Label ID="LabelCultureGroupId" runat="server" Text="CultureGroupId :" ></asp:Label>
        <asp:TextBox ID="CultureGroupId" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelClassId" runat="server" Text="ClassId :" ></asp:Label>
        <asp:TextBox ID="ClassId" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelPartNumber" runat="server" Text="PartNumber :" ></asp:Label>
        <asp:TextBox ID="PartNumber" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelDateFrom" runat="server" Text="DateFrom :" ></asp:Label>
        <asp:TextBox ID="DateFrom" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelDateTo" runat="server" Text="DateTo :" ></asp:Label>
        <asp:TextBox ID="DateTo" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelLocation" runat="server" Text="Location :" ></asp:Label>
        <asp:TextBox ID="Location" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelSeatsAllocated" runat="server" Text="SeatsAllocated :" ></asp:Label>
        <asp:TextBox ID="SeatsAllocated" runat="server" ></asp:TextBox>
            </li>
              <li>
        <asp:Label ID="LabelExpiryDate" runat="server" Text="ExpiryDate :" ></asp:Label>
        <asp:TextBox ID="ExpiryDate" runat="server" ></asp:TextBox>
            </li>
              <li>
       
        <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click"></asp:Button>
                <asp:Button ID="Cancel" runat="server" Text="Cancel" OnClick="Cancel_Click"></asp:Button>
            </li>
              <li>
        <asp:Label ID="LabelSuccessMessage" runat="server" EnableViewState="false" ></asp:Label><br />
        <asp:Label ID="LabelFailureMessage" runat="server" EnableViewState="false" ></asp:Label>
            </li>
            </ul>
    </div>
    </form>
</body>
</html>
