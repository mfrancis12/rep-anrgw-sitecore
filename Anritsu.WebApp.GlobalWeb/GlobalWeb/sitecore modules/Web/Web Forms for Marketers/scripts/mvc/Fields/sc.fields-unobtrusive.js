﻿

if (typeof ($scw) === "undefined") {
    window.$scw = jQuery.noConflict(true);
}


//multi regular expression validation

$scw.validator.addMethod("multiregex", function (value, element, params) {
    var match;
    if (this.optional(element)) {
        return true;
    }

    match = new RegExp(params.mpattern).exec(value);
    return (match && (match.index === 0) && (match[0].length === value.length)); 
});

$scw.validator.unobtrusive.adapters.add("multiregex", ["pattern"], function (options) {

    var params = {
        mpattern: options.params.pattern
    }
    options.rules["multiregex"] = params;
    options.messages["multiregex"] = options.message;
});