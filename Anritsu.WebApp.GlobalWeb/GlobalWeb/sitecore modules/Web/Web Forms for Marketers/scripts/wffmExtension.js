function EnhancedRequiredValidator(obj, args) {
    var control = document.getElementById(obj.controltovalidate);
    if (control) {
        var relatedId = control.getAttribute("RelatedValidationItemClientId");

        if (ValidatorGetValue(relatedId).toLowerCase() != "") {
            var validValues = control.getAttribute("ValidValues");
            var initialValue = control.getAttribute("InitialValue");
            var arrayOfValidValues = validValues.split('|');

            initialValue = initialValue == null ? "" : initialValue;
            for (var i = 0; i < arrayOfValidValues.length; i++) {

                arrayOfValidValues[i] = ValidatorTrim(arrayOfValidValues[i]);

                if (arrayOfValidValues[i].toLowerCase() == ValidatorGetValue(relatedId).toLowerCase()) {

                    args.IsValid = ValidatorTrim(ValidatorGetValue(obj.controltovalidate)) != ValidatorTrim(initialValue);
                    break;
                }
            }
        }
    }
}