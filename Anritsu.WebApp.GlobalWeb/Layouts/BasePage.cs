﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Anritsu.WebApp.GlobalWeb.Layouts
{
    public class BasePage : Page
    {
        protected bool TerminatePage { get; set; }

        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            if (TerminatePage == false)
                base.RaisePostBackEvent(sourceControl, eventArgument);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (TerminatePage == false)
                base.Render(writer);
        }

        //return group header based on url
        public Item GetGroupHeaderItem()
        {
            if (HttpContext.Current.Request.Url.Segments.Length > 2)
            {
                string groupHeader = Convert.ToString(HttpContext.Current.Request.Url.Segments[2])
                    .Replace("/", "").Replace("#", "")
                    .Trim().ToLowerInvariant();
                Item groupHeaderItem = Sitecore.Context.Database.GetItem(ItemIds.GroupHeaders).Children
                    .Where(item => item.Name.ToLowerInvariant().Equals(groupHeader)).FirstOrDefault();

                return groupHeaderItem != null ? 
                    groupHeaderItem : Sitecore.Context.Database.GetItem(ItemIds.CorporateHeader);
            }
            else
            {
                return Sitecore.Context.Database.GetItem(ItemIds.CorporateHeader);
            }
        }

        public bool IsCAGroupHeader(Item grpHdrItm)
        {
            return grpHdrItm != null && grpHdrItm.ID.ToString() == ItemIds.ComponentsAndAccessoriesHeader;
        }

        public string GetTMGroupHeaderName()
        {
            return Sitecore.Context.Database.GetItem(ItemIds.TestAndMeasurementHeader).Name;
        }
    }
}