﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;

namespace Anritsu.WebApp.GlobalWeb.Layouts
{
    public partial class HeaderLayout : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Cookies[CookieVariables.IPAddressCountryCode] == null ||
                    string.IsNullOrEmpty(Request.Cookies[CookieVariables.IPAddressCountryCode].Value))
                {
                    var ipAddressCountryItem = RetrieveCountryCodeFromIpAddress();
                    if (ipAddressCountryItem != null)
                    {
                        Response.SetCustomCookie(CookieVariables.IPAddressCountryCode,
                            ipAddressCountryItem["CountryCode"]);
                    }
                }
            }
            CountryList.DataSource = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).GetChildren()
                   .Where(x => x.Fields["ShowInRegionSelector"] != null && ((CheckboxField)x.Fields["ShowInRegionSelector"]).Checked);
            CountryList.DataBind();

            CountryName.DataSource = GetSelectedCountryDataSource();

            BindConfirmCountryList();

            if (!Sitecore.Context.PageMode.IsNormal) return;

            //create support cookie and check if browser cookies are enabled
            //create cookie in application begin request
            CreateCookie();

            //Request from Bookmark or search url
            var cultureUrl = GetCultureFromUrl();
            if (!string.IsNullOrEmpty(cultureUrl))
            {
                var countryCode = cultureUrl;
                if (countryCode.IndexOf('-') <= 0) return;
                countryCode = countryCode.Substring(3, 2);
                Response.SetCustomCookie(CookieVariables.UserCountryCode, countryCode);
            }
            else
            {
                //cookies are enabled
                if (Request.Browser.Cookies || Request.Cookies["SupportCookies"] != null)
                {
                    Response.SetCustomCookie(CookieVariables.UserCountryCode, string.Empty);
                    CookieEnabled();
                }
                //cookies are disabled
                else
                {
                    CookieDisabled();
                }
            }
        }

        //Detect country from Ip Address
        private Item RetrieveCountryCodeFromIpAddress()
        {
            var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];

            var dataRowCountryCode = Country.GetCountryByIp(ipAddress);

            return dataRowCountryCode == null ? null : GetCountryFromCountryCode(dataRowCountryCode["ISOCountryCode"].ToString());
        }

        public static bool IsNumber(string colValue)
        {
            long results;
            return long.TryParse(colValue, out results);
        }

        public string GetCultureCode(object item, string field)
        {
            if (item == null) return string.Empty;

            var referenceItem = ((ReferenceField)((Item)item).Fields[field]).TargetItem;
            return referenceItem != null ? referenceItem["Value"] : string.Empty;
        }

        private static Item GetCountryFromCountryCode(string countryCode)
        {
            return Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children.FirstOrDefault(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()));
        }

        private string GetCultureFromUrl()
        {
            var language = string.Empty;

            var url = HttpContext.Current.Request.Url.AbsolutePath;
            //url = url.Replace("www-", string.Empty).Replace("?", "/");
            var regexMatch = Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

            if (!string.IsNullOrEmpty(regexMatch.Value))
            {
                language = regexMatch.Value.Replace("/", string.Empty);
            }
            return language;

        }

        private void CreateCookie()
        {
            var c = new HttpCookie("SupportCookies", "true");
            Response.Cookies.Add(c);
        }

        private void CookieEnabled()
        {
            //No User cookie found
            if (Request.Cookies[CookieVariables.UserCountryCode] == null || Request.Cookies[CookieVariables.UserCountryCode].Value == string.Empty)
            {
                Item countryItem;
                if (Request.Cookies[CookieVariables.IPAddressCountryCode] != null && !string.IsNullOrEmpty(Request.Cookies[CookieVariables.IPAddressCountryCode].Value))
                {
                    countryItem = GetCountryFromCountryCode(Request.Cookies[CookieVariables.IPAddressCountryCode].Value);
                }
                else
                {
                    countryItem = RetrieveCountryCodeFromIpAddress();
                }

                //Country detected from IpAddress
                if (countryItem != null)
                {
                    Response.SetCustomCookie(CookieVariables.UserCountryCode, countryItem["CountryCode"]);
                    Response.SetCustomCookie(CookieVariables.IPAddressCountryCode, countryItem["CountryCode"]);
                    Response.Redirect("/" + GetCultureCode(countryItem, "CultureGroup"));
                }
                //No country detected from IpAddress
                else
                {
                    //check for VPN
                    var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
                    if (ipAddress.StartsWith("172.15.15", StringComparison.OrdinalIgnoreCase) || ipAddress.StartsWith("::1", StringComparison.OrdinalIgnoreCase))
                    {
                        // if connected through VPN default country should be 'US'
                        Response.Redirect("/" + "en-US");
                    }


                    //if no country detected from IP and is not on VPN
                    if (Request.UserLanguages == null) return;

                    var language = Request.UserLanguages[0].ToUpperInvariant().Trim();
                    var cultureInfo = CultureInfo.CreateSpecificCulture(language);

                    //country detected from browser language
                    if (!string.IsNullOrEmpty(cultureInfo.Name))
                    {
                        var countryCode = cultureInfo.Name;
                        if (countryCode.IndexOf('-') > 0)
                        {
                            countryCode = countryCode.Substring(3, 2);
                            Response.SetCustomCookie(CookieVariables.UserCountryCode, countryCode);
                            Response.SetCustomCookie(CookieVariables.IPAddressCountryCode, countryCode);
                            Response.Redirect("/" + cultureInfo.Name);
                        }
                        else
                        {
                            Response.SetCustomCookie(CookieVariables.IsCountryConfirmed, "false");
                            Response.Redirect("/" + "en");
                        }
                    }
                    //country not detected from browser language
                    else
                    {
                        Response.SetCustomCookie(CookieVariables.IsCountryConfirmed, "false");
                        Response.Redirect("/" + "en");
                    }
                }
            }
            //User cookie found
            else
            {
                var countryItem = GetCountryFromCountryCode(Request.Cookies[CookieVariables.UserCountryCode].Value);
                if (countryItem != null)
                {
                    Response.Redirect("/" + GetCultureCode(countryItem, "CultureGroup"));
                }
            }
        }

        private void CookieDisabled()
        {
            //No User cookie found
            if (Session[SessionVariables.UserCountryCode] == null || Session[SessionVariables.UserCountryCode].ToString() == string.Empty)
            {
                Item countryItem;
                if (Session[SessionVariables.IPAddressCountryCode] != null && !string.IsNullOrEmpty(Session[SessionVariables.IPAddressCountryCode].ToString()))
                {
                    countryItem = GetCountryFromCountryCode(Session[SessionVariables.IPAddressCountryCode].ToString());
                }
                else
                {
                    countryItem = RetrieveCountryCodeFromIpAddress();
                }

                //Country detected from IpAddress
                if (countryItem != null)
                {
                    Session[SessionVariables.UserCountryCode] = countryItem["CountryCode"];
                    Session[SessionVariables.IPAddressCountryCode] = countryItem["CountryCode"];
                    Response.Redirect("/" + GetCultureCode(countryItem, "CultureGroup"));
                }
                //No country detected from IpAddress
                else
                {
                    //check for VPN
                    var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
                    if (ipAddress.StartsWith("172.15.15", StringComparison.OrdinalIgnoreCase) || ipAddress.StartsWith("::1", StringComparison.OrdinalIgnoreCase))
                    {
                        // if connected through VPN default country should be 'US'
                        Response.Redirect("/" + "en-US");
                    }

                    //if no country detected from IP and is not on VPN
                    if (Request.UserLanguages == null) return;

                    var language = Request.UserLanguages[0].ToUpperInvariant().Trim();
                    var cultureInfo = CultureInfo.CreateSpecificCulture(language);

                    //country detected from browser language
                    if (!string.IsNullOrEmpty(cultureInfo.Name))
                    {
                        var countryCode = cultureInfo.Name;
                        if (countryCode.IndexOf('-') > 0)
                        {
                            countryCode = countryCode.Substring(3, 2);
                            Session[SessionVariables.UserCountryCode] = countryCode;
                            Session[SessionVariables.IPAddressCountryCode] = countryCode;
                            Response.Redirect("/" + cultureInfo.Name);
                        }
                        else
                        {
                            Session[SessionVariables.IsCountryConfirmed] = "false";
                            Response.Redirect("/" + "en");
                        }
                    }
                    //country not detected from browser language
                    else
                    {
                        Session[SessionVariables.IsCountryConfirmed] = "false";
                        Response.Redirect("/" + "en");
                    }
                }
            }
            //User cookie found
            else
            {
                var countryItem = GetCountryFromCountryCode(Session[SessionVariables.UserCountryCode].ToString());
                if (countryItem != null)
                {
                    Response.Redirect("/" + GetCultureCode(countryItem, "CultureGroup"));
                }
            }
        }

        private string GetSelectedCountryDataSource()
        {
            if (Request.Cookies[CookieVariables.UserCountryCode] == null ||
                string.IsNullOrEmpty(Request.Cookies[CookieVariables.UserCountryCode].Value)) return "";

            var countryItem = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children.FirstOrDefault(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(Request.Cookies[CookieVariables.UserCountryCode].Value.ToUpperInvariant()));

            return countryItem != null ? countryItem.Paths.FullPath : "";
        }

        protected string ConfirmCountryPopup
        {
            get
            {
                //cookies are disabled
                if (!Request.Browser.Cookies || Request.Cookies["SupportCookies"] == null)
                {
                    //IsConfirmedCookie if false
                    if (Session[SessionVariables.IsCountryConfirmed] != null &&
                        Session[SessionVariables.IsCountryConfirmed].ToString().Equals("true")) return "";

                    //country from IpAddress available in cookie
                    if (Session[CookieVariables.IPAddressCountryCode] == null ||
                        string.IsNullOrEmpty(Session[CookieVariables.IPAddressCountryCode].ToString())) return "";

                    if (Request.UserLanguages != null)
                    {
                        var language = Request.UserLanguages[0].ToUpperInvariant().Trim();
                        var cultureInfo = CultureInfo.CreateSpecificCulture(language);
                        var countryCode = cultureInfo.Name;
                        if (countryCode.IndexOf('-') > 0)
                        {
                            countryCode = countryCode.Substring(3, 2);
                        }

                        if (
                            GetCultureCodeFromCountry(countryCode)
                                .Equals(
                                    GetCultureCodeFromCountry(Session[CookieVariables.IPAddressCountryCode].ToString())))
                            return "";

                        confirmCountry.Item = Sitecore.Context.Database.GetItem(ItemIds.ConfirmYourCountry, LanguageManager.GetLanguage(GetCultureCodeFromCountry(countryCode)));
                        ConfirmButton.Text = Translate.TextByDomain("GlobalDictionary", "Confirm");
                        return "active";
                    }
                }
                //cookies are enabled
                else
                {
                    //IsConfirmedCookie if false
                    if (Request.Cookies[CookieVariables.IsCountryConfirmed] != null &&
                        Request.Cookies[CookieVariables.IsCountryConfirmed].Value.Equals("true")) return "";

                    //country from IpAddress available in cookie
                    if (Request.Cookies[CookieVariables.IPAddressCountryCode] == null ||
                        string.IsNullOrEmpty(Request.Cookies[CookieVariables.IPAddressCountryCode].Value)) return "";

                    if (Request.UserLanguages == null) return "";

                    var language = Request.UserLanguages[0].ToUpperInvariant().Trim();
                    var cultureInfo = CultureInfo.CreateSpecificCulture(language);
                    var countryCode = cultureInfo.Name;
                    if (countryCode.IndexOf('-') > 0)
                    {
                        countryCode = countryCode.Substring(3, 2);
                    }

                    if (
                        GetCultureCodeFromCountry(countryCode)
                            .Equals(
                                GetCultureCodeFromCountry(
                                    Request.Cookies[CookieVariables.IPAddressCountryCode].Value))) return "";

                    confirmCountry.Item = Sitecore.Context.Database.GetItem(ItemIds.ConfirmYourCountry, LanguageManager.GetLanguage(GetCultureCodeFromCountry(countryCode)));
                    ConfirmButton.Text = Translate.TextByDomain("GlobalDictionary", "Confirm");
                    return "active";
                }
                return "";
            }
        }

        private string GetCultureCodeFromCountry(string countryCode)
        {
            var countryItem = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children.FirstOrDefault(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()));
            return GetCultureCode(countryItem, "CultureGroup");
        }

        private void BindConfirmCountryList()
        {
            var listItemCollection = new ListItemCollection();
            foreach (var item in Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).GetChildren()
                .Where(x => x.Fields["ShowInRegionSelector"] != null && ((CheckboxField)x.Fields["ShowInRegionSelector"]).Checked))
            {
                var li = new ListItem
                {
                    Text = item["CountryName"],
                    Value = GetCultureCode(item, "CultureGroup")
                };
                li.Attributes.Add("data-country-code", item["CountryCode"]);
                listItemCollection.Add(li);
            }
            ConfirmCountriesDropDown.DataSource = listItemCollection;
            ConfirmCountriesDropDown.DataTextField = "Text";
            ConfirmCountriesDropDown.DataValueField = "Value";
            ConfirmCountriesDropDown.DataBind();
            //ConfirmCountriesDropDown.SelectedItem.Text = Request.Cookies[CookieVariables.IPAddressCountryCode] != null ? ConfirmCountry(Request.Cookies[CookieVariables.IPAddressCountryCode].Value) : string.Empty;
            ConfirmCountriesDropDown.SelectedIndex = ConfirmCountriesDropDown.Items.IndexOf(ConfirmCountriesDropDown.Items.FindByText(Request.Cookies[CookieVariables.IPAddressCountryCode] != null ? ConfirmCountry(Request.Cookies[CookieVariables.IPAddressCountryCode].Value) : string.Empty));
        }

        private static string ConfirmCountry(string countryCode)
        {
            var countryItem = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children.FirstOrDefault(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()));

            return countryItem != null ? countryItem["CountryName"] : string.Empty;
        }
    }
}