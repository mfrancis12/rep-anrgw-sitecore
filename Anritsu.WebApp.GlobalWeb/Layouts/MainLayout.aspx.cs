﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using Anritsu.WebApp.GlobalWeb.SiteSearch;

namespace Anritsu.WebApp.GlobalWeb.Layouts
{
    public partial class MainLayout : BasePage
    {
        protected int MaxSearchTermLength
        {
            get
            {
                return ConfigurationManager.AppSettings["SearchTermLength"] == null ?
                    128 : Convert.ToInt16(ConfigurationManager.AppSettings["SearchTermLength"]);
            }
        }

        protected string SearchPath
        {
            get
            {
                return string.Format(ConfigurationManager.AppSettings["SearchDefaultUrl"], Sitecore.Context.Language);
            }
        }

        protected string CDNPath
        {
            get { return string.Format("//{0}", ConfigurationManager.AppSettings["AWSCdnPath"]); }
        }
        //this property used to get the region information from culture Refer GWS-3745 ticket
        protected string Region
        {
            get
            {
                string culture = GetCultureFromUrl();
                if (!string.IsNullOrEmpty(culture))
                {
                    return culture;
                }
                else
                {
                    return "en-US";
                }
               
            }
        }

        private string RequestedUrl
        {
            get;
            set;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Sitecore.Context.Item == null)
            {
                TerminatePage = true;
                Context.ApplicationInstance.CompleteRequest();
                return;
            }
            try
            {
                //Adding <tilt> for all the items wxcept for downloads & faqs
                // For downloads & faqs <title> will be added in respective sublayouts as these are wild card items
                if (Sitecore.Context.Item.ID.ToString().Equals(ItemIds.SoftwareWildcardItem) ||
                    Sitecore.Context.Item.ID.ToString().Equals(ItemIds.FaqWildcardItem)) return;

                var htmlTitle = new HtmlTitle
                {
                    Text = GetMetaTitle + " | " +
                           Translate.TextByDomain("GlobalDictionary", "anritsucompany")
                };
                TitlePlaceHolder.Controls.Add(htmlTitle);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (SiteSearchHelper.IsSearchCrawler(Request))
                    return;

                if (!Page.IsPostBack)
                {
                    if (Request.Cookies[CookieVariables.IPAddressCountryCode] == null ||
                        string.IsNullOrEmpty(Request.Cookies[CookieVariables.IPAddressCountryCode].Value))
                    {
                        var ipAddressCountryItem = RetrieveCountryCodeFromIpAddress();
                        if (ipAddressCountryItem != null)
                        {
                            Response.SetCustomCookie(CookieVariables.IPAddressCountryCode,
                                ipAddressCountryItem["CountryCode"]);
                            Response.SetCustomCookie(CookieVariables.IsIPDetected, "true");
                        }
                    }


                    if (GsaHelpers.IsGsaCrawler())
                        AddGsaMetaInfo();
                }

                if (Sitecore.Context.PageMode.IsNormal)
                {
                    //create support cookie and check if browser cookies are enabled
                    //create cookie in application begin request
                    CreateCookie();

                    //Request from Bookmark or search url
                    string cultureUrl = GetCultureFromUrl();
                    if (!string.IsNullOrEmpty(cultureUrl))
                    {
                        var countryCode = cultureUrl;
                        if (countryCode.IndexOf('-') > 0)
                        {
                            countryCode = countryCode.Substring(3, 2);
                            string previousCulture = string.Empty;
                            if (Request.Cookies[CookieVariables.UserCountryCode] != null)
                            {
                                previousCulture = GetCultureCodeFromCountry(Request.Cookies[CookieVariables.UserCountryCode].Value);
                            }
                            if (string.IsNullOrEmpty(previousCulture) || !cultureUrl.Equals(previousCulture, StringComparison.OrdinalIgnoreCase))
                                Response.SetCustomCookie(CookieVariables.UserCountryCode, countryCode);
                        }
                    }
                    else
                    {
                        RequestedUrl = HttpContext.Current.Request.Url.AbsolutePath;
                        //cookies are enabled
                        if (Request.Browser.Cookies || Request.Cookies["SupportCookies"] != null)
                        {
                            Response.SetCustomCookie(CookieVariables.UserCountryCode, string.Empty);
                            CookieEnabled();
                        }
                        //cookies are disabled
                        else
                        {
                            CookieDisabled();
                        }
                    }
                }
                Item countryItem = Sitecore.Context.Item.Database.GetItem(GetSelectedCountryDataSource());
                if (countryItem != null && countryItem.Fields["CountryName"] != null)
                    hdnCountryName.Value = countryItem["CountryName"];
                BindConfirmCountryList();
                CountryList.DataSource = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).GetChildren()
                    .Where(
                        x =>
                            x.Fields["ShowInRegionSelector"] != null &&
                            ((CheckboxField)x.Fields["ShowInRegionSelector"]).Checked);
                CountryList.DataBind();
                                
                if (Response.Cookies[CookieVariables.UserCountryCode] != null)
                {
                    Response.Cookies[CookieVariables.UserCountryCode].HttpOnly = false;
                    if (Request.Cookies[CookieVariables.UserCountryCode] != null && !string.IsNullOrEmpty(Request.Cookies[CookieVariables.UserCountryCode].Value))
                        Response.Cookies[CookieVariables.UserCountryCode].Value = Request.Cookies[CookieVariables.UserCountryCode].Value;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
                if (Response.Cookies[CookieVariables.UserCountryCode] != null)
                    Response.Cookies[CookieVariables.UserCountryCode].HttpOnly = false;
            }
        }

        //Detect country from Ip Address
        private Item RetrieveCountryCodeFromIpAddress()
        {
            var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];

            var dataRowCountryCode = Country.GetCountryByIp(ipAddress);

            return dataRowCountryCode == null ? null : GetCountryFromCountryCode(dataRowCountryCode["ISOCountryCode"].ToString());
        }

        public static bool IsNumber(string colValue)
        {
            long results;
            return long.TryParse(colValue, out results);
        }

        public string GetCultureCode(object item, string field)
        {
            if (item == null) return string.Empty;
            var referenceItem = ((ReferenceField)((Item)item).Fields[field]).TargetItem;
            return referenceItem != null ? referenceItem["Value"] : string.Empty;
        }

        private Item GetCountryFromCountryCode(string countryCode)
        {
            return Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children.FirstOrDefault(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()));
        }

        private string GetCultureFromUrl()
        {
            var language = string.Empty;

            var url = HttpContext.Current.Request.Url.AbsolutePath;
            //url = url.Replace("www-", string.Empty).Replace("?", "/");
            var regexMatch = Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

            if (!string.IsNullOrEmpty(regexMatch.Value))
            {
                language = regexMatch.Value.Replace("/", string.Empty);
            }
            return language;

        }

        private void CreateCookie()
        {
            var c = new HttpCookie("SupportCookies", "true");
            Response.Cookies.Add(c);
        }

        private void CookieEnabled()
        {
            //Cookie exists for confirmed country
            if (Request.Cookies[CookieVariables.ConfirmedCountryCode] != null)
            {
                var countryItem = GetCountryFromCountryCode(Request.Cookies[CookieVariables.ConfirmedCountryCode].Value);
                Response.SetCustomCookie(CookieVariables.UserCountryCode, countryItem["CountryCode"]);
                Response.Redirect(string.Format("/{0}/{1}", GetCultureCode(countryItem, "CultureGroup"), RequestedUrl), false);
                Context.ApplicationInstance.CompleteRequest();
            }
            else
            {
                RedirectBasedOnBrowserLanguage();
            }
        }

		private void RedirectBasedOnBrowserLanguage()
        {

            string language = UserLanguage;
            CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture(language);

            //country detected from browser language
            if (!string.IsNullOrEmpty(cultureInfo.Name))
            {
                var countryCode = cultureInfo.Name;

                if (countryCode.IndexOf('-') > 0)
                {
                    string regionToRedirect;
                    countryCode = countryCode.Substring(3, 2);
                    Response.SetCustomCookie(CookieVariables.UserCountryCode, countryCode);
                    //Response.SetCustomCookie(CookieVariables.IPAddressCountryCode, countryCode);
                    //Response.SetCustomCookie(CookieVariables.IsIPDetected, "false");

                    //brwoser language fallback
                    var currentCountryItem = GetCountryFromCountryCode(countryCode);
                    if (!string.IsNullOrEmpty(currentCountryItem["CultureGroup"]))
                    {
                        Item referenceItem = ((ReferenceField)(currentCountryItem.Fields["CultureGroup"])).TargetItem;
                        regionToRedirect = referenceItem != null ? referenceItem["Value"] : "en";
                    }
                    else regionToRedirect = "en";

                    Response.Redirect(string.Format("/{0}/{1}", regionToRedirect, RequestedUrl), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    Response.SetCustomCookie(CookieVariables.IsCountryConfirmed, "false");
                    Response.Redirect(string.Format("/{0}/{1}", "en", RequestedUrl), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            //country not detected from browser language
            else
            {
                Response.SetCustomCookie(CookieVariables.IsCountryConfirmed, "false");
                Response.Redirect(string.Format("/{0}/{1}", "en", RequestedUrl), false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }

        private void CookieDisabled()
        {
            //No User cookie found
            if (Session[SessionVariables.UserCountryCode] == null || Session[SessionVariables.UserCountryCode].ToString() == string.Empty)
            {
                Item countryItem;
                if (Session[SessionVariables.IPAddressCountryCode] != null && !string.IsNullOrEmpty(Session[SessionVariables.IPAddressCountryCode].ToString()))
                {
                    countryItem = GetCountryFromCountryCode(Session[SessionVariables.IPAddressCountryCode].ToString());
                }
                else
                {
                    countryItem = RetrieveCountryCodeFromIpAddress();
                }

                //Country detected from IpAddress
                if (countryItem != null)
                {
                    Session[SessionVariables.UserCountryCode] = countryItem["CountryCode"];
                    Session[SessionVariables.IPAddressCountryCode] = countryItem["CountryCode"];
                    Response.Redirect(string.Format("/{0}/{1}", GetCultureCode(countryItem, "CultureGroup"), RequestedUrl), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                //No country detected from IpAddress
                else
                {
                    //check for VPN
                    var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
                    if (ipAddress.StartsWith("172.15.15", StringComparison.OrdinalIgnoreCase) || ipAddress.StartsWith("::1", StringComparison.OrdinalIgnoreCase))
                    {
                        // if connected through VPN default country should be 'US'
                        Response.Redirect(string.Format("/{0}/{1}", "en-US", RequestedUrl), false);
                        Context.ApplicationInstance.CompleteRequest();
                    }

                    //if no country detected from IP and is not on VPN
                    string language = UserLanguage;
                    CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture(language);

                    //country detected from browser language
                    if (!string.IsNullOrEmpty(cultureInfo.Name))
                    {
                        var countryCode = cultureInfo.Name;
                        if (countryCode.IndexOf('-') > 0)
                        {
                            countryCode = countryCode.Substring(3, 2);
                            Session[SessionVariables.UserCountryCode] = countryCode;
                            Session[SessionVariables.IPAddressCountryCode] = countryCode;
                            Response.Redirect(string.Format("/{0}/{1}", cultureInfo.Name, RequestedUrl), false);
                            Context.ApplicationInstance.CompleteRequest();
                        }
                        else
                        {
                            Session[SessionVariables.IsCountryConfirmed] = "false";
                            Response.Redirect(string.Format("/{0}/{1}", "en", RequestedUrl), false);
                            Context.ApplicationInstance.CompleteRequest();
                        }
                    }
                    //country not detected from browser language
                    else
                    {
                        Session[SessionVariables.IsCountryConfirmed] = "false";
                        Response.Redirect(string.Format("/{0}/{1}", "en", RequestedUrl), false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                }
            }
            //User cookie found
            else
            {
                Item countryItem = GetCountryFromCountryCode(Session[SessionVariables.UserCountryCode].ToString());
                if (countryItem != null)
                {
                    Response.Redirect(string.Format("/{0}/{1}", GetCultureCode(countryItem, "CultureGroup"), RequestedUrl), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
            }
        }

        private string GetSelectedCountryDataSource()
        {
            string countryCode = string.Empty;
            if (Request.Cookies[CookieVariables.UserSelectedCountry] != null && !string.IsNullOrEmpty(Request.Cookies[CookieVariables.UserSelectedCountry].Value))
            {
                countryCode = Request.Cookies[CookieVariables.UserSelectedCountry].Value;
            }
            else if (Request.Cookies[CookieVariables.UserCountryCode] != null && !string.IsNullOrEmpty(Request.Cookies[CookieVariables.UserCountryCode].Value))
            {
                countryCode = Request.Cookies[CookieVariables.UserCountryCode].Value;
            }
            var countryItem = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children.FirstOrDefault(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()));
            return countryItem != null ? countryItem.Paths.FullPath : string.Empty;
        }

        protected string ConfirmCountryPopup
        {
            get
            {
                //cookies are disabled
                if (!Request.Browser.Cookies || Request.Cookies["SupportCookies"] == null)
                {
                    //IsConfirmedCookie if false
                    if (Session[SessionVariables.IsCountryConfirmed] != null &&
                        Session[SessionVariables.IsCountryConfirmed].ToString().Equals("true")) return "";

                    //country from IpAddress available in cookie
                    if (Session[CookieVariables.IPAddressCountryCode] == null ||
                        string.IsNullOrEmpty(Session[CookieVariables.IPAddressCountryCode].ToString())) return "";

                    var language = UserLanguage;
                    var cultureInfo = CultureInfo.CreateSpecificCulture(language);
                    var countryCode = cultureInfo.Name;
                    if (countryCode.IndexOf('-') > 0)
                    {
                        countryCode = countryCode.Substring(3, 2);
                    }

                    if (
                        GetCultureCodeFromCountry(countryCode)
                            .Equals(
                                GetCultureCodeFromCountry(
                                    Session[CookieVariables.IPAddressCountryCode].ToString()))) return "";
                    confirmCountry.Item = Sitecore.Context.Database.GetItem(ItemIds.ConfirmYourCountry, LanguageManager.GetLanguage(GetCultureCodeFromCountry(countryCode)));
                    btnConfirmCountry.Value = Translate.TextByDomain("GlobalDictionary", "Confirm");

                    return "active";
                }
                //cookies are enabled
                else
                {
                    //IsConfirmedCookie if false
                    if (Request.Cookies[CookieVariables.IsCountryConfirmed] != null &&
                        Request.Cookies[CookieVariables.IsCountryConfirmed].Value.Equals("true")) return "";

                    //country from IpAddress available in cookie
                    if (Request.Cookies[CookieVariables.IPAddressCountryCode] == null ||
                        string.IsNullOrEmpty(Request.Cookies[CookieVariables.IPAddressCountryCode].Value))
                        return "";

                    if (Request.Cookies[CookieVariables.IsIPDetected] == null ||
                        !Request.Cookies[CookieVariables.IsIPDetected].Value.Equals("true")) return "";

                    var language = UserLanguage;
                    var cultureInfo = CultureInfo.CreateSpecificCulture(language);
                    var countryCode = cultureInfo.Name;
                    if (countryCode.IndexOf('-') > 0)
                    {
                        countryCode = countryCode.Substring(3, 2);
                    }

                    if (!GetCultureCodeFromCountry(countryCode).Equals(GetCultureCodeFromCountry(Request.Cookies[CookieVariables.IPAddressCountryCode].Value)))
                    {
                        confirmCountry.Item = Sitecore.Context.Database.GetItem(ItemIds.ConfirmYourCountry, LanguageManager.GetLanguage(GetCultureCodeFromCountry(countryCode)));
                        btnConfirmCountry.Value = Translate.TextByDomain("GlobalDictionary", "Confirm");
                        return "active";
                    }

                    var httpCookie = Request.Cookies[CookieVariables.UserCountryCode];
                    if (
                        httpCookie != null && GetCultureCodeFromCountry(httpCookie.Value)
                            .Equals(
                                GetCultureCodeFromCountry(
                                    Request.Cookies[CookieVariables.IPAddressCountryCode].Value)))
                        return "";

                    confirmCountry.Item = Sitecore.Context.Database.GetItem(ItemIds.ConfirmYourCountry, LanguageManager.GetLanguage(GetCultureCodeFromCountry(countryCode)));
                    btnConfirmCountry.Value = Translate.TextByDomain("GlobalDictionary", "Confirm");
                    return "active";
                }
            }
        }

        private string GetCultureCodeFromCountry(string countryCode)
        {
            var countryItem = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children.FirstOrDefault(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()));
            return GetCultureCode(countryItem, "CultureGroup");
        }

        private string UserLanguage
        {
            get
            {
                var language = Request.UserLanguages != null ? Request.UserLanguages[0].ToUpperInvariant().Trim().Split(';')[0] : Sitecore.Context.Language.Name.ToUpperInvariant().Trim();
                if (language.Equals("EN"))
                {
                    language = "EN-AU";
                }
                return language;
            }
        }

        private Language GetBrowserLanguage()
        {
            var language = Sitecore.Context.Language;
            if (Request.UserLanguages == null) return language;
            var browserLanguage = Request.UserLanguages[0].ToUpperInvariant().Trim().Split(';')[0];
            if (string.IsNullOrEmpty(browserLanguage)) return language;
            var cultureInfo = CultureInfo.CreateSpecificCulture(browserLanguage);
            var countryCode = cultureInfo.Name;
            if (countryCode.IndexOf('-') > 0)
            {
                countryCode = countryCode.Substring(3, 2);
            }
            return Language.Parse(GetCultureCodeFromCountry(countryCode));
        }

        private void BindConfirmCountryList()
        {
            Language language = GetBrowserLanguage();

            ListItemCollection listItemCollection = new ListItemCollection();

            foreach (Item item in Sitecore.Context.Database.GetItem(ItemIds.ISOCountries, language).GetChildren()
                .Where(x => x.Fields["ShowInRegionSelector"] != null && ((CheckboxField)x.Fields["ShowInRegionSelector"]).Checked))
            {
                ListItem li = new ListItem
                {
                    Text = item["CountryName"],
                    Value = GetCultureCode(item, "CultureGroup")
                };
                li.Attributes.Add("data-country-code", item["CountryCode"]);
                listItemCollection.Add(li);
            }
            listItemCollection.Add(new ListItem("More", LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(ItemIds.ChangeRegion))));
            foreach (ListItem iListItem in listItemCollection)
            {
                ConfirmCountriesDropDown.Items.Add(iListItem);
            }

            if (Request.Cookies[CookieVariables.IPAddressCountryCode] != null)
            {
                ConfirmCountriesDropDown.SelectedIndex = ConfirmCountriesDropDown.Items.IndexOf(ConfirmCountriesDropDown.Items.FindByText(ConfirmCountry(Request.Cookies[CookieVariables.IPAddressCountryCode].Value)));
            }
        }

        private string ConfirmCountry(string countryCode)
        {
            var language = GetBrowserLanguage();

            var countryItem = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries, language).Children.FirstOrDefault(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()));

            return countryItem != null ? countryItem["CountryName"] : string.Empty;
        }

        protected string GetMetaTitle
        {
            get
            {
                return (Sitecore.Context.Item != null) ? HttpUtility.HtmlDecode(Sitecore.Context.Item["MetaTitle"]) : string.Empty;
            }
        }

 
        private void AddGsaMetaInfo()
        {
            try
            {
                var meta = new HtmlMeta
                {
                    Name = "title",
                    Content = SiteSearchItemName.GetGsaItemName(Sitecore.Context.Item)
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "gsadescription",
                    Content = SiteSearchMetaDescription.GetGsaItemMetaDescription(Sitecore.Context.Item)
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "geo.region",
                    Content = Sitecore.Context.Language.Name
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    HttpEquiv = "Content-Language",
                    Content = Sitecore.Context.Language.Name.Split('-')[0]
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);
                var area = SiteSearchArea.GetGsaArea(Sitecore.Context.Item);
                meta = new HtmlMeta
                {
                    Name = "area",
                    Content = area
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);
                var coveoarea = SiteSearchArea.GetGsaArea(Sitecore.Context.Item, Language.Parse("en"));
                meta = new HtmlMeta
                {
                    Name = "coveoarea",
                    Content = coveoarea
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);


                //Adding sortOrder for all items except for products
                //For products adding sortOrder based on product discontinuity
                if (!Sitecore.Context.Item.TemplateID.ToString().Equals(TemplateIds.ProductRegion) &&
                    !Sitecore.Context.Item.TemplateID.ToString().Equals(TemplateIds.InfivisProductDetailStatic) &&
                    !Sitecore.Context.Item.TemplateID.ToString().Equals(TemplateIds.InfivisProductDetailStaticC1))
                {
                    meta = new HtmlMeta
                    {
                        Name = "sortorder",
                        Content = Convert.ToString(SiteSearchArea.GetSearchFilterSortOrder(area.Trim()))
                    };
                    if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);
                }

                //below metatag will be useful to filter results based on groupheader                
                meta = new HtmlMeta
                {
                    Name = "groupheader",
                    Content = GetGroupHeaderName()
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);

                if (!Sitecore.Context.Item.Axes.IsDescendantOf(
                    Sitecore.Context.Item.Database.GetItem(ItemIds.TrainingIndex))) return;
                var trainingItem = Sitecore.Context.Item.Database.GetItem(ItemIds.TrainingIndex);
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(trainingItem,
                    trainingItem.Fields["PageTitle"]);
                meta = new HtmlMeta
                {
                    Name = "category",
                    Content =
                        fallbackItem != null
                            ? fallbackItem.Fields["PageTitle"] != null
                                ? area.Substring(0, 2) + GsaMetaStringSeperators.AreaCategorySeparator +
                                  fallbackItem.Fields["PageTitle"].Value
                                : area.Substring(0, 2) + GsaMetaStringSeperators.AreaCategorySeparator +
                                  fallbackItem.Name
                            : ""
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) GsaMetaPlaceHolder.Controls.Add(meta);

                
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " " + ex.StackTrace, "GsaMetaInfoAddition");
            }
        }

        protected void CountryList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                //add arrow mark besides current country name
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Item countryItem = (Item)e.Item.DataItem;
                    HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("countryLnk");

                    if (countryItem["CountryName"] == hdnCountryName.Value)
                        relatedAnchor.Attributes.Add("class", "current");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }

        protected string GetGroupHeaderName()
        {
            BasePage basePg = new BasePage();
            Item grpHdrItm = basePg.GetGroupHeaderItem();
            // For ComponentsAndAccessories pages we need to add TestAndMeasurement group header name
            return IsCAGroupHeader(grpHdrItm) ? GetTMGroupHeaderName() : grpHdrItm.Name;
        }
    }

}