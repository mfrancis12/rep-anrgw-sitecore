﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UFALayout.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Layouts.UFALayout" %>

<%@ Register TagPrefix="asp" Namespace="Anritsu.WebApp.GlobalWeb.CustomFields" Assembly="Anritsu.WebApp.GlobalWeb" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="mt" Namespace="Sitecore.WebControls" Assembly="Sitecore.MetaTags" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Constants" %>

<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8"> <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9"> <![endif]-->
<!--[if gt IE 9]>  <html class="ie gte9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<%=Region%>" class="no-ie">
<!--<![endif]-->
<head runat="server">    
    <asp:PlaceHolder id="TitlePlaceHolder" runat="server" />

    <mt:metatags runat="server" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="msapplication-config" content="none"/>
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
	<asp:PlaceHolder runat="server">
	  <link rel="icon" href="<%=CDNPath %>/appfiles/img/icons/favicon.ico" type="image/png">
	  <meta http-equiv="content-language" content="<%=Region%>">
	  <link href="<%= CDNPath %>/appfiles/css/jquery-ui-min.css" rel="Stylesheet" type="text/css" />
      <link rel="stylesheet" href="<%=CDNPath %>/appfiles/css/globalmin.css?v=20171111">	
	</asp:PlaceHolder>
	<sc:VisitorIdentification id="VisitorIdentification" runat="server" /> 
</head>
<body class="<%=Sitecore.Context.Language.Equals(Sitecore.Globalization.Language.Parse("ja-JP"))?"japan":""%>">
        <input type="hidden" id="hdnSearchPath" value="<%=SearchPath%>" />
        <!--googleoff: all--><div class="skipnav"><a href="#maincontent">Skip to main content </a></div><!--googleon: all-->
        <div class="out-container">
            <sc:placeholder runat="server" key="Menu" />
            <div class="rightContent">
                <div class="wrap">
				
                    <sc:placeholder runat="server" key="Header" />
                    <div id="maincontent">
                        <sc:placeholder runat="server" key="Content" />
                    </div>
                    <div class="footer">
                        <sc:placeholder runat="server" key="Footer" />
                    </div>
                </div>
            </div>
            <!--googleoff: all-->
            <div class="search-popup  search-popup-container">
                <div id="search-bar" class="search-bar hide">
                    <input type="text" id="searchbox" autocomplete="off" class="ui-autocomplete-input" maxlength="<%= MaxSearchTermLength %>"/>
                    <i id="searchbarbutton" class="icon icon-search-new"></i>                                                               
                    <a class="icon icon-close-new closeLink"></a>
                </div>
            </div>
            <div class="country-popup country-popup-container">
                <div class="hd">
                   <div class="heading">
                     <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "VisitAnotherCountry") %></span>
                      <input type="hidden" id="hdnCountryName" runat="server"/>
                   </div>      
                    <a class="icon icon-close-new closeLink" href="#"></a>
                </div>
                <div class="bd">                    
                    <div class="country-list">
                        <asp:Repeater ID="CountryList" runat="server" ItemType="Sitecore.Data.Items.Item" OnItemDataBound="CountryList_ItemDataBound">
                            <ItemTemplate>
                                 <a href=' <%# "/"+GetCultureCode(Item,"CultureGroup") %>' class="country" runat="server" id="countryLnk"
                                   data-country-code='<%# Item["CountryCode"] %>'><%# Item["CountryName"] %></a>                         
                            </ItemTemplate>
                            <FooterTemplate>
                                <a href="<%# Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(ItemIds.ChangeRegion)) %>" class="more">
                                    <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "More") %> <i class="icon icon-next-new"></i></a>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <!--googleon: all-->
        </div>
    <div class="loading">
        <div class="sk-fading-circle">
          <div class="sk-circle1 sk-circle"></div>
          <div class="sk-circle2 sk-circle"></div>
          <div class="sk-circle3 sk-circle"></div>
          <div class="sk-circle4 sk-circle"></div>
          <div class="sk-circle5 sk-circle"></div>
          <div class="sk-circle6 sk-circle"></div>
          <div class="sk-circle7 sk-circle"></div>
          <div class="sk-circle8 sk-circle"></div>
          <div class="sk-circle9 sk-circle"></div>
          <div class="sk-circle10 sk-circle"></div>
          <div class="sk-circle11 sk-circle"></div>
          <div class="sk-circle12 sk-circle"></div>
        </div>
      </div>
    
    <script src="<%=CDNPath %>/appfiles/js/jquery.min.js"></script>
    <asp:Literal runat="server" ID="ltrDynamicScript"></asp:Literal>
    <script src="<%=CDNPath %>/appfiles/js/global.min.js?v=20171110" type="text/javascript"></script>
	<script src="<%= CDNPath %>/appfiles/js/jquery-ui.min.js" type="text/javascript"></script>
    <sc:Sublayout runat="server" Path="/GlobalWeb/SubLayouts/ScriptRenderer.ascx" DataSource="/sitecore/content/GlobalWeb/components/Scripts/global"/>  		
</body>
</html>

