﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Sitecore.Diagnostics;
using Sitecore.Globalization;

namespace Anritsu.WebApp.GlobalWeb.Layouts
{
    public partial class UFALayout : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                var htmlTitle = new HtmlTitle
                {
                    Text = GetPageTitle + "- " +
                           Translate.TextByDomain("GlobalDictionary", "anritsucompany")
                };
                TitlePlaceHolder.Controls.Add(htmlTitle);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }

        protected string GetPageTitle
        {
            get
            {
                return (Sitecore.Context.Item != null) ? HttpUtility.HtmlDecode(Sitecore.Context.Item["PageTitle"]) : string.Empty;
            }
        }

        protected int MaxSearchTermLength
        {
            get
            {
                return ConfigurationManager.AppSettings["SearchTermLength"] == null ?
                    128 : Convert.ToInt16(ConfigurationManager.AppSettings["SearchTermLength"]);
            }
        }

        protected string SearchPath
        {
            get
            {
                return string.Format(ConfigurationManager.AppSettings["SearchDefaultUrl"], Sitecore.Context.Language);
            }
        }

        protected string CDNPath
        {
            get { return string.Format("//{0}", ConfigurationManager.AppSettings["AWSCdnPath"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var ipAddressCountryItem = RetrieveCountryCodeFromIpAddress();
                if (ipAddressCountryItem != null)
                {
                    Response.SetCustomCookie(CookieVariables.IPAddressCountryCode, ipAddressCountryItem["CountryCode"]);
                }
            }

            Item countryItem = Sitecore.Context.Item.Database.GetItem(GetSelectedCountryDataSource());
            if (countryItem != null && countryItem.Fields["CountryName"] != null)
                hdnCountryName.Value = countryItem["CountryName"];
            BindConfirmCountryList();            
            CountryList.DataSource = Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).GetChildren()
                .Where(
                    x =>
                        x.Fields["ShowInRegionSelector"] != null &&
                        ((CheckboxField) x.Fields["ShowInRegionSelector"]).Checked);
            CountryList.DataBind();

            if (!Sitecore.Context.PageMode.IsNormal) return;
            //create support cookie and check if browser cookies are enabled
            //create cookie in application begin request
            createCookie();

            //Request from Bookmark or search url
            var cultureUrl = GetCultureFromUrl();
            if (!string.IsNullOrEmpty(cultureUrl))
            {
                var countryCode = cultureUrl;
                if (countryCode.IndexOf('-') <= 0) return;
                countryCode = countryCode.Substring(3, 2);
                Response.SetCustomCookie(CookieVariables.UserCountryCode, countryCode);
            }
            else
            {
                //cookies are enabled
                if (Request.Browser.Cookies || Request.Cookies["SupportCookies"] != null)
                {
                    Response.SetCustomCookie(CookieVariables.UserCountryCode, string.Empty);
                    CookieEnabled();
                }
                //cookies are disabled
                else
                {
                    CookieDisabled();
                }
            }
        }

        //Detect country from Ip Address
        private Item RetrieveCountryCodeFromIpAddress()
        {
            var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null
                ? Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
                : Request.ServerVariables["REMOTE_ADDR"];
            var bytes = IPAddress.Parse(ipAddress).GetAddressBytes();
            var ipNumber = 16777216*(long) bytes[0] + 65536*(long) bytes[1] + 256*(long) bytes[2] + (long) bytes[3];

            const string delimiter = ",;";
            Item countryCodeFromIp = null;
            var filePath = Server.MapPath("~/App_Data/IP2Location/IPCountry.csv");
            if (!File.Exists(filePath)) return countryCodeFromIp;
            var csvInfo = File.ReadAllLines(filePath)
                .Where(line => !string.IsNullOrEmpty(line))
                .Select(line => line.Split(delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

            foreach (var strLine in csvInfo.Where(strLine => (IsNumber(strLine[0].Trim('"'))
                                                              && long.Parse(strLine[0].Trim('"')) <= ipNumber) &&
                                                             (IsNumber(strLine[1].Trim('"'))
                                                              && ipNumber <= long.Parse(strLine[1].Trim('"')))))
            {
                countryCodeFromIp = GetCountryFromCountryCode(strLine[2].Trim('"'));
                if (countryCodeFromIp != null)
                {
                    //countryCodeFromIp = countryItem["CountryCode"];
                    return countryCodeFromIp;
                }
            }
            return countryCodeFromIp;
        }

        public static bool IsNumber(string colValue)
        {
            try
            {
                long.Parse(colValue);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetCultureCode(object item, string field)
        {
            if (item == null) return string.Empty;
            var referenceItem = ((ReferenceField) ((Item) item).Fields[field]).TargetItem;
            return referenceItem != null ? referenceItem["Value"] : string.Empty;
        }

        private Item GetCountryFromCountryCode(string countryCode)
        {
            Item countryItem = null;
            countryItem = (Item) Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children
                .Where(country => country.Fields["CountryCode"] != null
                                  &&
                                  country.Fields["CountryCode"].Value.ToUpperInvariant()
                                      .Equals(countryCode.ToUpperInvariant())).FirstOrDefault();
            return countryItem;
        }

        private string GetCultureFromUrl()
        {
            var language = string.Empty;

            var url = HttpContext.Current.Request.Url.AbsolutePath;
            //url = url.Replace("www-", string.Empty).Replace("?", "/");
            var regexMatch = Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

            if (!string.IsNullOrEmpty(regexMatch.Value))
            {
                language = regexMatch.Value.Replace("/", string.Empty);
            }
            return language;

        }

        private void createCookie()
        {
            HttpCookie c = new HttpCookie("SupportCookies", "true");
            Response.Cookies.Add(c);
        }

        private void CookieEnabled()
        {
            //No User cookie found
            if (Request.Cookies[CookieVariables.UserCountryCode] == null ||
                Request.Cookies[CookieVariables.UserCountryCode].Value == string.Empty)
            {
                Item countryItem;
                if (Request.Cookies[CookieVariables.IPAddressCountryCode] != null &&
                    !string.IsNullOrEmpty(Request.Cookies[CookieVariables.IPAddressCountryCode].Value))
                {
                    countryItem = GetCountryFromCountryCode(Request.Cookies[CookieVariables.IPAddressCountryCode].Value);
                }
                else
                {
                    countryItem = RetrieveCountryCodeFromIpAddress();
                }

                //Country detected from IpAddress
                if (countryItem != null)
                {
                    Response.SetCustomCookie(CookieVariables.UserCountryCode, countryItem["CountryCode"]);
                    Response.SetCustomCookie(CookieVariables.IPAddressCountryCode, countryItem["CountryCode"]);
                    Response.Redirect("/" + GetCultureCode(countryItem, "CultureGroup"));
                }
                //No country detected from IpAddress
                else
                {
                    //check for VPN
                    var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null
                        ? Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
                        : Request.ServerVariables["REMOTE_ADDR"];
                    if (ipAddress.StartsWith("172.15.15", StringComparison.OrdinalIgnoreCase) ||
                        ipAddress.StartsWith("::1", StringComparison.OrdinalIgnoreCase))
                    {
                        // if connected through VPN default country should be 'US'
                        Response.Redirect("/" + "en-US");
                    }


                    //if no country detected from IP and is not on VPN
                    var language = Request.UserLanguages[0].ToUpperInvariant().Trim();
                    var cultureInfo = CultureInfo.CreateSpecificCulture(language);

                    //country detected from browser language
                    if (!string.IsNullOrEmpty(cultureInfo.Name))
                    {
                        var countryCode = cultureInfo.Name;
                        if (countryCode.IndexOf('-') > 0)
                        {
                            countryCode = countryCode.Substring(3, 2);
                            Response.SetCustomCookie(CookieVariables.UserCountryCode, countryCode);
                            Response.SetCustomCookie(CookieVariables.IPAddressCountryCode, countryCode);
                            Response.Redirect("/" + cultureInfo.Name);
                        }
                        else
                        {
                            Response.SetCustomCookie(CookieVariables.IsCountryConfirmed, "false");
                            Response.Redirect("/" + "en");
                        }
                    }
                    //country not detected from browser language
                    else
                    {
                        Response.SetCustomCookie(CookieVariables.IsCountryConfirmed, "false");
                        Response.Redirect("/" + "en");
                    }
                }
            }
            //User cookie found
            else
            {
                var countryItem = GetCountryFromCountryCode(Request.Cookies[CookieVariables.UserCountryCode].Value);
                if (countryItem != null)
                {
                    Response.Redirect("/" + GetCultureCode(countryItem, "CultureGroup"));
                }
            }
        }

        private void CookieDisabled()
        {
            //No User cookie found
            if (Session[SessionVariables.UserCountryCode] == null ||
                Session[SessionVariables.UserCountryCode].ToString() == string.Empty)
            {
                Item countryItem;
                if (Session[SessionVariables.IPAddressCountryCode] != null &&
                    !string.IsNullOrEmpty(Session[SessionVariables.IPAddressCountryCode].ToString()))
                {
                    countryItem = GetCountryFromCountryCode(Session[SessionVariables.IPAddressCountryCode].ToString());
                }
                else
                {
                    countryItem = RetrieveCountryCodeFromIpAddress();
                }

                //Country detected from IpAddress
                if (countryItem != null)
                {
                    Session[SessionVariables.UserCountryCode] = countryItem["CountryCode"];
                    Session[SessionVariables.IPAddressCountryCode] = countryItem["CountryCode"];
                    Response.Redirect("/" + GetCultureCode(countryItem, "CultureGroup"));
                }
                //No country detected from IpAddress
                else
                {
                    //check for VPN
                    var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null
                        ? Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
                        : Request.ServerVariables["REMOTE_ADDR"];
                    if (ipAddress.StartsWith("172.15.15", StringComparison.OrdinalIgnoreCase) ||
                        ipAddress.StartsWith("::1", StringComparison.OrdinalIgnoreCase))
                    {
                        // if connected through VPN default country should be 'US'
                        Response.Redirect("/" + "en-US");
                    }

                    //if no country detected from IP and is not on VPN
                    var language = Request.UserLanguages[0].ToUpperInvariant().Trim();
                    var cultureInfo = CultureInfo.CreateSpecificCulture(language);

                    //country detected from browser language
                    if (!string.IsNullOrEmpty(cultureInfo.Name))
                    {
                        var countryCode = cultureInfo.Name;
                        if (countryCode.IndexOf('-') > 0)
                        {
                            countryCode = countryCode.Substring(3, 2);
                            Session[SessionVariables.UserCountryCode] = countryCode;
                            Session[SessionVariables.IPAddressCountryCode] = countryCode;
                            Response.Redirect("/" + cultureInfo.Name);
                        }
                        else
                        {
                            Session[SessionVariables.IsCountryConfirmed] = "false";
                            Response.Redirect("/" + "en");
                        }
                    }
                    //country not detected from browser language
                    else
                    {
                        Session[SessionVariables.IsCountryConfirmed] = "false";
                        Response.Redirect("/" + "en");
                    }
                }
            }
            //User cookie found
            else
            {
                var countryItem = GetCountryFromCountryCode(Session[SessionVariables.UserCountryCode].ToString());
                if (countryItem != null)
                {
                    Response.Redirect("/" + GetCultureCode(countryItem, "CultureGroup"));
                }
            }
        }

        private string GetSelectedCountryDataSource()
        {
            if (Request.Cookies[CookieVariables.UserCountryCode] == null ||
                string.IsNullOrEmpty(Request.Cookies[CookieVariables.UserCountryCode].Value)) return "";
            var countryItem = (Item) Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children
                .Where(country => country.Fields["CountryCode"] != null
                                  &&
                                  country.Fields["CountryCode"].Value.ToUpperInvariant()
                                      .Equals(Request.Cookies[CookieVariables.UserCountryCode].Value.ToUpperInvariant()))
                .FirstOrDefault();
            return countryItem != null ? countryItem.Paths.FullPath : "";
        }

        private string GetCultureCodeFromCountry(string countryCode)
        {
            Item countryItem = null;
            countryItem = (Item) Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children
                .Where(
                    country =>
                        country.Fields["CountryCode"] != null &&
                        country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()))
                .FirstOrDefault();
            return GetCultureCode(countryItem, "CultureGroup");
        }

        private void BindConfirmCountryList()
        {
            var listItemCollection = new ListItemCollection();
            foreach (var item in Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).GetChildren()
                .Where(
                    x =>
                        x.Fields["ShowInRegionSelector"] != null &&
                        ((CheckboxField) x.Fields["ShowInRegionSelector"]).Checked))
            {
                var li = new ListItem
                {
                    Text = item["CountryName"],
                    Value = GetCultureCode(item, "CultureGroup")
                };
                li.Attributes.Add("data-country-code", item["CountryCode"]);
                listItemCollection.Add(li);
            }
            //ConfirmCountriesDropDown.DataSource = listItemCollection;
            //ConfirmCountriesDropDown.DataTextField = "Text";
            //ConfirmCountriesDropDown.DataValueField = "Value";
            //ConfirmCountriesDropDown.DataBind();
            ////ConfirmCountriesDropDown.SelectedItem.Text = Request.Cookies[CookieVariables.IPAddressCountryCode] != null ? ConfirmCountry(Request.Cookies[CookieVariables.IPAddressCountryCode].Value) : string.Empty;
            //ConfirmCountriesDropDown.SelectedIndex = ConfirmCountriesDropDown.Items.IndexOf(ConfirmCountriesDropDown.Items.FindByText(Request.Cookies[CookieVariables.IPAddressCountryCode] != null ? ConfirmCountry(Request.Cookies[CookieVariables.IPAddressCountryCode].Value) : string.Empty));
        }

        private string ConfirmCountry(string countryCode)
        {
            Item countryItem = null;
            countryItem = (Item) Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children
                .Where(
                    country =>
                        country.Fields["CountryCode"] != null &&
                        country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant()))
                .FirstOrDefault();

            return countryItem != null ? countryItem["CountryName"] : string.Empty;
        }

        protected void CountryList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                //add arrow mark besides current country name
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Item countryItem = (Item)e.Item.DataItem;
                    HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("countryLnk");

                    if (countryItem["CountryName"] == hdnCountryName.Value)
                        relatedAnchor.Attributes.Add("class", "current");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }

        protected string Region
        {
            get
            {
                string culture = GetCultureFromUrl();
                if (!string.IsNullOrEmpty(culture))
                {
                    return culture;
                }
                else
                {
                    return "en-US";
                }

            }
        }
    }
}