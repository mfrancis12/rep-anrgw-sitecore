﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb
{
    public partial class ManageLinks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Create virtual user
            Sitecore.Security.Accounts.User user = Sitecore.Security.Authentication.AuthenticationManager.BuildVirtualUser(@"ANRGW\anritsueditor", true);
            if (user != null)
            {
                string domainRole = @"ANRGW\PRODUCT_CA";
                if (Sitecore.Security.Accounts.Role.Exists(domainRole))
                {
                    user.Roles.Add(Sitecore.Security.Accounts.Role.FromName(domainRole));
                }
                Sitecore.Security.UserProfile profile = user.Profile;
                profile.FullName = "Anritsu Editor";
                profile.Save();
                Sitecore.Security.Authentication.AuthenticationManager.Login(user.Name);
            }
            //Sitecore.Context.ClientPage.Start(this, "Run");
            Response.Redirect("/sitecore/client/Your Apps/Speak UI/LinkManagement/LinkDialog");
        }

        protected void Run()
        {
            Sitecore.Text.UrlString url = new Sitecore.Text.UrlString("/sitecore/client/Your Apps/Speak UI/LinkManagement/LinkDialog");
            //url.Append("id", args.Parameters["id"]);
            Sitecore.Context.ClientPage.ClientResponse.ShowModalDialog(url.ToString());
        }
    }
}