﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrokenLinks.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.BrokenLinks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Broken Links</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Sitecore Path :</label>
            <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox>
            <br />
            <br />
            <label>Language :</label>
            <asp:DropDownList ID="Languages" runat="server"></asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="ButtonBrokenLinks" runat="server" Text="Fix Broken Links" OnClick="ButtonBrokenLinks_Click" />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
