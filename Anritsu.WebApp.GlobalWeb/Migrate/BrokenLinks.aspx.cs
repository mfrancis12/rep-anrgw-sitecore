﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using HtmlAgilityPack;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class BrokenLinks : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        int itemCount = 0, linkCount = 0;

        private Dictionary<string, string> FileLinks = new Dictionary<string, string>
        {
            {"/Files/en-AU/","/en-au/test-measurement/files/"},
            {"/Files/en-GB/","/en-gb/test-measurement/files/"},
            {"/Files/en-US/","/en-us/test-measurement/files/"},
            {"/Files/ja-JP/","/ja-jp/test-measurement/files/"},
            {"/Files/ko-KR/","/ko-kr/test-measurement/files/"},
            {"/Files/zh-CN/","/zh-cn/test-measurement/files/"},
            {"/Files/zh-TW/","/zh-tw/test-measurement/files/"}
        };

        private Dictionary<string, string> S3links = new Dictionary<string, string>
        {
            {"/epub/","/en-us/test-measurement/epub/"},
            {"/Files/","/common/test-measurement/files/"},
            {"/GWImages/images/","/images/legacy-images/images/"},
            {"/GWImages/apps/","/images/legacy-images/apps/"},
            {"/GWImages/controls/","/images/legacy-images/controls/"},
            {"/GWImages/css/","/images/legacy-images/css/"},
            {"/GWImages/custom/","/images/legacy-images/custom/"},
            {"/GWImages/js/","/images/legacy-images/js/"},
            {"/RefFiles/en/","/en-en/test-measurement/reffiles/"},
            {"/RefFiles/en-AU/","/en-au/test-measurement/reffiles/"},
            {"/RefFiles/en-CA/","/en-ca/test-measurement/reffiles/"},
            {"/RefFiles/en-GB/","/en-gb/test-measurement/reffiles/"},
            {"/RefFiles/en-US/","/en-us/test-measurement/reffiles/"},
            {"/RefFiles/es-MX/","/es-mx/test-measurement/reffiles/"},
            {"/RefFiles/ja-JP/","/ja-jp/test-measurement/reffiles/"},
            {"/RefFiles/ko-KR/","/ko-kr/test-measurement/reffiles/"},
            {"/RefFiles/pt-BR/","/pt-br/test-measurement/reffiles/"},
            {"/RefFiles/ru-RU/","/ru-ru/test-measurement/reffiles/"},
            {"/RefFiles/zh-CN/","/zh-cn/test-measurement/reffiles/"},
            {"/RefFiles/zh-HK/","/zh-hk/test-measurement/reffiles/"},
            {"/RefFiles/zh-TW/","/zh-tw/test-measurement/reffiles/"},
            {"/StreamFiles/custom/","/common/test-measurement/streamfiles/"},
            {"/StreamFiles/en-US/","/en-us/test-measurement/streamfiles/"}
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private string GetS3Links(string link)
        {
            //string domain = "http://gwdata.cdn-anritsu.com";
            string domain = "http://dl-dev.cdn-anritsu.com";            
            string newLink = string.Empty;

            bool hasFileLink = FileLinks.Keys.Any(x => link.Contains(x));
            if (hasFileLink)
            {
                string oldurl = FileLinks.Keys.FirstOrDefault(key => link.Contains(key));
                string newurl = FileLinks[oldurl];
                newLink = link.Replace(oldurl, newurl);
                newLink = domain + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));
            }
            else
            {
                bool hasLink = S3links.Keys.Any(x => link.Contains(x));
                if (hasLink)
                {
                    string oldurl = S3links.Keys.FirstOrDefault(key => link.Contains(key));
                    string newurl = S3links[oldurl];
                    newLink = link.Replace(oldurl, newurl);
                    newLink = domain + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));
                }
                else
                {
                    newLink = link;
                }
            }

            if (newLink.Contains("http://downloadfiles.anritsu.com"))
                newLink = newLink.Replace("/http://downloadfiles.anritsu.com", "");

            else if (newLink.Contains("http://downloadfile.anritsu.com"))
                newLink = newLink.Replace("/http://downloadfile.anritsu.com", "");

            return newLink;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        protected void ButtonBrokenLinks_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            Language lang = Language.Parse(Languages.SelectedValue);
            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                try
                {
                    _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrokenLinks_Debug_{0}.txt", Languages.SelectedValue));
                    _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrokenLinks_Error_{0}.txt", Languages.SelectedValue));
                    _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrokenLinks_Meta_{0}.txt", Languages.SelectedValue));

                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["meta"].Log("-------------------------------------------------------------------");
                    _logger["meta"].Log("Item ID    Item Path");

                    string scPath = txtSCPath.Text;
                    if (string.IsNullOrEmpty(scPath) || Languages.SelectedIndex == 0)
                    {
                        _logger["error"].Log("Sitecore Path is empty or Language is not selected.");
                        return;
                    }
                    
                    Item parentItem = _master.GetItem(scPath, lang);
                    _logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                    foreach(Item item in  parentItem.Axes.GetDescendants())
                    {
                        try
                        {
                            bool flag = false;
                            item.Fields.ReadAll();
                            var fieldsList = item.Fields.Where(field => field.Type.Equals("Rich Text") && !field.Name.Contains("__")).ToList();
                            foreach (var field in fieldsList)
                            {
                                if(field.Type.Equals("Rich Text"))
                                {
                                    HtmlDocument doc = new HtmlDocument();
                                    doc.LoadHtml(field.Value);
                                    if (doc.DocumentNode.SelectSingleNode("//a") != null)
                                    {
                                        foreach (HtmlNode nodes in doc.DocumentNode.SelectNodes("//a"))
                                        {
                                            string link = nodes.GetAttributeValue("href", "");
                                            if (!link.StartsWith("http://gwdata.cdn-anritsu.com") && !link.StartsWith("http://dl-dev.cdn-anritsu.com") &&
                                                (FileLinks.Keys.Any(x => link.ToLower().Contains(x.ToLower()))
                                                || S3links.Keys.Any(x => link.ToLower().Contains(x.ToLower()))))
                                            {
                                                string value = GetS3Links(link);
                                                nodes.SetAttributeValue("href", value);
                                                _logger["debug"].Log("Old Value : {0}", link);
                                                _logger["debug"].Log("Updated Value : {0}", value);
                                                linkCount++;
                                                flag = true;
                                            }
                                        }
                                        using (new Sitecore.SecurityModel.SecurityDisabler())
                                        {
                                            if (flag)
                                            {
                                                _logger["debug"].Log("Editing item {0} started {1}", item.Name, item.Paths.Path);
                                                item.Editing.BeginEdit();
                                                item[field.Name] = doc.DocumentNode.InnerHtml.ToString();
                                                _logger["debug"].Log("Editing end");
                                                item.Editing.EndEdit();
                                            }
                                        }                                      
                                    }
                                }
                            }
                            itemCount++;
                            _logger["meta"].Log("{0}	{1}", item.ID.ToString(), item.Paths.Path);   
                        }
                        catch (Exception ex)
                        {
                            _logger["error"].Log("{0}", DateTime.Now.ToString());
                            _logger["error"].Log("{0}", item.ID.ToString());
                            _logger["error"].Log("{0}", ex.Message);
                            _logger["error"].Log("{0}", ex.StackTrace);
                        }  
                    }                   
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("Number of links edited :{0}", linkCount);
                    _logger["debug"].Log("Number of items parsed :{0}", itemCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }
    }
}