﻿using Amazon;
using Amazon.S3;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Sitecore.Data.Managers;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class CaseSensitive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        protected void ButtonSubmit_OnClick(object sender, EventArgs e)
        {
            string delimiter = ",;";
            string filePath = Server.MapPath("~/App_Data/IP2Location/Teamsite-Download-FilePath.csv");

            #region Update sitecore item with the download file path
            if (File.Exists(filePath))
            {
                var csvInfo = File.ReadAllLines(filePath)
                    .Where(line => !string.IsNullOrEmpty(line))
                    .Select(line => line.Split(delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

                //var downloadList = new List<Item>();
                var downloadquery = string.Format("fast:/sitecore/content/Global/downloads//*[@@templateid='{0}']", TemplateIds.Downloads);

                Sitecore.Data.Database db = Sitecore.Configuration.Factory.GetDatabase("master");

                var downloadList = db.SelectItems(downloadquery).ToList();
                MultiLogger _logger;
                using (_logger = new MultiLogger())
                {
                    _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "DownloadsUpdateS3Path_Track.txt");
                    _logger["error"].Log("Total Download File Count" + downloadList.Count);
                    int totalCount = 0;
                    int mismatchCount = 0;
                    foreach (var s3Path in csvInfo.Select(strLine => strLine[0]))
                    {
                        var selectedList = downloadList.Where(x => !string.IsNullOrEmpty(x.Fields["FilePath"].Value)
                            && x.Fields["FilePath"].Value.ToLower().Equals(s3Path.ToLower()));

                        _logger["error"].Log("Download File Count after comparing both in lower case: " + selectedList.Count());

                        foreach (var download in selectedList)
                        {
                            _logger["error"].Log("~~~~~~~~~~~~~~~~~~~~~");
                            _logger["error"].Log("Actual S3 Path : " + s3Path);

                            _logger["error"].Log("Download FilePath from Teamsite: " + download["FilePath"]);
                            _logger["error"].Log("~~~~~~~~~~~~~~~~~~~~~");

                            foreach (var language in LanguageManager.GetLanguages(db)
                                .Where(x => x.Name.ToLower().Equals("ja-jp") ||
                                            x.Name.ToLower().Equals("ru-ru") ||
                                            x.Name.ToLower().Equals("ko-kr")))
                            {
                                var downloadItem = db.GetItem(download.ID.ToString(), language);

                                _logger["error"].Log("Download Item Name : " + downloadItem.Name);

                                //if (downloadItem == null) continue;
                                if (downloadItem == null)
                                {
                                    _logger["error"].Log("Download Item doesn't exist for :" + s3Path + " In " + language.Name);
                                    continue;
                                }

                                if (downloadItem != null && downloadItem["FilePath"].Equals(s3Path))
                                {
                                    _logger["error"].Log("-------------------------------");
                                    _logger["error"].Log("Download Language : " + language);
                                    _logger["error"].Log("Download Name : " + downloadItem.Name);
                                    _logger["error"].Log("Download Path : " + downloadItem.Paths.FullPath);
                                    _logger["error"].Log("Download Items which doesn't have any mismatch --- S3Path = " + s3Path);
                                    _logger["error"].Log("Download Items which doesn't have any mismatch --- teamsite path = " + downloadItem.Fields["FilePath"].Value.ToString());
                                    _logger["error"].Log("-------------------------------");

                                }
                                else if (downloadItem != null && !downloadItem["FilePath"].Equals(s3Path))
                                {
                                    using (new SecurityDisabler())
                                    {
                                        mismatchCount = mismatchCount + 1;
                                        _logger["error"].Log("================================");
                                        _logger["error"].Log("Download File Path Mismatch information");
                                        _logger["error"].Log("Download Name : " + downloadItem.Name);
                                        _logger["error"].Log("Download File Path : " + downloadItem.Paths.FullPath);
                                        _logger["error"].Log("For Language" + language.Name);
                                        _logger["error"].Log("Amazon S3 Path :  " + s3Path + downloadItem.Fields["FilePath"].Value.ToString());
                                        _logger["error"].Log("File Path from Teamsite:  " + downloadItem.Fields["FilePath"].Value.ToString());
                                        _logger["error"].Log("Mismatch File Count:  " + mismatchCount);
                                        _logger["error"].Log("================================");

                                        downloadItem.Editing.BeginEdit();
                                        downloadItem["FilePath"] = s3Path;
                                        downloadItem.Editing.EndEdit();

                                    }
                                }
                            }
                        }
                        totalCount = totalCount + 1;
                        _logger["error"].Log("Toal S3 File Count" + totalCount);
                    }
                }
            }
            LabelSuccess.Visible = true;
            #endregion
        }

        private AmazonS3Client GetS3Client()
        {
            AmazonS3Client s3Client;
            RegionEndpoint regionEndpoint = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["AWSEndPoint"]);
            s3Client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSAccessKey"], ConfigurationManager.AppSettings["AWSSecretKey"], regionEndpoint);
            return s3Client;
        }
    }
}