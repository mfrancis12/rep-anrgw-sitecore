﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.ContactUs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contact Us Migration</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Teamsite DCR Path :</label>
            <asp:TextBox ID="txtTSDCRPath" runat="server"></asp:TextBox>
            <br />
            <br />
            <label>Sitecore Template:</label>
            <asp:DropDownList ID="SCTemplates" runat="server"></asp:DropDownList>
            <br />
            <br />
            <label>Sitecore Path :</label>
            <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox>
            <br />
            <br />
            <label>Select Language :</label>
            <asp:DropDownList ID="Languages" runat="server"></asp:DropDownList><br />
            <br />
            <asp:Button ID="btnMigrate" runat="server" Text="Migrate Contact Us" OnClick="btnMigrate_Click"/>
        </div>
    </form>
</body>
</html>
