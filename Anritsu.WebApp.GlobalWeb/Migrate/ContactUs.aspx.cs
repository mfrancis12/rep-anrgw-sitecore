﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using ContentMigration.Services;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class ContactUs : System.Web.UI.Page
    {
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");
        private IEnumerable<TemplateItem> _sitecoreTemplates = null;
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        int itemCount = 0, versionCount = 0, overwriteCount = 0;

        private string comInfoPath = "/sitecore/content/GlobalWeb/components/contact-us/corporate-information/corporate-information";

        private string tmSalesPath = "/sitecore/content/GlobalWeb/components/contact-us/test-measurement/sales";
        private string tmServicePath = "/sitecore/content/GlobalWeb/components/contact-us/test-measurement/service-and-support";

        private string caSalesPath = "/sitecore/content/GlobalWeb/components/contact-us/components-and-accessories/sales";
        private string caServicePath = "/sitecore/content/GlobalWeb/components/contact-us/components-and-accessories/service-and-support";

        private string ceSalesPath = "/sitecore/content/GlobalWeb/components/contact-us/company-equipment/sales";
        private string ceServicePath = "/sitecore/content/GlobalWeb/components/contact-us/company-equipment/service-and-support";

        private string adSalesPath = "/sitecore/content/GlobalWeb/components/contact-us/optical-and-high-speed-devices/sales";
        private string adServicePath = "/sitecore/content/GlobalWeb/components/contact-us/optical-and-high-speed-devices/service-and-support";

        private Dictionary<string, string> countries = new Dictionary<string, string>
        {
            {"1", "{F8E10FC3-EC61-4569-ACFE-102DBE2ADE1C}"},
            {"2", "{A256B9E2-720A-4A97-B3F5-A36ED4883000}"},
            {"3","{488D5EB5-04CB-4A22-95F3-12844410A4B4}"},
            {"4","{762BD231-0303-4039-9C15-D51EB561552D}"},
            {"5","{99239EDB-463A-4EAC-B575-8C5EB1084A56}"},
            {"6","{FF4B8458-48FE-49FF-8048-63B158C1A22D}"},
            {"7","{F910B28D-FB60-4A35-A686-9C06B02DBD0F}"},
            {"8","{9249613C-0B8A-4312-9CAF-EF2B9BF05AD0}"},
            {"9","{F61826E5-7340-4D41-AEC6-9C91D4FC6253}"},
            {"10","{9FC18084-EE4F-43EA-8740-642117788676}"},
            {"11","{E74D37D2-4704-4DD4-A611-ECC8DC76C2F8}"},
            {"12","{AC9D08F2-751A-40C8-9A6E-40C89F380D0F}"},
            {"13","{9095271B-7E05-4B8F-8EFF-2AD781BF4F02}"},
            {"14","{BA0CF67B-0685-4E89-AAE0-75E896C28DE7}"},
            {"15","{C595DFA5-E74D-4A84-8434-606861F328C9}"},
            {"16","{1D7C349D-F2C9-475F-B330-B3EB006E13B7}"},
            {"17","{325CE1F9-E397-48E8-A645-FFCF21A506AC}"},
            {"18","{D801CA4E-1462-4FDE-9FEB-14D9EFF474FB}"},
            {"19","{FFE999FF-F3E4-43AA-9A06-8ACFAA560D61}"},
            {"20","{B834D9EE-ADDE-48F3-876D-21BD3D87D3A5}"},
            {"21","{8879FE66-B475-414C-8DC5-76491E252DC5}"},
            {"22","{8980BCC2-3F93-4E1C-B4A7-9950F8329FB2}"},
            {"23","{07F17E5A-2DAE-4809-B8D4-C392257B61F0}"},
            {"24","{4C759B93-0642-4C8C-9506-807568E21976}"},
            {"25","{3CADA1D8-C2D8-4703-924F-121658AC72BE}"},
            {"26","{E4BBBE35-5155-43C1-93AC-7227D1EAB157}"},
            {"27","{D9C40F42-9B75-4BD1-BA78-A1208FDECF3A}"},
            {"28","{E42F556F-7CA1-40C5-A437-5F87456FC904}"},
            {"29","{50F2B69B-F835-4E83-AC67-4A076508A2CF}"},
            {"30","{B68EF0B6-9379-4556-AD45-84D14F12067E}"},
            {"31","{37307BB1-4213-47C4-BD0D-867241161834}"},
            {"32","{99E39C29-025C-4B42-AA9A-E33E0DE55EA4}"},
            {"33","{74244728-63DA-48AB-B9A0-3AD3D717B27E}"},
            {"34","{50A7DA85-4764-4253-B2F8-390EA7A00F5C}"},
            {"35","{6414FED3-F72A-4422-941B-2F3C98F3B7E5}"},
            {"36","{37FB5345-DE20-4DBD-AF18-806B5EDE6F23}"},
            {"37","{E4C9160A-2E68-4C83-9A52-DB9B3311D340}"},
            {"38","{00712A41-B4BF-49D4-BAD3-538F8DCB8B65}"},
            {"39","{21021AB5-6D2F-4C02-A4F9-3ECD8D5B1C36}"},
            {"40","{B7E6E065-56B3-442F-BE21-BD81FF8E5BB3}"},
            {"41","{B2E8BDF8-1714-4700-8940-EB9C83B62755}"},
            {"42","{EE9399EE-9A50-43EA-8A04-577B2944DCCA}"},
            {"43","{55FF5290-4477-460F-94F4-A3A75112341B}"},
            {"44","{85D45DA5-BD24-4241-A52A-F5EC3B71381E}"},
            {"45","{0FE1EAA3-AECB-430F-A3F7-E814A6A04E7D}"},
            {"46","{1A126A0E-375E-4D34-A7E5-1612D4F1DBA9}"},
            {"47","{7EAD0B9E-779C-4253-A6EC-9705F55ECA7C}"},
            {"48","{ADFE1C23-EEE8-4B8A-970B-CE404FD00699}"},
            {"49","{4699532E-0874-4517-9745-F06B43E53793}"},
            {"50","{4BB04FEE-7913-4899-A295-F6D55D756F63}"},
            {"51","{D0007B13-87FB-4D9F-B370-24D55325E25A}"},
            {"52","{2B1E0C32-752C-4D25-9AAF-81E527BEA980}"},
            {"53","{50A72EDB-A8A3-43BF-89DC-298B7418E202}"},
            {"54","{330A3096-B5F5-4CC4-BA36-A737A3DBA6BC}"},
            {"55","{D0F40AF3-56B8-446F-9866-541C640795E1}"},
            {"56","{02546EEC-0245-47F5-AB45-86702CD434BA}"},
            {"57","{A08079B5-65E9-4E0B-BBEF-F490BC5BCD94}"},
            {"58","{684B0AFD-B134-4ACB-91B3-23EF759C9344}"},
            {"59","{416B3DCD-1738-46B5-B562-7D633CF031AB}"},
            {"60","{AE86818E-BBD6-498C-9DAC-05E0E126E240}"},
            {"61","{D4B3EE41-B273-4253-93CB-A810EF1C5BCE}"},
            {"62","{B84E5865-4CB3-498D-A635-3D1909118449}"},
            {"63","{62459AC0-16C6-4C55-B8C8-052D66BCCB29}"},
            {"64","{F818E127-885D-479A-9424-219A58DC5444}"},
            {"65","{87D4BE41-76E4-4C04-BF9F-C44F6A813642}"},
            {"66","{FE6218F4-E375-4E4B-ABF1-B73A74D9DA8F}"},
            {"67","{7EED3D22-F23F-4830-9625-4CD22C602B78}"},
            {"68","{A771F2E2-8449-46F9-B29E-A677216985E1}"},
            {"69","{725A47DE-D412-4836-B271-B93AE26E97BC}"},
            {"70","{267B1536-1E0B-4F43-BE95-157574E621E5}"},
            {"71","{FF00C05E-7664-4738-8E09-8F33A334F44D}"},
            {"72","{9C4F5455-B5E7-4C7E-B14E-693BE32801C8}"},
            {"73","{D424468C-B276-41A0-B651-825352842102}"},
            {"74","{25924D89-CF39-472E-ACBE-27060A5FE5CA}"},
            {"75","{FE2FE833-72A4-4283-8A03-3B25710B9DFA}"},
            {"76","{F9479642-4BAB-4CA5-AAA1-817734AB99DE}"},
            {"77","{F76E511C-55A6-4D75-93B7-EE6300940111}"},
            {"78","{716FA41C-8767-49FE-B858-D205C0BC75BD}"},
            {"79","{1BB49DEF-906A-4E16-8B68-71CF3D382D85}"},
            {"80","{976F6A05-6C58-4E6E-8FCB-8A77743BD39E}"},
            {"81","{2C7E5527-AB62-4B40-B320-03D393C2EBA8}"},
            {"82","{3D52DAED-4355-4018-B285-6C73B2454199}"},
            {"83","{E8034A5C-CFC9-49FE-A56D-73A692E045C6}"},
            {"84","{BC112943-1E61-4F91-90E9-CC61D4D9BF2A}"},
            {"85","{F579BC7B-E314-42FD-9B54-B53E6342C2FC}"},
            {"86","{919B42D6-AAE4-4421-8CDC-0F7A3A5D4995}"},
            {"87","{7F764B27-C650-41EB-8480-B3ED3C2D6318}"},
            {"88","{DD2A82B4-071C-4D67-8E07-119F13B040BB}"},
            {"89","{8402025E-9D1C-4458-8369-E615CC53FD08}"},
            {"90","{429AF626-0FE6-4A18-B979-C61EB9C517B0}"},
            {"91","{D9A8B4E9-32D4-4629-9449-8EF0F586883A}"},
            {"92","{24F7C409-BAEB-4DF9-A78F-57EA6761DC63}"},
            {"93","{6E0AB498-2DFF-47E5-ACD1-D5D7E3E55AD8}"},
            {"94","{B43AEF16-B511-4DFC-B59B-ED4EF98E8304}"},
            {"95","{063C5DFB-70C2-40EE-A2AC-CC0AAF0CCFD2}"},
            {"96","{691A54D3-F476-4054-BE54-CFC6E15F08A0}"},
            {"97","{665C3F4F-3A7A-45AD-867A-850886A458D0}"},
            {"98","{BFD103B9-2422-434D-B05B-E866FC6A66A3}"},
            {"99","{4643A7AC-2B57-48C0-9E6B-9241377ADA59}"},
            {"100","{03C90CF6-9CC2-4ACB-92FA-41868B38CDC0}"},
            {"101","{EA7BA1DE-8FAE-4F15-B751-F0C777807FF2}"},
            {"102","{1C6FEC42-6E32-43DB-AC82-2FA37FB0E344}"},
            {"103","{F1814652-ADAD-498B-890A-A294DBF85C12}"},
            {"104","{D701845C-C7C8-421D-8A2E-59996F8E72AC}"},
            {"105","{A347FD4D-06E9-4CD4-BA38-8057BEF6B149}"},
            {"106","{74FEC0F6-D3E3-4087-9DB4-3C356EE076E2}"},
            {"107","{3AAE787F-A184-43BE-8109-BD454DE70086}"},
            {"108","{FE17F020-3A7C-4D22-AD8F-3CFF8E639447}"},
            {"109","{7A85FFF6-72E0-4349-A0A8-E7C385700C48}"},
            {"110","{9795A70F-27EB-4DE2-AD04-FD75B4B57691}"},
            {"111","{C2CEB1D7-C26B-4EF1-AE28-A2D2B9D4BB3E}"},
            {"112","{B8FD429D-2E70-42B2-84CA-021E689D42AB}"},
            {"113","{E463A4DD-8A3C-4754-83DA-2D9CF5ED74A8}"},
            {"114","{38752918-D0A5-48C0-B8D6-A9D41D5E26D3}"},
            {"115","{6B808DAA-C936-4F3B-89F4-6E183F480F87}"},
            {"116","{73292D75-62C5-437D-A47C-E787C1F70CF7}"},
            {"117","{A850B78E-B5D9-44CF-A06F-E4A72E60AD4D}"},
            {"118","{E4AE13C3-A5EC-4629-8462-0C1081B44460}"},
            {"119","{C395402A-0B49-4CB5-ABC3-2CB1B5433523}"},
            {"120","{FC42DDA4-D08A-439A-9557-5CB9C48C3B8C}"},
            {"121","{8ED8EEDD-BBCE-4ED2-9BE5-9CD2EDE3F319}"},
            {"122","{2E81ABD7-A7D0-423D-A4EB-11E1D0AB14B8}"},
            {"123","{0791197E-2372-4057-802E-B178CA601612}"},
            {"124","{0431897F-0D6F-43BB-9224-E5ED3F0B2570}"},
            {"125","{63A30F8D-C81C-42C6-BA2B-ACDA19FD610A}"},
            {"126","{B0A0F778-BA35-48DC-B75D-61D7CAB0AB79}"},
            {"127","{82ECCCB0-DC00-497B-8FB8-FFA7DF1556EF}"},
            {"128","{F43872A3-7207-4EFF-9685-4DE86AA991E2}"},
            {"129","{C2DEBB1C-9116-44F2-9C29-5A21B5AF12FA}"},
            {"130","{DFE87739-9C4A-416E-90D9-2F54FD8FB276}"},
            {"131","{3AC746D6-DD64-4F9C-AB0F-D3F6CE1682A9}"},
            {"132","{E2BE068B-4B61-4DC0-8C8D-A80FBD61AF50}"},
            {"133","{4DDE9976-B383-44AC-8A39-783152AD0E7C}"},
            {"134","{79F3073E-7852-44C2-B56A-3FC641EDB001}"},
            {"135","{97078F4A-7A1A-4B2B-8B86-46C4811F46D7}"},
            {"136","{6D13F321-7DA3-4AD3-8E91-E7D6AF1EEB2F}"},
            {"137","{47DD67A5-F043-4A57-BD00-A11B8EBCBD2B}"},
            {"138","{EE642DFA-E77B-4B8D-A152-F7944E435117}"},
            {"139","{15C7CDAF-60C1-49DD-8F24-79CA04F841DF}"},
            {"140","{1275C993-2285-45F4-A180-42AE35286C74}"},
            {"141","{C719D3CD-0936-4A31-84B9-387D430B72AC}"},
            {"142","{94F58195-31C6-4DF1-A3A2-835508AFCAA4}"},
            {"143","{FCB5530E-0320-40D9-9518-A40628E59481}"},
            {"144","{4EFBA76C-3F72-4E26-B088-62A11DCBA7EB}"},
            {"145","{C24234B8-C02D-48DF-BDBE-C4B9061F0697}"},
            {"146","{AF6B4E39-F146-4D33-B695-C54643A6BBDE}"},
            {"147","{574DEDEF-18EB-422E-A32E-07FD8B29462C}"},
            {"148","{D3684250-4A80-419F-8460-B7EA6001AA5C}"},
            {"149","{E6788FB5-A53B-4D55-90A8-1289A007BFD3}"},
            {"150","{61582514-5DFD-46AE-B0BD-D2E272E27ECB}"},
            {"151","{21A6C0B8-F1A4-49A1-A9E5-57E918F945C0}"},
            {"152","{1D1772FD-170B-40E9-BCEB-13F1C4A3DF4E}"},
            {"153","{36AB4585-97C4-4CBF-821A-8F624A4A6D67}"},
            {"154","{8016ABBA-4B25-4D36-8942-B3AD26B5933D}"},
            {"155","{90EDD7EC-DD09-4CC0-AE26-D2BD554B65AA}"},
            {"156","{6CA3C0AB-2DD6-4835-AF3D-6A0C614486B0}"},
            {"157","{79D7E8BA-8386-432D-9164-2CF513B52585}"},
            {"158","{41380142-B891-41D9-A71A-287759C2D3F5}"},
            {"159","{F6B35151-FCDE-4DE1-A150-807AEF6B855E}"},
            {"160","{9AA0E1E4-E6BF-4C7C-B911-3029FA412F78}"},
            {"161","{35818CB0-C782-4111-9FAA-CC3959B02CEA}"},
            {"162","{CEB24FE6-9192-4193-AACC-F9FEBEBDC19B}"},
            {"163","{CFF61640-D641-4CE7-A24E-F7B6E6BFB98A}"},
            {"164","{8A048332-38AB-4195-9E54-5B62C50535E3}"},
            {"165","{E7C3ECC6-2C64-4A2E-9861-8169F47E4F12}"},
            {"166","{46DBB44C-6DEA-4DD1-93F9-E4C8622C0310}"},
            {"167","{5063B575-852D-4E58-8D3E-7ABCCF21177E}"},
            {"168","{E1F3F164-2B67-404D-802B-682421CFE8A1}"},
            {"169","{A19CE2E0-2CE7-47A7-98C0-E43E0BEFB4D3}"},
            {"170","{DB3F2D65-C831-4559-B669-0E87FB25498D}"},
            {"171","{611BB8A0-26BE-4FEB-BD48-40D5CF685B5D}"},
            {"172","{E415D5AB-8164-4B71-AC42-32F7CF6D00E4}"},
            {"173","{E0A4D537-53DA-420C-96A0-F62952294AF3}"},
            {"174","{94C5B1AF-A99B-42E3-8269-C4195EA51C15}"},
            {"175","{461E9D39-E493-4DBC-9D6A-04CDD690F92E}"},
            {"176","{5458DF60-5627-48C7-A094-87AFCDF6E54E}"},
            {"177","{E4D223DA-5F6D-491A-AC01-928345F85104}"},
            {"178","{7D7C2C64-42AA-4B20-869F-3CF066A33C38}"},
            {"179","{18A3AB56-7B77-4807-B2FC-FC75040AB08B}"},
            {"180","{2BAFA0FE-4291-4125-B591-E3CA297486F7}"},
            {"181","{07D31CA2-8CF1-4E25-8103-B2E5930BD6FC}"},
            {"182","{6C08C300-F27D-4398-BD4D-30EB22963F1B}"},
            {"183","{D15978FC-DA38-4451-B2D4-378DBDBB677F}"},
            {"184","{5CCA39B6-22A2-446C-9096-4FDD7C1819BC}"},
            {"185","{C7F73CF2-667A-4E0E-83D8-7A36FE49673C}"},
            {"186","{B2F6D09A-B9B7-432F-8B86-0AC0C2767485}"},
            {"187","{7076FE31-F1FE-4811-9B38-A24EE8664734}"},
            {"188","{19F56123-A8D6-474E-A2B9-C22DB1C86F82}"},
            {"189","{7AC714FA-F02B-4CCA-8B12-EB919D9E8B8A}"},
            {"190","{D565BF56-F53D-498E-91E1-D80432E72752}"},
            {"191","{6085C360-A5D4-49EC-B51D-20FF9DD3D58D}"},
            {"192","{DDCD1EA2-CE35-4CE5-8DB5-35AF5E12F6C9}"},
            {"193","{F31CBA33-3EBB-479E-96E0-5E15BEE328C3}"},
            {"194","{766E6046-66A0-473B-B23D-333AC4CAB7DF}"},
            {"195","{5DE849A3-975C-4112-A49D-EFFC01FBB927}"},
            {"196","{A89DB475-703B-4D0A-99F3-EA098DE50757}"},
            {"197","{DCA65783-735C-404F-89EE-E9012BFFB936}"},
            {"198","{443D1542-72F3-4B95-9241-347D48FEA966}"},
            {"199","{05F5FAE8-14BD-40FC-A7D1-97DFB0967048}"},
            {"200","{427DF804-2C62-4B46-AF68-F5A8ABEBD749}"},
            {"201","{20050AFB-50D5-4DBE-8ED3-9861C0FF02FB}"},
            {"202","{F03F05C4-488E-48FF-A205-576BB83C92C4}"},
            {"203","{A52C7142-A005-4591-8FAD-F088E57CAFBE}"},
            {"204","{805A3E33-0446-4972-8CCC-A155CFA90E8E}"},
            {"205","{A0167A16-D591-4B43-A8A2-41CF0DE19B52}"},
            {"206","{A40B1E3A-2F24-4342-88D4-86DBA3D1E00C}"},
            {"207","{62C2D402-EADC-4FA2-B1A5-400B155D882D}"},
            {"208","{1EAD39F0-F479-48EB-A03B-56A8D5C6C94C}"},
            {"209","{68C11CE5-BB69-4E9E-84C3-74C8377A202E}"},
            {"210","{EFBF3420-419A-43E0-BF63-D439E145D261}"},
            {"211","{D2DFCA75-A565-4C14-BEA4-D98B8980E01C}"},
            {"212","{066C893A-3E8B-4BA9-95ED-430E14917249}"},
            {"213","{0DDD0E00-D612-48C1-B183-5FC28BC15CED}"},
            {"214","{56EC8EF1-896E-40AE-89E3-05D229F1AB1E}"},
            {"215","{1B8F5D9F-2C4A-4767-94AB-3AAD2D42C17A}"},
            {"216","{D52E3D9D-08A5-41D6-9D9B-E15841D81065}"},
            {"217","{5C201E08-C42E-48DB-ABBE-79C75F02AA53}"},
            {"218","{A688237A-1259-4942-9326-B0A9C751E854}"},
            {"219","{8612A317-215C-4037-ADEF-E0BF3774998F}"},
            {"220","{665A571E-61D0-43F3-94C1-A7A58DB24108}"},
            {"221","{5057A672-2DDC-4382-AD3C-83A63AAEC85E}"},
            {"222","{7F7A9967-862A-4F37-BA56-5447B813874F}"},
            {"223","{BE2B03B5-1F21-4A7A-9399-D8EB908E4E20}"},
            {"224","{D9793BF7-2B3F-47CE-A22B-25ABC3F66B11}"},
            {"225","{9AC8982E-1B90-4F2E-B7C9-B77A9C86FBE9}"},
            {"226","{776F8AE0-9A86-4DF4-B064-EF1BB429A484}"},
            {"227","{393DF140-BE91-4A43-852D-8DC714A94E6E}"},
            {"228","{AD2BEAA3-467B-44E9-9A5C-475A4B66FC48}"},
            {"229","{1EF11390-00BF-4EA4-ACA5-AC497B36BF81}"},
            {"230","{FF0C4AFD-9567-44B8-82C2-761A80F3E699}"},
            {"231","{0D865205-2F29-494E-AE36-183C30983FB1}"},
            {"232","{B46C528D-19A4-4575-B935-4BFB558C8048}"},
            {"233","{94C28598-E4E4-410B-9475-D9684ABE044D}"},
            {"234","{AB31AD1C-EFB0-4CAD-ADFA-51B851E8FA51}"},
            {"235","{3DCB2184-D626-47AF-85C0-267D6DCB4006}"},
            {"236","{B15FF309-80F6-4260-A9EF-2E02EBF8542D}"},
            {"237","{2857D427-7432-4A2F-B2CF-0CE95EC805BA}"},
            {"238","{2EC907E2-0753-463E-9F73-1A0BAE9D11C1}"},
            {"239","{C20F0F13-78DE-484F-9BC3-03A8A52BD5AC}"},
            {"240","{84794458-C13D-439D-AD3B-1CFE0CF66D2F}"},
            {"241","{F21F97B6-04FD-4A81-90EA-BECB6BF9BC65}"},
            {"242","{F4734FC9-6C89-48D6-BE00-6D589327B90B}"},
            {"243","{EE6DC496-BA81-40E0-975A-B2BF2D3E93A1}"},
            {"244","{5D3F5B5B-9719-40B4-B74B-03AF8811D49E}"},
            {"245","{824553EF-443F-48ED-A8E5-630D7D4B1F30}"},
            {"246","{68D49DD8-7101-41A9-982F-C18A37574E38}"}
        };

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    ; ;
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));

                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "btn"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "btn"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode")]
        protected void btnMigrate_Click(object sender, EventArgs e)
        {
            MultiLogger logger;
            DateTime startTime = DateTime.Now;

            using (logger = new MultiLogger())
            {
                logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ContactUs_Debug_{0}.txt", Languages.SelectedValue));
                logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ContactUs_Error_{0}.txt", Languages.SelectedValue));

                logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ContactUs_Meta_{0}.txt", Languages.SelectedValue));
                logger["meta"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("Teamsite Path	AddressID	ItemID	ItemPath");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;
                string scPath = txtSCPath.Text;

                FileService fs = new FileService();
                var fileList = fs.GetAllFiles(tsPath);
                //var fileList = new string[] { tsPath };

                foreach (string filePath in fileList)
                {
                    try
                    {
                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);
                        var xDoc = XDocument.Load(bufferStream);

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item"));

                        Language language = Language.Parse(Languages.SelectedValue);

                        Item parentItem = _master.GetItem(scPath, language);

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                        string[] array = filePath.Split('/');

                        string addressID = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;

                        string itemName = string.Empty;
                        string folderName = string.Empty;

                        if (ItemUtil.ProposeValidItemName(array[array.Length - 1]).Equals("index"))
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));
                        }
                        else
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 1]));
                        }

                        Item newItem = null;
                        try
                        {
                            List<string> eleList = new List<string>();
                            using (new Sitecore.SecurityModel.SecurityDisabler())
                            {
                                string title = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                    x.Attribute("name").Value.Equals("Address_Title")).FirstOrDefault().Value;

                                string details = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Address_details")).FirstOrDefault().Value;

                                string country = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Country")).FirstOrDefault().Value;

                                string countryID = string.Empty;
                                if(countries.ContainsKey(country))
                                {
                                    countryID =  countries[country];
                                }
                                else
                                {
                                    logger["error"].Log("Country not in list, Country ID = {0} in filePath = {1}", country, filePath);
                                }
                                
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    if(itemName.StartsWith("com-info"))
                                    {
                                        parentItem = _master.GetItem(comInfoPath, language);
                                    }
                                    else if (itemName.StartsWith("tm-sales"))
                                    {
                                        parentItem = _master.GetItem(tmSalesPath, language);
                                    }
                                    else if (itemName.StartsWith("tm-service"))
                                    {
                                        parentItem = _master.GetItem(tmServicePath, language);
                                    }
                                    else if (itemName.StartsWith("component-sales"))
                                    {
                                        parentItem = _master.GetItem(caSalesPath, language);
                                    }
                                    else if (itemName.StartsWith("component-service"))
                                    {
                                        parentItem = _master.GetItem(caServicePath, language);
                                    }
                                    else if (itemName.StartsWith("ce-sales"))
                                    {
                                        parentItem = _master.GetItem(ceSalesPath, language);
                                    }
                                    else if (itemName.StartsWith("ce-service"))
                                    {
                                        parentItem = _master.GetItem(ceServicePath, language);
                                    }
                                    else if (itemName.StartsWith("ad-sales"))
                                    {
                                        parentItem = _master.GetItem(adSalesPath, language);
                                    }
                                    else if (itemName.StartsWith("ad-service"))
                                    {
                                        parentItem = _master.GetItem(adServicePath, language);
                                    }

                                    newItem = _master.GetItem(parentItem.Paths.Path + "/" + itemName, language);
                                    if (newItem != null)
                                    {
                                        bool hasLangVersion = HasLanguageVersion(newItem, Languages.SelectedValue);
                                        if (!hasLangVersion)
                                        {
                                            newItem = newItem.Versions.AddVersion();
                                            versionCount++;
                                            logger["debug"].Log("Added new {1} version for Item:{0}", newItem.Paths.Path, Languages.SelectedItem.Text);
                                        }
                                        else
                                        {
                                            overwriteCount++;
                                            logger["debug"].Log("Overwriting the item: {0} in {1} language", newItem.Paths.Path, Languages.SelectedItem.Text);
                                        }
                                    }
                                    else
                                    {
                                        newItem = parentItem.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                                        itemCount++;
                                        logger["debug"].Log("New item added {0} in {1}", newItem.Paths.Path, newItem.Language.Name);
                                    }

                                    newItem.Editing.BeginEdit();
                                    logger["debug"].Log("Editing Item {0} started", itemName);

                                    logger["debug"].Log("Editing Title field.");
                                    newItem["Title"] = title;

                                    logger["debug"].Log("Editing Details field.");
                                    newItem["Details"] = details;

                                    logger["debug"].Log("Editing SelectCountry field.");
                                    newItem["SelectCountry"] = countryID;

                                    logger["debug"].Log("Editing End.");
                                    newItem.Editing.EndEdit();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger["error"].Log(DateTime.Now.ToString());
                            logger["error"].Log("Error occured while creating item {0} under {1}", itemName, parentItem.Paths.Path);
                            logger["error"].Log("{0}", filePath);
                            logger["error"].Log(ex.Message);
                            logger["error"].Log(ex.StackTrace);
                        }
                        logger["meta"].Log("{0}	{1}	{2}	{3}", filePath, addressID, newItem.ID.ToString(), newItem.Paths.Path);
                    }
                    catch (Exception ex)
                    {
                        logger["error"].Log("{0}", DateTime.Now.ToString());
                        logger["error"].Log("{0}", filePath);
                        logger["error"].Log("{0}", ex.Message);
                        logger["error"].Log("{0}", ex.StackTrace);
                    }
                }
                if (logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    logger["debug"].Log("-------------------------------------------------");
                    logger["debug"].Log("Time taken:{0}", duration);
                    logger["debug"].Log("New address items added : {0}", itemCount);
                    logger["debug"].Log("Overwritten address items : {0}", overwriteCount);
                    logger["debug"].Log("Total number of versions for address items : {0}", versionCount);
                    logger["debug"].Log("-------------------------------------------------");
                }
            }
        }
    }
}