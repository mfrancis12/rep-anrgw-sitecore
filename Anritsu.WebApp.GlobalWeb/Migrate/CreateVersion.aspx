﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateVersion.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.CreateVersion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Adding en-IN version</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Parent Item Path :</label>
            <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="AddVersion" runat="server" Text="Add en-IN Version" OnClick="AddVersion_Click" />
        </div>
    </form>
</body>
</html>
