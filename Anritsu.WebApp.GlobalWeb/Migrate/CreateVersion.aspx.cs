﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class CreateVersion : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        int versionCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddVersion_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "CreateVersion_Debug.txt"));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "CreateVersion_Error.txt"));

                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                _logger["debug"].Log("-------------------------------------------------------------------");

                try
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        string scPath = txtSCPath.Text;

                        Item parentItem = _master.GetItem(scPath, Language.Parse("en-IN"));

                        foreach (Item item in parentItem.Axes.GetDescendants())
                        {
                            bool has_en_IN_Version = HasLanguageVersion(item, "en-IN");
                            bool has_en_AU_Version = HasLanguageVersion(item, "en-AU");
                            bool has_en_Version = HasLanguageVersion(item, "en");
                            if (item != null && !has_en_IN_Version && has_en_Version && has_en_AU_Version)
                            {
                                item.Versions.AddVersion();
                                _logger["debug"].Log("Added new en-IN version for Item:{0}", item.Paths.Path);
                                versionCount++;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("Total number of versions for Download items:{0}", versionCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}