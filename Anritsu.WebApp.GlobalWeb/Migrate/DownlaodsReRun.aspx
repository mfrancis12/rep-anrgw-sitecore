﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownlaodsReRun.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.DownlaodsReRun" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Teamsite DCR Path :</label>
            <asp:TextBox ID="txtTSDCRPath" runat="server" Style="width: 400px"></asp:TextBox>
            <br />
            <br />
            <label>Sitecore Template:</label>
            <asp:DropDownList ID="SCTemplates" runat="server"></asp:DropDownList>
            <br />
            <br />
            <label>Sitecore Path :</label>
            <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox>
            <br />
            <br />
            <label>Language :</label>
            <asp:DropDownList ID="Languages" runat="server"></asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="ButtonDownloads" runat="server" Text="Import Downloads" OnClick="ButtonDownloads_Click" />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
