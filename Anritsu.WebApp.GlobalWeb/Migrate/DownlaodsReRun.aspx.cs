﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using Excel;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Sitecore.Buckets.Extensions;
using Sitecore.SecurityModel;
using Sitecore.Data.Fields;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ReRun"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Re"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Downlaods")]
    public partial class DownlaodsReRun : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");

        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        private string localPath = Sitecore.Configuration.Settings.GetSetting("ImageDownloadPath");
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");
        private string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");

        List<string> lstSelectedTSFields = new List<string>();
        List<string> lstSelectedSCFields = new List<string>();

        MultiLogger _logger;
        int itemCount = 0, versionCount = 0, overwriteCount = 0;
        int relatedProductsCount = 0, imageCount = 0, reusedImages = 0;

        private Dictionary<int, string> cultureID = new Dictionary<int, string>
        {
            {1,"en-US"},
            {2,"ja-JP"},
            {3,"en-GB"},
            {4,"en-AU"},
            {6,"zh-CN"},
            {7,"ko-KR"},
            {10,"zh-TW"},
            {12,"aa-GW"},
            {14,"ru-RU"},
            {15,"en-IN"}
        };

        private Dictionary<string, string> FileLinks = new Dictionary<string, string>
        {
            {"/Files/en-AU/","en-au/test-measurement/files/"},
            {"/Files/en-GB/","en-gb/test-measurement/files/"},
            {"/Files/en-US/","en-us/test-measurement/files/"},
            {"/Files/ja-JP/","ja-jp/test-measurement/files/"},
            {"/Files/ko-KR/","ko-kr/test-measurement/files/"},
            {"/Files/zh-CN/","zh-cn/test-measurement/files/"},
            {"/Files/zh-TW/","zh-tw/test-measurement/files/"}
        };

        private Dictionary<string, string> S3links = new Dictionary<string, string>
        {
            {"/epub/","/en-us/test-measurement/epub/"},
            {"/Files/","/common/test-measurement/files/"},
            {"/GWImages/images/","/images/legacy-images/images/"},
            {"/GWImages/apps/","/images/legacy-images/apps/"},
            {"/GWImages/controls/","/images/legacy-images/controls/"},
            {"/GWImages/css/","/images/legacy-images/css/"},
            {"/GWImages/custom/","/images/legacy-images/custom/"},
            {"/GWImages/js/","/images/legacy-images/js/"},
            {"/RefFiles/en/","/en-en/test-measurement/reffiles/"},
            {"/RefFiles/en-AU/","/en-au/test-measurement/reffiles/"},
            {"/RefFiles/en-CA/","/en-ca/test-measurement/reffiles/"},
            {"/RefFiles/en-GB/","/en-gb/test-measurement/reffiles/"},
            {"/RefFiles/en-US/","/en-us/test-measurement/reffiles/"},
            {"/RefFiles/es-MX/","/es-mx/test-measurement/reffiles/"},
            {"/RefFiles/ja-JP/","/ja-jp/test-measurement/reffiles/"},
            {"/RefFiles/ko-KR/","/ko-kr/test-measurement/reffiles/"},
            {"/RefFiles/pt-BR/","/pt-br/test-measurement/reffiles/"},
            {"/RefFiles/ru-RU/","/ru-ru/test-measurement/reffiles/"},
            {"/RefFiles/zh-CN/","/zh-cn/test-measurement/reffiles/"},
            {"/RefFiles/zh-HK/","/zh-hk/test-measurement/reffiles/"},
            {"/RefFiles/zh-TW/","/zh-tw/test-measurement/reffiles/"},
            {"/StreamFiles/custom/","/common/test-measurement/streamfiles/"},
            {"/StreamFiles/en-US/","/en-us/test-measurement/streamfiles/"}
        };

        private Dictionary<string, string> DownloadCategories = new Dictionary<string, string>
        {
            {"1", "{ACD1CA64-D1A8-4C3D-B9D4-1B2B6312F563}"},
            {"2", "{A3B18ED4-8EC6-4253-A0BC-93FFCD582A53}"},
            {"3", "{A3B80F6C-6482-472D-B847-955C401D09A6}"},
            {"4", "{8B3646BC-1EDA-4272-92C6-6A70CC1DA1AA}"},
            {"5", "{D48DF50B-68AF-4FF2-99B0-FE7AC445996D}"},
            {"6", "{0E3C8E23-CA9F-488A-B819-9E60FEF3816D}"},
            {"7", "{F1235750-CBD8-4CC3-9C5B-C6BAC2E18523}"},
            {"8", "{F708ECBA-3885-4E44-851E-ABBF5439DB3D}"},
            {"9", "{B7ECAA31-8D14-46DE-A22D-12E2879CC43A}"},
            {"10", "{959E6D67-998C-4D1C-8390-ADCF2512B513}"},
            {"12", "{FBAFF51A-D274-400E-BFF6-FFA23B8F1819}"},
            {"16", "{EBDAFA00-35E5-4651-BEF6-8E8165A73383}"},
            {"18", "{8EF86541-987E-4D6A-BD8E-CC078A5FDF18}"},
            {"19", "{7B2DA0EA-B095-4F18-94EA-5D6D5B740619}"},
            {"21", "{12B8720C-A18E-42E9-9CDF-6A61284D60A0}"},
            {"23", "{E73B1743-F0F0-4DEB-9997-AD421CF4E6E1}"},
            {"25", "{0B294504-CB48-46DE-902F-D7FD300F2641}"},
            {"27", ""},
            {"28", ""},
            {"29", ""},
            {"30", ""},
            {"31", ""},
            {"32", ""},
            {"34", ""},
            {"36", ""},
            {"37", "{AE60ACEA-0752-4514-ABF7-7374B8F41E87}"},
            {"38", "{ECB50C4E-1A39-410D-89AA-47B6D8B0D88F}"},
            {"39", "{00889488-8F01-41DE-954E-56CAEE37F1AD}"},
            {"40", "{F151F19C-ACA3-4BA5-A4C1-ABC3CEB0D01A}"},
            {"41", "{4C18BD02-D5C8-4E96-92B1-933E4B0396C0}"},
            {"42", ""},
            {"43", "{89AEF84E-1B5D-4976-97F2-9364B7714F5F}"},
            {"45", ""},
            {"46", "{258221B9-8323-4C0F-9B3A-82289CCEF04F}"},
            {"47", ""},
            {"48", "{6B6DB80C-6E10-4009-AD82-4942A76A0A9E}"},
            {"49", "{F174BF79-24EF-4930-BDCC-EE766C2EDF2A}"},
            {"50", ""},
            {"51", "{687A2D66-DAE4-4A8E-9BB4-D46F7CED23DD}"},
            {"52", "{ECEAB816-F0D5-4FF2-98DE-F71E7FAF9BBE}"},
            {"53", "{99AA2BD8-A768-48F0-BDA1-448182C8F5FA}"},
            {"54", "{F5D28BF8-0E79-4D2A-9C05-ECC8B1FA3072}"},
            {"56", ""},
            {"57", ""},
            {"58", ""},
            {"92", "{7C8600B6-BB6A-45D1-A241-DE6AFBF55466}"},
            {"93", ""},
            {"96", ""},
            {"97", ""}
        };

        private IEnumerable<TemplateItem> _sitecoreTemplates = null;

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));

                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public ID CreateDownloadFolders(string[] folderPath, string scPath, Language language)
        {
            Item folderItem = _master.GetItem(scPath, language);
            string folderItemPath = scPath;
            string path1 = folderPath[0];
            string path2 = folderPath[1];

            if (!path1.Contains('.') && !string.IsNullOrWhiteSpace(path1))
            {
                var folderTemplate = _master.Templates[new ID("{F547E7B1-B7B9-41AD-8958-4756BD7A5633}")];

                var parentItem = _master.GetItem(folderItemPath);
                folderItemPath = folderItemPath + "/" + GetItemName(path1);
                folderItem = _master.GetItem(folderItemPath, language);

                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    try
                    {
                        if (folderItem == null)
                        {
                            folderItem = parentItem.Add(GetItemName(path1), folderTemplate);
                        }
                        else if (folderItem.Versions.Count == 0)
                        {
                            folderItem = folderItem.Versions.AddVersion();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine(ex.ToString());
                    }
                }
            }

            if (!path2.Contains('.') && !string.IsNullOrWhiteSpace(path2))
            {
                var folderTemplate = _master.Templates[new ID("{E7B62AFB-B727-4257-B6EE-67FA097E8417}")];

                var parentItem = _master.GetItem(folderItemPath);
                folderItemPath = folderItemPath + "/" + GetItemName(path2);
                folderItem = _master.GetItem(folderItemPath, language);

                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    try
                    {
                        if (folderItem == null)
                        {
                            folderItem = parentItem.Add(GetItemName(path2), folderTemplate);
                        }
                        else if (folderItem.Versions.Count == 0)
                        {
                            folderItem = folderItem.Versions.AddVersion();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine(ex.ToString());
                    }
                }
            }
            return folderItem.ID;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1306:SetLocaleForDataTypes"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "Extension"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "isHDR"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sheetName")]
        private DataTable GetDataFromFile(string FilePath, string sheetName)
        {
            DataTable dt = new DataTable();
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileStream stream = File.Open(FilePath, FileMode.Open, FileAccess.Read);

                    //Reading from a OpenXml Excel file (2007 format; *.xlsx)                    
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                    //DataSet - Create column names from first row
                    excelReader.IsFirstRowAsColumnNames = true;

                    //DataSet - The result of each spreadsheet will be created in the result.Tables
                    DataSet result = excelReader.AsDataSet();

                    //6. Free resources (IExcelDataReader is IDisposable)
                    excelReader.Close();

                    dt = result.Tables[sheetName];
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToShortDateString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
            return dt;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace("---", "-").Replace("--", "-").TrimEnd('-').ToLowerInvariant();
        }

        private string GetS3Links(string link)
        {
            //string domain = "http://gwdata.cdn-anritsu.com";
            string newLink = string.Empty;

            bool hasFileLink = FileLinks.Keys.Any(x => link.Contains(x));
            if (hasFileLink)
            {
                string oldurl = FileLinks.Keys.FirstOrDefault(key => link.Contains(key));
                string newurl = FileLinks[oldurl];
                newLink = link.Replace(oldurl, newurl);
                //newLink = doamin + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));
            }
            else
            {
                bool hasLink = S3links.Keys.Any(x => link.Contains(x));
                if (hasLink)
                {
                    string oldurl = S3links.Keys.FirstOrDefault(key => link.Contains(key));
                    string newurl = S3links[oldurl];
                    newLink = link.Replace(oldurl, newurl);
                    //newLink = domain + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));

                }
                else
                {
                    newLink = link;
                }
            }
            return newLink;
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "SubFolder"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Sub")]
        public static Item CovertToBucketItem(Item SubFolderItem)
        {
            Sitecore.Buckets.Managers.BucketManager.CreateBucket(SubFolderItem);
            using (new Sitecore.Data.Items.EditContext(SubFolderItem, SecurityCheck.Disable))
            {
                if (!IsBucketItemCheck(SubFolderItem))
                {
                    IsBucketItemCheckBox(SubFolderItem).Checked = true;
                }
            }
            return SubFolderItem;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static bool IsBucketItemCheck(Item item)
        {
            return (((item != null) && (item.Fields[Sitecore.Buckets.Util.Constants.IsBucket] != null)) && item.Fields[Sitecore.Buckets.Util.Constants.IsBucket].Value.Equals("1"));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static CheckboxField IsBucketItemCheckBox(Item item)
        {
            return item.Fields[Sitecore.Buckets.Util.Constants.IsBucket];
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "path"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "isproduct"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "richtextimage")]
        protected MediaItem ImageFields(XElement element, string itemName, bool isproduct, string richtextimage, string altText)
        {
            string elementPath = string.Empty;
            if (element != null)
            {
                elementPath = element.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                elementPath = HttpUtility.UrlDecode(elementPath);
            }
            else if (!string.IsNullOrEmpty(richtextimage))
            {
                elementPath = HttpUtility.UrlDecode(richtextimage);
            }

            string tsFilePath = workAreaPath.Replace("/templatedata/", "") + elementPath;

            elementPath = isproduct ? "Images/Products/" + itemName + "/" + elementPath.Split('/').Last() : elementPath;

            if (string.IsNullOrWhiteSpace(elementPath))
            {
                return null;
            }
            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/" + path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            FileService imageFS = new FileService();
            var bytes = imageFS.GetFileContent(tsFilePath);
            var mediaPath = elementPath.Split('/');
            var mediaItemName = mediaPath.Last();
            if (bytes != null)
            {
                var stream = new MemoryStream(bytes);
                var bw = new BinaryWriter(new FileStream(localPath + mediaItemName, FileMode.Append, FileAccess.Write));
                bw.Write(bytes);
                bw.Close();
            }
            else
            {
                return null;
            }

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public MediaItem CreateMediaItem(string mediaUrl, string localMediaPath, string altText)
        {
            MediaItem mediaItem = null;
            MultiLogger logger;
            using (logger = new MultiLogger())
            {
                logger.Add("image", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog.txt");
                logger["image"].Log("{0}	Started uploading image", DateTime.Now.ToString());
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaPath = mediaUrl.Split('/');
                    var mediaFolderID = CreateMediaFolders(mediaPath);
                    var mediaFolder = master.GetItem(mediaFolderID);
                    var mediaItemName = mediaPath.Last();

                    FileInfo file = new FileInfo(localMediaPath + mediaItemName);
                    if (file.Exists)
                    {
                        var filename = GetItemName(mediaItemName.Split('.')[0]);
                        filename = filename.Replace(".", "_");
                        var options = new Sitecore.Resources.Media.MediaCreatorOptions();
                        options.Database = master;
                        options.Versioned = false;
                        options.Destination = mediaFolder.Paths.FullPath + "/" + filename;
                        options.IncludeExtensionInItemName = false;
                        options.AlternateText = string.IsNullOrEmpty(altText) ? mediaItemName : altText;
                        options.FileBased = Sitecore.Configuration.Settings.Media.UploadAsFiles;
                        var mediaCreator = new Sitecore.Resources.Media.MediaCreator();
                        try
                        {
                            mediaItem = mediaCreator.CreateFromFile(file.FullName, options);
                            logger["image"].Log("{0}	New image created : {1}", DateTime.Now.ToString(), mediaItem.Path);
                            logger["image"].Log("Image Size {0} bytes", mediaItem.Size.ToString());
                            imageCount++;
                        }
                        catch (Exception ex)
                        {
                            logger["image"].Log(DateTime.Now.ToString() + "	Error while creating image");
                            logger["image"].Log(ex.Message);
                            logger["image"].Log(ex.StackTrace);
                            //Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return mediaItem;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "path")]
        public ID CreateMediaFolders(string[] mediaPath)
        {
            Item mediaFolder = null;
            string mediaLibraryPath = "/sitecore/media library";
            foreach (string path in mediaPath)
            {
                if (!path.Contains('.') && !string.IsNullOrWhiteSpace(path))
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaFolderTemplate = master.Templates["System/Media/Media folder"];

                    var mediaLibraryItem = master.GetItem(mediaLibraryPath);
                    mediaLibraryPath = mediaLibraryPath + "/" + GetItemName(path);
                    mediaFolder = master.GetItem(mediaLibraryPath);

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (mediaFolder == null)
                        {
                            try
                            {
                                mediaFolder = mediaLibraryItem.Add(GetItemName(path), mediaFolderTemplate);
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return mediaFolder.ID;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)")]
        public string ProcessMediaInRichText(string htmlSource)
        {
            List<string> imgUrls = FetchLinksFromSource(htmlSource);
            string html = htmlSource;
            if (imgUrls.Count > 0)
            {
                imgUrls = imgUrls.Distinct().Where(x => !x.StartsWith("http")).ToList();

                foreach (string url in imgUrls)
                {
                    MediaItem image = ImageFields(null, string.Empty, false, url, string.Empty);
                    if (image == null)
                        continue;
                    string newUrl = "~/media/" + image.ID.ToShortID() + ".ashx";
                    html = html.Replace(url, newUrl);
                }
            }

            return html;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode")]
        protected void ButtonDownloads_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;

            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("notes", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Notes_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));

                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	DownloadID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    //var fileList = fs.GetAllFiles(tsPath);
                    var fileList = new string[] { tsPath };

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string langSelected = Languages.SelectedValue.Equals("en") ? "en-AU" : Languages.SelectedValue;
                            string langString = "/" + langSelected + "/";
                            string[] array = filePath.Split('/');
                            string folderPath = filePath.Split(new string[] { langString }, StringSplitOptions.None)[1];
                            string[] folders = folderPath.Split('/');
                            folders = folders.Take(folders.Count() - 1).ToArray();

                            string itemName = GetItemName(array[array.Length - 1]);
                            string scPath = txtSCPath.Text;

                            if (string.IsNullOrEmpty(scPath))
                            {
                                _logger["error"].Log("Sitecore path is empty or No item matched the path.");
                                continue;
                            }

                            IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                            var titleElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Title"));
                            string title = titleElement.FirstOrDefault() == null ? string.Empty : titleElement.FirstOrDefault().Value;

                            var descriptionElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Description"));
                            string description = descriptionElement.FirstOrDefault() == null ? string.Empty : descriptionElement.FirstOrDefault().Value;

                            var metaValues = nodes.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("meta_values"));

                            var metaDescElement = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("desc"));
                            string metaDesc = metaDescElement.FirstOrDefault() == null ? string.Empty : metaDescElement.FirstOrDefault().Value;

                            var metaKeywordsElement = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("keywords"));
                            string metaKeywords = metaKeywordsElement.FirstOrDefault() == null ? string.Empty : metaKeywordsElement.FirstOrDefault().Value;

                            var legacyIdElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("id"));
                            string legacyId = legacyIdElement.FirstOrDefault() == null ? string.Empty : legacyIdElement.FirstOrDefault().Value;

                            var pageUrlElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("PageURl"));
                            string legacyUrl = pageUrlElement.FirstOrDefault() == null ? string.Empty : pageUrlElement.FirstOrDefault().Value;

                            Language lang = Language.Parse(Languages.SelectedValue);
                            var parentID = CreateDownloadFolders(folders, scPath, lang);
                            var parentBucketItem = _master.GetItem(parentID, lang);
                            Item parentItem = parentBucketItem;
                            if (!parentBucketItem.IsABucket())
                            {
                                parentItem = CovertToBucketItem(parentBucketItem);
                            }

                            Item downloadItem = null;
                            bool isNewItem = false;
                            if (parentItem != null)
                            {
                                _logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    Language language = Language.Parse(Languages.SelectedValue);
                                    Item testItem = _master.SelectSingleItem("/sitecore/content/global/downloads//*[@@name='" + itemName + "']");
                                    if (testItem != null)
                                    {
                                        downloadItem = _master.GetItem(testItem.ID.ToString(), language);
                                    }

                                    if (downloadItem != null)
                                    {
                                        bool hasLangVersion = HasLanguageVersion(downloadItem, Languages.SelectedValue);
                                        if (!hasLangVersion)
                                        {
                                            downloadItem = downloadItem.Versions.AddVersion();
                                            _logger["debug"].Log("Added new {1} version for Item:{0}", downloadItem.Paths.Path, Languages.SelectedItem.Text);
                                            versionCount++;
                                            isNewItem = true;
                                        }
                                        else
                                        {
                                            overwriteCount++;
                                            _logger["debug"].Log("Overwriting the item: {0} in {1} language", downloadItem.Paths.Path, Languages.SelectedItem.Text);
                                        }
                                    }
                                    else
                                    {
                                        downloadItem = parentItem.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                                        _logger["debug"].Log("New item added {0} in {1}", downloadItem.Paths.Path, downloadItem.Language.Name);
                                        itemCount++;
                                        isNewItem = true;
                                    }

                                    if (downloadItem != null)
                                    {
                                        string master = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                            x.Attribute("name").Value.Equals("master")).FirstOrDefault().Value;

                                        string masterLanguage = cultureID.ContainsKey(Convert.ToInt32(master)) ? cultureID[Convert.ToInt32(master)].ToString() : string.Empty;

                                        IEnumerable<XElement> masterNodes = null;
                                        if (!string.IsNullOrEmpty(masterLanguage) && !masterLanguage.Equals(Languages.SelectedValue))
                                        {
                                            string masterFilePath = filePath.Replace(langSelected, masterLanguage);
                                            var masterfileByteContent = fs.GetFileContent(masterFilePath);
                                            if (masterfileByteContent != null)
                                            {
                                                var masterbufferStream = new MemoryStream(masterfileByteContent);
                                                var masterDoc = XDocument.Load(masterbufferStream);
                                                masterNodes = masterDoc.Root.Descendants();
                                            }
                                        }
                                        string relatedProductsValue = string.Empty;
                                        string downloadCategoryType = string.Empty;
                                        string replacementDownloadId = string.Empty;
                                        string fileTypeId = string.Empty;
                                        string fileType = string.Empty;
                                        string downloadLink = string.Empty;
                                        string size = string.Empty;
                                        string version = string.Empty;
                                        string isNewerVersionAvl = string.Empty;
                                        string releaseDate = string.Empty;
                                        string LogOnRequired = string.Empty;
                                        string RedirectToMyAnritsu = string.Empty;

                                        if (masterNodes != null && masterNodes.Count() > 0)
                                        {
                                            var relatedProductsValueElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Related_Products"));
                                            relatedProductsValue = relatedProductsValueElement.FirstOrDefault() == null ? string.Empty : relatedProductsValueElement.FirstOrDefault().Value;

                                            var downloadCategoryTypeElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("DownloadCategory"));
                                            downloadCategoryType = downloadCategoryTypeElement.FirstOrDefault() == null ? string.Empty : downloadCategoryTypeElement.FirstOrDefault().Value;

                                            var replacementDownloadIdElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("ReplacementDownloadId"));
                                            replacementDownloadId = replacementDownloadIdElement.FirstOrDefault() == null ? string.Empty : replacementDownloadIdElement.FirstOrDefault().Value;

                                            var fileTypeIdElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("FileTypeId"));
                                            fileTypeId = fileTypeIdElement.FirstOrDefault() == null ? string.Empty : fileTypeIdElement.FirstOrDefault().Value;

                                            var downloadLinkElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("FilePath"));
                                            downloadLink = downloadLinkElement.FirstOrDefault() == null ? string.Empty : downloadLinkElement.FirstOrDefault().Value;

                                            var sizeElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Size"));
                                            size = sizeElement.FirstOrDefault() == null ? string.Empty : sizeElement.FirstOrDefault().Value;

                                            var versionElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Version"));
                                            version = versionElement.FirstOrDefault() == null ? string.Empty : versionElement.FirstOrDefault().Value;

                                            var isNewerVersionAvlElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("isNewerVersionAvl"));
                                            isNewerVersionAvl = isNewerVersionAvlElement.FirstOrDefault() == null ? string.Empty : isNewerVersionAvlElement.FirstOrDefault().Value;

                                            var ReleaseDateElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("ReleaseDate"));
                                            releaseDate = ReleaseDateElement.FirstOrDefault() == null ? string.Empty : ReleaseDateElement.FirstOrDefault().Value;

                                            var LoginReqdElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("LoginReqd"));
                                            LogOnRequired = LoginReqdElement.FirstOrDefault() == null ? string.Empty : LoginReqdElement.FirstOrDefault().Value;

                                            var RedirectToMyAnritsuElement = masterNodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("ProductRegistrationReqd"));
                                            RedirectToMyAnritsu = RedirectToMyAnritsuElement.FirstOrDefault() == null ? string.Empty : RedirectToMyAnritsuElement.FirstOrDefault().Value;
                                        }
                                        else
                                        {
                                            var relatedProductsValueElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Related_Products"));
                                            relatedProductsValue = relatedProductsValueElement.FirstOrDefault() == null ? string.Empty : relatedProductsValueElement.FirstOrDefault().Value;

                                            var downloadCategoryTypeElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("DownloadCategory"));
                                            downloadCategoryType = downloadCategoryTypeElement.FirstOrDefault() == null ? string.Empty : downloadCategoryTypeElement.FirstOrDefault().Value;

                                            var replacementDownloadIdElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("ReplacementDownloadId"));
                                            replacementDownloadId = replacementDownloadIdElement.FirstOrDefault() == null ? string.Empty : replacementDownloadIdElement.FirstOrDefault().Value;

                                            var fileTypeIdElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("FileTypeId"));
                                            fileTypeId = fileTypeIdElement.FirstOrDefault() == null ? string.Empty : fileTypeIdElement.FirstOrDefault().Value;

                                            var downloadLinkElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("FilePath"));
                                            downloadLink = downloadLinkElement.FirstOrDefault() == null ? string.Empty : downloadLinkElement.FirstOrDefault().Value;

                                            var sizeElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Size"));
                                            size = sizeElement.FirstOrDefault() == null ? string.Empty : sizeElement.FirstOrDefault().Value;

                                            var versionElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Version"));
                                            version = versionElement.FirstOrDefault() == null ? string.Empty : versionElement.FirstOrDefault().Value;

                                            var isNewerVersionAvlElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("isNewerVersionAvl"));
                                            isNewerVersionAvl = isNewerVersionAvlElement.FirstOrDefault() == null ? string.Empty : isNewerVersionAvlElement.FirstOrDefault().Value;

                                            var ReleaseDateElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("ReleaseDate"));
                                            releaseDate = ReleaseDateElement.FirstOrDefault() == null ? string.Empty : ReleaseDateElement.FirstOrDefault().Value;

                                            var LoginReqdElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("LoginReqd"));
                                            LogOnRequired = LoginReqdElement.FirstOrDefault() == null ? string.Empty : LoginReqdElement.FirstOrDefault().Value;

                                            var RedirectToMyAnritsuElement = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("ProductRegistrationReqd"));
                                            RedirectToMyAnritsu = RedirectToMyAnritsuElement.FirstOrDefault() == null ? string.Empty : RedirectToMyAnritsuElement.FirstOrDefault().Value;
                                        }

                                        if (fileTypeId.Equals("1"))
                                        {
                                            fileType = "exe";
                                        }
                                        else if (fileTypeId.Equals("2"))
                                        {
                                            fileType = "pdf";
                                        }
                                        else if (fileTypeId.Equals("3"))
                                        {
                                            fileType = "jpeg";
                                        }
                                        else if (fileTypeId.Equals("4"))
                                        {
                                            fileType = "dll";
                                        }
                                        else if (fileTypeId.Equals("5"))
                                        {
                                            fileType = "doc";
                                        }
                                        else if (fileTypeId.Equals("6"))
                                        {
                                            fileType = "rar";
                                        }
                                        else if (fileTypeId.Equals("7"))
                                        {
                                            fileType = "zip";
                                        }
                                        else if (fileTypeId.Equals("8"))
                                        {
                                            fileType = "xls";
                                        }
                                        else if (fileTypeId.Equals("9"))
                                        {
                                            fileType = "txt";
                                        }
                                        else if (fileTypeId.Equals("10"))
                                        {
                                            fileType = "msi";
                                        }
                                        else if (fileTypeId.Equals("11"))
                                        {
                                            fileType = "html";
                                        }
                                        else if (fileTypeId.Equals("12"))
                                        {
                                            fileType = "htm";
                                        }

                                        string downloadCategory = DownloadCategories.ContainsKey(downloadCategoryType) ? DownloadCategories[downloadCategoryType] : string.Empty;
                                        if (!DownloadCategories.ContainsKey(downloadCategoryType) || string.IsNullOrEmpty(downloadCategory))
                                        {
                                            _logger["notes"].Log("------------------------------------------------------------------------");
                                            _logger["notes"].Log("DownloadCategoryType   FilePath   ItemID");
                                            _logger["notes"].Log("{0}   {1} {2}", downloadCategoryType, filePath, downloadItem.ID.ToString());
                                        }

                                        using (new Sitecore.SecurityModel.SecurityDisabler())
                                        {
                                            _logger["debug"].Log("Editing item started");
                                            downloadItem.Editing.BeginEdit();

                                            if (isNewItem)
                                            {
                                                _logger["debug"].Log("Editing Title Field");
                                                downloadItem["Title"] = Sitecore.StringUtil.RemoveTags(title);

                                                _logger["debug"].Log("Editing Description Field");
                                                downloadItem["Description"] = StringHtmlExtensions.RemoveAttributes(
                                                    ProcessMediaInRichText(description), new string[] { "class", "style", "width", "height" });
                                            }

                                            _logger["debug"].Log("Editing Version Field");
                                            downloadItem["Version"] = Sitecore.StringUtil.RemoveTags(version);

                                            if (isNewerVersionAvl.Equals("1"))
                                            {
                                                _logger["debug"].Log("Editing IsNewVersionAvailable Field");
                                                downloadItem["IsNewVersionAvailable "] = "1";
                                            }

                                            _logger["debug"].Log("Editing ReleaseDate Field");
                                            downloadItem["ReleaseDate"] = Sitecore.DateUtil.ToIsoDate(DateTime.Parse(releaseDate));

                                            _logger["debug"].Log("Editing Subcategory Field");
                                            downloadItem["Subcategory"] = downloadCategory;

                                            if (!string.IsNullOrEmpty(replacementDownloadId))
                                            {
                                                _logger["notes"].Log("--------------------------------------------------------------------------");
                                                _logger["notes"].Log("ReplacementDownloadID   FilePath  ItemID");
                                                _logger["notes"].Log("{0}   {1} {2}", replacementDownloadId, filePath, downloadItem.ID.ToString());
                                            }

                                            if (isNewItem)
                                            {
                                                _logger["debug"].Log("Editing Meta Description field");
                                                downloadItem["Metatags-Description"] = Sitecore.StringUtil.RemoveTags(metaDesc);

                                                _logger["debug"].Log("Editing Meta Keywords field");
                                                downloadItem["Metatags-Other keywords"] = Sitecore.StringUtil.RemoveTags(metaKeywords);

                                                _logger["debug"].Log("Editing MenuTitle Field");
                                                downloadItem["MenuTitle"] = Sitecore.StringUtil.RemoveTags(title);

                                                _logger["debug"].Log("Editing PageTitle Field");
                                                downloadItem["PageTitle"] = Sitecore.StringUtil.RemoveTags(title);

                                                _logger["debug"].Log("Editing MetaTitle Field");
                                                downloadItem["MetaTitle"] = Sitecore.StringUtil.RemoveTags(title);
                                            }

                                            _logger["debug"].Log("Editing FilePath Field");
                                            downloadItem["FilePath"] = GetS3Links(downloadLink);

                                            _logger["debug"].Log("Editing FileType Field");
                                            downloadItem["FileType"] = fileType;

                                            _logger["debug"].Log("Editing Size Field");
                                            downloadItem["Size"] = Sitecore.StringUtil.RemoveTags(size);

                                            if (RedirectToMyAnritsu.Equals("1") || downloadLink.Equals("https://www1.anritsu.co.jp/Download/MService/Login.asp"))
                                            {
                                                _logger["debug"].Log("Editing RedirectToMyAnritsu  Field");
                                                downloadItem["RedirectToMyAnritsu"] = "1";
                                                _logger["debug"].Log("Editing FilePath Field again");
                                                downloadItem["FilePath"] = "https://my.anritsu.com/myproduct/home";
                                            }

                                            if (LogOnRequired.Equals("1"))
                                            {
                                                _logger["debug"].Log("Editing LogOnRequired  Field");
                                                downloadItem["LogOnRequired"] = "1";
                                            }

                                            downloadItem.Editing.EndEdit();
                                            _logger["debug"].Log("Editing End");
                                        }

                                        #region code for Related Products

                                        DataTable prodRegional = GetDataFromFile(path, "ProductRegional");

                                        if (prodRegional != null && prodRegional.Rows.Count > 0)
                                        {
                                            List<string> productItemList = new List<string>();
                                            string[] relatedProducts = relatedProductsValue.Split(',').Select(x => x.Trim()).ToArray();

                                            if (relatedProducts != null && relatedProducts.Count() > 0)
                                            {
                                                foreach (string productID in relatedProducts)
                                                {
                                                    string productItemId = string.Empty;
                                                    var productRows = (from DataRow row in prodRegional.Rows
                                                                       where (row["ProductID"].ToString().Equals(productID))
                                                                       select row);
                                                    if (productRows != null && productRows.Count() > 0)
                                                    {
                                                        productItemId = productRows.Select(x => x["ItemID"]).FirstOrDefault().ToString();
                                                    }
                                                    if (string.IsNullOrEmpty(productItemId))
                                                    {
                                                        continue;
                                                    }
                                                    Item productItem = _master.GetItem(productItemId, lang);
                                                    if (productItem != null)
                                                    {
                                                        productItemList.Add(productItem.ID.ToString());
                                                        relatedProductsCount++;
                                                    }
                                                }
                                            }
                                            using (new Sitecore.SecurityModel.SecurityDisabler())
                                            {
                                                _logger["debug"].Log("Editing item started");
                                                downloadItem.Editing.BeginEdit();
                                                _logger["debug"].Log("Editing Related Products Field");
                                                downloadItem["RelatedProducts"] = string.Join("|", productItemList);
                                                downloadItem.Editing.EndEdit();
                                                _logger["debug"].Log("Editing End");
                                            }
                                        }
                                        #endregion

                                        _logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, legacyId, downloadItem.ID.ToString(), downloadItem.Paths.Path, legacyUrl);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger["error"].Log("{0}", DateTime.Now.ToString());
                            _logger["error"].Log("{0}", filePath);
                            _logger["error"].Log("{0}", ex.Message);
                            _logger["error"].Log("{0}", ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("New Download items added:{0}", itemCount);
                    _logger["debug"].Log("Number of related products items count:{0}", relatedProductsCount);
                    _logger["debug"].Log("Overwritten Download items:{0}", overwriteCount);
                    _logger["debug"].Log("Total number of versions for Download items:{0}", versionCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }
    }
}

