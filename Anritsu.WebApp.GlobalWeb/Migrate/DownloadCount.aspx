﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadCount.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.DownloadCount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Sitecore Path :</label>
            <asp:TextBox ID="txtSCPath" runat="server" Style="width: 400px"></asp:TextBox>
            <br />
            <br />
            <label>Download Subcategory :</label>
            <asp:TextBox ID="txtCategory" runat="server" Style="width: 400px"></asp:TextBox>
            <br />
            <br />
            <label>Language :</label>
                <asp:DropDownList ID="Languages" runat="server"></asp:DropDownList><br />
                <br />
                <br />
            <asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click" />
        </div>
    </form>
</body>
</html>
