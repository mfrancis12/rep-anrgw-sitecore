﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class DownloadCount : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "btn"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "btn")]
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var itemPath = txtSCPath.Text;
            var downloadSubCategory = txtCategory.Text;
            Language selectedLanguage = LanguageManager.GetLanguage(Languages.SelectedValue);

            List<Item> totalDownloads = Sitecore.Context.Database.GetItem(itemPath, selectedLanguage).Children.ToList();
            if (totalDownloads.Count > 0)
            {
                Response.Write("Total Downloads:" + totalDownloads.Count);
                foreach (Item item in totalDownloads)
                {
                    if ((item != null) && (!string.IsNullOrEmpty(item["Subcategory"]) &&
                        Sitecore.Context.Database.GetItem(item["Subcategory"]) != null &&
                        Sitecore.Context.Database.GetItem(item["Subcategory"])["key"].Equals(downloadSubCategory)))
                    {
                        Response.Write("Matching Downloads:");
                        Response.Write("=======================");
                        Response.Write(item.ID + "    " + item.Language.Name + "    " + item["Subcategory"]);
                        Response.Write("=======================");
                    }
                }
            }
        }
    }
}