﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadLinks.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.DownloadLinks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="LabelIntro" runat="server" Text="Update file path from teamsite to Amazon S3 path"></asp:Label>
            <asp:Button ID="ButtonSubmit" runat="server" Text="Button" OnClick="ButtonSubmit_OnClick" />
            <br />
            <asp:Label ID="LabelSuccess" runat="server" Text="File Path update was successfull" Visible="false"></asp:Label>
        </div>
        <script runat="server">
            
        
        </script>
    </form>
</body>
</html>
