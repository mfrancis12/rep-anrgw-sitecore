﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using ContentMigration.Services;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class DownloadLinks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSubmit_OnClick(object sender, EventArgs e)
        {          
            var logger = new MultiLogger();
            using (logger)
            {
                logger.Add("error", Settings.GetSetting("LogPath") + "DownloadFilePath.txt");
                var downloadquery = string.Format("fast:/sitecore/content/Global/downloads//*[@@templateid='{0}']", TemplateIds.Downloads);

                var db = Factory.GetDatabase("master");

                var downloadList = db.SelectItems(downloadquery).ToList();

                logger["error"].Log("Total Download File Count :{0}", downloadList.Count);
                logger["error"].Log("{0}", downloadList.Count);
                logger["error"].Log("========================================");

                foreach (var download in downloadList)
                {
                    foreach (var language in LanguageManager.GetLanguages(db))
                    {
                        var downloadItem = db.GetItem(download.ID.ToString(), language);

                        if (downloadItem != null && (downloadItem.Versions.Count > 0))
                        {
                            try
                            {
                                if (downloadItem.Fields["FilePath"] != null &&
                                    !string.IsNullOrEmpty(downloadItem.Fields["FilePath"].Value))
                                {
                                    logger["error"].Log("{0}    {1} {2} {3}", downloadItem.ID.ToString(), downloadItem["FilePath"], language.Name, downloadItem.Name);
                                }

                                else
                                {
                                    logger["error"].Log("Download FilePath field is Null or the field value is empty for download or ll-cc of file path doesn't match item language:");
                                    logger["error"].Log("{0}    {1} {2} {3}", downloadItem.ID.ToString(), downloadItem["FilePath"], language.Name, downloadItem.Name);
                                }
                            }
                            catch (Exception ex)
                            {
                                logger["error"].Log("**********************************************");
                                logger["error"].Log("Error : {0}", ex.Message);
                                logger["error"].Log("**********************************************");
                            }
                        }
                    }
                }
                LabelSuccess.Visible = true;
                logger["error"].Log("Successfully Done!");
            }
        }
    }
}