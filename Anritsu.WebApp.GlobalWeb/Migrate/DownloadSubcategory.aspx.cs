﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class DownloadSubcategory : System.Web.UI.Page
    {
        int itemCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Equals(System.String,System.StringComparison)")]
        protected void ButtonSubmit_OnClick(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                try
                {
                    _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "DownloadSubCategory_Debug.txt"));
                    _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "DownloadSubCategory_Error.txt"));

                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                    _logger["debug"].Log("-------------------------------------------------------------------");

                    var downloadquery = string.Format("fast:/sitecore/content/Global/downloads//*[@@templateid='{0}']", TemplateIds.Downloads);
                    var db = Factory.GetDatabase("master");
                    var downloadList = db.SelectItems(downloadquery).ToList();
                    itemCount = downloadList.Count;

                    foreach (var download in downloadList)
                    {
                        foreach (var language in LanguageManager.GetLanguages(db))
                        {
                            var downloadItem = db.GetItem(download.ID.ToString(), language);

                            if (downloadItem != null && (downloadItem.Versions.Count > 0))
                            {
                                try
                                {
                                    string subcategoryItemID = downloadItem["Subcategory"];
                                    if (string.IsNullOrEmpty(subcategoryItemID))
                                    {
                                        Item subcategoryItem = db.GetItem(subcategoryItemID);
                                        if (subcategoryItem != null && !string.IsNullOrEmpty(subcategoryItem["Key"]))
                                        {

                                            Item parentItem = downloadItem.Axes
                              .GetAncestors(
                                  )
                              .FirstOrDefault(temp => temp.TemplateID.ToString()
                                          .Equals("{E7B62AFB-B727-4257-B6EE-67FA097E8417}",
                                              StringComparison.InvariantCultureIgnoreCase));

                                            if (!subcategoryItem["Key"].Equals(parentItem["Key"]))
                                            {
                                                _logger["debug"].Log("{0}	{1}	{2}	{3}", downloadItem.ID.ToString(), downloadItem.Language.ToString(), subcategoryItem["Key"], parentItem["Key"]);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                                    _logger["error"].Log("{0}", downloadItem.ID.ToString());
                                    _logger["error"].Log("{0}", ex.Message);
                                    _logger["error"].Log("{0}", ex.StackTrace);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("Number of items :{0}", itemCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }
    }
}