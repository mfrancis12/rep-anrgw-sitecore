﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EcoProducts.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.EcoProducts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <label>Sitecore Path :</label>
                <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox><br />
                <br />
                <br />
        <label>Sitecore Template:</label>
            <asp:DropDownList ID="SCTemplates" runat="server"></asp:DropDownList>
            <br />
            <br />
        <label>Language :</label>
            <asp:DropDownList ID="Languages" runat="server"></asp:DropDownList><br />
            <br />
            <br />
        <asp:Button ID="ecoProducts" runat="server" Text="Eco Products" OnClick="ecoProducts_Click" />
    </div>
    </form>
</body>
</html>
