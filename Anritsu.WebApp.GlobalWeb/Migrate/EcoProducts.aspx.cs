﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Data;
using System.IO;
using System.Xml.Linq;
using Sitecore.Data.Managers;
using System.Text;
using Sitecore.Sites;
using System.Text.RegularExpressions;
using System.Net;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class EcoProducts : System.Web.UI.Page
    {
        MultiLogger _logger;
        DateTime startTime = DateTime.Now;
        private string teamsitePath = "/default/main/www/WORKAREA/contentwa/templatedata/About-Anritsu/StaticSingleBoxDetailPage/data/{0}/Environment/{1}";
        private string[] dcrNames = new string[] { 
            "MT1100A"//,"MT1000A","MT8870A","MG3710A","MD8475A","MS9740A","MT8820C","MP2100A","MS2830A","MU909011A","MS2690A","MP1800A","MT1810A"
        };
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        private string localPath = Sitecore.Configuration.Settings.GetSetting("ImageDownloadPath");
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");

        int itemCount = 0, versionCount = 0, overwriteCount = 0;
        int imageCount = 0, reusedImages = 0;

        private IEnumerable<TemplateItem> _sitecoreTemplates = null;
        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));

                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "eco")]
        protected void ecoProducts_Click(object sender, EventArgs e)
        {
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	ItemID	ItemPath	LegacyUrl");

                FileService fs = new FileService();

                foreach (string dcr in dcrNames)
                {
                    try
                    {
                        string filePath = string.Format(teamsitePath, Languages.SelectedValue, dcr);
                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);

                        var xDoc = XDocument.Load(bufferStream);
                        var nodes = xDoc.Root.Descendants();

                        string legacyUrl = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;
                        string metaKeywords = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("desc")).FirstOrDefault().Value;
                        string metadescription = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("keywords")).FirstOrDefault().Value;
                        string title = string.Empty;
                        string content = string.Empty;
                        XElement bodyNode = nodes.FirstOrDefault(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("body"));
                        if (bodyNode != null && bodyNode.DescendantNodes().Count() > 0)
                        {                            
                            foreach (XElement valueNode in bodyNode.Elements("value"))
                            {
                                XElement boxDetailNode = valueNode.Elements("item").FirstOrDefault(x => x.Attribute("name").Value.Equals("BoxDetail"));
                                if(boxDetailNode != null && boxDetailNode.DescendantNodes().Count() > 0)
                                {
                                    title = boxDetailNode.Descendants().Where(x=>x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Title")).FirstOrDefault().Value;
                                    XElement boxContentNode = boxDetailNode.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("box_content")).FirstOrDefault();
                                    if (boxContentNode != null && boxContentNode.DescendantNodes().Count() > 0)
                                    {
                                        XElement contentNode = boxContentNode.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Content")).FirstOrDefault(); ;
                                        if(contentNode!=null && !string.IsNullOrEmpty(contentNode.Value))
                                        {
                                            content = contentNode.Value;
                                        }
                                    }
                                }
                            }
                        }
                        string h1Value = Languages.SelectedValue.Equals("ja-JP") ? "環境への取り組み" : "Environment";
                        StringBuilder finalContent = new StringBuilder();
                        finalContent.AppendLine(string.Format("<h1>{0}</h1>",h1Value));
                        finalContent.AppendLine(string.Format("<h3>{0}</h3>", title));
                        string newContent = StringHtmlExtensions.RemoveAttributes(ProcessMediaInRichText(content), new string[] { "class", "style", "height", "width" }); 
                        finalContent.AppendLine(WrapTableTag(newContent));

                        string scPath = txtSCPath.Text;
                        Language language = Language.Parse(Languages.SelectedValue);
                        Item parentItem = _master.GetItem(scPath, language);

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            _logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        string itemName = ItemUtil.ProposeValidItemName(dcr);
                        Item ecoItem = _master.GetItem(parentItem.Paths.Path + "/" + itemName, language);
                        
                        using (new Sitecore.SecurityModel.SecurityDisabler())
                        {
                            if (ecoItem != null)
                            {
                                bool hasLangVersion = (ecoItem.Versions.Count > 0);//HasLanguageVersion(newItem, Languages.SelectedValue);
                                if (!hasLangVersion)
                                {
                                    ecoItem = ecoItem.Versions.AddVersion();
                                    _logger["debug"].Log("Added new {1} version for Item:{0}", ecoItem.Paths.Path, Languages.SelectedItem.Text);
                                    versionCount++;
                                }
                                else
                                {
                                    overwriteCount++;
                                    _logger["debug"].Log("Overwriting the item: {0} in {1} language", ecoItem.Paths.Path, Languages.SelectedItem.Text);
                                }
                            }
                            else
                            {
                                ecoItem = parentItem.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                                _logger["debug"].Log("New item added {0} in {1}", parentItem.Paths.Path, parentItem.Language.Name);
                                itemCount++;
                            }

                            ecoItem.Editing.BeginEdit();
                            _logger["debug"].Log("Editing item {0}", itemName);

                            _logger["debug"].Log("Editing Content Field ");
                            ecoItem["content"] = finalContent.ToString();

                            _logger["debug"].Log("Editing Meta Description field");
                            ecoItem["Metatags-Description"] = metadescription;

                            _logger["debug"].Log("Editing Meta Keywords field");
                            ecoItem["Metatags-Other keywords"] = metaKeywords;

                            _logger["debug"].Log("Editing item ended");
                            ecoItem.Editing.EndEdit();
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger["error"].Log("Error in dcr {0}", dcr);
                        _logger["error"].Log("{0}", DateTime.Now.ToString());
                        _logger["error"].Log(ex.Message);
                        _logger["error"].Log(ex.StackTrace);
                    }
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("New items added : {0}", itemCount);
                    _logger["debug"].Log("Overwritten items : {0}", overwriteCount);
                    _logger["debug"].Log("Total number of versions for items : {0}", versionCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)")]
        public string ProcessMediaInRichText(string htmlSource)
        {
            List<string> imgUrls = FetchLinksFromSource(htmlSource);
            string html = htmlSource;
            if (imgUrls.Count > 0)
            {
                imgUrls = imgUrls.Distinct().ToList();
                //imgUrls = imgUrls.Distinct().Where(x => !x.StartsWith("http")).ToList();

                foreach (string url in imgUrls)
                {
                    MediaItem image = null;
                    if (url.StartsWith("http"))
                    {
                        image = GetImageItem(url, string.Empty);
                    }
                    else
                    {
                        image = ImageFields(null, string.Empty, false, url, string.Empty);
                    }
                    if (image == null)
                        continue;
                    string newUrl = "~/media/" + image.ID.ToShortID() + ".ashx";
                    html = html.Replace(url, newUrl);
                }
            }
            return html;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "isproduct"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "richtextimage")]
        protected MediaItem ImageFields(XElement element, string itemName, bool isproduct, string richtextimage, string altText)
        {
            string elementPath = string.Empty;
            if (element != null)
            {
                elementPath = element.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                elementPath = HttpUtility.UrlDecode(elementPath);
            }
            else if (!string.IsNullOrEmpty(richtextimage))
            {
                elementPath = HttpUtility.UrlDecode(richtextimage);
            }

            string tsFilePath = workAreaPath.Replace("/templatedata/", "") + elementPath;

            elementPath = isproduct ? "Images/Products/" + itemName + "/" + elementPath.Split('/').Last() : elementPath;

            if (string.IsNullOrWhiteSpace(elementPath))
            {
                return null;
            }
            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/" + path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            FileService imageFS = new FileService();
            var bytes = imageFS.GetFileContent(tsFilePath);
            var mediaPath = elementPath.Split('/');
            var mediaItemName = mediaPath.Last();
            if (bytes != null)
            {
                var stream = new MemoryStream(bytes);
                var bw = new BinaryWriter(new FileStream(localPath + mediaItemName, FileMode.Append, FileAccess.Write));
                bw.Write(bytes);
                bw.Close();
            }
            else
            {
                return null;
            }

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public MediaItem CreateMediaItem(string mediaUrl, string localMediaPath, string altText)
        {
            MediaItem mediaItem = null;
            MultiLogger logger;
            using (logger = new MultiLogger())
            {
                logger.Add("image", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog.txt");
                logger["image"].Log("{0}	Started uploading image", DateTime.Now.ToString());
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaPath = mediaUrl.Split('/');
                    var mediaFolderID = CreateMediaFolders(mediaPath);
                    var mediaFolder = master.GetItem(mediaFolderID);
                    var mediaItemName = mediaPath.Last();

                    FileInfo file = new FileInfo(localMediaPath + mediaItemName);
                    if (file.Exists)
                    {
                        var filename = GetItemName(mediaItemName.Split('.')[0]);
                        filename = filename.Replace(".", "_");
                        var options = new Sitecore.Resources.Media.MediaCreatorOptions();
                        options.Database = master;
                        options.Versioned = false;
                        options.Destination = mediaFolder.Paths.FullPath + "/" + filename;
                        options.IncludeExtensionInItemName = false;
                        options.AlternateText = string.IsNullOrEmpty(altText) ? mediaItemName : altText;
                        options.FileBased = Sitecore.Configuration.Settings.Media.UploadAsFiles;
                        var mediaCreator = new Sitecore.Resources.Media.MediaCreator();
                        try
                        {
                            mediaItem = mediaCreator.CreateFromFile(file.FullName, options);
                            logger["image"].Log("{0}	New image created : {1}", DateTime.Now.ToString(), mediaItem.Path);
                            logger["image"].Log("Image Size {0} bytes", mediaItem.Size.ToString());
                            imageCount++;
                        }
                        catch (Exception ex)
                        {
                            logger["image"].Log(DateTime.Now.ToString() + "	Error while creating image");
                            logger["image"].Log(ex.Message);
                            logger["image"].Log(ex.StackTrace);
                            //Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return mediaItem;
        }

        public ID CreateMediaFolders(string[] mediaPath)
        {
            Item mediaFolder = null;
            string mediaLibraryPath = "/sitecore/media library";
            foreach (string path in mediaPath)
            {
                if (!path.Contains('.') && !string.IsNullOrWhiteSpace(path))
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaFolderTemplate = master.Templates["System/Media/Media folder"];

                    var mediaLibraryItem = master.GetItem(mediaLibraryPath);
                    mediaLibraryPath = mediaLibraryPath + "/" + GetItemName(path);
                    mediaFolder = master.GetItem(mediaLibraryPath);

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (mediaFolder == null)
                        {
                            try
                            {
                                mediaFolder = mediaLibraryItem.Add(GetItemName(path), mediaFolderTemplate);
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return mediaFolder.ID;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        protected MediaItem GetImageItem(string url, string altText)
        {
            var mediaPath = url.Split('/');
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(url, localPath + mediaPath.Last());
                }
                catch (Exception ex)
                {
                }
            }

            string host = new Uri(url).GetLeftPart(UriPartial.Authority);
            string elementPath = url.Replace(host, "");//TO-DO : remove host from the file

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public string WrapTableTag(string html)
        {
            html = html.Replace("<table", "<div class=\"detail-table\"><table");
            html = html.Replace("</table>", "</table></div>");
            return html;
        }
    }
}