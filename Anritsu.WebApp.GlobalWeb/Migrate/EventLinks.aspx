﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventLinks.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.EventLinks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Event Links</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>File Path :</label>
            <asp:TextBox ID="txtFilePath" runat="server" Style="width: 400px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="GetLinks" runat="server" Text="Get Links from Teamsite" OnClick="GetLinks_Click" />
            <br />
            <br />
            <asp:Button ID="FixLinks" runat="server" Text="Fix Links in Sitecore" OnClick="FixLinks_Click" />
            <br />
            <br />
            <asp:Button ID="FixTitles" runat="server" Text="Fix Titles" OnClick="FixTitles_Click" />
        </div>
    </form>
</body>
</html>
