﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using ContentMigration.Services;
using Sitecore.Configuration;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class EventLinks : System.Web.UI.Page
    {
        private Dictionary<string, string> FileLinks = new Dictionary<string, string>
        {
            {"/Files/en-AU/","/en-au/test-measurement/files/"},
            {"/Files/en-GB/","/en-gb/test-measurement/files/"},
            {"/Files/en-US/","/en-us/test-measurement/files/"},
            {"/Files/ja-JP/","/ja-jp/test-measurement/files/"},
            {"/Files/ko-KR/","/ko-kr/test-measurement/files/"},
            {"/Files/zh-CN/","/zh-cn/test-measurement/files/"},
            {"/Files/zh-TW/","/zh-tw/test-measurement/files/"}
        };

        private Dictionary<string, string> S3links = new Dictionary<string, string>
        {
            {"/epub/","/en-us/test-measurement/epub/"},
            {"/Files/","/common/test-measurement/files/"},
            {"/GWImages/images/","/images/legacy-images/images/"},
            {"/GWImages/apps/","/images/legacy-images/apps/"},
            {"/GWImages/controls/","/images/legacy-images/controls/"},
            {"/GWImages/css/","/images/legacy-images/css/"},
            {"/GWImages/custom/","/images/legacy-images/custom/"},
            {"/GWImages/js/","/images/legacy-images/js/"},
            {"/RefFiles/en/","/en-en/test-measurement/reffiles/"},
            {"/RefFiles/en-AU/","/en-au/test-measurement/reffiles/"},
            {"/RefFiles/en-CA/","/en-ca/test-measurement/reffiles/"},
            {"/RefFiles/en-GB/","/en-gb/test-measurement/reffiles/"},
            {"/RefFiles/en-US/","/en-us/test-measurement/reffiles/"},
            {"/RefFiles/es-MX/","/es-mx/test-measurement/reffiles/"},
            {"/RefFiles/ja-JP/","/ja-jp/test-measurement/reffiles/"},
            {"/RefFiles/ko-KR/","/ko-kr/test-measurement/reffiles/"},
            {"/RefFiles/pt-BR/","/pt-br/test-measurement/reffiles/"},
            {"/RefFiles/ru-RU/","/ru-ru/test-measurement/reffiles/"},
            {"/RefFiles/zh-CN/","/zh-cn/test-measurement/reffiles/"},
            {"/RefFiles/zh-HK/","/zh-hk/test-measurement/reffiles/"},
            {"/RefFiles/zh-TW/","/zh-tw/test-measurement/reffiles/"},
            {"/StreamFiles/custom/","/common/test-measurement/streamfiles/"},
            {"/StreamFiles/en-US/","/en-us/test-measurement/streamfiles/"}
        };

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private string GetS3Links(string link)
        {
            //string domain = "http://gwdata.cdn-anritsu.com";
            string domain = "http://dl-dev.cdn-anritsu.com";
            string newLink = string.Empty;

            bool hasFileLink = FileLinks.Keys.Any(x => link.Contains(x));
            if (hasFileLink)
            {
                string oldurl = FileLinks.Keys.FirstOrDefault(key => link.Contains(key));
                string newurl = FileLinks[oldurl];
                newLink = link.Replace(oldurl, newurl);
                newLink = domain + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));
            }
            else
            {
                bool hasLink = S3links.Keys.Any(x => link.Contains(x));
                if (hasLink)
                {
                    string oldurl = S3links.Keys.FirstOrDefault(key => link.Contains(key));
                    string newurl = S3links[oldurl];
                    newLink = link.Replace(oldurl, newurl);
                    newLink = domain + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));
                }
                else
                {
                    newLink = link;
                }
            }

            if (newLink.Contains("http://downloadfiles.anritsu.com"))
                newLink = newLink.Replace("/http://downloadfiles.anritsu.com", "");

            else if (newLink.Contains("http://downloadfile.anritsu.com"))
                newLink = newLink.Replace("/http://downloadfile.anritsu.com", "");

            return newLink;
        }

        protected void GetLinks_Click(object sender, EventArgs e)
        {
            MultiLogger _logger;
            string tsPath = txtFilePath.Text;
            DateTime startTime = DateTime.Now;
            int count = 0;
            using (_logger = new MultiLogger())
            {
                _logger.Add("links", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "EventsLinks.txt"));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Error.txt"));
                _logger["links"].Log("Teamsite Path Additional Link     Register Link");
                _logger["links"].Log("---------------------------------------------------");
                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);
                    //var fileList = new string[] { tsPath };

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);
                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            var additionalLinkElement = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                       x.Attribute("name").Value.Equals("additional_link"));

                            string additionalLink = additionalLinkElement.FirstOrDefault() == null ?
                                                 string.Empty : additionalLinkElement.FirstOrDefault().Value;

                            var RegisterOrLaunchLinkElement = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("register_link"));

                            string RegisterOrLaunchLink = RegisterOrLaunchLinkElement.FirstOrDefault() == null ?
                                                        string.Empty : RegisterOrLaunchLinkElement.FirstOrDefault().Value;

                            _logger["links"].Log("{0}	{1}	{2}", filePath, additionalLink, RegisterOrLaunchLink);
                        }
                        catch (Exception ex)
                        {
                            _logger["error"].Log("{0}", DateTime.Now.ToString());
                            _logger["error"].Log("{0}", filePath);
                            _logger["error"].Log("{0}", ex.Message);
                            _logger["error"].Log("{0}", ex.StackTrace);
                        }
                    }
                }

                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }

                TimeSpan ts = DateTime.Now - startTime;
                string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                _logger["links"].Log("-------------------------------------------------");
                _logger["links"].Log("Time taken:{0}", duration);
                _logger["links"].Log("Numbe of links:{0}", count);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)")]
        protected void FixLinks_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            int itemCount = 0;
            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                try
                {
                    _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "EventsFix_Debug.txt"));
                    _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "EventsFix_Error.txt"));
                    _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "EventsFix_Meta.txt"));

                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                    _logger["debug"].Log("-------------------------------------------------------------------");

                    var db = Factory.GetDatabase("master");
                    Item parentItem = db.GetItem(txtFilePath.Text);

                    foreach (Item items in parentItem.Axes.GetDescendants())
                    {
                        foreach (var language in LanguageManager.GetLanguages(db))
                        {
                            Item item = db.GetItem(items.ID.ToString(), language);

                            if (item != null && (item.Versions.Count > 0) &&
                                (item.Template.ID.ToString().Equals("{4BE1B2CA-2BD0-43C0-B6B5-6892CB9E5F79}")
                                || item.Template.ID.ToString().Equals("{508AE037-8CB8-4DA2-B425-0095E738395E}")))
                            {
                                try
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        string title = item["Title"];
                                        Sitecore.Data.Fields.LinkField registerLinkField = item.Fields["RegisterOrLaunchLink"];

                                        _logger["debug"].Log("Editing item {0} started in {1}", item.Name, item.Language.ToString());
                                        item.Editing.BeginEdit();

                                        if (registerLinkField != null)
                                        {
                                            _logger["debug"].Log("Editing RegisterOrLaunchLink Field");
                                            _logger["meta"].Log("Old Value : {0}", registerLinkField.Url);

                                            if (registerLinkField.Url.Contains("http"))
                                            {
                                                registerLinkField.Url = GetS3Links(registerLinkField.Url);
                                                _logger["meta"].Log("Updated Value : {0}", registerLinkField.Url);
                                                registerLinkField.LinkType = "external";
                                            }
                                            else if (FileLinks.Keys.Any(x => registerLinkField.Url.ToLower().Contains(x.ToLower()))
                                                        || S3links.Keys.Any(x => registerLinkField.Url.ToLower().Contains(x.ToLower())))
                                            {
                                                registerLinkField.Url = GetS3Links(registerLinkField.Url);
                                                _logger["meta"].Log("Updated Value : {0}", registerLinkField.Url);
                                                registerLinkField.LinkType = "external";
                                            }
                                        }

                                        _logger["debug"].Log("Editing PageTitle Field");
                                        item["PageTitle"] = title;

                                        _logger["debug"].Log("Editing MetaTitle Field");
                                        item["MetaTitle"] = title;

                                        item.Editing.EndEdit();
                                        itemCount++;
                                        _logger["debug"].Log("Editing End");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                                    _logger["error"].Log("{0}", item.ID);
                                    _logger["error"].Log("{0}", ex.Message);
                                    _logger["error"].Log("{0}", ex.StackTrace);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }

                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("Number of Items edited :{0}", itemCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        protected void FixTitles_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            int itemCount = 0;
            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                try
                {
                    _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "FixTitles_Debug.txt"));
                    _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "FixTitles_Error.txt"));
                    _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "FixTitles_Meta.txt"));

                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                    _logger["debug"].Log("-------------------------------------------------------------------");

                    var db = Factory.GetDatabase("master");
                    Item parentItem = db.GetItem(txtFilePath.Text);

                    foreach (Item items in parentItem.Axes.GetDescendants())
                    {
                        foreach (var language in LanguageManager.GetLanguages(db))
                        {
                            Item item = db.GetItem(items.ID.ToString(), language);

                            if (item != null && item.Versions.Count > 0)
                            {
                                try
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        string title = item["MenuTitle"];
                                        
                                        _logger["debug"].Log("Editing item {0} started in {1}", item.Name, item.Language.ToString());
                                        item.Editing.BeginEdit();
 
                                        _logger["debug"].Log("Editing PageTitle Field");
                                        item["PageTitle"] = title;

                                        _logger["debug"].Log("Editing MetaTitle Field");
                                        item["MetaTitle"] = title;

                                        item.Editing.EndEdit();
                                        itemCount++;
                                        _logger["debug"].Log("Editing End");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                                    _logger["error"].Log("{0}", item.ID);
                                    _logger["error"].Log("{0}", ex.Message);
                                    _logger["error"].Log("{0}", ex.StackTrace);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }

                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("Number of Items edited :{0}", itemCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }
    }
}