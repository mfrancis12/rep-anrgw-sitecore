﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FixLinks.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.FixLinks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Event Links</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>File Path :</label>
            <asp:TextBox ID="txtFilePath" runat="server" Style="width: 400px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="ButtonFixLinks" runat="server" Text="Fix Links in Sitecore" OnClick="FixLinks_Click" />
            <br />
            <br />
            <asp:Button ID="ButtonBrokenLinks" runat="server" Text="Fix Broken Links" OnClick="ButtonBrokenLinks_Click" />
        </div>
    </form>
</body>
</html>
