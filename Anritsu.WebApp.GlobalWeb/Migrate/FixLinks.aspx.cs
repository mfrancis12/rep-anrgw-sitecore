﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using HtmlAgilityPack;
using Sitecore.Configuration;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class FixLinks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)")]
        protected void FixLinks_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            int itemCount = 0;
            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                try
                {
                    _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "EventsFix1_Debug.txt"));
                    _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "EventsFix1_Error.txt"));
                    _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "EventsFix1_Meta.txt"));

                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                    _logger["debug"].Log("-------------------------------------------------------------------");

                    var db = Factory.GetDatabase("master");
                    Item parentItem = db.GetItem(txtFilePath.Text);

                    foreach (Item items in parentItem.Axes.GetDescendants())
                    {
                        foreach (var language in LanguageManager.GetLanguages(db))
                        {
                            Item item = db.GetItem(items.ID.ToString(), language);

                            if (item != null && (item.Versions.Count > 0) &&
                                (item.Template.ID.ToString().Equals("{4BE1B2CA-2BD0-43C0-B6B5-6892CB9E5F79}")
                                || item.Template.ID.ToString().Equals("{508AE037-8CB8-4DA2-B425-0095E738395E}")))
                            {
                                try
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        Sitecore.Data.Fields.LinkField registerLinkField = item.Fields["RegisterOrLaunchLink"];

                                        _logger["debug"].Log("Editing item {0} started in {1}", item.Name, item.Language.ToString());
                                        item.Editing.BeginEdit();

                                        if (registerLinkField != null)
                                        {
                                            _logger["debug"].Log("Editing RegisterOrLaunchLink Field");
                                            _logger["meta"].Log("Old Value : {0}", registerLinkField.Url);

                                            if (registerLinkField.Url.StartsWith("http://dl-dev.cdn-anritsu.com"))
                                            {
                                                registerLinkField.Url = registerLinkField.Url.Replace("dl-dev.cdn-anritsu.com", "dl.cdn-anritsu.com");
                                                _logger["meta"].Log("Updated Value : {0}", registerLinkField.Url);
                                                registerLinkField.LinkType = "external";
                                            }

                                            if(string.IsNullOrEmpty(registerLinkField.Url))
                                            {
                                                registerLinkField.Clear();
                                            }
                                        }
                                    }
                                    item.Editing.EndEdit();
                                    itemCount++;
                                    _logger["debug"].Log("Editing End");
                                }
                                catch (Exception ex)
                                {
                                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                                    _logger["error"].Log("{0}", item.ID);
                                    _logger["error"].Log("{0}", ex.Message);
                                    _logger["error"].Log("{0}", ex.StackTrace);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }

                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("Number of Items edited :{0}", itemCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)")]
        protected void ButtonBrokenLinks_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            MultiLogger _logger;
            int itemCount = 0, linkCount = 0;
            using (_logger = new MultiLogger())
            {
                try
                {
                    _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrokenLinks1_Debug.txt"));
                    _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrokenLinks1_Error.txt"));
                    _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrokenLinks1_1Meta.txt"));

                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["meta"].Log("-------------------------------------------------------------------");
                    _logger["meta"].Log("Item ID    Item Path");

                    string scPath = txtFilePath.Text;

                    var db = Factory.GetDatabase("master");
                    Item parentItem = db.GetItem(txtFilePath.Text);

                    foreach (Item items in parentItem.Axes.GetDescendants())
                    {
                        foreach (var language in LanguageManager.GetLanguages(db))
                        {
                            Item item = db.GetItem(items.ID.ToString(), language);

                            if (item != null && item.Versions.Count > 0)
                            {
                                try
                                {
                                    bool flag = false;
                                    item.Fields.ReadAll();
                                    var fieldsList = item.Fields.Where(field => field.Type.Equals("Rich Text") && !field.Name.Contains("__")).ToList();
                                    foreach (var field in fieldsList)
                                    {
                                        if (field.Type.Equals("Rich Text"))
                                        {
                                            HtmlDocument doc = new HtmlDocument();
                                            doc.LoadHtml(field.Value);
                                            if (doc.DocumentNode.SelectSingleNode("//a") != null)
                                            {
                                                foreach (HtmlNode nodes in doc.DocumentNode.SelectNodes("//a"))
                                                {
                                                    string link = nodes.GetAttributeValue("href", "");
                                                    if (link.StartsWith("http://dl-dev.cdn-anritsu.com"))
                                                    {
                                                        string value = link.Replace("dl-dev.cdn-anritsu.com", "dl.cdn-anritsu.com");
                                                        nodes.SetAttributeValue("href", value);
                                                        _logger["debug"].Log("Old Value : {0}", link);
                                                        _logger["debug"].Log("Updated Value : {0}", value);
                                                        linkCount++;
                                                        flag = true;
                                                    }
                                                }
                                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                                {
                                                    if (flag)
                                                    {
                                                        _logger["debug"].Log("Editing item {0} started {1}", item.Name, item.Paths.Path);
                                                        item.Editing.BeginEdit();
                                                        item[field.Name] = doc.DocumentNode.InnerHtml.ToString();
                                                        _logger["debug"].Log("Editing end");
                                                        item.Editing.EndEdit();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    itemCount++;
                                    _logger["meta"].Log("{0}	{1}", item.ID.ToString(), item.Paths.Path);
                                }
                                catch (Exception ex)
                                {
                                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                                    _logger["error"].Log("{0}", item.ID.ToString());
                                    _logger["error"].Log("{0}", ex.Message);
                                    _logger["error"].Log("{0}", ex.StackTrace);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("Number of links edited :{0}", linkCount);
                    _logger["debug"].Log("Number of items parsed :{0}", itemCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }
    }
}