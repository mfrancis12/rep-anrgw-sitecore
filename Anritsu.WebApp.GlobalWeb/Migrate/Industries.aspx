﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Industries.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.Products.Industries" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Industries Migration</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Teamsite DCR Path :</label>
            <asp:TextBox ID="txtTSDCRPath" runat="server"></asp:TextBox>
            <br />
            <br />
            <label>Sitecore Template:</label>
            <asp:DropDownList ID="SCTemplates" runat="server"></asp:DropDownList>
            <br />
            <br />
            <label>Sitecore Path :</label>
            <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox>
            <br />
            <br />
            <label>Select Language :</label>
            <asp:DropDownList ID="Languages" runat="server"></asp:DropDownList><br />
            <br />
            <asp:Button ID="ButtonIndustries" runat="server" Text="Import Industries" OnClick="ButtonIndustries_Click"/>
        </div>
    </form>
</body>
</html>
