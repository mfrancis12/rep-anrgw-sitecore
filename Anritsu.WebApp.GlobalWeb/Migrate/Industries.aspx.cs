﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using Excel;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate.Products
{
    public partial class Industries : System.Web.UI.Page
    {
        private Dictionary<string, string> customTabTitlesJP = new Dictionary<string, string>
        {
            {"規格","standard"},
            {"ライブラリ","library“"}
        };

        private Dictionary<string, string> customTabTitlesKR = new Dictionary<string, string>
        {
            {"규격","standard"},
            {"기준","standard"},
            {"라이브러리","library"},
            {"다운로드","downloads"},
            {"교육","education"},
            {"리쏘스","resources"},
            {"용어 해설","glossary"}
        };

        private Dictionary<string, string> customTabTitlesCN = new Dictionary<string, string>
        {
            {"上行/下行","downlink/uplink"},
            {"下载","downloads"},
            {"标准","standard"},
            {"资料库	","library"},
            {"培训","education"},
            {"资源","resources"},
            {"词汇","glossary"},
        };

        private Dictionary<string, string> customTabTitlesRU = new Dictionary<string, string>
        {
            {"Стандарт","standard"},
            {"Библиотека","library"},
        };

        private Dictionary<string, string> descriptionTabTitle = new Dictionary<string, string>
        {
            {"ja-JP","概要"},
            {"ko-KR","설명"},
            {"zh-CN","描述"},
            {"zh-TW","描述"},
            {"ru-RU","Описание"}
        };

        private Dictionary<int, string> cultureID = new Dictionary<int, string>
        {
            {1,"en-US"},
            {2,"ja-JP"},
            {3,"en-GB"},
            {4,"en-AU"},
            {6,"zh-CN"},
            {7,"ko-KR"},
            {10,"zh-TW"},
            {12,"aa-GW"},
            {14,"ru-RU"},
            {15,"en-IN"}
        };

        private IEnumerable<TemplateItem> _sitecoreTemplates = null;

        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");

        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        private string localPath = Sitecore.Configuration.Settings.GetSetting("ImageDownloadPath");
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");

        int itemCount = 0, versionCount = 0, overwriteCount = 0, relatedProductItemCount = 0, imageCount = 0, reusedImages = 0;

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));

                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected void ButtonIndustries_Click(object sender, EventArgs e)
        {
            MultiLogger logger;
            DateTime startTime = DateTime.Now;

            using (logger = new MultiLogger())
            {
                logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Industries_Debug_{0}.txt", Languages.SelectedValue));
                logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Industries_Error_{0}.txt", Languages.SelectedValue));

                logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Industries_Meta_{0}.txt", Languages.SelectedValue));
                logger["meta"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("Teamsite Path	IndustryID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;
                string scPath = txtSCPath.Text;

                FileService fs = new FileService();
                var fileList = fs.GetAllFiles(tsPath);
                //var fileList = new string[] { tsPath };

                foreach (string filePath in fileList)
                {
                    try
                    {
                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);
                        var xDoc = XDocument.Load(bufferStream);

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item"));

                        Language language = Language.Parse(Languages.SelectedValue);

                        Item parentItem = _master.GetItem(scPath, language);

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                        string[] array = filePath.Split('/');

                        string legacyUrl = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;

                        string industryID = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;

                        string itemName = string.Empty;
                        if (ItemUtil.ProposeValidItemName(array[array.Length - 1]).Equals("index"))
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));

                        }
                        else
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 1]));
                        }

                        Item industryItem = CreateItem(parentItem, itemName, logger, elementList);

                        if (industryItem != null)
                        {
                            #region code for Related Products

                            string master = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("master")).FirstOrDefault().Value;

                            string masterLanguage = cultureID.ContainsKey(Convert.ToInt32(master)) ? cultureID[Convert.ToInt32(master)].ToString() : string.Empty;

                            IEnumerable<XElement> masterNodes = null;
                            if (!string.IsNullOrEmpty(masterLanguage) && !masterLanguage.Equals(Languages.SelectedValue))
                            {
                                string masterFilePath = filePath.Replace(Languages.SelectedValue, masterLanguage);
                                var masterfileByteContent = fs.GetFileContent(masterFilePath);
                                var masterbufferStream = new MemoryStream(masterfileByteContent);

                                var masterDoc = XDocument.Load(masterbufferStream);
                                masterNodes = masterDoc.Root.Descendants();
                            }

                            string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");

                            List<string> productItemList = new List<string>();

                            XElement node = null;
                            DataTable prodRegional = GetDataFromFile(path, "ProductRegional");
                            if (prodRegional != null && prodRegional.Rows.Count > 0)
                            {
                                if (masterNodes != null && masterNodes.Count() > 0)
                                {
                                    node = masterNodes.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("products"));
                                }
                                else
                                {
                                    node = xDoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("products"));
                                }

                                foreach (var valueNode in node.Elements("value"))
                                {
                                    string productItemId = string.Empty;
                                    var oldProductId = valueNode.Elements("item").FirstOrDefault(x => x.Attribute("name").Value.Equals("product_id")).Value;
                                    var productRows = (from DataRow row in prodRegional.Rows
                                                       where (row["ProductID"].ToString().Equals(oldProductId))
                                                       select row);
                                    if (productRows != null && productRows.Count() > 0)
                                    {
                                        productItemId = productRows.Select(x => x["ItemID"]).FirstOrDefault().ToString();
                                    }
                                    if (string.IsNullOrEmpty(productItemId))
                                    {
                                        continue;
                                    }

                                    Item item = _master.GetItem(productItemId);
                                    if (item != null)
                                    {
                                        productItemList.Add(productItemId);
                                    }
                                }
                            }

                            using (new Sitecore.SecurityModel.SecurityDisabler())
                            {
                                logger["debug"].Log("Editing item started");
                                industryItem.Editing.BeginEdit();
                                logger["debug"].Log("Editing Related Products Field");
                                industryItem["RelatedProducts"] = string.Join("|", productItemList);
                                relatedProductItemCount++;
                                industryItem.Editing.EndEdit();
                                logger["debug"].Log("Editing End");
                            }
                        }
                            #endregion

                        ProcessTabs(industryItem, xDoc.Root.Descendants(), logger);

                        logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, industryID, industryItem.ID.ToString(), industryItem.Paths.Path, legacyUrl);
                    }
                    catch (Exception ex)
                    {
                        logger["error"].Log("{0}", DateTime.Now.ToString());
                        logger["error"].Log("{0}", ex.Message);
                        logger["error"].Log("{0}", ex.StackTrace);
                    }
                }
                if (logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    logger["debug"].Log("-------------------------------------------------");
                    logger["debug"].Log("Time taken:{0}", duration);
                    logger["debug"].Log("New industry items added : {0}", itemCount);
                    logger["debug"].Log("Number of Related Products added : {0}", relatedProductItemCount);
                    logger["debug"].Log("Overwritten industry items : {0}", overwriteCount);
                    logger["debug"].Log("Total number of versions for industry items : {0}", versionCount);
                    logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "dest")]
        protected Item CreateItem(Item dest, string itemName, MultiLogger logger, IEnumerable<XElement> elementList)
        {
            Item newItem = null;
            try
            {
                List<string> eleList = new List<string>();
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Language language = Language.Parse(Languages.SelectedValue);
                    newItem = _master.GetItem(dest.Paths.Path + "/" + itemName, language);

                    string heading = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                        x.Attribute("name").Value.Equals("heading")).FirstOrDefault().Value;

                    string technologyName = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("TechnologyName")).FirstOrDefault().Value;

                    string description = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("description")).FirstOrDefault().Value;

                    string altText = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("image_caption")).FirstOrDefault().Value;

                    var imageElement = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("left_image")).FirstOrDefault();

                    var metaValues = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("meta_values"));

                    string metaDesc = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("desc")).FirstOrDefault().Value;

                    string metaKeywords = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("keywords")).FirstOrDefault().Value;

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (newItem != null)
                        {
                            bool hasLangVersion = HasLanguageVersion(newItem, Languages.SelectedValue);
                            if (!hasLangVersion)
                            {
                                newItem = newItem.Versions.AddVersion();
                                versionCount++;
                                logger["debug"].Log("Added new {1} version for Item:{0}", newItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                            else
                            {
                                overwriteCount++;
                                logger["debug"].Log("Overwriting the item: {0} in {1} language", newItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                        }
                        else
                        {
                            newItem = dest.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                            itemCount++;
                            logger["debug"].Log("New item added {0} in {1}", newItem.Paths.Path, newItem.Language.Name);
                        }

                        newItem.Editing.BeginEdit();

                        logger["debug"].Log("Editing TechnologyName field.");
                        newItem["IndustryName"] = technologyName;

                        logger["debug"].Log("Editing Title field.");
                        newItem["BannerTitle"] = Sitecore.StringUtil.RemoveTags(heading);

                        logger["debug"].Log("Editing Description field.");
                        newItem["BannerDescription"] = StringHtmlExtensions.RemoveAttributes(description, new string[] { "class", "style", "width", "height" });

                        logger["debug"].Log("Editing Menu Title field");
                        newItem["MenuTitle"] = technologyName;

                        logger["debug"].Log("Editing Meta Description field");
                        newItem["Metatags-Description"] = metaDesc;

                        logger["debug"].Log("Editing Meta Keywords field");
                        newItem["Metatags-Other keywords"] = metaKeywords;

                        logger["debug"].Log("Editing Image field");
                        if (!string.IsNullOrEmpty(imageElement.Value))
                        {
                            MediaItem mediaImage = ImageFields(imageElement, itemName, false, string.Empty, altText);
                            if (mediaImage != null)
                            {
                                Sitecore.Data.Fields.ImageField iconImage = newItem.Fields["BannerImage"];
                                iconImage.Clear();
                                iconImage.MediaID = mediaImage.ID;
                                if (!string.IsNullOrEmpty(altText))
                                {
                                    iconImage.Alt = altText;
                                }
                                logger["debug"].Log("Image Assigned {0}", iconImage.MediaItem.Paths.Path);
                            }
                        }

                        logger["debug"].Log("Editing End.");
                        newItem.Editing.EndEdit();
                    }
                }
            }
            catch (Exception ex)
            {
                logger["error"].Log(DateTime.Now.ToString());
                logger["error"].Log("Error occured while creating item {0} under {1}", itemName, dest.Paths.Path);
                logger["error"].Log(ex.Message);
                logger["error"].Log(ex.StackTrace);
            }

            return newItem;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private void ProcessTabs(Item item, IEnumerable<XElement> nodes, MultiLogger logger)
        {
            try
            {
                TemplateID tabTemplate = new TemplateID(new ID("{E280A6CF-E094-498A-A96E-E7032EB8FC9F}"));
                Language language = Language.Parse(Languages.SelectedValue);
                var tabNode = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Tabs")).FirstOrDefault();

                string description = string.Empty;
                var descriptionNode = tabNode.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("decription_tab_content"));
                if (descriptionNode != null && descriptionNode.Count() > 0)
                {
                    description = descriptionNode.FirstOrDefault().Value;
                }

                var customTabs = nodes.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("custom_tab"));


                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    if (!string.IsNullOrEmpty(description))
                    {
                        Item descriptionItem = _master.GetItem(item.Paths.Path + "/description", language);
                        if (descriptionItem == null)
                        {
                            descriptionItem = item.Add("description", tabTemplate);
                            logger["debug"].Log("New item added {0}", descriptionItem.Paths.Path);
                        }
                        else
                        {
                            bool hasLangVersion = HasLanguageVersion(descriptionItem, Languages.SelectedValue);
                            if (!hasLangVersion)
                            {
                                descriptionItem = descriptionItem.Versions.AddVersion();
                                logger["debug"].Log("Added new {1} version for Item:{0}", descriptionItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                            else
                            {
                                logger["debug"].Log("Overwriting the item: {0} in {1} language", descriptionItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                        }

                        descriptionItem.Editing.BeginEdit();
                        logger["debug"].Log("Editing item {0}", descriptionItem.Paths.Path);
                        logger["debug"].Log("Editing field Title");
                        string descText = "Description";
                        if (descriptionTabTitle.ContainsKey(Languages.SelectedValue))
                        {
                            descText = descriptionTabTitle[Languages.SelectedValue];
                        }
                        descriptionItem["Title"] = descText;
                        logger["debug"].Log("Editing End");
                        logger["debug"].Log("Editing field Description");
                        string newdescription = ProcessMediaInRichText(description);
                        descriptionItem["Description"] = WrapTableTag(StringHtmlExtensions.RemoveAttributes(newdescription, new string[] { "class", "style", "width", "height" }));
                        logger["debug"].Log("Editing End");

                        descriptionItem.Editing.EndEdit();
                    }

                    if (customTabs != null && customTabs.Count() > 0)
                    {
                        foreach (XElement tab in customTabs.Distinct())
                        {
                            string tabTitle = tab.Descendants().Where(x => x.Name.LocalName.Equals("item")
                                                        && x.Attribute("name").Value.Equals("custom_tab_title")).FirstOrDefault().Value;
                            string tabDescription = tab.Descendants().Where(x => x.Name.LocalName.Equals("item")
                                                        && x.Attribute("name").Value.Equals("custom_tab_content")).FirstOrDefault().Value;

                            string tabTitleValue = tabTitle;
                            string englishTitle = GetTabTitle(tabTitle, Languages.SelectedValue);

                            string tabItemName = GetItemName(englishTitle);
                            Item customTab = _master.GetItem(item.Paths.Path + "/" + tabItemName, language);

                            if (customTab == null)
                            {
                                customTab = item.Add(tabItemName, tabTemplate);
                                logger["debug"].Log("New item added {0}", customTab.Paths.Path);
                            }
                            else
                            {
                                bool hasLangVersion = HasLanguageVersion(customTab, Languages.SelectedValue);
                                if (!hasLangVersion)
                                {
                                    customTab = customTab.Versions.AddVersion();
                                    logger["debug"].Log("Added new {1} version for Item:{0}", customTab.Paths.Path, Languages.SelectedItem.Text);
                                }
                                else
                                {
                                    logger["debug"].Log("Overwriting the item: {0} in {1} language", customTab.Paths.Path, Languages.SelectedItem.Text);
                                }
                            }

                            customTab.Editing.BeginEdit();
                            logger["debug"].Log("Editing item {0}", customTab.Paths.Path);
                            logger["debug"].Log("Editing field Title");
                            customTab["Title"] = tabTitleValue;
                            logger["debug"].Log("Editing End");
                            logger["debug"].Log("Editing field Description");
                            string newtabDescription = ProcessMediaInRichText(tabDescription);
                            customTab["Description"] = WrapTableTag(StringHtmlExtensions.RemoveAttributes(newtabDescription, new string[] { "class", "style", "width", "height" }));
                            logger["debug"].Log("Editing End");
                            customTab.Editing.EndEdit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger["error"].Log("{0}", DateTime.Now.ToString());
                logger["error"].Log("{0}", ex.Message);
                logger["error"].Log("{0}", ex.StackTrace);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "isproduct"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "richtextimage")]
        protected MediaItem ImageFields(XElement element, string itemName, bool isproduct, string richtextimage, string altText)
        {
            string elementPath = string.Empty;
            if (element != null)
            {
                elementPath = element.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                elementPath = HttpUtility.UrlDecode(elementPath);
            }
            else if (!string.IsNullOrEmpty(richtextimage))
            {
                elementPath = HttpUtility.UrlDecode(richtextimage);
            }

            string tsFilePath = workAreaPath.Replace("/templatedata/", "") + elementPath;

            elementPath = isproduct ? "Images/Products/" + itemName + "/" + elementPath.Split('/').Last() : elementPath;

            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/" + path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            FileService imageFS = new FileService();
            var bytes = imageFS.GetFileContent(tsFilePath);
            var mediaPath = elementPath.Split('/');
            var mediaItemName = mediaPath.Last();
            if (bytes != null)
            {
                var stream = new MemoryStream(bytes);
                var bw = new BinaryWriter(new FileStream(localPath + mediaItemName, FileMode.Append, FileAccess.Write));
                bw.Write(bytes);
                bw.Close();
            }
            else
            {
                return null;
            }

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public MediaItem CreateMediaItem(string mediaUrl, string localMediaPath, string altText)
        {
            MediaItem mediaItem = null;
            MultiLogger logger;
            using (logger = new MultiLogger())
            {
                logger.Add("image", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog.txt");
                logger["image"].Log("{0}	Started uploading image", DateTime.Now.ToString());
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaPath = mediaUrl.Split('/');
                    var mediaFolderID = CreateMediaFolders(mediaPath);
                    var mediaFolder = master.GetItem(mediaFolderID);
                    var mediaItemName = mediaPath.Last();

                    FileInfo file = new FileInfo(localMediaPath + mediaItemName);
                    if (file.Exists)
                    {
                        var filename = GetItemName(mediaItemName.Split('.')[0]);
                        filename = filename.Replace(".", "_");
                        var options = new Sitecore.Resources.Media.MediaCreatorOptions();
                        options.Database = master;
                        options.Versioned = false;
                        options.Destination = mediaFolder.Paths.FullPath + "/" + filename;
                        options.IncludeExtensionInItemName = false;
                        options.AlternateText = string.IsNullOrEmpty(altText) ? mediaItemName : altText;
                        options.FileBased = Sitecore.Configuration.Settings.Media.UploadAsFiles;
                        var mediaCreator = new Sitecore.Resources.Media.MediaCreator();
                        try
                        {
                            mediaItem = mediaCreator.CreateFromFile(file.FullName, options);
                            logger["image"].Log("{0}	New image created : {1}", DateTime.Now.ToString(), mediaItem.Path);
                            logger["image"].Log("Image Size {0} bytes", mediaItem.Size.ToString());
                            imageCount++;
                        }
                        catch (Exception ex)
                        {
                            logger["image"].Log(DateTime.Now.ToString() + "	Error while creating image");
                            logger["image"].Log(ex.Message);
                            logger["image"].Log(ex.StackTrace);
                            //Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return mediaItem;
        }

        public ID CreateMediaFolders(string[] mediaPath)
        {
            Item mediaFolder = null;
            string mediaLibraryPath = "/sitecore/media library";
            foreach (string path in mediaPath)
            {
                if (!path.Contains('.') && !string.IsNullOrWhiteSpace(path))
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaFolderTemplate = master.Templates["System/Media/Media folder"];

                    var mediaLibraryItem = master.GetItem(mediaLibraryPath);
                    mediaLibraryPath = mediaLibraryPath + "/" + GetItemName(path);
                    mediaFolder = master.GetItem(mediaLibraryPath);

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (mediaFolder == null)
                        {
                            try
                            {
                                mediaFolder = mediaLibraryItem.Add(GetItemName(path), mediaFolderTemplate);
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return mediaFolder.ID;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)")]
        public string ProcessMediaInRichText(string htmlSource)
        {
            List<string> imgUrls = FetchLinksFromSource(htmlSource);
            string html = htmlSource;
            if (imgUrls.Count > 0)
            {
                imgUrls = imgUrls.Distinct().Where(x => !x.StartsWith("http")).ToList();

                foreach (string url in imgUrls)
                {
                    MediaItem image = ImageFields(null, string.Empty, false, url, string.Empty);
                    if (image == null)
                        continue;
                    string newUrl = "~/media/" + image.ID.ToShortID() + ".ashx";
                    html = html.Replace(url, newUrl);
                }
            }

            return html;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }

        public string WrapTableTag(string html)
        {
            html = html.Replace("<table", "<div class=\"detail-table\"><table");
            html = html.Replace("</table>", "</table></div>");
            return html;
        }

        public string GetTabTitle(string tabTitle, string languageName)
        {
            string title = tabTitle;
            string tabValue;
            switch (languageName)
            {
                case "ja-JP":
                    if (customTabTitlesJP.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
                case "ko-KR":
                    if (customTabTitlesKR.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
                case "zh-CN":
                    if (customTabTitlesCN.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
                case "ru-RU":
                    if (customTabTitlesRU.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
            }
            return title;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1306:SetLocaleForDataTypes"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "Extension"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "isHDR"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sheetName")]
        private DataTable GetDataFromFile(string FilePath, string sheetName)
        {
            MultiLogger _logger = null;
            DataTable dt = new DataTable();
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileStream stream = File.Open(FilePath, FileMode.Open, FileAccess.Read);

                    //Reading from a OpenXml Excel file (2007 format; *.xlsx)                    
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                    //DataSet - Create column names from first row
                    excelReader.IsFirstRowAsColumnNames = true;

                    //DataSet - The result of each spreadsheet will be created in the result.Tables
                    DataSet result = excelReader.AsDataSet();

                    //6. Free resources (IExcelDataReader is IDisposable)
                    excelReader.Close();

                    dt = result.Tables[sheetName];
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToShortDateString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
            return dt;
        }
    }
}