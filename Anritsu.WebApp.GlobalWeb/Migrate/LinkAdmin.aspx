﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LinkAdmin.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.LinkAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Link Admin URL's</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Teamsite DCR Path :</label>
            <asp:TextBox ID="txtTSDCRPath" runat="server" Style="width: 400px"></asp:TextBox>
            <br />
            <br />
            <label>Sitecore Template :</label>
            <asp:TextBox ID="txtSCTemplates" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="GetLinks" runat="server" Text="Get Links" OnClick="GetLinks_Click" />
        </div>
    </form>
</body>
</html>
