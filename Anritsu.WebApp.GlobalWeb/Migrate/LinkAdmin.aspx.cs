﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using ContentMigration.Services;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class LinkAdmin : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    var languages = LanguageManager.GetLanguages(_master);
            //    foreach (var language in languages)
            //    {
            //        Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
            //    }
            //    Languages.Items.Insert(0, new ListItem("--select--", "0"));
            //}
        }

        protected void GetLinks_Click(object sender, EventArgs e)
        {
            MultiLogger _logger;
            string tsPath = txtTSDCRPath.Text;
            DateTime startTime = DateTime.Now;
            int count = 0;
            using (_logger = new MultiLogger())
            {
                _logger.Add("links", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "FilePath.txt"));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Error.txt"));
                _logger["links"].Log("Teamsite Path	SitecoreLink");
                _logger["links"].Log("-------------------------------------------------------------------------------------------");
                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);
                    //var fileList = new string[] { tsPath };

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string itemName = string.Empty;
                            string[] array = filePath.Split('/');

                            if (ItemUtil.ProposeValidItemName(array[array.Length - 1]).Equals("index"))
                            {
                                itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));
                            }
                            else
                            {
                                itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 1]));
                            }

                            string lang = string.Empty;
                            if (filePath.Contains("en-US"))
                                lang = "en-US";
                            else if (filePath.Contains("en-GB"))
                                lang = "en-GB";
                            else if (filePath.Contains("en-AU"))
                                lang = "en-AU";
                            else if (filePath.Contains("ja-JP"))
                                lang = "ja-JP";
                            else if (filePath.Contains("ko-KR"))
                                lang = "ko-KR";
                            else if (filePath.Contains("ru-RU"))
                                lang = "ru-RU";
                            else if (filePath.Contains("zh-CN"))
                                lang = "zh-CN";
                            else if (filePath.Contains("zh-TW"))
                                lang = "zh-TW";

                            Language language = Language.Parse(lang);
                            //Language language = Language.Parse(Languages.SelectedValue);

                            string templateName = txtSCTemplates.Text;
                            //string templateName = string.Empty;
                            //if (filePath.Contains("Online-Webinars"))
                            //    templateName = "Webinar";
                            //else if (filePath.Contains("Trade-Shows"))
                            //    templateName = "TradeShow";
                            //else if (filePath.Contains("Other-Special-Events"))
                            //    templateName = "TMEvent";

                            Item testItem = _master.SelectSingleItem("fast:/sitecore/content/GlobalWeb/home//*[@@templatename = \"" + templateName + "\" and @@name=\"" + itemName + "\"]");
                            if (testItem != null)
                            {
                                Item item = _master.GetItem(testItem.ID.ToString(), language);
                                if (item != null)
                                {
                                    var options = Sitecore.Links.LinkManager.GetDefaultUrlOptions();
                                    options.Language = language;
                                    string link = Sitecore.Links.LinkManager.GetItemUrl(item, options);
                                    _logger["links"].Log("{0}	{1}	", filePath, link);
                                    count++;
                                }
                            }
                            else
                            {
                                _logger["error"].Log("Item Not Found : {0}", filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger["error"].Log("{0}", filePath);
                            _logger["error"].Log("{0}", ex.Message);
                            _logger["error"].Log("{0}", ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }

                TimeSpan ts = DateTime.Now - startTime;
                string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                _logger["links"].Log("-------------------------------------------------");
                _logger["links"].Log("Time taken:{0}", duration);
                _logger["links"].Log("Numbe of links:{0}", count);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }

        //    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.IndexOf(System.String)")]
        //    protected void GetLinks_Click(object sender, EventArgs e)
        //    {
        //        MultiLogger _logger;
        //        string tsPath = txtTSDCRPath.Text;
        //        DateTime startTime = DateTime.Now;
        //        int count = 0;
        //        using (_logger = new MultiLogger())
        //        {
        //            _logger.Add("links", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "FilePath.txt"));
        //            _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Error.txt"));
        //            _logger["links"].Log("Teamsite Path	SitecoreLink");
        //            _logger["links"].Log("-------------------------------------------------------------------------------------------");
        //            try
        //            {
        //                FileService fs = new FileService();
        //                var fileList = fs.GetAllFiles(tsPath);
        //                //var fileList = new string[] { tsPath };

        //                foreach (string filePath in fileList)
        //                {
        //                    try
        //                    {
        //                        string itemName = string.Empty;
        //                        string[] array = filePath.Split('/');

        //                        if (ItemUtil.ProposeValidItemName(array[array.Length - 1]).Equals("index"))
        //                        {
        //                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));
        //                        }
        //                        else
        //                        {
        //                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 1]));
        //                        }

        //                        string lang = string.Empty;
        //                        if (filePath.Contains("en-US"))
        //                            lang = "en-US";
        //                        else if (filePath.Contains("en-GB"))
        //                            lang = "en-GB";
        //                        else if (filePath.Contains("en-AU"))
        //                            lang = "en-AU";
        //                        else if (filePath.Contains("ja-JP"))
        //                            lang = "ja-JP";
        //                        else if (filePath.Contains("ko-KR"))
        //                            lang = "ko-KR";
        //                        else if (filePath.Contains("ru-RU"))
        //                            lang = "ru-RU";
        //                        else if (filePath.Contains("zh-CN"))
        //                            lang = "zh-CN";
        //                        else if (filePath.Contains("zh-TW"))
        //                            lang = "zh-TW";

        //                        Language language = Language.Parse(lang);
        //                        //Language language = Language.Parse(Languages.SelectedValue);

        //                        filePath.Contains("data/");
        //                        string templateName = txtSCTemplates.Text;
        //                        string dataString = "/data/";
        //                        string code = filePath.Substring(filePath.IndexOf(dataString) + dataString.Length);
        //                        //string templatePath = code;

        //                        Item[] items = _master.SelectItems("fast:/sitecore/content/GlobalWeb/home/#test-measurement#/solutions//*[@@templatename = \"" + templateName + "\"]");
        //                        foreach (Item item in items)
        //                        {
        //                            string itemPath = item.Paths.Path.ToString();
        //                            if (itemPath.Contains("en-us"))
        //                                lang = "en-US";
        //                            else if (itemPath.Contains("en-gb"))
        //                                lang = "en-GB";
        //                            else if (itemPath.Contains("en-au"))
        //                                lang = "en-AU";
        //                            else if (itemPath.Contains("ja-jp"))
        //                                lang = "ja-JP";
        //                            else if (itemPath.Contains("ko-kr"))
        //                                lang = "ko-KR";
        //                            else if (itemPath.Contains("ru-ru"))
        //                                lang = "ru-RU";
        //                            else if (itemPath.Contains("zh-cn"))
        //                                lang = "zh-CN";
        //                            else if (itemPath.Contains("zh-tw"))
        //                                lang = "zh-TW";
        //                            string link = itemPath.Replace("sitecore/content/GlobalWeb/home", lang);
        //                            _logger["links"].Log("{0}", link);
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        //_logger["error"].Log("{0}", filePath);
        //                        _logger["error"].Log("{0}", ex.Message);
        //                        _logger["error"].Log("{0}", ex.StackTrace);
        //                    }
        //                }
        //            //}
        //            catch (Exception ex)
        //            {
        //                _logger["error"].Log("{0}", ex.Message);
        //                _logger["error"].Log("{0}", ex.StackTrace);
        //            }

        //            TimeSpan ts = DateTime.Now - startTime;
        //            string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
        //            _logger["links"].Log("-------------------------------------------------");
        //            _logger["links"].Log("Time taken:{0}", duration);
        //            _logger["links"].Log("Numbe of links:{0}", count);
        //        }
        //    }
        //}
    }
}