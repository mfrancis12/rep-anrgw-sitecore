﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsReleases.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.NewsReleases" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>News Releases Migration</title>
    <style type="text/css">
        .mappingContainer {
            width: 800px;
            display: inline-block;
        }

        .tsFields, .scFields {
            width: 300px;
            float: left;
            border: 1px solid #b4b4b4;
            padding: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Teamsite DCR Path :</label>
            <asp:TextBox ID="txtTSDCRPath" runat="server" Style="width: 400px"></asp:TextBox>
            <br />
            <br />
            <label>Sitecore Template:</label>
            <asp:DropDownList ID="SCTemplates" runat="server"></asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="GetFields" runat="server" Text="Get Fields" OnClick="GetFields_Click" />
            <br />
            <br />
            <asp:Repeater ID="repeaterTSFields" runat="server" OnItemDataBound="TeamsiteFields_ItemDataBound">
                <HeaderTemplate>
                    <div style="display: inline-block;">
                </HeaderTemplate>
                <ItemTemplate>
                    <div id="divMain" runat="server" class="mappingContainer">
                        <div class="tsFields">
                            <asp:Label ID="tsFieldName" runat="server"></asp:Label>
                            <asp:HiddenField ID="tsFieldID" runat="server"></asp:HiddenField>
                        </div>
                        <div class="scFields">
                            <asp:DropDownList ID="scFields" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            <br />
            <br />
            <asp:Panel ID="panelSC" runat="server" Visible="false">
                <label>Sitecore Path :</label>
                <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox><br />
                <br />
                <br />
                <label>Language :</label>
                <asp:DropDownList ID="Languages" runat="server"></asp:DropDownList><br />
                <br />
                <br />
                <asp:Button ID="ButtonNewsReleases" runat="server" Text="Import News Releases" OnClick="ButtonNewsReleases_Click" />
                <br />
                <br />
                <asp:Button ID="ButtonEvents" runat="server" Text="Import Events" OnClick="ButtonEvents_Click" />
                <br />
                <br />
                <asp:Button ID="ButtonAnnouncements" runat="server" Text="Import Announcements" OnClick="ButtonAnnouncements_Click" />
                <br />
                <br />
                <asp:Button ID="ButtonIRNews" runat="server" Text="Import IRNews" OnClick="ButtonIRNews_Click" />
                <br />
                <br />
            </asp:Panel>
        </div>
    </form>
</body>
</html>
