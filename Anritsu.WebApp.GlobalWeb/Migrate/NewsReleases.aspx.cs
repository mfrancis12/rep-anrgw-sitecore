﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using Excel;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
    public partial class NewsReleases : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");

        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        private string localPath = Sitecore.Configuration.Settings.GetSetting("ImageDownloadPath");
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");
        private string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");

        List<string> lstSelectedTSFields = new List<string>();
        List<string> lstSelectedSCFields = new List<string>();

        MultiLogger _logger;
        int itemCount = 0, versionCount = 0, overwriteCount = 0;
        int imageCount = 0, reusedImages = 0;
        int relatedProductsCount = 0, relatedProductFamiliesCount = 0, relatedTechnologiesCount, relatedIndustriesCount = 0;

        private Dictionary<int, string> cultureID = new Dictionary<int, string>
        {
            {1,"en-US"},
            {2,"ja-JP"},
            {3,"en-GB"},
            {4,"en-AU"},
            {6,"zh-CN"},
            {7,"ko-KR"},
            {10,"zh-TW"},
            {12,"aa-GW"},
            {14,"ru-RU"},
            {15,"en-IN"}
        };

        private Dictionary<int, string> technologyIds = new Dictionary<int, string>
        {
            {6210,"{32DE0B26-6E9F-4D57-B0C5-708EA61698C7}"},
            {6207,"{B8B29AF9-2172-44FA-833D-B4513775BFB1}"},
            {6212,"{6FFD737C-6FFA-4F90-98C4-12422C218F13}"},
            {6208,"{D6066EAA-8441-49D5-9F0D-3C59D5B3BC4A}"},
            {4583,"{AB3A116F-31C0-42B0-A32F-48D1868DC6A3}"},
            {4824,"{19751F5E-DAF6-459E-BC38-52EA117CA439}"},
            {4973,"{15D0520D-EF3C-4DCB-98A5-E24DD5BDA7F7}"},
            {5248,"{1FCEA098-F5DA-44B9-876D-8BC1AF0A8256}"},
            {5534,"{3EC78899-02B2-4B03-AB2B-B05F6DBBEF9B}"},
            {5544,"{629EF911-8198-407B-B343-08DF96D0D3B8}"},
            {5351,"{15AD6EC7-1D2F-40CE-9078-32230FA838FE}"},
            {4582,"{A5509E0C-B421-40EE-B817-B9DC1A8CDBA3}"},
            {4955,"{E2EAE336-99C4-4ACA-B1F6-33E244142009}"},
            {4553,"{F10E1C21-FDB5-4327-BBF0-B2B6789A0A3C}"}
        };

        private Dictionary<int, string> industryIds = new Dictionary<int, string>
        {
            {5536,"DC0DCC35-3F09-44EE-A447-4514A2DCD9F3"},
            {5546,"E98142CD-E6DE-45E3-B348-F1EDA50884CE"},
            {5538,"A42D3D1F-FB24-4AB5-8D5B-7FD87B0A0398"},
            {5539,"F8283589-E9AB-42C0-A7A5-B3218562EA78"},
            {5540,"994C223F-0DDB-4794-82DB-B64062717C7A"}
        };

        private Dictionary<string, string> FileLinks = new Dictionary<string, string>
        {
            {"/Files/en-AU/","/en-au/test-measurement/files/"},
            {"/Files/en-GB/","/en-gb/test-measurement/files/"},
            {"/Files/en-US/","/en-us/test-measurement/files/"},
            {"/Files/ja-JP/","/ja-jp/test-measurement/files/"},
            {"/Files/ko-KR/","/ko-kr/test-measurement/files/"},
            {"/Files/zh-CN/","/zh-cn/test-measurement/files/"},
            {"/Files/zh-TW/","/zh-tw/test-measurement/files/"}
        };

        private Dictionary<string, string> S3links = new Dictionary<string, string>
        {
            {"/epub/","/en-us/test-measurement/epub/"},
            {"/Files/","/common/test-measurement/files/"},
            {"/Files/en-AU/","/en-au/test-measurement/files/"},
            {"/Files/en-GB/","/en-gb/test-measurement/files/"},
            {"/Files/en-US/","/en-us/test-measurement/files/"},
            {"/Files/ja-JP/","/ja-jp/test-measurement/files/"},
            {"/Files/ko-KR/","/ko-kr/test-measurement/files/"},
            {"/Files/zh-CN/","/zh-cn/test-measurement/files/"},
            {"/Files/zh-TW/","/zh-tw/test-measurement/files/"},
            {"/GWImages/images/","/images/legacy-images/images/"},
            {"/GWImages/apps/","/images/legacy-images/apps/"},
            {"/GWImages/controls/","/images/legacy-images/controls/"},
            {"/GWImages/css/","/images/legacy-images/css/"},
            {"/GWImages/custom/","/images/legacy-images/custom/"},
            {"/GWImages/js/","/images/legacy-images/js/"},
            {"/RefFiles/en/","/en-en/test-measurement/reffiles/"},
            {"/RefFiles/en-AU/","/en-au/test-measurement/reffiles/"},
            {"/RefFiles/en-CA/","/en-ca/test-measurement/reffiles/"},
            {"/RefFiles/en-GB/","/en-gb/test-measurement/reffiles/"},
            {"/RefFiles/en-US/","/en-us/test-measurement/reffiles/"},
            {"/RefFiles/es-MX/","/es-mx/test-measurement/reffiles/"},
            {"/RefFiles/ja-JP/","/ja-jp/test-measurement/reffiles/"},
            {"/RefFiles/ko-KR/","/ko-kr/test-measurement/reffiles/"},
            {"/RefFiles/pt-BR/","/pt-br/test-measurement/reffiles/"},
            {"/RefFiles/ru-RU/","/ru-ru/test-measurement/reffiles/"},
            {"/RefFiles/zh-CN/","/zh-cn/test-measurement/reffiles/"},
            {"/RefFiles/zh-HK/","/zh-hk/test-measurement/reffiles/"},
            {"/RefFiles/zh-TW/","/zh-tw/test-measurement/reffiles/"},
            {"/StreamFiles/custom/","/common/test-measurement/streamfiles/"},
            {"/StreamFiles/en-US/","/en-us/test-measurement/streamfiles/"}
        };

        private IEnumerable<TemplateItem> _sitecoreTemplates = null;

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "path"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public string GetFamilyIds(List<string> legacyFamilyIds)
        {
            string familyIds = string.Empty;
            string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");

            if (legacyFamilyIds != null && legacyFamilyIds.Count() > 0)
            {
                DataTable productFamiyAssoc = GetDataFromFile(path, "SCProdFamilyAssoc");
                var familyItemIds = (from DataRow row in productFamiyAssoc.Rows
                                     where legacyFamilyIds.Contains(row["FamilyID"].ToString())
                                     select row);
                familyIds = string.Join("|", familyItemIds.Select(x => x["ItemID"].ToString()));
            }
            return familyIds;
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        protected void GetFields_Click(object sender, EventArgs e)
        {
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    if (!string.IsNullOrEmpty(txtTSDCRPath.Text))
                    {
                        List<string[]> list = new List<string[]>();

                        string dataCaptureFile = txtTSDCRPath.Text.Split(new string[] { "/data/" }, StringSplitOptions.None)[0] + "/datacapture.cfg";
                        list = ParseXml(dataCaptureFile, list);

                        if (list == null || list.Count == 0)
                        {
                            return;
                        }

                        repeaterTSFields.DataSource = list;
                        repeaterTSFields.DataBind();
                    }

                    var languages = LanguageManager.GetLanguages(_master);
                    foreach (var language in languages)
                    {
                        Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                    }
                    Languages.Items.Insert(0, new ListItem("--select--", "0"));

                    panelSC.Visible = true;
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log("Error occured while clicking the Get Fields Button.");
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<string[]> ParseXml(string filePath, List<string[]> list)
        {
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileService fs = new FileService();
                    Byte[] fileByteContent = fs.GetFileContent(filePath);

                    if (fileByteContent == null)
                        return null;

                    string[] fieldTypes = new string[] { "text", "browser", "textarea", "select", "checkbox", "radio", "readonly" };

                    MemoryStream bufferStream = new MemoryStream(fileByteContent);
                    XDocument xDoc = XDocument.Load(bufferStream);

                    foreach (var element in xDoc.Root.Descendants())
                    {
                        if (element.Descendants().Where(x => x.Name.LocalName.Equals("hidden")).Count() > 0)
                            continue;

                        if (element.Name.LocalName.Equals("container") && element.Attribute("min") != null && element.Attribute("max") != null)
                        {
                            var ancestors = element.Ancestors();
                            string[] replicantAnc = ancestors.Where(x => x.Name.LocalName.Equals("container") && x.Attribute("min") != null && x.Attribute("max") != null).Select(x => x.Name.LocalName).ToArray();
                            if (replicantAnc.Count() == 0)
                            {
                                string[] array = new string[4];

                                array[0] = element.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("label")).Value;
                                array[1] = element.Attribute("name").Value;
                                array[2] = "container";
                                array[3] = filePath;
                                list.Add(array);
                            }
                        }
                        else if (element.Name.LocalName.Equals("item"))
                        {
                            var ancestors = element.Ancestors();
                            string[] replicantAnc = ancestors.Where(x => x.Name.LocalName.Equals("container") && x.Attribute("min") != null && x.Attribute("max") != null).Select(x => x.Name.LocalName).ToArray();
                            if (replicantAnc.Count() == 0)
                            {
                                string[] array = new string[4];
                                var labelElement = element.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("label"));
                                array[0] = (labelElement == null) ? element.Attribute("name").Value : labelElement.Value;
                                array[1] = element.Attribute("name").Value;

                                string fieldType = element.Descendants().Where(x => fieldTypes.Contains(x.Name.LocalName)).FirstOrDefault().Name.LocalName;
                                var dateField = element.Descendants().Where(x => x.Name.LocalName.Equals("text") && x.Attribute("validation-regex") != null).FirstOrDefault();
                                if (dateField != null)
                                {
                                    if (dateField.Attribute("validation-regex").Value.Equals("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}") || dateField.Attribute("validation-regex").Value.Equals("[0-9]{4}-([1-9]{1}|0[1-9]{1}|1[012]{1})-(0[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1}|[1-9]{1})$") || dateField.Attribute("validation-regex").Value.Equals("[0-9]{4}-([1-9]{1}|0[1-9]{1}|1[012]{1})-(0[1-9]{1}$|[1-2]{1}[0-9]{1}$|3[0-1]{1}$|[1-9]{1}$)"))
                                    {
                                        fieldType = "date";
                                    }
                                }
                                array[2] = fieldType;
                                array[3] = string.Empty;
                                list.Add(array);
                            }
                        }
                        else if (element.Name.LocalName.Equals("inline") && !element.Parent.Name.LocalName.Equals("container"))
                        {
                            string value = element.Attribute("command").Value.Split(' ').Where(val => val.Contains(".xml")).FirstOrDefault();

                            if (!string.IsNullOrEmpty(value))
                            {
                                value = value.Replace(@"\", @"/");
                                string newFilePath = value.Replace("/templatedata/", "");
                                ParseXml(workAreaPath + newFilePath, list);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log("Error occured while Parsing the XML.");
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
                return list;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Teamsite")]
        protected void TeamsiteFields_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string[] tsFieldInfo = e.Item.DataItem as string[];

                Label label = e.Item.FindControl("tsFieldName") as Label;
                HiddenField hidden = e.Item.FindControl("tsFieldID") as HiddenField;
                DropDownList scFields = e.Item.FindControl("scFields") as DropDownList;

                label.Text = tsFieldInfo[0];
                hidden.Value = tsFieldInfo[1];

                scFields.DataSource = GetSCFields(tsFieldInfo[2], SCTemplates.SelectedValue);
                scFields.DataTextField = "Text";
                scFields.DataValueField = "Value";
                scFields.DataBind();

                scFields.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected List<ListItem> GetSCFields(string fieldType, string selected)
        {
            List<ListItem> fields = new List<ListItem>();
            if (selected == null)
                return null;
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    var template = _master.Templates[new ID(selected)];

                    if (template == null || template.Fields.Count() == 0)
                        return null;

                    if (fieldType.Equals("text"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && field.Type.Equals("Single-Line Text")))
                        {

                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("textarea"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && ((field.Type.Equals("Multi-Line Text") || field.Type.Equals("Rich Text") || field.Type.Equals("memo")))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("browser"))
                    {
                        var filteredFields = template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("General Link") || field.Type.Equals("Image")));

                        foreach (var field in filteredFields)
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("select"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Checklist") || field.Type.Equals("Droplist") || field.Type.Equals("DropLink") || field.Type.Equals("Droptree"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("checkbox") || fieldType.Equals("radio"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Checkbox"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("container"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Multilist") || field.Type.Equals("Treelist") || field.Type.Equals("TreelistEx") || field.Type.Equals("Multilist with Search"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("date"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Date"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }

            }
            return fields;
        }

        protected void GetFieldMappings()
        {
            foreach (RepeaterItem rptItem in repeaterTSFields.Items)
            {
                if (rptItem.ItemType == ListItemType.Item || rptItem.ItemType == ListItemType.AlternatingItem)
                {
                    var label = rptItem.FindControl("tsFieldName") as Label;
                    var hidden = rptItem.FindControl("tsFieldID") as HiddenField;
                    var scFields = rptItem.FindControl("scFields") as DropDownList;

                    string teamSiteField = hidden.Value;
                    string sitecoreField = scFields.SelectedItem.Text;
                    string scFieldValue = scFields.SelectedValue;
                    int result;
                    bool fieldNotSelected = int.TryParse(scFieldValue, out result);

                    if (!fieldNotSelected)
                    {
                        lstSelectedTSFields.Add(teamSiteField);
                        lstSelectedSCFields.Add(sitecoreField);
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "dest"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected Item CreateItem(Item dest, MultiLogger logger, string itemName, IEnumerable<XElement> elementList, bool isProduct, string altText, string filePath)
        {
            Item newItem = null;
            Item folderItem = null;

            try
            {
                List<string> eleList = new List<string>();
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Language language = Language.Parse(Languages.SelectedValue);

                    string[] array = filePath.Split('/');
                    string folderName = array[array.Length - 2];

                    if (folderName.Equals("Information"))
                        folderName = itemName.Substring(4, 4);

                    folderItem = _master.GetItem(dest.Paths.Path + "/" + folderName, language);

                    if (folderItem != null)
                    {
                        bool hasLangVersion = HasLanguageVersion(folderItem, Languages.SelectedValue);
                        if (!hasLangVersion)
                        {
                            folderItem = folderItem.Versions.AddVersion();
                            logger["debug"].Log("Added new {1} version for Item:{0}", folderItem.Paths.Path, Languages.SelectedItem.Text);
                        }
                        else
                        {
                            logger["debug"].Log("Overwriting the item: {0} in {1} language", folderItem.Paths.Path, Languages.SelectedItem.Text);
                        }
                    }
                    else
                    {
                        folderItem = dest.Add(GetItemName(folderName), new TemplateID(Sitecore.Data.ID.Parse("{3CAF38BE-A8DA-4073-884E-1CDB1F4A5F0E}")));
                        logger["debug"].Log("New item added {0} in {1}", dest.Paths.Path, dest.Language.Name);
                    }

                    newItem = _master.GetItem(folderItem.Paths.Path + "/" + itemName, language);

                    if (newItem != null)
                    {
                        bool hasLangVersion = HasLanguageVersion(newItem, Languages.SelectedValue);
                        if (!hasLangVersion)
                        {
                            newItem = newItem.Versions.AddVersion();
                            logger["debug"].Log("Added new {1} version for Item:{0}", newItem.Paths.Path, Languages.SelectedItem.Text);
                            versionCount++;
                        }
                        else
                        {
                            overwriteCount++;
                            logger["debug"].Log("Overwriting the item: {0} in {1} language", newItem.Paths.Path, Languages.SelectedItem.Text);
                        }
                    }
                    else
                    {

                        newItem = folderItem.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                        logger["debug"].Log("New item added {0} in {1}", newItem.Paths.Path, newItem.Language.Name);
                        itemCount++;
                    }

                    foreach (string item in lstSelectedTSFields)
                    {
                        foreach (var element in elementList)
                        {
                            if (element.Attribute("name").Value.Equals(item))
                            {
                                if (!eleList.Contains(element.Attribute("name").Value))
                                {
                                    if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Image")
                                    {
                                        if (!string.IsNullOrEmpty(element.Value))
                                        {
                                            MediaItem mediaImage = ImageFields(element, itemName, isProduct, string.Empty, altText);
                                            if (mediaImage != null)
                                            {
                                                newItem.Editing.BeginEdit();

                                                logger["debug"].Log("Editing image field : {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                                Sitecore.Data.Fields.ImageField iconImage = newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]];
                                                iconImage.Clear();
                                                iconImage.MediaID = mediaImage.ID;
                                                if (!string.IsNullOrEmpty(altText))
                                                {
                                                    iconImage.Alt = altText;
                                                }

                                                logger["debug"].Log("Image Assigned {0}", iconImage.MediaItem.Paths.Path);

                                                newItem.Editing.EndEdit();

                                                logger["debug"].Log("Editing image field end");
                                                eleList.Add(element.Attribute("name").Value);
                                            }
                                        }
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Date")
                                    {
                                        string isoDate = string.Empty;
                                        string dateValue = element.Value;
                                        if (!string.IsNullOrEmpty(dateValue))
                                        {
                                            isoDate = Sitecore.DateUtil.ToIsoDate(DateTime.Parse(dateValue));
                                        }

                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = isoDate;

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Single-Line Text"
                                        || newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Multi-Line Text"
                                        || newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "memo")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = Sitecore.StringUtil.RemoveTags(value);

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Rich Text")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;
                                        string newvalue = ProcessMediaInRichText(value);
                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = StringHtmlExtensions.RemoveAttributes(newvalue, new string[] { "class", "style", "width", "height" });

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Checkbox")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = "0";
                                        var checkboxfields = element.Descendants().Where(x => x.Name.LocalName.Equals("value"));

                                        if (checkboxfields != null && checkboxfields.Count() > 0)
                                        {
                                            value = element.Value;
                                        }

                                        if (value.Equals("1"))
                                            newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = "1";
                                        else
                                            newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = string.Empty;

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = Sitecore.StringUtil.RemoveTags(value);

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger["error"].Log(DateTime.Now.ToString());
                logger["error"].Log("Error occured while creating item {0} under {1}", itemName, dest.Paths.Path);
                logger["error"].Log(ex.Message);
                logger["error"].Log(ex.StackTrace);
            }

            return newItem;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "path"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "isproduct"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "richtextimage")]
        protected MediaItem ImageFields(XElement element, string itemName, bool isproduct, string richtextimage, string altText)
        {
            string elementPath = string.Empty;
            if (element != null)
            {
                elementPath = element.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                elementPath = HttpUtility.UrlDecode(elementPath);
            }
            else if (!string.IsNullOrEmpty(richtextimage))
            {
                elementPath = HttpUtility.UrlDecode(richtextimage);
            }

            string tsFilePath = workAreaPath.Replace("/templatedata/", "") + elementPath;

            elementPath = isproduct ? "Images/Products/" + itemName + "/" + elementPath.Split('/').Last() : elementPath;

            if (string.IsNullOrWhiteSpace(elementPath))
            {
                return null;
            }
            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/" + path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            FileService imageFS = new FileService();
            var bytes = imageFS.GetFileContent(tsFilePath);
            var mediaPath = elementPath.Split('/');
            var mediaItemName = mediaPath.Last();
            if (bytes != null)
            {
                var stream = new MemoryStream(bytes);
                var bw = new BinaryWriter(new FileStream(localPath + mediaItemName, FileMode.Append, FileAccess.Write));
                bw.Write(bytes);
                bw.Close();
            }
            else
            {
                return null;
            }

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public MediaItem CreateMediaItem(string mediaUrl, string localMediaPath, string altText)
        {
            MediaItem mediaItem = null;
            MultiLogger logger;
            using (logger = new MultiLogger())
            {
                logger.Add("image", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog.txt");
                logger["image"].Log("{0}	Started uploading image", DateTime.Now.ToString());
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaPath = mediaUrl.Split('/');
                    var mediaFolderID = CreateMediaFolders(mediaPath);
                    var mediaFolder = master.GetItem(mediaFolderID);
                    var mediaItemName = mediaPath.Last();

                    FileInfo file = new FileInfo(localMediaPath + mediaItemName);
                    if (file.Exists)
                    {
                        var filename = GetItemName(mediaItemName.Split('.')[0]);
                        filename = filename.Replace(".", "_");
                        var options = new Sitecore.Resources.Media.MediaCreatorOptions();
                        options.Database = master;
                        options.Versioned = false;
                        options.Destination = mediaFolder.Paths.FullPath + "/" + filename;
                        options.IncludeExtensionInItemName = false;
                        options.AlternateText = string.IsNullOrEmpty(altText) ? mediaItemName : altText;
                        options.FileBased = Sitecore.Configuration.Settings.Media.UploadAsFiles;
                        var mediaCreator = new Sitecore.Resources.Media.MediaCreator();
                        try
                        {
                            mediaItem = mediaCreator.CreateFromFile(file.FullName, options);
                            logger["image"].Log("{0}	New image created : {1}", DateTime.Now.ToString(), mediaItem.Path);
                            logger["image"].Log("Image Size {0} bytes", mediaItem.Size.ToString());
                            imageCount++;
                        }
                        catch (Exception ex)
                        {
                            logger["image"].Log(DateTime.Now.ToString() + "	Error while creating image");
                            logger["image"].Log(ex.Message);
                            logger["image"].Log(ex.StackTrace);
                            //Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return mediaItem;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "path")]
        public ID CreateMediaFolders(string[] mediaPath)
        {
            Item mediaFolder = null;
            string mediaLibraryPath = "/sitecore/media library";
            foreach (string path in mediaPath)
            {
                if (!path.Contains('.') && !string.IsNullOrWhiteSpace(path))
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaFolderTemplate = master.Templates["System/Media/Media folder"];

                    var mediaLibraryItem = master.GetItem(mediaLibraryPath);
                    mediaLibraryPath = mediaLibraryPath + "/" + GetItemName(path);
                    mediaFolder = master.GetItem(mediaLibraryPath);

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (mediaFolder == null)
                        {
                            try
                            {
                                mediaFolder = mediaLibraryItem.Add(GetItemName(path), mediaFolderTemplate);
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return mediaFolder.ID;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)")]
        public string ProcessMediaInRichText(string htmlSource)
        {
            List<string> imgUrls = FetchLinksFromSource(htmlSource);
            string html = htmlSource;
            if (imgUrls.Count > 0)
            {
                imgUrls = imgUrls.Distinct().ToList();
                //imgUrls = imgUrls.Distinct().Where(x => !x.StartsWith("http")).ToList();

                foreach (string url in imgUrls)
                {
                    MediaItem image = null;
                    if (url.StartsWith("http"))
                    {
                        image = GetImageItem(url, string.Empty);
                    }
                    else
                    {
                        image = ImageFields(null, string.Empty, false, url, string.Empty);
                    }
                    if (image == null)
                        continue;
                    string newUrl = "~/media/" + image.ID.ToShortID() + ".ashx";
                    html = html.Replace(url, newUrl);
                }
            }

            return html;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1306:SetLocaleForDataTypes"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "Extension"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "isHDR"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sheetName")]
        private DataTable GetDataFromFile(string FilePath, string sheetName)
        {
            DataTable dt = new DataTable();
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileStream stream = File.Open(FilePath, FileMode.Open, FileAccess.Read);

                    //Reading from a OpenXml Excel file (2007 format; *.xlsx)                    
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                    //DataSet - Create column names from first row
                    excelReader.IsFirstRowAsColumnNames = true;

                    //DataSet - The result of each spreadsheet will be created in the result.Tables
                    DataSet result = excelReader.AsDataSet();

                    //6. Free resources (IExcelDataReader is IDisposable)
                    excelReader.Close();

                    dt = result.Tables[sheetName];
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToShortDateString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
            return dt;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace("---", "-").Replace("--", "-").TrimEnd('-').ToLowerInvariant();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "path"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)")]
        public string GetFamilyIds(string legacyProductId, string language)
        {
            string familyIds = string.Empty;
            string[] prodFamilyIds = null;
            string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");
            DataTable oldproductFamiyAssoc = GetDataFromFile(path, "OldProdFamilyAssoc");
            if (oldproductFamiyAssoc != null)
            {
                prodFamilyIds = (from DataRow row in oldproductFamiyAssoc.Rows
                                 where (row["ProductId"].ToString().Equals(legacyProductId) &&
                                 row["categorypageurl"].ToString().StartsWith("/" + language))
                                 select row).Select(x => x["CategoryId"].ToString()).ToArray<string>();
            }

            if (prodFamilyIds != null && prodFamilyIds.Count() > 0)
            {
                DataTable productFamiyAssoc = GetDataFromFile(path, "SCProdFamilyAssoc");
                var familyItemIds = (from DataRow row in productFamiyAssoc.Rows
                                     where prodFamilyIds.Contains(row["FamilyID"].ToString())
                                     select row);
                familyIds = string.Join("|", familyItemIds.Select(x => x["ItemID"].ToString()));
            }

            return familyIds;
        }

        public string WrapTableTag(string html)
        {
            html = html.Replace("<table", "<div class=\"detail-table\"><table");
            html = html.Replace("</table>", "</table></div>");
            return html;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "path"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)")]
        protected void ButtonEvents_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            GetFieldMappings();

            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));

                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	EventID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);
                    //var fileList = new string[] { tsPath };

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string[] array = filePath.Split('/');

                            string itemName = GetItemName(array[array.Length - 1]);
                            string scPath = txtSCPath.Text;

                            IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                            string legacyId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;
                            string legacyUrl = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;
                            string menuTitle = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Title")).FirstOrDefault().Value;

                            string altText = string.Empty;
                            var altNode = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("imageAltText"));

                            if (altNode != null && altNode.Count() > 0)
                            {
                                altText = altNode.FirstOrDefault().Value;
                            }

                            Item parentItem = _master.GetItem(scPath, Language.Parse(Languages.SelectedValue));

                            if (string.IsNullOrEmpty(scPath) || parentItem == null)
                            {
                                _logger["error"].Log("Sitecore path is empty or No item matched the path.");
                                continue;
                            }

                            _logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                            Item eventItem = CreateItem(parentItem, _logger, itemName, elementList, false, altText, filePath);

                            if (eventItem != null)
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    _logger["debug"].Log("Editing item started");
                                    eventItem.Editing.BeginEdit();

                                    _logger["debug"].Log("Editing MenuTitle Field");
                                    eventItem["MenuTitle"] = menuTitle;

                                    _logger["debug"].Log("Editing MetaTitle Field");
                                    eventItem["MetaTitle"] = menuTitle;

                                    _logger["debug"].Log("Editing PageTitle Field");
                                    eventItem["PageTitle"] = menuTitle;

                                    eventItem.Editing.EndEdit();
                                    _logger["debug"].Log("Editing End");
                                }

                                string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");

                                AssociationHandling(nodes, eventItem, _logger, false);

                                #region code for handling Additional Link field

                                var additionalLinkElement = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                    x.Attribute("name").Value.Equals("additional_link"));
                                string additionalLink = additionalLinkElement.FirstOrDefault() == null ?
                                                     string.Empty : additionalLinkElement.FirstOrDefault().Value;

                                Sitecore.Data.Fields.LinkField link = eventItem.Fields["AdditionalLink"];
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    eventItem.Editing.BeginEdit();
                                    _logger["debug"].Log("Editing item started");
                                    link.Url = GetS3Links(additionalLink);
                                    link.LinkType = "external";
                                    _logger["debug"].Log("Editing AdditionalLink Field");
                                    eventItem.Editing.EndEdit();
                                }
                                #endregion

                                #region code for handling ImageURL field

                                var imageUrlElement = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                    x.Attribute("name").Value.Equals("Trade_Show_Image_url"));
                                string imageUrl = imageUrlElement.FirstOrDefault() == null ?
                                                     string.Empty : imageUrlElement.FirstOrDefault().Value;

                                Sitecore.Data.Fields.LinkField imageUrlField = eventItem.Fields["ImageUrl"];
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    eventItem.Editing.BeginEdit();
                                    _logger["debug"].Log("Editing item started");
                                    imageUrlField.Url = GetS3Links(imageUrl);
                                    imageUrlField.LinkType = "external";
                                    _logger["debug"].Log("Editing ImageUrl Field");
                                    eventItem.Editing.EndEdit();
                                }
                                #endregion

                                #region code for handling Register/Launch fields

                                string eventType = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("EventType")).FirstOrDefault().Value;

                                if (eventType.Equals("W") || eventType.Equals("A"))
                                {
                                    var RegisterOrLaunchLinkElement = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                        x.Attribute("name").Value.Equals("register_link"));

                                    string RegisterOrLaunchLink = RegisterOrLaunchLinkElement.FirstOrDefault() == null ?
                                                                        string.Empty : RegisterOrLaunchLinkElement.FirstOrDefault().Value;

                                    var RegisterOrLaunchTitleElement = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                        x.Attribute("name").Value.Equals("register"));

                                    string RegisterOrLaunchTitle = RegisterOrLaunchTitleElement.FirstOrDefault() == null ?
                                                                        string.Empty : RegisterOrLaunchTitleElement.FirstOrDefault().Value;

                                    Sitecore.Data.Fields.LinkField RegisterOrLaunchLinkfield = eventItem.Fields["RegisterOrLaunchLink"];
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        _logger["debug"].Log("Editing item started");
                                        eventItem.Editing.BeginEdit();

                                        _logger["debug"].Log("Editing RegisterOrLaunchTitle Field");
                                        if (RegisterOrLaunchTitle.ToLower().Equals("launch"))
                                        {
                                            eventItem["RegisterOrLaunchTitle"] = "{69A87317-5C65-4A97-B76E-4564B43AFC39}";
                                        }

                                        else if (RegisterOrLaunchTitle.ToLower().Equals("register"))
                                        {
                                            eventItem["RegisterOrLaunchTitle"] = "{00D5A14C-DCE2-4C6F-BBF8-E554D3482B76}";
                                        }

                                        _logger["debug"].Log("Editing RegisterOrLaunchLink Field");
                                        RegisterOrLaunchLinkfield.Url = GetS3Links(RegisterOrLaunchLink);
                                        RegisterOrLaunchLinkfield.LinkType = "external";
                                        eventItem.Editing.EndEdit();
                                        _logger["debug"].Log("Editing item ended");
                                    }
                                }
                                #endregion

                                #region code for handling Event Type field

                                string eventTypeID = string.Empty;
                                if (eventType.Equals("W"))
                                {
                                    eventTypeID = "{7A0CBB6E-337E-4D67-A65F-3E3E7847243F}";
                                }
                                else if (eventType.Equals("T"))
                                {
                                    eventTypeID = "{0D8FB17E-3B23-46D1-A5A7-BE849C8A25BA}";
                                }

                                else if (eventType.Equals("A"))
                                {
                                    eventTypeID = "{B539B5B9-2E96-4402-B167-A29323CC996C}";
                                }

                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    _logger["debug"].Log("Editing item started");
                                    eventItem.Editing.BeginEdit();
                                    _logger["debug"].Log("Editing EventType Field");
                                    eventItem["EventType"] = eventTypeID;
                                    eventItem.Editing.EndEdit();
                                    _logger["debug"].Log("Editing End");
                                }

                                #endregion

                                _logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, legacyId, eventItem.ID.ToString(), eventItem.Paths.Path, legacyUrl);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger["error"].Log("{0}", DateTime.Now.ToString());
                            _logger["error"].Log("{0}", filePath);
                            _logger["error"].Log("{0}", ex.Message);
                            _logger["error"].Log("{0}", ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("New Event items added:{0}", itemCount);
                    _logger["debug"].Log("Number of related products items count:{0}", relatedProductsCount);
                    _logger["debug"].Log("Number of related products family items count:{0}", relatedProductFamiliesCount);
                    _logger["debug"].Log("Number of related technologies count:{0}", relatedTechnologiesCount);
                    _logger["debug"].Log("Number of related industries count:{0}", relatedIndustriesCount);
                    _logger["debug"].Log("Overwritten Event items:{0}", overwriteCount);
                    _logger["debug"].Log("Total number of versions for Event items:{0}", versionCount);
                    _logger["debug"].Log("New images count:{0}", imageCount);
                    _logger["debug"].Log("Reused images count:{0}", reusedImages);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected void ButtonAnnouncements_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            GetFieldMappings();

            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));

                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	AnnouncementID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);
                    //var fileList = new string[] { tsPath };

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string[] array = filePath.Split('/');

                            if (array[array.Length - 1].Contains("index"))
                            {
                                _logger["error"].Log("Skipping index page ");
                                continue;
                            }

                            string itemName = GetItemName(array[array.Length - 1]);
                            string year = itemName.Substring(4, 4);
                            string month = itemName.Substring(8, 2);
                            string day = itemName.Substring(10, 2);

                            string scPath = txtSCPath.Text;

                            IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                            string legacyUrl = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;
                            string menuTitle = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("page_title")).FirstOrDefault().Value;

                            Item parentItem = _master.GetItem(scPath, Language.Parse(Languages.SelectedValue));

                            if (string.IsNullOrEmpty(scPath) || parentItem == null)
                            {
                                _logger["error"].Log("Sitecore path is empty or No item matched the path.");
                                continue;
                            }

                            _logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                            Item announcementItem = CreateItem(parentItem, _logger, itemName, elementList, false, string.Empty, filePath);

                            if (announcementItem != null)
                            {
                                #region code to Handle Description Field
                                XElement contaniers = nodes.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("body_content")).FirstOrDefault();

                                foreach (XElement container in contaniers.Elements("value"))
                                {
                                    string content = container.Descendants().Where(x => x.Name.LocalName.Equals("item")
                                                                && x.Attribute("name").Value.Equals("content")).FirstOrDefault().Value;

                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        announcementItem.Editing.BeginEdit();
                                        _logger["debug"].Log("Editing item {0}", announcementItem.Paths.Path);

                                        _logger["debug"].Log("Editing Description Field");
                                        announcementItem["Description"] = announcementItem["Description"] + StringHtmlExtensions.RemoveAttributes(content, new string[] { "class", "style" });

                                        _logger["debug"].Log("Editing End");
                                        announcementItem.Editing.EndEdit();
                                    }
                                }
                                #endregion

                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    _logger["debug"].Log("Editing item started");
                                    announcementItem.Editing.BeginEdit();

                                    _logger["debug"].Log("Editing MenuTitle Field");
                                    announcementItem["MenuTitle"] = menuTitle;

                                    _logger["debug"].Log("Editing MetaTitle Field");
                                    announcementItem["MetaTitle"] = menuTitle;

                                    _logger["debug"].Log("Editing PageTitle Field");
                                    announcementItem["PageTitle"] = menuTitle;

                                    _logger["debug"].Log("Editing ReleaseDate Field");
                                    announcementItem["ReleaseDate"] = year + month + day + "T000000";

                                    announcementItem.Editing.EndEdit();
                                    _logger["debug"].Log("Editing End");
                                }
                                _logger["meta"].Log("{0}	{1}	{2}	{3}", filePath, announcementItem.ID.ToString(), announcementItem.Paths.Path, legacyUrl);

                            }
                        }
                        catch (Exception ex)
                        {
                            _logger["error"].Log("{0}", DateTime.Now.ToString());
                            _logger["error"].Log("{0}", ex.Message);
                            _logger["error"].Log("{0}", ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("New Announcement items added:{0}", itemCount);
                    _logger["debug"].Log("Overwritten Announcement items:{0}", overwriteCount);
                    _logger["debug"].Log("Total number of versions for Announcement items:{0}", versionCount);
                    _logger["debug"].Log("New images count:{0}", imageCount);
                    _logger["debug"].Log("Reused images count:{0}", reusedImages);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected void ButtonIRNews_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            GetFieldMappings();

            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));

                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	IRNewsID	ItemID	ItemPath");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);
                    //var fileList = new string[] { tsPath };

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string[] array = filePath.Split('/');

                            string itemName = GetItemName(array[array.Length - 1]);
                            string scPath = txtSCPath.Text;

                            IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                            string legacyId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;

                            Item parentItem = _master.GetItem(scPath, Language.Parse(Languages.SelectedValue));

                            if (string.IsNullOrEmpty(scPath) || parentItem == null)
                            {
                                _logger["error"].Log("Sitecore path is empty or No item matched the path.");
                                continue;
                            }

                            _logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                            Item IRNewsItem = CreateItem(parentItem, _logger, itemName, elementList, false, string.Empty, filePath);

                            if (IRNewsItem != null)
                            {
                                #region code for handling PDF Link field
                                string pdfLink = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                    x.Attribute("name").Value.Equals("PDFLink")).FirstOrDefault().Value;

                                Sitecore.Data.Fields.LinkField link = IRNewsItem.Fields["Link"];
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    _logger["debug"].Log("Editing item started");
                                    IRNewsItem.Editing.BeginEdit();
                                    link.Url = GetS3Links(pdfLink);
                                    link.LinkType = "external";
                                    _logger["debug"].Log("Editing Link Field");
                                    IRNewsItem.Editing.EndEdit();
                                }
                                #endregion

                                _logger["meta"].Log("{0}	{1}	{2}	{3}", filePath, legacyId, IRNewsItem.ID.ToString(), IRNewsItem.Paths.Path);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger["error"].Log("{0}", DateTime.Now.ToString());
                            _logger["error"].Log("{0}", filePath);
                            _logger["error"].Log("{0}", ex.Message);
                            _logger["error"].Log("{0}", ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("New IRNews items added:{0}", itemCount);
                    _logger["debug"].Log("Overwritten IRNews items:{0}", overwriteCount);
                    _logger["debug"].Log("Total number of versions for IRNews items:{0}", versionCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected void ButtonNewsReleases_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            GetFieldMappings();

            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));

                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	PressReleaseID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);
                    //var fileList = new string[] { tsPath }

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string[] array = filePath.Split('/');

                            string itemName = GetItemName(array[array.Length - 1]);
                            string scPath = txtSCPath.Text;

                            IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                            string legacyId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;
                            string legacyUrl = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;
                            string menuTitle = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Title")).FirstOrDefault().Value;

                            string altText = string.Empty;
                            var altNode = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("imageAltText"));

                            if (altNode != null && altNode.Count() > 0)
                            {
                                altText = altNode.FirstOrDefault().Value;
                            }

                            Item parentItem = _master.GetItem(scPath, Language.Parse(Languages.SelectedValue));

                            if (string.IsNullOrEmpty(scPath) || parentItem == null)
                            {
                                _logger["error"].Log("Sitecore path is empty or No item matched the path.");
                                continue;
                            }

                            _logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                            Item newsItem = CreateItem(parentItem, _logger, itemName, elementList, false, altText, filePath);

                            if (newsItem != null)
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    _logger["debug"].Log("Editing item started");
                                    newsItem.Editing.BeginEdit();

                                    _logger["debug"].Log("Editing MenuTitle Field");
                                    newsItem["MenuTitle"] = Sitecore.StringUtil.RemoveTags(menuTitle);

                                    _logger["debug"].Log("Editing MetaTitle Field");
                                    newsItem["MetaTitle"] = Sitecore.StringUtil.RemoveTags(menuTitle);

                                    _logger["debug"].Log("Editing PageTitle Field");
                                    newsItem["PageTitle"] = Sitecore.StringUtil.RemoveTags(menuTitle);

                                    _logger["debug"].Log("Clearing Long Description Field");
                                    newsItem["LongDescription"] = string.Empty;

                                    newsItem.Editing.EndEdit();
                                    _logger["debug"].Log("Editing End");
                                }

                                #region code for description fields
                                XElement contaniers = nodes.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("BodyContent")).FirstOrDefault();

                                foreach (XElement container in contaniers.Elements("value"))
                                {
                                    var textElement = container.Descendants().Where(x => x.Name.LocalName.Equals("item")
                                                                && x.Attribute("name").Value.Equals("text"));
                                    var contentElement = container.Descendants().Where(x => x.Name.LocalName.Equals("item")
                                                                && x.Attribute("name").Value.Equals("Content"));
                                    string text = (textElement.FirstOrDefault() == null) ? string.Empty : textElement.FirstOrDefault().Value;
                                    string content = (contentElement.FirstOrDefault() == null) ? string.Empty : contentElement.FirstOrDefault().Value;

                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        newsItem.Editing.BeginEdit();
                                        _logger["debug"].Log("Editing item {0}", newsItem.Paths.Path);

                                        _logger["debug"].Log("Editing LongDescription Field");
                                        if (!string.IsNullOrEmpty(text))
                                        {
                                            newsItem["LongDescription"] = newsItem["LongDescription"] + "<h3>" + text + "</h3>";
                                        }

                                        if (!string.IsNullOrEmpty(content))
                                        {
                                            newsItem["LongDescription"] = newsItem["LongDescription"] + StringHtmlExtensions.RemoveAttributes(ProcessMediaInRichText(content), new string[] { "class", "style", "height", "width" });
                                        }
                                        _logger["debug"].Log("Editing End");
                                        newsItem.Editing.EndEdit();
                                    }
                                }
                                #endregion

                                #region code for handling PDF Link field

                                var pdfLinkElement = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                    x.Attribute("name").Value.Equals("PDFLink"));

                                string pdfLink = pdfLinkElement.FirstOrDefault() == null ? string.Empty : pdfLinkElement.FirstOrDefault().Value;

                                if (!string.IsNullOrEmpty(pdfLink))
                                {
                                    Sitecore.Data.Fields.LinkField link = newsItem.Fields["Link"];
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        _logger["debug"].Log("Editing item started");
                                        newsItem.Editing.BeginEdit();
                                        link.Url = GetS3Links(pdfLink);
                                        link.LinkType = "external";
                                        _logger["debug"].Log("Editing Link Field");
                                        newsItem.Editing.EndEdit();
                                    }
                                }
                                #endregion

                                AssociationHandling(nodes, newsItem, _logger, true);

                                _logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, legacyId, newsItem.ID.ToString(), newsItem.Paths.Path, legacyUrl);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger["error"].Log("{0}", DateTime.Now.ToString());
                            _logger["error"].Log("{0}", filePath);
                            _logger["error"].Log("{0}", ex.Message);
                            _logger["error"].Log("{0}", ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("New NewsRelease items added:{0}", itemCount);
                    _logger["debug"].Log("Number of related products items count:{0}", relatedProductsCount);
                    _logger["debug"].Log("Number of related products family items count:{0}", relatedProductFamiliesCount);
                    _logger["debug"].Log("Number of related technologies count:{0}", relatedTechnologiesCount);
                    _logger["debug"].Log("Number of related industries count:{0}", relatedIndustriesCount);
                    _logger["debug"].Log("Overwritten NewsRelease items:{0}", overwriteCount);
                    _logger["debug"].Log("Total number of versions for NewsRelease items:{0}", versionCount);
                    _logger["debug"].Log("New images count:{0}", imageCount);
                    _logger["debug"].Log("Reused images count:{0}", reusedImages);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private void AssociationHandling(IEnumerable<XElement> nodes, Item item, MultiLogger _logger, bool isNews)
        {
            #region code for Related Products

            var relatedProductElement = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                            x.Attribute("name").Value.Equals("Related_Products"));

            string relatedProductsValue = relatedProductElement.FirstOrDefault() == null ? string.Empty : relatedProductElement.FirstOrDefault().Value;

            if (!string.IsNullOrEmpty(relatedProductsValue))
            {
                string[] relatedProducts = relatedProductsValue.Split(',').Select(x => x.Trim()).ToArray();

                if (relatedProducts != null && relatedProducts.Count() > 0)
                {
                    List<string> productItemList = new List<string>();
                    DataTable prodRegional = GetDataFromFile(path, "ProductRegional");
                    if (prodRegional != null && prodRegional.Rows.Count > 0)
                    {
                        foreach (string productID in relatedProducts)
                        {
                            string productItemId = string.Empty;
                            var productRows = (from DataRow row in prodRegional.Rows
                                               where (row["ProductID"].ToString().Equals(productID))
                                               select row);
                            if (productRows != null && productRows.Count() > 0)
                            {
                                productItemId = productRows.Select(x => x["ItemID"]).FirstOrDefault().ToString();
                            }
                            if (string.IsNullOrEmpty(productItemId))
                            {
                                continue;
                            }
                            Language lang = Language.Parse(Languages.SelectedValue);
                            Item productItem = _master.GetItem(productItemId, lang);
                            if (productItem != null)
                            {
                                productItemList.Add(productItem.ID.ToString());
                                relatedProductsCount++;
                            }
                        }
                    }
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        _logger["debug"].Log("Editing item started");
                        item.Editing.BeginEdit();
                        _logger["debug"].Log("Editing Related Products Field");
                        item["RelatedProducts"] = string.Join("|", productItemList);
                        item.Editing.EndEdit();
                        _logger["debug"].Log("Editing End");
                    }
                }
            }

            #endregion

            #region code for Related Product Family

            IEnumerable<XElement> relatedProductFamilyNodes;
            if (isNews)
            {
                relatedProductFamilyNodes = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                            x.Attribute("name").Value.Equals("ProductFamilies"));
            }

            else
            {
                relatedProductFamilyNodes = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                            x.Attribute("name").Value.Equals("Related_ProductFamily"));
            }

            List<string> productFamilyItemList = new List<string>();
            List<string> legacyFamilyIds = new List<string>();
            int i = 0;
            var familyNodes = relatedProductFamilyNodes.Descendants().Where(x => x.Name.LocalName.Equals("value"));
            foreach (var node in familyNodes)
            {
                string productItemId = string.Empty;
                var oldProductIds = node.Value.Replace("cid", ".").Split('.');
                legacyFamilyIds.Add(oldProductIds[0]);
                i++;
            }

            string familyIds = GetFamilyIds(legacyFamilyIds);
            string[] FamilyItemIdsList = familyIds.Split('|');

            foreach (string productFamilyItemID in FamilyItemIdsList)
            {
                Item productFamilyItem = _master.GetItem(productFamilyItemID, Language.Parse(Languages.SelectedValue));
                if (productFamilyItem != null)
                {
                    productFamilyItemList.Add(productFamilyItem.ID.ToString());
                    relatedProductFamiliesCount++;
                }
            }

            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                _logger["debug"].Log("Editing item started");
                item.Editing.BeginEdit();
                _logger["debug"].Log("Editing RelatedProductFamilies Field");
                item["RelatedProductFamilies"] = string.Join("|", productFamilyItemList);
                item.Editing.EndEdit();
                _logger["debug"].Log("Editing End");
            }
            #endregion

            #region code for Related Technologies

            IEnumerable<XElement> relatedTechnologiesNode;

            if (isNews)
            {
                relatedTechnologiesNode = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                           x.Attribute("name").Value.Equals("RelatedTechnologies"));
            }
            else
            {
                relatedTechnologiesNode = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                           x.Attribute("name").Value.Equals("Related_Technologies"));
            }

            List<string> relatedTechnologyItemList = new List<string>();
            foreach (string technologyID in relatedTechnologiesNode.Elements("value"))
            {
                if (technologyIds.ContainsKey(Convert.ToInt32(technologyID)))
                {
                    string technologyItemID = technologyIds[Convert.ToInt32(technologyID)];
                    Language lang = Language.Parse(Languages.SelectedValue);
                    Item technologyItem = _master.GetItem(technologyItemID, lang);
                    if (technologyItem != null)
                    {
                        relatedTechnologyItemList.Add(technologyItem.ID.ToString());
                        relatedTechnologiesCount++;
                    }
                }
            }
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                _logger["debug"].Log("Editing item started");
                item.Editing.BeginEdit();
                _logger["debug"].Log("Editing Related Technologies Field");
                item["RelatedTechnologies"] = string.Join("|", relatedTechnologyItemList);
                item.Editing.EndEdit();
                _logger["debug"].Log("Editing End");
            }

            #endregion

            #region code for Related Industries

            IEnumerable<XElement> relatedIndustriesNode;

            if (isNews)
            {
                relatedIndustriesNode = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                           x.Attribute("name").Value.Equals("RelatedIndustries"));
            }
            else
            {
                relatedIndustriesNode = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                           x.Attribute("name").Value.Equals("Related_Industry"));
            }

            List<string> relatedIdustryItemList = new List<string>();
            foreach (string industryID in relatedIndustriesNode.Elements("value"))
            {
                if (industryIds.ContainsKey(Convert.ToInt32(industryID)))
                {
                    string industryItemID = industryIds[Convert.ToInt32(industryID)];
                    Language lang = Language.Parse(Languages.SelectedValue);
                    Item industryItem = _master.GetItem(industryItemID, lang);
                    if (industryItem != null)
                    {
                        relatedIdustryItemList.Add(industryItem.ID.ToString());
                        relatedIndustriesCount++;
                    }
                }
            }
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                _logger["debug"].Log("Editing item started");
                item.Editing.BeginEdit();
                _logger["debug"].Log("Editing Related Industries Field");
                item["RelatedIndustries"] = string.Join("|", relatedIdustryItemList);
                item.Editing.EndEdit();
                _logger["debug"].Log("Editing End");
            }
            #endregion
        }

        //private string GetS3Links(string link)
        //{
        //    string domain = "http://gwdata.cdn-anritsu.com";
        //    string newLink = string.Empty;
        //    bool hasLink = S3links.Keys.Any(x => link.Contains(x));
        //    if (hasLink)
        //    {
        //        string oldurl = S3links.Keys.FirstOrDefault(key => link.Contains(key));
        //        string newurl = S3links[oldurl];
        //        newLink = domain + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));
        //    }
        //    else
        //    {
        //        newLink = link;
        //    }

        //    return newLink;
        //}

        private string GetS3Links(string link)
        {
            //string domain = "http://gwdata.cdn-anritsu.com";
            string domain = "http://dl-dev.cdn-anritsu.com";
            string newLink = string.Empty;

            bool hasFileLink = FileLinks.Keys.Any(x => link.Contains(x));
            if (hasFileLink)
            {
                string oldurl = FileLinks.Keys.FirstOrDefault(key => link.Contains(key));
                string newurl = FileLinks[oldurl];
                newLink = link.Replace(oldurl, newurl);
                newLink = domain + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));
            }
            else
            {
                bool hasLink = S3links.Keys.Any(x => link.Contains(x));
                if (hasLink)
                {
                    string oldurl = S3links.Keys.FirstOrDefault(key => link.Contains(key));
                    string newurl = S3links[oldurl];
                    newLink = link.Replace(oldurl, newurl);
                    newLink = domain + Sitecore.StringUtil.EnsurePrefix('/', link.Replace(oldurl, newurl));
                }
                else
                {
                    newLink = link;
                }
            }

            if (newLink.Contains("http://downloadfiles.anritsu.com"))
                newLink = newLink.Replace("/http://downloadfiles.anritsu.com", "");

            else if (newLink.Contains("http://downloadfile.anritsu.com"))
                newLink = newLink.Replace("/http://downloadfile.anritsu.com", "");

            return newLink;
        }

        protected MediaItem GetImageItem(string url, string altText)
        {
            var mediaPath = url.Split('/');
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(url, localPath + mediaPath.Last());
                }
                catch (Exception ex)
                {
                }
            }

            string host = new Uri(url).GetLeftPart(UriPartial.Authority);
            string elementPath = url.Replace(host, "");//TO-DO : remove host from the file

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }
    }
}