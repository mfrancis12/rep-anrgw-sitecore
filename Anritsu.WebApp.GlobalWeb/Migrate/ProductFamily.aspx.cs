﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using Excel;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class ProductFamily : System.Web.UI.Page
    {
        private IEnumerable<TemplateItem> _sitecoreTemplates = null;

        private Dictionary<int, string> cultureID = new Dictionary<int, string>
        {
            {1,"en-US"},
            {2,"ja-JP"},
            {3,"en-GB"},
            {4,"en-AU"},
            {6,"zh-CN"},
            {7,"ko-KR"},
            {10,"zh-TW"},
            {12,"aa-GW"},
            {14,"ru-RU"},
            {15,"en-IN"}
        };

        private Dictionary<string, string> SCItemNames = new Dictionary<string, string>
        {
            {"cable-antenna-analyzers", "cable-and-antenna-analyzers"},
            {"broadcasting-and-multimedia","broadcasting-multimedia"},
            {"service-testers", "shield-box"}
        };

        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        private string localPath = Sitecore.Configuration.Settings.GetSetting("ImageDownloadPath");
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");
        int productCount = 0, imageCount = 0, reusedImages = 0;

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));

                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1306:SetLocaleForDataTypes"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "Extension"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "isHDR"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sheetName")]
        private DataTable GetDataFromFile(string FilePath, string sheetName)
        {
            MultiLogger _logger = null;
            DataTable dt = new DataTable();
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileStream stream = File.Open(FilePath, FileMode.Open, FileAccess.Read);

                    //Reading from a OpenXml Excel file (2007 format; *.xlsx)                    
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                    //DataSet - Create column names from first row
                    excelReader.IsFirstRowAsColumnNames = true;

                    //DataSet - The result of each spreadsheet will be created in the result.Tables
                    DataSet result = excelReader.AsDataSet();

                    //6. Free resources (IExcelDataReader is IDisposable)
                    excelReader.Close();

                    dt = result.Tables[sheetName];
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToShortDateString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
            return dt;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected void ButtonMigrate_Click(object sender, EventArgs e)
        {
            MultiLogger logger;
            DateTime startTime = DateTime.Now;

            using (logger = new MultiLogger())
            {
                logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ProductFamily_Debug_{0}.txt", Languages.SelectedValue));
                logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ProductFamily_Error_{0}.txt", Languages.SelectedValue));

                logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ProductFamily_Meta_{0}.txt", Languages.SelectedValue));
                logger["meta"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("Teamsite Path	ProductFamilyID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;
                string scPath = txtSCPath.Text;

                FileService fs = new FileService();
                var fileList = fs.GetAllFiles(tsPath);
                //var fileList = new string[] { tsPath };

                foreach (string filePath in fileList)
                {
                    try
                    {
                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);
                        var xDoc = XDocument.Load(bufferStream);

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item"));

                        Language language = Language.Parse(Languages.SelectedValue);

                        Item parentItem = _master.GetItem(scPath, language);

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                        string[] array = filePath.Split('/');

                        string productFamilyID = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;

                        string familyName = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("FamilyName")).FirstOrDefault().Value;

                        string description = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("description")).FirstOrDefault().Value;

                        string legacyUrl = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;

                        string itemName = string.Empty;
                        string subCategoryName = string.Empty;

                        if (ItemUtil.ProposeValidItemName(array[array.Length - 1]).Equals("index"))
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));
                            subCategoryName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 3]));

                        }
                        else
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 1]));
                            subCategoryName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));
                        }

                        Item newItem = null;
                        try
                        {
                            List<string> eleList = new List<string>();

                            string newSubCategoryName = SCItemNames.ContainsKey(subCategoryName) ? SCItemNames[subCategoryName] : subCategoryName;
                            string newItemName = SCItemNames.ContainsKey(itemName) ? SCItemNames[itemName] : itemName;

                            newItem = _master.GetItem(parentItem.Paths.Path + "/" + newSubCategoryName + "/" + newItemName, language);

                            #region code for Products Field
                            string master = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("master")).FirstOrDefault().Value;

                            string masterLanguage = cultureID.ContainsKey(Convert.ToInt32(master)) ? cultureID[Convert.ToInt32(master)].ToString() : string.Empty;

                            IEnumerable<XElement> masterNodes = null;
                            if (!string.IsNullOrEmpty(masterLanguage) && !masterLanguage.Equals(Languages.SelectedValue))
                            {
                                string langSelected = Languages.SelectedValue.Equals("en") ? "en-AU" : Languages.SelectedValue;
                                string masterFilePath = filePath.Replace(langSelected, masterLanguage);
                                var masterfileByteContent = fs.GetFileContent(masterFilePath);
                                var masterbufferStream = new MemoryStream(masterfileByteContent);

                                var masterDoc = XDocument.Load(masterbufferStream);
                                masterNodes = masterDoc.Root.Descendants();
                            }

                            string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");

                            List<string> productItemList = new List<string>();

                            XElement node = null;
                            DataTable prodRegional = GetDataFromFile(path, "ProductRegional");
                            if (prodRegional != null && prodRegional.Rows.Count > 0)
                            {
                                if (masterNodes != null && masterNodes.Count() > 0)
                                {
                                    node = masterNodes.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("associated_products"));
                                }
                                else
                                {
                                    node = xDoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("associated_products"));
                                }

                                foreach (var valueNode in node.Elements("value"))
                                {
                                    string productItemId = string.Empty;
                                    var oldProductId = valueNode.Elements("item").FirstOrDefault(x => x.Attribute("name").Value.Equals("title")).Value;
                                    var productRows = (from DataRow row in prodRegional.Rows
                                                       where (row["ProductID"].ToString().Equals(oldProductId))
                                                       select row);
                                    if (productRows != null && productRows.Count() > 0)
                                    {
                                        productItemId = productRows.Select(x => x["ItemID"]).FirstOrDefault().ToString();
                                    }
                                    if (string.IsNullOrEmpty(productItemId))
                                    {
                                        continue;
                                    }
                                    
                                    Item item = _master.GetItem(productItemId,language);

                                    if (item != null &&  item.Versions.Count > 0)
                                    {
                                        Item globalItem = _master.GetItem(item["SelectProduct"], language);

                                        if (globalItem != null && globalItem.Versions.Count > 0 && string.IsNullOrEmpty(globalItem["IsDiscontinued"]))
                                        {
                                            productItemList.Add(productItemId);
                                            productCount++;
                                        }
                                    }
                                }
                            }
                            #endregion

                            using (new Sitecore.SecurityModel.SecurityDisabler())
                            {
                                logger["debug"].Log("Editing item started");
                                newItem.Editing.BeginEdit();

                                logger["debug"].Log("Editing Description Field");
                                string newvalue = ProcessMediaInRichText(description);
                                newItem["Description"] = StringHtmlExtensions.RemoveAttributes(newvalue, new string[] { "class", "style", "width", "height" });

                                //logger["debug"].Log("Editing Products Field");
                                //newItem["Products"] = string.Join("|", productItemList);

                                logger["debug"].Log("Editing Menu Title Field");
                                newItem["MenuTitle"] = familyName;

                                logger["debug"].Log("Editing Meta Title Field");
                                newItem["MetaTitle"] = familyName;

                                logger["debug"].Log("Editing Page Title Field");
                                newItem["PageTitle"] = familyName;

                                newItem.Editing.EndEdit();
                                logger["debug"].Log("Editing End");
                            }                           
                        }
                        catch (Exception ex)
                        {
                            logger["error"].Log(DateTime.Now.ToString());
                            logger["error"].Log("Error occured while creating item {0} under {1}", itemName, parentItem.Paths.Path);
                            logger["error"].Log(ex.Message);
                            logger["error"].Log(ex.StackTrace);
                        }
                        logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, productFamilyID, newItem.ID.ToString(), newItem.Paths.Path, legacyUrl);
                    }
                    catch (Exception ex)
                    {
                        logger["error"].Log("{0}", DateTime.Now.ToString());
                        logger["error"].Log("{0}", ex.Message);
                        logger["error"].Log("{0}", ex.StackTrace);
                    }
                }
                if (logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    logger["debug"].Log("-------------------------------------------------");
                    logger["debug"].Log("Time taken:{0}", duration);
                    logger["debug"].Log("Number of Products added : {0}", productCount);
                    logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)")]
        public string ProcessMediaInRichText(string htmlSource)
        {
            List<string> imgUrls = FetchLinksFromSource(htmlSource);
            string html = htmlSource;
            if (imgUrls.Count > 0)
            {
                imgUrls = imgUrls.Distinct().ToList();//.Where(x => !x.StartsWith("http")).ToList();

                foreach (string url in imgUrls)
                {
                    //MediaItem image = ImageFields(null, string.Empty, false, url,string.Empty);
                    MediaItem image = null;
                    if (url.StartsWith("http"))
                    {
                        image = GetImageItem(url, string.Empty);
                    }
                    else
                    {
                        image = ImageFields(null, string.Empty, false, url, string.Empty);
                    }
                    if (image == null)
                        continue;
                    string newUrl = "~/media/" + image.ID.ToShortID() + ".ashx";
                    html = html.Replace(url, newUrl);
                }
            }

            return html;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "isproduct"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "richtextimage")]
        protected MediaItem ImageFields(XElement element, string itemName, bool isproduct, string richtextimage, string altText)
        {
            string elementPath = string.Empty;
            if (element != null)
            {
                elementPath = element.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                elementPath = HttpUtility.UrlDecode(elementPath);
            }
            else if (!string.IsNullOrEmpty(richtextimage))
            {
                elementPath = HttpUtility.UrlDecode(richtextimage);
            }

            string tsFilePath = workAreaPath.Replace("/templatedata/", "") + elementPath;

            elementPath = isproduct ? "Images/Products/" + itemName + "/" + elementPath.Split('/').Last() : elementPath;

            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/" + path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            FileService imageFS = new FileService();
            var bytes = imageFS.GetFileContent(tsFilePath);
            var mediaPath = elementPath.Split('/');
            var mediaItemName = mediaPath.Last();
            if (bytes != null)
            {
                var stream = new MemoryStream(bytes);
                var bw = new BinaryWriter(new FileStream(localPath + mediaItemName, FileMode.Append, FileAccess.Write));
                bw.Write(bytes);
                bw.Close();
            }
            else
            {
                return null;
            }

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public MediaItem CreateMediaItem(string mediaUrl, string localMediaPath, string altText)
        {
            MediaItem mediaItem = null;
            MultiLogger logger;
            using (logger = new MultiLogger())
            {
                logger.Add("image", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog_Products.txt");
                logger.Add("image-error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog_Error.txt");
                logger["image"].Log("{0}	Started uploading image", DateTime.Now.ToString());
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaPath = mediaUrl.Split('/');
                    var mediaFolderID = CreateMediaFolders(mediaPath);
                    var mediaFolder = master.GetItem(mediaFolderID);
                    var mediaItemName = mediaPath.Last();

                    FileInfo file = new FileInfo(localMediaPath + mediaItemName);
                    if (file.Exists)
                    {
                        var filename = GetItemName(mediaItemName.Split('.')[0]);
                        filename = filename.Replace(".", "_");
                        var options = new Sitecore.Resources.Media.MediaCreatorOptions();
                        options.Database = master;
                        options.Versioned = false;
                        options.Destination = mediaFolder.Paths.FullPath + "/" + filename;
                        options.IncludeExtensionInItemName = false;
                        options.AlternateText = string.IsNullOrEmpty(altText) ? mediaItemName : altText;
                        options.FileBased = Sitecore.Configuration.Settings.Media.UploadAsFiles;
                        var mediaCreator = new Sitecore.Resources.Media.MediaCreator();
                        try
                        {
                            mediaItem = mediaCreator.CreateFromFile(file.FullName, options);
                            logger["image"].Log("{0}	New image created : {1}", DateTime.Now.ToString(), mediaItem.Path);
                            logger["image"].Log("Image Size {0} bytes", mediaItem.Size.ToString());
                            imageCount++;
                        }
                        catch (Exception ex)
                        {
                            logger["image-error"].Log(DateTime.Now.ToString() + "	Error while creating image");
                            logger["image-error"].Log("Error while processing Mediaurl : {0}", mediaUrl);
                            logger["image-error"].Log(ex.Message);
                            logger["image-error"].Log(ex.StackTrace);
                            //Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return mediaItem;
        }

        public ID CreateMediaFolders(string[] mediaPath)
        {
            Item mediaFolder = null;
            string mediaLibraryPath = "/sitecore/media library";
            foreach (string path in mediaPath)
            {
                if (!path.Contains('.') && !string.IsNullOrWhiteSpace(path))
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaFolderTemplate = master.Templates["System/Media/Media folder"];

                    var mediaLibraryItem = master.GetItem(mediaLibraryPath);
                    mediaLibraryPath = mediaLibraryPath + "/" + GetItemName(path);
                    mediaFolder = master.GetItem(mediaLibraryPath);

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (mediaFolder == null)
                        {
                            try
                            {
                                mediaFolder = mediaLibraryItem.Add(GetItemName(path), mediaFolderTemplate);
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return mediaFolder.ID;
        }

        protected MediaItem GetImageItem(string url, string altText)
        {
            var mediaPath = url.Split('/');
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(url, localPath + mediaPath.Last());
                }
                catch (Exception ex)
                {
                }
            }

            string host = new Uri(url).GetLeftPart(UriPartial.Authority);
            string elementPath = url.Replace(host, "");

            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/" + path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }
    }
}