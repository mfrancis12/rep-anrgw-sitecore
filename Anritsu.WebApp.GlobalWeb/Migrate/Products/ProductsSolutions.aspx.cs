﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using ContentService;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Excel;
using System.Text.RegularExpressions;

namespace Anritsu.WebApp.GlobalWeb.Migrate.Products
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
    public partial class ProductsSolutions : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");

        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        private string localPath = Sitecore.Configuration.Settings.GetSetting("ImageDownloadPath");
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");

        List<string> lstSelectedTSFields = new List<string>();
        List<string> lstSelectedSCFields = new List<string>();

        MultiLogger _logger;
        int itemCount = 0, versionCount = 0, overwriteCount = 0;
        int imageCount = 0, reusedImages = 0;

        private Dictionary<int, string> specIds = new Dictionary<int, string>
        {
            {5,"{30D36FFD-5BB5-418E-B8CF-24DBF4B55B1E}"},
            {8,"{994A30D8-527D-476A-8E12-3C79820F75C6}"},
            {9,"{2CC96A6E-A4CB-459C-9E90-FDAFB9EDC356}"},
            {12,"{CC30F76E-25D9-4289-B578-03B4E6D3C46A}"},
            {14,"{E9913612-567C-4B77-91F0-59C9992386FC}"},
            {27,"{BE60A9BC-7431-4637-8BF3-C3AC471D6422}"},
            {29,"{2E4E0F54-E1A4-48A4-B069-A5A6522A51C5}"},
            {36,"{CCAE2D99-D35B-45C2-B2F6-D392DEA959F7}"},
            {39,"{59AAB5AA-76E4-4B3C-919A-0E40959E06C8}"},
            {45,"{5DE32398-B9BA-489D-81DD-02C3E88D3F06}"},
            {46,"{3F02B186-0187-4527-8859-232FDC8BC436}"},
            {58,"{4F0DF4C8-9A6F-4BE3-9042-79412F43C173}"},
            {59,"{EA8BCE96-AE70-4190-A699-96FA8ABDA70E}"},
            {60,"{9F3C7FAC-D261-4342-B03A-AC89D1B40348}"},
            {61,"{2DB21D35-D8EC-4830-85EE-F5442DE6D6A4}"},
            {62,"{7E9D4957-AA3F-4836-8794-C76DD3668948}"},
            {64,"{138BA033-B2E2-456F-BC3C-1A84AAF76093}"},
            {66,"{D5B3BD7C-0370-4DA4-8EE0-00EADC9CCCB0}"},
            {67,"{5D489F33-8CD7-4984-A98C-8F8EE247F94B}"},
            {68,"{D90BA254-7F68-483D-B52B-56929EFE2708}"},
            {70,"{1AF8B562-2B2F-4908-8B5A-C864C372646B}"},
            {75,"{B9B276EE-9847-4432-992D-9A68C75D97DE}"},
            {76,"{1EB01AEC-56FC-490B-8916-8C81442D0344}"},
            {77,"{4057C2F3-E05C-48B4-BA75-76A66F21728E}"},
            {79,"{5FB77151-3505-4132-BBD3-915FD3D61787}"},
            {84,"{C50796CA-ABFF-450A-8EBE-163D0A18AF40}"},
            {86,"{1BFDFBC2-A97F-4714-A8F2-FA3AE6DC65B1}"},
            {87,"{324EDFCF-0FCA-4529-95DE-791E6AC044A0}"},
            {89,"{4041697B-EEB7-444B-AEE6-30209C639F16}"},
            {90,"{A047775C-CEDA-49F4-B44E-EF54DC4E2914}"},
            {91,"{C67C3F49-3225-4C46-BC9B-AAA611A9E59F}"},
            {93,"{42FD97F9-D01D-4763-BC90-DF87CFFE5791}"},
            {97,"{7314C1E5-2DB3-4083-8951-0DF6D3E67912}"},
            {98,"{89524C74-EFC1-4EE3-8921-36AA24562422}"},
            {99,"{47F2B515-D83D-445F-83F8-FE4B1EB0D0DC}"},
            {103,"{23C7A875-C830-4F15-AD45-0E20A60E0DE7}"},
            {104,"{E965E83E-2930-467F-8754-D050D71AFB0E}"},
            {105,"{B137EE26-06E2-4B25-87FA-44BF2915C6BC}"},
            {106,"{A32972DA-1D60-4AB6-B165-EE8C47646FA1}"},
            {107,"{1CDE0B55-C18E-4ED9-B69C-F6261863396F}"},
            {109,"{45A48527-0978-40D1-86BD-8CC3B84159FF}"},
            {112,"{DAC4FBFF-44DF-4443-840A-37A4DB0AE464}"},
            {119,"{D56ACBF6-9D05-48C7-9E03-932888F095F8}"},
            {125,"{C99AFAC0-5AC6-430B-8CA2-F646B954B17A}"},
            {126,"{737EA8E6-55DB-4347-82DA-93EF1917E2C8}"},
            {128,"{6CFD21B2-FAB3-4D10-A531-0FA12859CD44}"},
            {129,"{126D645B-15E2-4C9A-9FF6-DB5A589BC09B}"},
            {130,"{809A2585-0F7D-45A9-BFFE-EEC8A7B30755}"},
            {131,"{2CBE6005-84FA-4D8A-8C64-FD0836A8129D}"},
            {134,"{53F9F849-0F77-406C-BC9B-9BB981D76CA5}"},
            {137,"{9BC7B240-492B-4663-9EC9-E9DABF271744}"},
            {138,"{E9693103-4F7E-4A31-9F08-76DFD8C5ED7A}"},
            {139,"{71FCC058-20B6-407D-ABA7-5DB7BE854547}"},
            {140,"{19C75E48-7EFC-4C78-8F4E-847537B88299}"},
            {141,"{87C5CB14-FAB9-47C4-BFC9-455200E9FDED}"},
            {142,"{4C63685B-F919-4BF5-9224-AB118D846790}"},
            {144,"{9FC642BB-042A-4732-8C30-B22087B629FB}"},
            {145,"{B0673F73-419F-47D4-893E-1DB256A54B3F}"},
            {146,"{DFFFB630-97C4-467D-AB97-93CED1F09222}"},
            {147,"{F726DBB3-F4D6-4482-BA68-21B1E5FADEBC}"},
            {148,"{DE26FA61-FA04-4DCD-A098-55F84A61C7ED}"},
            {149,"{C166A0EA-3577-4012-B579-7577561D2273}"},
            {151,"{8845E541-C965-4EC7-9A0C-1D7840597219}"},
            {152,"{1ACDF657-72BD-40E5-B304-F91A6F337326}"},
            {155,"{A005AFB6-5C1D-4DA4-B3A6-E53718B0C61F}"},
            {156,"{4079A8F1-2905-41B6-A535-8C406E955F78}"},
            {157,"{4BB8BC9E-489E-4D6D-B848-FDA648947994}"},
            {158,"{9AC02796-F45B-4E72-A6E7-83CD4D945ED5}"},
            {159,"{70C7E7F4-509D-4109-9027-73E28F516DE1}"},
            {160,"{F5CE1B68-1601-4BEB-9BE1-6488EE15B8FF}"},
            {161,"{098ED189-9433-4431-8AD3-2541AC79BD6B}"},
            {162,"{244D58B6-D7DA-47FF-BEBB-42DB53F4E89F}"},
            {163,"{6D682BB6-C6A5-4AF0-B9AD-761BF6559E92}"},
            {164,"{633D3A2B-3B3A-4FD5-B4A9-A70B18EE8303}"},
            {165,"{ACECEDF7-0599-47D4-8C2D-B85FCC1B49C0}"},
            {166,"{DB4697EE-CEC0-4CDA-BE94-0DCA50480BB0}"}
        };

        private Dictionary<string, string> customTabTitles = new Dictionary<string, string>
        {
            {"CDMA2000®規格","CDMA2000® standard"},
            {"PC要件","PC Requirements"},
            {"TD-SCDMA/GSM規格","TD-SCDMA / GSM standard"},
            {"VSG波形","VSG Waveforms"},
            {"W-CDMA/GSM規格","W-CDMA / GSM standard"},
            {"アクセサリ","Accessories"},
            {"アジア－太平洋","Asia-Pacific"},
            {"アプリケーション","Applications"},
            {"アンテナ","Antennas"},
            {"イメージ","Images"},
            {"オプション","Options"},
            {"ソフトウェア","Software"},
            {"ソフトウエア","Software"},
            {"ソリューション","Solutions"},
            {"ダウンロード","Downloads"},
            {"ビデオ","Video"},
            {"マップ","Maps"},
            {"ラインナップ","Lineup"},
            {"中南米","Latin America"},
            {"互換性","Compatibility"},
            {"仕様","Specifications"},
            {"北米","North America"},
            {"対応測定器","Instruments"},
            {"校正キット","Cal Kits"},
            {"機能","Functions"},
            {"機能/仕様","Functions / Specifications"},
            {"特性","Characteristics"},
            {"用途","Applications"},
            {"画像","Images"},
            {"規格","Specifications"}
        };

        private Dictionary<string, string> customTabTitlesKR = new Dictionary<string, string>
        {
            {"MST(이 제품만 해당)","MST (only)"},
            {"PC 요구사항","PC Requirements"},
            {"USB 로더","USB Loader"},
            {"기기","Instruments"},
            {"다운로드","Downloads"},
            {"라틴 아메리카","Latin America"},
            {"레거시","Legacy"},
            {"맵","Maps"},
            {"부속품","Accessories"},
            {"북미","North America"},
            {"분야","Applications"},
            {"사양","Specifications"},
            {"소프트웨어","Software"},
            {"솔루션","Solutions"},
            {"아시아 태평양","Asia-Pacific"},
            {"안테나","Antennas"},
            {"액세서리","Accessories"},
            {"업데이트 방법","Update Methods"},
            {"옵션","Options"},
            {"용도","Applications"},
            {"유틸리티","Utilities"},
            {"응용","Applications"},
            {"응용 분야","Applications"},
            {"이미지","Images"},
            {"적용","Applications"},
            {"적용 분야","Applications"},
            {"적합성","Compatibility"},
            {"코드 로더","Code loader"},
            {"특징","Characteristics"},
            {"호환성","Compatibility"}
        };

        private Dictionary<string, string> customTabTitlesCN = new Dictionary<string, string>
        {
            {"MST（仅限）","MST (only)"},
            {"PC 配置要求","PC Requirements"},
            {"USB 加载器","USB Loader"},
            {"下载","Downloads"},
            {"亚太地区","Asia-Pacific"},
            {"产品线","Lineup"},
            {"代码加载器","Code Loader"},
            {"仪器","Instruments"},
            {"兼容性","Compatibility"},
            {"功能","Functions"},
            {"北美","North America"},
            {"图像","Images"},
            {"地图","Applications"},
            {"型号、选件","Models/Options"},
            {"天线","Antennas"},
            {"实用程序","Utilities"},
            {"常见问题","FAQs"},
            {"应用","Applications"},
            {"拉丁美洲","Latin America"},
            {"指标","Specifications"},
            {"接口","Interfaces"},
            {"旧系统","Legacy"},
            {"更新方法","Update Methods"},
            {"校准件","Cal Kits"},
            {"欧洲、中东和非洲地区","EMEA"},
            {"特征","Specifications"},
            {"矢量信号源波形","VSG Waveforms"},
            {"规格","Specifications"},
            {"解决方案","Solutions"},
            {"软件","Software"},
            {"选件","Options"},
            {"选件项","Options"},
            {"选项","Options"},
            {"附件","Accessories"},
            {"高速测试","High Speed Msmt"}
        };

        private Dictionary<string, string> customTabTitlesTW = new Dictionary<string, string>
        {
            {"MST（僅限）","MST (only)"},
            {"PC 要求","PC Requirements"},
            {"USB 載入儀","USB Loader"},
            {"VSG 波形","VSG Waveforms"},
            {"下載","Downloads"},
            {"亞太地區","Asia-Pacific"},
            {"儀器","Instruments"},
            {"北美","North America"},
            {"圖像","Images"},
            {"地圖","Maps"},
            {"天線","Antennas"},
            {"常見問題","FAQs"},
            {"應用","Applications"},
            {"拉丁美洲","Latin America"},
            {"更新方法","Update Methods"},
            {"更新說明","Update Methods"},
            {"校準元件","Cal Kits"},
            {"歐洲、中東及非洲","EMEA"},
            {"產品應用","Applications"},
            {"相容性","Compatibility"},
            {"編碼載入儀","Code Loader"},
            {"規格","Specifications"},
            {"軟體","Software"},
            {"軟體下載","Downloads"},
            {"選件","Options"},
            {"選配","Options"},
            {"選項","Options"},
            {"配件","Accessories"}
        };

        private Dictionary<string, string> customTabTitlesRU = new Dictionary<string, string>
        {
            {"MST (только)","MST (only)"},
            {"Specificiations","Specifications"},
            {"Антенны","Antennas"},
            {"Варианты","options"},
            {"Измерительные приборы","Instruments"},
            {"Области применения","Applications"},
            {"Опции","options"},
            {"Применение","application"},
            {"Программное обеспечение","software"},
            {"Служебные программы","Utilities"},
            {"Совместимость","Compatibility"},
            {"Способы обновления","Update Methods"},
            {"Технические характеристики","Specifications"},
            {"Требования к компьютеру","PC Requirements"},
            {"Устаревшие модели","Legacy"},
            {"Файлы для загрузки","Downloads"},
            {"ЧаВо","FAQs"}
        };

        private Dictionary<string, string> descriptionTabTitle = new Dictionary<string, string>
        {
            {"ja-JP","概要"},
            {"ko-KR","설명"},
            {"zh-CN","描述"},
            {"zh-TW","描述"},
            {"ru-RU","Описание"}
        };

        private Dictionary<string, string> featuresTitle = new Dictionary<string, string>
        {
            {"ja-JP","特長"},
            {"ko-KR","기능"},
            {"zh-CN","特征"},
            {"zh-TW","功能"},
            {"ru-RU","Свойства"}
        };       

        private Dictionary<int, string> cultureID = new Dictionary<int, string>
        {
            {1,"en-US"},
            {2,"ja-JP"},
            {3,"en-GB"},
            {4,"en-AU"},
            {6,"zh-CN"},
            {7,"ko-KR"},
            {10,"zh-TW"},
            {12,"aa-GW"},
            {14,"ru-RU"},
            {15,"en-IN"}
        };

        private IEnumerable<TemplateItem> _sitecoreTemplates = null;

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot,StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        protected void ProductCategory_Click(object sender, EventArgs e)
        {
            GetFieldMappings();
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger["meta"].Log("-------------------------------------------------------------------"); 
                _logger["meta"].Log("Teamsite Path	CategoryID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);

                    foreach (string filePath in fileList)
                    {
                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);

                        var xDoc = XDocument.Load(bufferStream);
                        var nodes = xDoc.Root.Descendants();

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                        lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                        string legacyCategoryId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;
                        string legacyUrl = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;

                        string[] array = filePath.Split('/');
                        string categoryItemName = ItemUtil.ProposeValidItemName(array[array.Length - 2]);
                        string scPath = txtSCPath.Text;

                        Item parentItem = _master.GetItem(scPath, Language.Parse(Languages.SelectedValue));

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            _logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        _logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                        Item categoryItem = CreateItem(parentItem, _logger, categoryItemName, elementList,false,string.Empty);

                        if (categoryItem != null)
                        {
                            _logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, legacyCategoryId, categoryItem.ID.ToString(), categoryItem.Paths.Path, legacyUrl);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
        }

        protected void ProductSubcategory_Click(object sender, EventArgs e)
        {
            GetFieldMappings();
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	Sub-CategoryID	ItemID	ItemPath");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);

                    foreach (string filePath in fileList)
                    {
                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);

                        var xDoc = XDocument.Load(bufferStream);
                        var nodes = xDoc.Root.Descendants();

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                        lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                        string legacyId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                            x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;

                        string[] array = filePath.Split('/');
                        string subcategoryItemName = GetItemName(array[array.Length - 2]);
                        string categoryItemName = GetItemName(array[array.Length - 3]);
                        string scPath = txtSCPath.Text + "/" + categoryItemName;

                        Item parentItem = _master.GetItem(scPath, Language.Parse(Languages.SelectedValue));

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            _logger["error"].Log("Sitecore path is empty or No item matched the path");
                            continue;
                        }

                        _logger["debug"].Log("Parent Item Path:{0}", parentItem.Paths.Path);

                        Item subCategoryItem = CreateItem(parentItem, _logger, subcategoryItemName, elementList, false,string.Empty);

                        using (new Sitecore.SecurityModel.SecurityDisabler())
                        {
                            subCategoryItem.Editing.BeginEdit();

                            _logger["debug"].Log("Editing field: Redirect Link");
                            LinkField redirectLinkField = (LinkField)subCategoryItem.Fields["RedirectLink"];
                            redirectLinkField.TargetID = subCategoryItem.Parent.ID;
                            redirectLinkField.LinkType = "internal";

                            subCategoryItem.Editing.EndEdit();
                        }

                        if (subCategoryItem != null)
                        {
                            _logger["meta"].Log("{0}	{1}	{2}	{3}", filePath, legacyId, subCategoryItem.ID.ToString(), subCategoryItem.Paths.Path);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected void ProductFamily_Click(object sender, EventArgs e)
        {
            GetFieldMappings();

            string specPath = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");
            string specSheet = Sitecore.Configuration.Settings.GetSetting("SpecificationExcelSheetName");
            string extension = specPath.Split('.')[1];

            DataTable specData = GetDataFromFile(specPath, specSheet);

            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	FamilyID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string[] array = filePath.Split('/');

                            if (array[array.Length - 1].Contains("test"))
                            {
                                _logger["error"].Log("Skipping test item ");
                                continue;
                            }

                            string familyItemName = GetItemName(array[array.Length - 2]);
                            string subcategoryItemName = GetItemName(array[array.Length - 3]);
                            string categoryItemName = GetItemName(array[array.Length - 4]);
                            string scPath = txtSCPath.Text + "/" + categoryItemName + "/" + subcategoryItemName;

                            IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                            string legacyId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;

                            string legacyUrl = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;

                            var specNodes = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Specifications1"));



                            Item parentItem = _master.GetItem(scPath, Language.Parse(Languages.SelectedValue));

                            if (string.IsNullOrEmpty(scPath) || parentItem == null)
                            {
                                _logger["error"].Log("Sitecore path is empty or No item matched the path");
                                continue;
                            }

                            _logger["debug"].Log("Parent Item Path:{0}", parentItem.Paths.Path);

                            Item familyItem = CreateItem(parentItem, _logger, familyItemName, elementList, false,string.Empty);

                            using (new Sitecore.SecurityModel.SecurityDisabler())
                            {
                                familyItem.Editing.BeginEdit();

                                _logger["debug"].Log("Editing field: Specifications");
                                var specLegacyIds = specNodes.Descendants().Where(x => x.Name.LocalName.Equals("value")).Select(y => y.Value).ToArray();
                                var dataRows = from DataRow row in specData.Rows
                                               where specLegacyIds.Contains(row["SpecificationID"].ToString())
                                               select row;

                                familyItem["Specifications"] = string.Join("|", dataRows.Select(x => x["ItemID"].ToString()));
                                _logger["debug"].Log("specification legacy Id: {0}", string.Join(",", specLegacyIds));
                                _logger["debug"].Log("specification Id: {0}", string.Join("|", familyItem["Specifications"]));

                                familyItem.Editing.EndEdit();
                            }

                            if (familyItem != null)
                            {
                                _logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, legacyId, familyItem.ID.ToString(), familyItem.Paths.Path, legacyUrl);
                            }
                        }
                        catch(Exception ex)
                        {
                            _logger["error"].Log(DateTime.Now.ToString());
                            _logger["error"].Log(ex.Message);
                            _logger["error"].Log(ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        protected void GetFields_Click(object sender, EventArgs e)
        {
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    if (!string.IsNullOrEmpty(txtTSDCRPath.Text))
                    {
                        List<string[]> list = new List<string[]>();

                        string dataCaptureFile = txtTSDCRPath.Text.Split(new string[] { "/data/" }, StringSplitOptions.None)[0] + "/datacapture.cfg";
                        list = ParseXml(dataCaptureFile, list);

                        if (list == null || list.Count == 0)
                        {
                            return;
                        }

                        repeaterTSFields.DataSource = list;
                        repeaterTSFields.DataBind();
                    }

                    var languages = LanguageManager.GetLanguages(_master);
                    foreach (var language in languages)
                    {
                        Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                    }
                    Languages.Items.Insert(0, new ListItem("--select--", "0"));

                    panelSC.Visible = true;
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log("Error occured while clicking the Get Fields Button.");
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<string[]> ParseXml(string filePath, List<string[]> list)
        {
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileService fs = new FileService();
                    Byte[] fileByteContent = fs.GetFileContent(filePath);

                    if (fileByteContent == null)
                        return null;

                    string[] fieldTypes = new string[] { "text", "browser", "textarea", "select", "checkbox", "radio", "readonly" };

                    MemoryStream bufferStream = new MemoryStream(fileByteContent);
                    XDocument xDoc = XDocument.Load(bufferStream);

                    foreach (var element in xDoc.Root.Descendants())
                    {
                        if (element.Descendants().Where(x => x.Name.LocalName.Equals("hidden")).Count() > 0)
                            continue;

                        if (element.Name.LocalName.Equals("container") && element.Attribute("min") != null && element.Attribute("max") != null)
                        {
                            var ancestors = element.Ancestors();
                            string[] replicantAnc = ancestors.Where(x => x.Name.LocalName.Equals("container") && x.Attribute("min") != null && x.Attribute("max") != null).Select(x => x.Name.LocalName).ToArray();
                            if (replicantAnc.Count() == 0)
                            {
                                string[] array = new string[4];

                                array[0] = element.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("label")).Value;
                                array[1] = element.Attribute("name").Value;
                                array[2] = "container";
                                array[3] = filePath;
                                list.Add(array);
                            }
                        }
                        else if (element.Name.LocalName.Equals("item"))
                        {
                            var ancestors = element.Ancestors();
                            string[] replicantAnc = ancestors.Where(x => x.Name.LocalName.Equals("container") && x.Attribute("min") != null && x.Attribute("max") != null).Select(x => x.Name.LocalName).ToArray();
                            if (replicantAnc.Count() == 0)
                            {
                                string[] array = new string[4];
                                var labelElement = element.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("label"));
                                array[0] = (labelElement == null) ? element.Attribute("name").Value : labelElement.Value;
                                array[1] = element.Attribute("name").Value;

                                string fieldType = element.Descendants().Where(x => fieldTypes.Contains(x.Name.LocalName)).FirstOrDefault().Name.LocalName;
                                var dateField = element.Descendants().Where(x => x.Name.LocalName.Equals("text") && x.Attribute("validation-regex") != null).FirstOrDefault();
                                if (dateField != null)
                                {
                                    if (dateField.Attribute("validation-regex").Value.Equals("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}"))
                                    {
                                        fieldType = "date";
                                    }
                                }
                                array[2] = fieldType;
                                array[3] = string.Empty;
                                list.Add(array);
                            }
                        }
                        else if (element.Name.LocalName.Equals("inline") && !element.Parent.Name.LocalName.Equals("container"))
                        {
                            string value = element.Attribute("command").Value.Split(' ').Where(val => val.Contains(".xml")).FirstOrDefault();

                            if (!string.IsNullOrEmpty(value))
                            {
                                value = value.Replace(@"\", @"/");
                                string newFilePath = value.Replace("/templatedata/", "");
                                ParseXml(workAreaPath + newFilePath, list);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log("Error occured while Parsing the XML.");
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
                return list;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Teamsite")]
        protected void TeamsiteFields_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string[] tsFieldInfo = e.Item.DataItem as string[];

                Label label = e.Item.FindControl("tsFieldName") as Label;
                HiddenField hidden = e.Item.FindControl("tsFieldID") as HiddenField;
                DropDownList scFields = e.Item.FindControl("scFields") as DropDownList;

                label.Text = tsFieldInfo[0];
                hidden.Value = tsFieldInfo[1];

                scFields.DataSource = GetSCFields(tsFieldInfo[2], SCTemplates.SelectedValue);
                scFields.DataTextField = "Text";
                scFields.DataValueField = "Value";
                scFields.DataBind();

                scFields.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected List<ListItem> GetSCFields(string fieldType, string selected)
        {
            List<ListItem> fields = new List<ListItem>();
            if (selected == null)
                return null;
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    var template = _master.Templates[new ID(selected)];

                    if (template == null || template.Fields.Count() == 0)
                        return null;

                    if (fieldType.Equals("text"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && field.Type.Equals("Single-Line Text")))
                        {

                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("textarea"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && ((field.Type.Equals("Multi-Line Text") || field.Type.Equals("Rich Text") || field.Type.Equals("memo")))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("browser"))
                    {
                        var filteredFields = template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("General Link") || field.Type.Equals("Image")));

                        foreach (var field in filteredFields)
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("select"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Checklist") || field.Type.Equals("Droplist") || field.Type.Equals("DropLink") || field.Type.Equals("Droptree"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("checkbox") || fieldType.Equals("radio"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Checkbox"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("container"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Multilist") || field.Type.Equals("Treelist") || field.Type.Equals("TreelistEx") || field.Type.Equals("Multilist with Search"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("date"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Date"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
                
            }
            return fields;
        }

        protected void GetFieldMappings()
        {
            foreach (RepeaterItem rptItem in repeaterTSFields.Items)
            {
                if (rptItem.ItemType == ListItemType.Item || rptItem.ItemType == ListItemType.AlternatingItem)
                {
                    var label = rptItem.FindControl("tsFieldName") as Label;
                    var hidden = rptItem.FindControl("tsFieldID") as HiddenField;
                    var scFields = rptItem.FindControl("scFields") as DropDownList;

                    string teamSiteField = hidden.Value;
                    string sitecoreField = scFields.SelectedItem.Text;
                    string scFieldValue = scFields.SelectedValue;
                    int result;
                    bool fieldNotSelected = int.TryParse(scFieldValue, out result);

                    if (!fieldNotSelected)
                    {
                        lstSelectedTSFields.Add(teamSiteField);
                        lstSelectedSCFields.Add(sitecoreField);
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "dest"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected Item CreateItem(Item dest, MultiLogger logger, string itemName, IEnumerable<XElement> elementList, bool isProduct, string altText)
        {
            Item newItem = null;
            
            try
            {
                List<string> eleList = new List<string>();
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Language language = Language.Parse(Languages.SelectedValue);
                    newItem = _master.GetItem(dest.Paths.Path + "/" + itemName, language);

                    if (newItem != null)
                    {
                        bool hasLangVersion = HasLanguageVersion(newItem, Languages.SelectedValue);
                        if (!hasLangVersion)
                        {
                            newItem = newItem.Versions.AddVersion();
                            logger["debug"].Log("Added new {1} version for Item:{0}", newItem.Paths.Path, Languages.SelectedItem.Text);
                            versionCount++;
                        }
                        else
                        {
                            overwriteCount++;
                            logger["debug"].Log("Overwriting the item: {0} in {1} language", newItem.Paths.Path, Languages.SelectedItem.Text);
                        }
                    }
                    else
                    {
                        newItem = dest.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                        logger["debug"].Log("New item added {0} in {1}", newItem.Paths.Path, newItem.Language.Name);
                        itemCount++;
                    }

                    foreach (string item in lstSelectedTSFields)
                    {
                        foreach (var element in elementList)
                        {
                            if (element.Attribute("name").Value.Equals(item))
                            {
                                if (!eleList.Contains(element.Attribute("name").Value))
                                {
                                    if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Image")
                                    {
                                        MediaItem mediaImage = ImageFields(element,itemName,isProduct,string.Empty,altText);
                                        if (mediaImage != null)
                                        {
                                            newItem.Editing.BeginEdit();

                                            logger["debug"].Log("Editing image field : {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                            Sitecore.Data.Fields.ImageField iconImage = newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]];
                                            iconImage.Clear();
                                            iconImage.MediaID = mediaImage.ID;
                                            if (!string.IsNullOrEmpty(altText))
                                            {
                                                iconImage.Alt = altText;
                                            }

                                            logger["debug"].Log("Image Assigned {0}", iconImage.MediaItem.Paths.Path);

                                            newItem.Editing.EndEdit();

                                            logger["debug"].Log("Editing image field end");
                                            eleList.Add(element.Attribute("name").Value);
                                        }
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Date")
                                    {
                                        string dateValue = element.Value;
                                        string isoDate = Sitecore.DateUtil.ToIsoDate(DateTime.Parse(dateValue));

                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = isoDate;

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Single-Line Text"
                                        || newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Multi-Line Text"
                                        || newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "memo")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = Sitecore.StringUtil.RemoveTags(value);

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Rich Text")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;
                                        string newvalue = ProcessMediaInRichText(value);
                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = StringHtmlExtensions.RemoveAttributes(newvalue, new string[] { "class", "style","width","height" });

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Checkbox")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = "0";
                                        var checkboxfields = element.Descendants().Where(x => x.Name.LocalName.Equals("value"));
                                        
                                        if(checkboxfields != null && checkboxfields.Count() > 0)
                                        {
                                             value = element.Value;
                                        }

                                        if(value.Equals("1"))
                                            newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = "1";
                                        else
                                            newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = string.Empty;

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = Sitecore.StringUtil.RemoveTags(value);

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger["error"].Log(DateTime.Now.ToString());
                logger["error"].Log("Error occured while creating item {0} under {1}", itemName, dest.Paths.Path);
                logger["error"].Log(ex.Message);
                logger["error"].Log(ex.StackTrace);
            }

            return newItem;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "isproduct"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "richtextimage")]
        protected MediaItem ImageFields(XElement element,string itemName,bool isproduct, string richtextimage,string altText)
        {
            string elementPath = string.Empty;
            if(element != null)
            {
                elementPath = element.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                elementPath = HttpUtility.UrlDecode(elementPath);
            } else if(!string.IsNullOrEmpty(richtextimage))
            {
                elementPath = HttpUtility.UrlDecode(richtextimage);
            }
            
            string tsFilePath = workAreaPath.Replace("/templatedata/", "") + elementPath;

            elementPath = isproduct ? "Images/Products/" + itemName + "/" + elementPath.Split('/').Last() : elementPath;

            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/"+path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            FileService imageFS = new FileService();
            var bytes = imageFS.GetFileContent(tsFilePath);
            var mediaPath = elementPath.Split('/');
            var mediaItemName = mediaPath.Last();
            if (bytes != null)
            {
                var stream = new MemoryStream(bytes);
                var bw = new BinaryWriter(new FileStream(localPath + mediaItemName, FileMode.Append, FileAccess.Write));
                bw.Write(bytes);
                bw.Close();
            }
            else
            {
                return null;
            }            
            
            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public MediaItem CreateMediaItem(string mediaUrl, string localMediaPath,string altText)
        {
            MediaItem mediaItem = null;
            MultiLogger logger;
            using(logger = new MultiLogger())
            {
                logger.Add("image", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog.txt");
                logger["image"].Log("{0}	Started uploading image", DateTime.Now.ToString());
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
                    
                    var mediaPath = mediaUrl.Split('/');
                    var mediaFolderID = CreateMediaFolders(mediaPath);
                    var mediaFolder = master.GetItem(mediaFolderID);
                    var mediaItemName = mediaPath.Last();

                    FileInfo file = new FileInfo(localMediaPath + mediaItemName);
                    if (file.Exists)
                    {
                        var filename = GetItemName(mediaItemName.Split('.')[0]);
                        filename = filename.Replace(".", "_");
                        var options = new Sitecore.Resources.Media.MediaCreatorOptions();
                        options.Database = master;
                        options.Versioned = false;
                        options.Destination = mediaFolder.Paths.FullPath + "/" + filename;
                        options.IncludeExtensionInItemName = false;
                        options.AlternateText = string.IsNullOrEmpty(altText) ? mediaItemName : altText;
                        options.FileBased = Sitecore.Configuration.Settings.Media.UploadAsFiles;
                        var mediaCreator = new Sitecore.Resources.Media.MediaCreator();
                        try
                        {
                            mediaItem = mediaCreator.CreateFromFile(file.FullName, options);
                            logger["image"].Log("{0}	New image created : {1}" , DateTime.Now.ToString(), mediaItem.Path);
                            logger["image"].Log("Image Size {0} bytes", mediaItem.Size.ToString());
                            imageCount++;
                        }
                        catch (Exception ex)
                        {
                            logger["image"].Log(DateTime.Now.ToString() + "	Error while creating image");
                            logger["image"].Log(ex.Message);
                            logger["image"].Log(ex.StackTrace);
                            //Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
                return mediaItem;            
        }

        public ID CreateMediaFolders(string[] mediaPath)
        {
            Item mediaFolder = null;
            string mediaLibraryPath = "/sitecore/media library";
            foreach (string path in mediaPath)
            {
                if (!path.Contains('.') && !string.IsNullOrWhiteSpace(path))
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaFolderTemplate = master.Templates["System/Media/Media folder"];

                    var mediaLibraryItem = master.GetItem(mediaLibraryPath);
                    mediaLibraryPath = mediaLibraryPath + "/" + GetItemName(path);
                    mediaFolder = master.GetItem(mediaLibraryPath);

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (mediaFolder == null)
                        {
                            try
                            {
                                mediaFolder = mediaLibraryItem.Add(GetItemName(path), mediaFolderTemplate);
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return mediaFolder.ID;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1306:SetLocaleForDataTypes"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "Extension"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "isHDR"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sheetName")]
        private DataTable GetDataFromFile(string FilePath, string sheetName)
        {
            DataTable dt = new DataTable();
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileStream stream = File.Open(FilePath, FileMode.Open, FileAccess.Read);

                    //Reading from a OpenXml Excel file (2007 format; *.xlsx)                    
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                    //DataSet - Create column names from first row
                    excelReader.IsFirstRowAsColumnNames = true;

                    //DataSet - The result of each spreadsheet will be created in the result.Tables
                    DataSet result = excelReader.AsDataSet();

                    //6. Free resources (IExcelDataReader is IDisposable)
                    excelReader.Close();

                    dt = result.Tables[sheetName];
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToShortDateString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
            return dt;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA150I2:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger")]
        protected void Product_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            GetFieldMappings();
            string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");
                        
            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger.Add("replacementproducts", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_replacementproducts_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                _logger["debug"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("-------------------------------------------------------------------");
                _logger["meta"].Log("Teamsite Path	ProductID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    _logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string[] array = filePath.Split('/');

                            if (array[array.Length - 1].Contains("test"))
                            {
                                _logger["error"].Log("Skipping test item ");
                                continue;
                            }

                            string productItemName = GetItemName(array[array.Length - 1]);
                            string scPath = txtSCPath.Text;

                            IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                            string legacyProductId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;
                            string legacyUrl = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;
                            string ecoIcon = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Marker_Image")).FirstOrDefault().Value;
                            string features = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Features")).FirstOrDefault().Value;
                            //string modelName = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                            //                                   x.Attribute("name").Value.Equals("Model_Number")).FirstOrDefault().Value;
                            //string productItemName = GetItemName(modelName);
                            string altText = string.Empty;
                            var altNode = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("imageAltText"));

                            if (altNode != null && altNode.Count() > 0)
                            {
                                altText = altNode.FirstOrDefault().Value;
                            }                            

                            string isActive = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Active_Flag")).FirstOrDefault().Value;
                            string master = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("master")).FirstOrDefault().Value;
                            string globalizationId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("globalization_id")).FirstOrDefault().Value;

                            IEnumerable<XElement> masterNodes = null;
                            if (!master.Equals(globalizationId))
                            {
                                string currentCulture = string.Empty;
                                bool isculture = cultureID.TryGetValue(Convert.ToInt32(globalizationId), out currentCulture);
                                string masterCulture = string.Empty;
                                bool ismasterCulture = cultureID.TryGetValue(Convert.ToInt32(master), out masterCulture);

                                string masterFilePath = filePath.Replace(currentCulture, masterCulture);
                                var masterfileByteContent = fs.GetFileContent(masterFilePath);
                                var masterbufferStream = new MemoryStream(masterfileByteContent);

                                var masterDoc = XDocument.Load(masterbufferStream);
                                masterNodes = masterDoc.Root.Descendants();

                                isActive = masterNodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Active_Flag")).FirstOrDefault().Value;
                            }

                            string replacementProductId = string.Empty;
                            if (isActive.Equals("0"))
                            {
                                IEnumerable<XElement> replacementNode = null;
                                if (masterNodes != null)
                                {
                                    replacementNode = masterNodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                               x.Attribute("name").Value.Equals("Replacement_Pdt_Model_Number"));
                                }
                                else
                                {
                                    replacementNode = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                               x.Attribute("name").Value.Equals("Replacement_Pdt_Model_Number"));
                                }

                                if (replacementNode != null && replacementNode.Count() > 0)
                                {
                                    DataTable prodRegional = GetDataFromFile(path, "ProductRegional");
                                    if (prodRegional != null && prodRegional.Rows.Count > 0)
                                    {
                                        var oldProductIds = replacementNode.Select(x => x.Value).ToArray<string>();
                                        var productRows = (from DataRow row in prodRegional.Rows
                                                           where oldProductIds.Contains(row["ProductID"].ToString())
                                                           select row);
                                        if(productRows!=null && productRows.Count() > 0)
                                        { 
                                            replacementProductId = string.Join("|", productRows.Select(x => x["ItemId"].ToString()));
                                        }
                                        else
                                        {
                                            _logger["replacementproducts"].Log("{0}	{1}	{2}", legacyProductId, productItemName, string.Join(",", oldProductIds));
                                        }
                                    }
                                }
                            }                            

                            string masterLanguage = cultureID.ContainsKey(Convert.ToInt32(master)) ? cultureID[Convert.ToInt32(master)] : Languages.SelectedValue;
                            string prodFamilyIds = GetFamilyIds(legacyProductId,masterLanguage);
                            
                            var specificationNames = nodes.Where((x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Specification_Name")));
                            var specificationValues = nodes.Where((x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("Specification_Value")));

                            List<string> specNames = new List<string>();
                            List<string> specValues = new List<string>();
                            Dictionary<string, string> parameters = new Dictionary<string, string>();
                            string url = string.Empty;

                            if (specificationNames != null && specificationNames.Count() > 0
                                && specificationValues != null && specificationValues.Count() > 0)
                            {
                                foreach (XElement specName in specificationNames)
                                {
                                    if (specName.Descendants().Count() > 0)
                                    {
                                        specNames.Add(specName.Value);
                                    }
                                }

                                foreach (XElement specValue in specificationValues)
                                {
                                    if (specValue.Descendants().Count() > 0)
                                    {
                                        specValues.Add(specValue.Value);
                                    }
                                }

                                if (specNames.Count == specValues.Count)
                                {
                                    for (int i = 0; i < specNames.Count; i++)
                                    {
                                        if (!specIds.ContainsKey(Convert.ToInt32(specNames[i])))
                                            continue;
                                        string key = HttpUtility.UrlEncode(HttpUtility.HtmlDecode(specValues[i]));
                                        string value = HttpUtility.UrlEncode(specIds[Convert.ToInt32(specNames[i])]);
                                        parameters.Add(value, key);
                                    }
                                }

                                url = string.Join("&", parameters.Select(kvp => string.Format("{0}={1}", kvp.Value, kvp.Key)));
                            }

                            string[] statusIds = new string[] { "{F877AB82-57E5-4247-A513-764E719E8953}", "{1CD4BB35-29F5-4433-A15E-22DD0AE5ECEB}", "{8383A963-F531-430D-AAD1-175DC5BC2D46}" };

                            var statusElement = nodes.Where((x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("highlightType")));

                            string statusText = (statusElement != null && statusElement.Count() > 0) ? statusElement.FirstOrDefault().Value : "none";
                            string statusId = statusIds[0];
                            if (statusText.Equals("Update"))
                            {
                                statusId = statusIds[2];
                            }
                            else if (statusText.Equals("New"))
                            {
                                statusId = statusIds[1];
                            }

                            if (!string.IsNullOrEmpty(prodFamilyIds))
                            {
                                string familyId = prodFamilyIds.Contains('|') ? prodFamilyIds.Split('|')[0] : prodFamilyIds;
                                Item prodFamily = _master.GetItem(familyId);
                                string categoryname = GetProductCategoryName(prodFamily);
                                scPath = scPath + "/" + categoryname;
                            }

                            string showEcoIcon = "0";
                            if (!string.IsNullOrEmpty(ecoIcon) || features.Contains("ecoproduct-e.gif") || features.Contains("ecoproduct.gif"))
                            {
                                showEcoIcon = "1";
                            }

                            Item parentItem = _master.GetItem(scPath, Language.Parse(Languages.SelectedValue));

                            if (string.IsNullOrEmpty(scPath) || parentItem == null)
                            {
                                _logger["error"].Log("Sitecore path is empty or No item matched the path.");
                                continue;
                            }

                            _logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                            Item productItem = CreateItem(parentItem, _logger, productItemName, elementList, true, altText);

                            if (productItem != null)
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    productItem.Editing.BeginEdit();

                                    _logger["debug"].Log("Editing Field: IsDiscontinued");
                                    productItem["IsDiscontinued"] = isActive.Equals("1") ? "" : "1";
                                    _logger["debug"].Log("Editing End");

                                    _logger["debug"].Log("Editing Field: ProductFamily");
                                    productItem["ProductFamily"] = prodFamilyIds;
                                    _logger["debug"].Log("Editing End");

                                    _logger["debug"].Log("Editing Field: Specifications");
                                    productItem["Specifications"] = url;
                                    _logger["debug"].Log("Editing End");

                                    _logger["debug"].Log("Editing Field: Status");
                                    productItem["Status"] = statusId;
                                    _logger["debug"].Log("Editing End");

                                    if (!string.IsNullOrEmpty(replacementProductId))
                                    {
                                        _logger["debug"].Log("Editing Field: Replacement Product");
                                        productItem["ReplacementProducts"] = replacementProductId;
                                        _logger["debug"].Log("Editing End");
                                    }

                                    _logger["debug"].Log("Editing Field: ShowEcoIcon");
                                    productItem["ShowEcoIcon"] = showEcoIcon;
                                    _logger["debug"].Log("Editing End");

                                    _logger["debug"].Log("Editing subitems sorting standand field");
                                    productItem["__Subitems Sorting"] = Sitecore.Configuration.Settings.GetSetting("CretedSubitemSortingId");
                                    _logger["debug"].Log("Editing End");

                                    productItem.Editing.EndEdit();
                                }
                            }

                            ProcessTabs(productItem, nodes, _logger);

                            if (productItem != null)
                            {
                                _logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, legacyProductId, productItem.ID.ToString(), productItem.Paths.Path, legacyUrl);
                            }
                        }
                        catch(Exception ex)
                        {
                            _logger["error"].Log("{0}",DateTime.Now.ToString());
                            _logger["error"].Log("{0}",ex.Message);
                            _logger["error"].Log("{0}",ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}",DateTime.Now.ToString());
                    _logger["error"].Log("{0}",ex.Message);
                    _logger["error"].Log("{0}",ex.StackTrace);
                }
                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}",duration);
                    _logger["debug"].Log("New product global items added:{0}", itemCount);
                    _logger["debug"].Log("Overwritten product global items:{0}", overwriteCount);
                    _logger["debug"].Log("Total number of versions for product global items:{0}", versionCount);
                    _logger["debug"].Log("New images count:{0}", imageCount);
                    _logger["debug"].Log("Reused images count:{0}", reusedImages);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode")]
        protected void ProductRegional_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            GetFieldMappings();
            
            MultiLogger logger;
            using (logger = new MultiLogger())
            {
                logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Debug_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Error_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_Meta_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                logger.Add("relatedproducts", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "{0}_relatedproducts_{1}.txt", SCTemplates.SelectedItem.Text, Languages.SelectedValue));
                logger["debug"].Log("-------------------------------------------------------------------");
                logger["debug"].Log("Start Time:{0}",startTime.ToString());
                logger["debug"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("Teamsite Path	ProductID	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;

                try
                {
                    FileService fs = new FileService();
                    var fileList = fs.GetAllFiles(tsPath);

                    foreach (string filePath in fileList)
                    {
                        try
                        {
                            var fileByteContent = fs.GetFileContent(filePath);
                            var bufferStream = new MemoryStream(fileByteContent);

                            var xDoc = XDocument.Load(bufferStream);
                            var nodes = xDoc.Root.Descendants();

                            string[] array = filePath.Split('/');

                            if (array[array.Length - 1].Contains("test"))
                            {
                                logger["error"].Log("Skipping test item ");
                                continue;
                            }

                            string productItemName = GetItemName(array[array.Length - 1]);
                            string scPath = txtSCPath.Text;

                            IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                            lstSelectedTSFields.ToArray().Contains(x.Attribute("name").Value));

                            string legacyProductId = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;
                            string legacyUrl = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;
                            //string modelName = nodes.Where(x => x.Name.LocalName.Equals("item") &&
                            //                                    x.Attribute("name").Value.Equals("Model_Number")).FirstOrDefault().Value;
                            //string productItemName = GetItemName(modelName);
                            Language language = Language.Parse(Languages.SelectedValue);
                            string globalProductId = string.Empty;
                            Item globalProductItem = GetGlobalProduct(productItemName, language);
                            if (globalProductItem != null)
                            {
                                globalProductId = globalProductItem.ID.ToString();

                                string catName = GetProductCategoryName(globalProductItem);
                                if (string.IsNullOrEmpty(catName))
                                {
                                    scPath = scPath + "/products";
                                }
                                else
                                {
                                    scPath = scPath + "/" + GetProductCategoryName(globalProductItem) + "/products";
                                }
                            }

                            string relatedProductIds = string.Empty;
                            List<string> models = new List<string>();
                            var relatedLinks = nodes.Where((x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("link")));
                            if(relatedLinks != null && relatedLinks.Count() > 0)
                            {
                                string skippedItems = string.Empty;
                                foreach(XElement link in relatedLinks)
                                {
                                    string linkUrl = link.Value;
                                    if (linkUrl.Contains("/Products-Solutions/Products/"))
                                    {
                                        string model = linkUrl.Split('/').Last();
                                        string modelNumber = model.Contains('.') ? model.Split('.')[0] : model;
                                        Item prodItem = GetRegionalProduct(GetItemName(modelNumber), language);//_master.GetItem(scPath + "/" + GetItemName(modelNumber));
                                        if (prodItem != null)
                                        {
                                            models.Add(prodItem.ID.ToString());
                                        }
                                        else
                                        {
                                            skippedItems = skippedItems + modelNumber + ",";
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(skippedItems))
                                {
                                    logger["relatedproducts"].Log("{0}	{1}	{2}", legacyProductId, productItemName, skippedItems.TrimEnd(','));
                                }
                            }
                            relatedProductIds = (models != null && models.Count() > 0) ? string.Join("|", models) : string.Empty;
                            
                            Item parentItem = _master.GetItem(scPath, language);

                            if (string.IsNullOrEmpty(scPath) || parentItem == null)
                            {
                                logger["error"].Log("Sitecore path is empty or No item matched the path.");
                                continue;
                            }

                            logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                            Item productItem = CreateItem(parentItem, logger, productItemName, elementList, false, string.Empty);                            

                            if (productItem != null)
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    productItem.Editing.BeginEdit();

                                    logger["debug"].Log("Editing Field: SelectProduct");
                                    productItem["SelectProduct"] = globalProductId;
                                    logger["debug"].Log("Editing End");

                                    logger["debug"].Log("Editing Field : Related Products");
                                    productItem["RelatedProducts"] = relatedProductIds;
                                    logger["debug"].Log("Editing End");

                                    productItem.Editing.EndEdit();
                                }

                                logger["meta"].Log("{0}	{1}	{2}	{3}	{4}", filePath, legacyProductId, productItem.ID.ToString(), productItem.Paths.Path, legacyUrl);
                            }
                        }
                        catch(Exception ex)
                        {
                            logger["error"].Log("{0}", DateTime.Now.ToString());
                            logger["error"].Log("{0}", ex.Message);
                            logger["error"].Log("{0}", ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger["error"].Log("{0}", DateTime.Now.ToString());
                    logger["error"].Log("{0}", ex.Message);
                    logger["error"].Log("{0}", ex.StackTrace);
                }
                if(logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    logger["debug"].Log("-------------------------------------------------");
                    logger["debug"].Log("Time taken:{0}", duration);
                    logger["debug"].Log("New product regional items added:{0}", itemCount);
                    logger["debug"].Log("Overwritten product regional items:{0}", overwriteCount);
                    logger["debug"].Log("Total number of versions for product regional items:{0}", versionCount);
                    logger["debug"].Log("New images count:{0}", imageCount);
                    logger["debug"].Log("Reused images count:{0}", reusedImages);
                    logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private void ProcessTabs(Item item, IEnumerable<XElement> nodes, MultiLogger logger)
        {
            try
            {
                TemplateID tabTemplate = new TemplateID(new ID("{E280A6CF-E094-498A-A96E-E7032EB8FC9F}"));
                Language language = Language.Parse(Languages.SelectedValue);
                var tabNode = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Tabs")).FirstOrDefault();

                string features = string.Empty;

                var featuresNode = nodes.Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Features"));
                if( featuresNode != null && featuresNode.Count() > 0 )
                {
                    features = featuresNode.FirstOrDefault().Value;
                }

                string description = string.Empty;
                var descriptionNode = tabNode.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Additional_Content"));
                if(descriptionNode != null && descriptionNode.Count() > 0)
                {
                    description = descriptionNode.FirstOrDefault().Value;
                }

                var customTabs = nodes.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("custom_tab"));

                var libraryTab = nodes.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Library Tab"));

                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Item descriptionTabItem = _master.GetItem(item.Paths.Path + "/overview", language);

                    if (descriptionTabItem == null)
                    {
                        descriptionTabItem = item.Add("overview", tabTemplate);
                        logger["debug"].Log("New item added {0}", descriptionTabItem.Paths.Path);
                    }
                    else
                    {
                        bool hasLangVersion = HasLanguageVersion(descriptionTabItem, Languages.SelectedValue);
                        if (!hasLangVersion)
                        {
                            descriptionTabItem = descriptionTabItem.Versions.AddVersion();
                            logger["debug"].Log("Added new {1} version for Item:{0}", descriptionTabItem.Paths.Path, Languages.SelectedItem.Text);
                        }
                        else
                        {
                            logger["debug"].Log("Overwriting the item: {0} in {1} language", descriptionTabItem.Paths.Path, Languages.SelectedItem.Text);
                        }
                    }

                    descriptionTabItem.Editing.BeginEdit();
                    logger["debug"].Log("Editing item {0}", descriptionTabItem.Paths.Path);
                    logger["debug"].Log("Editing field Title");
                    string descriptionText = "Overview";                    
                    if(descriptionTabTitle.ContainsKey(Languages.SelectedValue))
                    {
                        descriptionText = descriptionTabTitle[Languages.SelectedValue];
                    }
                    descriptionTabItem["Title"] = descriptionText;
                    logger["debug"].Log("Editing End");

                    logger["debug"].Log("Editing sort order standand field");
                    descriptionTabItem["__Sortorder"] = "1";
                    logger["debug"].Log("Editing End");

                    logger["debug"].Log("Editing subitems sorting standand field");
                    descriptionTabItem["__Subitems Sorting"] = Sitecore.Configuration.Settings.GetSetting("CretedSubitemSortingId");
                    logger["debug"].Log("Editing End");
                    descriptionTabItem.Editing.EndEdit();

                    if (!string.IsNullOrEmpty(features))
                    {
                        Item featuresItem = _master.GetItem(descriptionTabItem.Paths.Path + "/features", language);
                        if (featuresItem == null)
                        {
                            featuresItem = descriptionTabItem.Add("features", tabTemplate);
                            logger["debug"].Log("New item added {0}", featuresItem.Paths.Path);
                        }
                        else
                        {
                            bool hasLangVersion = HasLanguageVersion(featuresItem, Languages.SelectedValue);
                            if (!hasLangVersion)
                            {
                                featuresItem = featuresItem.Versions.AddVersion();
                                logger["debug"].Log("Added new {1} version for Item:{0}", featuresItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                            else
                            {
                                logger["debug"].Log("Overwriting the item: {0} in {1} language", featuresItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                        }
                        string featureText = "Features";
                        if(featuresTitle.ContainsKey(Languages.SelectedValue))
                        {
                            featureText = featuresTitle[Languages.SelectedValue];
                        }
                        featuresItem.Editing.BeginEdit();
                        logger["debug"].Log("Editing item {0}", featuresItem.Paths.Path);
                        logger["debug"].Log("Editing field Title");
                        featuresItem["Title"] = featureText;
                        logger["debug"].Log("Editing End");
                        logger["debug"].Log("Editing field Description");
                        string featuresText = ProcessMediaInRichText(features);
                        featuresItem["Description"] = WrapTableTag(StringHtmlExtensions.RemoveAttributes(featuresText, new string[] { "class", "style","width","height" }));
                        logger["debug"].Log("Editing End");

                        logger["debug"].Log("Editing sort order standand field");
                        featuresItem["__Sortorder"] = "1";
                        logger["debug"].Log("Editing End");

                        featuresItem.Editing.EndEdit();
                    }

                    if (!string.IsNullOrEmpty(description))
                    {
                        Item descriptionItem = _master.GetItem(descriptionTabItem.Paths.Path + "/description", language);
                        if (descriptionItem == null)
                        {
                            descriptionItem = descriptionTabItem.Add("description", tabTemplate);
                            logger["debug"].Log("New item added {0}", descriptionItem.Paths.Path);
                        }
                        else
                        {
                            bool hasLangVersion = HasLanguageVersion(descriptionItem, Languages.SelectedValue);
                            if (!hasLangVersion)
                            {
                                descriptionItem = descriptionItem.Versions.AddVersion();
                                logger["debug"].Log("Added new {1} version for Item:{0}", descriptionItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                            else
                            {
                                logger["debug"].Log("Overwriting the item: {0} in {1} language", descriptionItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                        }
                       
                        descriptionItem.Editing.BeginEdit();
                        logger["debug"].Log("Editing item {0}", descriptionItem.Paths.Path);
                        logger["debug"].Log("Editing field Title");
                        descriptionItem["Title"] = "Description";
                        logger["debug"].Log("Editing End");
                        logger["debug"].Log("Editing field Description");
                        string newdescription = ProcessMediaInRichText(description);
                        descriptionItem["Description"] = WrapTableTag(StringHtmlExtensions.RemoveAttributes(newdescription, new string[] { "class", "style", "width", "height" }));
                        logger["debug"].Log("Editing End");
                        
                        logger["debug"].Log("Editing sort order standand field");
                        descriptionItem["__Sortorder"] = "2";
                        logger["debug"].Log("Editing End");

                        descriptionItem.Editing.EndEdit();
                    }

                    if (customTabs != null && customTabs.Count() > 0)
                    {
                        foreach (XElement tab in customTabs.Distinct())
                        {
                            string tabTitle = tab.Descendants().Where(x => x.Name.LocalName.Equals("item")
                                                        && x.Attribute("name").Value.Equals("custom_tab_title")).FirstOrDefault().Value;
                            string tabDescription = tab.Descendants().Where(x => x.Name.LocalName.Equals("item")
                                                        && x.Attribute("name").Value.Equals("custom_tab_content")).FirstOrDefault().Value;

                            string tabTitleValue = tabTitle;
                            string englishTitle = GetTabTitle(tabTitle, Languages.SelectedValue);

                            string tabItemName = GetItemName(englishTitle);
                            Item customTab = _master.GetItem(item.Paths.Path + "/" + tabItemName, language);
                            
                            if (customTab == null)
                            {
                                customTab = item.Add(tabItemName, tabTemplate);
                                logger["debug"].Log("New item added {0}", customTab.Paths.Path);
                            }
                            else
                            {
                                bool hasLangVersion = HasLanguageVersion(customTab, Languages.SelectedValue);
                                if (!hasLangVersion)
                                {
                                    customTab = customTab.Versions.AddVersion();
                                    logger["debug"].Log("Added new {1} version for Item:{0}", customTab.Paths.Path, Languages.SelectedItem.Text);
                                }
                                else
                                {
                                    logger["debug"].Log("Overwriting the item: {0} in {1} language", customTab.Paths.Path, Languages.SelectedItem.Text);
                                }
                            }

                            customTab.Editing.BeginEdit();
                            logger["debug"].Log("Editing item {0}", customTab.Paths.Path);
                            logger["debug"].Log("Editing field Title");
                            customTab["Title"] = tabTitleValue;
                            logger["debug"].Log("Editing End");
                            logger["debug"].Log("Editing field Description");
                            string newtabDescription = ProcessMediaInRichText(tabDescription);
                            customTab["Description"] = WrapTableTag(StringHtmlExtensions.RemoveAttributes(newtabDescription, new string[] { "class", "style","width","height" }));
                            logger["debug"].Log("Editing End");
                            customTab.Editing.EndEdit();
                        }
                    }

                    if(libraryTab!=null && libraryTab.Count()>0)
                    {
                        string libraryTitle = libraryTab.FirstOrDefault().Value;
                        Item libraryTabItem = _master.GetItem(item.Paths.Path + "/library", language);

                        if (libraryTabItem == null)
                        {
                            libraryTabItem = item.Add("library", new TemplateID(new ID(Constants.TemplateIds.Library)));
                            logger["debug"].Log("New item added {0}", libraryTabItem.Paths.Path);
                        }
                        else
                        {
                            bool hasLangVersion = HasLanguageVersion(libraryTabItem, Languages.SelectedValue);
                            if (!hasLangVersion)
                            {
                                libraryTabItem = libraryTabItem.Versions.AddVersion();
                                logger["debug"].Log("Added new {1} version for Item:{0}", libraryTabItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                            else
                            {
                                logger["debug"].Log("Overwriting the item: {0} in {1} language", libraryTabItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                        }

                        libraryTabItem.Editing.BeginEdit();
                        logger["debug"].Log("Editing item {0}", libraryTabItem.Paths.Path);
                        logger["debug"].Log("Editing field Title");
                        libraryTabItem["Title"] = libraryTitle;
                        logger["debug"].Log("Editing End");
                        libraryTabItem.Editing.EndEdit();
                    }
                }
            }
            catch (Exception ex)
            {
                logger["error"].Log("{0}", DateTime.Now.ToString());
                logger["error"].Log("{0}", ex.Message);
                logger["error"].Log("{0}", ex.StackTrace);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)")]
        public string ProcessMediaInRichText(string htmlSource)
        {
            List<string> imgUrls = FetchLinksFromSource(htmlSource);
            string html = htmlSource;
            if (imgUrls.Count > 0)
            {
                imgUrls = imgUrls.Distinct().Where(x => !x.StartsWith("http")).ToList();

                foreach (string url in imgUrls)
                {
                    MediaItem image = ImageFields(null, string.Empty, false, url,string.Empty);
                    if (image == null)
                        continue;
                    string newUrl = "~/media/" + image.ID.ToShortID() + ".ashx";
                    html = html.Replace(url, newUrl);
                }
            }

            return html;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace("---","-").Replace("--","-").TrimEnd('-').ToLowerInvariant();
        }

        public string GetTabTitle(string tabTitle, string languageName)
        {
            string title = tabTitle;
            string tabValue;
            switch(languageName)
            {
                case "ja-JP":
                    if (customTabTitles.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
                case "ko-KR":
                    if (customTabTitlesKR.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
                case "zh-CN":
                    if (customTabTitlesCN.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
                case "zh-TW":
                    if (customTabTitlesTW.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
                case "ru-RU":
                    if (customTabTitlesRU.TryGetValue(tabTitle, out tabValue))
                    {
                        title = tabValue;
                    }
                    break;
            }
            return title;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)")]
        public string GetFamilyIds(string legacyProductId, string language)
        {
            string familyIds = string.Empty;
            string[] prodFamilyIds = null;
            string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");
            DataTable oldproductFamiyAssoc = GetDataFromFile(path, "OldProdFamilyAssoc");
            if (oldproductFamiyAssoc != null)
            {
                prodFamilyIds = (from DataRow row in oldproductFamiyAssoc.Rows
                                 where (row["ProductId"].ToString().Equals(legacyProductId) &&
                                 row["categorypageurl"].ToString().StartsWith("/" + language))
                                 select row).Select(x => x["CategoryId"].ToString()).ToArray<string>();
            }

            if (prodFamilyIds != null && prodFamilyIds.Count() > 0)
            {
                DataTable productFamiyAssoc = GetDataFromFile(path, "SCProdFamilyAssoc");
                var familyItemIds = (from DataRow row in productFamiyAssoc.Rows
                                     where prodFamilyIds.Contains(row["FamilyID"].ToString())
                                     select row);
                familyIds = string.Join("|", familyItemIds.Select(x => x["ItemID"].ToString()));
            }

            return familyIds;
        }

        public string GetProductCategoryName(Item item)
        {
            string categoryName = string.Empty;
            if(item !=null)
            {
                string familyPath = item.Paths.FullPath;
                if (familyPath.Contains("/test-measurement/"))
                {
                    return "test-measurement";
                }
                else
                if (familyPath.Contains("/components-accessories/"))
                {
                    return "components-accessories";
                }
                else if (familyPath.Contains("/anritsu-devices/"))
                {
                    return "anritsu-devices";
                }
            }
            return categoryName;
        }

        public Item GetGlobalProduct(string productName, Language language)
        {
            Item globalProduct = null;

            Item tm = _master.GetItem("/sitecore/content/Global/Products/test-measurement/" + productName, language);
            if (tm != null)
                return tm;

            Item components = _master.GetItem("/sitecore/content/Global/Products/components-accessories/" + productName, language);
            if (components != null)
                return components;

            Item anritsudevices = _master.GetItem("/sitecore/content/Global/Products/anritsu-devices/" + productName, language);
            if (anritsudevices != null)
                return anritsudevices;

            Item globalItems = _master.GetItem("/sitecore/content/Global/Products/" + productName, language);
            if (globalItems != null)
                return globalItems;

            return globalProduct;
        }

        public Item GetRegionalProduct(string productName, Language language)
        {
            Item regionalProduct = null;

            Item tm = _master.GetItem("/sitecore/content/GlobalWeb/home/test-measurement/Products/" + productName, language);
            if (tm != null)
                return tm;

            Item components = _master.GetItem("/sitecore/content/GlobalWeb/home/components-accessories/Products/" + productName, language);
            if (components != null)
                return components;

            Item anritsudevices = _master.GetItem("/sitecore/content/GlobalWeb/home/anritsu-devices/Products/" + productName, language);
            if (anritsudevices != null)
                return anritsudevices;

            Item regionalItems = _master.GetItem("/sitecore/content/GlobalWeb/home/Products/" + productName, language);
            if (regionalItems != null)
                return regionalItems;

            return regionalProduct;
        }

        public string WrapTableTag(string html)
        {
            html = html.Replace("<table", "<div class=\"detail-table\"><table");
            html = html.Replace("</table>", "</table></div>");
            return html;
        }
        //protected void CleanNames_Click(object sender, EventArgs e)
        //{
        //    MultiLogger logger;
        //    using (logger = new MultiLogger())
        //    {
        //        logger.Add("cleannames", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "cleannames.txt");

        //        var productGlobals = _master.SelectItems("fast:/sitecore/content/global/products//*[@@templatename='ProductGlobal']");
        //        foreach (Item item in productGlobals)
        //        {
        //            string name = GetItemName(item["ModelNumber"]);
        //            if (!name.Equals(item.Name))
        //            {
        //                logger["cleannames"].Log("{0}	{1} {2}", item.Name, name,item.ID.ToString());
        //                using (new Sitecore.SecurityModel.SecurityDisabler())
        //                {
        //                    item.Editing.BeginEdit();
        //                    item.Name = name;
        //                    item.Editing.EndEdit();
        //                }
        //            }
        //        }
        //        logger["cleannames"].Log("----------------------------------------------------");
        //        var productRegionals = _master.SelectItems("fast:/sitecore/content/globalweb/home//*[@@templatename='ProductRegional']");
        //        foreach (Item regionitem in productRegionals)
        //        {
        //            Item globalProd = _master.GetItem(regionitem["SelectProduct"]);
        //            if (globalProd != null)
        //            {
        //                string regionname = GetItemName(globalProd["ModelNumber"]);
        //                if (!regionname.Equals(regionitem.Name))
        //                {
        //                    logger["cleannames"].Log("{0}	{1} {2}", regionitem.Name, regionname,regionitem.ID.ToString());
        //                    using (new Sitecore.SecurityModel.SecurityDisabler())
        //                    {
        //                        regionitem.Editing.BeginEdit();
        //                        regionitem.Name = regionname;
        //                        regionitem.Editing.EndEdit();
        //                    }
        //                }
        //            }
        //        }
        //    }
            
        //}
    }
}