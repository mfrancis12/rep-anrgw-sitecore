﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Specifications.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.Products.Specifications" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <label>Upload File:</label>
        <asp:FileUpload ID="fileUpload" runat="server" /> <br /><br />
        <label>Sheet Name</label>
        <asp:TextBox ID="txtSheetName" runat="server"></asp:TextBox><br /><br />
        <label>Sitecore Path:</label>
        <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox><br /><br />
        <asp:Button ID="ButtonSpecifications" runat="server" Text="Migrate Specifications" OnClick="ButtonSpecifications_Click" />
    </div>
    </form>
</body>
</html>
