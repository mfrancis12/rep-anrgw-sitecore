﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate.Products
{
    public partial class Specifications : System.Web.UI.Page
    {
        MultiLogger _logger;
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        protected string templateID = "{F547E7B1-B7B9-41AD-8958-4756BD7A5633}";

        private Dictionary<int, string> cultureID = new Dictionary<int, string>
        {
            {1,"en-US"},
            {2,"ja-JP"},
            {3,"en-GB"},
            {4,"en-AU"},
            {6,"zh-CN"},
            {7,"ko-KR"},
            {10,"zh-TW"},
            {12,"en"},
            {14,"ru-RU"},
            {15,"en-IN"}
        };

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void ButtonSpecifications_Click(object sender, EventArgs e)
        {            
            DataTable fileData = GetDataFromFile();                

            if (fileData != null)
            {
                using (_logger = new MultiLogger())
                {
                    try
                    {
                        _logger.Add("debug", @"C:\Migration\Logs\Specifications_Debug.txt");
                        _logger.Add("error", @"C:\Migration\Logs\Specifications_Error.txt");
                        _logger.Add("meta", @"C:\Migration\Logs\Specifications_Meta.txt");
                        _logger["meta"].Log("-------------------------------------------------------------------");
                        _logger["meta"].Log("SpecificationID	ItemID	ItemPath	Language");
                        _logger["meta"].Log("-------------------------------------------------------------------");
                        foreach (DataRow row in fileData.Rows)
                        {
                            int cultureId = Convert.ToInt32(row["culturegroupId"]);
                            string language = cultureID[cultureId];
                            string name = row["SpecificationResKeyValue"].ToString();
                            string value = row["ResourceValue"].ToString();
                            string legacySpecId = row["specificationtypeid"].ToString();
                            string itemName = ItemUtil.ProposeValidItemName(row["SpecificationResKeyValue"].ToString().Trim());
                            string scPath = txtSCPath.Text;

                            CreateItem(language, name, value, itemName, legacySpecId, scPath);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger["error"].Log("Logged Time :", DateTime.Now.ToShortTimeString());
                        _logger["error"].Log(ex.Message);
                        _logger["error"].Log(ex.StackTrace);
                    }
                }
            }            
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1306:SetLocaleForDataTypes")]
        private DataTable GetDataFromFile()
        {
            DataTable fileData = new DataTable();
            if (fileUpload.HasFile)
            {
                string fileName = Path.GetFileName(fileUpload.PostedFile.FileName);
                string extension = Path.GetExtension(fileName);
                string folderPath = Sitecore.Configuration.Settings.GetSetting("ExcelFiles");

                string filePath = folderPath + fileName;
                fileUpload.SaveAs(filePath);
                fileData = GetDataFromFile(filePath, extension, "Yes", txtSheetName.Text);
            }
            return fileData;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1306:SetLocaleForDataTypes")]
        private DataTable GetDataFromFile(string FilePath, string Extension, string isHDR,string sheetName)
        {
            string conStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";

            if(Extension.Equals("xlx"))
            {
                conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
            }
            
            conStr = String.Format(conStr, FilePath, isHDR);
            OleDbConnection connExcel = new OleDbConnection(conStr);
            OleDbCommand cmdExcel = new OleDbCommand();
            OleDbDataAdapter oda = new OleDbDataAdapter();
            
            DataTable dt = new DataTable();
            cmdExcel.Connection = connExcel;

            //Get the name of First Sheet
            connExcel.Open();
            DataTable dtExcelSchema;
            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            //string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();

            //Read Data from First Sheet
            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
            oda.SelectCommand = cmdExcel;
            oda.Fill(dt);
            connExcel.Close();

            return dt;
        }

        private void CreateItem(string language, string name, string value, string itemName, string legacySpecId, string scPath)
        {
            Item parentItem = _master.GetItem(scPath);

            if (string.IsNullOrEmpty(scPath) || parentItem == null)
            {
                _logger["error"].Log("Sitecore path is empty or No item matched the path");
                return;
            }

            _logger["debug"].Log("Parent Item Path:{0}", parentItem.Paths.Path);

            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                Language lang = Language.Parse(language);
                Item specItem = _master.GetItem(scPath + "/" + itemName, lang);

                if (specItem != null)
                {
                    bool hasLangVersion = HasLanguageVersion(specItem, language);
                    if (!hasLangVersion)
                    {
                        specItem = specItem.Versions.AddVersion();
                        _logger["debug"].Log("Added new {1} version for Item:{0}", specItem.Paths.Path, lang.CultureInfo.DisplayName);
                    }
                    else
                    {
                        _logger["debug"].Log("Overwriting the item: {0} in {1} language", specItem.Paths.Path, lang.CultureInfo.DisplayName);
                    }
                }
                else
                {
                    _logger["debug"].Log("Adding new specification Item to:{0}", parentItem.Paths.Path);
                    specItem = parentItem.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(templateID)));
                    _logger["debug"].Log("New specification item added: {0}", specItem.Paths.Path);
                }

                specItem.Editing.BeginEdit();

                _logger["debug"].Log("Editing the item");
                specItem["name"] = name.Trim();
                specItem["value"] = value.Trim();
                _logger["debug"].Log("Editing finished");

                specItem.Editing.EndEdit();

                _logger["meta"].Log("{0}	{1}	{2}	{3}", legacySpecId, specItem.ID.ToString(), specItem.Paths.Path, language);
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}