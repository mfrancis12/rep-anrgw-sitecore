﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductsSolutions.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.Products.ProductsSolutions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .mappingContainer { width: 800px; display:inline-block;}
        .tsFields,.scFields { width:300px; float:left; border:1px solid #b4b4b4;padding:5px;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Teamsite DCR Path :</label>
            <asp:TextBox ID="txtTSDCRPath" runat="server" style="width:400px"></asp:TextBox>
            <br />
            <br />
            <label>Sitecore Template:</label>
            <asp:DropDownList ID="SCTemplates" runat="server"></asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="GetFields" runat="server" Text="Get Fields" OnClick="GetFields_Click" />            
            <br />
            <br />            
            <asp:Repeater ID="repeaterTSFields" runat="server" OnItemDataBound="TeamsiteFields_ItemDataBound">
                <HeaderTemplate>
                    <div style="display: inline-block;">
                </HeaderTemplate>
                <ItemTemplate>
                    <div id="divMain" runat="server" class="mappingContainer">
                        <div class="tsFields">
                            <asp:Label ID="tsFieldName" runat="server"></asp:Label>
                            <asp:HiddenField ID="tsFieldID" runat="server"></asp:HiddenField>
                        </div>
                        <div class="scFields">
                            <asp:DropDownList ID="scFields" runat="server" >
                            </asp:DropDownList>
                        </div>            
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            <br />
            <br />
            <asp:Panel ID="panelSC" runat="server" Visible="false">
                <label>Sitecore Path :</label>
                <asp:TextBox ID="txtSCPath" runat="server"></asp:TextBox><br />
                <br />

                <label>Language :</label>
                <asp:DropDownList ID="Languages" runat="server"></asp:DropDownList><br />
                <br />

                <asp:Button ID="ProductCategory" runat="server" OnClick="ProductCategory_Click" Text="Import Product Categories" />
                <br />
                <br />
                <asp:Button ID="ProductSubcategory" runat="server" Text="Import Sub-Categories" OnClick="ProductSubcategory_Click" />
                <br />
                <br />
                <asp:Button ID="ProductFamily" runat="server" Text="Import Product Families" OnClick="ProductFamily_Click" />
                <br />
                <br />
                <asp:Button ID="Product" runat="server" Text="Import Products Global" OnClick="Product_Click" />
                <br />
                <br />
                <asp:Button ID="ProductRegional" runat="server" Text="Import Products Regional" OnClick="ProductRegional_Click" />
                <br />
                <br />
                <%--<asp:Button ID="CleanNames" runat="server" Text="CleanNames" OnClick="CleanNames_Click" />--%>
                <asp:Button ID="BindTechnologies" runat="server" Text="Bind Technologies" OnClick="BindTechnologies_Click" />
                <br />
                <br />
                <asp:Button ID="SpecificationsFix" runat="server" Text="Bind Specifications" OnClick="SpecificationsFix_Click" />
            </asp:Panel>
        </div>
    </form>
</body>
</html>
