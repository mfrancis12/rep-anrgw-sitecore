﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReplaceTitles.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.Migrate.ReplaceTitles" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Replace Titles</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Enter Sitecore Parent Item Path :</label>
            <asp:TextBox ID="txtSCPath" runat="server" Style="width: 400px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="ButtonReplaceTitle" runat="server" Text="Replace Titles" OnClick="ReplaceTitles_Click"/>
        </div>
    </form>
</body>
</html>
