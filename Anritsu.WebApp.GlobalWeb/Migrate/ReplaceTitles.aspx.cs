﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Sitecore.Configuration;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class ReplaceTitles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ReplaceTitles_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            int itemCount = 0;
            int pageTitleCount = 0, menuTitleCount = 0, metaTitleCount = 0;
            MultiLogger _logger;
            using (_logger = new MultiLogger())
            {
                try
                {
                    _logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ReplaceTitles_Debug.txt"));
                    _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ReplaceTitles_Error.txt"));

                    _logger["debug"].Log("-------------------------------------------------------------------");
                    _logger["debug"].Log("Start Time: {0}", startTime.ToString());
                    _logger["debug"].Log("-------------------------------------------------------------------");

                    var db = Factory.GetDatabase("master");
                    Item parentItem = db.GetItem(txtSCPath.Text);

                    foreach (Item items in parentItem.Axes.GetDescendants())
                    {
                        foreach (var language in LanguageManager.GetLanguages(db))
                        {
                            Item item = db.GetItem(items.ID.ToString(), language);

                            if (item != null && item.Versions.Count > 0 && item.Fields["PageTitle"] != null
                                                                        && item.Fields["MenuTitle"] != null
                                                                        && item.Fields["MetaTitle"] != null)
                            {
                                try
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        bool flag = true;
                                        string menuTitle = item["MenuTitle"];
                                        string metaTitle = item["MetaTitle"];
                                        string pageTitle = item["PageTitle"];

                                        _logger["debug"].Log("Editing item {0} started in {1}", item.Name, item.Language.ToString());
                                        item.Editing.BeginEdit();

                                        if (pageTitle.Equals("$name") || string.IsNullOrEmpty(pageTitle))
                                        {
                                            _logger["debug"].Log("Editing PageTitle Field");
                                            item["PageTitle"] = string.Empty;
                                            pageTitleCount++;
                                            flag = false;
                                        }

                                        if (menuTitle.Equals("$name") || string.IsNullOrEmpty(menuTitle))
                                        {
                                            _logger["debug"].Log("Editing MenuTitle Field");
                                            item["MenuTitle"] = flag ? pageTitle : string.Empty;
                                            menuTitleCount++;
                                        }

                                        if (metaTitle.Equals("$name") || string.IsNullOrEmpty(metaTitle))
                                        {
                                            _logger["debug"].Log("Editing MetaTitle Field");
                                            item["MetaTitle"] = flag ? pageTitle : string.Empty;
                                            metaTitleCount++;
                                        }

                                        item.Editing.EndEdit();
                                        itemCount++;
                                        _logger["debug"].Log("Editing End");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                                    _logger["error"].Log("{0}", item.ID);
                                    _logger["error"].Log("{0}", ex.Message);
                                    _logger["error"].Log("{0}", ex.StackTrace);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log("{0}", DateTime.Now.ToString());
                    _logger["error"].Log("{0}", ex.Message);
                    _logger["error"].Log("{0}", ex.StackTrace);
                }

                if (_logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    _logger["debug"].Log("-------------------------------------------------");
                    _logger["debug"].Log("Time taken:{0}", duration);
                    _logger["debug"].Log("Number of Page Titles updated :{0}", pageTitleCount);
                    _logger["debug"].Log("Number of Menu Titles updated :{0}", menuTitleCount);
                    _logger["debug"].Log("Number of Meta Titles updated :{0}", metaTitleCount);
                    _logger["debug"].Log("Number of Items :{0}", itemCount);
                    _logger["debug"].Log("-------------------------------------------------");
                }
            }
        }
    }
}