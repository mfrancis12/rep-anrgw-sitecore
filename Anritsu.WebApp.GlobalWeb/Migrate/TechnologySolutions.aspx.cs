﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using ContentMigration.Services;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class TechnologySolutions : System.Web.UI.Page
    {
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");
        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        private string localPath = Sitecore.Configuration.Settings.GetSetting("ImageDownloadPath");
        private IEnumerable<TemplateItem> _sitecoreTemplates = null;
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        int itemCount = 0, versionCount = 0, overwriteCount = 0, imageCount = 0, reusedImages = 0;

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));

                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected void ButtonTechnologies_Click(object sender, EventArgs e)
        {
            MultiLogger logger;
            DateTime startTime = DateTime.Now;

            using (logger = new MultiLogger())
            {
                logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BTS_Debug_{0}.txt", Languages.SelectedValue));
                logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BTS_Error_{0}.txt", Languages.SelectedValue));

                logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BTS_Meta_{0}.txt", Languages.SelectedValue));
                logger["meta"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("Teamsite Path	ItemID	ItemPath    LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;
                string scPath = txtSCPath.Text;

                FileService fs = new FileService();
                //var fileList = fs.GetAllFiles(tsPath);
                var fileList = new string[] { tsPath };

                foreach (string filePath in fileList)
                {
                    try
                    {
                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);
                        var xDoc = XDocument.Load(bufferStream);

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item"));

                        Language language = Language.Parse(Languages.SelectedValue);

                        Item parentItem = _master.GetItem(scPath, language);

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                        string[] array = filePath.Split('/');

                        string itemName = string.Empty;
                        string folderName = string.Empty;

                        string legacyUrl = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                 x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;

                        if (ItemUtil.ProposeValidItemName(array[array.Length - 1]).Equals("index"))
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));
                        }
                        else
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 1]));
                        }

                        Item newItem = null;
                        try
                        {
                            List<string> eleList = new List<string>();
                            using (new Sitecore.SecurityModel.SecurityDisabler())
                            {
                                string title = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                    x.Attribute("name").Value.Equals("page_title")).FirstOrDefault().Value;

                                var metaValues = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                    x.Attribute("name").Value.Equals("meta_values"));

                                string metaDescription = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("desc")).FirstOrDefault().Value;

                                string metaKeywords = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("keywords")).FirstOrDefault().Value;

                                string content = "<h1>" + title + "</h1>";
                                XElement contentElement = elementList.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("body_content"));
                                if (contentElement != null && contentElement.DescendantNodes().Count() > 0)
                                {
                                    foreach (XElement valueNode in contentElement.Elements("value"))
                                    {
                                        XElement contentNode = valueNode.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("content")).FirstOrDefault(); ;
                                        if (contentNode != null && !string.IsNullOrEmpty(contentNode.Value))
                                        {
                                            content = content + contentNode.Value;
                                        }
                                    }
                                }
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    newItem = _master.GetItem(parentItem.Paths.Path + "/" + itemName, language);
                                    if (newItem != null)
                                    {
                                        bool hasLangVersion = HasLanguageVersion(newItem, Languages.SelectedValue);
                                        if (!hasLangVersion)
                                        {
                                            newItem = newItem.Versions.AddVersion();
                                            versionCount++;
                                            logger["debug"].Log("Added new {1} version for Item:{0}", newItem.Paths.Path, Languages.SelectedItem.Text);
                                        }
                                        else
                                        {
                                            overwriteCount++;
                                            logger["debug"].Log("Overwriting the item: {0} in {1} language", newItem.Paths.Path, Languages.SelectedItem.Text);
                                        }
                                    }
                                    else
                                    {
                                        newItem = parentItem.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                                        itemCount++;
                                        logger["debug"].Log("New item added {0} in {1}", newItem.Paths.Path, newItem.Language.Name);
                                    }

                                    newItem.Editing.BeginEdit();
                                    logger["debug"].Log("Editing Item {0} started", itemName);

                                    logger["debug"].Log("Editing Content field.");
                                    newItem["Content"] = ProcessMediaInRichText(content);

                                    logger["debug"].Log("Editing PageTitle field.");
                                    newItem["PageTitle"] = title;

                                    logger["debug"].Log("Editing MetaTitle field.");
                                    newItem["MetaTitle"] = title;

                                    logger["debug"].Log("Editing MenuTitle field.");
                                    newItem["MenuTitle"] = title;

                                    logger["debug"].Log("Editing Meta Description field.");
                                    newItem["Metatags-Description"] = metaDescription;

                                    logger["debug"].Log("Editing Meta Keywords field.");
                                    newItem["Metatags-Other keywords"] = metaKeywords;

                                    logger["debug"].Log("Editing End.");
                                    newItem.Editing.EndEdit();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger["error"].Log(DateTime.Now.ToString());
                            logger["error"].Log("Error occured while creating item {0} under {1}", itemName, parentItem.Paths.Path);
                            logger["error"].Log("{0}", filePath);
                            logger["error"].Log(ex.Message);
                            logger["error"].Log(ex.StackTrace);
                        }
                        logger["meta"].Log("{0}	{1}	{2}	{3}", filePath, newItem.ID.ToString(), newItem.Paths.Path, legacyUrl);
                    }
                    catch (Exception ex)
                    {
                        logger["error"].Log("{0}", DateTime.Now.ToString());
                        logger["error"].Log("{0}", filePath);
                        logger["error"].Log("{0}", ex.Message);
                        logger["error"].Log("{0}", ex.StackTrace);
                    }
                }
                if (logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    logger["debug"].Log("-------------------------------------------------");
                    logger["debug"].Log("Time taken:{0}", duration);
                    logger["debug"].Log("New items added : {0}", itemCount);
                    logger["debug"].Log("Overwritten items : {0}", overwriteCount);
                    logger["debug"].Log("Total number of versions for items : {0}", versionCount);
                    logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "richtextimage"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "isproduct"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        protected MediaItem ImageFields(XElement element, string itemName, bool isproduct, string richtextimage, string altText)
        {
            string elementPath = string.Empty;
            if (element != null)
            {
                elementPath = element.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                elementPath = HttpUtility.UrlDecode(elementPath);
            }
            else if (!string.IsNullOrEmpty(richtextimage))
            {
                elementPath = HttpUtility.UrlDecode(richtextimage);
            }

            string tsFilePath = workAreaPath.Replace("/templatedata/", "") + elementPath;

            elementPath = isproduct ? "Images/Products/" + itemName + "/" + elementPath.Split('/').Last() : elementPath;

            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/" + path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            FileService imageFS = new FileService();
            var bytes = imageFS.GetFileContent(tsFilePath);
            var mediaPath = elementPath.Split('/');
            var mediaItemName = mediaPath.Last();
            if (bytes != null)
            {
                var stream = new MemoryStream(bytes);
                var bw = new BinaryWriter(new FileStream(localPath + mediaItemName, FileMode.Append, FileAccess.Write));
                bw.Write(bytes);
                bw.Close();
            }
            else
            {
                return null;
            }

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public MediaItem CreateMediaItem(string mediaUrl, string localMediaPath, string altText)
        {
            MediaItem mediaItem = null;
            MultiLogger logger;
            using (logger = new MultiLogger())
            {
                logger.Add("image", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog.txt");
                logger["image"].Log("{0}	Started uploading image", DateTime.Now.ToString());
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaPath = mediaUrl.Split('/');
                    var mediaFolderID = CreateMediaFolders(mediaPath);
                    var mediaFolder = master.GetItem(mediaFolderID);
                    var mediaItemName = mediaPath.Last();

                    FileInfo file = new FileInfo(localMediaPath + mediaItemName);
                    if (file.Exists)
                    {
                        var filename = GetItemName(mediaItemName.Split('.')[0]);
                        filename = filename.Replace(".", "_");
                        var options = new Sitecore.Resources.Media.MediaCreatorOptions();
                        options.Database = master;
                        options.Versioned = false;
                        options.Destination = mediaFolder.Paths.FullPath + "/" + filename;
                        options.IncludeExtensionInItemName = false;
                        options.AlternateText = string.IsNullOrEmpty(altText) ? mediaItemName : altText;
                        options.FileBased = Sitecore.Configuration.Settings.Media.UploadAsFiles;
                        var mediaCreator = new Sitecore.Resources.Media.MediaCreator();
                        try
                        {
                            mediaItem = mediaCreator.CreateFromFile(file.FullName, options);
                            logger["image"].Log("{0}	New image created : {1}", DateTime.Now.ToString(), mediaItem.Path);
                            logger["image"].Log("Image Size {0} bytes", mediaItem.Size.ToString());
                            imageCount++;
                        }
                        catch (Exception ex)
                        {
                            logger["image"].Log(DateTime.Now.ToString() + "	Error while creating image");
                            logger["image"].Log(ex.Message);
                            logger["image"].Log(ex.StackTrace);
                            //Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return mediaItem;
        }

        public ID CreateMediaFolders(string[] mediaPath)
        {
            Item mediaFolder = null;
            string mediaLibraryPath = "/sitecore/media library";
            foreach (string path in mediaPath)
            {
                if (!path.Contains('.') && !string.IsNullOrWhiteSpace(path))
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaFolderTemplate = master.Templates["System/Media/Media folder"];

                    var mediaLibraryItem = master.GetItem(mediaLibraryPath);
                    mediaLibraryPath = mediaLibraryPath + "/" + GetItemName(path);
                    mediaFolder = master.GetItem(mediaLibraryPath);

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (mediaFolder == null)
                        {
                            try
                            {
                                mediaFolder = mediaLibraryItem.Add(GetItemName(path), mediaFolderTemplate);
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return mediaFolder.ID;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)")]
        public string ProcessMediaInRichText(string htmlSource)
        {
            List<string> imgUrls = FetchLinksFromSource(htmlSource);
            string html = htmlSource;
            if (imgUrls.Count > 0)
            {
                imgUrls = imgUrls.Distinct().Where(x => !x.StartsWith("http")).ToList();

                foreach (string url in imgUrls)
                {
                    MediaItem image = ImageFields(null, string.Empty, false, url, string.Empty);
                    if (image == null)
                        continue;
                    string newUrl = "~/media/" + image.ID.ToShortID() + ".ashx";
                    html = html.Replace(url, newUrl);
                }
            }

            return html;
        }
    }
}