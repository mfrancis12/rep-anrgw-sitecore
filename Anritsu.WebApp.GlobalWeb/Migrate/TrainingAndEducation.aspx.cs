﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class TrainingAndEducation : System.Web.UI.Page
    {
        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");
        private string workAreaPath = Sitecore.Configuration.Settings.GetSetting("TS_WorkAreaPath");
        private string localPath = Sitecore.Configuration.Settings.GetSetting("ImageDownloadPath");

        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");
        int itemCount = 0, versionCount = 0, overwriteCount = 0, imageCount = 0, reusedImages = 0;
        int accordianItemCount = 0, accordianItem_versionCount = 0, accordianItem_overwriteCount = 0;
        List<string> lstSelectedTSFields = new List<string>();
        List<string> lstSelectedSCFields = new List<string>();

        MultiLogger _logger;

        private IEnumerable<TemplateItem> _sitecoreTemplates = null;

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "richtextimage"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "isproduct"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        protected MediaItem ImageFields(XElement element, string itemName, bool isproduct, string richtextimage, string altText)
        {
            string elementPath = string.Empty;
            if (element != null)
            {
                elementPath = element.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                elementPath = HttpUtility.UrlDecode(elementPath);
            }
            else if (!string.IsNullOrEmpty(richtextimage))
            {
                elementPath = HttpUtility.UrlDecode(richtextimage);
            }

            string tsFilePath = workAreaPath.Replace("/templatedata/", "") + elementPath;

            elementPath = isproduct ? "Images/Products/" + itemName + "/" + elementPath.Split('/').Last() : elementPath;

            string imagename = elementPath.Split('/').Last();
            string path = elementPath.Replace(imagename, "");
            string imagepath = "/sitecore/media library/" + path + GetItemName(imagename.Split('.')[0]);
            Item image = _master.GetItem(imagepath);
            if (image != null)
            {
                reusedImages++;
                return new MediaItem(image);
            }

            FileService imageFS = new FileService();
            var bytes = imageFS.GetFileContent(tsFilePath);
            var mediaPath = elementPath.Split('/');
            var mediaItemName = mediaPath.Last();
            if (bytes != null)
            {
                var stream = new MemoryStream(bytes);
                var bw = new BinaryWriter(new FileStream(localPath + mediaItemName, FileMode.Append, FileAccess.Write));
                bw.Write(bytes);
                bw.Close();
            }
            else
            {
                return null;
            }

            MediaItem mediaImage = CreateMediaItem(elementPath, localPath, altText);
            return mediaImage;
        }

        public MediaItem CreateMediaItem(string mediaUrl, string localMediaPath, string altText)
        {
            MediaItem mediaItem = null;
            MultiLogger logger;
            using (logger = new MultiLogger())
            {
                logger.Add("image", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "ImageLog.txt");
                logger["image"].Log("{0}	Started uploading image", DateTime.Now.ToString());
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaPath = mediaUrl.Split('/');
                    var mediaFolderID = CreateMediaFolders(mediaPath);
                    var mediaFolder = master.GetItem(mediaFolderID);
                    var mediaItemName = mediaPath.Last();

                    FileInfo file = new FileInfo(localMediaPath + mediaItemName);
                    if (file.Exists)
                    {
                        var filename = GetItemName(mediaItemName.Split('.')[0]);
                        filename = filename.Replace(".", "_");
                        var options = new Sitecore.Resources.Media.MediaCreatorOptions();
                        options.Database = master;
                        options.Versioned = false;
                        options.Destination = mediaFolder.Paths.FullPath + "/" + filename;
                        options.IncludeExtensionInItemName = false;
                        options.AlternateText = string.IsNullOrEmpty(altText) ? mediaItemName : altText;
                        options.FileBased = Sitecore.Configuration.Settings.Media.UploadAsFiles;
                        var mediaCreator = new Sitecore.Resources.Media.MediaCreator();
                        try
                        {
                            mediaItem = mediaCreator.CreateFromFile(file.FullName, options);
                            logger["image"].Log("{0}	New image created : {1}", DateTime.Now.ToString(), mediaItem.Path);
                            logger["image"].Log("Image Size {0} bytes", mediaItem.Size.ToString());
                            imageCount++;
                        }
                        catch (Exception ex)
                        {
                            logger["image"].Log(DateTime.Now.ToString() + "	Error while creating image");
                            logger["image"].Log(ex.Message);
                            logger["image"].Log(ex.StackTrace);
                            //Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return mediaItem;
        }

        public ID CreateMediaFolders(string[] mediaPath)
        {
            Item mediaFolder = null;
            string mediaLibraryPath = "/sitecore/media library";
            foreach (string path in mediaPath)
            {
                if (!path.Contains('.') && !string.IsNullOrWhiteSpace(path))
                {
                    Sitecore.Context.Site = SiteContextFactory.GetSiteContext("service");
                    Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                    var mediaFolderTemplate = master.Templates["System/Media/Media folder"];

                    var mediaLibraryItem = master.GetItem(mediaLibraryPath);
                    mediaLibraryPath = mediaLibraryPath + "/" + GetItemName(path);
                    mediaFolder = master.GetItem(mediaLibraryPath);

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (mediaFolder == null)
                        {
                            try
                            {
                                mediaFolder = mediaLibraryItem.Add(GetItemName(path), mediaFolderTemplate);
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return mediaFolder.ID;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.String.Replace(System.String,System.String)")]
        public string ProcessMediaInRichText(string htmlSource)
        {
            List<string> imgUrls = FetchLinksFromSource(htmlSource);
            string html = htmlSource;
            if (imgUrls.Count > 0)
            {
                imgUrls = imgUrls.Distinct().Where(x => !x.StartsWith("http")).ToList();

                foreach (string url in imgUrls)
                {
                    MediaItem image = ImageFields(null, string.Empty, false, url, string.Empty);
                    if (image == null)
                        continue;
                    string newUrl = "~/media/" + image.ID.ToShortID() + ".ashx";
                    html = html.Replace(url, newUrl);
                }
            }

            return html;
        }

        protected void GetFields_Click(object sender, EventArgs e)
        {
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    if (!string.IsNullOrEmpty(txtTSDCRPath.Text))
                    {
                        List<string[]> list = new List<string[]>();

                        string dataCaptureFile = txtTSDCRPath.Text.Split(new string[] { "/data/" }, StringSplitOptions.None)[0] + "/datacapture.cfg";
                        list = ParseXml(dataCaptureFile, list);

                        if (list == null || list.Count == 0)
                        {
                            return;
                        }

                        repeaterTSFields.DataSource = list;
                        repeaterTSFields.DataBind();
                    }

                    var languages = LanguageManager.GetLanguages(_master);
                    foreach (var language in languages)
                    {
                        Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                    }
                    Languages.Items.Insert(0, new ListItem("--select--", "0"));

                    panelSC.Visible = true;
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log("Error occured while clicking the Get Fields Button.");
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<string[]> ParseXml(string filePath, List<string[]> list)
        {
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileService fs = new FileService();
                    Byte[] fileByteContent = fs.GetFileContent(filePath);

                    if (fileByteContent == null)
                    {
                        _logger["error"].Log("FileByteContent is null");
                        return null;
                    }

                    string[] fieldTypes = new string[] { "text", "browser", "textarea", "select", "checkbox", "radio", "readonly" };

                    MemoryStream bufferStream = new MemoryStream(fileByteContent);
                    XDocument xDoc = XDocument.Load(bufferStream);

                    foreach (var element in xDoc.Root.Descendants())
                    {
                        if (element.Descendants().Where(x => x.Name.LocalName.Equals("hidden")).Count() > 0)
                            continue;

                        if (element.Name.LocalName.Equals("container") && element.Attribute("min") != null && element.Attribute("max") != null)
                        {
                            var ancestors = element.Ancestors();
                            string[] replicantAnc = ancestors.Where(x => x.Name.LocalName.Equals("container") && x.Attribute("min") != null && x.Attribute("max") != null).Select(x => x.Name.LocalName).ToArray();
                            if (replicantAnc.Count() == 0)
                            {
                                string[] array = new string[4];

                                array[0] = element.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("label")).Value;
                                array[1] = element.Attribute("name").Value;
                                array[2] = "container";
                                array[3] = filePath;
                                list.Add(array);
                            }
                        }
                        else if (element.Name.LocalName.Equals("item"))
                        {
                            var ancestors = element.Ancestors();
                            string[] replicantAnc = ancestors.Where(x => x.Name.LocalName.Equals("container") && x.Attribute("min") != null && x.Attribute("max") != null).Select(x => x.Name.LocalName).ToArray();
                            if (replicantAnc.Count() == 0)
                            {
                                string[] array = new string[4];
                                var labelElement = element.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("label"));
                                array[0] = (labelElement == null) ? element.Attribute("name").Value : labelElement.Value;
                                array[1] = element.Attribute("name").Value;

                                string fieldType = element.Descendants().Where(x => fieldTypes.Contains(x.Name.LocalName)).FirstOrDefault().Name.LocalName;
                                var dateField = element.Descendants().Where(x => x.Name.LocalName.Equals("text") && x.Attribute("validation-regex") != null).FirstOrDefault();
                                if (dateField != null)
                                {
                                    if (dateField.Attribute("validation-regex").Value.Equals("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}") || dateField.Attribute("validation-regex").Value.Equals("[0-9]{4}-([1-9]{1}|0[1-9]{1}|1[012]{1})-(0[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1}|[1-9]{1})$") || dateField.Attribute("validation-regex").Value.Equals("[0-9]{4}-([1-9]{1}|0[1-9]{1}|1[012]{1})-(0[1-9]{1}$|[1-2]{1}[0-9]{1}$|3[0-1]{1}$|[1-9]{1}$)"))
                                    {
                                        fieldType = "date";
                                    }
                                }
                                array[2] = fieldType;
                                array[3] = string.Empty;
                                list.Add(array);
                            }
                        }
                        else if (element.Name.LocalName.Equals("inline") && !element.Parent.Name.LocalName.Equals("container"))
                        {
                            string value = element.Attribute("command").Value.Split(' ').Where(val => val.Contains(".xml")).FirstOrDefault();

                            if (!string.IsNullOrEmpty(value))
                            {
                                value = value.Replace(@"\", @"/");
                                string newFilePath = value.Replace("/templatedata/", "");
                                ParseXml(workAreaPath + newFilePath, list);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log("Error occured while Parsing the XML.");
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
                return list;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Teamsite")]
        protected void TeamsiteFields_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string[] tsFieldInfo = e.Item.DataItem as string[];

                Label label = e.Item.FindControl("tsFieldName") as Label;
                HiddenField hidden = e.Item.FindControl("tsFieldID") as HiddenField;
                DropDownList scFields = e.Item.FindControl("scFields") as DropDownList;

                label.Text = tsFieldInfo[0];
                hidden.Value = tsFieldInfo[1];

                scFields.DataSource = GetSCFields(tsFieldInfo[2], SCTemplates.SelectedValue);
                scFields.DataTextField = "Text";
                scFields.DataValueField = "Value";
                scFields.DataBind();

                scFields.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected List<ListItem> GetSCFields(string fieldType, string selected)
        {
            List<ListItem> fields = new List<ListItem>();
            if (selected == null)
                return null;
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    var template = _master.Templates[new ID(selected)];

                    if (template == null || template.Fields.Count() == 0)
                        return null;

                    if (fieldType.Equals("text"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && field.Type.Equals("Single-Line Text")))
                        {

                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("textarea"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && ((field.Type.Equals("Multi-Line Text") || field.Type.Equals("Rich Text") || field.Type.Equals("memo")))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("browser"))
                    {
                        var filteredFields = template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("General Link") || field.Type.Equals("Image")));

                        foreach (var field in filteredFields)
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("select"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Checklist") || field.Type.Equals("Droplist") || field.Type.Equals("DropLink") || field.Type.Equals("Droptree"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("checkbox") || fieldType.Equals("radio"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Checkbox"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("container"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Multilist") || field.Type.Equals("Treelist") || field.Type.Equals("TreelistEx") || field.Type.Equals("Multilist with Search"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else if (fieldType.Equals("date"))
                    {
                        foreach (var field in template.Fields.Where(field => !field.Name.StartsWith("__") && (field.Type.Equals("Date"))))
                        {
                            fields.Add(new ListItem(field.Name, field.ID.ToString()));
                        }
                        return fields;
                    }
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }

            }
            return fields;
        }

        protected void GetFieldMappings()
        {
            foreach (RepeaterItem rptItem in repeaterTSFields.Items)
            {
                if (rptItem.ItemType == ListItemType.Item || rptItem.ItemType == ListItemType.AlternatingItem)
                {
                    var label = rptItem.FindControl("tsFieldName") as Label;
                    var hidden = rptItem.FindControl("tsFieldID") as HiddenField;
                    var scFields = rptItem.FindControl("scFields") as DropDownList;

                    string teamSiteField = hidden.Value;
                    string sitecoreField = scFields.SelectedItem.Text;
                    string scFieldValue = scFields.SelectedValue;
                    int result;
                    bool fieldNotSelected = int.TryParse(scFieldValue, out result);

                    if (!fieldNotSelected)
                    {
                        lstSelectedTSFields.Add(teamSiteField);
                        lstSelectedSCFields.Add(sitecoreField);
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "dest"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected Item CreateItem(Item dest, MultiLogger logger, string itemName, IEnumerable<XElement> elementList, bool isProduct, string altText)
        {
            Item newItem = null;
            try
            {
                List<string> eleList = new List<string>();
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Language language = Language.Parse(Languages.SelectedValue);

                    newItem = _master.GetItem(dest.Paths.Path + "/" + itemName, language);
                    if (newItem != null)
                    {
                        bool hasLangVersion = HasLanguageVersion(newItem, Languages.SelectedValue);
                        if (!hasLangVersion)
                        {
                            newItem = newItem.Versions.AddVersion();
                            logger["debug"].Log("Added new {1} version for Item:{0}", newItem.Paths.Path, Languages.SelectedItem.Text);
                            versionCount++;
                        }
                        else
                        {
                            overwriteCount++;
                            logger["debug"].Log("Overwriting the item: {0} in {1} language", newItem.Paths.Path, Languages.SelectedItem.Text);
                        }
                    }
                    else
                    {
                        newItem = dest.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                        logger["debug"].Log("New item added {0} in {1}", newItem.Paths.Path, newItem.Language.Name);
                        itemCount++;
                    }

                    foreach (string item in lstSelectedTSFields)
                    {
                        foreach (var element in elementList)
                        {
                            if (element.Attribute("name").Value.Equals(item))
                            {
                                if (!eleList.Contains(element.Attribute("name").Value))
                                {
                                    if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Image")
                                    {
                                        MediaItem mediaImage = ImageFields(element, itemName, isProduct, string.Empty, altText);
                                        if (mediaImage != null)
                                        {
                                            newItem.Editing.BeginEdit();

                                            logger["debug"].Log("Editing image field : {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                            Sitecore.Data.Fields.ImageField iconImage = newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]];
                                            iconImage.Clear();
                                            iconImage.MediaID = mediaImage.ID;
                                            if (!string.IsNullOrEmpty(altText))
                                            {
                                                iconImage.Alt = altText;
                                            }

                                            logger["debug"].Log("Image Assigned {0}", iconImage.MediaItem.Paths.Path);

                                            newItem.Editing.EndEdit();

                                            logger["debug"].Log("Editing image field end");
                                            eleList.Add(element.Attribute("name").Value);
                                        }
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Date")
                                    {
                                        string dateValue = element.Value;
                                        string isoDate = Sitecore.DateUtil.ToIsoDate(DateTime.Parse(dateValue));

                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = isoDate;

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Single-Line Text"
                                        || newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Multi-Line Text"
                                        || newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "memo")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = Sitecore.StringUtil.RemoveTags(value);

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Rich Text")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;
                                        string newvalue = ProcessMediaInRichText(value);
                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = StringHtmlExtensions.RemoveAttributes(newvalue, new string[] { "class", "style", "width", "height" });

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else if (newItem.Fields[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]].Type == "Checkbox")
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = "0";
                                        var checkboxfields = element.Descendants().Where(x => x.Name.LocalName.Equals("value"));

                                        if (checkboxfields != null && checkboxfields.Count() > 0)
                                        {
                                            value = element.Value;
                                        }

                                        if (value.Equals("1"))
                                            newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = "1";
                                        else
                                            newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = string.Empty;

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                    else
                                    {
                                        newItem.Editing.BeginEdit();

                                        logger["debug"].Log("Editing field: {0}", lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]);

                                        string value = element.Value;

                                        newItem[lstSelectedSCFields[lstSelectedTSFields.IndexOf(item)]] = Sitecore.StringUtil.RemoveTags(value);

                                        newItem.Editing.EndEdit();

                                        logger["debug"].Log("Editing end");
                                        eleList.Add(element.Attribute("name").Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger["error"].Log(DateTime.Now.ToString());
                logger["error"].Log("Error occured while creating item {0} under {1}", itemName, dest.Paths.Path);
                logger["error"].Log(ex.Message);
                logger["error"].Log(ex.StackTrace);
            }

            return newItem;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected void ButtonMigrate_Click(object sender, EventArgs e)
        {
            MultiLogger logger;
            DateTime startTime = DateTime.Now;
            GetFieldMappings();

            using (logger = new MultiLogger())
            {
                logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "TrainingAndEducationDebug_{0}.txt", Languages.SelectedValue));
                logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "TrainingAndEducationError_{0}.txt", Languages.SelectedValue));

                logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "TrainingAndEducationMeta_{0}.txt", Languages.SelectedValue));
                logger["meta"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("Teamsite Path	ItemID	ItemPath    LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;
                string scPath = txtSCPath.Text;

                FileService fs = new FileService();
                //var fileList = fs.GetAllFiles(tsPath);
                var fileList = new string[] { tsPath };

                foreach (string filePath in fileList)
                {
                    try
                    {
                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);
                        var xDoc = XDocument.Load(bufferStream);

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item"));

                        Language language = Language.Parse(Languages.SelectedValue);

                        Item parentItem = _master.GetItem(scPath, language);

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                        string[] array = filePath.Split('/');

                        string itemName = string.Empty;
                        string folderName = string.Empty;

                        string id = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                 x.Attribute("name").Value.Equals("id")).FirstOrDefault().Value;

                        string title = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                 x.Attribute("name").Value.Equals("NameofClass")).FirstOrDefault().Value;

                        string legacyUrl = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                 x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;

                        if (ItemUtil.ProposeValidItemName(array[array.Length - 1]).Equals("index"))
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));
                        }
                        else
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 1]));
                        }

                        Item newItem = CreateItem(parentItem, logger, itemName, elementList, false, string.Empty);

                        if (newItem != null)
                        {
                            try
                            {
                                List<string> eleList = new List<string>();
                                Item accordianItem = null;
                                string content = string.Empty;
                                string boxLabel = string.Empty;
                                string toggleButton = string.Empty;

                                XElement contentElement = elementList.Descendants().FirstOrDefault(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("BoxContainer"));
                                if (contentElement != null && contentElement.DescendantNodes().Count() > 0)
                                {
                                    foreach (XElement valueNode in contentElement.Elements("value"))
                                    {
                                        boxLabel = valueNode.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("BoxLabel")).FirstOrDefault().Value;
                                        toggleButton = valueNode.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("toggleButton")).FirstOrDefault().Value; XElement contentNode = valueNode.Descendants().Where(x => x.Name.LocalName.Equals("item") && x.Attribute("name").Value.Equals("Content")).FirstOrDefault();

                                        if (contentNode != null && !string.IsNullOrEmpty(contentNode.Value))
                                        {
                                            content = contentNode.Value;
                                        }

                                        string accordianItemName = GetItemName(ItemUtil.ProposeValidItemName(boxLabel));
                                        using (new Sitecore.SecurityModel.SecurityDisabler())
                                        {
                                            accordianItem = _master.GetItem(newItem.Paths.Path + "/" + accordianItemName, language);
                                            if (accordianItem != null)
                                            {
                                                bool hasLangVersion = HasLanguageVersion(accordianItem, Languages.SelectedValue);
                                                if (!hasLangVersion)
                                                {
                                                    accordianItem = accordianItem.Versions.AddVersion();
                                                    accordianItem_versionCount++;
                                                    logger["debug"].Log("Added new {1} version for accordianItem:{0}", accordianItem.Paths.Path, Languages.SelectedItem.Text);
                                                }
                                                else
                                                {
                                                    accordianItem_overwriteCount++;
                                                    logger["debug"].Log("Overwriting the accordianItem: {0} in {1} language", accordianItem.Paths.Path, Languages.SelectedItem.Text);
                                                }
                                            }
                                            else
                                            {
                                                accordianItem = newItem.Add(accordianItemName, new TemplateID(Sitecore.Data.ID.Parse("{67120DE4-2FB4-4C1B-9ADA-F7071015A1D7}")));
                                                accordianItemCount++;
                                                logger["debug"].Log("New accordianItem added {0} in {1}", accordianItem.Paths.Path, accordianItem.Language.Name);
                                            }

                                            accordianItem.Editing.BeginEdit();
                                            logger["debug"].Log("Editing Item {0} started", accordianItemName);

                                            logger["debug"].Log("Editing Title field.");
                                            accordianItem["Title"] = boxLabel;

                                            logger["debug"].Log("Editing Description field.");
                                            accordianItem["Description"] = ProcessMediaInRichText(content);

                                            logger["debug"].Log("Editing OpenByDefault field.");
                                            if (toggleButton.ToLower().Equals("open"))
                                                accordianItem["OpenByDefault"] = "1";

                                            logger["debug"].Log("Editing End.");
                                            accordianItem.Editing.EndEdit();

                                            newItem.Editing.BeginEdit();
                                            logger["debug"].Log("Editing Item {0} started", title);

                                            logger["debug"].Log("Editing PageTitle field.");
                                            newItem["PageTitle"] = title ;

                                            logger["debug"].Log("Editing MenuTitle field.");
                                            newItem["MenuTitle"] = title;

                                            logger["debug"].Log("Editing MetaTitle field.");
                                            newItem["MetaTitle"] = title;

                                            logger["debug"].Log("Editing End.");
                                            newItem.Editing.EndEdit();
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                logger["error"].Log(DateTime.Now.ToString());
                                logger["error"].Log("Error occured while creating item {0} under {1}", itemName, parentItem.Paths.Path);
                                logger["error"].Log("{0}", filePath);
                                logger["error"].Log(ex.Message);
                                logger["error"].Log(ex.StackTrace);
                            }
                            logger["meta"].Log("{0}	{1}	{2}	{3}", filePath, newItem.ID.ToString(), newItem.Paths.Path, legacyUrl);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger["error"].Log("{0}", DateTime.Now.ToString());
                        logger["error"].Log("{0}", filePath);
                        logger["error"].Log("{0}", ex.Message);
                        logger["error"].Log("{0}", ex.StackTrace);
                    }
                }
                if (logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    logger["debug"].Log("-------------------------------------------------");
                    logger["debug"].Log("Time taken:{0}", duration);
                    logger["debug"].Log("New items added : {0}", itemCount);
                    logger["debug"].Log("Overwritten items : {0}", overwriteCount);
                    logger["debug"].Log("Total number of versions for items : {0}", versionCount);
                    logger["debug"].Log("New Accordian items added : {0}", accordianItemCount);
                    logger["debug"].Log("Overwritten Accordian items : {0}", accordianItem_overwriteCount);
                    logger["debug"].Log("Total number of versions for Accordian items : {0}", accordianItem_versionCount);
                    logger["debug"].Log("-------------------------------------------------");
                }
            }
        }
    }
}