﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Sitecore.Data.Managers;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class UpdateDownloadCategory : System.Web.UI.Page
    {
        MultiLogger _logger;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "_logger"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        protected void ButtonSubmit_OnClick(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                string delimiter = ",;";
                string filePath = Server.MapPath("~/App_Data/IP2Location/Download-Category-Values.csv.ods");
                MultiLogger _logger;
                using (_logger = new MultiLogger())
                {
                    _logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Download-Category-Error"));
                    #region Update sitecore item with the download file path
                    if (File.Exists(filePath))
                    {
                        var csvInfo = File.ReadAllLines(filePath)
                            .Where(line => !string.IsNullOrEmpty(line))
                            .Select(line => line.Split(delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

                        _logger["error"].Log("Execution Started");
                        var db = Sitecore.Configuration.Factory.GetDatabase("master");
                        foreach (var strLine in csvInfo)
                        {
                            var language = LanguageManager.GetLanguage(strLine[1]);

                            var downloadItem = db.GetItem(strLine[0], language);

                            if (downloadItem != null)
                            {
                                using (new SecurityDisabler())
                                {
                                    if (downloadItem.Versions.Count <= 0)
                                    {
                                        downloadItem.Versions.AddVersion();
                                    }
                                    downloadItem.Editing.BeginEdit();
                                    downloadItem["Value"] = strLine[2];
                                    downloadItem.Editing.EndEdit();
                                }
                            }
                        }
                    }
                    LabelSuccess.Visible = true;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                _logger["error"].Log("{0}", DateTime.Now.ToString());
                _logger["error"].Log("{0}", ex.Message);
                _logger["error"].Log("{0}", ex.StackTrace);
            }

            if (_logger != null)
            {
                TimeSpan ts = DateTime.Now - startTime;
                string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                _logger["error"].Log("-------------------------------------------------");
                _logger["error"].Log("Time taken:{0}", duration);
            }
        }
    }
}