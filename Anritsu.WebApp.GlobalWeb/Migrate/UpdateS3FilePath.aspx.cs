﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Sitecore.Configuration;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class UpdateS3FilePath : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSubmit_OnClick(object sender, EventArgs e)
        {
            #region Update S3 with meta information
            var logger = new MultiLogger();
            using (logger)
            {
                logger.Add("error", Settings.GetSetting("LogPath") + "Downloads-Update-S3-MetaInfo-00-0500.txt");
                var downloadquery = string.Format("fast:/sitecore/content/Global/downloads//*[@@templateid='{0}']", TemplateIds.Downloads);

                var db = Factory.GetDatabase("master");

                var downloadList = db.SelectItems(downloadquery).ToList();
                var filteredList = downloadList.Take(500).ToList();

                logger["error"].Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                logger["error"].Log("Begin Update S3 Meta information");
                logger["error"].Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

                logger["error"].Log("========================================");
                logger["error"].Log("Total Download File Count:");
                logger["error"].Log("{0}", downloadList.Count);
                logger["error"].Log("========================================");

                logger["error"].Log("========================================");
                logger["error"].Log("Total Filtered File Count:");
                logger["error"].Log("{0}", filteredList.Count);
                logger["error"].Log("========================================");

                var count = 0;

                foreach (var download in filteredList)
                {
                    count = count + 1;
                    logger["error"].Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    logger["error"].Log("Download Count:");
                    logger["error"].Log("{0}", count);
                    logger["error"].Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

                    foreach (var language in LanguageManager.GetLanguages(db))
                    {
                        var downloadItem = db.GetItem(download.ID.ToString(), language);

                        if (downloadItem != null && (downloadItem.Versions.Count > 0))
                        {
                            try
                            {
                                if (downloadItem.Fields["FilePath"] != null &&
                                    !string.IsNullOrEmpty(downloadItem.Fields["FilePath"].Value)
                                    && downloadItem.Language.Name.ToLower(CultureInfo.InvariantCulture)
                                        .Equals(downloadItem["FilePath"].Substring(0,
                                            downloadItem.Fields["FilePath"].Value.ToLower(CultureInfo.InvariantCulture)
                                                .IndexOf("/", StringComparison.Ordinal))))
                                {
                                    UpdateS3MetaInformation(downloadItem);
                                    logger["error"].Log("##############################################");
                                    logger["error"].Log("Updated S3 meta data Successfully for:");
                                    logger["error"].Log("DownloadId");
                                    logger["error"].Log("{0}", downloadItem.ID.ToString());
                                    logger["error"].Log("LanguageName");
                                    logger["error"].Log("{0}", language.Name);
                                    logger["error"].Log("FilePath");
                                    logger["error"].Log("{0}", downloadItem["FilePath"]);
                                    logger["error"].Log("##############################################");
                                }
                                //else
                                //{
                                //    logger["error"].Log("`````````````````````````````````````````");
                                //    logger["error"].Log("Download FilePath field is Null or the field value is empty for download or ll-cc of file path doesn't match item language:");
                                //    logger["error"].Log("DownloadId");
                                //    logger["error"].Log("{0}", downloadItem.ID.ToString());
                                //    logger["error"].Log("LanguageName");
                                //    logger["error"].Log("{0}", language.Name);
                                //    logger["error"].Log("FilePath");
                                //    logger["error"].Log("{0}", downloadItem["FilePath"]);
                                //    logger["error"].Log("`````````````````````````````````````````");
                                //}
                            }
                            catch (Exception ex)
                            {
                                logger["error"].Log("**********************************************");
                                logger["error"].Log("Updating S3 meta is not successfull for:");
                                logger["error"].Log("DownloadId");
                                logger["error"].Log("{0}", downloadItem.ID.ToString());
                                logger["error"].Log("LanguageName");
                                logger["error"].Log("{0}", language.Name);
                                logger["error"].Log("FilePath");
                                logger["error"].Log("{0}", downloadItem["FilePath"]);
                                logger["error"].Log("Error : ");
                                logger["error"].Log("{0}", ex.Message);
                                logger["error"].Log("**********************************************");
                            }
                        }
                    }
                }
                logger["error"].Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                logger["error"].Log("End Update S3 Meta information");
                logger["error"].Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                LabelSuccess.Visible = true;
                logger["error"].Log("Successfully Updated Meta Information");
            }
            #endregion
        }

        private void UpdateS3MetaInformation(Item currentItem)
        {
            var db = Factory.GetDatabase("master");
            var s3Client = GetS3Client();

            var request = new CopyObjectRequest
            {
                DestinationBucket = ConfigurationManager.AppSettings["AWSBucketName"],
                DestinationKey = currentItem["FilePath"],
                MetadataDirective = S3MetadataDirective.REPLACE,
                StorageClass = S3StorageClass.Standard,
                SourceBucket = ConfigurationManager.AppSettings["AWSBucketName"],
                SourceKey = currentItem["FilePath"]
            };

            request.Metadata.Add("x-amz-meta-SitecoreId", currentItem.ID.ToString());
            request.Metadata.Add("x-amz-meta-Region", currentItem.Language.Name);
            request.Metadata.Add("x-amz-meta-ItemPath", currentItem.Paths.Path);

            s3Client.CopyObject(request);
        }

        private AmazonS3Client GetS3Client()
        {
            var regionEndpoint = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["AWSEndPoint"]);
            var s3Client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSAccessKey"], ConfigurationManager.AppSettings["AWSSecretKey"], regionEndpoint);
            return s3Client;
        }
    }
}