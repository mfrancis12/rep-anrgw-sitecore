﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Excel;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Amazon.S3;
using Amazon.S3.Model;
using System.Configuration;
using System.Text;
using Amazon;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Anritsu.WebApp.SitecoreUtilities.ContentMigration;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Teamsite")]
    public partial class UpdateSitecoreWithActualS3PathCheckingMismatchFilePathFromTeamsite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        protected void ButtonSubmit_OnClick(object sender, EventArgs e)
        {
            string delimiter = ",;";
            var filePath = Server.MapPath("~/App_Data/IP2Location/Sitecore-Download-FilePath-zh-TW.csv");

            #region Update sitecore item with the download file path
            if (File.Exists(filePath))
            {
                var csvInfo = File.ReadAllLines(filePath)
                    .Where(line => !string.IsNullOrEmpty(line))
                    .Select(line => line.Split(delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

                var db = Sitecore.Configuration.Factory.GetDatabase("master");

                MultiLogger logger;
                using (logger = new MultiLogger())
                {
                    logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "DownloadsUpdateS3Path_Track_zh-TW.txt");
                    int count = 0;
                    foreach (var strLine in csvInfo)
                    {
                        logger["error"].Log("~~~~~~~~~~~~~~~~~~~~~");
                        logger["error"].Log("Actual S3 Path : " + strLine[2]);

                        logger["error"].Log("Download FilePath from Teamsite: " + strLine[1]);

                        var language = LanguageManager.GetLanguage("zh-TW");

                        var downloadItem = db.GetItem(strLine[0], language);

                        logger["error"].Log("Download Item Name : " + downloadItem.Name);

                        if (downloadItem.Versions.Count <= 0)
                        {
                            logger["error"].Log("Download Item Version doesn't exist for :" + strLine[0] + " In " + language.Name);
                            continue;
                        }
                        using (new SecurityDisabler())
                        {
                            count = count + 1;
                            downloadItem.Editing.BeginEdit();
                            //downloadItem["FilePath"] = strLine[2];
                            logger["error"].Log(strLine[2]);
                            downloadItem.Editing.EndEdit();
                        }
                        logger["error"].Log("Sitecore download filePath after update: " + downloadItem["FilePath"]);
                        logger["error"].Log("Update Count:" + count);
                        logger["error"].Log("~~~~~~~~~~~~~~~~~~~~~");
                    }
                }
            }
            LabelSuccess.Visible = true;
            #endregion
        }

        //private void updateS3MetaInformation(Item currentItem)
        //{
        //    try
        //    {
        //        Sitecore.Data.Database db = Sitecore.Configuration.Factory.GetDatabase("master");
        //        var s3Client = GetS3Client();

        //         Each user defined metadata will start with "x-amz-meta-"
        //        List<Item> globalProducts = (from regionalProduct in ((MultilistField)currentItem.Fields["RelatedProducts"]).GetItems()
        //                                     select db.GetItem(regionalProduct.ID.ToString(), LanguageManager.GetLanguage(currentItem.Language.Name))
        //                                         into productRegional
        //                                         where productRegional != null && ((ReferenceField)productRegional.Fields["SelectProduct"]).TargetItem != null
        //                                         select ((ReferenceField)productRegional.Fields["SelectProduct"]).TargetItem).ToList();

        //        var keywords = globalProducts.Select(product => db.GetItem(product.ID.ToString(), LanguageManager.GetLanguage(currentItem.Language.Name)))
        //            .Where(globalProduct => globalProduct != null)
        //            .Aggregate(string.Empty, (current, globalProduct) => current + globalProduct["ModelNumber"] + ",");

        //        var referenceItem =
        //            db.GetItem(
        //                ((ReferenceField)currentItem.Fields["DownloadCategoryType"]).TargetID, LanguageManager.GetLanguage(currentItem.Language.Name));

        //        var category = string.Empty;
        //        var subCategory = string.Empty;

        //        if (referenceItem != null)
        //        {
        //            subCategory = referenceItem["Value"];
        //            if (referenceItem.Parent != null)
        //            {
        //                category = referenceItem.Parent["Value"];
        //            }
        //        }

        //        var request = new CopyObjectRequest
        //        {
        //            DestinationBucket = ConfigurationManager.AppSettings["AWSBucketName"],
        //            DestinationKey = currentItem["FilePath"],
        //            MetadataDirective = S3MetadataDirective.REPLACE,
        //            StorageClass = S3StorageClass.Standard,
        //            SourceBucket = ConfigurationManager.AppSettings["AWSBucketName"],
        //            SourceKey = currentItem["FilePath"]
        //        };

        //        request.Metadata.Add("x-amz-meta-Area", "Downloads");
        //        request.Metadata.Add("x-amz-meta-Category", category);
        //        request.Metadata.Add("x-amz-meta-Sub-Category", subCategory);
        //        request.Metadata.Add("x-amz-meta-Keywords", keywords.TrimEnd(','));
        //        request.Metadata.Add("x-amz-meta-Region", referenceItem.Language.Name);

        //        s3Client.CopyObject(request);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error("ERROR: updating the s3 meta information on saving an item " + ex.Message, GetType());
        //    }
        //}

        //private AmazonS3Client GetS3Client()
        //{
        //    AmazonS3Client s3Client;
        //    RegionEndpoint regionEndpoint = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["AWSEndPoint"]);
        //    s3Client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSAccessKey"], ConfigurationManager.AppSettings["AWSSecretKey"], regionEndpoint);
        //    return s3Client;
        //}
    }
}