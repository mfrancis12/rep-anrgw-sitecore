﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using Excel;
using HtmlAgilityPack;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class Videos : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");

        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");
        int itemCount = 0, versionCount = 0, overwriteCount = 0, RelatedProdItemCount = 0, prodItemCount = 0, RelatedProdFamilyItemCount = 0, prodVideoItemCount = 0;

        StringBuilder sb ;
        List<string> links;

        UrlOptions urlOptions = new UrlOptions();

        private IEnumerable<TemplateItem> _sitecoreTemplates = null;

        private Dictionary<int, string> cultureID = new Dictionary<int, string>
        {
            {1,"en-US"},
            {2,"ja-JP"},
            {3,"en-GB"},
            {4,"en-AU"},
            {6,"zh-CN"},
            {7,"ko-KR"},
            {10,"zh-TW"},
            {12,"aa-GW"},
            {14,"ru-RU"},
            {15,"en-IN"}
        };

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));

                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected void ButtonVideos_Click(object sender, EventArgs e)
        {
            MultiLogger logger;
            DateTime startTime = DateTime.Now;

            using (logger = new MultiLogger())
            {
                logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrightcoveVideo_Debug_{0}.txt", Languages.SelectedValue));
                logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrightcoveVideo_Error_{0}.txt", Languages.SelectedValue));
                
                logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrightcoveVideo_Meta_{0}.txt", Languages.SelectedValue));
                logger["meta"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("Teamsite Path	VideoID	ItemID	ItemPath	LegacyUrl");

                logger.Add("metaProduct", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "BrightcoveVideo_MetaProduct_{0}.txt", Languages.SelectedValue));
                logger["metaProduct"].Log("-------------------------------------------------------------------");
                logger["metaProduct"].Log("Teamsite Path	VideoID	ItemID	ItemPath	LegacyUrl");
                
                logger.Add("external", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "ExternalLinks_{0}.txt", Languages.SelectedValue));
                logger["external"].Log("Teamsite Path   VideoID VideoItemID ItemPath");
                
                logger.Add("AnritsuTV", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "AnritsuTV_{0}.txt", Languages.SelectedValue));
                logger["AnritsuTV"].Log("Teamsite Path  VideoID VideoItemID ItemPath");
                
                logger.Add("others", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Others_{0}.txt", Languages.SelectedValue));
                logger["others"].Log("Teamsite Path VideoID VideoItemID ItemPath");

                logger.Add("Products", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "Products_{0}.txt", Languages.SelectedValue));
                logger["Products"].Log("Teamsite Path VideoID VideoItemID ItemPath");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;
                string scPath = txtSCPath.Text;

                FileService fs = new FileService();
                var fileList = fs.GetAllFiles(tsPath);
                //var fileList = new string[] { tsPath };

                foreach (string filePath in fileList)
                {
                    try
                    {
                        links = new List<string>();

                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);
                        var xDoc = XDocument.Load(bufferStream);

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item"));

                        Language language = Language.Parse(Languages.SelectedValue);

                        Item parentItem = _master.GetItem(scPath, language);

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                        string[] array = filePath.Split('/');


                        string itemName = GetItemName(array[array.Length - 1]);

                        string value = elementList.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(value);

                        string videoId = string.Empty;
                        if (doc.DocumentNode.SelectSingleNode("//param[@name=\"@videoPlayer\"]") != null)
                            videoId = doc.DocumentNode.SelectSingleNode("//param[@name=\"@videoPlayer\"]").GetAttributeValue("value", "");

                        Item videoItem = CreateItem(parentItem, itemName, logger, elementList, filePath);

                        if (videoItem != null)
                        {
                            string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");

                            #region code for Related Products
                            var relatedProductNodes = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                        x.Attribute("name").Value.Equals("relatedProducts"));

                            List<Item> productItemList = new List<Item>();

                            if (relatedProductNodes != null && relatedProductNodes.Count() > 0)
                            {
                                DataTable prodRegional = GetDataFromFile(path, "ProductRegional");
                                if (prodRegional != null && prodRegional.Rows.Count > 0)
                                {
                                    var nodes = relatedProductNodes.Descendants().Where(x => x.Name.LocalName.Equals("value"));
                                    foreach (var node in nodes)
                                    {
                                        string productItemId = string.Empty;
                                        var oldProductIds = node.Value.Replace("cid", ".").Split('.');
                                        var productRows = (from DataRow row in prodRegional.Rows
                                                           where (row["ProductID"].ToString().Equals(oldProductIds[0]))
                                                           select row);
                                        if (productRows != null && productRows.Count() > 0)
                                        {
                                            productItemId = productRows.Select(x => x["ItemID"]).FirstOrDefault().ToString();
                                        }
                                        if (string.IsNullOrEmpty(productItemId))
                                        {
                                            continue;
                                        }
                                        Language lang = Language.Parse(cultureID[Convert.ToInt32(oldProductIds[1])]);
                                        Item item = _master.GetItem(productItemId, lang);
                                        if (item != null)
                                        {
                                            productItemList.Add(item);
                                        }
                                    }
                                }
                            }

                            foreach (Item productItem in productItemList)
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    logger["debug"].Log("Editing item started");
                                    productItem.Editing.BeginEdit();

                                    logger["debug"].Log("Editing Videos field");
                                    if (string.IsNullOrEmpty(productItem["Videos"]))
                                        productItem["Videos"] = videoItem.ID.ToString();
                                    else
                                    {
                                        string arr = productItem["Videos"] + "|" + videoItem.ID.ToString();
                                        List<string> list = arr.Split('|').ToList<string>();
                                        productItem["Videos"] = string.Join("|", list.Distinct());
                                    }

                                    logger["debug"].Log("Editing End");
                                    productItem.Editing.EndEdit();

                                    links.Add(EditItem(productItem));
                                    RelatedProdItemCount++;

                                }
                                prodVideoItemCount++;
                                logger["metaProduct"].Log("{0}	{1}	{2} {3}", filePath, videoId, productItem.ID.ToString(), productItem.Paths.Path);
                            }
                            #endregion

                            #region code for Related Product Family
                            var relatedProductFamilyNodes = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                        x.Attribute("name").Value.Equals("relatedProductFamily"));

                            List<Item> productFamilyItemList = new List<Item>();
                            List<string> legacyFamilyIds = new List<string>();
                            int i = 0;
                            var familyNodes = relatedProductFamilyNodes.Descendants().Where(x => x.Name.LocalName.Equals("value"));
                            foreach (var node in familyNodes)
                            {
                                string productItemId = string.Empty;
                                var oldProductIds = node.Value.Replace("cid", ".").Split('.');
                                legacyFamilyIds.Add(oldProductIds[0]);
                                i++;
                            }

                            string familyIds = GetFamilyIds(legacyFamilyIds);
                            string[] FamilyItemIdsList = familyIds.Split('|');

                            foreach (string productFamilyItemID in FamilyItemIdsList)
                            {
                                Item productFamilyItem = _master.GetItem(productFamilyItemID, Language.Parse(Languages.SelectedValue));
                                if (productFamilyItem != null)
                                {
                                    links.Add(EditItem(productFamilyItem));
                                    RelatedProdFamilyItemCount++;
                                }
                            }
                            #endregion

                            using (new Sitecore.SecurityModel.SecurityDisabler())
                            {
                                logger["debug"].Log("Editing Related Links started");
                                videoItem.Editing.BeginEdit();

                                //videoItem["RelatedLinks"] = "<links>" + sb1.ToString() + sb2.ToString() + "</links>";                                
                                videoItem["RelatedLinks"] = "<links>" + string.Join(string.Empty, links.Distinct()) + "</links>";

                                logger["debug"].Log("Editing End");
                                videoItem.Editing.EndEdit();

                            }
                            logger["meta"].Log("{0}	{1} {2} {3}", filePath, videoId, videoItem.ID.ToString(), videoItem.Paths.Path);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger["error"].Log("{0}", DateTime.Now.ToString());
                        logger["error"].Log("{0}", ex.Message);
                        logger["error"].Log("{0}", ex.StackTrace);
                    }
                }
                if (logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    logger["debug"].Log("-------------------------------------------------");
                    logger["debug"].Log("Time taken:{0}", duration);
                    logger["debug"].Log("New video items added : {0}", itemCount);
                    logger["debug"].Log("Number of Product Items Edited Videos Field : {0}", prodVideoItemCount);
                    logger["debug"].Log("Number of Product Items Added to RelatedLinks Field : {0}", prodItemCount);
                    logger["debug"].Log("Number of Product Items Added to RelatedLinks Field (from Related Products Field) : {0}", RelatedProdItemCount);
                    logger["debug"].Log("Number of Product Items Added to RelatedLinks Field(from Related Products Family Field) : {0}", RelatedProdFamilyItemCount);
                    logger["debug"].Log("Overwritten video items : {0}", overwriteCount);
                    logger["debug"].Log("Total number of versions for video items : {0}", versionCount);
                    logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        protected Item CreateItem(Item item, string itemName, MultiLogger logger, IEnumerable<XElement> elementList, string filePath)
        {
            Item newItem = null;
            Item videoItem = null;
            try
            {
                string value = elementList.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(value);

                Language language = Language.Parse(Languages.SelectedValue);
                newItem = _master.GetItem(item.Paths.Path + "/" + itemName, language);

                string videoId = string.Empty;
                if (doc.DocumentNode.SelectSingleNode("//param[@name=\"@videoPlayer\"]") != null)
                    videoId = doc.DocumentNode.SelectSingleNode("//param[@name=\"@videoPlayer\"]").GetAttributeValue("value", "");

                videoItem = _master.SelectSingleItem("fast:/sitecore/media library/Media Framework/Accounts/AnritsuBrightcoveAccount/Media Content//*[@ID='" + videoId + "']");

                if (videoItem != null && !string.IsNullOrEmpty(videoId))
                {
                    string title = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("title")).FirstOrDefault().Value;

                    string ShortDesc = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("shortDescription")).FirstOrDefault().Value;

                    string LongDesc = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("longDescription")).FirstOrDefault().Value;

                    var metaValues = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("meta_values"));

                    string metaDesc = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("desc")).FirstOrDefault().Value;

                    string metaKeywords = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                    x.Attribute("name").Value.Equals("keywords")).FirstOrDefault().Value;

                    #region code for FieldSuite Related Links

                    HtmlDocument longDescDoc = new HtmlDocument();
                    longDescDoc.LoadHtml(LongDesc);

                    if (longDescDoc.DocumentNode.SelectSingleNode("//a") != null)
                    {
                        foreach (var nodes in longDescDoc.DocumentNode.SelectNodes("//a"))
                        {
                            if (nodes.GetAttributeValue("href", "").ToLower().Contains("/products-solutions/products/"))
                            {
                                string model = nodes.GetAttributeValue("href", "").ToLower().Split('/').Last();
                                string modelNumber = model.Contains('.') ? model.Split('.')[0] : model;

                                Item productItem = _master.SelectSingleItem("/sitecore/content/GlobalWeb/home//*[@@templatename='ProductRegional' and @@name='" + ItemUtil.ProposeValidItemName(modelNumber) + "']");
                                if (productItem != null)
                                {
                                    links.Add(EditItem(productItem));
                                    prodItemCount++;
                                }
                                logger["Products"].Log("{0}	{1}	{2} {3}", filePath, videoId, productItem.ID.ToString(), videoItem.ID.ToString());
                            }
                            else if (nodes.GetAttributeValue("href", "").ToLower().StartsWith("http://www.anritsu.tv/"))
                            {
                                logger["AnritsuTV"].Log("{0}	{1}	{2} {3}", filePath, videoId, videoItem.ID.ToString(), videoItem.Paths.Path);
                            }
                            else if (nodes.GetAttributeValue("href", "").ToLower().StartsWith("/"))
                            {
                                logger["external"].Log("{0}	{1}	{2} {3}", filePath, videoId, videoItem.ID.ToString(), videoItem.Paths.Path);
                            }
                            else
                            {
                                //sb1.Append("<link linktype ='external' ");
                                //sb1.Append("url='" + nodes.GetAttributeValue("href", "").ToLower() + "' ");
                                //sb1.Append("target='" + nodes.GetAttributeValue("tagret", "").ToLower() + "' ");
                                //sb1.Append("linktext='" + HttpUtility.HtmlDecode(nodes.InnerText) + "' />");

                                logger["others"].Log("{0}	{1}	{2} {3}", filePath, videoId, videoItem.ID.ToString(), videoItem.Paths.Path);
                            }
                        }
                    }
                    #endregion

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        if (newItem != null)
                        {
                            bool hasLangVersion = HasLanguageVersion(newItem, Languages.SelectedValue);
                            if (!hasLangVersion)
                            {
                                newItem.Versions.AddVersion();
                                logger["debug"].Log("Added new {1} version for Item:{0}", newItem.Paths.Path, Languages.SelectedItem.Text);
                                versionCount++;
                            }
                            else
                            {
                                overwriteCount++;
                                logger["debug"].Log("Overwriting the item: {0} in {1} language", newItem.Paths.Path, Languages.SelectedItem.Text);
                            }
                        }
                        else
                        {
                            newItem = item.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                            logger["debug"].Log("New item added {0} in {1}", newItem.Paths.Path, newItem.Language.Name);
                            itemCount++;
                        }


                        newItem.Editing.BeginEdit();

                        logger["debug"].Log("Editing Title field");
                        newItem["Name"] = Sitecore.StringUtil.RemoveTags(title);

                        logger["debug"].Log("Editing Short Description field");
                        newItem["ShortDescription"] = Sitecore.StringUtil.RemoveTags(ShortDesc);

                        logger["debug"].Log("Editing Long Description field");
                        newItem["LongDescription"] = StringHtmlExtensions.RemoveAttributes(LongDesc, new string[] { "class", "style" });

                        logger["debug"].Log("Editing Menu Title field");
                        newItem["MenuTitle"] = Sitecore.StringUtil.RemoveTags(title);

                        logger["debug"].Log("Editing Meta Title field");
                        newItem["MetaTitle"] = Sitecore.StringUtil.RemoveTags(title);

                        logger["debug"].Log("Editing Page Title field");
                        newItem["PageTitle"] = Sitecore.StringUtil.RemoveTags(title);

                        logger["debug"].Log("Editing Meta Description field");
                        newItem["Metatags-Description"] = metaDesc;

                        logger["debug"].Log("Editing Meta Keywords field");
                        newItem["Metatags-Other keywords"] = metaKeywords;

                        logger["debug"].Log("Editing Video field");
                        newItem["Video"] = videoItem.ID.ToString();

                        logger["debug"].Log("Editing end");
                        newItem.Editing.EndEdit();
                    }
                }
            }
            catch (Exception ex)
            {
                logger["error"].Log(DateTime.Now.ToString());
                logger["error"].Log("Error occured while creating item {0} under {1}", itemName, item.Paths.Path);
                logger["error"].Log(ex.Message);
                logger["error"].Log(ex.StackTrace);
            }

            return newItem;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1306:SetLocaleForDataTypes"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "Extension"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "isHDR"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sheetName")]
        private DataTable GetDataFromFile(string FilePath, string sheetName)
        {
            MultiLogger _logger = null;
            DataTable dt = new DataTable();
            using (_logger = new MultiLogger())
            {
                _logger.Add("error", @Sitecore.Configuration.Settings.GetSetting("LogPath") + "Migration_Error.txt");
                try
                {
                    FileStream stream = File.Open(FilePath, FileMode.Open, FileAccess.Read);

                    //Reading from a OpenXml Excel file (2007 format; *.xlsx)                    
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                    //DataSet - Create column names from first row
                    excelReader.IsFirstRowAsColumnNames = true;

                    //DataSet - The result of each spreadsheet will be created in the result.Tables
                    DataSet result = excelReader.AsDataSet();

                    //6. Free resources (IExcelDataReader is IDisposable)
                    excelReader.Close();

                    dt = result.Tables[sheetName];
                }
                catch (Exception ex)
                {
                    _logger["error"].Log(DateTime.Now.ToShortDateString());
                    _logger["error"].Log(ex.Message);
                    _logger["error"].Log(ex.StackTrace);
                }
            }
            return dt;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public string GetFamilyIds(List<string> legacyFamilyIds)
        {
            string familyIds = string.Empty;
            string path = Sitecore.Configuration.Settings.GetSetting("SpecificationExcel");

            if (legacyFamilyIds != null && legacyFamilyIds.Count() > 0)
            {
                DataTable productFamiyAssoc = GetDataFromFile(path, "SCProdFamilyAssoc");
                var familyItemIds = (from DataRow row in productFamiyAssoc.Rows
                                     where legacyFamilyIds.Contains(row["FamilyID"].ToString())
                                     select row);
                familyIds = string.Join("|", familyItemIds.Select(x => x["ItemID"].ToString()));
            }
            return familyIds;
        }

        protected string EditItem(Item item)
        {
            sb = new StringBuilder();
            sb.Append("<link id ='" + item.ID.ToString() + "' ");
            sb.Append("linkid='" + System.Guid.NewGuid().ToString() +"' ");
            sb.Append("linktype ='internal' ");
            //sb.Append("linktext ='" + item.Name.ToUpperInvariant() + "' ");
            string linkText = item.Name.ToUpperInvariant();
            if(item.TemplateName.Equals("ProductRegional"))
            {
                string globalProductId = item["SelectProduct"];
                Item globalProduct = _master.GetItem(globalProductId,Language.Parse(Languages.SelectedValue));
                if(globalProduct !=null && globalProduct.Versions.Count > 0)
                {
                    linkText = globalProduct["ModelNumber"];
                }                
            } else if(item.TemplateName.Equals("ProductFamily"))
            {
                Item family = _master.GetItem(item.ID.ToString(),Language.Parse(Languages.SelectedValue));
                if(family != null && family.Versions.Count > 0)
                {
                    linkText = item["PageTitle"];
                }                
            }
            sb.Append("linktext ='" + linkText + "' ");
            urlOptions.Language = Language.Parse(Languages.SelectedValue);
            sb.Append("url='" + LinkManager.GetItemUrl(item, urlOptions) + "' />");

            return sb.ToString();
        }
    }
}