﻿using Anritsu.WebApp.SitecoreUtilities.ContentMigration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using ContentMigration.Services;
using Excel;
using HtmlAgilityPack;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Anritsu.WebApp.GlobalWeb.Migrate
{
    public partial class WebinarVideo : System.Web.UI.Page
    {
        private readonly Database _master = Sitecore.Data.Database.GetDatabase("master");

        private string scTemplateRoot = Sitecore.Configuration.Settings.GetSetting("SC_TemplateRoot");
        int itemCount = 0, versionCount = 0, overwriteCount = 0;

        StringBuilder sb;
        List<string> links;

        UrlOptions urlOptions = new UrlOptions();

        private IEnumerable<TemplateItem> _sitecoreTemplates = null;

        protected IEnumerable<TemplateItem> SitecoreTemplates
        {
            get
            {
                if (_sitecoreTemplates == null)
                {
                    Language enLanguage = Sitecore.Globalization.Language.Parse("en");
                    var templates = _master.Templates.GetTemplates(enLanguage)
                                    .Where(template => template.InnerItem.Paths.Path.StartsWith(scTemplateRoot, StringComparison.CurrentCultureIgnoreCase)
                                            || template.InnerItem.Paths.Path.StartsWith("/sitecore/templates/Global", StringComparison.CurrentCultureIgnoreCase));

                    _sitecoreTemplates = templates;
                }
                return _sitecoreTemplates;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IEnumerable<TemplateItem> templates = SitecoreTemplates;
                if (templates == null || templates.Count() == 0)
                    return;

                foreach (var template in templates)
                {
                    SCTemplates.Items.Add(new ListItem(template.Name, template.ID.ToString()));
                }

                SCTemplates.Items.Insert(0, new ListItem("--select--", "0"));

                var languages = LanguageManager.GetLanguages(_master);
                foreach (var language in languages)
                {
                    Languages.Items.Add(new ListItem(language.CultureInfo.DisplayName, language.Name));
                }
                Languages.Items.Insert(0, new ListItem("--select--", "0"));
            }
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            Language language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                Item languageSpecificItem = global::Sitecore.Configuration.Factory.GetDatabase("master").GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected void ButtonVideos_Click(object sender, EventArgs e)
        {
            MultiLogger logger;
            DateTime startTime = DateTime.Now;

            using (logger = new MultiLogger())
            {
                logger.Add("debug", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "WebinarVideo_Debug_{0}.txt", Languages.SelectedValue));
                logger.Add("error", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "WebinarVideo_Error_{0}.txt", Languages.SelectedValue));
                logger.Add("meta", string.Format(@Sitecore.Configuration.Settings.GetSetting("LogPath") + "WebinarVideo_Meta_{0}.txt", Languages.SelectedValue));
                logger["meta"].Log("-------------------------------------------------------------------");
                logger["meta"].Log("Teamsite Path	ItemID	ItemPath	LegacyUrl");

                if (string.IsNullOrEmpty(txtTSDCRPath.Text) || Languages.SelectedIndex == 0)
                {
                    logger["error"].Log("DCR Path is empty or Language is not selected.");
                    return;
                }

                string tsPath = txtTSDCRPath.Text;
                string scPath = txtSCPath.Text;

                FileService fs = new FileService();
                //var fileList = fs.GetAllFiles(tsPath);
                var fileList = new string[] { tsPath };

                foreach (string filePath in fileList)
                {
                    try
                    {
                        links = new List<string>();

                        var fileByteContent = fs.GetFileContent(filePath);
                        var bufferStream = new MemoryStream(fileByteContent);
                        var xDoc = XDocument.Load(bufferStream);

                        IEnumerable<XElement> elementList = xDoc.Root.Descendants().Where(x => x.Name.LocalName.Equals("item"));

                        Language language = Language.Parse(Languages.SelectedValue);

                        Item parentItem = _master.GetItem(scPath, language);

                        if (string.IsNullOrEmpty(scPath) || parentItem == null)
                        {
                            logger["error"].Log("Sitecore path is empty or No item matched the path.");
                            continue;
                        }

                        logger["debug"].Log("Parent Item Path : {0}", parentItem.Paths.Path);

                        string[] array = filePath.Split('/');
                        string itemName = string.Empty;
                        if (ItemUtil.ProposeValidItemName(array[array.Length - 1]).Equals("index"))
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 2]));

                        }
                        else
                        {
                            itemName = GetItemName(ItemUtil.ProposeValidItemName(array[array.Length - 1]));
                        }

                        try
                        {
                            string value = elementList.Descendants().Where(x => x.Name.LocalName.Equals("value")).FirstOrDefault().Value;
                            HtmlDocument doc = new HtmlDocument();
                            doc.LoadHtml(value);

                            string videoId = string.Empty;
                            if (doc.DocumentNode.SelectSingleNode("//param[@name=\"@videoPlayer\"]") != null)
                                videoId = doc.DocumentNode.SelectSingleNode("//param[@name=\"@videoPlayer\"]").GetAttributeValue("value", "");

                            Item videoItem = _master.SelectSingleItem("fast:/sitecore/media library/Media Framework/Accounts/AnritsuBrightcoveAccount/Media Content//*[@ID='" + videoId + "']");

                            if (videoItem != null && !string.IsNullOrEmpty(videoId))
                            {
                                string title = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("page_title")).FirstOrDefault().Value;

                                var windowTitleElement = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("windowTitle"));

                                string windowTitle = windowTitleElement.FirstOrDefault() != null ?  
                                    windowTitleElement.FirstOrDefault().Value : title;

                                string legacyUrl = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("PageUrl")).FirstOrDefault().Value;

                                var metaValues = elementList.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("meta_values"));

                                string metaDesc = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("desc")).FirstOrDefault().Value;

                                string metaKeywords = metaValues.Descendants().Where(x => x.Name.LocalName.Equals("item") &&
                                                                x.Attribute("name").Value.Equals("keywords")).FirstOrDefault().Value;

                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    string folderName = array[array.Length - 2];

                                    Item yearFolderItem = _master.GetItem(parentItem.Paths.Path + "/" + folderName , language);
                                    Item videoFolderItem = _master.GetItem(yearFolderItem.Paths.Path + "/" + "videos", language);

                                    if (videoFolderItem != null)
                                    {
                                        bool hasLangVersion = HasLanguageVersion(videoFolderItem, Languages.SelectedValue);
                                        if (!hasLangVersion)
                                        {
                                            videoFolderItem = videoFolderItem.Versions.AddVersion();
                                            logger["debug"].Log("Added new {1} version for Item:{0}", videoFolderItem.Paths.Path, Languages.SelectedItem.Text);
                                        }
                                        else
                                        {
                                            logger["debug"].Log("Overwriting the item: {0} in {1} language", videoFolderItem.Paths.Path, Languages.SelectedItem.Text);
                                        }
                                    }
                                    else
                                    {
                                        videoFolderItem = yearFolderItem.Add("videos", new TemplateID(Sitecore.Data.ID.Parse("{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}")));
                                        logger["debug"].Log("New Video Folder item added {0} in {1}", videoFolderItem.Paths.Path, videoFolderItem.Language.Name);
                                    }

                                    Item webinarVideoItem = _master.GetItem(videoFolderItem.Paths.Path + "/" + itemName, language);

                                    if (webinarVideoItem != null)
                                    {
                                        bool hasLangVersion = HasLanguageVersion(webinarVideoItem, Languages.SelectedValue);
                                        if (!hasLangVersion)
                                        {
                                            webinarVideoItem.Versions.AddVersion();
                                            logger["debug"].Log("Added new {1} version for Item:{0}", webinarVideoItem.Paths.Path, Languages.SelectedItem.Text);
                                            versionCount++;
                                        }
                                        else
                                        {
                                            overwriteCount++;
                                            logger["debug"].Log("Overwriting the item: {0} in {1} language", webinarVideoItem.Paths.Path, Languages.SelectedItem.Text);
                                        }
                                    }
                                    else
                                    {
                                        webinarVideoItem = videoFolderItem.Add(itemName, new TemplateID(Sitecore.Data.ID.Parse(SCTemplates.SelectedValue)));
                                        logger["debug"].Log("New item added {0} in {1}", webinarVideoItem.Paths.Path, webinarVideoItem.Language.Name);
                                        itemCount++;
                                    }

                                    webinarVideoItem.Editing.BeginEdit();
                                    logger["debug"].Log("Editing Item {0} started", webinarVideoItem.Name);

                                    logger["debug"].Log("Editing MenuTitle field");
                                    webinarVideoItem["MenuTitle"] = title;

                                    logger["debug"].Log("Editing PageTitle field");
                                    webinarVideoItem["PageTitle"] = title;

                                    logger["debug"].Log("Editing MetaTitle field");
                                    webinarVideoItem["MetaTitle"] = windowTitle;

                                    logger["debug"].Log("Editing Meta Description field");
                                    webinarVideoItem["Metatags-Description"] = metaDesc;

                                    logger["debug"].Log("Editing Meta Keywords field");
                                    webinarVideoItem["Metatags-Other keywords"] = metaKeywords;

                                    logger["debug"].Log("Editing Video field");
                                    webinarVideoItem["Video"] = videoItem.ID.ToString();

                                    logger["debug"].Log("Editing end");
                                    webinarVideoItem.Editing.EndEdit();
                                }

                                logger["meta"].Log("{0}	{1} {2} {3}", filePath, videoId, videoItem.ID.ToString(), videoItem.Paths.Path);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger["error"].Log(DateTime.Now.ToString());
                            logger["error"].Log(filePath);
                            logger["error"].Log(ex.Message);
                            logger["error"].Log(ex.StackTrace);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger["error"].Log("{0}", DateTime.Now.ToString());
                        logger["error"].Log("{0}", ex.Message);
                        logger["error"].Log("{0}", ex.StackTrace);
                    }
                }
                if (logger != null)
                {
                    TimeSpan ts = DateTime.Now - startTime;
                    string duration = string.Format("{0}:{1}:{2}", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                    logger["debug"].Log("-------------------------------------------------");
                    logger["debug"].Log("Time taken:{0}", duration);
                    logger["debug"].Log("New video items added : {0}", itemCount);
                    logger["debug"].Log("Overwritten video items : {0}", overwriteCount);
                    logger["debug"].Log("Total number of versions for video items : {0}", versionCount);
                    logger["debug"].Log("-------------------------------------------------");
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
        public string GetItemName(string name)
        {
            return ItemUtil.ProposeValidItemName(name).Replace(' ', '-').Replace('_', '-').ToLowerInvariant();
        }
    }
}