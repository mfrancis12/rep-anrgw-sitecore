﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RenameItems.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.RenameItems" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Enter Item/Folder Path : <asp:TextBox ID="TextBoxFolderPath" runat="server" Width="600px"></asp:TextBox><br />
        <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" /><br />
        <asp:Label ID="LabelSuccessMessage" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
