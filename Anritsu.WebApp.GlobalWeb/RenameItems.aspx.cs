﻿using System;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;

namespace Anritsu.WebApp.GlobalWeb
{
    public partial class RenameItems : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            Database master = Sitecore.Configuration.Factory.GetDatabase("master");
            Item parentItem = master.GetItem(TextBoxFolderPath.Text, LanguageManager.GetLanguage("en"));
            if (parentItem != null)
            {
                RenameItem(parentItem);

                if (parentItem.HasChildren)
                {
                    foreach (Item childItem in parentItem.Axes.GetDescendants())
                    {
                        RenameItem(childItem);
                    }
                }
            }

            LabelSuccessMessage.Text = "Items has been renamed successfully";
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        private void RenameItem(Item currentItem)
        {
            if (currentItem != null)
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    currentItem.Editing.BeginEdit();
                    try
                    {
                        var itemName = currentItem.Name;
                        currentItem.Name = itemName.ToLower().Replace(" ", "-");
                        currentItem[Sitecore.FieldIDs.DisplayName] = itemName.ToLower().Replace(" ", "-");
                        currentItem.Editing.EndEdit();
                    }
                    catch (Exception ex)
                    {
                        Sitecore.Diagnostics.Log.Error(ex.Message, ex.GetType());
                        currentItem.Editing.CancelEdit();
                    }
                }
            }
        }
    }
}