function getBrowserSupportText(e, t) {
    var r = "title" == t ? enWarningTitle : enWarningMsg;
    if (null == e || e.length < 2) return r;
    var a = e.substring(0, 2).toLowerCase();
    switch ("zh" == a && (a = e.toLowerCase()), a) {
        case "ja":
            r = "title" == t ? jaWarningTitle : jaWarningMsg;
            break;
        case "ko":
            r = "title" == t ? koWarningTitle : koWarningMsg;
            break;
        case "ru":
            r = "title" == t ? ruWarningTitle : ruWarningMsg;
            break;
        case "zh-tw":
            r = "title" == t ? twWarningTitle : twWarningMsg;
            break;
        case "zh-cn":
            r = "title" == t ? cnWarningTitle : cnWarningMsg
    }
    return r
}

function getIEVersion() {
    var e = -1;
    if ("Microsoft Internet Explorer" == navigator.appName) {
        var t = navigator.userAgent,
            r = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
        null != r.exec(t) && (e = parseFloat(RegExp.$1))
    }
    return e
}

function getFireFoxVersion() {
    var e = -1;
    if ("netscape" == navigator.appName.toLowerCase()) {
        var t = navigator.userAgent.toLowerCase(),
            r = new RegExp("firefox/([0-9]{1,}[.0-9]{0,})");
        null != r.exec(t) && (e = parseFloat(RegExp.$1))
    }
    return e
}

function getSafariVersion() {
    var e = -1;
    if ("netscape" == navigator.appName.toLowerCase()) {
        var t = navigator.userAgent.toLowerCase(),
            r = new RegExp("safari/([0-9]{1,}[.0-9]{0,})");
        null != r.exec(t) && (r = new RegExp("version/([0-9]{1,}[.0-9]{0,})"), null != r.exec(t) && (e = parseFloat(RegExp.$1)))
    }
    return e
}

function getChromeVersion() {
    var e = -1;
    if ("netscape" == navigator.appName.toLowerCase()) {
        var t = navigator.userAgent.toLowerCase(),
            r = new RegExp("chrome/([0-9]{1,}[.0-9]{0,})");
        null != r.exec(t) && (e = parseFloat(RegExp.$1))
    }
    return e
}

function isUnSupportedIE() {
    var e = getFireFoxVersion(),
        t = getIEVersion(),
        r = getSafariVersion(),
        a = getChromeVersion()
    return t > -1 && 10 >= t ? !0 : e > -1 && 53 > e ? !0 : r > -1 && 9 > r ? !0 : a > -1 && 58 > a ? !0 : t > -1 && document.documentMode && parseInt(document.documentMode) < 10 ? !0 : !1
}

function isMobile() {
    var e = navigator.userAgent || navigator.vendor || window.opera;
    return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(e) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0, 4))
}

function CheckUnSupportedBrowser(e) {
    !isMobile() && isUnSupportedIE() && ($(function() {
        function e() {
            $("#badbwrcontainer").css({
                position: "absolute",
                top: $(window).scrollTop() + $(window).height() - $("#badbwrcontainer").height() + "px"
            })
        }
        e(), $(window).scroll(e).resize(e)
    }), $(function() {
        $(UnSupportedBrowserMsgHtml(e)).appendTo("#bwrsupport"), $("#badbwr").show()
    }), $(document).ready(function() {
		if(ieEdge()){
			$("#bwrsupport, #badbwrcontainer, #badbwr").hide();
		}
        $("#badbwrTitleMsg img").click(function() {
            $("#badbwrBody").slideToggle("slow");
            var e = "https://static.cdn-anritsu.com/images/down.png" === $(this).attr("src") ? "https://static.cdn-anritsu.com/images/up.png" : "https://static.cdn-anritsu.com/images/down.png";
            return $(this).attr("src", e), !1
        })
    })
		
	)
}

function UnSupportedBrowserMsgHtml(e) {
    var t = "";
    return t += "<div id='badbwrcontainer'>", t += "<div id='badbwr'>", t += "<div class='badbwrTitle'>", t += "<a id='badbwrTitleMsg' href='#' class='msgTitle'><span id='badbwrTitleText'>" + getBrowserSupportText(e, "title") + "</span><img src='https://static.cdn-anritsu.com/images/down.png' /></a>", t += "</div>", t += "<div id='badbwrBody' class='warningText'>", t += "<p id='badbwrBodyText'>" + getBrowserSupportText(e, "body") + "</p>", t += "<div class='browserLogos'>", t += "<a href='http://www.mozilla.org' target='_blank'><img src='https://dl.cdn-anritsu.com/appfiles/img/browser%20icons/firefox.png' alt='Mozilla Firefox' width='65px'/></a>", t += "<a href='http://www.apple.com/safari/' target='_blank'><img src='https://dl.cdn-anritsu.com/appfiles/img/browser%20icons/safari.png' alt='Safari' width='65px'/></a>", t += "<a href='http://www.google.com/intl/en/chrome/browser/' target='_blank'><img src='https://dl.cdn-anritsu.com/appfiles/img/browser%20icons/chrome.png' alt='Chrome' width='65px' /></a>", t += "<a href='http://windows.microsoft.com/en-us/windows/home' target='_blank'><img src='https://dl.cdn-anritsu.com/appfiles/img/browser%20icons/ie.png' alt='Internet Explorer' width='65px' /></a>", t += "</div></div>", t += "</div>", t += "</div>"
}

function ieEdge(){
	if (/Edge\/\d./i.test(navigator.userAgent)){
	   // This is Microsoft Edge
	   $("#bwrsupport, #badbwrcontainer, #badbwr").hide();
	}
}
ieEdge();

var enWarningTitle = "This page requires you to upgrade your browser.",
    enWarningMsg = "To get the best possible experience using our website, we recommend that you upgrade to a newer version or other web browser. Just click on the icons to upgrade or install a new browser.",
    jaWarningTitle = "このページを閲覧するにはブラウザのアップグレードが必要です。",
    jaWarningMsg = "弊社のウェブサイトを快適にご覧いただくために、新しいバージョンへのアップグレードまたは他のウェブブラウザのご利用をおすすめします。下のアイコンをクリックするだけでアップグレードまたは新しいブラウザのインストールができます。",
    koWarningTitle = "이 페이지는 브라우저 업그레이드가 필요합니다.",
    koWarningMsg = "이 웹사이트를 최적으로 이용하기 위해서 다른 웹프라우저나 더 높은 버전으로 업그레이드할 것을 추천합니다. 이 아이콘을 클릭하여 새로운 브라우저로 업그레이드하거나 설치하시면 됩니다.",
    twWarningTitle = "该网页要求升级您的浏览器。",
    twWarningMsg = "为了得到最佳的使用体验，我们建议您升级网页浏览器或下载其他的网页浏览器。点击按钮即可升级或安装为新的浏览器。",
    cnWarningTitle = "請升級您的網頁瀏覽器以完整瀏覽此頁面",
    cnWarningMsg = "為使您在使用Anritsu安立知網站時擁有最佳的體驗，我們建議您將網頁瀏覽器升級到較新的版本。只需點擊以下圖示，即可升級或安裝新的網頁瀏覽器。",
    ruWarningTitle = "Для корректного отображения страницы требуется обновить браузер.",
    ruWarningMsg = "Для того чтобы использовать все возможности нашего сайта, мы рекомендуем обновить версию браузера или установить новый. Просто нажмите на иконки для обновления или установки нового браузера.";
