$(function(){
  initSearch();
  initMenu();
  announcement();
  result_filter();

  if (/iPhone|iPad/i.test(navigator.userAgent)) {
    $(".pusher").css("position","absolute")
    $(".aside-menu-trigger").css("position","absolute")
  } 

  $(".video-link").each(function(i,item){
    $(item).click(function(){
      $(item).toggleClass("on")
    })
  })


  $(".tab-interface").each(function(i,item){
    var tab = $(item).children("ul");
    var tabContent = $(item).children(".section")
    $(tab).find("li a").each(function(index,el){
      $(el).click(function(e){
        e.stopPropagation()
        e.preventDefault()
        $(tab).find("li a").removeClass("active")
        $(el).addClass("active")
        tabContent.removeClass("active")
        tabContent.eq(index).addClass("active")
      })
   })
  })
var totop = $("#totop")
  totop.click(function(){
    $("body, html").animate({
      scrollTop:0
    },500)
  })

var asideHelper = $(".aside-helper")
asideHelper.css("display","none")  
$(window).scroll(function(){
  if($(window).scrollTop() > 0){
    asideHelper.css("display","block")
  }else{
    asideHelper.css("display","none")
  }
})

});

var resetslider = function(){
  
    if (typeof homeSlider !== 'undefined'){
      setTimeout(function(){
        homeSlider.slick("setPosition")
      },500)
    }
    if (typeof categorySlider !== 'undefined'){
      setTimeout(function(){
        categorySlider.slick("setPosition")
      },500)
    }
    if(typeof newProductsSlider !== 'undefined'){
      setTimeout(function(){
        newProductsSlider.slick("setPosition")
      },500)
    }
    // when open  aside-menu , close the popup element
    $(".search-popup").removeClass("active")
    $(".country-popup").removeClass("active")
    $(".country-confirm").removeClass("active")
}

var announcement = function(){
  var maxlen = $(".announcement .text").length
  var len = 0;
  $(".announcement .text").eq(0).addClass("active");
  setInterval(function(){
    if(len >= maxlen-1){
      len = 0
    }else{
      len++;
    }
    $(".announcement .text").css("opacity","0").removeClass("active");
    $(".announcement .text").eq(len).addClass("active").transition({
      opacity:"1",
      duration:1000,
      easing:'out'
    });
  },5000)
}
var initMenu = function(){
  $(".aside-menu-trigger .icon").click(function(){
    $("body").toggleClass("open-aside-menu")
    //for reset the position for the slider
    //if you create a news slider make sure add the name here
    resetslider()

    $(".aside-menu .menu-level1").css('left','0px');
    $(".aside-menu .menu-level2").css('left','-280px');
    $(".aside-menu .menu-level3").css('left','-280px');
  })
  $(".aside-menu a").each(function(i,item){
    $(item).click(function(e){
      if($(item).hasClass("haschild")){
        e.stopPropagation();
        $(item).parent().parent().css("left","0px").transition({
          left:"-280px",
          duration:300,
          easing: 'in',
        })
        var targetId = $(item).attr("data-target");
        $("#"+targetId).css("left","280px").transition({
          left:"0px",
          duration:300,
          easing: 'in',
        })
      }

      if($(item).hasClass("back")){
        e.stopPropagation();
        e.preventDefault();
        $(item).parent().parent().css("left","0px").transition({
          left:"280px",
          duration:300,
          easing: 'in',
        })
        var targetId = $(item).attr("data-target");
        $("#"+targetId).css("left","-280px").transition({
          left:"0px",
          duration:300,
          easing: 'in',
        })
      }

    })
  })
  $(".aside-menu .backhome").click(function(e){
    e.stopPropagation();
    e.preventDefault();
    $("body").removeClass("open-aside-menu");
    resetslider()
    $(".aside-menu .menu-level1").css('left','0px');
    $(".aside-menu .menu-level2").css('left','-280px');
    $(".aside-menu .menu-level3").css('left','-280px');
  })
} 
var initSearch = function(){
  $("#search-bar .icon-search").click(function(e){
    e.stopPropagation();
    e.preventDefault();
    $("#search-bar").removeClass("hide")
    $("body").removeClass("open-aside-menu")
  })
  $("#search-bar .icon-close").click(function(e){
    e.stopPropagation();
    e.preventDefault();
    $("#search-bar").addClass("hide")
    $("#search-bar input").val("")
    $(".ac").css("display","none")
  });

  $("#search-bar span").click(function(e){
    $("#search-bar input").val("")
  })

  $("#search-bar input").AutoComplete({
      'data': ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve'],
      'width':280
  });
  // you can find the docment here http://autocomplete.jiani.info/doc/ 
 // $("#search-bar input").AutoComplete({
 //       'data': "../test/data.json",
 //       'ajaxDataType': 'json',
 //       'onerror': function(msg){alert(msg);}
 //   });
 // } 
  $(".header .search").click(function(e){
    e.stopPropagation();
    e.preventDefault();
    $(".search-popup").toggleClass("active")
    $("body").removeClass("open-aside-menu")
    
  })
  $(".header .nav .select-country").click(function(e){
    e.stopPropagation();
    e.preventDefault();
    $(".country-popup").toggleClass("active")
    $("body").removeClass("open-aside-menu")
  })
  $(".country-popup .icon-close").click(function(){
    $(".country-popup").toggleClass("active")
  })
  $(".country-confirm .icon-close").click(function(){
    $(".country-confirm").removeClass("active")
  })

  $(".search-popup .icon-close").click(function(){
    $(".search-popup").removeClass("active")
  })

}

var result_filter = function(){
  $(".result-filter .level1-item .icon").each(function(i,item){
    $(item).click(function(){
      $(item).parent().parent().toggleClass("on")
      if($(item).hasClass("icon-arrow-down-grey")){
        $(item).removeClass("icon-arrow-down-grey").addClass("icon-arrow-up-grey")
      }else{
        $(item).removeClass("icon-arrow-down-grey").addClass("icon-arrow-down-grey")
      }
    })
  })

  $(".result-filter .level2-item .icon").each(function(i,item){
    $(item).click(function(){
      $(item).parent().parent().toggleClass("on")
      if($(item).hasClass("icon-plus-grey")){
        $(item).removeClass("icon-plus-grey").addClass("icon-minus-grey")
      }else{
        $(item).removeClass("icon-minus-grey").addClass("icon-plus-grey")
      }
    })
  })
}

