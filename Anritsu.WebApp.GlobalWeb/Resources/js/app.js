$(document).ready(function () {
    initSearch();
    initMenu();
    initPage();
    initPage2();
    setMenu();
    showIRTab();
    resizeTabs();
    anirtsuSearchEvents.searchClickEvents();
    leftNav.quickLinksEnabled();
    leftNav.dynamicHeaderHeight();


//    initPage2();
    if (/iPhone|iPad/i.test(navigator.userAgent)) {
		// No need use this properties
		//  $(".pusher").css("position", "absolute");
		// $(".aside-menu-trigger").css("position", "absolute");
    }
/*
    $(document).pjax("a.pjax", ".mainForm");
    if ($.support.pjax) {
        $(document).on("click", "a[data-pjax]", function (event) {
            var container = $(".mainForm");
            $.pjax.click(event, { container: container, fragment: ".mainForm" });
            var pagetitle = $(event.target).attr("data-title");
            $("title").html(pagetitle);
        });
    }
    $(document).on("pjax:timeout", function (event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    });
    $(document).on("pjax:send", function () {
        $(".loading").each(function () {
            $(this).show();
        });
    });
    $(document).on("pjax:complete", function () {
        $(".loading").each(function () {
            $(this).hide();
        });
		if(!window.pjaxTriggered){			
			$(".mainForm select").select();
			initPage();
			initPage2();
			initSearch();
			setMenu();
			setTimeout(function () {
			    heightSet();
			}, 2000);
			
		}
		window.pjaxTriggered = false;

        if (navigator.userAgent.toLowerCase().match(/mobi/) || navigator.userAgent.match(/Android/i)) {
            $("body").removeClass("open-aside-menu");
        }
        //This function used to show the IR tab on Investor Relation Page(Refer fixes.js file)
        showIRTab();
        resizeTabs();
        anirtsuSearchEvents.searchClickEvents();
		leftNav.quickLinksEnabled();
		leftNav.dynamicHeaderHeight();
    });

    //call before the PJAX call is executed
	 $(document).on("pjax:beforeSend", function () {
        //unassign the slider before we cache the slider html using PJAX as this will create a problem in reinitializing the sliders
         if($('.home-slider').length > 0){
            $('.home-slider').slick('unslick');
         }

         if($('.category-slider').length > 0){
            $('.category-slider').slick('unslick');
         }
      
    });

    //call this, to bind the events on all elements after the page has rendered with new html content from Pjax
    $(document).on('pjax:end ', function(event) {
        //check for second args to be null, according to pjax doc 2nd args is null if its a browser back/forward
        if(arguments.length && arguments[1] == null){
            $(".mainForm select").select();
            initPage();
            initPage2();
            setMenu();
            initSearch();
        }
		leftNav.quickLinksEnabled();
		leftNav.dynamicHeaderHeight();
    });

    $(document).on('pjax:beforeReplace ', function(event) {
		$('.loading').hide();
		});*/
   
});

function getPath() {
    return document.location.pathname.replace(/\/$/, "");
}
function setMenu() {
    var menus = $(".aside-menu .item");
    var links = $(".aside-menu .link");
    var path = getPath();
    var pathStatu = "";
    menus.each(function (i, item) {
        if ($(item).attr("data-tag").toLowerCase() === path.toLowerCase()) {
            pathStatu = "landing";
            if (!$(item).hasClass("active")) {
                setMenuElementActive(item);
            }
            return false;
        }
    });
    if (pathStatu !== "landing") {
        links.each(function(i, item) {
            if ($(item).attr("href").toLowerCase() === path.toLowerCase()) {
                pathStatu = "link";
                if (!$(item).parent().parent().hasClass("active")) {
                    setLinkeElementActive(item);
                }
                return false;
            }
        });
    }
    if (pathStatu !== "landing" && pathStatu !== "link") {
        menus.each(function(i, item) {
            var $item = $(item);
            $item.attr("data-tag", $item.attr("data-tag").toLowerCase());
        });
        var isMenuActivated = false, isLinkActivated = false, isActivated = false;
        //loop to check path splitting by "/"
        do {
            path = path.substr(0, path.lastIndexOf("/"));
            if (path == '') {
                menus.eq(0).addClass("active").css("left", "280px").transition({
                    left: "0px",
                    duration: 300,
                    easing: "in"
                });
                break;
            }
            //look for menus
            var $menuItem = menus.filter('[data-tag="' + path.toLowerCase() + '"]').eq(0);
            isMenuActivated = ($menuItem.length === 0) ? false : true;
            if (isMenuActivated) {
                if (!$menuItem.hasClass("active")) {
                    setMenuElementActive($menuItem);
                    pathStatu = "landing";
                    isActivated = true;
                } else {
                    pathStatu = "landing";
                    break;
                }

            } else {
                //look for Links
                var $linkItem = links.filter('[href="' + path.toLowerCase() + '"]').eq(0);
                isLinkActivated = ($linkItem.length === 0) ? false : true;
                if (isLinkActivated && !$linkItem.parent().parent().hasClass("active")) {
                    setLinkeElementActive($linkItem);
                    pathStatu = "Link";
                    isActivated = true;
                }
            }
            if (path.lastIndexOf('/') === -1) {
                isActivated = true;
                pathStatu = "";
            }
        } while (!isActivated);

    }
    if (pathStatu === "") {
        $("#loading").hide();
        $("body").removeClass("open-aside-menu");
        if ($(".aside-menu .active").length <= 0) {
            menus.eq(0).addClass("active").css("left", "280px").transition({
                left: "0px",
                duration: 300,
                easing: "in"
            });
        }
    }

}
var setMenuElementActive = function(ele) {
        $(".aside-menu .active").css("left", "-280px").removeClass("active");
        $(ele).addClass("active").css("left", "280px").transition({
            left: "0px",
            duration: 300,
            easing: "in"
        });
}
var setLinkeElementActive = function(ele) {
    $(".aside-menu .active").css("left", "-280px").removeClass("active");
    $(ele).parent().parent().addClass("active").css("left", "280px").transition({
        left: "0px",
        duration: 300,
        easing: "in"
    });
}
var initMenu = function () {
    $(".aside-menu-trigger .icon").click(function (e) {
		e.stopPropagation();
        $("body").toggleClass("open-aside-menu");
        resetslider();
        searchbarHide();
		leftNav.leftNavElements.quickLinksItems.removeClass("active ipad-active");
		leftNav.leftNavElements.quickLinksWrapper.removeClass("active");
		leftNav.leftNavElements.quickLinksTitle.removeClass("active");
		leftNav.leftNavElements.body.removeClass("overlay-active");
    });
};

var searchbarHide = function () {
    if ($(".search-popup").hasClass("active")) {
        $("body").removeClass("search-popup-active");
        $(".search-popup").removeClass("active");
        $("#search-bar").addClass("hide");
        $("#search-bar input").val("");
        $(".ac").css("display", "none");
    }
}
var resetslider = function () {
    if (typeof homeSlider !== "undefined") {
        setTimeout(function () {
            homeSlider.slick("setPosition");
        }, 500);
    }
    if (typeof categorySlider !== "undefined") {
        setTimeout(function () {
            categorySlider.slick("setPosition");
        }, 500);
    }
    //if (typeof newProductsSlider !== "undefined") {
    //    setTimeout(function () {
    //        newProductsSlider.slick("setPosition");
    //    }, 500);
    //}
};
var initSearch = function() {
    $("#search-bar .icon-search").click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $("#search-bar").removeClass("hide");
    });
    $("#search-bar .icon-search-new").click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("#search-bar").removeClass("hide");
    });
	/* Coveo search box focus */
	$(".icon-search-new").click(function (e) {
		$(".CoveoSearchbox .magic-box .magic-box-input > input").focus();
	});
	
    $("#search-bar .icon-close").click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $("#search-bar").addClass("hide");
        $("#search-bar input").val("");
        $(".ac").css("display", "none");
    });
    $("#search-bar .icon-close-new").click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("#search-bar").addClass("hide");
        $("#search-bar input").val("");
        $(".ac").css("display", "none");
    });
	/* Coveo search box focus */
	$(".icon-search-new").click(function (e) {
		$(".CoveoSearchbox .magic-box .magic-box-input > input").focus();
	});
	
    $("#search-bar span").click(function(e) {
        $("#search-bar input").val("");
    });
    //$("#search-bar input").AutoComplete({
    //    'data': ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve"],
    //    'width': 280
    //});
    //$("#search-bar input").keyup(function () {
    //    var autoSearchTerm = $("#search-bar input").val();
    //    $.ajax({
    //        dataType: 'json',
    //        url: 'http://solrdev.anrgw.com:8080/api/v1/suggest?',
    //        async: false,
    //        data: { queryText: autoSearchTerm, count: 10 },
    //        success: function (data) {
    //            //console.log(data);
    //            suggestionData = data;
    //            // var parsed = options.parse && options.parse(data) || parse(data);
    //            // cache.add(term, parsed);
    //            // success(term, parsed);

    //        }
    //    });

    //$("#search-bar input").AutoComplete({
    //    'data': suggestionData.Terms,
    //    'width': 280
    //});
    // you can find the docment here http://autocomplete.jiani.info/doc/ 
    // $("#search-bar input").AutoComplete({
    //       'data': "../test/data.json",
    //       'ajaxDataType': 'json',
    //       'onerror': function(msg){alert(msg);}
    //   });
    // } 

	//GWS-4706 disabled '#' appending in url
$(document).on("click",".country-confirm .hd .icon, .checkout-wrapper a",function (e) {
	var getHref = $(this).attr("href");
        if(getHref == "#"){
            e.preventDefault();
        }
});
    $(".header .search").click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(".search-popup").toggleClass("active");
        $("body").removeClass("open-aside-menu").toggleClass("search-popup-active");
    });
    $(".header .nav .select-country").click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(".country-popup").toggleClass("active");
        $("body").removeClass("open-aside-menu");
    });
    $(".country-popup .icon-close-new").click(function (e) {
        $(".country-popup").toggleClass("active");
		e.preventDefault();
    });
    $(".country-confirm .icon-close-new").click(function () {
        $(".country-confirm").removeClass("active");
    });
    $(".search-popup .icon-close").click(function () {
        $(".search-popup").removeClass("active");
        $("body").removeClass("search-popup-active");
    });
    $(".search-popup .icon-close-new").click(function () {
        $(".search-popup").removeClass("active");
        $("body").removeClass("search-popup-active");
    });
};
var maxHeight = 0;
function heightSet() {
    maxHeight = 0;
    $(".productItemHeight,.products-item").css('height', 'auto').each(function () {
        if ($(this).height() >= maxHeight) { maxHeight = $(this).height(); }
    });
    $(".productItemHeight,.products-item").height(maxHeight);

}

