function initPage2() {
    /* code for home link closing */
    $(function () {
       var showChar = 250;
        var dots = "...";
        var moretext = "READ MORE";
        var lesstext = "LESS";
        var items = "";

    //    $('p.more').each(function () {
    //        var content = $(this).html();
    //        var trimmedContent = $.trim(content);
    //        trimmedContent = trimmedContent.replace(/(<([^>]+)>)/ig, "");
    //        $(this).html(trimmedContent);
    //        if (trimmedContent.length > showChar) {
    //            var visibleText = trimmedContent.substr(0, showChar);
    //            var hiddenText = trimmedContent.substr(showChar - 0, trimmedContent.length - showChar);
				//var hiddenTextTemp;
    //            for (var i = showChar; i < trimmedContent.length; i++) {
    //                var j = i - showChar;
    //                if (trimmedContent.substr(i, 1) !== " ") {
    //                    visibleText += trimmedContent.substr(i, 1);
    //                    hiddenTextTemp = hiddenText.substr(j + 1, trimmedContent.length - showChar);
    //                } else {
    //                    hiddenText = " " + hiddenTextTemp;
    //                    break;
    //                }
    //            }
    //            var htmlContent = visibleText + '<span>' + dots + ' </span><div class="morecontent"><span class="hiddentxt">' + hiddenText + '</span><a href="#" class="morelink">' + moretext + '<span class="icon icon-arrow-blue">&nbsp;</span></a></div>';
    //            $(this).html(htmlContent);
    //        }
    //    });

    //    $(".morelink").click(function () {
    //        if ($(this).hasClass("less")) {
    //            $(this).removeClass("less");
    //            $(this).html(moretext).append("<span class='icon icon-arrow-blue'>&nbsp;</span>");
    //        } else {
    //            $(this).addClass("less");
    //            $(this).html(lesstext);
    //        }
    //        $(this).parent().prev().toggle();
    //        $(this).prev().toggle();

    //        return false;
    //    });


        //code to close menu when click outside
        $(".rightContent").on('click', function (e) {
            $("body").removeClass("open-aside-menu");
            $(".country-popup, .logout-option").removeClass("active");
			resetslider();
        });

        //code for video gallery check box functionality
        $('.video-filter-left .result-filter ul li :checkbox').on('click', function () {
            var currentChk = $(this);
            var currentLi = currentChk.closest('li');
            var currentUl, parentEle;
            if (currentLi.has('ul')) {
                currentLi.find(':checkbox').not(this).prop('checked', this.checked);
            }
            do {
                currentUl = currentLi.parent();
                parentEle = currentUl.siblings(':checkbox');
                if (currentChk.is(':checked')) {
                    parentEle.prop('checked', currentUl.has(':checkbox:not(:checked)').length == 0);
                } else {
                    parentEle.prop('checked', false);
                }
                currentChk = parentEle;
                currentLi = currentChk.closest('li');
            } while (currentUl.is(':not(.someclass)'));
            if (!$(this).is(':checked')) {
                $(this).closest('ul').parents('li').each(function () {
                    console.log($(this));
                    $(this).find(':checkbox:first').prop('checked', false)
                })
            }
        });

        ////code for global search check box functionality
        // $('.search-filter-left .result-filter ul li :checkbox').on('click', function () {
        //    var $this = $(this),
        //    $closestLi = $this.closest('li');
        //    if ($this.is(':checked')) {
        //        $closestLi.addClass('on').find('.icon').addClass('.icon-arrow-up-grey').removeClass('.icon-arrow-down-grey');
        //        $closestLi.find('>ul>li input:checkbox').prop('checked', true);
        //    } else {
        //        $closestLi.removeClass('on').find('.icon').removeClass('.icon-arrow-up-grey').addClass('.icon-arrow-down-grey');
        //        $closestLi.find('>ul>li input:checkbox').prop('checked', false)
        //    }
        //    $('#buttonFacetSearch').click();
        // });

        //global search clear all changes
        $('#searchClearAll').click(function () {
            $('.loading').show();
            $(':checkbox').each(function () {
                $(this).removeAttr('checked');
            });
            $('#buttonFacetSearch').click();
        });

        //code for closing menu on click back home
        $(".aside-menu .backhome").click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("body").removeClass("open-aside-menu");
        });

        // counntry dropdown changes for storing cookie information
        $('.country-list a.country').on('click', function (e) {
            e.preventDefault();
            document.cookie = "IsCC=true" + ";path=/";
            document.cookie = "UCC=" + $(this).attr("data-country-code") + ";path=/";
            window.location.href = $(this).attr("href");
        });

        var selectedCountry = {};
        selectedCountry.txt = $("#ConfirmCountriesDropDown option:selected").text();
        selectedCountry.val = $("#ConfirmCountriesDropDown").val();
        $('#btnConfirmCountry').on('click', function (e) {
            e.preventDefault();
            //$.cookie("IsCC", "true", { expires: 365 * 10, path: "/" });
            //$.cookie("CCC", selectedCountry.val.split('-')[1], { expires: 365 * 10, path: "/" });
            //document.cookie = "UCC=" + $("#ConfirmCountriesDropDown option:selected").attr("data-country-code") + ";path=/";
            selectedCountry = {};
            selectedCountry.txt = $("#ConfirmCountriesDropDown option:selected").text();
            selectedCountry.val = $("#ConfirmCountriesDropDown").val();
            if (typeof ($("#ConfirmCountriesDropDown option:selected").attr("data-country-code")) != "undefined") {
                $.cookie("IsCC", "true", { expires: 365 * 10, path: "/" });
                $.cookie("CCC", selectedCountry.val.split('-')[1], { expires: 365 * 10, path: "/" });
                document.cookie = "UCC=" + $("#ConfirmCountriesDropDown option:selected").attr("data-country-code") + ";path=/";
            } else {
                $.cookie("IsCC", "false", { expires: 365 * 10, path: "/" });
            }

            var urlPath = window.location.protocol + "//" + window.location.host;
            if (selectedCountry.val.indexOf("/") == 0) {
                urlPath += selectedCountry.val;
            }
            else { urlPath += "/" + selectedCountry.val; }
            window.location.href = urlPath;
        });

        $("#ConfirmCountriesDropDown,#content_0_ddlSelectCountry").off('change').on("change", function (e) {
            e.preventDefault();
            var $sel = $(this).find("option:selected");
            $(this).find("option").removeAttr("selected");
            $sel.prop("selected", "selected");
            $(this).closest('div').find('.btn-select').text($sel.text());
            selectedCountry.txt = $sel.text();
            selectedCountry.val = $sel.val();
            var countrySelected = document.getElementById("content_0_ddlSelectCountry");
            countrySelected.options[countrySelected.options.selectedIndex].setAttribute("selected", "selected");

        });

        $('.region-form').on('click', '.country-item a', function (e) {
            e.preventDefault();
            var urlString = $(this).attr("href");
            if (!getParameterByName('id', urlString)) {
                document.cookie = "UCC=" + $(this).attr("data-country-code") + ";path=/";
                $.cookie("CCC", $(this).attr("data-country-code"), { expires: 365 * 10, path: "/" });
                $.cookie("IsCC", "true", { expires: 365 * 10, path: "/" });
            }
            window.location.href = $(this).attr("href");
        });

        function getParameterByName(name, urlString) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		        results = regex.exec(urlString);
            return results === null ? false : true;
        }

        //video gallery clear all changes
        $('#clearAll').click(function () {
            $(':checkbox').each(function () {
                $(this).removeAttr('checked');
            });
        });

        //code for touch event for close pop up
        $(".closeLinkDiv").click(function () {

            $(".country-confirm").removeClass("active");
        });



        /*T-11 Fixess*/
        $('.item').find('div.img').each(function (i) {
            $(this).next('div.text').addClass("width60");
        });

        /*T-12- Fixes */
        if ($('.content-form').children().hasClass('content-aside')) {
            $('.content-detail').removeClass("width100");
            $('.content-detail').addClass("width70");
        }
        else {
            $(".content-detail").removeClass("width70");
            $(".content-detail").addClass("width100")
        }
		/*GWS-4725 start*/
		if($(window).width() < 768 ){
			$('.warranty-check-status').parent(".content-detail").removeClass("width70").addClass("width100");
		}
		/*GWS-4725 End*/
		
        $(".category-slider .slick-dots").css("display", "none");

        $("body").on("change", '.custom-select-table select', function () {
            $("<div id='hidden-layer'></div>").css({
                "width": $(window).width(),
                "height": $(document).height(),
                position: "absolute",
                top: "0px",
                "background-color": "transparent",
                "z-index": "999999999999999999"
            }).appendTo("html");
        });


        if (typeof Sys != "undefined") {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if ((sender._postBackSettings.panelsToUpdate != null) && $('select').parent().hasClass("custom-select-table")) {
                        $('.custom-select-table select')["select"]();
                        $("#hidden-layer").remove();
                    }
                    $(".tab-interface").each(function (i, item) {
                        var tab = $(item).children("ul");
                        var tabContent = $(item).find(".section");
                        $(tab).find("li a").each(function (index, el) {
                            $(el).click(function (e) {
                                if (!$(item).hasClass('product-downloads')) {
                                    e.stopPropagation();
                                    e.preventDefault();
                                }
                                $(tab).find("li a").removeClass("active");
                                $(el).addClass("active");
                                tabContent.removeClass("active");
                                tabContent.eq(index).addClass("active");
                                if ($(".content-list.active > div").not(".pagination").find(".download-item").length == 0) {
                                    $(".content-list.active > .pagination").hide();
                                    $(".content-list.active > ul").hide();
                                    $(".content-list.active > h3").css("visibility", "hidden");
                                }
                                else {
                                    $(".content-list.active > .pagination").show();
                                    $(".content-list.active > ul").show();
                                    $(".content-list.active > h3").css("visibility", "visible");
                                }
                            });
                        });
                    });
                    var $tabs = $('.tab-interface');
                    if ($tabs.length > 0) {
                        var $tabList = $('.tab-interface  > ul > li a'),
                                $sectionList = $('.tab-interface .section'),
                            activeTabIndex = $tabList.index($('.tab-interface >  ul  >  li a.active'));
                        $sectionList.removeClass('active').eq(activeTabIndex).addClass('active');
                    }

                    // hide submenu (works well)
                    $('.sub-slider').hide();
                    $('.accordion-list').addClass("fa fa-plus");

                    // accordion multilevel menu (works well)
                    $('.accordion-list').on('click', function (event) {
                        var jsPro = $(this);
                        if (jsPro.hasClass('fa-minus')) {
                            jsPro.removeClass('fa-minus').addClass('fa-plus').closest('li').find('.sub-slider').find('.accordion-list').addClass('fa-plus').removeClass('fa-minus').end().slideUp();
                        }
                        else {
                            jsPro.closest('ul').find('li').find('ul.sub-slider').slideUp().end().find(' .accordion-list').removeClass('fa-minus').addClass('fa-plus');
                            jsPro.addClass('fa-minus').removeClass('fa-plus').closest('li').find('> .sub-slider').slideDown();
                        }
                        event.preventDefault();
                    });
                    $('#downloadLibrary li a').on('click', function () {
                        $(this).addClass('currentActiveEle').siblings().removeClass('currentActiveEle');
                    });

                    //video gallery clear all changes
                    $('#clearAll').click(function () {
                        $(':checkbox').each(function () {
                            $(this).removeAttr('checked');
                        });
                    });

                    //video gallary update panel scripts
                    //$(".result-filter .level2 .icon,.result-filter .level3 .icon").each(function (i, item) {
                    //    $(item).click(function () {
                    //        $(item).closest('li').toggleClass("on");
                    //        if ($(item).hasClass("icon-plus-grey")) {
                    //            $(item).removeClass("icon-plus-grey").addClass("icon-minus-grey");
                    //        } else {
                    //            $(item).removeClass("icon-minus-grey").addClass("icon-plus-grey");
                    //        }
                    //    });
                    //});

                    ////code for global search check box functionality
                    //$('.search-filter-left .result-filter ul li :checkbox').on('click', function () {
                    //    var $this = $(this),
                    //    $closestLi = $this.closest('li');
                    //    if ($this.is(':checked')) {
                    //        $closestLi.addClass('on').find('.icon').addClass('.icon-arrow-up-grey').removeClass('.icon-arrow-down-grey');
                    //        $closestLi.find('>ul>li input:checkbox').prop('checked', true);
                    //    } else {
                    //        $closestLi.removeClass('on').find('.icon').removeClass('.icon-arrow-up-grey').addClass('.icon-arrow-down-grey');
                    //        $closestLi.find('>ul>li input:checkbox').prop('checked', false)
                    //    }
                    //    $('#buttonFacetSearch').click();
                    //});

                    //global search clear all changes
                    $('#searchClearAll').click(function () {
                        $('.loading').show();
                        $(':checkbox').each(function () {
                            $(this).removeAttr('checked');
                        });
                        $('#buttonFacetSearch').click();
                    });

                    if (activeLinkId != undefined) {
                        $('#' + activeLinkId).parents('ul').each(function () {
                            var $this = $(this);
                            $this.show();
                            $this.closest('li').find('.fa-plus:first').removeClass('fa-plus').addClass('fa-minus');
                        });
                        $('#' + activeLinkId).closest('li').find('> .sub-slider').show();
                        $('#' + activeLinkId).closest('li').find('.fa-plus:first').removeClass('fa-plus').addClass('fa-minus');
                    }

                    if (selectChkBoxId != undefined) {
                        $('#' + selectChkBoxId).parents('li').each(function () {
                            var $this = $(this);
                            $this.addClass('on');
                            $this.find('> .iconStyles').addClass('icon-minus-grey').removeClass('icon-plus-grey');
                        });
                        if (!$('#' + selectChkBoxId).is(':checked')) {
                            $('#' + selectChkBoxId).closest('ul').parents('li').each(function () {
                                $(this).find(':checkbox:first').prop('checked', false)
                            })
                        }
                    }
                    $('.loading').hide();
                    $(".mainForm select").select();

                    //global-search changes
                    $('.searchLevel .level1').each(function (i, elem) {
                        var $elem = $(elem);
                        if ($elem.find('li').length == 0) {
                            $elem.find('.icon').hide();
                        }
                    })
                    //$(".result-filter .level1-item .icon").each(function (i, item) {
                    //    $(item).click(function () {
                    //        $(item).parent().parent().toggleClass("on");
                    //        if ($(item).hasClass("icon-arrow-down-grey")) {
                    //            $(item).removeClass("icon-arrow-down-grey").addClass("icon-arrow-up-grey");
                    //        } else {
                    //            $(item).removeClass("icon-arrow-down-grey").addClass("icon-arrow-down-grey");
                    //        }
                    //    });
                    //});
                    if (activeLinkId != undefined) {
                        $('#' + activeLinkId).parents('ul').each(function () {
                            var $this = $(this);
                            $this.show();
                            $this.closest('li').find('.fa-plus:first').removeClass('fa-plus').addClass('fa-minus');
                        });
                        $('#' + activeLinkId).closest('li').find('> .sub-slider').show();
                        $('#' + activeLinkId).closest('li').find('.fa-plus:first').removeClass('fa-plus').addClass('fa-minus');
                    }

                    /***Search Results New Implementation**/
                    if ($('.search-result-right .item').length > 0) {
                        $(".search-result-body .container").removeClass("dispNone");
                        $(".search-empty").addClass("dispNone");
                    } else {
                        $(".search-result-body .container").addClass("dispNone");
                        $(".search-empty").removeClass("dispNone");
                    }
                    /* global search changes */
                });
            }
        }

        var activeLinkId = "", selectChkBoxId = "";
        $(document).on('click', '#accordion a', function () {
            activeLinkId = $(this).attr('id');
            $('.loading').show();
        });

        $(document).on('click', '#downloadLibrary > ul a, #downloadSoftware > ul a', function () {
            $('.loading').show();
        });

        $(document).on('click', '#checkbox-accordion input:checkbox', function () {
            selectChkBoxId = $(this).attr('id');
            $('.loading').show();
        })
        $(document).on('click', '.search-filter-left .result-filter ul.globalfacet li :checkbox', function () {
            $('.loading').show();
        });

        $(document).on('click', '.pagination a', function () {
            $('.loading').show();
        });
        //var limitChar = 350;
        //var $productsFamilyContent = $('.products-family-content');
        //if ($productsFamilyContent.length > 0) {
        //    var productContent = $productsFamilyContent.html();
        //    if (productContent.length > limitChar) {
        //        $('.products-family-content').addClass('columnCls');
        //    }
        //}
        //sign-out 


        $(".logout-icon").on('click', function (e) {
            e.stopPropagation();
            $(".logout-option").toggleClass("active");
        });
        //sign-out end

        $('.searchLevel .level1').each(function (i, elem) {
            var $elem = $(elem);
            if ($elem.find('li').length == 0) {
                $elem.find('.icon').hide()
            }
        })
    });

    $(document).ready(function () {
        // hide submenu (works well)
        $('.sub-slider').hide();
        $('.accordion-list').addClass("fa fa-plus");

        // accordion multilevel menu (works well)
        $('.accordion-list').on('click', function (event) {
            var jsPro = $(this);
            if (jsPro.hasClass('fa-minus')) {
                jsPro.removeClass('fa-minus').addClass('fa-plus').closest('li').find('.sub-slider').find('.accordion-list').addClass('fa-plus').removeClass('fa-minus').end().slideUp();
            }
            else {
                jsPro.closest('ul').find('li').find('ul.sub-slider').slideUp().end().find(' .accordion-list').removeClass('fa-minus').addClass('fa-plus');
                jsPro.addClass('fa-minus').removeClass('fa-plus').closest('li').find('> .sub-slider').slideDown();
            }
            event.preventDefault();
        });
        $('#downloadLibrary li a').on('click', function () {
            $(this).addClass('currentActiveEle').siblings().removeClass('currentActiveEle');
        });

        if ($('.txtUserInteractionIdentifier').length > 0) {
            $('.txtUserInteractionIdentifier').val("green");
            $(document).on('focus', ':input', function () {
                $('.txtUserInteractionIdentifier').val("blue");
            });
            $(document).on("click", '.scfSubmitButton', function (e) {
                if ($('.txtUserInteractionIdentifier').val() === "green") {
                    e.preventDefault();
                }
            });
        }
        var minHeight = 0;
        $(".category-slider .slick-slide").each(function () {
            if (minHeight < $(this).height()) {
                minHeight = $(this).height();
            }
        });
        $(".category-slider .slick-slide").each(function () {
            $(this).css("min-height", minHeight);
        });
    });


    $(window).load(function () {

        var arr = [];
        var ModelNumberResults = [];
        if ($("body").find(".download-center-container").length > 0) {
            ModelNumberResults = searchModelNumber();
        }
        var maxHeight = 0;
        heightSet();
        $(window).on("orientationchange", function () {
            heightSet();
        });
        function heightSet() {
            maxHeight = 0;
            $(".productItemHeight,.products-item").css('height', 'auto').each(function () {
                if ($(this).height() >= maxHeight) { maxHeight = $(this).height(); }
            });
            $(".productItemHeight,.products-item").height(maxHeight);

        }
        $(".search-model input").AutoComplete({
            "data": ModelNumberResults
        });
        $(".search-model input").keyup(function () {
        });
        function searchModelNumber() {

            var url = "/api/Downloads/GetModelsByKey";

            $.ajax({
                type: "POST",
                jsonp: true,
                crossDomain: true,
                url: url,
                async: false,
                success: function (data) {
                    items = data;
                }
            });

            if (items.length > 0) {
                if (items[0].ResponseStatusCode == 200) {
                    for (var i = 0; i < items.length; i++) {
                        arr.push(items[i].ModelNumber);
                    }
                }
                else {
                    if (items[0].ResponseStatusMessage != undefined) {
                        arr.push(items[0].ResponseStatusMessage);
                    }
                    else {

                    }
                }
            }
            return arr;
        }

        
        /*Start: Activating tab on pageload*/
        var tabIndex = parseInt(GetQueryStringParams("tab")),
            $tabContainer = ($('.tab-interface-2').length) ? $('.tab-interface-2').find('>ul>li') : $('.tab-interface:first').find('>ul>li');
        if (tabIndex >= 1 && tabIndex <= $tabContainer.length) {
            $tabContainer.eq(tabIndex - 1).find('a').trigger("click");
        } else {
                if ($('.download-center-container').length > 0) {
                    $tabContainer.eq(1).find('a').trigger("click");
                } else {
                $tabContainer.eq(0).find('a').trigger("click");
            }
        }
        /*End: Activating tab on pageload*/

        if ($(".content-list.active > div").not(".pagination").find(".download-item").length == 0) {
            $(".content-list.active > .pagination").hide();
            $(".content-list.active > ul").hide();
            $(".content-list.active > h3").css("visibility", "hidden");
        }
        else {
            $(".content-list.active > .pagination").show();
            $(".content-list.active > ul").show();
            $(".content-list.active > h3").css("visibility", "visible");
        }
    });
}
$('.download-item').eq(0).css('padding-top', '0');

var anirtsuSearchEvents = {
    searchClickEvents: function () {
        $("#searchbox").on("keypress", function (e) {            
            if (e.which == 13) {
                e.preventDefault();
                RouteToSearch();
            }
        });

       $("#searchbarbutton").on("click", function(e) {
			if( !$("#searchbox").val()){
				$("#searchbox").focus();
			}
			else{
				RouteToSearch();
			}
        });


        $(".search").click(function () {
            $("#searchbox").focus();
        });
		
		
		if (typeof jQuery.ui !== 'undefined'){
			var appLangTxt = '';
				if (typeof (appLang) == !'undefined') {
					appLangTxt = appLang;
					$("#searchbox").autocomplete({
						minLength: 1,
						source: "https://" + $(location).attr('hostname') + "/search/QuerySuggestions.ashx?culturegroupcode=" + appLangTxt,
						select: function (event, ui) {
							setTimeout(function(){
								RouteToSearch();
							}, 200);
						}
					})
				}

			$(".search-popup-container #searchbox").autocomplete({
				appendTo: ".search-popup-container",
				open: function () {
					var inputWidth = $(".search-popup-container #searchbox").outerWidth();
					var leftPos = $(".search-popup-container .ui-widget").position().left + 13;
					var boxWidth = inputWidth - 24;
					$(".search-popup-container .ui-widget").width(boxWidth);
					$(".search-popup-container .ui-widget").css("left", leftPos);
				}
			});
        }
    }
}


/* Search Redirection */
function RouteToSearch() {
    var searchURL = $("#hdnSearchPath").val();
    var isCorporate; var groupHeader;
    if ($.trim($("#searchbox").val()).length != 0) {
        if ($("#header_0_hdnIsCorporate").val()) { isCorporate = $("#header_0_hdnIsCorporate").val(); }
        else if ($("#content_0_hdnIsCorporate").val()) { isCorporate = $("#content_0_hdnIsCorporate").val(); }

        if ($("#header_0_hdnGroupHeader").val()) { groupHeader = $("#header_0_hdnGroupHeader").val(); }
        else if ($("#content_0_hdnGroupHeader").val()) { groupHeader = $("#content_0_hdnGroupHeader").val(); }

        if (isCorporate && groupHeader && isCorporate === "false") {
            window.location.href = searchURL + "?q=" + encodeURIComponent($("#searchbox").val())
                + "&requiredfields=groupheader:" + groupHeader;            
        }
        else {
            window.location.href = searchURL + "?q=" + encodeURIComponent($("#searchbox").val());
        }
    }
}
/* End */


/* Activate Tabs */
function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            if (sParameterName[1] != null) {
                return sParameterName[1].toLowerCase()
            }
        }
    }
}
/* End */


function resizeTabs() {
  var width = Math.floor(100 / $(".hero-tab .hero-tab-ul li").size());
  if (width == 50)
  {
    $(".infivis.hero-tab .hero-tab-ul li").first().css('width', "50%");
    $(".infivis.hero-tab .hero-tab-ul li").last().css('width', "49.5%");

  }
  else
  {
    $(".infivis.hero-tab .hero-tab-ul li").css('width', width + "%");
  }

  //To handle Products tab panel resize
  var numTabs = $(".infivis.product-detail-tab .product-detail-tab-ul li").size();
  var marginRight = $(".infivis.product-detail-tab .product-detail-tab-ul li").first().css("margin-right") == "0px"
      ? 0 : 0.5;
  if (numTabs > 0) {
    
    width = ((100 - ((numTabs - 1) * marginRight)) / numTabs).toFixed(2);

    width = (100 / numTabs).toFixed(2);
    width = width - ((marginRight * (numTabs - 1)).toFixed(2) / numTabs);
    $(".infivis.product-detail-tab .product-detail-tab-ul li").css('width', width + "%");
    //make sure the first tab is selected for each tab.
    initInfivisDownloadTabs();
  }
  $(".infivis.tab-interface,.infivis.tab-interface-2").find("li:first a").click();
}

function initInfivisDownloadTabs(){
  $(".infivis .tab-interface.inner-tab.product-downloads > ul > li > a").on("click", function (m) {
    m.preventDefault();
    var n = $(this).attr("href");
    $(".infivis .tab-interface.inner-tab.product-downloads > ul > li > table").hide();
    var f = $(n)
      , l = $(".infivis .tab-interface.inner-tab.product-downloads > ul");
    f.show();
    l.css("height", "auto");
    if (l.height() < f.height()) {
      l.css("height", f.height() + "px")
    }
  });
  $(".infivis .tab-interface.inner-tab.product-downloads  ul  li  .detail-table a").on("click", function () {
    $(this).closest("table.detail-table").prev().addClass("active")
  });
  var d = $(".infivis .product-downloads.products-table ul")
    , b = $(".infivis .inner-tab.product-downloads.products-table .select-mobile")
        , a = d.find("li").eq(0).find("table").clone();
    //AI-147
  //b.append(a);
  b.find("table").wrap("<div class='responsive-table'></div>");
  var c = b.find(".responsive-table");
  $("#product-download-category").off("change").on("change", function () {
    var f = $(this)
      , m = f.find("option:selected")
      , l = d.find("li").eq(m.index()).find("table").clone();
    f.closest(".custom-select").find(".btn-select").text(m.text());
    c.empty();
    c.append(l);
    $(".responsive-table .detail-table").css("display", "block")
  });
  if ($(".ie8").length > 0) {
    $(".infivis .tab-interface.inner-tab.product-downloads > ul").css("position", "relative");
    $(".infivis .tab-interface.inner-tab.product-downloads > ul > li > table").css({
      left: "110%",
      width: "800px",
      display: "none",
      "min-width": "400%",
      "padding-top": "20px",
      position: "absolute",
      top: "0"
    })
  }
  $(".infivis .tab-interface.inner-tab.product-downloads > ul > li > a").eq(0).trigger("click");
}


$(window).load(function () {
    $('.sitemapCSS > div').css('display', 'block');
    //AI-147
    //resizeTabs();
    //$(".tab-interface").find("li:first a").click();
});
/***Search Results New Implementation**/
$(function() {
    if ($('.search-result-right .item').length > 0) {
        $(".search-result-body .container").removeClass("dispNone");
        $(".search-empty").addClass("dispNone");
    } else {
        $(".search-result-body .container").addClass("dispNone");
        $(".search-empty").removeClass("dispNone");
    }

    $(document).on("click", ".result-filter .level1-item .icon", function() {
        var $this = $(this);
        $this.parent().parent().toggleClass("on");
        if ($this.hasClass("icon-arrow-down-grey")) {
            $this.removeClass("icon-arrow-down-grey").addClass("icon-arrow-up-grey");
        } else {
            $this.removeClass("icon-arrow-down-grey").addClass("icon-arrow-down-grey");
        }
    });

    $(document).on("click", " .result-filter .level2 .icon,.result-filter .level3 .icon", function() {
        var $this = $(this);
        $this.closest('li').toggleClass("on");
        if ($this.hasClass("icon-plus-grey")) {
            $this.removeClass("icon-plus-grey").addClass("icon-minus-grey");
        } else if ($this.hasClass("icon-minus-grey")) {
            $this.removeClass("icon-minus-grey").addClass("icon-plus-grey");
        }
    });

    //code for global search check box functionality
    $(document).on('click', '.result-filter ul li :checkbox', function() {
        var $this = $(this),
            $closestLi = $this.closest('li'),
            $icon = $closestLi.find('.icon:first');
        if ($this.is(':checked')) {
            $closestLi.addClass('on');
            if ($icon.hasClass('icon-arrow-down-grey')) {
                $icon.addClass('icon-arrow-up-grey').removeClass('icon-arrow-down-grey');
            }
            if ($icon.hasClass('icon-plus-grey')) {
                $icon.addClass('icon-minus-grey').removeClass('icon-plus-grey');
            }
            $closestLi.find('>ul>li input:checkbox').prop('checked', true);
        } else {
            $closestLi.removeClass('on');
            if ($icon.hasClass('icon-arrow-up-grey')) {
                $icon.addClass('icon-arrow-down-grey').removeClass('icon-arrow-up-grey');
            }
            if ($icon.hasClass('icon-minus-grey')) {
                $icon.addClass('icon-plus-grey').removeClass('icon-minus-grey');
            }
            $closestLi.find('>ul>li input:checkbox').prop('checked', false)
        }

        var isParentChecked = ($this.closest('ul').find('>li input:checkbox').length == $this.closest('ul').find('>li input:checkbox:checked').length);
        var $videoGallaryBtn = $('#SubmitVideoGalleryFacets');
        $this.closest('ul').closest('li').find('input:checkbox:first').prop('checked', isParentChecked);
        if ($closestLi.hasClass('level3')) {
            var level1ChkBoxchecked = ($this.closest('.level2_ul').find('>li input:checkbox').length == $this.closest('.level2_ul').find('>li input:checkbox:checked').length)
            $this.closest('li.level1').find('input:checkbox:first').prop('checked', level1ChkBoxchecked);
        }
        if ($closestLi.hasClass('level4')) {
            var level2ChkBoxchecked = ($this.closest('.level3_ul').find('>li input:checkbox').length == $this.closest('.level3_ul').find('>li input:checkbox:checked').length)
            $this.closest('li.level2').find('input:checkbox:first').prop('checked', level2ChkBoxchecked);
        }
        $('#buttonFacetSearch').click();

        if ($videoGallaryBtn.length > 0) {
            $videoGallaryBtn.click();
        }
    });

    /*
    $(".globalfacet>li").each(function (a, b) {
        $(b).each(function (c, d) {
            if ($(d).find('input').is(':checked')) {
                $(b).addClass('on');
                $(b).find('.icon').removeClass("icon-arrow-down-grey").addClass("icon-arrow-up-grey");
                $(d).find('ul li').each(function (e, f) {
                    if ($(f).find('input').is(':checked')) {
                        $(f).parent().parent().addClass('on').children('span.icon').removeClass('icon-plus-grey').addClass('icon-minus-grey');
                    }
                });
            }
        });
    });*/

});
$(document).ready(function () {
    // Sign out position based on my anritsu position
    function signOutPos() {
        if ($('#header_0_signOutIconDesktop').length != 0) {
            var right = $(document).width() - ($('#header_0_signOutIconDesktop').offset().left + $('#header_0_signOutIconDesktop').outerWidth());
            var rightpos = right + 4 + 'px';
            $('#header_0_linkSignOut').css('right', rightpos);
        }
    }
    signOutPos();

    $(window).resize(function () {
        signOutPos();
    });
    

    anirtsuSearchEvents.searchClickEvents();
    $('.tab-interface-2.product-detail-tab > ul > li > a').on('click', function (n) {
        var firstTable = $('.tab-interface.inner-tab.product-downloads  ul li:first-child .section').height();
        var ulHeight = $('.tab-interface.inner-tab.product-downloads > ul');
        ulHeight.css('height', 'auto');
        if (ulHeight.height() < firstTable) {
            ulHeight.css('height', firstTable + 'px')
        }
    })
    $(".tab-interface.inner-tab.product-downloads > ul > li > a").on("click", function (e) {
        e.preventDefault();
        var selectedDiv = $(this).attr("href");
        $(".tab-interface.inner-tab.product-downloads > ul > li > table").hide();
        var $selectedTable = $(selectedDiv), $ul = $(".tab-interface.inner-tab.product-downloads > ul");
        $selectedTable.show();
        $ul.css("height", "auto");
        if ($ul.height() < $selectedTable.height()) {
            $ul.css("height", $selectedTable.height() + 'px');
        } 
    });
    $('.tab-interface.inner-tab.product-downloads  ul  li  .detail-table a').on('click', function () {
        $(this).closest('table.detail-table').prev().addClass("active")
    });

    // product downloads dropdown change in mobile
    var $ul = $('.product-downloads.products-table ul'),
        $mobileContainer = $(".inner-tab.product-downloads.products-table .select-mobile"),
        $clonedTable = $ul.find('li').eq(0).find('table').clone();
    $mobileContainer.append($clonedTable);
    $mobileContainer.find('table').wrap("<div class='responsive-table'></div>");
    var $respTable = $mobileContainer.find('.responsive-table');
    $("#product-download-category").off('change').on('change', function () {

        var $this = $(this),
            currOption = $this.find('option:selected'),
            currentTable = $ul.find('li').eq(currOption.index()).find('table').clone();
        $this.closest('.custom-select').find('.btn-select').text(currOption.text());

        $respTable.empty();
        $respTable.append(currentTable);
        $(".responsive-table .detail-table").css("display","block");
    });
    if ($('.ie8').length > 0) {
        $(".tab-interface.inner-tab.product-downloads > ul").css("position", "relative");
    $(".tab-interface.inner-tab.product-downloads > ul > li > table").css({
    "left":"110%",
    "width": "800px",
    "display": "none",
    "min-width": "400%",
    "padding-top": "20px",
    "position": "absolute",
    "top": "0"
    });
}
    $(".tab-interface.inner-tab.product-downloads > ul > li > a").eq(0).trigger("click");
    showIRTab();
});
$(".rightContent").on("tap", function () {
    $("body").removeClass("open-aside-menu");
});
//This function used to show the IR tab on Investor Relation Page(Reference app.js file)
function showIRTab() {

    var $container = $('.tab-interface.hero-tab');

    $container.find(' > ul >li a').each(function (i, elem) {
        var $elem = $(elem),
            divId = $elem.attr('href');
        if ($container.find(divId).length == 0) {
            $(elem).closest('li').remove();
            if (!$container.find(' > ul >li:eq(0) a').hasClass("active")) {
                $container.find(' > ul >li:eq(0) a').addClass("active");
            }

            $(".tab-interface").each(function (i, item) {
                var tab = $(item).children("ul");
                var tabContent = $(item).children(".section");

                $(tab).find("li a").click();
                $(tab).find("li a").each(function (index, el) {

                    $(el).click(function (e) {
                        //e.stopPropagation()
                        e.preventDefault()

                        $(tab).find("li a").removeClass("active")
                        $(el).addClass("active")
                        tabContent.removeClass("active")
                        tabContent.eq(index).addClass("active")
                    })
                })
            })
        }

    })
}

initPage2();