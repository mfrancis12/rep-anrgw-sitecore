var allCookies = {
  getItem: function (sKey) {
    if (!sKey || !this.hasItem(sKey)) { return null; }
    return unescape(document.cookie.replace(new RegExp("(?:^|.*;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"), "$1"));
  },
  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Tue, 19 Jan 2038 03:14:07 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toGMTString();
          break;
      }
    }
    document.cookie = escape(sKey) + "=" + escape(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
  },
  removeItem: function (sKey, sPath) {
    if (!sKey || !this.hasItem(sKey)) { return; }
    document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sPath ? "; path=" + sPath : "");
  },
  hasItem: function (sKey) {
    return (new RegExp("(?:^|;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  },
  keys: /* optional method: you can safely remove it! */ function () {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nIdx = 0; nIdx < aKeys.length; nIdx++) { aKeys[nIdx] = unescape(aKeys[nIdx]); }
    return aKeys;
  }
};

function isAllowedToDrink (birth_month, birth_day, birth_year, majority_age) {


  var majority_age_in_days = majority_age * 365 + Math.floor(majority_age/4)

  var birth = new Date(birth_month+"/"+birth_day+"/"+birth_year)
  var now = new Date()
  var time_diff = now.getTime() - birth.getTime()
  var age_in_days = time_diff / (1000 * 3600 * 24)

  if (majority_age_in_days <= age_in_days) {
    allCookies.setItem(window.cookie_key, 1,'','/')
    if ($('input[name=remember_date]:checked').val()=="yes") {
      allCookies.setItem(window.cookie_key, 1, Infinity,'/')
    }
    return true
  }
  else return false
}

function validateInput(birth_month, birth_day, birth_year) {

  var is_valid = {}
  var is_number = /\d+/
  var this_year = new Date().getFullYear()
  /* Let's validate */
  is_valid.birth_month = is_number.test(birth_month) && birth_month > 0 && birth_month < 13
  is_valid.birth_day = is_number.test(birth_day) && birth_day > 0 && birth_day < 32
  is_valid.birth_year = is_number.test(birth_year) && birth_year > 1890 && birth_year < this_year+1

  return is_valid

}

function sanitizeInput(birth_month, birth_day, birth_year) {

  return {
          'birth_day': parseInt(birth_day.trim(),10),
          'birth_month': parseInt(birth_month.trim(),10),
          'birth_year': parseInt(birth_year.trim(),10)
         }
}

function showWebsite() {
  $.magnificPopup.close();
}
function setError(str){
  $(".gateway-error").html(str);
}

function enterWebsite(ev) {

  if ( allCookies && allCookies.getItem(window.cookie_key) && allCookies.getItem(window.cookie_key) == 1 ) {
    showWebsite()
    return false

    } else if (ev && ev.type == 'click') {

    window.majority = document.getElementById('majority-age-by-country').value
    //console.log(majority);
    if (majority <= 0) {
      setError('Sorry, it appears your country does not allow you to visit our website.')
      return false;
    }

    var birth_month = document.getElementById('birth_month').value
    var birth_day = document.getElementById('birth_day').value
    var birth_year = document.getElementById('birth_year').value

    var sanitized_input = sanitizeInput(birth_month, birth_day, birth_year)
    var validated_input = validateInput(sanitized_input.birth_month, sanitized_input.birth_day, sanitized_input.birth_year)

    if ( validated_input.birth_month && validated_input.birth_day && validated_input.birth_year ) {

      if (isAllowedToDrink(sanitized_input.birth_month, sanitized_input.birth_day, sanitized_input.birth_year, window.majority)) {
        showWebsite()
      }
      else {
        setError("Unfortunately we are unable to offer you access to the website as it appears you are too young to enter the site.");
        setTimeout(function () {
          window.location.href = './'
        }, 5000)
      }
    } else {
      setError('Your input doesn\'t match the requested format. Please provide your birth date.')
    }

  }

  return false;

}

// If not trim method (IE), we roll our own
if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, ''); 
  }
}
$(document).ready(function() {
  window.majority = 21
  window.cookie_key = 'remember_my_age'

  $.magnificPopup.open({
    items: {
      src: 'gateway.html'
    },
    type: 'ajax',
    closeOnBgClick : false,
    enableEscapeKey: false,
    showCloseBtn : false,
    callbacks:{
      ajaxContentAdded: function(){
         if (window.attachEvent) {
          document.getElementById('enter_btn').attachEvent('onclick', enterWebsite)
        }
        else {
          document.getElementById('enter_btn').addEventListener('click', enterWebsite, false)
        }

      }
    }

    // You may add options here, they're exactly the same as for $.fn.magnificPopup call
    // Note that some settings that rely on click event (like disableOn or midClick) will not work here
  }, 0);
  enterWebsite()

});


