(function (t, e) { if (typeof define === "function" && define.amd) { define(["jquery"], e) } else if (typeof exports === "object") { module.exports = e(require("jquery")) } else { e(t.jQuery) } })(this, function (t) { t.transit = { version: "0.9.12", propertyMap: { marginLeft: "margin", marginRight: "margin", marginBottom: "margin", marginTop: "margin", paddingLeft: "padding", paddingRight: "padding", paddingBottom: "padding", paddingTop: "padding" }, enabled: true, useTransitionEnd: false }; var e = document.createElement("div"); var n = {}; function i(t) { if (t in e.style) return t; var n = ["Moz", "Webkit", "O", "ms"]; var i = t.charAt(0).toUpperCase() + t.substr(1); for (var r = 0; r < n.length; ++r){ var s = n[r] + i; if (s in e.style) { return s } } } function r() { e.style[n.transform] = ""; e.style[n.transform] = "rotateY(90deg)"; return e.style[n.transform] !== "" } var s = navigator.userAgent.toLowerCase().indexOf("chrome") > -1; n.transition = i("transition"); n.transitionDelay = i("transitionDelay"); n.transform = i("transform"); n.transformOrigin = i("transformOrigin"); n.filter = i("Filter"); n.transform3d = r(); var a = { transition: "transitionend", MozTransition: "transitionend", OTransition: "oTransitionEnd", WebkitTransition: "webkitTransitionEnd", msTransition: "MSTransitionEnd" }; var o = n.transitionEnd = a[n.transition] || null; for (var u in n) { if (n.hasOwnProperty(u) && typeof t.support[u] === "undefined") { t.support[u] = n[u] } } e = null; t.cssEase = { _default: "ease", "in": "ease-in", out: "ease-out", "in-out": "ease-in-out", snap: "cubic-bezier(0,1,.5,1)", easeInCubic: "cubic-bezier(.550,.055,.675,.190)", easeOutCubic: "cubic-bezier(.215,.61,.355,1)", easeInOutCubic: "cubic-bezier(.645,.045,.355,1)", easeInCirc: "cubic-bezier(.6,.04,.98,.335)", easeOutCirc: "cubic-bezier(.075,.82,.165,1)", easeInOutCirc: "cubic-bezier(.785,.135,.15,.86)", easeInExpo: "cubic-bezier(.95,.05,.795,.035)", easeOutExpo: "cubic-bezier(.19,1,.22,1)", easeInOutExpo: "cubic-bezier(1,0,0,1)", easeInQuad: "cubic-bezier(.55,.085,.68,.53)", easeOutQuad: "cubic-bezier(.25,.46,.45,.94)", easeInOutQuad: "cubic-bezier(.455,.03,.515,.955)", easeInQuart: "cubic-bezier(.895,.03,.685,.22)", easeOutQuart: "cubic-bezier(.165,.84,.44,1)", easeInOutQuart: "cubic-bezier(.77,0,.175,1)", easeInQuint: "cubic-bezier(.755,.05,.855,.06)", easeOutQuint: "cubic-bezier(.23,1,.32,1)", easeInOutQuint: "cubic-bezier(.86,0,.07,1)", easeInSine: "cubic-bezier(.47,0,.745,.715)", easeOutSine: "cubic-bezier(.39,.575,.565,1)", easeInOutSine: "cubic-bezier(.445,.05,.55,.95)", easeInBack: "cubic-bezier(.6,-.28,.735,.045)", easeOutBack: "cubic-bezier(.175, .885,.32,1.275)", easeInOutBack: "cubic-bezier(.68,-.55,.265,1.55)" }; t.cssHooks["transit:transform"] = { get: function (e) { return t(e).data("transform") || new f }, set: function (e, i) { var r = i; if (!(r instanceof f)) { r = new f(r) } if (n.transform === "WebkitTransform" && !s) { e.style[n.transform] = r.toString(true) } else { e.style[n.transform] = r.toString() } t(e).data("transform", r) } }; t.cssHooks.transform = { set: t.cssHooks["transit:transform"].set }; t.cssHooks.filter = { get: function (t) { return t.style[n.filter] }, set: function (t, e) { t.style[n.filter] = e } }; if (t.fn.jquery < "1.8") { t.cssHooks.transformOrigin = { get: function (t) { return t.style[n.transformOrigin] }, set: function (t, e) { t.style[n.transformOrigin] = e } }; t.cssHooks.transition = { get: function (t) { return t.style[n.transition] }, set: function (t, e) { t.style[n.transition] = e } } } p("scale"); p("scaleX"); p("scaleY"); p("translate"); p("rotate"); p("rotateX"); p("rotateY"); p("rotate3d"); p("perspective"); p("skewX"); p("skewY"); p("x", true); p("y", true); function f(t) { if (typeof t === "string") { this.parse(t) } return this } f.prototype = { setFromString: function (t, e) { var n = typeof e === "string" ? e.split(",") : e.constructor === Array ? e : [e]; n.unshift(t); f.prototype.set.apply(this, n) }, set: function (t) { var e = Array.prototype.slice.apply(arguments, [1]); if (this.setter[t]) { this.setter[t].apply(this, e) } else { this[t] = e.join(",") } }, get: function (t) { if (this.getter[t]) { return this.getter[t].apply(this) } else { return this[t] || 0 } }, setter: { rotate: function (t) { this.rotate = b(t, "deg") }, rotateX: function (t) { this.rotateX = b(t, "deg") }, rotateY: function (t) { this.rotateY = b(t, "deg") }, scale: function (t, e) { if (e === undefined) { e = t } this.scale = t + "," + e }, skewX: function (t) { this.skewX = b(t, "deg") }, skewY: function (t) { this.skewY = b(t, "deg") }, perspective: function (t) { this.perspective = b(t, "px") }, x: function (t) { this.set("translate", t, null) }, y: function (t) { this.set("translate", null, t) }, translate: function (t, e) { if (this._translateX === undefined) { this._translateX = 0 } if (this._translateY === undefined) { this._translateY = 0 } if (t !== null && t !== undefined) { this._translateX = b(t, "px") } if (e !== null && e !== undefined) { this._translateY = b(e, "px") } this.translate = this._translateX + "," + this._translateY } }, getter: { x: function () { return this._translateX || 0 }, y: function () { return this._translateY || 0 }, scale: function () { var t = (this.scale || "1,1").split(","); if (t[0]) { t[0] = parseFloat(t[0]) } if (t[1]) { t[1] = parseFloat(t[1]) } return t[0] === t[1] ? t[0] : t }, rotate3d: function () { var t = (this.rotate3d || "0,0,0,0deg").split(","); for (var e = 0; e <= 3; ++e){ if (t[e]) { t[e] = parseFloat(t[e]) } } if (t[3]) { t[3] = b(t[3], "deg") } return t } }, parse: function (t) { var e = this; t.replace(/([a-zA-Z0-9]+)\((.*?)\)/g, function (t, n, i) { e.setFromString(n, i) }) }, toString: function (t) { var e = []; for (var i in this) { if (this.hasOwnProperty(i)) { if (!n.transform3d && (i === "rotateX" || i === "rotateY" || i === "perspective" || i === "transformOrigin")) { continue } if (i[0] !== "_") { if (t && i === "scale") { e.push(i + "3d(" + this[i] + ",1)") } else if (t && i === "translate") { e.push(i + "3d(" + this[i] + ",0)") } else { e.push(i + "(" + this[i] + ")") } } } } return e.join(" ") } }; function c(t, e, n) { if (e === true) { t.queue(n) } else if (e) { t.queue(e, n) } else { t.each(function () { n.call(this) }) } } function l(e) { var i = []; t.each(e, function (e) { e = t.camelCase(e); e = t.transit.propertyMap[e] || t.cssProps[e] || e; e = h(e); if (n[e]) e = h(n[e]); if (t.inArray(e, i) === -1) { i.push(e) } }); return i } function d(e, n, i, r) { var s = l(e); if (t.cssEase[i]) { i = t.cssEase[i] } var a = "" + y(n) + " " + i; if (parseInt(r, 10) > 0) { a += " " + y(r) } var o = []; t.each(s, function (t, e) { o.push(e + " " + a) }); return o.join(", ") } t.fn.transition = t.fn.transit = function (e, i, r, s) { var a = this; var u = 0; var f = true; var l = t.extend(true, {}, e); if (typeof i === "function") { s = i; i = undefined } if (typeof i === "object") { r = i.easing; u = i.delay || 0; f = typeof i.queue === "undefined" ? true : i.queue; s = i.complete; i = i.duration } if (typeof r === "function") { s = r; r = undefined } if (typeof l.easing !== "undefined") { r = l.easing; delete l.easing } if (typeof l.duration !== "undefined") { i = l.duration; delete l.duration } if (typeof l.complete !== "undefined") { s = l.complete; delete l.complete } if (typeof l.queue !== "undefined") { f = l.queue; delete l.queue } if (typeof l.delay !== "undefined") { u = l.delay; delete l.delay } if (typeof i === "undefined") { i = t.fx.speeds._default } if (typeof r === "undefined") { r = t.cssEase._default } i = y(i); var p = d(l, i, r, u); var h = t.transit.enabled && n.transition; var b = h ? parseInt(i, 10) + parseInt(u, 10) : 0; if (b === 0) { var g = function (t) { a.css(l); if (s) { s.apply(a) } if (t) { t() } }; c(a, f, g); return a } var m = {}; var v = function (e) { var i = false; var r = function () { if (i) { a.unbind(o, r) } if (b > 0) { a.each(function () { this.style[n.transition] = m[this] || null }) } if (typeof s === "function") { s.apply(a) } if (typeof e === "function") { e() } }; if (b > 0 && o && t.transit.useTransitionEnd) { i = true; a.bind(o, r) } else { window.setTimeout(r, b) } a.each(function () { if (b > 0) { this.style[n.transition] = p } t(this).css(l) }) }; var z = function (t) { this.offsetWidth; v(t) }; c(a, f, z); return this }; function p(e, i) { if (!i) { t.cssNumber[e] = true } t.transit.propertyMap[e] = n.transform; t.cssHooks[e] = { get: function (n) { var i = t(n).css("transit:transform"); return i.get(e) }, set: function (n, i) { var r = t(n).css("transit:transform"); r.setFromString(e, i); t(n).css({ "transit:transform": r }) } } } function h(t) { return t.replace(/([A-Z])/g, function (t) { return "-" + t.toLowerCase() }) } function b(t, e) { if (typeof t === "string" && !t.match(/^[\-0-9\.]+$/)) { return t } else { return "" + t + e } } function y(e) { var n = e; if (typeof n === "string" && !n.match(/^[\-0-9\.]+/)) { n = t.fx.speeds[n] || t.fx.speeds._default } return b(n, "ms") } t.transit.getTransitionValue = d; return t });


/*!
 * jQuery Custom Scrollbar
 * https://github.com/mzubala/jquery-custom-scrollbar
 *
 * Released under the MIT license
 */
(function ($) {

    $.fn.customScrollbar = function (options, args) {

        var defaultOptions = {
            skin: undefined,
            hScroll: true,
            vScroll: true,
            updateOnWindowResize: false,
            animationSpeed: 300,
            onCustomScroll: undefined,
            swipeSpeed: 1,
            wheelSpeed: 40,
            fixedThumbWidth: undefined,
            fixedThumbHeight: undefined
        }

        var Scrollable = function (element, options) {
            this.$element = $(element);
            this.options = options;
            this.addScrollableClass();
            this.addSkinClass();
            this.addScrollBarComponents();
            if (this.options.vScroll)
                this.vScrollbar = new Scrollbar(this, new VSizing());
            if (this.options.hScroll)
                this.hScrollbar = new Scrollbar(this, new HSizing());
            this.$element.data("scrollable", this);
            this.initKeyboardScrolling();
            this.bindEvents();
        }

        Scrollable.prototype = {

            addScrollableClass: function () {
                if (!this.$element.hasClass("scrollable")) {
                    this.scrollableAdded = true;
                    this.$element.addClass("scrollable");
                }
            },

            removeScrollableClass: function () {
                if (this.scrollableAdded)
                    this.$element.removeClass("scrollable");
            },

            addSkinClass: function () {
                if (typeof (this.options.skin) == "string" && !this.$element.hasClass(this.options.skin)) {
                    this.skinClassAdded = true;
                    this.$element.addClass(this.options.skin);
                }
            },

            removeSkinClass: function () {
                if (this.skinClassAdded)
                    this.$element.removeClass(this.options.skin);
            },

            addScrollBarComponents: function () {
                this.assignViewPort();
                if (this.$viewPort.length == 0) {
                    this.$element.wrapInner("<div class=\"viewport\" />");
                    this.assignViewPort();
                    this.viewPortAdded = true;
                }
                this.assignOverview();
                if (this.$overview.length == 0) {
                    this.$viewPort.wrapInner("<div class=\"overview\" />");
                    this.assignOverview();
                    this.overviewAdded = true;
                }
                this.addScrollBar("vertical", "prepend");
                this.addScrollBar("horizontal", "append");
            },

            removeScrollbarComponents: function () {
                this.removeScrollbar("vertical");
                this.removeScrollbar("horizontal");
                if (this.overviewAdded)
                    this.$element.unwrap();
                if (this.viewPortAdded)
                    this.$element.unwrap();
            },

            removeScrollbar: function (orientation) {
                if (this[orientation + "ScrollbarAdded"])
                    this.$element.find(".scroll-bar." + orientation).remove();
            },

            assignViewPort: function () {
                this.$viewPort = this.$element.find(".viewport");
            },

            assignOverview: function () {
                this.$overview = this.$viewPort.find(".overview");
            },

            addScrollBar: function (orientation, fun) {
                if (this.$element.find(".scroll-bar." + orientation).length == 0) {
                    this.$element[fun]("<div class='scroll-bar " + orientation + "'><div class='thumb'></div></div>")
                    this[orientation + "ScrollbarAdded"] = true;
                }
            },

            resize: function (keepPosition) {
                if (this.vScrollbar)
                    this.vScrollbar.resize(keepPosition);
                if (this.hScrollbar)
                    this.hScrollbar.resize(keepPosition);
            },

            scrollTo: function (element) {
                if (this.vScrollbar)
                    this.vScrollbar.scrollToElement(element);
                if (this.hScrollbar)
                    this.hScrollbar.scrollToElement(element);
            },

            scrollToXY: function (x, y) {
                this.scrollToX(x);
                this.scrollToY(y);
            },

            scrollToX: function (x) {
                if (this.hScrollbar)
                    this.hScrollbar.scrollOverviewTo(x, true);
            },

            scrollToY: function (y) {
                if (this.vScrollbar)
                    this.vScrollbar.scrollOverviewTo(y, true);
            },

            remove: function () {
                this.removeScrollableClass();
                this.removeSkinClass();
                this.removeScrollbarComponents();
                this.$element.data("scrollable", null);
                this.removeKeyboardScrolling();
                if (this.vScrollbar)
                    this.vScrollbar.remove();
                if (this.hScrollbar)
                    this.hScrollbar.remove();
            },

            setAnimationSpeed: function (speed) {
                this.options.animationSpeed = speed;
            },

            isInside: function (element, wrappingElement) {
                var $element = $(element);
                var $wrappingElement = $(wrappingElement);
                var elementOffset = $element.offset();
                var wrappingElementOffset = $wrappingElement.offset();
                return (elementOffset.top >= wrappingElementOffset.top) && (elementOffset.left >= wrappingElementOffset.left) &&
                    (elementOffset.top + $element.height() <= wrappingElementOffset.top + $wrappingElement.height()) &&
                    (elementOffset.left + $element.width() <= wrappingElementOffset.left + $wrappingElement.width())
            },

            initKeyboardScrolling: function () {
                var _this = this;

                this.elementKeydown = function (event) {
                    if (document.activeElement === _this.$element[0]) {
                        if (_this.vScrollbar)
                            _this.vScrollbar.keyScroll(event);
                        if (_this.hScrollbar)
                            _this.hScrollbar.keyScroll(event);
                    }
                }

                this.$element
                    .attr('tabindex', '-1')
                    .keydown(this.elementKeydown);
            },

            removeKeyboardScrolling: function () {
                this.$element
                    .removeAttr('tabindex')
                    .unbind("keydown", this.elementKeydown);
            },

            bindEvents: function () {
                if (this.options.onCustomScroll)
                    this.$element.on("customScroll", this.options.onCustomScroll);
            }

        }

        var Scrollbar = function (scrollable, sizing) {
            this.scrollable = scrollable;
            this.sizing = sizing
            this.$scrollBar = this.sizing.scrollBar(this.scrollable.$element);
            this.$thumb = this.$scrollBar.find(".thumb");
            this.setScrollPosition(0, 0);
            this.resize();
            this.initMouseMoveScrolling();
            this.initMouseWheelScrolling();
            this.initTouchScrolling();
            this.initMouseClickScrolling();
            this.initWindowResize();
        }

        Scrollbar.prototype = {

            resize: function (keepPosition) {
                this.scrollable.$viewPort.height(this.scrollable.$element.height());
                this.sizing.size(this.scrollable.$viewPort, this.sizing.size(this.scrollable.$element));
                this.viewPortSize = this.sizing.size(this.scrollable.$viewPort);
                this.overviewSize = this.sizing.size(this.scrollable.$overview);
                this.ratio = this.viewPortSize / this.overviewSize;
                this.sizing.size(this.$scrollBar, this.viewPortSize);
                this.thumbSize = this.calculateThumbSize();
                this.sizing.size(this.$thumb, this.thumbSize);
                this.maxThumbPosition = this.calculateMaxThumbPosition();
                this.maxOverviewPosition = this.calculateMaxOverviewPosition();
                this.enabled = (this.overviewSize > this.viewPortSize);
                if (this.scrollPercent === undefined)
                    this.scrollPercent = 0.0;
                if (this.enabled)
                    this.rescroll(keepPosition);
                else
                    this.setScrollPosition(0, 0);
                this.$scrollBar.toggle(this.enabled);
            },

            calculateThumbSize: function () {
                var fixedSize = this.sizing.fixedThumbSize(this.scrollable.options)
                var size;
                if (fixedSize)
                    size = fixedSize;
                else
                    size = this.ratio * this.viewPortSize
                return Math.max(size, this.sizing.minSize(this.$thumb));
            },

            initMouseMoveScrolling: function () {
                var _this = this;
                this.$thumb.mousedown(function (event) {
                    if (_this.enabled)
                        _this.startMouseMoveScrolling(event);
                });
                this.documentMouseup = function (event) {
                    _this.stopMouseMoveScrolling(event);
                };
                $(document).mouseup(this.documentMouseup);
                this.documentMousemove = function (event) {
                    _this.mouseMoveScroll(event);
                };
                $(document).mousemove(this.documentMousemove);
                this.$thumb.click(function (event) {
                    event.stopPropagation();
                });
            },

            removeMouseMoveScrolling: function () {
                this.$thumb.unbind();
                $(document).unbind("mouseup", this.documentMouseup);
                $(document).unbind("mousemove", this.documentMousemove);
            },

            initMouseWheelScrolling: function () {
                var _this = this;
                this.scrollable.$element.mousewheel(function (event, delta, deltaX, deltaY) {
                    if (_this.enabled) {
                        if (_this.mouseWheelScroll(deltaX, deltaY)) {
                            event.stopPropagation();
                            event.preventDefault();
                        }
                    }
                });
            },

            removeMouseWheelScrolling: function () {
                this.scrollable.$element.unbind("mousewheel");
            },

            initTouchScrolling: function () {
                if (document.addEventListener) {
                    var _this = this;
                    this.elementTouchstart = function (event) {
                        if (_this.enabled)
                            _this.startTouchScrolling(event);
                    }
                    this.scrollable.$element[0].addEventListener("touchstart", this.elementTouchstart);
                    this.documentTouchmove = function (event) {
                        _this.touchScroll(event);
                    }
                    document.addEventListener("touchmove", this.documentTouchmove);
                    this.elementTouchend = function (event) {
                        _this.stopTouchScrolling(event);
                    }
                    this.scrollable.$element[0].addEventListener("touchend", this.elementTouchend);
                }
            },

            removeTouchScrolling: function () {
                if (document.addEventListener) {
                    this.scrollable.$element[0].removeEventListener("touchstart", this.elementTouchstart);
                    document.removeEventListener("touchmove", this.documentTouchmove);
                    this.scrollable.$element[0].removeEventListener("touchend", this.elementTouchend);
                }
            },

            initMouseClickScrolling: function () {
                var _this = this;
                this.scrollBarClick = function (event) {
                    _this.mouseClickScroll(event);
                };
                this.$scrollBar.click(this.scrollBarClick);
            },

            removeMouseClickScrolling: function () {
                this.$scrollBar.unbind("click", this.scrollBarClick);
            },

            initWindowResize: function () {
                if (this.scrollable.options.updateOnWindowResize) {
                    var _this = this;
                    this.windowResize = function () {
                        _this.resize();
                    };
                    $(window).resize(this.windowResize);
                }
            },

            removeWindowResize: function () {
                $(window).unbind("resize", this.windowResize);
            },

            isKeyScrolling: function (key) {
                return this.keyScrollDelta(key) != null;
            },

            keyScrollDelta: function (key) {
                for (var scrollingKey in this.sizing.scrollingKeys)
                    if (scrollingKey == key)
                        return this.sizing.scrollingKeys[key](this.viewPortSize);
                return null;
            },

            startMouseMoveScrolling: function (event) {
                this.mouseMoveScrolling = true;
                $("html").addClass("not-selectable");
                this.setUnselectable($("html"), "on");
                this.setScrollEvent(event);
            },

            stopMouseMoveScrolling: function (event) {
                this.mouseMoveScrolling = false;
                $("html").removeClass("not-selectable");
                this.setUnselectable($("html"), null);
            },

            setUnselectable: function (element, value) {
                if (element.attr("unselectable") != value) {
                    element.attr("unselectable", value);
                    element.find(':not(input)').attr('unselectable', value);
                }
            },

            mouseMoveScroll: function (event) {
                if (this.mouseMoveScrolling) {
                    var delta = this.sizing.mouseDelta(this.scrollEvent, event);
                    this.scrollThumbBy(delta);
                    this.setScrollEvent(event);
                }
            },

            startTouchScrolling: function (event) {
                if (event.touches && event.touches.length == 1) {
                    this.setScrollEvent(event.touches[0]);
                    this.touchScrolling = true;
                    event.stopPropagation();
                }
            },

            touchScroll: function (event) {
                if (this.touchScrolling && event.touches && event.touches.length == 1) {
                    var delta = -this.sizing.mouseDelta(this.scrollEvent, event.touches[0]) * this.scrollable.options.swipeSpeed;
                    var scrolled = this.scrollOverviewBy(delta);
                    if (scrolled) {
                        event.stopPropagation();
                        event.preventDefault();
                        this.setScrollEvent(event.touches[0]);
                    }
                }
            },

            stopTouchScrolling: function (event) {
                this.touchScrolling = false;
                event.stopPropagation();
            },

            mouseWheelScroll: function (deltaX, deltaY) {
                var delta = -this.sizing.wheelDelta(deltaX, deltaY) * this.scrollable.options.wheelSpeed;
                if (delta != 0)
                    return this.scrollOverviewBy(delta);
            },

            mouseClickScroll: function (event) {
                var delta = this.viewPortSize - 20;
                if (event["page" + this.sizing.scrollAxis()] < this.$thumb.offset()[this.sizing.offsetComponent()])
                    // mouse click over thumb
                    delta = -delta;
                this.scrollOverviewBy(delta);
            },

            keyScroll: function (event) {
                var keyDown = event.which;
                if (this.enabled && this.isKeyScrolling(keyDown)) {
                    if (this.scrollOverviewBy(this.keyScrollDelta(keyDown)))
                        event.preventDefault();
                }
            },

            scrollThumbBy: function (delta) {
                var thumbPosition = this.thumbPosition();
                thumbPosition += delta;
                thumbPosition = this.positionOrMax(thumbPosition, this.maxThumbPosition);
                var oldScrollPercent = this.scrollPercent;
                this.scrollPercent = thumbPosition / this.maxThumbPosition;
                var overviewPosition = (thumbPosition * this.maxOverviewPosition) / this.maxThumbPosition;
                this.setScrollPosition(overviewPosition, thumbPosition);
                if (oldScrollPercent != this.scrollPercent) {
                    this.triggerCustomScroll(oldScrollPercent);
                    return true
                }
                else
                    return false;
            },

            thumbPosition: function () {
                return this.$thumb.position()[this.sizing.offsetComponent()];
            },

            scrollOverviewBy: function (delta) {
                var overviewPosition = this.overviewPosition() + delta;
                return this.scrollOverviewTo(overviewPosition, false);
            },

            overviewPosition: function () {
                return -this.scrollable.$overview.position()[this.sizing.offsetComponent()];
            },

            scrollOverviewTo: function (overviewPosition, animate) {
                overviewPosition = this.positionOrMax(overviewPosition, this.maxOverviewPosition);
                var oldScrollPercent = this.scrollPercent;
                this.scrollPercent = overviewPosition / this.maxOverviewPosition;
                var thumbPosition = this.scrollPercent * this.maxThumbPosition;
                if (animate)
                    this.setScrollPositionWithAnimation(overviewPosition, thumbPosition);
                else
                    this.setScrollPosition(overviewPosition, thumbPosition);
                if (oldScrollPercent != this.scrollPercent) {
                    this.triggerCustomScroll(oldScrollPercent);
                    return true;
                }
                else
                    return false;
            },

            positionOrMax: function (p, max) {
                if (p < 0)
                    return 0;
                else if (p > max)
                    return max;
                else
                    return p;
            },

            triggerCustomScroll: function (oldScrollPercent) {
                this.scrollable.$element.trigger("customScroll", {
                    scrollAxis: this.sizing.scrollAxis(),
                    direction: this.sizing.scrollDirection(oldScrollPercent, this.scrollPercent),
                    scrollPercent: this.scrollPercent * 100
                }
                );
            },

            rescroll: function (keepPosition) {
                if (keepPosition) {
                    var overviewPosition = this.positionOrMax(this.overviewPosition(), this.maxOverviewPosition);
                    this.scrollPercent = overviewPosition / this.maxOverviewPosition;
                    var thumbPosition = this.scrollPercent * this.maxThumbPosition;
                    this.setScrollPosition(overviewPosition, thumbPosition);
                }
                else {
                    var thumbPosition = this.scrollPercent * this.maxThumbPosition;
                    var overviewPosition = this.scrollPercent * this.maxOverviewPosition;
                    this.setScrollPosition(overviewPosition, thumbPosition);
                }
            },

            setScrollPosition: function (overviewPosition, thumbPosition) {
                this.$thumb.css(this.sizing.offsetComponent(), thumbPosition + "px");
                this.scrollable.$overview.css(this.sizing.offsetComponent(), -overviewPosition + "px");
            },

            setScrollPositionWithAnimation: function (overviewPosition, thumbPosition) {
                var thumbAnimationOpts = {};
                var overviewAnimationOpts = {};
                thumbAnimationOpts[this.sizing.offsetComponent()] = thumbPosition + "px";
                this.$thumb.animate(thumbAnimationOpts, this.scrollable.options.animationSpeed);
                overviewAnimationOpts[this.sizing.offsetComponent()] = -overviewPosition + "px";
                this.scrollable.$overview.animate(overviewAnimationOpts, this.scrollable.options.animationSpeed);
            },

            calculateMaxThumbPosition: function () {
                return this.sizing.size(this.$scrollBar) - this.thumbSize;
            },

            calculateMaxOverviewPosition: function () {
                return this.sizing.size(this.scrollable.$overview) - this.sizing.size(this.scrollable.$viewPort);
            },

            setScrollEvent: function (event) {
                var attr = "page" + this.sizing.scrollAxis();
                if (!this.scrollEvent || this.scrollEvent[attr] != event[attr])
                    this.scrollEvent = { pageX: event.pageX, pageY: event.pageY };
            },

            scrollToElement: function (element) {
                var $element = $(element);
                if (this.sizing.isInside($element, this.scrollable.$overview) && !this.sizing.isInside($element, this.scrollable.$viewPort)) {
                    var elementOffset = $element.offset();
                    var overviewOffset = this.scrollable.$overview.offset();
                    var viewPortOffset = this.scrollable.$viewPort.offset();
                    this.scrollOverviewTo(elementOffset[this.sizing.offsetComponent()] - overviewOffset[this.sizing.offsetComponent()], true);
                }
            },

            remove: function () {
                this.removeMouseMoveScrolling();
                this.removeMouseWheelScrolling();
                this.removeTouchScrolling();
                this.removeMouseClickScrolling();
                this.removeWindowResize();
            }

        }

        var HSizing = function () {
        }

        HSizing.prototype = {
            size: function ($el, arg) {
                if (arg)
                    return $el.width(arg);
                else
                    return $el.width();
            },

            minSize: function ($el) {
                return parseInt($el.css("min-width")) || 0;
            },

            fixedThumbSize: function (options) {
                return options.fixedThumbWidth;
            },

            scrollBar: function ($el) {
                return $el.find(".scroll-bar.horizontal");
            },

            mouseDelta: function (event1, event2) {
                return event2.pageX - event1.pageX;
            },

            offsetComponent: function () {
                return "left";
            },

            wheelDelta: function (deltaX, deltaY) {
                return deltaX;
            },

            scrollAxis: function () {
                return "X";
            },

            scrollDirection: function (oldPercent, newPercent) {
                return oldPercent < newPercent ? "right" : "left";
            },

            scrollingKeys: {
                37: function (viewPortSize) {
                    return -10; //arrow left
                },
                39: function (viewPortSize) {
                    return 10; //arrow right
                }
            },

            isInside: function (element, wrappingElement) {
                var $element = $(element);
                var $wrappingElement = $(wrappingElement);
                var elementOffset = $element.offset();
                var wrappingElementOffset = $wrappingElement.offset();
                return (elementOffset.left >= wrappingElementOffset.left) &&
                    (elementOffset.left + $element.width() <= wrappingElementOffset.left + $wrappingElement.width());
            }

        }

        var VSizing = function () {
        }

        VSizing.prototype = {

            size: function ($el, arg) {
                if (arg)
                    return $el.height(arg);
                else
                    return $el.height();
            },

            minSize: function ($el) {
                return parseInt($el.css("min-height")) || 0;
            },

            fixedThumbSize: function (options) {
                return options.fixedThumbHeight;
            },

            scrollBar: function ($el) {
                return $el.find(".scroll-bar.vertical");
            },

            mouseDelta: function (event1, event2) {
                return event2.pageY - event1.pageY;
            },

            offsetComponent: function () {
                return "top";
            },

            wheelDelta: function (deltaX, deltaY) {
                return deltaY;
            },

            scrollAxis: function () {
                return "Y";
            },

            scrollDirection: function (oldPercent, newPercent) {
                return oldPercent < newPercent ? "down" : "up";
            },

            scrollingKeys: {
                38: function (viewPortSize) {
                    return -10; //arrow up
                },
                40: function (viewPortSize) {
                    return 10; //arrow down
                },
                33: function (viewPortSize) {
                    return -(viewPortSize - 20); //page up
                },
                34: function (viewPortSize) {
                    return viewPortSize - 20; //page down
                }
            },

            isInside: function (element, wrappingElement) {
                var $element = $(element);
                var $wrappingElement = $(wrappingElement);
                var elementOffset = $element.offset();
                var wrappingElementOffset = $wrappingElement.offset();
                return (elementOffset.top >= wrappingElementOffset.top) &&
                    (elementOffset.top + $element.height() <= wrappingElementOffset.top + $wrappingElement.height());
            }

        }

        return this.each(function () {
            if (options == undefined)
                options = defaultOptions;
            if (typeof (options) == "string") {
                var scrollable = $(this).data("scrollable");
                if (scrollable)
                    scrollable[options](args);
            }
            else if (typeof (options) == "object") {
                options = $.extend(defaultOptions, options);
                new Scrollable($(this), options);
            }
            else
                throw "Invalid type of options";
        });

    }
        ;

})
    (jQuery);

(function ($) {

    var types = ['DOMMouseScroll', 'mousewheel'];

    if ($.event.fixHooks) {
        for (var i = types.length; i;) {
            $.event.fixHooks[types[--i]] = $.event.mouseHooks;
        }
    }

    $.event.special.mousewheel = {
        setup: function () {
            if (this.addEventListener) {
                for (var i = types.length; i;) {
                    this.addEventListener(types[--i], handler, false);
                }
            } else {
                this.onmousewheel = handler;
            }
        },

        teardown: function () {
            if (this.removeEventListener) {
                for (var i = types.length; i;) {
                    this.removeEventListener(types[--i], handler, false);
                }
            } else {
                this.onmousewheel = null;
            }
        }
    };

    $.fn.extend({
        mousewheel: function (fn) {
            return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
        },

        unmousewheel: function (fn) {
            return this.unbind("mousewheel", fn);
        }
    });


    function handler(event) {
        var orgEvent = event || window.event, args = [].slice.call(arguments, 1), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
        event = $.event.fix(orgEvent);
        event.type = "mousewheel";

        // Old school scrollwheel delta
        if (orgEvent.wheelDelta) {
            delta = orgEvent.wheelDelta / 120;
        }
        if (orgEvent.detail) {
            delta = -orgEvent.detail / 3;
        }

        // New school multidimensional scroll (touchpads) deltas
        deltaY = delta;

        // Gecko
        if (orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
            deltaY = 0;
            deltaX = delta;
        }

        // Webkit
        if (orgEvent.wheelDeltaY !== undefined) {
            deltaY = orgEvent.wheelDeltaY / 120;
        }
        if (orgEvent.wheelDeltaX !== undefined) {
            deltaX = orgEvent.wheelDeltaX / 120;
        }

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

})(jQuery);
/*!
 * jQuery Custom Scrollbar
 * https://github.com/mzubala/jquery-custom-scrollbar
 *
 * Released under the MIT license
 */
