var leftNav = {
	leftNavElements : {
		body : $("body"),
		quickLinksWrapper : $(".mega-menu-wrapper"),
		quickLinks : $(".menu-options"),
		quickLinksItems : $(".menu-options ul li"),
		quickLinksItemActive : $(".menu-options ul li.active"),
		quickLinksIcons : $(".menu-options ul li .link"),
		quickLinksTitle : $(".menu-options ul li .label-title"),
		countryPopupLogout : $(".country-popup, .logout-option")
	},
	init: function(){
		leftNav.hoverEvents();
		leftNav.registerEvents();
		leftNav.dynamicHeaderHeight();
		leftNav.quickLinksEnabled();
		leftNav.detectIEBrowser();
		$(".aside-menu-trigger .icon").next("span").addClass("hamburger-title");
	},
	
	detectIEBrowser : function(){
		if ((/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) || (navigator.appVersion.indexOf("MSIE 10") !== -1)) {
			leftNav.leftNavElements.body.addClass("ie-browser");
		}
	},
	/* Margin top based on header height */
	dynamicHeaderHeight : function (){
		var headerHeight = $(".header-container").outerHeight();
		$(".global-web #maincontent").css("margin-top",headerHeight);
		$(leftNav.leftNavElements.quickLinksWrapper).css("top",headerHeight);
	},
	/* Mega Menu width and height dynamic*/
	dynamicMegaMenuHeight : function(){
		var headerHeight = $(".header-container").outerHeight(),
		asideMenuWidth = $(".aside-menu-trigger").outerWidth();
		$(leftNav.leftNavElements.quickLinksWrapper).css({"width":($(window).width()-asideMenuWidth),"height":($(window).height()-headerHeight)});
	},
	/* Reset left Naviation when click on Hamburger Icon */
	resetLeftNavHamburger : function(){
		leftNav.leftNavElements.body.removeClass("open-aside-menu");
		leftNav.leftNavElements.countryPopupLogout.removeClass("active");
	},
	/* When hover on quick links */
	hoverEvents : function(){
		var $windowWidth = $(window).width();
		/* Desktop */
		if($windowWidth > 1025){
			$(document).on("mouseover",".menu-options ul li .link",function(){
				var $this = $(this);
					leftNav.leftNavElements.quickLinksItems.removeClass("active");
				if (!leftNav.leftNavElements.body.hasClass("overlay-active")) {
					leftNav.leftNavElements.quickLinksTitle.removeClass("active");
					$this.next(".label-title").addClass("active");
				}
				else{
					$this.next(".label-title").addClass("active");
					leftNav.leftNavElements.quickLinksWrapper.removeClass("active");
					leftNav.leftNavElements.body.removeClass("overlay-active");
				}
			}).on("mouseout",".menu-options ul li .link",function(){
				leftNav.leftNavElements.quickLinksTitle.removeClass("active");
			}).on("mouseover",'.menu-options ul li .label-title',function(){
				$(this).addClass("active");
			}).on("mouseout",'.menu-options ul li .label-title',function(){
				leftNav.leftNavElements.quickLinksTitle.removeClass("active");
			});
		}
	},
	/* click events */
	registerEvents: function(){
		var $windowWidth = $(window).width();
		/* Reset Menga menu when click on main  form */
		$(".mainForm").click(function(){
			leftNav.leftNavElements.body.removeClass("open-aside-menu");
			leftNav.leftNavElements.countryPopupLogout.removeClass("active");
			leftNav.leftNavElements.quickLinksItemActive.removeClass("active");
		});
		
		$(leftNav.leftNavElements.quickLinks).click(function(e){
			e.stopPropagation();
		});
		/* Reset left Naviation when click on Hamburger Icon */
		leftNav.leftNavElements.quickLinksItems.click(function(){
			leftNav.resetLeftNavHamburger();
		});
		/* When click on Quick link title mega menu open */
		$(leftNav.leftNavElements.quickLinksTitle).click(function(e){
			var $this = $(this),
				$thisParent = $this.closest("li.mega-menu-link"),
				$thisURL = $this.attr("data-url"),
				$thisTarget = $this.attr("data-target");
				leftNav.leftNavElements.quickLinksTitle.removeClass("active");
				leftNav.leftNavElements.quickLinksItems.removeClass("active");
				leftNav.leftNavElements.quickLinksWrapper.removeClass("active");
				if($windowWidth < 1025){
					e.stopPropagation();
					if($this.parents("li.mega-menu-link").find(".mega-menu-wrapper").length){
							$this.parents("li.mega-menu-link").addClass("active").find(".mega-menu-wrapper").addClass("active");;
							leftNav.leftNavElements.body.addClass("overlay-active");
						}
					else{
						leftNav.leftNavElements.quickLinksItems.removeClass("active");
						if($thisTarget){
							window.open($thisURL, $thisTarget);
						}
						else{
							window.location.href = $thisURL;
						}
					}
				}
				else{
					if($this.parents("li.mega-menu-link").find(".mega-menu-wrapper").length){
						$this.parents("li.mega-menu-link").addClass("active").find(".mega-menu-wrapper").addClass("active");;
						leftNav.leftNavElements.body.addClass("overlay-active");
					}
					else{
						leftNav.leftNavElements.quickLinksItems.removeClass("active");
						var $thisURL = $this.attr("data-url");
						if($thisURL) {
							leftNav.leftNavElements.quickLinksItems.removeClass("active");
							if($thisTarget){
								window.open($thisURL, $thisTarget);
							}
							else{
								window.location.href = $thisURL;
							}
						}
					}
					if (!$('body').hasClass("overlay-active")) {
						leftNav.leftNavElements.quickLinksItems.removeClass("active");
						leftNav.leftNavElements.quickLinksTitle.removeClass("active");
						$this.next(".label-title").addClass("active");
					}
				}
				leftNav.dynamicMegaMenuHeight();
		});
		/* When click on Quick link icon mega menu open */
		$(leftNav.leftNavElements.quickLinksIcons).click(function(e){
			var $this = $(this);
				leftNav.leftNavElements.quickLinksTitle.removeClass("active");
				leftNav.leftNavElements.quickLinksWrapper.removeClass("active");
				leftNav.leftNavElements.quickLinksItems.removeClass("active");
			var $thisURL = $this.next(".label-title").attr("data-url"),
				$thisTarget = $this.next(".label-title").attr("data-target");
			
			if($windowWidth < 1025){
					e.stopPropagation();
					leftNav.leftNavElements.quickLinksItems.removeClass("active");
					$this.next(".label-title").addClass("active");
					if($this.closest("li.mega-menu-link").hasClass("ipad-active")){
						$("li.mega-menu-link.ipad-active").removeClass("ipad-active");
						if($this.closest("li.mega-menu-link").find(".mega-menu-wrapper").length){
							leftNav.leftNavElements.quickLinksTitle.removeClass("active");
							$this.closest("li.mega-menu-link").addClass("active").find(".mega-menu-wrapper").removeClass("active");
							leftNav.leftNavElements.body.addClass("overlay-active");
						}
						else{
							leftNav.leftNavElements.quickLinksTitle.removeClass("active");
							$("li.mega-menu-link.ipad-active").removeClass("ipad-active");
							if($thisTarget){
								$("li.mega-menu-link.ipad-active").removeClass("ipad-active");
								window.open($thisURL, $thisTarget);
							}
							else{
								window.location.href = $thisURL;
							}
						}
					}
					$(".menu-options ul li.mega-menu-link.ipad-active").removeClass("ipad-active");
					$this.closest("li.mega-menu-link").addClass("ipad-active");
				}
				else{
					if($this.parents("li.mega-menu-link").find(".mega-menu-wrapper").length){
						$this.parents("li.mega-menu-link").addClass("active").find(".mega-menu-wrapper").removeClass("active");
						leftNav.leftNavElements.body.addClass("overlay-active");
					}
					else{
						leftNav.leftNavElements.quickLinksItems.removeClass("active");
						if($thisTarget){
							window.open($thisURL, $thisTarget);
						}
						else{
							window.location.href = $thisURL;
						}
					}
				}
				leftNav.dynamicMegaMenuHeight();
		});
		
		if(leftNav.leftNavElements.body.hasClass("overlay-active")){
			leftNav.dynamicMegaMenuHeight();
		}
		if($windowWidth < 1025){
			$(document).click(function(){
				leftNav.leftNavElements.body.removeClass("overlay-active");
				leftNav.leftNavElements.quickLinksItems.removeClass("active ipad-active");
				leftNav.leftNavElements.quickLinksTitle.removeClass("active");
			});
		}
		/* close mega menu When click on close icon */
		$(".close-mega-menu").click(function(){
			leftNav.leftNavElements.quickLinksItems.removeClass("active ipad-active");
			leftNav.leftNavElements.quickLinksWrapper.removeClass("active");
			leftNav.leftNavElements.body.removeClass("overlay-active");
		});
		/* Reset left Naviation when click on search container */
		$(".icon-search-new, .header-row .nav *, .search-popup-container.search-popup *").click(function(){
			leftNav.leftNavElements.quickLinksWrapper.removeClass("active");
			leftNav.leftNavElements.body.removeClass("open-aside-menu overlay-active");
			leftNav.leftNavElements.quickLinksItems.removeClass("active ipad-active");
			leftNav.leftNavElements.quickLinksTitle.removeClass("active");
		});
	},
	/* Quick links show based on current url */
	quickLinksEnabled : function (){
		var hiddenVal = $("#hdnGroupHeaders").val(),
		hdnCorporateHeader = $("#hdnCorporateHeader").val(),
		href = window.location.pathname.toLowerCase().replace(/.*\/\/[^\/]*/, '').trim(),
		segments = href.replace('#','').split( '/' );
		if(hdnCorporateHeader || hiddenVal){
			segments = segments.filter(function(k){return k != ""});
			var splitUrl = segments[1],
			headerGroupTxt = hiddenVal.indexOf(splitUrl) >= 0;
			$(".quick-links-wrapper").removeClass("active");
			if(headerGroupTxt){
				if($(".quick-links-wrapper[data-id="+splitUrl+"]").length){
					$(".quick-links-wrapper[data-id="+splitUrl+"]").addClass("active");
				}
			}
			else{
				$(".quick-links-wrapper[data-id="+hdnCorporateHeader+"]").addClass("active");
			}
		}
	}
}

leftNav.init();

/* Hamburger Menu Code Starts Here*/
$(document).ready(function () {
    var $asideMenu = $('.aside-menu')
    $pusher = $('.pusher');
    $('.aside-menu .menu-subtitle').css("cursor", "pointer");
    $('.aside-menu .menu-title span').css("cursor", "pointer");

    var span = $('<span />').attr({ 'class': 'hamburger-menu' });
    $('#search .icon-burguer').append(span);
    //$('#search .icon').css('margin-bottom', '-15px');
    $('#errpage .icon-burguer').append(span); /* Display Hamburger icon on Page not found page */

    $(".mainForm").click(function () {
        $("body").removeClass("open-aside-menu");
        $(".menu-has-open").removeClass("menu-has-open");
        $(".country-popup, .logout-option").removeClass("active");
    });

    /* Defines height for the scrollable container Start*/
    var wh = $(window).height();
    var ww = $(window).width();
    var menu_height = 0;
    $(".menu-item").each(function () {

        var menu_item_height = $(this).outerHeight();
        menu_height = $(this).parent().find(".menu-subtitle").outerHeight() + $(this).parent().find(".menu-title").outerHeight() + menu_item_height;
        if (ww >= 768) {
            if (wh < menu_height) {
                var th = $(this).parent().find(".menu-subtitle").outerHeight() + $(this).parent().find(".menu-title").outerHeight();
                var hd = wh - th;
                $(this).css("height", hd);
                $(this).addClass(" default-skin");
                $(this).customScrollbar();
                var vh = $(this).find(".overview").outerHeight();
                $(this).find(".viewport").css("height", vh);
            }
        }
    });
    /* Defines height for the scrollable container End */

    $(".aside-menu-trigger .icon").click(function (e) {

        e.stopPropagation();

        $(this).toggleClass("menu-has-open");

        var menus = $(".aside-menu .item");
        var path = document.location.pathname.replace(/\/$/, "");

        var to = path.lastIndexOf('/');
        to = to == -1 ? path.length : to + 1;
        var nextvalidpath = path.substring(0, to - 1);

        menus.each(function (i, item) {

            if ($(item).attr("data-tag").toLowerCase() === path.toLowerCase()) {

                if (!$(item).hasClass("show-menu")) {
                    $(item).addClass('show-menu');
                }
            }
            else if ($(item).attr("data-tag").toLowerCase() === nextvalidpath.toLowerCase()) {
                if (!$(item).hasClass("show-menu")) {
                    $(item).addClass('show-menu');
                }
            }
        });
        $('.item.active').addClass('show-menu');
        $('.show-menu.show-menu-level-two').removeClass('show-menu show-menu-level-two');

    });

    if (ww >= 768) { /* $(".aside-menu .menu-item a").hover only works on desktop */
        $(".aside-menu .menu-item a").hover(function (e) {

            e.preventDefault();
            e.stopPropagation();
            var $this = $(this),
                currentlyActiveLength = $('.item.show-menu').length,
                currentHref = $this.attr('href'),
                $toActivateMenu = $('[data-tag="' + currentHref + '"]');
            $pusher.addClass('has-two-level-menu');
            var menus = $(".aside-menu .item");
            menus.each(function (i, item) {
                if ($(item).attr("data-tag").toLowerCase() === currentHref.toLowerCase()) {
                    currentHref = $(item).attr("data-tag");
                }
            });
            $toActivateMenu = $('[data-tag="' + currentHref + '"]');

            if ($toActivateMenu.length) {

                if (currentlyActiveLength == 1) {
                    $toActivateMenu.addClass('show-menu show-menu-level-two');
                    $pusher.addClass('has-two-level-menu');
                    if (!$this.closest('.show-menu-level-two').length) {
                        $('.show-menu-level-two').removeClass('show-menu show-menu-level-two');
                        $toActivateMenu.removeClass('show-menu show-menu-level-two');
                        $toActivateMenu.addClass('show-menu show-menu-level-two');

                    }

                } else {
                    if (!$this.closest('.show-menu-level-two').length) {
                        $('.show-menu-level-two').removeClass('show-menu show-menu-level-two');
                        $toActivateMenu.addClass('show-menu show-menu-level-two');


                    }

                }
            }
            else {
                if (currentlyActiveLength > 0) {
                    if (!$this.closest('.show-menu-level-two').length) {
                        $('.show-menu.show-menu-level-two').removeClass('show-menu show-menu-level-two');
                    }
                }

            }


        });
    }   /* $(".aside-menu .menu-item a").hover only works on desktop */

    $(".aside-menu .menu-item a").click(function (e) {

        //var $this = $(this),
        //    currentlyActiveLength = $('.item.show-menu').length,
        //    currentHref = $this.attr('href'),
        //    $toActivateMenu = $('[data-tag="' + currentHref + '"]');
        //if ($toActivateMenu.length) {
        //    if ($this.closest('.show-menu-level-two').length) {
        //        if (currentlyActiveLength > 0) {
        //            return true;
        //        }
        //    }
        //    else {
        //        return false;
        //    }
        //}
        //else {
        //    if ($toActivateMenu.length == 0) {
        //        return true;
        //    }
        //    else {
        //        return false;
        //    }

        //}
    });


    $('.aside-menu .menu-subtitle').hover(function (e) {

    });
    $('.aside-menu .menu-subtitle').click(function (e) {

        e.preventDefault();
        var currentHref = $(this).parent().attr('data-tag');
        window.location.href = currentHref;

    });
    $('.aside-menu .menu-title span').hover(function (e) {

    });
    $('.aside-menu .menu-title span').click(function (e) {

        e.preventDefault();
        var currentHref = $(this).attr('href');
        window.location.href = currentHref;

    });
    $('.aside-menu .menu-title a').hover(function (e) {

    });
    $('.aside-menu .menu-title a').click(function (e) {

        e.preventDefault();
        var $this = $(this),
            currentHref = $this.attr('href'),
            currentlyActiveLength = $('.item.show-menu').length,
            $toActivateMenu = $('[data-tag="' + currentHref + '"]');
        $('.show-menu').removeClass('show-menu');
        var menus = $(".aside-menu .item");
        menus.each(function (i, item) {

            if ($(item).attr("data-tag").toLowerCase() === currentHref.toLowerCase()) {
                currentHref = $(item).attr("data-tag");
            }
        });
        $toActivateMenu = $('[data-tag="' + currentHref + '"]');
        $toActivateMenu.addClass('show-menu');
        $(".pusher.has-two-level-menu").removeClass("has-two-level-menu");
        var cls = document.getElementsByClassName("item active");
        for (var i = 0; i < cls.length; i++) {
            cls[i].removeAttribute('style');
        }

    });
})
    /* Hamburger Menu Code Ends Here*/

/* Footer Mobile Layout Adjustment when social icon is empty START*/
$(function () {
    // $(".social-links").empty();
    if (window.innerWidth <= 568) {
        if ($(".social-links").children().length == 0) {
            $(".footer").css("padding-top", "0");
            $(".footer .site-info").css("border-top-width", "0");
        }
    }
});
/* Footer Mobile Layout Adjustment when social icon is empty END*/

/* Product Description Empty more padding on wrapper div for action buttons START */
$(function () {
    if ($(".product-detail .left .inner .description").text() == "") {
        $(".product-detail .left .inner .action").css("padding-top", "34px");
    }
    if (window.innerWidth <= 568) {
        if ($(".product-detail .left .inner .description").text() == "") {
            $(".product-detail .left .inner .action").css("padding-top", "40px");
        }
    }
})
/* Product Description Empty more padding on wrapper div for action buttons END */