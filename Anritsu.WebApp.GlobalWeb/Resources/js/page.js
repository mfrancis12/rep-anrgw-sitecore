function initPage() {
    $(".tab-interface").each(function (i, item) {
        var tab = $(item).children("ul");
        var tabContent = $(item).find(".section");
        $(tab).find("li a").each(function (index, el) {
            var activeEleIndex = $('.tab-interface').find('ul li .active').parent().index();
            $('.tab-interface').find('.location-inner-tab .section').eq(activeEleIndex).addClass("active");
            $(el).click(function (e) {
                if (!$(item).hasClass('product-downloads')) {
                    e.stopPropagation();
                    e.preventDefault();
                }
                $(tab).find("li a").removeClass("active");
                $(el).addClass("active");
                tabContent.removeClass("active");
                if (tabContent.eq(index).find('.new-products-slider') && tabContent.eq(index).find('.new-products-slider .slick-slide').width() == 0) {
                    tabContent.eq(index).css('visibility', 'hidden');
                    tabContent.eq(index).addClass("active");
                    $('.new-products-slider').slick('unslick');
                    $('.new-products-slider').slick(customSlickOptions);
                    tabContent.eq(index).css('visibility', 'visible');
                } else {
                    tabContent.eq(index).addClass("active");
                }
                
                var activeEleIndex = $('.tab-interface').find('.location-inner-tab').eq(index).children('ul').find('li a.active').parent().index();
                var tabInnerContent = $(".tab-interface").find(".active").eq(0).closest('ul').siblings('.section').eq(index).find('.location-inner-tab .section')
                tabInnerContent.removeClass('active');
                tabInnerContent.eq(activeEleIndex).addClass('active');
                if ($(".content-list.active > div").not(".pagination").find(".download-item").length == 0) {
                    $(".content-list.active > .pagination").hide();
                    $(".content-list.active > ul").hide();
                    $(".content-list.active > h3").css("visibility", "hidden");
                }
                else {
                    $(".content-list.active > .pagination").show();
                    $(".content-list.active > ul").show();
                    $(".content-list.active > h3").css("visibility", "visible");
                }
                
            });
        });
    });
    window.homeSlider = $(".home-slider").slick({
        "dots": true,
        "autoplay": true,
        "autoplaySpeed": 5000,
        "speed": 500
    });

    $(".tab-interface-2").each(function (i, item) {
        var tab = $(item).children("ul");
        var tabContent = $(item).children(".section");
        $(tab).find("li a").each(function (index, el) {
            $(el).click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(tab).find("li a").removeClass("active");
                $(el).addClass("active");
                tabContent.removeClass("active");
                tabContent.eq(index).addClass("active");
                if ($(".ie8").length > 0 && $('.product-downloads').length > 0) {
                    $(".tab-interface.inner-tab.product-downloads > ul > li > a").eq(0).trigger("click");
                }
            });
        });
        $(tabContent).find(".section-title").each(function (index, el) {
            $(el).click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(tab).find("li a").removeClass("active");
                $(tab).find("li a").eq(index).addClass("active");
                $(tabContent).removeClass("active");
                $(tabContent).eq(index).addClass("active");
            });
        });
    });
    var slickResponsiveArray = [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
        }
    }, {
        breakpoint: 905,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1
        }
    }, {
        breakpoint: 578,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }];
    window.productImageSlider = $(".product-image-slider").slick({
        "dots": false,
        "slidesToShow": 4,
        "slidesToScroll": 1,
        "variableWidth": true,
        "infinite": false,
        "speed": 500,
        responsive: slickResponsiveArray
    });
    window.productVideoSlider = $(".product-video-slider").slick({
        "dots": false,
        "slidesToShow": 4,
        "slidesToScroll": 1,
        "variableWidth": true,
        "infinite": false,
        "speed": 500,
        responsive: slickResponsiveArray
    });


    window.productRelatedSlider = $(".product-related-slider").slick({
        "dots": false,
        "slidesToShow": 4,
        "slidesToScroll": 1,
        "variableWidth": true,
        "infinite": false,
        "speed": 500,
        responsive: slickResponsiveArray
    });
    $(".tab-interface-2").each(function (i, item) {
        var tab = $(item).children("ul");
        var tabContent = $(item).children(".section");
        $(tab).find("li a").each(function (index, el) {
            $(el).click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(tab).find("li a").removeClass("active");
                $(el).addClass("active");
                tabContent.removeClass("active");
                tabContent.eq(index).addClass("active");
                if ($(".ie8").length > 0 && $('.product-downloads').length > 0) {
                    $(".tab-interface.inner-tab.product-downloads > ul > li > a").eq(0).trigger("click");
                }
            });
        });
        $(tabContent).find(".section-title").each(function (index, el) {
            $(el).click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(tab).find("li a").removeClass("active");
                $(tab).find("li a").eq(index).addClass("active");
                $(tabContent).removeClass("active");
                $(tabContent).eq(index).addClass("active");
            });
        });
    });
    $(".select-mobile select").change(function () {
		var $this = $(this);
        $this.closest(".tab-interface").find("ul li a").eq($this.children('option:selected').index()).click();		
    });
    $(".products-info .action a").click(function (e) {

        e.stopPropagation();
        e.preventDefault();
        var target = $(this).attr("href");
        $("body, html").animate({
            scrollTop: $(target).offset().top
        }, 500);
    });
    if ($('.category-slider').length > 0) {

        window.categorySlider = $(".category-slider").slick({
            "dots": true,
            "autoplay": true,
            "autoplaySpeed": 5000,
            "speed": 500
        });
        categorySlider.on('afterChange', function (slick, slider) {
            $(".category-slider-tab .item").removeClass("active");
            $(".category-slider-tab .item").eq(slider.currentSlide).addClass("active");
        });
        $(".category-slider-tab .item").each(function (i, item) {
            $(item).click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                categorySlider.slick('slickGoTo', i);
            });
        });
    }
    if ($('.product-image-slider').length > 0) {

        var popupTitle = $(".product-detail h1.cate").html();
        $('.product-image-slider .slider a').magnificPopup({
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return popupTitle;
                }
            }
        });

    }


    window.newProductsSlider = $('.new-products-slider').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 680,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 568,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    window.customSlickOptions = {
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 680,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 568,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    }
    

    $(".tab-interface").each(function (i, item) {
        var tab = $(item).children("ul");
        var tabContent = $(item).children(".section");
        $(tab).find("li a").each(function (index, el) {
            $(el).click(function (e) {
                if (!$(item).hasClass("product-downloads")) {
                    //e.stopPropagation();
                    e.preventDefault();
                }
                $(tab).find("li a").removeClass("active");
                $(el).addClass("active");
                tabContent.removeClass("active");
                tabContent.eq(index).addClass("active");
            });
        });
    });
    var totop = $("#totop");
    totop.click(function () {
        $("body, html").animate({
            scrollTop: 0
        }, 500);
    });
    var asideHelper = $(".aside-helper");
    asideHelper.css("display", "none");
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
            asideHelper.css("display", "block");
        } else {
            asideHelper.css("display", "none");
        }
    });
    var announcement = function () {
        var maxlen = $(".announcement .text").length;
        if (maxlen > 0) {
            var len = 0;
            $(".announcement .text").eq(0).addClass("active");
            setInterval(function () {
                if (len >= maxlen - 1) {
                    len = 0;
                } else {
                    len++;
                }
                $(".announcement .text").css("opacity", "0").removeClass("active");
                $(".announcement .text").eq(len).addClass("active").transition({
                    opacity: "1",
                    duration: 1000,
                    easing: 'out'
                });
            }, 5000);
        }
    };
    announcement();


    result_filter();


    $(".video-link").each(function (i, item) {
        $(item).click(function () {
            $(item).toggleClass("on");
        });
    });
}

var result_filter = function () {
    if ($(".level1:not(:has(ul))")) {
        $(".level1:not(:has(ul))").find('.level1-item').find('div').css('display', 'none');
    }
    if ($(".level3:not(:has(ul))")) {
        $(".level3:not(:has(ul))").find('span.icon').css('display', 'none');
    }
    if ($(".level2:not(:has(ul))")) {
        $(".level2:not(:has(ul))").find('span.icon').css('display', 'none');
    }
    //$(".result-filter .level1-item .icon").each(function (i, item) {
    //    $(item).click(function () {
    //        $(item).parent().parent().toggleClass("on");
    //        if ($(item).hasClass("icon-arrow-down-grey")) {
    //            $(item).removeClass("icon-arrow-down-grey").addClass("icon-arrow-up-grey");
    //        } else {
    //            $(item).removeClass("icon-arrow-down-grey").addClass("icon-arrow-down-grey");
    //        }
    //    });
    //});
    $(".accordion .accordion-item .accordion-title").each(function (i, item) {
        $(item).click(function () {
            $(item).parent().toggleClass("open");
            $(item).find(".icon").toggleClass("icon-minus-grey");
            $(item).find(".icon").toggleClass("icon-plus-grey");

        });
    });
    //$(".result-filter .level2 .icon,.result-filter .level3 .icon").each(function (i, item) {
    //    $(item).click(function () {
    //        $(item).closest('li').toggleClass("on");
    //        if ($(item).hasClass("icon-plus-grey")) {
    //            $(item).removeClass("icon-plus-grey").addClass("icon-minus-grey");
    //        } else {
    //            $(item).removeClass("icon-minus-grey").addClass("icon-plus-grey");
    //        }
    //    });
    //});
};



/* Functionality for Search Result */

/* Assigning start value to 0 */
if ($("#HiddenSearchItemID").val() == "{8D1B174B-0A7A-45E7-9535-72C474E73D3C}") {
    //$("#searchTxtBx").keyup(function () {
    //     var autoSearchTerm = $("#searchTxtBx").val();
    //     $.ajax({
    //         dataType: 'json',
    //         url: 'http://solrdev.anrgw.com:8080/api/v1/suggest?',
    //         async: false,
    //         data: { queryText: autoSearchTerm, count: 10 },
    //         success: function (data) {
    //             suggestionData = data;
    //         }
    //     });
    //     $("#searchTxtBx").AutoComplete({
    //         'data': suggestionData.Terms,
    //         'width': 280
    //     });
    // });
    $("#searchTxtBx").keypress(function (e) {
        if (e.which == 13) {
            $(".searchResBtn").click();
        }
    });

    /* Functionality for Search Button Click */
    var uuid = guid();
    var queryuidVal;
    /* Assigning count value to 20 */
    var countVal = 20;

    var JSONData = null;
    var newData = null;
    var totalRecords;
    var regionVal = $('#HiddenRegion').val();
    //var regionVal= "Americas";
    hostName = location.host;
    var searchHostName;
    switch (hostName) {
        case "dev-www.anritsu.com":
            // console.log("dev");
            hostName = 'http://solrdev.anrgw.com:8080/api/v1/';
            searchHostName = 'http://dev-www.anritsu.com/api/SearchFilters/GetFilters';
            break;
        case "qa-www.anritsu.com":
            // console.log("qa");
            hostName = 'http://solrsbx.anrgw.com:8080/api/v1';
            searchHostName = 'http://qa-www.anritsu.com/api/SearchFilters/GetFilters';
            break;
        case "www.anritsu.com":
            // console.log("prod");
            hostName = 'http://solr.anrgw.com:8080/api/v1';
            searchHostName = 'http://www.anritsu.com/api/SearchFilters/GetFilters';
            break;
        default:
            // console.log("default");
            hostName = 'http://solr.anrgw.com:8080/api/v1';
            searchHostName = 'http://www.anritsu.com/api/SearchFilters/GetFilters';
    }
    var basicUrl = hostName + "/query/";
/*     $('.hd-right input').keyup(function () {
        if (!this.value) {
            $(".search-result-body .container").addClass("dispNone");
            $(".search-empty,div#spellContent").addClass("dispNone");
            $(".search-result-body").css('min-height', '200px');
        }
    }); */
    $(".searchResBtn").on('click', function (e) {

        /* Getting url from host name*/

        $('.searchLevel li').find('input[type=checkbox]:checked').removeAttr('checked');
        /* Appending 'start' and 'query text' parameters to url */
        startVal = 0;
        var searchval = $('.hd-right input').val();
        if (!searchval.trim()) { return false; }
        else {
            getSearchData(startVal).then(function (resp) {
                JSONData = resp;
                //console.log(resp+"resp");
                /* Getting total number of records */
                getTotalRecords(JSONData);
                resultsCnt = totalRecords;
                spellchkVal = JSONData.SpellChecking;
                var startupData = JSONData.Documents.slice(0, 20)
                documentsCnt = JSONData.Documents;
                /* Suggestions for search text */
                if (spellchkVal == null && documentsCnt.length == "") {
                    $(".search-empty").removeClass("dispNone");
                    $(".search-result-body .container,div#spellContent").addClass("dispNone");
                    $(".search-result-body").css('min-height', '0');
                }
                    /* Spell Checking of the search text */
                else if (JSONData.SpellChecking != null) {
                    $("div#spellContent").removeClass('dispNone');
                    $(".search-result-body .container,.search-empty").addClass("dispNone");
                    $(".search-result-body").css('min-height', '0');
                }
                    /* If the entered search text is valid */
                else {
                    var startupData = JSONData.Documents.slice(0, 20)
                    loadContent(startupData);
                    $(".search-result-body .container").removeClass("dispNone");
                    $(".search-empty,div#spellContent").addClass("dispNone");
                }
                displayLevels(JSONData);
                pagination(JSONData);
                filterCnt(JSONData);
                /* List of suggestions we get on entering wrong spelling */
                if (spellchkVal != null) {
                    $('span.meanTxt').removeClass("dispNone");
                    $("#spellContent").css("margin", "50px 0");
                    $.each(JSONData.SpellChecking, function (key, val) {
                        $('.spellContainer').empty();
                        for (var i = 0; i < val.Suggestions.length; i++) {
                            var itemSpellValues = val.Suggestions[i];
                            var spellContentDiv = '<span class="spellFont">' + '<a  class="spellLink">' + itemSpellValues + '</a>' + '</span>';
                            $('<span class="spellContainer">' + spellContentDiv + '</span>').appendTo("div#spellContent");
                        }
                        $("div#spellContent").removeClass('dispNone');
                    });
                }
                /* load content on clicking the suggestion text */
                $(".spellLink").on('click', function () {
                    spellSearchval = $(this).text();
                    $(".hd-right input#searchTxtBx").val(spellSearchval);
                    location.hash = "?&q=" + spellSearchval + "&" + "start=" + '0';
                    startVal = document.URL.split('start=')[1];
                    //getSearchData(startVal);
                    getSearchData(startVal).then(function (resp) {
                        JSONData = resp;
                        getTotalRecords(JSONData);
                        resultsCnt = totalRecords;
                        displayLevels(JSONData);
                        pagination(JSONData);
                        filterCnt(JSONData);
                        $('.search-result-body .container').removeClass('dispNone');
                        $("div#spellContent,.search-empty").addClass("dispNone");
                    });
                });
                /* Suggestions for search text */
                // if (spellchkVal == null && documentsCnt.length == "") {
                //     $(".search-result-body").addClass("dispNone");
                //     $(".search-empty").removeClass("dispNone");
                // }
            });
        }
    });
    /* Subtracting General area count from Numfound */
    function getTotalRecords(data) {
        $.each(data.FacetPivots, function (key, val) {
            for (var i = 0; i < val.length; i++) {
                var areaValues = val[i].Value;
                var areaFields = val[i].Field;
                if (areaFields == 'area' && areaValues == 'General') {
                    totalRecords = JSONData.NumFound - val[i].Count;
                }
                else {
                    totalRecords = JSONData.NumFound;
                }
            }
        });
    }

    function getSearchData(startVal) {
        $('.blrMask').remove();
        $('body').append('<div class="blrMask"></div>');
        var promise = $.Deferred();
        $(".loading").show();
        if (startVal == undefined) {
            startVal = 0;
        }
        var searchval = $('.hd-right input').val();
        location.hash = "?&q=" + searchval + "&" + "start=" + startVal;
        searchValUrl = document.URL.split('&')[1].split('=')[1];
        searchValUrl = decodeURIComponent(searchValUrl);
        var inptCheckLength = $("input:checked").length;
        if (inptCheckLength == 0) {
            /* Ajax Call for search text*/
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: hostName + "/search",
                async: true,
                data: JSON.stringify({
                     QueryText: "\'"+ searchValUrl+"\'" + " AND region:" + regionVal,				 
                    ParentQueryId: uuid
                })
            }).pipe(function (data) {
                queryuidVal = data.QueryUid;
                return queryuidVal;
            }).then(function (queryuidVal) {
                finalRespone(queryuidVal);
                promise.resolve(JSONData);
                //console.log(promise+"promise");
                return promise;
            });
            //console.log(JSONData);
            return promise;
        }/* Ajax Call for filters selected*/
        else { filtersearchCnt(); }
    }
    /* sort the filters selected*/
    function SortJSONData(JSONData) {
        var SortOrderArray = [];
        $.ajax({
            url: searchHostName,
            async: false,
            success: function (data) {
                //console.log(data);
                SortOrderArray = data;
            }
        });
        //console.log(SortOrderArray);
        var ourData = [];
        var SortIndex = [];
        $.each(JSONData.FacetPivots, function (key, val) {
            for (var i = 0; i < val.length; i++) {
                ourData[i] = val[i].Value;
            }
        });

        var cnt = 0;
        var temp = 0;

        for (var i = 0; i < ourData.length; i++) {
            SortIndex[i] = i;
        }
        for (var i = 0; i < SortOrderArray.length; i++) {
            for (var j = 0; j < ourData.length; j++) {
                if (SortOrderArray[i] == ourData[j]) {
                    temp = ourData[cnt];
                    ourData[cnt] = ourData[j];
                    ourData[j] = temp;

                    temp = SortIndex[cnt];
                    SortIndex[cnt] = SortIndex[j];
                    SortIndex[j] = temp;

                    $.each(JSONData.FacetPivots, function (key, val) {
                        temp = val[cnt];
                        val[cnt] = val[j];
                        val[j] = temp;
                    });

                    cnt++;
                }
            }
        }
        return JSONData;
    }
    /*formation of ajax url */
    function finalRespone(queryuidVal) {
        startVal = document.URL.split('start=')[1];
        var resultsUrl = "/results?start=" + startVal + "&count=" + countVal;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: basicUrl + queryuidVal + resultsUrl,
            async: false,
            success: function (data) {
                $(".loading").hide();
                JSONData = data;
                //console.log(JSONData.NumFound+"finaldata")
            },
            error: function (data) {
                $(".loading").hide();
            }
        }).then(function () {
            JSONData = SortJSONData(JSONData);
            var startupData = JSONData.Documents.slice(0, 20);
            loadContent(startupData);
        });
    }

    /* loadContent Starts */
    function loadContent(data) {
        getTotalRecords(JSONData);
        resultsCnt = totalRecords;
        start_from = document.URL.split('start=')[1];
        start_from_Count = parseInt(start_from) + 1;
        endVal = JSONData.Documents.length;
        end_on = parseInt(start_from) + parseInt(endVal);
        if (resultsCnt == 1) {
            $(".resultsCount").removeClass('noReslts');
            $(".resultsCount").text(resultsCnt + " result");
            $('.next_link').addClass('disabled');
        }
        else if (resultsCnt == 0) {
            $(".resultsCount").text("No results found for this search criteria");
            $(".resultsCount").addClass('noReslts');
            $('.next_link').addClass('disabled');
        }
        else {
            $(".resultsCount").removeClass('noReslts');
            $(".resultsCount").text(start_from_Count + " - " + parseInt(end_on) + " / " + resultsCnt + " results");
        }
        $('div#itemContainer').empty();
        /* Appending content to HTML */
        $.each(data, function (key, val) {
            /* Limit the character count of description to 160 */
            description = limitCharCount(val.Description);
            var contentDiv = '<div class="item">' +
                "<h2>" + '<a href="' + val.Url + '">' + val.Title + "</a>" + "</h2>" +
                "<p>" + description + "</p>" +
                '</div>';
            $(contentDiv).appendTo("div#itemContainer");
        });
    }
    /*loadContent Ends */
    /* limitCharCount Starts */
    function limitCharCount(data) {
        var showChar = 160;
        var dots = "...";
        var moretext = "read more";
        var lesstext = "less";
        var content = data;
        if (content != null) {
            if (content.length > showChar) {
                var visibleText = content.substr(0, showChar);
                var hiddenText = content.substr(showChar - 0, content.length - showChar);
                for (var i = showChar; i < content.length; i++) {
                    var j = i - showChar;
                    if (content.substr(i, 1) !== " ") {
                        visibleText += content.substr(i, 1);
                        hiddenText = hiddenText.substr(j + 1, content.length - showChar);
                    } else {
                        hiddenText = " " + hiddenText;
                        break;
                    }
                }
                return visibleText + '<span>' + dots + ' </span>';
            } else {
                return content;
            }
        } else {
            return null;
        }
    }

    /* Display Levels Starts */
    function displayLevels(data) {
        $('.result-filter .hd').removeClass("dispNone");
        $('.result-filter .searchLevel').removeClass("dispNone");
        $('.result-filter .clearall').removeClass("dispNone");
        $('.searchLevel').empty();
        $.each(data.FacetPivots, function (key, val) {
            for (var i = 0; i < val.length; i++) {
                var itemSetValues = val[i].Value;
                var itemSetFields = val[i].Field;
                var itemSetChild = val[i].ChildPivots;
                var level2Content = '';
                var itemResultCount = val[i].Count;
                if (itemSetFields == 'area' && itemSetValues != 'General') {

                    /* Appending level1 to searchLevel */
                    var level1Content = '<li class="level1">' + '<div class="level1-item">' + '<input type="checkbox" class="areaFilter">' + '<a  class="filterLinks">' + '<span class="level1hd">' + itemSetValues + ' (' + itemResultCount + ') ' + '</span>' + '<span class="dispNone">' + itemSetFields + '</span>' + '</a>' + '<div class="icon icon-arrow-down-grey"></div>' + '</div>' + '</li>'
                    $(level1Content).appendTo("ul.searchLevel");
                }
                for (j = 0; j < itemSetChild.length; j++) {
                    if (itemSetChild[j].Field == "category") {
                        var itemResultCount = itemSetChild[j].Count;
                        var iconsymbol = '<span class="icon icon-plus-grey iconStyles"></span>'
                        level2Content += '<li class="level2">' + '<input type="checkbox">' + '<a  class="filterLinks">' + '<span class="level2hd">' + itemSetChild[j].Value + ' (' + itemResultCount + ') ' + '</span>' + '<span class="dispNone">' + itemSetChild[j].Field + '</span>' + '</a>' + '<span class="icon icon-plus-grey iconStyles"></span>' +
                            '</li>';
                    }
                }
                if (level2Content) {
                    /* Appending level2 to searchLevel */
                    var level2SubContent = '<ul class="level2_ul">' + level2Content + '</ul>';
                    $('.searchLevel').find('li.level1').eq(i).append(level2SubContent);
                }
            }
            for (var i = 0; i < val.length; i++) {
                var itemSetValues = val[i].Value;
                var itemSetFields = val[i].Field;
                var itemSetChild = val[i].ChildPivots;
                for (j = 0; j < itemSetChild.length; j++) {
                    var itemSetSubChild = itemSetChild[j].ChildPivots;
                    var level3Content = '';
                    for (k = 0; k < itemSetSubChild.length; k++) {
                        if (itemSetSubChild[k].Field == "subcategory") {
                            var itemResultCount = itemSetSubChild[k].Count;
                            level3Content += '<li class="level3">' + '<input type="checkbox">' + '<a  class="filterLinks">' + '<span class="level3hd">' + itemSetSubChild[k].Value + ' (' + itemResultCount + ') ' + '</span>' + '<span class="dispNone">' + itemSetSubChild[k].Field + '</span>' + '</a>' + '<span class="icon icon-plus-grey iconStyles"></span>' +
                                '</li>';
                        }
                    }
                    if (level3Content) {
                        /* Appending level3 to searchLevel */
                        var level3SubContent = '<ul class="level3_ul">' + level3Content + '</ul>';
                        $('.searchLevel').find('.level1').eq(i).find('.level2_ul').find('.level2').eq(j).append(level3SubContent);
                    }
                }
            }
        });
        result_filter();
    }
    /* Display Levels Ends */
    /* Pagination Starts*/
    function pagination(data) {
        getTotalRecords(JSONData);
        resultsCnt = totalRecords;

        /* number of items to show per page */
        var show_per_page = 20,

            /* getting the total number of amount of elements */
            number_of_items = data.NumFound;

        /* calculate the number of pages we are going to have */
        number_of_pages = Math.ceil(number_of_items / show_per_page),

            max_no_of_pagination_items = 10,

        /* If the number of pages are less than 10 */
            number_of_pagination_items = number_of_pages < max_no_of_pagination_items ? number_of_pages : max_no_of_pagination_items,

            moveAfterPage = 5,

            navigation_html = "",

            disableMove = false,

            current_link = 0;
        if (window.matchMedia('(max-width: 362px)').matches) {
            PageLinkWidth = 30;
        }
        else {
            PageLinkWidth = 32;
        }

        if (number_of_pages <= max_no_of_pagination_items) {

            disableMove = true;
        }

        navigation_html = '<a href="#" class="previous_link disabled">&lt;</a><span class="pagination-wrap"><span class="pagination-inner">';

        while (number_of_pages > current_link) {
            navigation_html += '<a class="page_link"  href="#" longdesc="' + current_link + '">' + (current_link + 1) + '</a>';
            current_link++;
        }
        navigation_html += '</span></span><a class="next_link disabled" href="#">&gt;</a>';

        /* Binding HTML to page navigation div*/
        $('#page_navigation').html(navigation_html);

        /* Setting the width of pagination-wrap */
        $('#page_navigation .pagination-wrap').width(number_of_pagination_items * PageLinkWidth);

        /* Setting the width of pagination-inner */
        $('.pagination-inner').width(number_of_pages * PageLinkWidth + 50);
        if (number_of_pages > 10) {
            $('.next_link').removeClass('disabled');

        }

        /* Binding click events to the pagination buttons */
        /* go to previous page */
        $('.previous_link').on('click', function (e) {

            e.preventDefault();
            previous();
        });

        /* go to next page */
        $('.next_link').on('click', function (e) {

            e.preventDefault();
            next();
        });

        /* go to specific page */
        $('.page_link').on('click', function (e) {

            e.preventDefault();
            go_to_page($(this).attr('longdesc'), true);

        });

        /* value initializations */
        /*setting values to hidden input fields */
        $('#current_page').val(0);
        $('#show_per_page').val(show_per_page);

        /* setting first page as active */
        $('#page_navigation .page_link:first').addClass('active_page');

        function previous() {

            new_page = parseInt($('#current_page').val()) - 1;

            /* if there is an item before the current active link */
            if ($('.active_page').prev('.page_link').length == true) {

                $('.previous_link').removeClass('disabled');
                go_to_page(new_page, false);
            } else {
                $('.previous_link').addClass('disabled');
            }

            /* Changing the left of active page */
            if ($('.active_page').index() >= 4 && $('.active_page').index() < $('.page_link').length - 6) {
                if (parseInt($('.pagination-inner').css('left')) > 0) {
                    $('.pagination-inner').css('left', '0');
                } else {
                    $('.pagination-inner').css('left', parseInt($('.pagination-inner').css('left')) + PageLinkWidth + 'px');
                }
            }
        }

        function next() {

            new_page = parseInt($('#current_page').val()) + 1;

            /* if there is an item after the current active link run the function */
            if ($('.active_page').next('.page_link').length == true) {
                $('.next_link').removeClass('disabled');
                go_to_page(new_page, false);
            }
                /*if($('.page_link').index($('.page_link').length - 1).is('.active_page')){
                  $('.next_link').addClass('disabled');
                }*/
            else {
                $('.next_link').addClass('disabled');
            }

            /* Changing the left of active page */
            if ($('.active_page').index() > 4 && $('.active_page').index() < $('.page_link').length - 5) {
                $('.pagination-inner').css('left', parseInt($('.pagination-inner').css('left')) - PageLinkWidth + 'px');
            }
        }

        function go_to_page(page_num, move) {

            /* Calculating 'start' parameter Value based on page number */
            startValCount = page_num * 20;

            searchValPage = document.URL.split('&')[1].split('=')[1];
			searchValPage =  decodeURIComponent(searchValPage);
            /* Appending updated 'start' and 'query text' parameters to url */
            location.hash = "?&q=" + searchValPage + "&" + "start=" + startValCount;

            startValCount = document.URL.split('start=')[1];

            /* Getting updated search results data */
            getSearchData(startValCount);

            /* get the number of items shown per page */
            var show_per_page = parseInt($('#show_per_page').val());

            /* get the element number from where to start the count */
            start_from = page_num * show_per_page;
            /* get the element number where to end the count */

            start_from_Count = start_from + 1;
            /* Storing updated search results in newData and passing it to loadContent */
            // var newData = searchData.Documents;
            //loadContent(newData);
            var endVal = JSONData.Documents.length;
            end_on = start_from + endVal;

          //  $(".resultsCount").text(start_from_Count + " - " + end_on + " / " + resultsCnt + " results");

            /* Since this always points to the page link, use that instead of looking for it */
            $('.page_link[longdesc=' + page_num + ']').addClass("active_page").siblings(".active_page").removeClass("active_page");

            /* Update the current page input field */
            $('#current_page').val(page_num);

            /* Changing the left of active page */
            if (move) {
                if ($('.active_page').index() > 6 && $('.active_page').index() < $('.page_link').length - 6) {
                    $('.pagination-inner').css('left', -(parseInt($('.active_page').index() - 4) * PageLinkWidth) + 'px');
                } else if ($('.active_page').index() <= 6) {
                    $('.pagination-inner').css('left', '0');
                } else {
                    $('.pagination-inner').css('left', -($('.page_link').length - max_no_of_pagination_items) * PageLinkWidth + 'px');
                }
            }

            if ($('.active_page').index() == 0) {
                $('.previous_link').addClass('disabled');
            } else {
                $('.previous_link').removeClass('disabled');
            }

            if ($('.active_page').index() == $('.page_link').length - 1) {

                $('.next_link').addClass('disabled');
            }
            else {
                $('.next_link').removeClass('disabled');
            }
            if (number_of_pages < 10) {
                $('.pagination-inner').css('left', '0');
                if ($('#page_navigation .page_link:first').hasClass('active_page')) {
                    $('.next_link').addClass('disabled');
                }
            }
        }
    }
    /* pagination Ends */

    /*start of filter content */
    function filterCnt(data) {
        dataObject = {};
        $(".level1-item > .areaFilter").on('click', function (e) {
            var thisCheck = $(this);
            var directParent = thisCheck.parent();
            var filterAreaTxt = thisCheck.siblings('.filterLinks').find('span:first', this).text();

            var filterAreaFeild = thisCheck.siblings('.filterLinks').find('span:last', this).text();
            filterAreaTxt = filterAreaTxt.split(" (")[0];
            /*if (filterAreaTxt.contains("(")) {
                filterAreaTxt = filterAreaTxt.split(" (")[0];
                //console.log(typeof filterAreaTxt);
            }*/

            /* If this checkbox is checked */
            if (thisCheck.is(':checked')) {
                /* all the child input checkboxes are checked */
                thisCheck.parentsUntil('.level1 .on').children('.level2_ul').find('input[type=checkbox]').prop('checked', true);

                /* input checkboxes other than this checkbox are unchecked */
                thisCheck.parentsUntil('.level1 .on').siblings('.level1').find('input[type=checkbox]').prop('checked', false);

                /* If checkbox is checked then add categories and subcategories in the dataObject */
                dataObject = {};
                dataObject[filterAreaTxt] = {};
                var catLength = directParent.siblings().children().length;
                for (var i = 0; i < catLength; i++) {
                    var filterCatTxt = directParent.siblings().children().eq(i).find('.level2hd').text();
                    filterCatTxt = filterCatTxt.split(" (")[0];
                    var subcatLength = directParent.siblings('.level2_ul').children('.level2').eq(i).children('.level3_ul').children().length;
                    addCatToObject(filterAreaTxt, filterCatTxt);
                    for (var j = 0; j < subcatLength; j++) {
                        var filterSubcatTxt = directParent.siblings('.level2_ul').children('.level2').eq(i).children('.level3_ul').children().eq(j).find('.level3hd').text();
                        filterSubcatTxt = filterSubcatTxt.split(" (")[0];
                        // filterSubcatTxt = filterSubcatTxt.split(" (")[0];
                        addSubcatToObject(filterAreaTxt, filterCatTxt, filterSubcatTxt);
                    }
                }
                var area = "";
                for (i in dataObject) {
                    area = ' (area: ' + '\"' + i + '\"' + ')';
                }
                searchText = "\'"+decodeURIComponent($("#searchTxtBx").val())+"\'" + " AND" + area;
                console.log(searchText);
                getStartValue();
                filtersearchCnt().then(function (resp) {
                    JSONData = resp;
                    pagination(JSONData);
                });
            }
            else {
                /* child input checkboxes are unchecked */
                thisCheck.parentsUntil('.level1 .on').children('.level2_ul').find('input[type=checkbox]').prop('checked', false);
                dataObject = {};
                getSearchData(0).then(function (resp) {
                    pagination(JSONData);
                });
            }
        });
        $(".level2>input").on('click', function (e) {
            var thisCheck = $(this);
            var directParent = thisCheck.parent();
            var level1Parent = thisCheck.closest('li.level1.on');
            var filterAreaTxt = level1Parent.find('span').html().split(" (")[0];
            var filterCatTxt = thisCheck.parent('li.level2').find('span.level2hd').text().split(" (")[0];

            /* If this checkbox is not checked */
            if (thisCheck.prop('checked') == false) {
                /* child input checkboxes are to be unchecked */
                directParent.children('.level3_ul').find('input').prop('checked', false);

                /* Deleting category */
                var delCat = dataObject[filterAreaTxt];
                for (g in delCat) {
                    if (g == filterCatTxt) {
                        delete delCat[g];
                    }
                }

                /* if all the same level elements are unchecked, uncheck the parent */
                if ((thisCheck.parent('.level2').siblings().children('input').is(':checked')) == false) {
                    level1Parent.children('.level1-item').children('.areaFilter').prop('checked', false);
                    dataObject = {};
                }

                /*  If child is unchecked and if there are no siblings then uncheck the parent */
                if (thisCheck.parent('.level2').siblings().size() == 0) {
                    level1Parent.children('.level1-item').children('.areaFilter').prop('checked', false);
                    dataObject = {};
                }
                getData();
            }
            else {
                /* If this checkbox is checked */
                if (thisCheck.is(':checked')) {
                    /* parent input checkbox is checked */
                    directParent.parent().siblings('.level1-item').find('.areaFilter').prop('checked', true);

                    /* child input checkboxes are to be checked */
                    directParent.children('.level3_ul').find('input').prop('checked', true);

                    /* other area checkboxes are to be unchecked */
                    level1Parent.siblings().find('input').prop('checked', false);
                    if (filterAreaTxt in dataObject) {
                        addCatToObject(filterAreaTxt, filterCatTxt);
                    }
                    else {
                        dataObject = {};
                        dataObject[filterAreaTxt] = {};
                        addCatToObject(filterAreaTxt, filterCatTxt);
                    }
                    var subcatLength = directParent.children('.level3_ul').children('.level3').length;
                    for (var k = 0; k < subcatLength; k++) {
                        var filterSubcatTxt = directParent.children('.level3_ul').children('.level3').find('.level3hd').eq(k).text();
                        filterSubcatTxt = filterSubcatTxt.split(" (")[0];
                        addSubcatToObject(filterAreaTxt, filterCatTxt, filterSubcatTxt);
                    }

                }
                getStartValue();
                getUrl();
            }

        });
        $(".level3>input").on('click', function (e) {
            var thisCheck = $(this);
            var directParent = thisCheck.parent();
            var level1Parent = thisCheck.closest('li.level2.on');
            var level2Parent = thisCheck.closest('li.level1.on');
            var filterSubcatTxt = thisCheck.next().find('span:first').text().split(" (")[0];

            var filterCatTxt = level1Parent.find('a span:first').html().split(" (")[0];

            var filterAreaTxt = level2Parent.find('.level1-item a span:first').text().split(" (")[0];

            /* If this checkbox is not checked */
            if (thisCheck.prop('checked') == false) {
                /* Deleting subcategory */
                var cat = dataObject[filterAreaTxt][filterCatTxt];
                for (h in cat) {
                    if (h == filterSubcatTxt) {
                        delete cat[h];
                    }
                }

                /* if all the same level elements are unchecked, uncheck the parent */
                if ((directParent.siblings().children('input').is(':checked')) == false) {
                    level1Parent.children('input').prop('checked', false);
                    /* Deleting category */
                    var cat = dataObject[filterAreaTxt];
                    for (i in cat) {
                        if (i == filterCatTxt) {
                            delete cat[i];
                        }
                    }
                    /* if all the parent level elements are unchecked, uncheck the parent */
                    if ((level1Parent.siblings().children('input').is(':checked')) == false) {
                        level2Parent.children('.level1-item').children('.areaFilter').prop('checked', false);
                        dataObject = {};
                    }
                }

                /* If there are no siblings then uncheck the parent, if child is unchecked */
                if (thisCheck.parent('.level3').siblings().size() == 0) {
                    thisCheck.parentsUntil('.level1 .on').siblings('input').prop('checked', false);

                    /* Deleting category */
                    var cat = dataObject[filterAreaTxt];
                    for (i in cat) {
                        if (i == filterCatTxt) {
                            delete cat[i];
                        }
                    }
                    /* if all the parent level elements are unchecked, uncheck the parent */
                    if ((level1Parent.siblings().children('input').is(':checked')) == false) {
                        level2Parent.children('.level1-item').children('.areaFilter').prop('checked', false);
                        dataObject = {};
                    }
                    if (level1Parent.siblings().size() == 0) {
                        dataObject = {};
                    }

                }
                getData();
            }
            else {
                /* If this checkbox is checked */
                if (thisCheck.is(':checked')) {
                    /* parent input checkbox is checked */
                    directParent.parent().siblings('input').prop('checked', true);
                    /* parent-parent input checkbox is checked */
                    level2Parent.find('.level1-item .areaFilter').prop('checked', true);
                    /* uncheck parent-parent siblings input checkbox are to be unchecked */
                    level2Parent.siblings('.level1').find('input').prop('checked', false);

                    if (JSON.stringify(dataObject) === '{}') {
                        addAreaToObject(filterAreaTxt, filterCatTxt, filterSubcatTxt);
                        getStartValue();
                        getUrl();
                    }
                    else {
                        if (filterAreaTxt in dataObject) {
                            if (filterCatTxt in dataObject[filterAreaTxt]) {
                                if (!(filterSubcatTxt in dataObject[filterAreaTxt][filterCatTxt])) {
                                    addSubcatToObject(filterAreaTxt, filterCatTxt, filterSubcatTxt);
                                }
                                getStartValue();
                                getUrl();
                            } else {
                                addCatToObject(filterAreaTxt, filterCatTxt);
                                addSubcatToObject(filterAreaTxt, filterCatTxt, filterSubcatTxt);
                                getStartValue();
                                getUrl();
                            }

                        }
                        else {
                            dataObject = {};
                            addAreaToObject(filterAreaTxt, filterCatTxt, filterSubcatTxt);
                            getStartValue();
                            getUrl();
                        }
                    }
                }
            }
        });
        function addAreaToObject(filterAreaTxt, filterCatTxt, filterSubcatTxt) {
            dataObject[filterAreaTxt] = {};
            dataObject[filterAreaTxt][filterCatTxt] = {};
            dataObject[filterAreaTxt][filterCatTxt].checkedValue = true;
            dataObject[filterAreaTxt][filterCatTxt][filterSubcatTxt] = {};
        }
        function addCatToObject(filterAreaTxt, filterCatTxt) {
            dataObject[filterAreaTxt][filterCatTxt] = {};
            dataObject[filterAreaTxt][filterCatTxt].checkedValue = true;
        }
        function addSubcatToObject(filterAreaTxt, filterCatTxt, filterSubcatTxt) {
            dataObject[filterAreaTxt][filterCatTxt][filterSubcatTxt] = {};
        }
        function getData() {
            /* If dataObject is not null */
            if (!(JSON.stringify(dataObject) === '{}')) {
                getStartValue();
                getUrl();
            }
            else {
                getSearchData(0).then(function (resp) {
                    pagination(JSONData);
                });
            }
        }
        var area;
        window.searchText = "";
        function getStartValue() {
            searchValPage = document.URL.split('&')[1].split('=')[1];
			searchValPage =  decodeURIComponent(searchValPage);
            /* Appending updated 'start' and 'query text' parameters to url */
            location.hash = "?&q=" + searchValPage + "&" + "start=" + 0;
            startVal = document.URL.split('start=')[1];
            endValue = JSONData.Documents.length;
       /*      if (resultsCnt == 1) {
                $(".resultsCount").removeClass('noReslts');
                $(".resultsCount").text(resultsCnt + " result");
                $('.next_link').addClass('disabled');
            }
            else if (resultsCnt == 0) {
                $(".resultsCount").text("No results found for this search criteria");
                $(".resultsCount").addClass('noReslts');
                $('.next_link').addClass('disabled');
            }
            else {
                $(".resultsCount").removeClass('noReslts');
                $(".resultsCount").text("1 - " + endValue + " / " + resultsCnt + " results");
            } */
        }
        function getUrl() {
            var area = "";
            var areaCount = 0;
            for (i in dataObject) {
                area = ' (area: ' + '\"' + i + '\"';
                var category = "";
                var catCount = 0;
                for (j in dataObject[i]) {
                    if (dataObject[i][j].checkedValue == true) {
                        if (catCount > 0) {
                            category += ' OR (' + 'category: ' + '\"' + j + '\"';
                        } else {
                            category += ' AND ((' + 'category: ' + '\"' + j + '\"';
                        }
                    }
                    var subcat = "";
                    var subCatCount = 0;
                    for (k in dataObject[i][j]) {

                        if (k != "checkedValue") {
                            if (subCatCount > 1) {
                                subcat += ' OR subcategory: ' + '\"' + k + '\"';
                            } else {
                                subcat += ' AND (subcategory: ' + '\"' + k + '\"';
                            }
                        }
                        subCatCount++;
                    }
                    if (subcat != "") {
                        subcat += ")";
                    }
                    category += subcat;
                    if (dataObject[i][j].checkedValue == true) {
                        category += ')';
                    }
                    catCount++;
                }
                // if(category != "") {
                //     category += ")";
                // }
                area += category;
                areaCount++;
                if (area != "") {
                    area += "))";
                }
                searchText = "\'" + searchValPage + "\'" + " AND" + area;
            }
            area = $("#searchTxtBx").val() + " AND" + area;
            console.log(searchText);

            /* Displaying First 20 Records */

            //var startupData1 = JSONData.Documents.slice(0, 20);
            filtersearchCnt().then(function (resp) {
                //loadContent(startupData);
                JSONData = resp;
                pagination(JSONData);
            });
        }
    }

    function filtersearchCnt() {
        $(".loading").show();
        var promise = $.Deferred();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: hostName + "/search",
            async: true,
            data: JSON.stringify({
                QueryText: searchText + " AND region:" + regionVal,
                ParentQueryId: uuid
            })
        }).pipe(function (data) {
            queryuidVal = data.QueryUid;
            return queryuidVal;
        }).then(function (queryuidVal) {
            finalRespone(queryuidVal);
            promise.resolve(JSONData);
        });
        return promise;
    }

    /* Uncheck all the checked input fields */
    $("#clearAll").on('click', function () {
        $('.searchLevel li').find('input[type=checkbox]:checked').removeAttr('checked');
        getSearchData(0).then(function (resp) {
            pagination(JSONData);
        });
    });





}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}


/* Remove parameters on reload */
$(document).ready(function () {
    serachIconRes();
});
$("#searchbox").keypress(function (e) {
    if (e.which == 13) {
        serachIconRes();
    }
});
function serachIconRes() {
    if ((location.href.indexOf("#")) != -1) {
        if (document.URL.split('&').length > 1) {
            var searchTerm = document.URL.split('&')[1].split('=')[1];
            searchTerm = decodeURIComponent(searchTerm);
            if (searchTerm != null && searchTerm != "") {
                $(".hd-right input#searchTxtBx").val(searchTerm);
                $('.searchResBtn').trigger('click');
            }
        }
    }
}
