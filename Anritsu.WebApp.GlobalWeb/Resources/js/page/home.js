$(document).ready(function(){
  initPage()
})

function initPage(){
  $(".tab-interface").each(function(i,item){
    var tab = $(item).children("ul");
    var tabContent = $(item).children(".section")
    $(tab).find("li a").each(function(index,el){
        $(el).click(function (e) {
            if (!$(item).hasClass("product-downloads")) {
                e.stopPropagation()
                e.preventDefault()
            }
            $(tab).find("li a").removeClass("active")
        $(el).addClass("active")
        tabContent.removeClass("active")
        tabContent.eq(index).addClass("active")
      })
   })
  })

  window.homeSlider = $(".home-slider").slick({
      "dots": true,
      "autoplay": true,
      "autoplaySpeed": 5000,
      "speed": 500
  });


  
}
