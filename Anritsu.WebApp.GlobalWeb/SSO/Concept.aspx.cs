﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SSO
{
    public partial class Concept : System.Web.UI.Page
    {
        private bool IsRedirectFromGw
        {
            get
            {
                var mIsRedirectFromGw = false;
                if (Request.QueryString["frmGW"] != null &&
                    (Request.QueryString["frmGW"].ToLower() == "true" ||
                    Request.QueryString["frmGW"].ToLower() == "false"))
                    mIsRedirectFromGw = Boolean.Parse(Request.QueryString["frmGW"]);
                else
                {
                    if (Request.Form.Get("frmGW") != null &&
                        (Request.Form.Get("frmGW").ToLower() == "true" ||
                        Request.Form.Get("frmGW").ToLower() == "false"))
                        mIsRedirectFromGw = Boolean.Parse(Request.Form.Get("frmGW"));
                }
                return mIsRedirectFromGw;
            }
        }

        private string IekValue
        {
            get
            {
                var mIekValue = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["iek"]))
                    mIekValue = Request.QueryString["iek"];
                if (!string.IsNullOrEmpty(Request.Form.Get("iek")))
                    mIekValue = HttpUtility.UrlDecode(Request.Form.Get("iek"));
                return mIekValue;
            }
        }

        private string TargetUrl
        {
            get
            {
                var mTargetUrl = "~/Main.aspx";
                if (!string.IsNullOrEmpty(Request.QueryString["TargetURL"]))
                    mTargetUrl = Request.QueryString["TargetURL"];
                if (!string.IsNullOrEmpty(Request.Form.Get("TargetURL")))
                    mTargetUrl = HttpUtility.UrlDecode(Request.Form.Get("TargetURL"));
                if (!BasePage.IsWhiteListHost(new Uri((mTargetUrl.StartsWith("http://") || mTargetUrl.StartsWith("https://")) 
                        ? mTargetUrl : "http://" + mTargetUrl).Host, true))
                    mTargetUrl = ConfigUtility.AppSettingGetValue("AnrSso.GWSp.Logout");
                return mTargetUrl;
            }
        }

        private void ProcessRedirect()
        {
            var enc = new Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SecurityUtilities.AnritsuEncrypter("E2BE0601-0AFE-4306-8FE3-FDC60CB3FD6C");

            if (string.IsNullOrEmpty(IekValue) && IsRedirectFromGw)
            {
                #region " coming from anritsu side "
                if (!User.Identity.IsAuthenticated)
                    Response.Redirect(TargetUrl);
                else
                {
                    var usr =
                        HttpContext.Current.Session[Constants.SessionVariables.UserLoggedIn]
                        as Library.SSOSDK.SSOApiClient.GWSsoUserType;


                    if (usr == null)
                    {
                        Response.Redirect(TargetUrl);//not login
                    }
                    else
                    {
                        var mIekValue = enc.EncryptData(string.Format(
                            "{0}|AC|{1}", usr.UserId, usr.SecToken));

                        var remotePostPage = new RemotePost();
                        remotePostPage.Url = TargetUrl;
                        remotePostPage.Add("iek", mIekValue);
                        remotePostPage.Post();
                    }
                }
                #endregion
            }
            else
            {
                #region " most likely, coming from concept side "

                var mDecryptedIek = "";
                try { mDecryptedIek = enc.DecryptData(IekValue); }
                catch { }

                if (string.IsNullOrEmpty(mDecryptedIek))
                    Response.Redirect(TargetUrl);
                else
                {
                    var mSplittedIek = mDecryptedIek.Replace("|AC|", "~").Split('~');

                    int holding;
                    var iekIsInt = int.TryParse(mSplittedIek[0], out holding);

                    if (mSplittedIek.Length != 2 ||
                        !iekIsInt)
                        Response.Redirect(TargetUrl);

                    var mAclUserId = holding;
                    var mSecretKey = mSplittedIek[1];

                    ProcessLogin(mAclUserId, mSecretKey);
                }

                #endregion
            }
        }

        private void ProcessLogin(int iUserId, string sSecretKey)
        {
            var ssoClient = new Library.SSOSDK.AnritsuSsoClient();
            var user = ssoClient.GetUserByGWUserId(iUserId, sSecretKey);

            var mIsValidUser = user != null;

            if (mIsValidUser)
            {
                Session[Constants.SessionVariables.UserLoggedIn] = user;

                var httpCookie = Response.Cookies["ASPNETCommerce_FirstName"];
                if (httpCookie != null)
                    httpCookie.Value =
                        Server.HtmlEncode(user.EmailAddress);
            }

            Response.Redirect(TargetUrl);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ProcessRedirect();
            }
        }
    }
}