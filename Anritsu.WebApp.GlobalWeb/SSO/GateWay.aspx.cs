﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;

namespace Anritsu.WebApp.GlobalWeb.SSO
{
    public partial class GateWay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString[QueryStringVariables.ReturnUrl]) && !IsPostBack)
            {
                var noPost = true;
                var redirectUrl = Request.QueryString[QueryStringVariables.ReturnUrl];

                foreach (string str in BasePage.GetSsoClientListfromCache())
                {
                    if (redirectUrl.IndexOf(str, StringComparison.Ordinal) > 0)
                        noPost = false;
                }
                Trace.Write(String.Format("Redirect to:{0}|NoPost:{1}", redirectUrl, noPost));
                if (!User.Identity.IsAuthenticated || noPost)
                {
                    Trace.Write(String.Format("IsAuthenticated:{0}|NoPost:{1}", User.Identity.IsAuthenticated, noPost));
                    //String url = String.Format("/{0}/Home.aspx", BasePage.GetCurrentLanguage());
                    Response.Redirect(redirectUrl);
                }
                else
                {
                    var remotePostPage = new RemotePost { Url = redirectUrl };
                    remotePostPage.Add("iek", User.Identity.Name);
                    remotePostPage.Post();
                }
            }
            else
            {
                throw new HttpException(401, "You are not authorized to view this page");
            }
        }
    }
}