﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Configuration;

namespace Anritsu.WebApp.GlobalWeb.SSO
{
    public partial class SignOut : System.Web.UI.Page
    {
        public string ReturnUrl
        {
            get
            {
                var returnUrl = Request.QueryString[QSKeys.ReturnURL];
                //var lang = Request.QueryString[QSKeys.SiteLocale];
                //if (string.IsNullOrEmpty(lang))
                //{
                //    lang =Sitecore.Configuration.Settings.GetSetting("DefaultLocale");
                //}
                var defaultUrl = string.Format(ConfigUtility.GetStringProperty("AnrSso.GWSp.DefaultReturnUrl", "/{0}"), BasePage.GetCurrentLanguage());
                return BasePage.GetRedirectUrl(returnUrl, defaultUrl);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (!Request.IsSecureConnection)
            {
                //WebUtility.ForceSecureConnection();
            }
            DoSingleSignOut();

        }

        private void DoSingleSignOut()
        {
            var wreply = Request[QSKeys.Wreply].ConvertNullToEmptyString();
            wreply = string.IsNullOrEmpty(wreply) ? wreply : HttpUtility.UrlDecode(wreply.Contains(",") ?
                wreply.Split(',')[0] : wreply);
            var redirectUrl = !BasePage.IsWhiteListUrl(wreply) ? ReturnUrl : wreply;
            if (Request[QSKeys.Idplgo] == "logout")
            {
                var dotnetSessionCookie = Request.Cookies.Get("ASP.NET_SessionId");
                if (dotnetSessionCookie != null) dotnetSessionCookie.Expires = DateTime.Now.AddMinutes(-1);
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                var anrLoginCookie = Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                if (anrLoginCookie != null)
                    Request.Cookies.Remove(FormsAuthentication.FormsCookieName);

                //remove login state cookie
                var loginStateCookieName = Settings.GetSetting("AnrSso.GWSP.LoginStatus");
                var clonedCookie = new HttpCookie(loginStateCookieName??"lgs")
                {
                    Expires = DateTime.Now.AddDays(-1d),
                    Domain = "anritsu.com"
                };
                HttpContext.Current.Response.Cookies.Add(clonedCookie);
                Response.Redirect(redirectUrl, true);
            }
            else
            {

                FormsAuthentication.SignOut();
                Session.Abandon();
                Session.Clear();

                //intialize the required variables to pass
                var spResourceUrl = WebUtility.GetAbsoluteUrl(ReturnUrl.Trim());
                var idpLogout = ConfigUtility.AppSettingGetValue("AnrSso.GWIdp.Logout");
                var gwSptokenId = ConfigUtility.AppSettingGetValue("AnrSso.GWSp.SpTokenID");
                var localeAndmkt = BasePage.GetCurrentLanguage();
                var sPx509Certificate = GwSsoSamlUtility.GetSpCertificate();

                GwSsoSamlUtility.SamlLogoutRequest_SendHttpPost(spResourceUrl, idpLogout, gwSptokenId, localeAndmkt, sPx509Certificate);
            }
        }
    }
}