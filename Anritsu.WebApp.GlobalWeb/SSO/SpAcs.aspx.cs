﻿using Anritsu.WebApp.GlobalWeb.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using System.Web.Security;
using Atp.Saml;

namespace Anritsu.WebApp.GlobalWeb.SSO
{
    public partial class SpAcs : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Request.IsSecureConnection)
                {
                    WebUtility.ForceSecureConnection();
                }
            }

            if (Request["SAMLResponse"].IsNullOrEmptyString())
            {
                var resourceUrl = GetResourceUrl(String.Empty);
                if (User.Identity.IsAuthenticated && Session[Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SessionVariables.UserLoggedIn] != null)
                {
                    Trace.Write("SAMLResponse is null, already logged in:HandleRedirect " + resourceUrl);
                    GwSsoSamlUtility.HandleRedirect(resourceUrl);
                }
                else
                {
                    Trace.Write("SAMLResponse is null:DoDefaultSignout");
                    GwSsoSamlUtility.DoDefaultSignout(FormsAuthentication.DefaultUrl);
                }
            }
            else
            {
                ProcessSaml();
            }

        }

        private void ProcessSaml()
        {
            // Process the SAML response returned by the identity provider in response
            // to the authentication request sent by the service provider.
            string relayState;
            var artifactUrl = ConfigUtility.AppSettingGetValue("AnrSso.GWSp.ArtifactUrl");
            var idpx509Certificate = GwSsoSamlUtility.GetIdpCertificate();
            var bindingType = Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.Binding");

            var samlResponse = GwSsoSamlUtility.BuildACSResponse(out relayState, artifactUrl, idpx509Certificate, bindingType);

            if (samlResponse != null && samlResponse.IsSuccess())
            {
                Trace.Write("ProcessSaml:InitiateGwSsoUserObject:Starts");
                var rememberme = false;
                var ssoUser = GwSsoSamlUtility.InitiateGwSsoUserObject(samlResponse, out rememberme);
                Trace.Write("ProcessSaml:InitiateGwSsoUserObject:End");

                if (ssoUser == null || ssoUser.SecToken.IsNullOrEmptyString())
                {
                    Trace.Write("ProcessSaml:UserInfoOrTokenIsNull");
                    GwSsoSamlUtility.DoDefaultSignout(FormsAuthentication.DefaultUrl);
                }
                else
                {
                    var resourceUrl = GetResourceUrl(relayState);
                    var cookie = SecurityUtilities.ClientCacheEncrypter.EncryptString(
                    string.Format("{0}{1}{2}", ssoUser.UserId, "|", ssoUser.SecToken));
                    FormsAuthentication.SetAuthCookie(cookie, rememberme);
                    //FormsAuthentication.SetAuthCookie("9A0B66DC-6E21-49D0-A337-FAADC6048ECA", true);
                    GwSsoSamlUtility.HandleRedirect(resourceUrl);
                }
            }
            else
            {
                Trace.Write("ProcessSaml:DoDefaultSignout");
                GwSsoSamlUtility.DoDefaultSignout(FormsAuthentication.DefaultUrl);
            }
        }

        private String GetResourceUrl(String relayState)
        {
            #region " originally requested resource URL "
            // Get the originally requested resource URL from the relay state.
            var resourceUrl = relayState != null ? SamlSettings.CacheProvider.Remove(relayState) as string : string.Empty;
            resourceUrl = String.IsNullOrEmpty(resourceUrl)
                    ? Request.QueryString[QSKeys.Wreply]
                    : resourceUrl;

            var defaultUrl = string.Format(Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.DefaultReturnUrl", "/{0}/Home.aspx"), BasePage.GetCurrentLanguage());

            return BasePage.GetRedirectUrl(resourceUrl, defaultUrl);

            #endregion
        }
    }
}