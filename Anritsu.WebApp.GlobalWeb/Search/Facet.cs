﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Search
{
    [Serializable]
    public class Facet : IFacet
    {

        public string DisplayName
        {
            get;
            set;
        }

        public string FieldName
        {
            get;
            set;
        }




        public IEnumerable<FacetValue> FacetValues
        {
            get;
            set;
        }
    }
}
