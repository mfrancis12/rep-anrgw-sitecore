﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Search
{
    [Serializable]
    public class FacetValue : IFacetValue
    {
        public string DisplayName
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }

        public int DocumentCount
        {
            get;
            set;
        }

        public string ParentId
        {
            get;
            set;
        }

        public string SiteArea
        {
            get;
            set;
        }

        public IEnumerable<IFacet> NestedFacets
        {
            get;
            set;
        }
    }
}
