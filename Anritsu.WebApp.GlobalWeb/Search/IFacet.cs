﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.GlobalWeb.Search
{
    public interface IFacet
    {
        string DisplayName { get; }
        string FieldName { get; }
        IEnumerable<FacetValue> FacetValues { get;  }
    }
}
