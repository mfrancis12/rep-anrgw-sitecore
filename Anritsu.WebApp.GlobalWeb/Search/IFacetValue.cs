﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.WebApp.GlobalWeb.Search
{
    public interface IFacetValue
    {
        string DisplayName { get; }
        string Value { get;  }
        int DocumentCount { get; }
        string ParentId { get; }
        string SiteArea { get; }
        IEnumerable<IFacet> NestedFacets { get; }
    }
}
