﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Sitecore.Data.Fields;
using Anritsu.WebApp.GlobalWeb.SiteSearch;

namespace Anritsu.WebApp.GlobalWeb.Search
{
        public class ProductsModelNumber : IComputedIndexField
        {
            public object ComputeFieldValue(IIndexable indexable)
            {
                Item item = indexable as SitecoreIndexableItem;

                if (item == null)
                    return null;

                if (item.TemplateID.ToString() != TemplateIds.ProductRegion)
                    return null;

                using (new Sitecore.Globalization.LanguageSwitcher(item.Language))
                {
                    var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["SelectProduct"]);

                    if (((ReferenceField)(fallbackItem.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var productItem = ((ReferenceField)(fallbackItem.Fields["SelectProduct"])).TargetItem;
                        productItem = SiteSearchHelper.GetSitecoreFallbackItem(productItem, productItem.Fields["ModelNumber"]);

                        return (productItem["ModelNumber"]).Replace("<br>","/");
                    }
                }
                return null;
            }

            public string FieldName
            {
                get;
                set;
            }

            public string ReturnType
            {
                get;
                set;
            }
        }

        public class DiscontinuedProduct : IComputedIndexField
        {
            public object ComputeFieldValue(IIndexable indexable)
            {
                Item item = indexable as SitecoreIndexableItem;

                if (item == null)
                    return null;

                if (item.TemplateID.ToString() != TemplateIds.ProductRegion)
                    return null;

                using (new Sitecore.Globalization.LanguageSwitcher(item.Language))
                {
                    var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["SelectProduct"]);

                    if (((ReferenceField)(fallbackItem.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var productItem = ((ReferenceField)(fallbackItem.Fields["SelectProduct"])).TargetItem;                        
                        productItem = SiteSearchHelper.GetSitecoreFallbackItem(productItem, productItem.Fields["IsDiscontinued"]);

                        return (((CheckboxField)productItem.Fields["IsDiscontinued"])).Checked;
                    }
                }
                return false;
            }

            public string FieldName
            {
                get;
                set;
            }

            public string ReturnType
            {
                get;
                set;
            }
        }
        public class WhatSNewProduct : IComputedIndexField
        {
            public object ComputeFieldValue(IIndexable indexable)
            {
                Item item = indexable as SitecoreIndexableItem;

                if (item == null)
                    return null;

                if (item.TemplateID.ToString() != TemplateIds.ProductRegion)
                    return null;

                using (new Sitecore.Globalization.LanguageSwitcher(item.Language))
                {
                   var productItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["WhatSNew"]);
                   return (((CheckboxField)productItem.Fields["WhatSNew"])).Checked;                    
                }               
            }

            public string FieldName
            {
                get;
                set;
            }

            public string ReturnType
            {
                get;
                set;
            }
        }
    }
