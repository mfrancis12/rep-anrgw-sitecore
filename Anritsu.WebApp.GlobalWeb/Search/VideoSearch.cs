﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.Search
{  
    public class ProductFamily : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (item.TemplateID.ToString() != TemplateIds.Video)
                return null;

            if (string.IsNullOrEmpty(item["Video"]))
                return null;

            return GetProductFamilies(item);
        }

        private IEnumerable<ID> GetProductFamilies(Item videoItem)
        {
            List<Item> regionalProducts = new List<Item>();
            using (new Sitecore.Globalization.LanguageSwitcher(videoItem.Language))
            {
                regionalProducts.AddRange((from link in Sitecore.Globals.LinkDatabase.GetItemReferrers(videoItem, false)
                                           let sourceItem = link.GetSourceItem()
                                           where sourceItem != null
                                           where sourceItem.TemplateID.ToString() == TemplateIds.ProductRegion
                                           select sourceItem).ToList().Distinct(new ItemEqualityComparer()).ToList<Item>());

                var globalProducts = regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null).Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem).ToList();

                List<ID> families = new List<ID>();
                foreach (Item product in globalProducts)
                {
                    families.AddRange(((MultilistField)product.Fields["ProductFamily"]).TargetIDs.ToList());
                }
                // List<string> familyNames = new List<string>();
                //familyNames = family.Select(x => x["PageTitle"]).ToList();
                //family = family.Distinct(new ItemEqualityComparer()).ToList<ID>();
                return families;
            }
        }       

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    public class ProductSubcategory : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (item.TemplateID.ToString() != TemplateIds.Video)
                return null;

            if (string.IsNullOrEmpty(item["Video"]))
                return null;

            return GetSubcategories(item);
        }

        private IEnumerable<ID> GetSubcategories(Item videoItem)
        {
            var regionalProducts = new List<Item>();
            using (new Sitecore.Globalization.LanguageSwitcher(videoItem.Language))
            {
                regionalProducts.AddRange((from link in Sitecore.Globals.LinkDatabase.GetItemReferrers(videoItem, false)
                                           let sourceItem = link.GetSourceItem()
                                           where sourceItem != null
                                           where sourceItem.TemplateID.ToString() == TemplateIds.ProductRegion
                                           select sourceItem).ToList().Distinct(new ItemEqualityComparer()).ToList<Item>());

                var globalProducts = regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null).Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem).ToList();
                var families = new List<Item>();
                foreach (Item product in globalProducts)
                {
                    families.AddRange(((MultilistField)product.Fields["ProductFamily"]).GetItems().Where(x => x != null).Distinct(new ItemEqualityComparer()).ToList<Item>());
                }
                families = families.Distinct(new ItemEqualityComparer()).ToList<Item>();
                var subcategories = families.Select(x => x.ParentID);
                return subcategories;
            }
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    public class ProductCategory : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (item.TemplateID.ToString() != TemplateIds.Video)
                return null;

            if (string.IsNullOrEmpty(item["Video"]))
                return null;

            return GetCategories(item);
        }

        private IEnumerable<ID> GetCategories(Item videoItem)
        {
            List<Item> regionalProducts = new List<Item>();
            using (new Sitecore.Globalization.LanguageSwitcher(videoItem.Language))
            {
                regionalProducts.AddRange((from link in Sitecore.Globals.LinkDatabase.GetItemReferrers(videoItem, false)
                                           let sourceItem = link.GetSourceItem()
                                           where sourceItem != null
                                           where sourceItem.TemplateID.ToString() == TemplateIds.ProductRegion
                                           select sourceItem).ToList().Distinct(new ItemEqualityComparer()).ToList<Item>());

                List<Item> globalProducts = new List<Item>();
                globalProducts = regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null).Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem).ToList();

                List<Item> families = new List<Item>();
                foreach (Item product in globalProducts)
                {
                    families.AddRange(((MultilistField)product.Fields["ProductFamily"]).GetItems().Where(x => x != null).Distinct(new ItemEqualityComparer()).ToList<Item>());
                }
                families = families.Distinct(new ItemEqualityComparer()).ToList<Item>();
                var subcategories = families.Select(x => x.Parent);
                var categories = subcategories.Select(x => x.ParentID);
                return categories;
            }
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }
}