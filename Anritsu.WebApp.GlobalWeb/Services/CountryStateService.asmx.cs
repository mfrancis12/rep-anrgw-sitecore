﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Data;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using WebApi.Models;
using System.Text.RegularExpressions;


namespace Anritsu.WebApp.GlobalWeb.Services
{
    /// <summary>
    /// Summary description for CountryStateService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CountryStateService : System.Web.Services.WebService
    {
        Database db = Sitecore.Context.Database;

        [WebMethod]
        public string GetCountries()
        {
            List<Country> countries = new List<Country>();
            string currentCulture = GetCulture();
            Item countriesFolder = db.GetItem("{CBE79E28-2912-4127-BE67-D469825EA027}", Sitecore.Context.Language);
            countriesFolder.GetChildren().ToList().ForEach(delegate(Item p)
            {
                if (p.Fields["CountryName"] != null && p.Fields["CountryCode"] != null)
                {
                    Country country = new Country
                    {
                        CountryName = System.Convert.ToString(p.Fields["CountryName"]),
                        Region = System.Convert.ToString(p.Fields["CountryCode"]),
                        ContextRegion = (System.Convert.ToString(p.Fields["CountryCode"]) == currentCulture) ? System.Convert.ToString(p.Fields["CountryCode"]) : "",
                    };
                    countries.Add(country);
                }
            });
            return JsonConvert.SerializeObject(countries);
        }

        [WebMethod]
        public string GetStatesByCountry(string countryCode)
        {
            List<State> states = new List<State>();
            Item statesFolder = db.GetItem("{63316294-92EE-440A-8215-0C2FA0448D20}", Sitecore.Context.Language);
            Item countryItem = statesFolder.GetChildren().Where(x => x.Fields["CountryCode"].Value.Equals(countryCode)).FirstOrDefault();
            if (countryItem != null)
            {
                countryItem.GetChildren().ToList().ForEach(delegate(Item p)
                {
                    if (p.Fields["StateName"] != null && p.Fields["StateCode"] != null)
                    {
                        State state = new State
                        {
                            StateName = p.Fields["StateName"].Value,
                            StateCode = p.Fields["StateCode"].Value,
                        };
                        states.Add(state);
                    }
                });
            }
            return JsonConvert.SerializeObject(states);
        }

        [WebMethod]
        public string GetCulture()
        {
            return Sitecore.Context.Culture.Name.Split('-')[1];
        }
    }
}


