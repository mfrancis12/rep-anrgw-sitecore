﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb
{
    public partial class SignIn : BasePage
    {
        public String ReturnUrl
        {
            get
            {
                var returnUrl = Request.QueryString[QSKeys.ReturnURL];
                var defaultUrl =
                    string.Format(Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.DefaultReturnUrl", "/{0}"),
                       GetCurrentLanguage());//"/{0}/Home.aspx"

                return GetRedirectUrl(returnUrl, defaultUrl);
            }
        }


        public bool IsNew
        {
            get
            {
                var isNew = Request.QueryString[QSKeys.IsNew];
                return !isNew.IsNullOrEmptyString() && Convert.ToBoolean(isNew);
            }
        }


        public string SiteLocale
        {
            get
            {
                var siteLocale = Request.QueryString[QSKeys.SiteLocale];
                return !siteLocale.IsNullOrEmptyString() ? siteLocale : string.Empty;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            RequireSSL = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                if (Session[Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SessionVariables.UserLoggedIn] == null)
                {
                    Trace.Write("User session is null.");
                    FormsAuthentication.SignOut();
                    DoSamlLoginRequest();
                }
                else
                {
                    var returnUrl = ReturnUrl;
                    Trace.Write("User logged in, HandleRedirect to " + returnUrl);
                    GwSsoSamlUtility.HandleRedirect(ReturnUrl);
                }
            }
            else
            {
                DoSamlLoginRequest();
            }

        }
        private void DoSamlLoginRequest()
        {
            var spResourceUrl = WebUtility.GetAbsoluteUrl(ReturnUrl.Trim());
            var acsUrl = Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.AcsUrl");
            var idpLoginurl = Sitecore.Configuration.Settings.GetSetting("AnrSso.GWIdp.Login");
            var sPx509Certificate = GwSsoSamlUtility.GetSpCertificate();
            var spTokenId = Sitecore.Configuration.Settings.GetSetting("AnrSso.GWSp.SpTokenID");
            var localeAndmkt = string.IsNullOrEmpty(SiteLocale) ? GetCurrentLanguage() : SiteLocale;
            GwSsoSamlUtility.SamlLoginRequest_SendHttpPost(spResourceUrl, acsUrl, idpLoginurl, localeAndmkt,
               sPx509Certificate, spTokenId, IsNew);
        }
    }
}