﻿using System;
using System.Collections.Generic;
using SearchFacetValue = Anritsu.WebApp.GlobalWeb.Search.FacetValue;

namespace Anritsu.WebApp.GlobalWeb.SiteSearch
{
    public class FacetValueEqualityComparer : IEqualityComparer<SearchFacetValue>
    {
        // Items are equal if their names and Item numbers are equal.
        public bool Equals(SearchFacetValue x, SearchFacetValue y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the Items' ID's are equal.
            return x.Value == y.Value && x.SiteArea == y.SiteArea;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(SearchFacetValue obj)
        {
            // Check whether the object is null. 
            if (Object.ReferenceEquals(obj, null)) return 0;

            // Get the hash code for the Name field if it is not null. 
            int hashSiteArea = obj.SiteArea == null ? 0 : obj.SiteArea.GetHashCode();

            // Get the hash code for the Code field. 
            int hashFacetValue = obj.Value.GetHashCode();

            // Calculate the hash code for the product. 
            return hashSiteArea ^ hashFacetValue;
        }
    }
}