﻿using System;
using System.Linq;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.ContentSearch.Pipelines.GetDependencies;

namespace Anritsu.WebApp.GlobalWeb.SiteSearch
{
    public class GetDataSourceDependencies : BaseProcessor
    {
        public override void Process(GetDependenciesArgs args)
        {
            Func<ItemUri, bool> func = null;
            Assert.IsNotNull(args.IndexedItem, "indexed item");
            Assert.IsNotNull(args.Dependencies, "dependencies");
            Item item = (Item)(args.IndexedItem as SitecoreIndexableItem);
            if (item != null)
            {
                if (func == null)
                {
                    func = uri => (bool)((uri != null) && ((bool)(uri != item.Uri)));
                }
                System.Collections.Generic.IEnumerable<ItemUri> source = Enumerable.Where<ItemUri>(from l in Globals.LinkDatabase.GetReferrers(item, FieldIDs.LayoutField) select l.GetSourceItem().Uri, func).Distinct<ItemUri>();
                args.Dependencies.AddRange(source.Select(x => (SitecoreItemUniqueId)x));
            }
        }
    }
}