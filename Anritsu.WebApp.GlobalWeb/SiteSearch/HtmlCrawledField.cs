﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Sites;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SiteSearch
{
    /// <summary>
    /// Computed field that requests a page and indexes the content.
    /// Because it actually 'http requests' the page, the content of the components on the page are also indexed.
    /// </summary>
    public class HtmlCrawledField : IComputedIndexField
    {
        #region Properties

        private readonly HashSet<string> _textFieldTypes = new HashSet<string>(new[]
        {
            "Single-Line Text", 
            "Rich Text", 
            "Multi-Line Text", 
            "text", 
            "rich text", 
            "html", 
            "memo", 
            "Word Document"
        });

        public string FieldName { get; set; }
        public string ReturnType { get; set; }

        #endregion

        public object ComputeFieldValue(IIndexable indexable)
        {
            Assert.ArgumentNotNull(indexable, "indexable");
            string url = null;
            try
            {
                Item item = indexable as SitecoreIndexableItem;

                if (!item.ShouldIndexItem() && (!item.TemplateID.ToString().Equals(TemplateIds.Downloads) || !item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion)))
                {
                    return null;
                }
                
                var result = new StringBuilder();

                if (item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
                {
                    item.Fields.ReadAll();
                    foreach (var field in item.Fields.Where(ShouldIndexField))
                    {
                        result.AppendLine(field.Value);
                    }
                }
                else if(item.TemplateID.ToString().Equals(TemplateIds.Downloads))
                {
                    var downloadItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Description"]);
                    result.AppendLine(downloadItem["Description"]);
                }
                else
                {
                    // Determine the url to request
                    var urlOptions = UrlOptions.DefaultOptions;
                    urlOptions.AlwaysIncludeServerUrl = true;
                    urlOptions.SiteResolving = true;
                    urlOptions.Language = item.Language;
                    urlOptions.Site = SiteContext.GetSite("globalweb");
                    url = LinkManager.GetItemUrl(item, urlOptions);

                    if (IsUrlAvailable(url))
                    {
                        using (var client = new WebClient())
                        {
                            client.Headers.Add("SitecoreSearch", "SitecoreSiteSearch");
                            client.Encoding = System.Text.Encoding.UTF8;
                            string pageContent = client.DownloadString(url);

                            // Parse the page's html using HtmlAgilityPack
                            HtmlDocument htmlDocument = new HtmlDocument();
                            htmlDocument.LoadHtml(pageContent);

                            htmlDocument.DocumentNode.Descendants().Where(n => n.Name == "script" || n.Name == "style").ToList().ForEach(n => n.Remove());

                            HtmlNode bcnode = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='breadcrumb']");
                            if (bcnode != null)
                            {
                                bcnode.Remove();
                            }

                            HtmlNode videonode = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='full-container video-info nogap']");
                            if (videonode != null)
                            {
                                videonode.Remove();
                            }

                            // Strip out all the html tags, so we can index just the text
                            HtmlNode mainContainer = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='maincontent']");
                            string mainContent = mainContainer != null ? RemoveWhitespace(mainContainer.InnerText.Replace(Environment.NewLine, " ")) : null;

                            if (!string.IsNullOrEmpty(mainContent))
                                result.AppendLine(mainContent);
                        }
                    }
                    else
                    {
                        item.Fields.ReadAll();
                        foreach (var field in item.Fields.Where(ShouldIndexField))
                        {
                            result.AppendLine(field.Value);
                        }
                    }
                }

                return !string.IsNullOrEmpty(result.ToString()) ? result.ToString() : null;
            }
            catch (Exception exc)
            {
                //3chillies suppressing log entry (temp), issue seems to be were so home items are coming through without their language being embedded in the url.
                //which is kind of the desired effect as if it has no language it should have no content and certainly should not find it's way in to the index.
                //Log.Error(string.Format("An error occurred when indexing {0}: {1}", indexable.Id, exc.Message), exc, this);
            }

            return null;
        }

        /// <summary>
        /// Storing whitespace for a field that is only to be used for searching in is not very useful.
        /// This methods removes excessive whitespace.
        /// </summary>
        /// <param name="inputStr"></param>
        /// <returns></returns>
        private static string RemoveWhitespace(string inputStr)
        {
            const int n = 5;
            StringBuilder tmpbuilder = new StringBuilder(inputStr.Length);
            for (int i = 0; i < n; ++i)
            {
                string scopy = inputStr;
                bool inspaces = false;
                tmpbuilder.Length = 0;
                for (int k = 0; k < inputStr.Length; ++k)
                {
                    char c = scopy[k];
                    if (inspaces)
                    {
                        if (c != ' ')
                        {
                            inspaces = false;
                            tmpbuilder.Append(c);
                        }
                    }
                    else if (c == ' ')
                    {
                        inspaces = true;
                        tmpbuilder.Append(' ');
                    }
                    else
                    {
                        tmpbuilder.Append(c);
                    }
                }
            }
            return tmpbuilder.ToString();
        }

        private bool IsUrlAvailable(string url)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Headers.Add("SitecoreSearch", "SitecoreSite");
                req.Method = "HEAD";
                req.Timeout = 15000;
                using (HttpWebResponse rsp = (HttpWebResponse)req.GetResponse())
                {
                    if (rsp.StatusCode == HttpStatusCode.OK)
                    {
                        return true;
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Audit("Search Crawler Url:" + url, GetType());
                Log.Error(ex.Message + " " + ex.StackTrace, GetType());
            }
            return false;
        }

        private bool ShouldIndexField(Field field)
        {
            //process non-empty text fields that are not part of the standard template
            return !field.Name.StartsWith("__") && IsTextField(field) && !string.IsNullOrEmpty(field.Value);
        }

        private bool IsTextField(Field field)
        {
            return _textFieldTypes.Contains(field.Type);
        }
    }
}