﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Web;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Sites;

namespace Anritsu.WebApp.GlobalWeb.SiteSearch
{
    # region Item Name Computed Field

    public class SiteSearchItemName : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (!item.ShouldIndexItem())
                return null;

            return GetItemName(item);
        }

        private string GetItemName(Item item)
        {
            if (item.TemplateID.ToString().Equals(TemplateIds.Downloads))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Title"]);
                return fallbackItem["Title"];
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Question"]);
                return fallbackItem["Question"];
            }
            else
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["MetaTitle"]);

                if (!string.IsNullOrEmpty(fallbackItem["MetaTitle"]))
                {
                    return fallbackItem["MetaTitle"];
                }
                else if (!string.IsNullOrEmpty(fallbackItem["PageTitle"]))
                {
                    return fallbackItem["PageTitle"];
                }
                else if (fallbackItem.Fields["Title"] != null && !string.IsNullOrEmpty(fallbackItem["Title"]))
                {
                    return fallbackItem["Title"];
                }
                else
                {
                    return Context.Culture.TextInfo.ToTitleCase(item.Name);
                }
            }
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gsa")]
        public static string GetGsaItemName(Item item)
        {
            if (item.Name.Trim() == "*")
            {
                return string.Empty;
            }
            if (item.TemplateID.ToString().Equals(TemplateIds.Downloads))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Title"]);
                return fallbackItem["Title"];
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Question"]);
                return fallbackItem["Question"];
            }
            else
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["MetaTitle"]);

                if (!string.IsNullOrEmpty(fallbackItem["MetaTitle"]))
                {
                    return fallbackItem["MetaTitle"];
                }
                else if (!string.IsNullOrEmpty(fallbackItem["PageTitle"]))
                {
                    return fallbackItem["PageTitle"];
                }
                else if (fallbackItem.Fields["Title"] != null && !string.IsNullOrEmpty(fallbackItem["Title"]))
                {
                    return fallbackItem["Title"];
                }
                else
                {
                    return Context.Culture.TextInfo.ToTitleCase(item.Name);
                }
            }
        }
    }

    #endregion

    # region Meta Description Computed Field

    public class SiteSearchMetaDescription : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (!item.ShouldIndexItem())
                return null;

            return GetItemMetaDescription(item);
        }

        private string GetItemMetaDescription(Item item)
        {
            var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Metatags-Description"]);

            if (fallbackItem.Fields["Metatags-Description"] != null && !string.IsNullOrEmpty(fallbackItem["Metatags-Description"]))
            {
                return fallbackItem["Metatags-Description"];
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.Downloads))
            {
                var downloadItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Description"]);
                return downloadItem["Description"];
            }
            return null;
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gsa")]
        public static string GetGsaItemMetaDescription(Item item)
        {
            if (item.Name.Trim() == "*")
            {
                return string.Empty;
            }

            var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Metatags-Description"]);
            if (fallbackItem.Fields["Metatags-Description"] != null && !string.IsNullOrEmpty(fallbackItem["Metatags-Description"]))
            {
                return fallbackItem["Metatags-Description"];
            }

            fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Description"]);
            if (fallbackItem.Fields["Description"] != null && !string.IsNullOrEmpty(fallbackItem["Description"]))
            {
                return fallbackItem["Description"];
            }
            return string.Empty;
        }
    }

    #endregion

    # region MetaKeywords Computed Field

    public class SiteSearchMetaKeywords : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (!item.ShouldIndexItem())
                return null;

            return GetItemMetaKeywords(item);
        }

        private string GetItemMetaKeywords(Item item)
        {
            string content = string.Empty;

            var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Metatags-Other keywords"]);

            if (fallbackItem.Fields["Metatags-Other keywords"] != null && !string.IsNullOrEmpty(fallbackItem["Metatags-Other keywords"]))
            {
                content = fallbackItem["Metatags-Other keywords"];
            }

            if (item.Template.ID.ToString().Equals(TemplateIds.ProductRegion) || item.Template.ID.ToString().Equals(TemplateIds.Video) || item.Template.ID.ToString().Equals(TemplateIds.Downloads))
                content = content + " " + GetProductModelNumbers(item);

            if (item.TemplateID.ToString().Equals(TemplateIds.Downloads))
            {
                string downloadPath = item.Paths.Path.Replace("/sitecore/content/global/", "");

                if (downloadPath.Split('/').Any())
                {
                    if (!string.IsNullOrEmpty(downloadPath.Split('/')[0].ToString()))
                    {
                        content = content + " " + downloadPath.Split('/')[0].ToString();
                    }

                    if (!string.IsNullOrEmpty(downloadPath.Split('/')[1].ToString()))
                    {
                        content = content + " " + downloadPath.Split('/')[1].ToString();
                    }

                    if (!string.IsNullOrEmpty(downloadPath.Split('/')[2].ToString()))
                    {
                        content = content + " " + downloadPath.Split('/')[2].ToString();
                    }

                    if (!string.IsNullOrEmpty(downloadPath.Split('/')[8].ToString()))
                    {
                        content = content + " " + downloadPath.Split('/')[8].ToString();
                    }
                }
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
            {
                string faqPath = item.Paths.Path.Replace("/sitecore/content/global/", "");

                if (faqPath.Split('/').Any())
                {
                    if (!string.IsNullOrEmpty(faqPath.Split('/')[0].ToString()))
                    {
                        content = content + " " + faqPath.Split('/')[0].ToString();
                    }

                    if (!string.IsNullOrEmpty(faqPath.Split('/')[1].ToString()))
                    {
                        content = content + " " + faqPath.Split('/')[1].ToString();
                    }

                    if (!string.IsNullOrEmpty(faqPath.Split('/')[7].ToString()))
                    {
                        content = content + " " + faqPath.Split('/')[7].ToString();
                    }
                }
            }
            else
            {
                string contentPath = item.Paths.Path.Replace("/sitecore/content/GlobalWeb/home/", "");

                foreach (var path in contentPath.Split('/'))
                {
                    content = content + " " + path;
                }
            }

            return !string.IsNullOrEmpty(content) ? content : null;
        }

        private string GetProductModelNumbers(Item item)
        {
            StringBuilder sb = new StringBuilder();

            if (item.Template.ID.ToString().Equals(TemplateIds.Downloads))
            {
                using (new LanguageSwitcher(item.Language))
                {
                    var regionalProducts = ((MultilistField)item.Fields["RelatedProducts"]).GetItems();

                    foreach (var product in regionalProducts)
                    {
                        sb.Append(" " + product.Name + " ");

                        if (((ReferenceField)(product.Fields["SelectProduct"])).TargetItem != null)
                        {
                            var globalProduct = ((ReferenceField)(product.Fields["SelectProduct"])).TargetItem;
                            sb.Append(" " + globalProduct.Name + " " + globalProduct["ModelNumber"]);
                        }
                    }
                }
            }
            else if (item.Template.ID.ToString().Equals(TemplateIds.Video))
            {
                List<Item> regionalProducts = new List<Item>();
                using (new LanguageSwitcher(item.Language))
                {
                    regionalProducts.AddRange((from link in Globals.LinkDatabase.GetItemReferrers(item, false)
                                               let sourceItem = link.GetSourceItem()
                                               where sourceItem != null
                                               where sourceItem.TemplateID.ToString() == TemplateIds.ProductRegion
                                               select sourceItem).ToList().Distinct(new ItemEqualityComparer()).ToList<Item>());

                    regionalProducts = regionalProducts.Distinct(new ItemEqualityComparer()).ToList();

                    foreach (var product in regionalProducts)
                    {
                        sb.Append(" " + product.Name + " ");

                        if (((ReferenceField)(product.Fields["SelectProduct"])).TargetItem != null)
                        {
                            var globalProduct = ((ReferenceField)(product.Fields["SelectProduct"])).TargetItem;
                            sb.Append(" " + globalProduct.Name + " " + globalProduct["ModelNumber"]);
                        }
                    }
                }
            }
            else if (item.Template.ID.ToString().Equals(TemplateIds.ProductRegion))
            {
                using (new LanguageSwitcher(item.Language))
                {
                    if (((ReferenceField)(item.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var globalProduct = ((ReferenceField)(item.Fields["SelectProduct"])).TargetItem;
                        sb.Append(" " + globalProduct.Name + " " + globalProduct["ModelNumber"]);
                    }
                }
            }

            return sb.ToString();
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    #endregion

    # region Item Url Computed Field

    public class SiteSearchItemUrl : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            return GetItemUrl(item);
        }

        private string GetItemUrl(Item item)
        {
            var urlOptions = UrlOptions.DefaultOptions;
            urlOptions.SiteResolving = true;
            urlOptions.Language = item.Language;
            urlOptions.Site = SiteContext.GetSite("globalweb");

            if (item.TemplateID.ToString().Equals(TemplateIds.Downloads))
            {
                var subcategoryfallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Subcategory"]);
                if (subcategoryfallbackItem.Fields["Subcategory"].Value.Equals(ItemIds.DownloadSoftwareCategory))
                {
                    var position = item.Paths.Path.LastIndexOf("/", StringComparison.Ordinal) + 1;
                    return string.Format("/" + item.Language.Name + "/test-measurement/support/downloads/software/{0}",
                                            item.Paths.Path.Substring(position, item.Paths.Path.Length - position));
                }
                else
                {
                    if (((CheckboxField)subcategoryfallbackItem.Fields["RedirectToMyAnritsu"]).Checked)
                    {
                        return Settings.GetSetting("RedirectToMyAnritsu");
                    }
                    else
                    {
                        var bucketPath = "http://" + ConfigurationManager.AppSettings["DownloadDistributionList"] + "/";
                        if (item.Fields["FilePath"] != null)
                        {
                            var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["FilePath"]);
                            bucketPath = bucketPath + fallbackItem["FilePath"];

                            if (((CheckboxField)fallbackItem.Fields["LogOnRequired"]).Checked)
                            {
                                return "/Sign-In.aspx?" + QSKeys.ReturnURL + "=" + bucketPath;
                            }

                            return bucketPath;
                        }
                    }
                }

                return LinkManager.GetItemUrl(item, urlOptions);
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
            {
                var position = item.Paths.Path.LastIndexOf("/", StringComparison.Ordinal) + 1;
                return string.Format("/" + item.Language.Name + "/test-measurement/support/downloads/faq/{0}", item.Paths.Path.Substring(position, item.Paths.Path.Length - position));
            }
            else
            {
                return LinkManager.GetItemUrl(item, urlOptions);
            }
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    #endregion

    # region Site Area Computed Field

    public class SiteSearchArea : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            try
            {
                Item item = indexable as SitecoreIndexableItem;

                if (item == null)
                    return null;

                return GetArea(item);
            }
            catch (Exception)
            {
                //3chillies - suppress error
            }

            return null;
        }

        private string GetArea(Item item)
        {
            string area = "Other";
            Database contextDB = item.Database;

            if (item.TemplateID.ToString().Equals(TemplateIds.ProductRegion) || item.TemplateID.ToString().Equals(TemplateIds.ProductFamily) || item.TemplateID.ToString().Equals(TemplateIds.ProductCategory))
            {
                return ItemIds.FilterItemProducts;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
            {
                return ItemIds.FilterItemFaqs;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.Downloads))
            {
                return ItemIds.FilterItemDownloads;
            }
            else if (item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.Employment)))
            {
                return ItemIds.FilterItemEmployment;
            }
            else if (item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.AboutAnritsu)))
            {
                return ItemIds.FilterItemAboutAnritsu;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.ContactSupport) || item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.CorporateContact)))
            {
                return ItemIds.FilterItemContactUs;
            }
            else if (item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.BusinessServices)))
            {
                return ItemIds.FilterItemBusinessServices;
            }
            else if (item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.Support)))
            {
                return ItemIds.FilterItemSupport;
            }
            else if (item.ID.ToString().Equals(ItemIds.HomeNewsPage) || item.ID.ToString().Equals(ItemIds.TMNewsPage) || item.TemplateID.ToString().Equals(TemplateIds.NewsRelease) || item.TemplateID.ToString().Equals(TemplateIds.Announcement))
            {
                return ItemIds.FilterItemNews;
            }
            else if (item.ID.ToString().Equals(ItemIds.HomeEventsPage) || item.ID.ToString().Equals(ItemIds.TMEventsPage) || item.TemplateID.ToString().Equals(TemplateIds.TMEvent) || item.TemplateID.ToString().Equals(TemplateIds.Webinar) || item.TemplateID.ToString().Equals(TemplateIds.Tradeshow) || item.TemplateID.ToString().Equals(TemplateIds.CorporateEvent))
            {
                return ItemIds.FilterItemEvents;
            }
            else if (item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.Buy)))
            {
                return ItemIds.FilterItemBuy;
            }
            else if (item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.Technologies)))
            {
                return ItemIds.FilterItemTechnologies;
            }
            else if (item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.Industries)))
            {
                return ItemIds.FilterItemIndustries;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.Video))
            {
                return ItemIds.FilterItemVideos;
            }

            return area;
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gsa"), SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        public static string GetGsaArea(Item item)
        {
            var itemId = string.Empty;
            var contextDB = item.Database;

            //// This check is for wild card items, currently we are handling them in the sublayouts for downloads & faq
            //if (item.Name.Trim() == "*")
            //{
            //    return string.Empty;
            //}

            // Below check is for download wild card item
            if (item.ID.ToString().Equals(ItemIds.SoftwareWildcardItem))
            {
                itemId = ItemIds.FilterItemDownloads;
            }
            // Below check is for faq wild card item
            else if (item.ID.ToString().Equals(ItemIds.FaqWildcardItem))
            {
                itemId = ItemIds.FilterItemFaqs;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.ProductRegion) || 
                item.TemplateID.ToString().Equals(TemplateIds.ProductFamily) || item.TemplateID.ToString().Equals(TemplateIds.ProductCategory) ||
                item.ID.ToString().Equals(ItemIds.InfivisProducts) || item.TemplateID.ToString().Equals(TemplateIds.InfivisProductFamilyStatic) ||
                item.TemplateID.ToString().Equals(TemplateIds.InfivisProductDetailStatic) || item.TemplateID.ToString().Equals(TemplateIds.InfivisAccordionPlusGridStatic) ||
                item.TemplateID.ToString().Equals(TemplateIds.InfivisProductFamilyFeatureStatic) ||
                item.TemplateID.ToString().Equals(TemplateIds.InfivisProductDetailStaticC1))
            {
                itemId = ItemIds.FilterItemProducts;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
            {
                itemId = ItemIds.FilterItemFaqs;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.Downloads))
            {
                itemId = ItemIds.FilterItemDownloads;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.Video) || item.TemplateID.ToString().Equals(TemplateIds.InfivisVideo))
            {
                itemId = ItemIds.FilterItemVideos;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.ContactSupport) || 
                item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.CorporateContact)) || item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.InfivisContactUs)))
            {
                itemId = ItemIds.FilterItemContactUs;
            }
            else if (item.ID.ToString().Equals(ItemIds.HomeNewsPage) || item.ID.ToString().Equals(ItemIds.TMNewsPage) ||
                item.ID.ToString().Equals(ItemIds.InfivisNews) || item.TemplateID.ToString().Equals(TemplateIds.NewsRelease) || 
                item.TemplateID.ToString().Equals(TemplateIds.Announcement))
            {
                itemId = ItemIds.FilterItemNews;
            }
            else if (item.ID.ToString().Equals(ItemIds.HomeEventsPage) || item.ID.ToString().Equals(ItemIds.TMEventsPage) || item.ID.ToString().Equals(ItemIds.InfivisEvents) ||
                item.TemplateID.ToString().Equals(TemplateIds.TMEvent) || item.TemplateID.ToString().Equals(TemplateIds.Webinar) ||
                item.TemplateID.ToString().Equals(TemplateIds.Tradeshow) || item.TemplateID.ToString().Equals(TemplateIds.CorporateEvent))
            {
                itemId = ItemIds.FilterItemEvents;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Employment)) ||
                IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisEmployment)))
            {
                itemId = ItemIds.FilterItemEmployment;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.AboutAnritsu)) ||
                IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisAboutUs)))
            {
                itemId = ItemIds.FilterItemAboutAnritsu;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Support)) || 
                IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisSupport)))
            {
                itemId = ItemIds.FilterItemSupport;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Buy)))
            {
                itemId = ItemIds.FilterItemBuy;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Technologies)))
            {
                itemId = ItemIds.FilterItemTechnologies;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Industries)))
            {
                itemId = ItemIds.FilterItemIndustries;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.BusinessServices)))
            {
                itemId = ItemIds.FilterItemBusinessServices;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisKnowledgeCenter)))
            {
                itemId = ItemIds.FilterItemKnowledgeCenter;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisServices)))
            {
                itemId = ItemIds.FilterItemServices;
            }

            if (string.IsNullOrWhiteSpace(itemId)) return string.Empty;
            var areaItem = contextDB.GetItem(itemId);
            var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(areaItem, areaItem.Fields["Value"]);
            return fallbackItem != null
                ? fallbackItem.Fields["Value"] != null ? fallbackItem.Fields["Value"].Value : areaItem.Name
                : string.Empty;
        }


        /// <summary>
        /// Debu: Gets the default English value such as coveo results are configurable by Area value
        /// </summary>
        /// <param name="item"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static string GetGsaArea(Item item, Language language)
        {
            var itemId = string.Empty;
            var contextDB = item.Database;

            //// This check is for wild card items, currently we are handling them in the sublayouts for downloads & faq
            //if (item.Name.Trim() == "*")
            //{
            //    return string.Empty;
            //}

            // Below check is for download wild card item
            if (item.ID.ToString().Equals(ItemIds.SoftwareWildcardItem))
            {
                itemId = ItemIds.FilterItemDownloads;
            }
            // Below check is for faq wild card item
            else if (item.ID.ToString().Equals(ItemIds.FaqWildcardItem))
            {
                itemId = ItemIds.FilterItemFaqs;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.ProductRegion) ||
                item.TemplateID.ToString().Equals(TemplateIds.ProductFamily) || item.TemplateID.ToString().Equals(TemplateIds.ProductCategory) ||
                item.ID.ToString().Equals(ItemIds.InfivisProducts) || item.TemplateID.ToString().Equals(TemplateIds.InfivisProductFamilyStatic) ||
                item.TemplateID.ToString().Equals(TemplateIds.InfivisProductDetailStatic) || item.TemplateID.ToString().Equals(TemplateIds.InfivisAccordionPlusGridStatic) ||
                item.TemplateID.ToString().Equals(TemplateIds.InfivisProductFamilyFeatureStatic) ||
                item.TemplateID.ToString().Equals(TemplateIds.InfivisProductDetailStaticC1))
            {
                itemId = ItemIds.FilterItemProducts;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
            {
                itemId = ItemIds.FilterItemFaqs;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.Downloads))
            {
                itemId = ItemIds.FilterItemDownloads;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.Video) || item.TemplateID.ToString().Equals(TemplateIds.InfivisVideo))
            {
                itemId = ItemIds.FilterItemVideos;
            }
            else if (item.TemplateID.ToString().Equals(TemplateIds.ContactSupport) ||
                item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.CorporateContact)) || item.Axes.IsDescendantOf(contextDB.GetItem(ItemIds.InfivisContactUs)))
            {
                itemId = ItemIds.FilterItemContactUs;
            }
            else if (item.ID.ToString().Equals(ItemIds.HomeNewsPage) || item.ID.ToString().Equals(ItemIds.TMNewsPage) ||
                item.ID.ToString().Equals(ItemIds.InfivisNews) || item.TemplateID.ToString().Equals(TemplateIds.NewsRelease) ||
                item.TemplateID.ToString().Equals(TemplateIds.Announcement))
            {
                itemId = ItemIds.FilterItemNews;
            }
            else if (item.ID.ToString().Equals(ItemIds.HomeEventsPage) || item.ID.ToString().Equals(ItemIds.TMEventsPage) || item.ID.ToString().Equals(ItemIds.InfivisEvents) ||
                item.TemplateID.ToString().Equals(TemplateIds.TMEvent) || item.TemplateID.ToString().Equals(TemplateIds.Webinar) ||
                item.TemplateID.ToString().Equals(TemplateIds.Tradeshow) || item.TemplateID.ToString().Equals(TemplateIds.CorporateEvent))
            {
                itemId = ItemIds.FilterItemEvents;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Employment)) ||
                IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisEmployment)))
            {
                itemId = ItemIds.FilterItemEmployment;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.AboutAnritsu)) ||
                IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisAboutUs)))
            {
                itemId = ItemIds.FilterItemAboutAnritsu;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Support)) ||
                IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisSupport)))
            {
                itemId = ItemIds.FilterItemSupport;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Buy)))
            {
                itemId = ItemIds.FilterItemBuy;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Technologies)))
            {
                itemId = ItemIds.FilterItemTechnologies;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.Industries)))
            {
                itemId = ItemIds.FilterItemIndustries;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.BusinessServices)))
            {
                itemId = ItemIds.FilterItemBusinessServices;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisKnowledgeCenter)))
            {
                itemId = ItemIds.FilterItemKnowledgeCenter;
            }
            else if (IsDescendantOfValidArea(item, contextDB.GetItem(ItemIds.InfivisServices)))
            {
                itemId = ItemIds.FilterItemServices;
            }

            if (string.IsNullOrWhiteSpace(itemId)) return string.Empty;
            var areaItem = contextDB.GetItem(itemId, language);
            var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(areaItem, areaItem.Fields["Value"]);
            return fallbackItem != null
                ? fallbackItem.Fields["Value"] != null ? fallbackItem.Fields["Value"].Value : areaItem.Name
                : string.Empty;
        }


        public static bool IsDescendantOfValidArea(Item contextItem, Item parentItem)
        {
            return contextItem != null && parentItem != null && contextItem.Axes.IsDescendantOf(parentItem);
        }

        public static int GetSearchFilterSortOrder(string area)
        {
            const string cacheKey = "SearchFilters";
            var objLock = new object();
            var cacheItem = HttpContext.Current.Cache[cacheKey] as Dictionary<string, List<string>>;
            var itemLanguage = Context.Language;

            lock (objLock)
            {
                if (cacheItem == null)
                {
                    cacheItem = new Dictionary<string, List<string>>();
                    if (!cacheItem.ContainsKey(itemLanguage.ToString()))
                    {
                        var searchItem = Context.Database.GetItem(ItemIds.SearchFilterFolder, itemLanguage);
                        var searchFilters = searchItem.GetChildren().Select(x => x.Fields["Value"].ToString()).ToList();
                        cacheItem.Add(itemLanguage.ToString(), searchFilters);
                        if (HttpContext.Current.Cache[cacheKey] == null)
                        {
                            HttpContext.Current.Cache.Insert(cacheKey, cacheItem, null,
                                DateTime.Now.AddSeconds(3600),
                                TimeSpan.Zero);
                        }
                    }
                }
                else if (!cacheItem.ContainsKey(itemLanguage.ToString()))
                {
                    var searchItem = Context.Database.GetItem(ItemIds.SearchFilterFolder, itemLanguage);
                    var searchFilters = searchItem.GetChildren().Select(x => x.Fields["Value"].ToString()).ToList();
                    cacheItem.Add(itemLanguage.ToString(), searchFilters);
                    HttpContext.Current.Cache[cacheKey] = cacheItem;
                }
            }

            var sortOrder = cacheItem[itemLanguage.ToString()].IndexOf(area);
            if (sortOrder < 0)
                sortOrder = 9998;

            return sortOrder * 2;
        }
    }

    #endregion

    # region Category Computed Field

    public class SiteSearchItemCategory : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            return GetItemCategory(item);
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private IEnumerable<string> GetItemCategory(Item item)
        {
            List<string> categories = new List<string>();

            using (new LanguageSwitcher(item.Language))
            {

                if (item.Template.ID.ToString().Equals(TemplateIds.ProductRegion))
                {
                    var productItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["SelectProduct"]);

                    if (((ReferenceField)(productItem.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var globalProduct = ((ReferenceField)(productItem.Fields["SelectProduct"])).TargetItem;

                        var globalProductItem = SiteSearchHelper.GetSitecoreFallbackItem(globalProduct, globalProduct.Fields["ProductFamily"]);

                        var families = ((MultilistField)globalProductItem.Fields["ProductFamily"]).GetItems().Where(x => x != null).Distinct(new ItemEqualityComparer()).ToList();
                        families = families.Distinct(new ItemEqualityComparer()).ToList();

                        var subcategories = families.Select(x => x.Parent).Distinct(new ItemEqualityComparer()).ToList();

                        categories = subcategories.Select(x => x.ParentID.ToShortID().ToString()).ToList();
                    }
                }
                // else if (item.Template.ID.ToString().Equals(ItemIds.TestMeasurement) || item.ID.ToString().Equals(ItemIds.ComponentAndAccessories) || //item.ID.ToString().Equals(ItemIds.AnritsuDevices))
                // {
                // categories.Add(item.ID.ToShortID().ToString());
                // }
                else if (item.Template.ID.ToString().Equals(TemplateIds.ProductFamily))
                {
                    categories.Add(item.Parent.Parent.ID.ToShortID().ToString());
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.Downloads))
                {
                    var downloadItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Subcategory"]);

                    if (((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem != null)
                    {
                        categories.Add(((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem.Parent.ID.ToShortID().ToString());
                    }
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
                {
                    var regionalProducts = new List<Item>();

                    regionalProducts.AddRange(((MultilistField)item.Fields["RelatedProducts"]).GetItems().ToList());

                    var globalProducts = regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null).Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem).ToList();

                    var families = new List<Item>();
                    foreach (var product in globalProducts)
                    {
                        families.AddRange(((MultilistField)product.Fields["ProductFamily"]).GetItems().Where(x => x != null).Distinct(new ItemEqualityComparer()).ToList());
                    }

                    families = families.Distinct(new ItemEqualityComparer()).ToList();

                    var subcategories = families.Select(x => x.Parent).Distinct(new ItemEqualityComparer()).ToList();

                    categories = subcategories.Select(x => x.Parent.ID.ToShortID().ToString()).ToList();
                }

            }

            if (!categories.Any())
                categories.Add("category");

            return categories.Distinct();
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gsa")]
        public static IEnumerable<string> GetGsaItemCategory(Item item)
        {
            var categories = new List<string>();

            using (new LanguageSwitcher(item.Language))
            {

                if (item.Template.ID.ToString().Equals(TemplateIds.ProductRegion))
                {
                    var productItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["SelectProduct"]);

                    if (((ReferenceField)(productItem.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var globalProduct = ((ReferenceField)(productItem.Fields["SelectProduct"])).TargetItem;

                        var globalProductItem = SiteSearchHelper.GetSitecoreFallbackItem(globalProduct,
                            globalProduct.Fields["ProductFamily"]);

                        var families =
                            ((MultilistField)globalProductItem.Fields["ProductFamily"]).GetItems()
                                .Where(x => x != null)
                                .Distinct(new ItemEqualityComparer())
                                .ToList();
                        families = families.Distinct(new ItemEqualityComparer()).ToList();

                        var subcategories = families.Select(x => x.Parent).Distinct(new ItemEqualityComparer()).ToList();

                        categories =
                            subcategories.Select(
                                x =>
                                    SiteSearchHelper.GetSitecoreFallbackItem(x.Parent, x.Parent.Fields["PageTitle"])
                                        .Fields["PageTitle"].Value).ToList();
                    }
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.InfivisProductDetailStatic) ||
                         item.Template.ID.ToString().Equals(TemplateIds.InfivisProductDetailStaticC1))
                {
                    var subCategoryItem = item.Axes.GetAncestors().FirstOrDefault(x => x.TemplateID.ToString().Equals(TemplateIds.InfivisProductFamilyStatic));
                    categories.Add(SiteSearchHelper.GetSitecoreFallbackItem(subCategoryItem.Parent.Parent, subCategoryItem.Parent.Parent.Fields["PageTitle"])
                                        .Fields["PageTitle"].Value);                    
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.Downloads))
                {
                    var downloadItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Subcategory"]);

                    if (((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem != null)
                    {
                        categories.Add(
                            SiteSearchHelper.GetSitecoreFallbackItem(
                                ((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem.Parent,
                                ((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem.Parent.Fields["Value"])
                                .Fields["Value"].Value);
                    }
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
                {
                    var regionalProducts = new List<Item>();

                    regionalProducts.AddRange(((MultilistField)item.Fields["RelatedProducts"]).GetItems().ToList());

                    var globalProducts =
                        regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null)
                            .Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem)
                            .ToList();

                    var families = new List<Item>();
                    foreach (var product in globalProducts)
                    {
                        families.AddRange(
                            ((MultilistField)product.Fields["ProductFamily"]).GetItems()
                                .Where(x => x != null)
                                .Distinct(new ItemEqualityComparer())
                                .ToList());
                    }

                    families = families.Distinct(new ItemEqualityComparer()).ToList();

                    var subcategories = families.Select(x => x.Parent).Distinct(new ItemEqualityComparer()).ToList();

                    categories =
                        subcategories.Select(
                            x =>
                                SiteSearchHelper.GetSitecoreFallbackItem(x.Parent, x.Parent.Fields["PageTitle"]).Fields[
                                    "PageTitle"].Value).ToList();
                }
                //else if (item.Template.ID.ToString().Equals(TemplateIds.TrainingCourse))
                //{
                //}
            }

            if (!categories.Any())
                categories.Add(" ");

            //return string.Join(",", categories.Distinct());
            return categories.Distinct();
        }
    }

    #endregion

    # region Subcategory Computed Field

    public class SiteSearchItemSubcategory : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            return GetItemSubcategory(item);
        }

        private IEnumerable<string> GetItemSubcategory(Item item)
        {
            List<string> subcategories = new List<string>();

            using (new LanguageSwitcher(item.Language))
            {

                if (item.Template.ID.ToString().Equals(TemplateIds.ProductRegion))
                {
                    var productItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["SelectProduct"]);
                    if (((ReferenceField)(productItem.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var globalProduct = ((ReferenceField)(productItem.Fields["SelectProduct"])).TargetItem;

                        var globalProductItem = SiteSearchHelper.GetSitecoreFallbackItem(globalProduct,
                            globalProduct.Fields["ProductFamily"]);
                        var families =
                            ((MultilistField)globalProductItem.Fields["ProductFamily"]).GetItems()
                                .Where(x => x != null)
                                .Distinct(new ItemEqualityComparer())
                                .ToList();
                        families = families.Distinct(new ItemEqualityComparer()).ToList();

                        subcategories = families.Select(x => x.ParentID.ToShortID().ToString()).ToList();
                    }
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.ProductFamily))
                {
                    subcategories.Add(item.Parent.ID.ToShortID().ToString());
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.Downloads))
                {
                    var downloadItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Subcategory"]);
                    if (((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem != null)
                    {
                        subcategories.Add(
                            ((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem.ID.ToShortID().ToString());
                    }
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
                {
                    var regionalProducts = new List<Item>();

                    regionalProducts.AddRange(((MultilistField)item.Fields["RelatedProducts"]).GetItems().ToList());

                    var globalProducts =
                        regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null)
                            .Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem)
                            .ToList();
                    var families = new List<Item>();
                    foreach (var product in globalProducts)
                    {
                        families.AddRange(
                            ((MultilistField)product.Fields["ProductFamily"]).GetItems()
                                .Where(x => x != null)
                                .Distinct(new ItemEqualityComparer())
                                .ToList());
                    }
                    families = families.Distinct(new ItemEqualityComparer()).ToList();
                    subcategories = families.Select(x => x.ParentID.ToShortID().ToString()).ToList();
                }
            }

            if (!subcategories.Any())
                subcategories.Add("subcategory");

            return subcategories.Distinct();
        }

        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gsa")]
        public static IEnumerable<string> GetGsaItemSubcategory(Item item)
        {
            var subcategories = new List<string>();

            using (new LanguageSwitcher(item.Language))
            {

                if (item.Template.ID.ToString().Equals(TemplateIds.ProductRegion))
                {
                    var productItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["SelectProduct"]);
                    if (((ReferenceField)(productItem.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var globalProduct = ((ReferenceField)(productItem.Fields["SelectProduct"])).TargetItem;

                        var globalProductItem = SiteSearchHelper.GetSitecoreFallbackItem(globalProduct,
                            globalProduct.Fields["ProductFamily"]);
                        var families =
                            ((MultilistField)globalProductItem.Fields["ProductFamily"]).GetItems()
                                .Where(x => x != null)
                                .Distinct(new ItemEqualityComparer())
                                .ToList();
                        families = families.Distinct(new ItemEqualityComparer()).ToList();

                        subcategories = families.Select(x =>
                            SiteSearchHelper.GetSitecoreFallbackItem(x.Parent.Parent,
                                x.Parent.Parent.Fields["PageTitle"]).Fields["PageTitle"].Value.Substring(0, 2) +
                            GsaMetaStringSeperators.CategorySubCategorySeparator +
                            SiteSearchHelper.GetSitecoreFallbackItem(x.Parent, x.Parent.Fields["MenuTitle"]).Fields[
                                "MenuTitle"].Value).ToList();
                    }
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.InfivisProductDetailStatic) ||
                         item.Template.ID.ToString().Equals(TemplateIds.InfivisProductDetailStaticC1))
                {
                    var subCategoryItem = item.Axes.GetAncestors().FirstOrDefault(x => x.TemplateID.ToString()
                                           .Equals(TemplateIds.InfivisProductFamilyStatic));

                    subcategories.Add(SiteSearchHelper.GetSitecoreFallbackItem(subCategoryItem.Parent.Parent,
                            subCategoryItem.Parent.Parent.Fields["PageTitle"]).Fields["PageTitle"].Value.Substring(0, 2) +
                            GsaMetaStringSeperators.CategorySubCategorySeparator + 
                            SiteSearchHelper.GetSitecoreFallbackItem(subCategoryItem, subCategoryItem.Fields["ProductSubcategoryTitle"])
                                        .Fields["ProductSubcategoryTitle"].Value);
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.Downloads))
                {
                    var downloadItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Subcategory"]);
                    if (((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem != null)
                    {
                        subcategories.Add(
                            SiteSearchHelper.GetSitecoreFallbackItem(
                                ((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem.Parent,
                                ((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem.Parent.Fields["Value"])
                                .Fields["Value"].Value.Substring(0, 2) +
                            GsaMetaStringSeperators.CategorySubCategorySeparator +
                            SiteSearchHelper.GetSitecoreFallbackItem(
                                ((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem,
                                ((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem.Fields["Value"]).Fields
                                ["Value"].Value + GsaMetaStringSeperators.SubCategoryMetaRegSeparator +
                            ((ReferenceField)downloadItem.Fields["Subcategory"]).TargetItem.Name);
                    }
                }
                else if (item.Template.ID.ToString().Equals(TemplateIds.FrequentlyAskedQuestion))
                {
                    var regionalProducts = new List<Item>();

                    regionalProducts.AddRange(((MultilistField)item.Fields["RelatedProducts"]).GetItems().ToList());

                    var globalProducts =
                        regionalProducts.Where(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem != null)
                            .Select(x => ((ReferenceField)(x.Fields["SelectProduct"])).TargetItem)
                            .ToList();
                    var families = new List<Item>();
                    foreach (var product in globalProducts)
                    {
                        families.AddRange(
                            ((MultilistField)product.Fields["ProductFamily"]).GetItems()
                                .Where(x => x != null)
                                .Distinct(new ItemEqualityComparer())
                                .ToList());
                    }
                    families = families.Distinct(new ItemEqualityComparer()).ToList();
                    subcategories = families.Select(x =>
                        SiteSearchHelper.GetSitecoreFallbackItem(x.Parent.Parent, x.Parent.Parent.Fields["PageTitle"])
                            .Fields["PageTitle"].Value.Substring(0, 2) +
                        GsaMetaStringSeperators.CategorySubCategorySeparator +
                        SiteSearchHelper.GetSitecoreFallbackItem(x.Parent, x.Parent.Fields["MenuTitle"]).Fields[
                            "MenuTitle"].Value).ToList();
                }
            }

            if (!subcategories.Any())
                subcategories.Add(" ");

            //return string.Join(",", subcategories.Distinct());
            return subcategories.Distinct();
        }
    }

    #endregion

    #region Site Search ResultItem Extension

    public class SiteSearchResultDownloadFileExtension : IComputedIndexField
    {
        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            return item == null ? null : GetFileExtension(item);
        }

        private string GetFileExtension(Item item)
        {
            if (!item.TemplateID.ToString().Equals(TemplateIds.Downloads)) return string.Empty;

            return item.Fields["FileType"] != null && !string.IsNullOrEmpty(item["FileType"])
                ? item["FileType"]
                : string.Empty;
        }
    }

    #endregion

    # region discontinued product Computed Field

    public class SiteSearchItemDiscontinuedProduct : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (item.TemplateID.ToString() != TemplateIds.ProductRegion)
                return null;

            using (new LanguageSwitcher(item.Language))
            {
                var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["SelectProduct"]);

                if (((ReferenceField)(fallbackItem.Fields["SelectProduct"])).TargetItem != null)
                {
                    var productItem = ((ReferenceField)(fallbackItem.Fields["SelectProduct"])).TargetItem;
                    productItem = SiteSearchHelper.GetSitecoreFallbackItem(productItem, productItem.Fields["IsDiscontinued"]);

                    return (((CheckboxField)productItem.Fields["IsDiscontinued"])).Checked;
                }
            }
            return false;
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    #endregion
}