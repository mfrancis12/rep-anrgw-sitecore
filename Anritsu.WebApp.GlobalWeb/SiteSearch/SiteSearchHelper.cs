﻿using Sitecore;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.Constants;
using System.Web;
using System.Linq;
using System;
using Sitecore.SharedSource.PartialLanguageFallback.Extensions;
using Sitecore.Data.Fields;
using System.Net;
using System.Text;

namespace Anritsu.WebApp.GlobalWeb.SiteSearch
{
    public static class SiteSearchHelper
    {
        public static bool ShouldIndexItem(this Item item)
        {
            //check for exlcude item from search
            if (item != null && item.Fields["Metatags-Metatags"] != null && item.Fields["Metatags-Metatags"].Value.Contains(ItemIds.SitecoreNoIndex))
                return false;

            //only items w/ layout that are not template standard values
            return item != null && item.Visualization != null && item.Visualization.Layout != null && !item.Paths.LongID.Contains(ItemIDs.TemplateRoot.ToString());
        }

        public static bool IsSearchCrawler(HttpRequest request)
        {
            return request.Headers.AllKeys.Contains("SitecoreSearch");
        }

        public static Item GetSitecoreFallbackItem(Item item, Field fld)
        {
            var checkValue = true;
            Item currentItem = item;
            try
            {

                if (item.Fields[fld.ID] != null)
                    checkValue = item.Fields[fld.ID].HasValue;

                if (checkValue)
                {
                    return item;
                }

                var fallbackItem = item.GetFallbackItem();
                if (fallbackItem != null)
                {
                    //Recursive call to get the item that has value for the particular field.
                    currentItem = GetSitecoreFallbackItem(fallbackItem, fld);
                }

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message + " " + ex.StackTrace, "SiteSearchHelper");
            }
            return currentItem;
        }
    }
}