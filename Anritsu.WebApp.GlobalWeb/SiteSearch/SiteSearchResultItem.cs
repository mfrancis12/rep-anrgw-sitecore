﻿using System.Collections.Specialized;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;

namespace Anritsu.WebApp.GlobalWeb.SiteSearch
{
    public class SiteSearchResultItem : SearchResultItem
    {
        [IndexField("itemname")]
        public string ItemName { get; set; }

        [IndexField("itemmetadescription")]
        public string ItemMetaDescription { get; set; }

        [IndexField("itemmetakeywords")]
        public string ItemMetaKeywords { get; set; }

        [IndexField("itemcontent")]
        public string ItemContent { get; set; }

        [IndexField("sitesearchitemurl")]
        public string ItemUrl { get; set; }

        [IndexField("sitesearcharea")]
        public string SiteArea { get; set; }

        [IndexField("sitesearchcategory")]
        public string SiteAreaCategory { get; set; }

        [IndexField("sitesearchsubcategory")]
        public string SiteAreaSubcategory { get; set; }

        [IndexField("sitesearchdiscontinuedproduct")]
        public bool IsDiscontinuedProduct { get; set; }

        [IndexField("sitesearchresultdownloadfileextension")]
        public string SearchDownloadFileExtension { get; set; }
    }
}