﻿using System.Linq;
using System.Text;
using System.Collections.Generic;
using Sitecore;
using Sitecore.Links;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SiteSearch
{
    public class VisualizationField : IComputedIndexField
    {
        private readonly HashSet<string> _textFieldTypes = new HashSet<string>(new[]
        {
            "Single-Line Text", 
            "Rich Text", 
            "Multi-Line Text", 
            "text", 
            "rich text", 
            "html", 
            "memo", 
            "Word Document"
        });

        public string FieldName { get; set; }
        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            var item = (Item)(indexable as SitecoreIndexableItem);
            Assert.ArgumentNotNull(item, "item");

            if (!item.ShouldIndexItem())
            {
                return null;
            }

            var result = new StringBuilder();

            item.Fields.ReadAll();

            foreach (var field in item.Fields.Where(x => x.Section.ToLowerInvariant().Equals("metatags")).Where(ShouldIndexField))
            {
                result.AppendLine(field.Value);
            }

            foreach (var field in item.Fields.Where(x => !x.Section.ToLowerInvariant().Equals("metatags")).Where(ShouldIndexField))
            {
                result.AppendLine(field.Value);
            }

            IEnumerable<Item> dataSources =
                Globals.LinkDatabase.GetReferences(item)
                       .Where(link => ShouldProcessLink(link, item))
                       .Select(link => link.GetTargetItem())
                       .Where(targetItem => targetItem != null)
                       .Distinct();


            foreach (var dataSource in dataSources.Where(ShouldIndexDataSource))
            {
                dataSource.Fields.ReadAll();
                foreach (var field in dataSource.Fields.Where(ShouldIndexField))
                {
                    result.AppendLine(field.Value);
                }
            }

            if (item.Template.ID.ToString().Equals(TemplateIds.Downloads) || item.Template.ID.ToString().Equals(TemplateIds.Video) || item.Template.ID.ToString().Equals(TemplateIds.ProductRegion))
                result.Append(GetProductModelNumbers(item));

            return result.ToString();
        }

        protected virtual bool ShouldProcessLink(ItemLink link, Item sourceItem)
        {
            //layout field references in the same database
            return link.SourceFieldID == FieldIDs.LayoutField && link.SourceDatabaseName == sourceItem.Database.Name;
        }

        protected virtual bool ShouldIndexDataSource(Item item)
        {
            //don't process references to renderings
            return !item.Paths.LongID.Contains(ItemIDs.LayoutRoot.ToString());
        }

        protected virtual bool ShouldIndexField(Field field)
        {
            //process non-empty text fields that are not part of the standard template
            return !field.Name.StartsWith("__") && IsTextField(field) && !string.IsNullOrEmpty(field.Value);
        }

        protected virtual bool IsTextField(Field field)
        {
            return _textFieldTypes.Contains(field.Type);
        }

        private string GetProductModelNumbers(Item item)
        {
            StringBuilder sb = new StringBuilder();

            if (item.Template.ID.ToString().Equals(TemplateIds.Downloads))
            {
                var regionalProducts = ((MultilistField)item.Fields["RelatedProducts"]).GetItems();

                foreach (var product in regionalProducts)
                {
                    sb.Append(" " + product.Name + " ");

                    if (((ReferenceField)(product.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var globalProduct = ((ReferenceField)(product.Fields["SelectProduct"])).TargetItem;
                        sb.Append(" " + globalProduct.Name + " " + globalProduct["ModelNumber"]);
                    }
                }
            }
            else if (item.Template.ID.ToString().Equals(TemplateIds.Video))
            {
                List<Item> regionalProducts = new List<Item>();
                regionalProducts.AddRange((from link in Sitecore.Globals.LinkDatabase.GetItemReferrers(item, false)
                                           let sourceItem = link.GetSourceItem()
                                           where sourceItem != null
                                           where sourceItem.TemplateID.ToString() == TemplateIds.ProductRegion
                                           select sourceItem).ToList().Distinct(new ItemEqualityComparer()).ToList<Item>());

                regionalProducts = regionalProducts.Distinct(new ItemEqualityComparer()).ToList();

                foreach (var product in regionalProducts)
                {
                    sb.Append(" " + product.Name + " ");

                    if (((ReferenceField)(product.Fields["SelectProduct"])).TargetItem != null)
                    {
                        var globalProduct = ((ReferenceField)(product.Fields["SelectProduct"])).TargetItem;
                        sb.Append(" " + globalProduct.Name + " " + globalProduct["ModelNumber"]);
                    }
                }
            }
            else if (item.Template.ID.ToString().Equals(TemplateIds.ProductRegion))
            {
                if (((ReferenceField)(item.Fields["SelectProduct"])).TargetItem != null)
                {
                    var globalProduct = ((ReferenceField)(item.Fields["SelectProduct"])).TargetItem;
                    sb.Append(" " + globalProduct.Name + " " + globalProduct["ModelNumber"]);
                }
            }

            return sb.ToString();
        }
    }
}