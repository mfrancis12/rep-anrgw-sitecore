﻿define(["sitecore"], function (Sitecore) {
    var AddLinks = Sitecore.Definitions.App.extend({
        initialized: function () {
            app = this;
            var contextId = this.getID();
            var $submitBtn = $('button[data-sc-id $= "btnFinish" ]')
            var prodList = { label: '[data-sc-id $="LabelProductList"]', control: '[data-sc-id$="ListProductsQueryListControl"]' };
            var treeView = { label: '[data-sc-id $= "LabelDisplayPage"]', control: '[data-sc-id$="TreeViewDisplayPage"]' };
            var linkWithMedia = "", linkType = "";
            var targetType = "";

            //hiding extra header
            $submitBtn.closest('.sc-list').find('.sc-globalHeader').remove();

            window.top.jqueryModalDialogsFrame.contentWindow.setDialogDimension(655, 600);

            $('input[name $= "PageTypeRadioButtonGroup"]').on("change", function () {
                if ($(this).val() == "displaypage") {
                    toggleControl(treeView, prodList);
                } else {
                    toggleControl(prodList, treeView);
                }

            });

            $submitBtn.on('click', function () {
                var linkName = app.txtLinkName.get("text");
                var linkURL = app.txtInsertLink.get("text");
                var mediaURL = app.txtMediaLink.get("text");
                var startDate = app.DatePickerStartDate.get("formattedDate");
                var endDate = app.DatePickerEndDate.get("formattedDate");
                var languageItems = app.LanguageQueryDataSource.get("items");
                var productsSelected = app.ProductQueryDataSource.get("items");
                var displayPageSelected = app.TreeViewDisplayPage.get("checkedItemIds");

                $.ajax({
                    type: "GET",
                    data: {
                        ID: contextId,
                        LinkWithMedia: linkWithMedia,
                        LinkType: linkType,
                        LinkName: linkName,
                        LinkURL: linkURL,
                        MediaURL: mediaURL,
                        TargetType: targetType,
                        StartDate: startDate,
                        EndDate: endDate,
                        LanguageSelected: selectedItems('LanguageControlList'),
                        ProductsSelected: selectedItems('ListProductsQueryListControl'),
                        DisplayPageSelected: displayPageSelected
                    },
                    url: "/SitecoreCustom/shell/client/YourApps/Properties/CustomHandler.ashx",
                    success: function (response) {
                        if (response) {
                            app.BorderManageLink.set("text", response);
                            $(response).prependTo('[data-sc-id $= "BorderManageLink"]');
                            $(window).scrollTop(0);
                        }
                        else {
                            window.top.dialogClose();
                        }
                    },
                    error: function (exception) {
                        $('[data-sc-id $= "lblFailureMessage"]').show();
                        $(window).scrollTop(0);
                    }

                });
            });

            function selectedItems(selectId) {
                var tempStr = "";
                $('select[data-sc-id $= "' + selectId + '"]').find('option:selected').each(function (i, n) {
                    tempStr += $(this).val() + ",";
                })
                return tempStr.substring(0, tempStr.lastIndexOf(","));
            }

            function toggleControl(arg1, arg2) {
                for (prop in arg1) {
                    $(arg1[prop]).show();
                }
                for (prop in arg2) {
                    $(arg2[prop]).hide();
                }
            }

            window.callbackFunc = function (response) {
                var selectedLinkid = "";

                //check wether response is id or link
                if (response.indexOf("{") == 0) {
                    linkWithMedia = "media";
                    selectedLinkid = response;
                } else {
                    var $resp = $(response);
                    linkType = $resp.attr('linktype'); selectedLinkid = $resp.attr('id');
                    if (linkType == "external") {
                        targetType = $resp.attr('target');
                    }
                }

                if (linkWithMedia == "media" || linkType == "internal") {
                    $.ajax({
                        type: "GET",
                        data: {
                            ID: selectedLinkid
                        },
                        url: "/SitecoreCustom/shell/client/YourApps/Properties/GetItemDetails.ashx",
                        success: function (response) {
                            if (linkWithMedia == "media") {
                                app.txtMediaLink.set("text", response);
                            }
                            else if (linkType == "internal") {
                                app.txtInsertLink.set("text", response);
                            }
                        },
                        error: function (exception) {
                        }

                    });
                } else {
                    app.txtInsertLink.set("text", $(response).attr("url"));
                }
            }
        },
        getID: function () {
            var id = Sitecore.Helpers.url.getQueryParameters(window.location.href)['id'];
            if (Sitecore.Helpers.id.isId(id)) {
                return id;
            }
        },
        HyperLinkButtonClick: function (linkType) {
            var targetURL, features = "";
            if (linkType == "internal") {
                targetURL = "/sitecore/shell/Applications/Dialogs/Internal link.aspx";
            }
            else if (linkType == "external") {
                targetURL = "/sitecore/shell/Applications/Dialogs/External link.aspx?hdl=A9497822AB9A4AA8992B3DC0B3FED25F";
            }
            else if (linkType == "media") {
                targetURL = "/sitecore/client/applications/Dialogs/SelectMediaDialog.aspx";
            }
            window.top.scForm.showModalDialog(targetURL, new Array(), "center:yes;help:no;resizable:yes;scroll:yes;status:no;dialogWidth:506;dialogHeight:285", null, callbackFunc);
        }
    });

    return AddLinks;
});