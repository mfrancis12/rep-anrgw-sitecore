﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Sitecore.Diagnostics;
using Anritsu.WebApp.GlobalWeb.Constants;

namespace Anritsu.WebApp.GlobalWeb.SitecoreCustom.Shell.Client.YourApps.Properties
{
    /// <summary>
    /// Handler to create link and teasers
    /// </summary>
    public class CustomHandler : IHttpHandler
    {
        /// <summary>
        /// IsReusable Property
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private static string _linksAssociatedMessage = string.Empty;
        private static string _referenceItems = string.Empty;

        /// <summary>
        /// Handler Process Request
        /// </summary>
        ///<param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            //ProcessRequestBase(new HttpContextWrapper(context));
            {
                Assert.ArgumentNotNull(context, "Context");

                string contextItem = context.Request.Params["ID"];
                string linkname = context.Request.Params["LinkName"];
                string linkSource = context.Request.Params["LinkURL"];
                string mediaUrl = context.Request.Params["MediaURL"];
                string selectedLanguage = context.Request.Params["LanguageSelected"];
                string linkWithMedia = context.Request.Params["linkWithMedia"];
                string linkType = context.Request.Params["LinkType"];
                string productsSelected = context.Request.Params["ProductsSelected"];
                string otherPagesSelected = context.Request.Params["DisplayPageSelected"];
                string targetType = context.Request.Params["TargetType"];
                string startDate = context.Request.Params["StartDate"];
                string endDate = context.Request.Params["EndDate"];

                ProcessHandlerRequest(contextItem, linkname, linkSource, mediaUrl, selectedLanguage, productsSelected, otherPagesSelected, linkWithMedia, linkType, targetType, startDate, endDate);

                //HttpContextBase wrapper = new HttpContextWrapper(context);
                //this.ProcessRequest(wrapper);
            }
        }

        ///  <summary>
        ///  Mock method to trigger ProcessRequest from unit test cases
        ///  </summary>
        //public void ProcessRequest(HttpContextBase context);

        public static void ProcessHandlerRequest(string contextItemId, string linkName, string linkSource, string mediaSource, string selectedLang,
                                        string prodSelected, string otherPages, string linkWithMedia, string linkType, string targetType, string startDate, string endDate)
        {
            //if (contextItemId != null)
            //{
            var db = Sitecore.Configuration.Factory.GetDatabase("master");

            var languageDelimiters = new[] { ',' };
            var selectedLanguage = selectedLang.Split(languageDelimiters, StringSplitOptions.RemoveEmptyEntries).ToList();
            var languageCodes = new List<string>();

            foreach (string id in selectedLanguage)
            {
                if (db.GetItem(id) != null)
                {
                    languageCodes.Add(string.IsNullOrEmpty(db.GetItem(id)["Regional Iso Code"]) ? db.GetItem(id)["Iso"] : db.GetItem(id)["Regional Iso Code"]);
                }
            }

            bool isLinkWithMedia = linkWithMedia != null && linkWithMedia.Equals("media");

            bool isLinkOnly = !string.IsNullOrEmpty(linkSource) && string.IsNullOrEmpty(mediaSource);

            using (new Sitecore.Security.Accounts.UserSwitcher(Sitecore.Context.User))
            {
                var linksParentFolder = db.GetItem(contextItemId);
                string templateId = isLinkWithMedia ? TemplateIds.TeaserTemplate : TemplateIds.LinkTemplate;

                var sourceTemplate = new TemplateID(new ID(templateId));

                linksParentFolder.Editing.BeginEdit();
                var newLinkItem = linksParentFolder.Add(linkName, sourceTemplate);
                linksParentFolder.Editing.EndEdit();

                newLinkItem.Versions.RemoveVersion();

                foreach (string lang in languageCodes)
                {
                    newLinkItem = db.GetItem(newLinkItem.ID, Sitecore.Globalization.Language.Parse(lang));
                    if (newLinkItem.Versions.Count <= 0)
                    {
                        newLinkItem.Versions.AddVersion();
                    }

                    #region Create Teasers/Links for Languages selected
                    //using (new Sitecore.Security.Accounts.UserSwitcher(Sitecore.Context.User))
                    //{
                    newLinkItem.Editing.BeginEdit();

                    if (isLinkWithMedia)
                    {
                        newLinkItem["Title"] = linkName;
                        ImageField teaserImgFld = newLinkItem.Fields["Image"];

                        var teaserMedia = new MediaItem(db.GetItem(mediaSource));

                        teaserImgFld.MediaID = teaserMedia.ID;
                        teaserImgFld.Alt = linkName;

                        LinkField linkField = newLinkItem.Fields["Link"];

                        //Process Link Field
                        ProcessLinkField(linkName, linkSource, targetType, linkField, linkType);

                    }
                    else if (isLinkOnly)
                    {
                        newLinkItem["Link Title"] = linkName;
                        LinkField linkField = newLinkItem.Fields["Link"];

                        //Process Link Field
                        ProcessLinkField(linkName, linkSource, targetType, linkField, linkType);
                    }

                    newLinkItem.Fields[Sitecore.FieldIDs.ValidFrom].Value = ProcessDate(startDate);
                    newLinkItem.Fields[Sitecore.FieldIDs.ValidTo].Value = ProcessDate(endDate);

                    newLinkItem.Editing.EndEdit();
                    //}
                    #endregion

                    #region Bind Links/Teasers to pages

                    //var productDelimiter = new[] { ',' };
                    var productsSelected = prodSelected.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    //var displayPageDelimiter = new[] { '|' };
                    var displayPageSelected = otherPages.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    if (productsSelected.Count > 0)
                    {
                        foreach (string productId in productsSelected)
                        {
                            Item currentProduct = db.GetItem(productId, Sitecore.Globalization.Language.Parse(lang));
                            if (currentProduct != null && currentProduct.Versions.Count > 0)
                            {
                                if (currentProduct.Access.CanWrite())
                                {
                                    //Mapping the newly created link/teaser to the current product selected
                                    MapLinks(currentProduct, newLinkItem, isLinkWithMedia);
                                }
                                else
                                {
                                    _referenceItems += "<li>" + currentProduct.Name + "</li>";
                                }
                                //MapLinks(currentProduct, newLinkItem, isLinkWithMedia);
                            }
                        }
                    }
                    else if (displayPageSelected.Count > 0)
                    {
                        foreach (string displayPageId in displayPageSelected)
                        {
                            Item displayPage = db.GetItem(displayPageId, Sitecore.Globalization.Language.Parse(lang));

                            if (displayPage != null && displayPage.Versions.Count > 0)
                            {
                                if (displayPage.Access.CanWrite())
                                {
                                    //Mapping the newly created link/teaser to the current display page selected
                                    MapLinks(displayPage, newLinkItem, isLinkWithMedia);
                                }
                                else
                                {
                                    _referenceItems += "<li>" + displayPage.Name + "</li>";
                                }
                                //MapLinks(displayPage, newLinkItem, isLinkWithMedia);
                            }
                        }
                    }
                    #endregion
                }
            }
            //}

            ErrorMessage(_referenceItems, _linksAssociatedMessage);
        }

        ///  <summary>
        ///  Process Link Field
        ///  </summary>
        private static void ProcessLinkField(string lnkName, string lnkUrl, string targetType, LinkField linkField, string linkType)
        {
            string linkName = lnkName;
            string linkSource = lnkUrl;
            Database db = Sitecore.Configuration.Factory.GetDatabase("master");

            switch (linkType)
            {
                case "internal":
                    {
                        linkField.Text = linkName;
                        linkField.LinkType = "internal";
                        linkField.TargetID = db.GetItem("/sitecore/content" + linkSource).ID;
                        break;
                    }
                case "external":
                    {
                        linkField.Text = linkName;
                        linkField.LinkType = "external";
                        linkField.Url = linkSource;
                        linkField.Target = targetType;
                        break;
                    }
                case "anchor":
                    {
                        linkField.Text = linkName;
                        linkField.LinkType = "anchor";
                        linkField.Url = linkSource;
                        linkField.Title = linkName;
                        break;
                    }
                case "mailto":
                    {
                        linkField.Text = linkName;
                        linkField.LinkType = "mailto";
                        linkField.Url = linkSource;
                        break;
                    }
            }
        }

        ///  <summary>
        ///  Process Link Field
        ///  </summary>
        /// <param name="currentItem"></param>
        /// <param name="newLinkItem"></param>
        /// <param name="isLinkWithMedia"></param>
        private static void MapLinks(Item currentItem, Item newLinkItem, bool isLinkWithMedia)
        {
            using (new Sitecore.Security.Accounts.UserSwitcher(Sitecore.Context.User))
            {
                currentItem.Editing.BeginEdit();
                if (isLinkWithMedia)
                {
                    //Mapping newly created teaser to current item selected
                    MultilistField teasersMultiList = currentItem.Fields["Teasers"];
                    if (teasersMultiList != null)
                    {
                        if (teasersMultiList.GetItems().Count() >= 3)
                        {
                            _linksAssociatedMessage += "<li>" + currentItem.DisplayName + "</li>";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(teasersMultiList.Value))
                            {
                                teasersMultiList.Value = teasersMultiList.Value + "|";
                            }
                            teasersMultiList.Value = teasersMultiList.Value + Convert.ToString(newLinkItem.ID, CultureInfo.InvariantCulture);
                        }
                    }
                }
                else
                {
                    //Mapping newly created link to current item selected
                    MultilistField linksMultiList = currentItem.Fields["Links"];
                    if (linksMultiList != null)
                    {
                        if (linksMultiList.GetItems().Count() >= 3)
                        {
                            _linksAssociatedMessage += "<li>" + currentItem.DisplayName + "</li>";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(linksMultiList.Value))
                            {
                                linksMultiList.Value = linksMultiList.Value + "|";
                            }
                            linksMultiList.Value = linksMultiList.Value + Convert.ToString(newLinkItem.ID, CultureInfo.InvariantCulture);
                        }
                    }
                }

                currentItem.Editing.EndEdit();
            }
        }

        ///  <summary>
        ///  Process Date Field
        ///  </summary>
        /// <param name="dateValue"></param>
        private static string ProcessDate(string dateValue)
        {
            if (!string.IsNullOrEmpty(dateValue))
            {
                DateTime startDate = DateTime.ParseExact(dateValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                return Sitecore.DateUtil.ToIsoDate(startDate);
            }
            return string.Empty;
        }

        ///  <summary>
        ///  Process Error Message
        ///  </summary>
        /// <param name="_referenceItemsMessage"></param>
        /// <param name="_linksAssociationMessage"></param>
        private static void ErrorMessage(string _referenceItemsMessage, string _linksAssociationMessage)
        {
            if (!string.IsNullOrEmpty(_referenceItemsMessage) && !string.IsNullOrEmpty(_linksAssociationMessage))
            {
                HttpContext.Current.Response.ContentType = "text/HTML";
                HttpContext.Current.Response.Write("<div>" + "Following item(s) already have mapping for 3 links :" + "<ul>" + _linksAssociationMessage + "</ul></div>" + "<div><b>" + Sitecore.Context.User.LocalName + "</b>" + ", Doesn't have the edit permission for the following items: " + "<ul>" + _referenceItemsMessage + "</ul></div>");
            }
            if (!string.IsNullOrEmpty(_referenceItemsMessage) && string.IsNullOrEmpty(_linksAssociationMessage))
            {
                HttpContext.Current.Response.ContentType = "text/HTML";
                HttpContext.Current.Response.Write("<div><b>" + Sitecore.Context.User.LocalName + "</b>" + ", Doesn't have the edit permission for the following items: " + "<ul>" + _referenceItemsMessage + "</ul></div>");
            }

            if (!string.IsNullOrEmpty(_linksAssociationMessage) && string.IsNullOrEmpty(_referenceItemsMessage))
            {
                HttpContext.Current.Response.ContentType = "text/HTML";
                HttpContext.Current.Response.Write("<div>" + "Following item(s) already have mapping for 3 links :" + "<ul>" + _linksAssociationMessage + "</ul></div>");
            }
            _referenceItems = string.Empty;
            _linksAssociatedMessage = string.Empty;
        }
    }
}