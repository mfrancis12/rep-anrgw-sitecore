﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutAnritsu.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.AboutAnritsu" %>
<%@ Register tagPrefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel"  %>

<div class="rightside">
<div class="boxitem">
    <!-- Box Item -->
    <div class="boxitemtitle">
        <div class="boxitemtitleleft"></div>
        <div class="boxitemtitleright"></div>
        <div class="boxitemtitletext">
            <sc:Text runat="server" ID="Title" Field="Title"/>
        </div>
    </div>
    <div class="boxitembody">
        <div class="overflowhidden">
            <div class="boxitembodyleft">
                <div class="boxitemlist">
                    <div class="boxitemlisttext">
                        <sc:Text runat="server" ID="Description" Field="Description"/>
                    </div>
                </div>
            </div>
            <!-- BEGIN SIDE BANNER-->
            <div class="boxitembodyright">
                <div class="boxitemimage">
                    <sc:Image runat="server" ID="AboutImage" Field="Image"/>
                </div>
            </div>
            <!-- END SIDE BANNER-->
        </div>
    </div>
    <div class="boxitemfooter">
        <div>
            <img alt="" src="http://static.cdn-anritsu.com/images/wdwftrl.gif">
        </div>
        <div class="movetoright">
            <img alt="" src="http://static.cdn-anritsu.com/images/wdwftrr.gif">
        </div>
    </div>
    <!-- end of boxitembody -->
</div>
</div>