﻿using Sitecore.Web.UI.WebControls;
using System;
using System.Web.UI;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class AboutAnritsu : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var aboutAnritsu = Parent as Sublayout;

            if (aboutAnritsu != null && !string.IsNullOrEmpty(aboutAnritsu.DataSource))
            {
                var currentItem = Sitecore.Context.Database.GetItem(aboutAnritsu.DataSource);
                if (currentItem != null)
                {
                    Title.Item = Description.Item = AboutImage.Item = currentItem;
                }

            }
        }
    }
}