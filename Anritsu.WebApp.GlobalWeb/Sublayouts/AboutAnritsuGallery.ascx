﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutAnritsuGallery.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.AboutAnritsuGallery" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="full-container video-gallery-title nogap">
    <div class="container nogap">
        <div class="hd-left">
            <h1><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "videogallery") %></h1>
        </div>
        <div class="hd-right"></div>
    </div>
</div>
<div class="full-container video-gallery-body">
    <div class="container nogap">
        <div class="video-filter-left">
            <div class="result-filter">
                <div class="hd"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "categoryfilter") %></div>
                <ul class="bd">
                    <li class="level1 on">
                        <div class="level1-item">
                            <span><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "products") %></span>
                            <div class="icon icon-arrow-up-grey"></div>
                        </div>
                        <ul class="level1_ul">
                            <asp:Repeater ID="level2" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue" >
                                <ItemTemplate>
                                    <li class="level2">
                                       <a href="javascript:void(0)"> <asp:CheckBox ID="category" runat="server" AutoPostBack="true" OnCheckedChanged="Category_CheckedChanged" /><asp:HiddenField ID="hiddenCategory" runat="server" Value="<%#Item.Value%>" />
                                        <span class="level2hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span></a>
                                        <span class="icon icon-plus-grey iconStyles"></span>
                                        <ul class="paddingLeft">
                                            <asp:Repeater ID="level3" runat="server" DataSource='<%#Item.NestedFacets.FirstOrDefault().FacetValues%>' ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                                <ItemTemplate>
                                                    <li class="level3">
                                                         <asp:CheckBox ID="subCategory" runat="server" AutoPostBack="true" OnCheckedChanged="Subcategory_CheckedChanged" /><asp:HiddenField ID="hiddenSubCategory" runat="server" Value="<%#Item.Value%>" />
                                                         <span class="level2hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span>
                                                         <span class="icon icon-plus-grey iconStyles"></span> 
                                                        <ul class="level4 paddingLeft">
                                                            <asp:Repeater ID="level4" runat="server" DataSource='<%#Item.NestedFacets.FirstOrDefault().FacetValues%>' ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                                                <ItemTemplate>
                                                                   <li>
                                                                       <asp:CheckBox ID="family" runat="server" AutoPostBack="true" OnCheckedChanged="Family_CheckedChanged" /><asp:HiddenField ID="hiddenfamily" runat="server" Value="<%#Item.Value%>" />
                                                                       <span class="level3hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span>
                                                                   </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="clearall align-right">
                <asp:LinkButton ID="clearAll" CssClass="clear-all" runat="server" ClientIDMode="Static" OnClick="ClearAll_OnClick"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "clearall") %></asp:LinkButton></div>
        </div>
        <div class="latest-video-right">
            <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "latestvideos") %></h3>
            <div class="latest-filter">
                <asp:DropDownList ID="dropListFilter" runat="server" OnSelectedIndexChanged="DropListFilter_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                    <asp:ListItem Text="Release Date" Value="Release Date"></asp:ListItem>
                    <asp:ListItem Text="Views" Value="Views"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <asp:ListView ID="videos" runat="server" OnPagePropertiesChanged="Videos_PagePropertiesChanged" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.SearchItem" >
                <ItemTemplate>
                    <div class="item">
                        <div class="left">
                            <div class="img">
                                <a href="<%#Item.Url%>">
                                    <img src="<%#Item.VideoItem.ThumbnailURL%>" alt="">
                                </a>
                            </div>
                            <div class="related">
                                <p class="duration">
                                    <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "duration") %>: <%#GetDuration(Editable(Item,x=>x.VideoItem.Length))%><br>
                                    <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "releasedate") %>:<%#!string.IsNullOrEmpty(Editable(Item,x=>x.VideoItem.PublishedDate))?Editable(Item,x=>Convert.ToDateTime(x.VideoItem.PublishedDate).ToLocalTime().ToFormattedDate()):"" %>
                                </p>
                            </div>
                        </div>
                        <div class="right">
                            <div class="content">
                                <h3><%#Editable(Item,x=>x.Name) %></h3>
                                <p><%#Editable(Item,x=>x.ShortDescription) %></p>
                            </div>
                            <div class="view"><b><%#Editable(Item,x=>x.VideoItem.PlaysTotal)%></b><%#!string.IsNullOrEmpty(Editable(Item,x=>x.VideoItem.PlaysTotal))?Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "views"):"" %></div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:ListView>
            <div class="pagination">
                <span class="page">
                <asp:DataPager ID="videoDataPager" runat="server" PagedControlID="videos" PageSize="20"  >
                    <Fields>
                        <asp:TemplatePagerField>
                            <PagerTemplate>
                                <span class="info">                                  
                                        <%# Container.TotalRowCount > 0 ? (Container.StartRowIndex + 1) : 0 %>-<%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%> / <%# Container.TotalRowCount %> Results                                   
                                </span>
                            </PagerTemplate>
                        </asp:TemplatePagerField>
                        <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active floatLft"  NextPageText=">" PreviousPageText="<"  />
                    </Fields>
                </asp:DataPager>
                </span>
            </div>
        </div>
    </div>
</div>

