﻿using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using Sitecore.ContentSearch.Linq;
using Glass.Mapper.Sc;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Web.Ui;
using System.Linq;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Search;
using SearchFacetValue = Anritsu.WebApp.GlobalWeb.Search.FacetValue;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class AboutAnritsuGallery : GlassUserControl<IAboutGallery>
    {
        #region Global Variables
        Database master = Sitecore.Context.Database;
        string indexName = "anritsu_brightcovevideotemplates";
        public int numberOfPages = 0;
        bool isChecked = false;
        #endregion
        private void Page_Load(object sender, EventArgs e)
        {
            BindFilters();
            BindVideos();
            // CreateLogs();  
        }

        protected void ClearAll_OnClick(object sender, EventArgs e)
        {
            BindVideos();
        }

        public string GetDuration(string length)
        {
            string duration = string.Empty;
            if (!string.IsNullOrEmpty(length))
            {
                TimeSpan time = TimeSpan.FromMilliseconds(Convert.ToInt32(length));
                duration = time.ToString(@"hh\:mm\:ss");
            }
            return duration;
        }

        protected void Videos_PagePropertiesChanged(object sender, EventArgs e)
        {
            if (isChecked)
            {
                BindVideoByFamily();
            }
            else
            {
                BindVideos();
            }
        }

        protected void DropListFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            videoDataPager.SetPageProperties(0, videoDataPager.PageSize, true);
            if (isChecked)
            {
                BindVideoByFamily();
            }
            else
            {
                BindVideos();
            }
        }

        protected void Category_CheckedChanged(object sender, EventArgs e)
        {
            videoDataPager.SetPageProperties(0, videoDataPager.PageSize, true);
            BindVideoByFamily();

        }
        protected void Subcategory_CheckedChanged(object sender, EventArgs e)
        {
            videoDataPager.SetPageProperties(0, videoDataPager.PageSize, true);
            BindVideoByFamily();

        }
        protected void Family_CheckedChanged(object sender, EventArgs e)
        {
            videoDataPager.SetPageProperties(0, videoDataPager.PageSize, true);
            BindVideoByFamily();

        }

        private void BindFilters()
        {
            try
            {
                //var sitecoreService = new SitecoreService("master");               
                using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
                {

                    var query = context.GetQueryable<SearchItem>().Filter(i => i != null && i.Language == Sitecore.Context.Language.Name).Where(x => x != null).Where(x => x.RelatedCategories != null && x.RelatedSubcategories != null && x.RelatedFamilies != null);
                    //foreach (SearchItem item in query)
                    //{
                    //    sitecoreService.Map(item);                        
                    //}
                    // query = query.Where(x => x.RelatedCategories != null && x.RelatedSubcategories != null && x.RelatedFamilies != null);
                    //foreach (SearchItem category in query)
                    //{
                    //    foreach(string categoryValue in category.RelatedCategories)
                    //    {
                    //        CreateLog.Create("Category Value : " + categoryValue);
                    //    }

                    //}
                    //CreateLog.Create("Error details : Step1" );
                    var categoryFacets = query.FacetPivotOn(x => x.FacetOn(y => y.RelatedCategories).FacetOn(y => y.RelatedSubcategories).FacetOn(y => y.RelatedFamilies)).GetFacets();
                    //CreateLog.Create("Error details : Step2" );
                    level2.DataSource = CollectFacets(categoryFacets.Categories).FirstOrDefault().FacetValues;
                    level2.DataBind();
                }
            }
            catch (Exception ex)
            {
                //CreateLog.Create("Error details : "+  ex.GetBaseException());
                //CreateLog.Create("Error details : " + ex.InnerException);
                //CreateLog.Create("Error details : " + ex.Message);
            }
        }

        private void BindVideos()
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var videoList = new List<SearchItem>();
            using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
            {
                var query = context.GetQueryable<SearchItem>().Filter(i => i.Language == Sitecore.Context.Language.Name);

                foreach (SearchItem item in query)
                {
                    sitecoreService.Map(item);
                    videoList.Add(item);
                }
                videoList = videoList.Distinct().Where(x => !string.IsNullOrEmpty(x.Video)).ToList();
                if (dropListFilter.SelectedItem.Text.ToUpperInvariant() == "RELEASE DATE")
                {
                    videos.DataSource = videoList.Where(x => !string.IsNullOrEmpty(x.VideoItem.PublishedDate.Trim())).OrderByDescending(x => Convert.ToDateTime(x.VideoItem.PublishedDate)).ToList();
                    videos.DataBind();
                }
                else if (dropListFilter.SelectedItem.Text.ToUpperInvariant() == "VIEWS")
                {
                    videos.DataSource = videoList.Where(x => !string.IsNullOrEmpty(x.VideoItem.PlaysTotal.Trim())).OrderByDescending(x => Convert.ToInt32(x.VideoItem.PlaysTotal)).ToList();
                    videos.DataBind();
                }
                else
                {
                    videos.DataSource = videoList;
                    videos.DataBind();
                }

            }
        }

        private IEnumerable<IFacet> CollectFacets(List<FacetCategory> facetCategoryList)
        {

            var list = new List<Facet>();
            try
            {
                foreach (var resultFacetCategory in facetCategoryList)
                {
                    var neestedFacetNames = resultFacetCategory.Name.Split(',');

                    list.Add(new Facet()
                    {
                        DisplayName = neestedFacetNames[0], // todo: use name resolver
                        FieldName = neestedFacetNames[0],
                        FacetValues = CollectFacetValues(resultFacetCategory.Values, neestedFacetNames, 0, new List<string>()),
                    });
                }

            }
            catch (Exception ex)
            {
                //CreateLog.Create("Error details(CollectFacets) : " + ex.Message);
            }
            return list;
        }

        private IEnumerable<IFacet> CollectNestedFacets(List<Sitecore.ContentSearch.Linq.FacetValue> facetValueList, string[] neestedFacetNames, int level, List<string> parentPathList)
        {
            var list = new List<Facet>();
            try
            {
                var parentPath = string.Join("/", parentPathList.ToArray());

                foreach (var resultFacetCategory in facetValueList
                        .Where(d => d.Name.StartsWith(parentPath))
                        .Select(d => d.Name)
                        .Select(s => s.Substring(parentPath.Length))
                        .Where(s => s.Length > 0)
                        .Select(s => s.Substring(0, (s.IndexOf('/') < 0) ? s.Length : s.IndexOf('/')))
                        .Distinct())
                {
                    list.Add(new Facet()
                    {
                        DisplayName = neestedFacetNames[level], // todo: use name resolver
                        FieldName = neestedFacetNames[level],
                        FacetValues = CollectFacetValues(facetValueList, neestedFacetNames, level, parentPathList)
                    });
                }
            }
            catch (Exception ex)
            {
                //CreateLog.Create("Error details(CollectNestedFacets): " + ex.Message);
            }

            return list;
        }

        private IEnumerable<SearchFacetValue> CollectFacetValues(List<Sitecore.ContentSearch.Linq.FacetValue> facetValueList, string[] neestedFacetNames, int level, List<string> parentPathList)
        {
            var list = new List<SearchFacetValue>();
            try
            {
                var parentPath = string.Join("/", parentPathList.ToArray());

                if (level >= neestedFacetNames.Length)
                    return list;

                foreach (var resultFacetValue in
                    facetValueList
                        .Where(d => d.Name.StartsWith(parentPath))
                        .Select(d => d.Name.Split('/'))
                        .Where(l => l.Count() > parentPathList.Count())
                        .Select(l => l.Skip(parentPathList.Count()).First())
                        .Distinct())
                {
                    var nextLevelPathList = new List<string>(parentPathList);
                    nextLevelPathList.Add(resultFacetValue);
                    var nextLevelPath = string.Join("/", nextLevelPathList.ToArray());

                    var sum = facetValueList.Where(d => d.Name.StartsWith(nextLevelPath)).Select(d => d.AggregateCount).Sum();

                    list.Add(new Anritsu.WebApp.GlobalWeb.Search.FacetValue()
                    {
                        DisplayName = ResolveFacetValueName(resultFacetValue), // todo: use value resolver
                        Value = resultFacetValue,
                        DocumentCount = sum,
                        NestedFacets = CollectNestedFacets(facetValueList, neestedFacetNames, level + 1, nextLevelPathList)
                    });
                }
            }
            catch (Exception ex)
            {
                //CreateLog.Create("Error details(CollectFacetValues) : " + ex.Message);
            }

            return list;
        }

        public string ResolveFacetValueName(string name)
        {
            Guid id = Guid.Empty;
            if (Guid.TryParse(name, out id))
            {
                var item = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(id));
                if (item != null)
                {
                    return Sitecore.Context.Culture.TextInfo.ToTitleCase(item.Name);
                }
            }

            return Sitecore.Context.Culture.TextInfo.ToTitleCase(name);
        }

        private void BindVideoByFamily()
        {
            var selectedFamilies = new List<string>();
            foreach (RepeaterItem item in level2.Items)
            {
                Repeater subCategoryRepeater = (Repeater)item.FindControl("level3");
                foreach (RepeaterItem subCategoryItem in subCategoryRepeater.Items)
                {
                    Repeater familyRepeater = (Repeater)subCategoryItem.FindControl("level4");
                    foreach (RepeaterItem familyItem in familyRepeater.Items)
                    {
                        CheckBox family = (CheckBox)familyItem.FindControl("family");
                        if (family.Checked)
                        {
                            string id = ((HiddenField)familyItem.FindControl("hiddenfamily")).Value;
                            if (!string.IsNullOrEmpty(id))
                            {
                                selectedFamilies.Add(id);
                            }
                            isChecked = true;
                        }
                    }
                }
            }
            BindVideos(selectedFamilies);
        }

        private void BindVideos(List<string> familyValues)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var finalVideos = new List<SearchItem>();
            var videoList = new List<SearchItem>();
            using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
            {
                var query = context.GetQueryable<SearchItem>().Filter(i => i.Language == Sitecore.Context.Language.Name);

                foreach (SearchItem item in query)
                {
                    sitecoreService.Map(item);
                    videoList.Add(item);
                }
                foreach (SearchItem video in videoList.ToList())
                {
                    if (video.RelatedFamilies != null)
                    {
                        foreach (string relatedFamily in video.RelatedFamilies)
                        {
                            if (familyValues.Contains(relatedFamily))
                            {
                                finalVideos.Add(video);
                            }
                        }
                    }
                }
                finalVideos = finalVideos.Distinct().Where(x => !string.IsNullOrEmpty(x.Video)).ToList();
                if (dropListFilter.SelectedItem.Text.ToUpperInvariant() == "RELEASE DATE")
                {

                    videos.DataSource = finalVideos.Where(x => !string.IsNullOrEmpty(x.VideoItem.PublishedDate.Trim())).OrderByDescending(x => Convert.ToDateTime(x.VideoItem.PublishedDate)).ToList();
                    videos.DataBind();

                }
                else if (dropListFilter.SelectedItem.Text.ToUpperInvariant() == "VIEWS")
                {
                    videos.DataSource = finalVideos.Where(x => !string.IsNullOrEmpty(x.VideoItem.PlaysTotal.Trim())).OrderByDescending(x => Convert.ToInt32(x.VideoItem.PlaysTotal)).ToList();
                    videos.DataBind();
                }
                else
                {
                    videos.DataSource = finalVideos;
                    videos.DataBind();
                }

            }

        }
    }
}