﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutAnritsuLandingBanner.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.AboutAnritsuLandingBanner" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>


<% if (Model.Content.BannerImage!=null && !string.IsNullOrEmpty(Model.Content.BannerImage.Src))
                     { %>
<div class="full-container multiProCls">
    <div class="hero-banner-3" style="background-image: url('<%=Model.Content.BannerImage.Src%>')">
        <div class="hero-content">
            <h1 class="heroEle <%=Editable(x=>x.Content.GetTextColor()) %>"><%=Editable(x=> x.Content.BannerTitle) %></h1>
            <h3 class="heroEle <%=Editable(x=>x.Content.GetTextColor()) %>"><%=Editable(x=> x.Content.BannerSubtitle) %></h3>
            <p class="heroEle <%=Editable(x=>x.Content.GetTextColor()) %>"><%=Editable(x=> x.Content.BannerDescription) %></p>
        </div>
    </div>
</div>
<% } %>
<%else 
  {%>
<div class="full-container container">
    <div class="products-heading">
        <div class="products-title">
            <h1><%=Editable(x=> x.Content.BannerTitle) %></h1>
        </div>
        <div class="products-description"><%=Editable(x=> x.Content.BannerDescription) %></div>
    </div>
</div>
<% }
%>