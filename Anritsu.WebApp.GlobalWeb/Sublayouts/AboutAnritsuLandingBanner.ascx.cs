﻿using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class AboutAnritsuLandingBanner : GlassUserControl<IAboutAnritsuLanding>
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected string GetDivImgClass(string imagePath)
        {
            return imagePath == null ? "hiddentxt" : "";
        }
    }
}