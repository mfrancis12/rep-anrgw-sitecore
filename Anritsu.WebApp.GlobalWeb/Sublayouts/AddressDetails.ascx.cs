﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Glass.Mapper.Sc;
using Sitecore;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class AddressDetails : GlassUserControl<IContactSupport>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindAddressTypes();
        }

        private void BindAddressTypes()
        {
            AddressTypes.DataSource = Addresses.DataSource = Model.GetChildren<IDropListItem>().Where(x => x != null);
            AddressTypes.DataBind();
            Addresses.DataBind();
        }

        public IEnumerable<IAddressDetails> GetContactDetails(IModelBase addressType)
        {
            var country = GetCountryItemBySelectedCountry();

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                return SitecoreContext.GetItem<IDropListItem>(addressType.Id.ToString()).GetChildren<IAddressDetails>().Where(x => x != null && x.SelectCountry.Any() && x.SelectCountry.Any(y => y.Id.ToString().Equals(Request.QueryString["id"], StringComparison.OrdinalIgnoreCase))).ToList(); ;
            }
            return SitecoreContext.GetItem<IDropListItem>(addressType.Id.ToString()).GetChildren<IAddressDetails>().Where(x => x != null && x.SelectCountry.Any() && x.SelectCountry.Any(y => y.Id.ToString().Equals(country, StringComparison.OrdinalIgnoreCase))).ToList();
        }

        private string GetCountryItemBySelectedCountry()
        {
            var defaultCountryId = SitecoreContext.GetItem<IModelBase>(ItemIds.UnitedStates).Id.ToString();

            if (Response.Cookies[CookieVariables.UserCountryCode] == null) return defaultCountryId;

            var currentCountryChildren =
                SitecoreContext
                    .GetItem<IModelBase>(ItemIds.ISOCountries)
                    .GetChildren<IISOCountry>()
                    .FirstOrDefault(y => y.CountryCode.Equals(Response.Cookies[CookieVariables.UserCountryCode].Value, StringComparison.OrdinalIgnoreCase));
            return currentCountryChildren != null ? currentCountryChildren.Id.ToString() : defaultCountryId;
        }
    }
}