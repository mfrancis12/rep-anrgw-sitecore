﻿using Sitecore.Web.UI.WebControls;
using System;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Analytics : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           var analytics = Parent as Sublayout;

           if (analytics != null && !string.IsNullOrEmpty(analytics.DataSource))
           {
               var scriptItem = Sitecore.Context.Database.GetItem(analytics.DataSource);
               if (scriptItem != null)
               {
                   Script.Item = scriptItem;
               }

           }
        }
    }
}