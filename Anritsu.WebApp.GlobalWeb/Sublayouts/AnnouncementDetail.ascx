﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnnouncementDetail.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.AnnouncementDetail" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="container">
    <div class="content-form">
        <div class="content-detail width100">
            <h1><%= Editable(x=>x.Title) %></h1>
            <p>
                
            </p>
            <div class="faq">
                <p><%= Editable(x=>x.Description) %></p>
            </div>
        </div>

    </div>
</div>