﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.Constants;
using Sitecore;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc;
using System.Collections.Generic;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using BLL = Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using System.Data;
using System.Web;
using System.Text.RegularExpressions;
using Anritsu.Library.GWCMSDK;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class AnnouncementTicker : GlassUserControl<IHome>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            GetTickerFromCMS();
          

        }

      
        private void GetTickerFromCMS()
        {
            try
            {
                var url = HttpContext.Current.Request.Url.AbsolutePath;

                DataTable dt = null;
                dt = GWCMServiceManager.GetTickers(url);
                        
                
                if (dt != null && dt.Rows.Count > 0)
                {
                    ticker.Visible = true;
                    pimticker.DataSource = dt;
                    pimticker.DataBind();

                }
                else
                {
                    ticker.Style.Add("display", "none");
                }


            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Ticker Error"+ex.Message, this);
                ticker.Style.Add("display", "none");
            }


        }
    }
}