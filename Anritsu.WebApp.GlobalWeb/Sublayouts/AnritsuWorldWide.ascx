﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnritsuWorldwide.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.AnritsuWorldwide" %>
<div class="container">
    <div class="content-inpage">
        <div class="hd">
            <h1><%=Editable(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IAnritsuWorldwide>(),x=>x.Title) %></h1>
            <p><%=Editable(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IAnritsuWorldwide>(),x=>x.Description) %></p>
        </div>
        <div class="bd">
            <div class="left">
                <div class="tab-interface locations">
                    <ul>
                        <asp:Repeater ID="RegionTabs" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                            <ItemTemplate>
                                <li><a href="#section<%#Item.Id.ToString()%>" class="<%#Container.ItemIndex==0?"active":"" %>"><span><%#Editable(Item,x=>x.Value) %></span></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <asp:Repeater ID="Regions" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                        <ItemTemplate>
                            <div id="section<%#Item.Id.ToString() %>" class="section <%#Container.ItemIndex==0?"active":"" %>">
                                <div class="location-inner-tab tab-interface">
                                    <ul>
                                        <asp:Repeater ID="CountryTabs" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem" DataSource="<%#GetCountries(Item)%>">
                                            <ItemTemplate>
                                                <li><a href="#section<%#Item.Id.ToString()%>" class="<%#Container.ItemIndex==0?"active":"" %>"><span><%#Editable(Item,x=>x.Value) %></span></a></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                    <asp:Repeater ID="Countries" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem" DataSource="<%#GetCountries(Item)%>">
                                        <ItemTemplate>
                                            <div id="section<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?"active":"" %>">
                                                <asp:Repeater ID="Addresses" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICompanyAddress" DataSource="<%#GetAddresses(Item)%>">
                                                    <ItemTemplate>
                                                        <div class="item">
                                                             
                                                           
                                                           <p class="name"><a href="<%#GetLink(Item)%>" class="<%# GetTitleLinkClass(Item)%>"><%# Editable(Item,x=>x.Title) %></a>
                                                            <p><%#Editable(Item,x=>x.ShortDescription) %> </p>
                                                            <p class="address">
                                                                <%#Editable(Item,x=>x.Details) %>                                                            
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="line"></div>
            </div>
            <div class="right">
                <div class="hq-localtion">
                    <div style="width: 100%; height: 40px; text-align: center; margin-left: auto; margin-right: auto; background: url('<%=Editable(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IAnritsuWorldwide>(),x=>x.Icon.Src)%>') no-repeat center;" class="img"></div>
                    <div class="location-text-container">
                        <p class="name"><%=Editable(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IAnritsuWorldwide>(),x=>x.Heading) %></p>
                        <p style="font-size: 14px" class="name"><%=Editable(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IAnritsuWorldwide>(),x=>x.CompanyName) %></p>
                        <p class="location"><%=Editable(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IAnritsuWorldwide>(),x=>x.Location) %></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



