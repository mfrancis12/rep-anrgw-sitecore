﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models;



namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class AnritsuWorldwide : GlassUserControl<IAnritsuWorldwide>
    {
        protected string HasLink { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            BindRegions();
        }

        private void BindRegions()
        {
            RegionTabs.DataSource = Model.GetChildren<IDropListItem>(); 
            RegionTabs.DataBind();
            Regions.DataSource = Model.GetChildren<IDropListItem>();
            Regions.DataBind();
        }
        public IEnumerable<IDropListItem> GetCountries(IModelBase region)
        {
            return SitecoreContext.GetItem<IDropListItem>(region.Id.ToString()).GetChildren<IDropListItem>().Where(x => x != null);             
        }
        public IEnumerable<ICompanyAddress> GetAddresses(IModelBase country)
        {
            return SitecoreContext.GetItem<IDropListItem>(country.Id.ToString()).GetChildren<ICompanyAddress>().Where(x => x != null);               
        }
        protected string GetLink(ICompanyAddress item)
        {
            if (item.TitleLink != null && !string.IsNullOrEmpty(item.TitleLink.Url))
            {
                HasLink = item.TitleLink.Url;
            }
            else
            {
                HasLink = "javascript:void(0);";
            }
            return HasLink;

        }
        protected string GetTitleLinkClass(ICompanyAddress item)
        {
            if (item.TitleLink != null && !string.IsNullOrEmpty(item.TitleLink.Url))
            {
                return string.Empty;
            }
            else
            {
                return "no-link";
            }
        }
    }
}