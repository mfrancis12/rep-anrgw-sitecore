﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BackToTopIcon.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.BackToTopIcon" %>
<div class="container nogap">
    <div id="totop"><i class="icon icon-arrow-grey-up"></i></div>
</div>
