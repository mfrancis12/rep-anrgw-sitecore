﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerWithDescription.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.BannerWithDescription" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Pages.Extensions" %>
<div class="full-container">
    <div class="products-family">
        <div class="products-content">
            <h1 class="<%=Editable(x=>x.GetTextColor()) %>"><%= Editable(x=>x.PageTitle) %></h1>
            <div class="img">
                <%=RenderImage(x=>x.Image) %>
            </div>
        </div>
    </div>
</div>
<div class="full-container">
    <div class="container">
        <div class="products-family-content">
            <%= Editable(x=>x.Description) %>
        </div>
    </div>
</div>
