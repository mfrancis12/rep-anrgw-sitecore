﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerWithTabRendering.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.BannerWithTabRendering" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Pages" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>

<div class="full-container nogap">
    <div class="page-heading">
        <div class="inner">
            <div class="heading-text">
                <h1 class="title emplyTitle"><%=Editable(SitecoreContext.GetCurrentItem<IBannerWithTabContent>(), x=> x.PageTitle) %></h1>
            </div>
            <div class="heading-button">
                        <a href=" <%=Editable(SitecoreContext.GetCurrentItem<IBannerWithTabContent>(),x=>x.Link.Url) %>"><%=Editable(SitecoreContext.GetCurrentItem<IBannerWithTabContent>(),x=>x.Title) %></a>
            </div>
            </div>
    </div>
<%--</div>
<div class="full-container nogap">--%>
    <div class="category-slider slick">
        
                <div class="slider">
                    <div class="product-image">
                        <%=RenderImage(SitecoreContext.GetCurrentItem<IBannerWithTabContent>(),x=>x.BannerImage,isEditable:true) %>
                    </div>
                    <div class="text">
                        <div class="inner">
                            <div class="name <%=Editable(x=>x.GetTextColor()) %>"><%=Editable(SitecoreContext.GetCurrentItem<IBannerWithTabContent>(),x=>x.BannerTitle) %></div>
                            <div class="sub-title <%=Editable(x=>x.GetTextColor()) %>"><%=Editable(SitecoreContext.GetCurrentItem<IBannerWithTabContent>(),x=>x.BannerSubtitle) %></div>
                            <div class="description <%=Editable(x=>x.GetTextColor()) %>">
                                <%=Editable(SitecoreContext.GetCurrentItem<IBannerWithTabContent>(),x=>x.BannerDescription) %>
                            </div>
                             </div>
                    </div>
                </div>
    </div>
    </div>
<div class="container">
            <div class="tab-interface hero-tab">
                <ul>
                <asp:Repeater ID="Tabs" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
                <ItemTemplate>
                    <li><a href="#<%#Item.Id.ToString()%>" class="<%#Container.ItemIndex==0?"active":""%>"><span><%#Editable(Item,x=>x.Title)%></span></a></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
                <asp:Repeater ID="TabsDescription" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
            <ItemTemplate>
                <div id="<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?"active":""%>">
                     <%#Editable(Item,x=>x.Description) %>
                </div>
            </ItemTemplate>
        </asp:Repeater>
                </div>
    </div>
