﻿using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class BannerWithTabRendering : GlassUserControl<IBannerWithTabContent>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            Tabs.DataSource = Model.GetChildren<ITitleWithDescription>();
            Tabs.DataBind();

            TabsDescription.DataSource = Model.GetChildren<ITitleWithDescription>();
            TabsDescription.DataBind();  
        }
    }
}