﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessUnit.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.BusinessUnit" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>

<h2 class="hd">
    <%= RenderLink(x=>x.Link, isEditable: true, contents:Editable(x=>x.Title)+"<div class='" + GetClass(Model.Link) + "'></div>")%>
</h2>
<div class="bd">
    <% using (BeginRenderLink(x => x.Link, isEditable: true))
       { %>
    <div style="background-image: url('<%=Model.GetImagePath() %>')" class="img"></div>
    <div class="text <%=Editable(x=>x.TextAlign.Key) %> <%=Editable(x=>x.GetTextColor()) %>">
        <p><%=Editable(x=> x.Description) %></p>
    </div>
      <% } %>
   <%-- <%= RenderLink(x=>x.Link,new { @class ="button "+ Editable(x=>x.ButtonAlign.Key)}, true)%>--%>
</div>