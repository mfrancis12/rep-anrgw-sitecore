﻿using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Carousel : GlassUserControl<IHome>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            RepeaterCarousel.DataSource = Model.HeroCarousel;
            RepeaterCarousel.DataBind();
        }
    }
}