﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Categories.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Categories" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="products-tech">
    <asp:Repeater ID="categories" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICategory">
        <ItemTemplate>
            <div class="item">
                <div class="inner">
                    <h3 class="title"><%#Editable(Item,x=>x.Title) %></h3>
                    <div class="content">
                        <div class="text">
                            <div class="img">
                                <%#Editable(Item,x=>x.Image,new {@class="img100"} ) %>
                            </div>
                            <p class="more">
                                <%#Editable(Item,x=>x.Description)%>
                            </p>
                        </div>
                        <div class="device">
                            <asp:Repeater ID="subcategories" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.IModelBase" DataSource="<%#GetSubcategories(Item) %>">
                                <ItemTemplate>
                                    <a href="<%#Item.Url %>"><%#SitecoreContext.GetItem<Sitecore.Data.Items.Item>(Item.Id.ToString())["MenuTitle"] %></a>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
