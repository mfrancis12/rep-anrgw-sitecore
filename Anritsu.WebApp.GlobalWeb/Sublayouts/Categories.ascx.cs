﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc;
    using Glass.Mapper.Sc.Web.Ui;
    using System;
    using System.Collections.Generic;
    
    public partial class Categories : GlassUserControl<IStaticPageWithBanner>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            //industrySubcategory.DataSource = SitecoreContext.GetItem<IModelBase>(ItemIds.IndustrySubcategories).GetChildren<ICategory>();
            categories.DataSource = Model.GetChildren<ICategory>();
            categories.DataBind();
            
        }
        public IEnumerable<IModelBase> GetSubcategories(ICategory category)
        {            
            return category.SelectSubcategories;            
        }
    }
}