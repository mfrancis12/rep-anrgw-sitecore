﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
    using System;
    using System.Web.UI.HtmlControls;
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;  
    using Glass.Mapper.Sc;
    using System.Collections.Generic;
    using Sitecore.SharedSource.FieldSuite.Types;
    using Sitecore.Data.Items;
    using System.Web.UI.WebControls;

    public partial class CategoryWithLinks : GlassUserControl<ICategoryWithLinks>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            Categories.DataSource = Model.GetChildren<ICategoryWithLinks>();
            Categories.DataBind();
            BindLinks();
        }

        public void BindLinks()
        {
            if (SitecoreContext.GetCurrentItem<ICategoryWithLinks>().CategoryLinks != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(SitecoreContext.GetCurrentItem<Item>(), "CategoryLinks");
                Repeater Categorylinks = (Repeater)Categories.FindControl("CategoryLinks");
                Categorylinks.DataSource = relatedGeneralLinks.LinkItems;
                Categorylinks.DataBind();
            }
            
        }

        protected void CategoryLinks_ItemDataBound1(object sender, RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("CategoryLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }
        
    }
}