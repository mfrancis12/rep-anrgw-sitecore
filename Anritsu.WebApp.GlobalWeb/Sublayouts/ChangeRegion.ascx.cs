﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ChangeRegion : GlassUserControl<IStaticPage>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var currentCountry = "US";
                var query = string.Format("{0}/*[@CountryCode='{1}']", SitecoreContext.GetItem<Item>(ItemIds.ISOCountries).Paths.FullPath,currentCountry);
                var currentCountryItem = SitecoreContext.QuerySingle<IISOCountry>(query);
                BindRegions();
                var currentRegion = ddlSelectRegion.Items.FindByValue(currentCountryItem.GeographicRegion.Id.ToString());
                if (currentRegion != null)
                {
                    ddlSelectRegion.SelectedIndex = ddlSelectRegion.Items.IndexOf(currentRegion);
                }
                BindCountries(currentCountryItem.GeographicRegion.Id.ToString());
            }
        }

        private void BindRegions()
        {
            ddlSelectRegion.DataSource = SitecoreContext.GetItem<IModelBase>(ItemIds.GlobalRegions).GetChildren<IDropListItem>();
            ddlSelectRegion.DataTextField = "Value";
            ddlSelectRegion.DataValueField = "Id";
            ddlSelectRegion.DataBind();

        }

        protected void ddlSelectRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCountries(ddlSelectRegion.SelectedValue);

        }

        private string GetCultureCode(object item, string field)
        {
            if (item != null)
            {
                Item referenceItem = ((ReferenceField)((Item)item).Fields[field]).TargetItem;
                return referenceItem != null ? referenceItem["Value"] : string.Empty;
            }
            return string.Empty;
        }

        private void BindCountries(string regionId)
        {
            var query = string.Format("{0}/*[@GeographicRegion='{{{1}}}']", SitecoreContext.GetItem<Item>(ItemIds.ISOCountries).Paths.FullPath, regionId.ToUpperInvariant());
            var countries = SitecoreContext.Query<IISOCountry>(query);
            SelectCountries.DataSource = countries;
            SelectCountries.DataBind();
        }

        public string GetUrl(string item)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
            {
                return Request.QueryString["ReturnUrl"] + "?id=" + item;
            }
            else
            {
                return "/" + GetCultureCode(SitecoreContext.GetItem<Item>(item), "CultureGroup");
            }
        }
    }
}