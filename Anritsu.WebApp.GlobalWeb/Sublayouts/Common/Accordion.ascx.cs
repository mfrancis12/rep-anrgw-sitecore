﻿using System;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Web.Ui;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Common
{
    public partial class Accordion : GlassUserControl<IModelBase>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            RepeaterAccordion.DataSource = Model.GetChildren<IAccordionPanel>();
            RepeaterAccordion.DataBind();
        }
    }
}