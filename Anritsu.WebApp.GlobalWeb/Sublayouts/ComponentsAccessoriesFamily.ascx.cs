﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
    using System;
    using System.Web.UI.HtmlControls;
    using System.Linq;
    using Sitecore.SharedSource.FieldSuite.Types;
    using Sitecore.Data.Items;

    public partial class ComponentsAccessoriesFamily : GlassUserControl<IComponentsAccessoriesFamily>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            var products =Model.Products.Where(x => !x.SelectProduct.IsDiscontinued);
            Products.DataSource = products;
            Products.DataBind();

            BindOptionalLinks();

            BindRelatedLinks();
        }

        protected void optionalLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }

        protected void relatedLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("RelatedLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }

        public void BindOptionalLinks()
        {
            IComponentsAccessoriesFamily currentItem = SitecoreContext.GetItem<IComponentsAccessoriesFamily>(SitecoreContext.GetCurrentItem<Item>().ID.ToString());
            if (currentItem.OptionalLinks != null)
            {
                GeneralLinks optionalGeneralLinks = new GeneralLinks(SitecoreContext.Database.GetItem(currentItem.Id.ToString()), "OptionalLinks");
                optionalLinks.DataSource = optionalGeneralLinks.LinkItems;
                optionalLinks.DataBind();
            }
        }

        public void BindRelatedLinks()
        {
            IComponentsAccessoriesFamily currentItem = SitecoreContext.GetItem<IComponentsAccessoriesFamily>(SitecoreContext.GetCurrentItem<Item>().ID.ToString());
            if (currentItem.RelatedLinks != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(SitecoreContext.Database.GetItem(currentItem.Id.ToString()), "RelatedLinks");
                relatedLinks.DataSource = relatedGeneralLinks.LinkItems;
                relatedLinks.DataBind();
            }
        }
    }
}