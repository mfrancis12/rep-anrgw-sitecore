﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ComponentsAccessoriesLanding : GlassUserControl<IProductSubcategory>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            
            if (Model == null)
                return;
            ComponentsAndAccessories.DataSource = Model.GetChildren<IComponentsAccessoriesFamily>().Where(x=>x.TemplateId.ToString().Equals(TemplateIds.ComponenetsAccessories));
            ComponentsAndAccessories.DataBind();

        }
        protected string GetLink(IModelBase item)
        {
            if (!SitecoreContext.GetItem<Item>(item.Id.ToString()).ID.ToString().Equals(ItemIds.ComponentAndAccessoriesDiscontinuedModel)) return item.Url;
            var firstOrDefault = item.GetChildren<IStaticPageWithContentRegional>().FirstOrDefault();
            return firstOrDefault != null ? firstOrDefault.Url : item.Url;
        }
    }
}