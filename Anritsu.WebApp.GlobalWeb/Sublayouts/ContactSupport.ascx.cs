﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ContactSupport : GlassUserControl<IContactSupport>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.TechmacContactUs) && !SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.TohokuAnritsuContactUs) && !SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.PartnerSolutionsContactUs) && !SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.AnritsuEngineeringContactUs) && !SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.CustomerSupportContactUs) && !SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.AnritsukousanContactUs) && !SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.AnritsuNetworksContactUs) && !SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.NetworkSolutionsContactUs))
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    labelCountryName.Text = GetSelectedRegionAndCountryName(Request.QueryString["id"]);
                }
                else
                {
                    labelCountryName.Text = GetSelectedRegionAndCountryName(GetCountryItemIdByCulture());
                }
            }
            else
            {
                 region.Visible = false;
            }
        }

        private string GetSelectedRegionAndCountryName(string cultureItemId)
        {
            var cultureItem = SitecoreContext.GetItem<IISOCountry>(cultureItemId);
            return cultureItem.GeographicRegion != null ? cultureItem.GeographicRegion.Value + ": " + cultureItem.CountryName : "";        
        }

        private string GetCountryItemIdByCulture()
        {
            string culture = Sitecore.Context.Language.Name;
            var usItem = SitecoreContext.GetItem<IModelBase>(ItemIds.UnitedStates);
            if (string.IsNullOrEmpty(culture) || culture.IndexOf('-') <= 0)
            {
                return usItem.Id.ToString();
            }
            var cultureCode = culture.Substring(3, 2);
            var countryItem =
                SitecoreContext
                    .GetItem<IModelBase>(ItemIds.ISOCountries)
                    .GetChildren<IISOCountry>()
                    .FirstOrDefault(y => y.CountryCode.Equals(cultureCode));
            return countryItem != null ? countryItem.Id.ToString() : usItem.Id.ToString();
        }
    }
}