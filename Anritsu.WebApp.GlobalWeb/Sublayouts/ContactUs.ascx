﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ContactUs" %>
<div class="full-container container">
    <div class="products-heading">
        <div class="products-title">
            <h1><%=Editable(x=>x.PageTitle) %></h1>
        </div>
        <div class="products-description"><%=Editable(x=>x.Content) %></div>
    </div>
</div>
<div class="full-container">
    <div class="products-tech">
        <asp:Repeater ID="ContactBlocks" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IContactBlock">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <h3 class="title"><%#Editable(Item,x=>x.Title) %></h3>
                        <div class="content">
                            <div class="text">
                                <div class="img">
                                    <%#RenderImage(Item,x=>x.Image,new {@class="img100"}) %>
                                </div>
                                <div>
                                    <p><%#Editable(Item,x=>x.Description) %></p>
                                </div>
                                <div class="read-more-link"><a href="javascript:void(0)"><b><%# RenderLink(Item, x=>x.Link, isEditable: true, contents:Editable(Item,x=>x.LinkTitle))%>&nbsp</b><i class="icon icon-arrow-blue"></i></a></div>
                            </div>
                            <div class="device">
                                <asp:Repeater ID="ContactLinks" runat="server" OnItemDataBound="ContactLinks_ItemDataBound" DataSource="<%#GetContactLinks(Item)%>">
                                    <ItemTemplate>
                                        <a id="ContactLink" runat="server"></a>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        
    </div>
</div>
