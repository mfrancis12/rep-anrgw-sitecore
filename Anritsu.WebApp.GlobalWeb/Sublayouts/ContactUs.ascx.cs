﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ContactUs : GlassUserControl<IStaticText>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindContactBlocks();           
        }
        private void BindContactBlocks()
        {
            ContactBlocks.DataSource = SitecoreContext.GetItem<Item>(ItemIds.ContactUsCategoriesFolder).GetChildren().Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<IContactBlock>(x.ID.ToString()));
            ContactBlocks.DataBind();
        }
        public List<GeneralLinkItem> GetContactLinks(IModelBase item)
        {
            var contactLinks = new List<GeneralLinkItem>();
            IContactBlock currentItem = SitecoreContext.GetItem<IContactBlock>(item.Id.ToString());
            if (currentItem.ContactLinks != null)
            {
                GeneralLinks contactGeneralLinks = new GeneralLinks(SitecoreContext.GetItem<Item>(currentItem.Id.ToString()), "ContactLinks");
                contactLinks = contactGeneralLinks.LinkItems;                
            }
            return contactLinks;
        }

        protected void ContactLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor contactAnchor = (HtmlAnchor)e.Item.FindControl("ContactLink");
            contactAnchor.InnerText = linkItem.LinkText;
            contactAnchor.HRef = linkItem.Url;
            contactAnchor.Target = linkItem.Target;
        }
    }
}