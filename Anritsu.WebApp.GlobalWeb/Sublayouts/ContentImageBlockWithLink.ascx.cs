﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using System;

    public partial class BuyHome : GlassUserControl<IModelBase>
    {
        protected string HasLink { get; set; }
        private void Page_Load(object sender, EventArgs e)
        {
            BuyContentBlocks.DataSource = Model.GetChildren<IContentImageBlockWithLink>();
            BuyContentBlocks.DataBind();
        }
        protected string GetLink(IContentImageBlockWithLink item)
        {
            if (item.Link != null && !string.IsNullOrEmpty(item.Link.Url))
            {
                HasLink = item.Link.Url;
            }
            else
            {
                HasLink = "javascript:void(0);";
            }
            return HasLink;

        }
        protected string GetTitleLinkClass(IContentImageBlockWithLink item)
        {
            if (item.Link != null && !string.IsNullOrEmpty(item.Link.Url))
            {
                return string.Empty;
            }
            else
            {
                return "no-link";
            }
        }
    }
}