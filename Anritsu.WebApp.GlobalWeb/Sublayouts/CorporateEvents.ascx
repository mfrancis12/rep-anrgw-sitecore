﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CorporateEvents.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.CorporateEvents" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="container">
    <div class="content-form">
        <div class="content-detail">
            <h1><%= Editable(x=>x.Title) %></h1>
            <p>
                 <% if ( string.IsNullOrEmpty(Editable(x=>x.Location)))
               {%>
                <%= Editable(x=>x.StartDate,x=>x.StartDate.ToLocalTime().ToFormattedDate()) %> -  <%= Editable(x=>x.EndDate,x=>x.EndDate.ToLocalTime().ToFormattedDate()) %> 
                 <% } %>
                 <% else 
                    {  %>
                <%= Editable(x=>x.StartDate,x=>x.StartDate.ToLocalTime().ToFormattedDate()) %> -  <%= Editable(x=>x.EndDate,x=>x.EndDate.ToLocalTime().ToFormattedDate()) %> ,   <%= Editable(x=>x.Location) %>
                <% } %>
            </p>

        
            <div class="faq">


                <p><%= Editable(x=>x.EventBodyText) %></p>
              <% if (Model.AdditionalLink != null)
                   {%>
                <%= RenderLink(x=>x.AdditionalLink, isEditable: true, contents:Editable(x=>x.AdditionalTitle))%>
                 <% } %>
            </div>
        </div>

    </div>
</div>