﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class DiscontinuedModels : GlassUserControl<IStaticPageWithContentRegional>
    {
        string indexName = "anritsu_products";
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            Search.Text = Translate.TextByDomain("GlobalDictionary", "Search");
            BindOptionalLinks();

        }
        private void BindData(string searchText)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database.Name);
            var regionalProducts = new List<ProductSearchItem>();
            using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
            {
                var query = context.GetQueryable<ProductSearchItem>().Filter(i => i.Language == Sitecore.Context.Language.Name && i.IsDiscontinued && i.ModelNumber.Contains(searchText));
                foreach (var item in query)
                {
                    sitecoreService.Map(item);
                    regionalProducts.Add(item);
                }
                regionalProducts = regionalProducts.Where(x => x != null &&SitecoreContext.GetItem<Item>(x.Path).Axes.IsDescendantOf(SitecoreContext.GetItem<Item>(Model.Id.ToString()))).ToList();
                DiscontinuedModelsList.DataSource = regionalProducts;
                DiscontinuedModelsList.DataBind();
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            DiscontinuedModelsDataPager.SetPageProperties(0, DiscontinuedModelsDataPager.PageSize, false);
            BindData(SearchBox.Text);
        }
        protected void DiscontinuedModelsList_PagePropertiesChanged(object sender, EventArgs e)
        {
            BindData(SearchBox.Text);
        }
        protected void OptionalLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
            if (linkItem != null && relatedAnchor != null)
            {
                relatedAnchor.InnerText = linkItem.LinkText;
                relatedAnchor.HRef = linkItem.Url;
                relatedAnchor.Target = linkItem.Target;
            }
        }

        protected void OptionalLinks_PreRender(object sender, EventArgs e)
        {
            if (OptionalLinks.Items.Count == 0)
            {
                OptionalLinks.Visible = false;
            }
        }
        public void BindOptionalLinks()
        {          
            GeneralLinks optionalLinks = new GeneralLinks(SitecoreContext.GetCurrentItem<Item>(), "OptionalLinks");
            OptionalLinks.DataSource = optionalLinks.LinkItems;
            OptionalLinks.DataBind();           
        }
    }
}