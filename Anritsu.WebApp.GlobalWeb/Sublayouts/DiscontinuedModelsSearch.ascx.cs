﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data.Items;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class DiscontinuedModelsSearch : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database.Name);
            var regionalProducts = new List<ProductSearchItem>();
            string indexName = "anritsu_products";
           
            using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
            {
                var query = context.GetQueryable<ProductSearchItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(i => i.Language == Sitecore.Context.Language.Name 
                    && i.IsDiscontinued);
                foreach (var item in query)
                {
                    sitecoreService.Map(item);
                    regionalProducts.Add(item);
                }
            }

            foreach (var item in regionalProducts)
            {
                //var position = item.Path.LastIndexOf("/", StringComparison.Ordinal) + 1; 
                Response.Write(string.Format("<a href='{0}' style='{2}'>{1}</a>", "/" + Sitecore.Context.Language.Name + "/test-measurement/products/" +
                    item.Name, item.Name, "font-weight:bold") + "<br/>");
            }
        }
    }
}