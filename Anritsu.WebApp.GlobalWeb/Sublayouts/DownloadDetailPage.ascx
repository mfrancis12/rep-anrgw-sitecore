﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DownloadDetailPage.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.DownloadDetailPage" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="container">
     <script type="text/javascript" language="javascript">
         var wsURL = "<%=DownloadLogUrl%>";
         var parameters = "<%= DownloadLogData %>";
         function LogDwlData()
         {
             //alert(parameters);
             var flag = document.getElementById('<%=hfTrackDownload.ClientID%>');
             //if not sign in 
             if (flag.value !== '1') return;
             $.ajax({
                 type: "POST",
                 url: wsURL,
                 data: parameters,
                 dataType: "text",
                 success: function (msg) { },
                 error: function (e) { alert("An Error Occured: " + e.statusText); }
             });
         }
     </script>
    <div class="content-form">
        <div class="content-detail download-detail">
            <% if (DownloadItem != null)
               { %>
            <h1><%= Editable(DownloadItem, x => x.Title) %></h1>
            <p><%= Editable(DownloadItem, x => x.Description.GetHtmlDecodedString()) %></p>
            <h2 class="faqTitle"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "filedetails") %></h2>
            <table id="downloadtbl">
                <tbody>
                    <tr>
                        <th scope="row"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "filename") %>:
                        </th>
                        <td><%= Editable(DownloadItem, x => x.FilePath, x => GetFileName(x.FilePath)) %>
                        </td>
                    </tr>
                    <tr class="fileBg">
                        <th scope="row"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "filesize") %>:
                        </th>
                        <td><%= Editable(DownloadItem, x => x.Size) %>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "filetype") %>:
                        </th>
                        <td><%= Editable(DownloadItem, x => x.GetDownloadCategory()) %>
                        </td>
                    </tr>
                    <tr class="fileBg">
                        <th scope="row"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "version") %>:
                        </th>
                        <td><%= Editable(DownloadItem, x => x.Version) %>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "releasedate") %>:
                        </th>
                        <td><%= Editable(DownloadItem,x=>x.ReleaseDate,x=>x.ReleaseDate.ToFormattedDate()) %>
                        </td>
                    </tr>
                </tbody>
            </table>
            <% if (DownloadItem != null && DownloadItem.RedirectToMyAnritsu)
               { %>
            <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "logintomyanritsu") %><br />
            <asp:Button ID="MyAnritsuLogOn" runat="server" CssClass="button" OnClick="MyAnritsuLogOn_OnClick" />
            <% } %>
            <%
               else
               { %>
            <asp:HyperLink runat="server" ID="hLinkDownload" onclick="LogDwlData()" CssClass="button color-white" Target="_blank"></asp:HyperLink>
            <% } %>
            <% } %>
            <asp:HiddenField runat="server" ID="hfTrackDownload" Value="0"/>
        </div>
        <div class="content-aside <%= HideOptionalLinks %>">
            <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
            <asp:Repeater ID="DownloadOptionalLinks" runat="server" OnItemDataBound="DownloadOptionalLinks_OnItemDataBound">
                <ItemTemplate>
                    <p><a id="otherLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
