﻿using System;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using SessionVariables = Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SessionVariables;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using System.Collections.Generic;
using Microsoft.SqlServer.Server;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class DownloadDetailPage : GlassUserControl<DownloadSearchItem>
    {

        private IProviderSearchContext _downloadcontext;
        protected IProviderSearchContext DownloadSearchContext
        {
            get
            {
                return _downloadcontext ?? (_downloadcontext = ContentSearchManager.GetIndex(StaticVariables.DownloadsIndex).CreateSearchContext());
            }
        }

        private DownloadSearchItem _downloadItem;

        protected DownloadSearchItem DownloadItem
        {
            get
            {
                try
                {
                    if (_downloadItem != null) return _downloadItem;

                    var sitecoreService = new SitecoreService(Sitecore.Context.Database);

                    var query = DownloadSearchContext.GetQueryable<DownloadSearchItem>()
                        .Filter(x => x.Language.Equals(Sitecore.Context.Language.Name));

                    object download = null;

                    foreach (var item in query)
                    {
                        if (!item.Name.Equals(Request.Url.Segments[Request.Url.Segments.Length - 1], StringComparison.OrdinalIgnoreCase)) continue;
                        sitecoreService.Map(item);
                        download = item;
                        break;
                    }

                    if (download != null)
                    {
                        _downloadItem = (DownloadSearchItem)download;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message, GetType());
                }
                return _downloadItem;
            }
            set { _downloadItem = value; }
        }

        private string DownloadFilePath
        {
            get
            {
                return IsExternalUrl(DownloadItem.FilePath)
                        ? string.Empty
                        : "//" + ConfigurationManager.AppSettings["DownloadDistributionList"] + "/" + DownloadItem.FilePath;
            }
        }

        public string DownloadLogUrl
        {
            get
            {
               return string.Format("{0}://{1}/{2}", Request.Url.Scheme, Request.Url.Host, "api/Downloads/LogDownloadRequest");
            }
        }

        public string DownloadLogData
        {
            get
            {
                var downloadLogInfo = GetDownloadLogInfo(DownloadItem, DownloadFilePath);
                var dwlInputParams = new StringBuilder();
                dwlInputParams.Append("GWUserId=" + downloadLogInfo.GwUserId);
                dwlInputParams.Append("&UserIPAddress=" + downloadLogInfo.UserIpAddress);
                dwlInputParams.Append("&DownloadPageUrl=" + downloadLogInfo.DownloadPageUrl);
                dwlInputParams.Append("&DownloadFilePath=" + downloadLogInfo.DownloadFilePath);
                dwlInputParams.Append("&UrlReferrer=" + downloadLogInfo.UrlReferrer);
                dwlInputParams.Append("&UserPrimaryLlcc=" + downloadLogInfo.UserPrimaryLlcc);
                dwlInputParams.Append("&UserCountryCode=" + downloadLogInfo.UserCountryCode);
                dwlInputParams.Append("&GWRegion=" + downloadLogInfo.GwRegion);
                dwlInputParams.Append("&BrowserAgent=" + downloadLogInfo.BrowserAgent);
                dwlInputParams.Append("&IsLoginRequired=" + downloadLogInfo.IsLoginRequired);
                return dwlInputParams.ToString();
            }
        }


        protected string HideOptionalLinks = string.Empty;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                //Adding <title>
                if (DownloadItem == null) return;
                var phTitle = (PlaceHolder)Parent.FindControl("TitlePlaceHolder");
                if (phTitle == null) return;

                var htmlTitle = new HtmlTitle
                {
                    Text = DownloadItem.Title + "- " +
                           Translate.TextByDomain("GlobalDictionary", "anritsucompany")
                };
                phTitle.Controls.Add(htmlTitle);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {

            if (DownloadItem == null) throw new HttpException(404, "Page not found");
            if (DownloadItem.DownloadCategory != null && !GsaHelpers.IsGsaCrawler()
                && !DownloadItem.DownloadCategory.Equals(StaticVariables.DriversSoftwareDownloads,
                    StringComparison.OrdinalIgnoreCase))
            {
                //throw new HttpException(404, "Page not found");
                var downloadFileUrl = IsExternalUrl(DownloadItem.FilePath)
                    ? string.Empty
                    : "//" + ConfigurationManager.AppSettings["DownloadDistributionList"] + "/" + DownloadItem.FilePath;

                if (DownloadItem.RedirectToMyAnritsu)
                {
                    Response.Redirect(Settings.GetSetting("RedirectToMyAnritsu"));
                }
                else if (DownloadItem.LogOnRequired)
                {
                    if (Context.User.Identity.IsAuthenticated)
                    {
                        if (Session[SessionVariables.UserLoggedIn] != null)
                        {
                            //log dwl request-1 with user info
                            var downloadLogInfo = GetDownloadLogInfo(DownloadItem, downloadFileUrl);
                            BllDownloads.Download_AddLog(downloadLogInfo);
                            Response.Redirect(downloadFileUrl);
                        }
                        else
                        {
                            RedirectToLogin(HttpContext.Current.Request.Url.AbsolutePath);
                            //Response.Redirect("/Sign-In.aspx?" + QSKeys.ReturnURL + "=" + HttpContext.Current.Request.Url.AbsolutePath);
                        }
                    }
                    else
                    {
                        RedirectToLogin(HttpContext.Current.Request.Url.AbsolutePath);
                        //Response.Redirect("/Sign-In.aspx?" + QSKeys.ReturnURL + "=" + HttpContext.Current.Request.Url.AbsolutePath);
                    }
                }
                else
                {
                    //log dwl request-1 with out user info
                    var downloadLogInfo = GetDownloadLogInfo(DownloadItem, downloadFileUrl);
                    BllDownloads.Download_AddLog(downloadLogInfo);
                    Response.Redirect(downloadFileUrl);
                }
            }

            BindDownloadOptionalLinks();

            MyAnritsuLogOn.Text = Translate.TextByDomain("GlobalDictionary", "register");
            hLinkDownload.Text = Translate.TextByDomain("GlobalDictionary", "download");

            var bucketPath = IsExternalUrl(DownloadItem.FilePath) ? string.Empty : "//" + ConfigurationManager.AppSettings["DownloadDistributionList"] + "/";

            if (DownloadItem.RedirectToMyAnritsu)
            {
                hLinkDownload.NavigateUrl = Settings.GetSetting("RedirectToMyAnritsu");
            }
            else if (DownloadItem.LogOnRequired)
            {
                if (Context.User.Identity.IsAuthenticated)
                {
                    if (Session[SessionVariables.UserLoggedIn] != null)
                    {
                        hLinkDownload.NavigateUrl = bucketPath + DownloadItem.FilePath;
                        hfTrackDownload.Value = "1";
                    }
                    else
                    {
                        hLinkDownload.NavigateUrl = "/Sign-In.aspx?" + QSKeys.ReturnURL + "=" +
                                                    GetItemUrl(DownloadItem.Url);
                    }
                }
                else
                {
                    hLinkDownload.NavigateUrl = "/Sign-In.aspx?" + QSKeys.ReturnURL + "=" +
                                      GetItemUrl(DownloadItem.Url);
                }
            }
            else
            {
                hLinkDownload.NavigateUrl = bucketPath + DownloadItem.FilePath;
                hfTrackDownload.Value = "1";
            }

            if (GsaHelpers.IsGsaCrawler())
                AddGsaMetaInfo();
        }

        private DownloadLog GetDownloadLogInfo(DownloadSearchItem dwlItem, string downloadFileUrl)
        {
            if (dwlItem == null) return null;
            var dwlLog = new DownloadLog();
            dwlLog.DownloadPageUrl = this.Request.Url.AbsolutePath;
            dwlLog.DownloadFilePath = downloadFileUrl;
            if (Request.UrlReferrer != null) dwlLog.UrlReferrer = Request.UrlReferrer.ToString();
            dwlLog.BrowserAgent = HttpContext.Current.Request.UserAgent;
            dwlLog.IsLoginRequired = dwlItem.LogOnRequired;
            if (Context.User.Identity.IsAuthenticated)
            {
                var user = BasePage.LoggedInUser();
                if (user != null)
                {
                    dwlLog.GwUserId = user.UserId;
                    dwlLog.UserPrimaryLlcc = user.PrimaryCultureCode;
                    dwlLog.UserCountryCode = user.CountryCode;
                }
            }
            dwlLog.GwRegion = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            dwlLog.UserIpAddress = GetVisitorIdpAddress();
            return dwlLog;
        }

        private string GetVisitorIdpAddress()
        {
            var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
            if (string.IsNullOrEmpty(ipAddress))
            {
                ipAddress = HttpContext.Current.Request.UserHostAddress;
            }
            return ipAddress;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.IndexOf(System.String,System.StringComparison)")]
        private void AddGsaMetaInfo()
        {
            try
            {
                var ltMetaTags = (PlaceHolder)Parent.FindControl("GsaMetaPlaceHolder");
                if (ltMetaTags == null) return;
                var downloadItem = Sitecore.Context.Database.GetItem(DownloadItem.ItemId);
                var meta = new HtmlMeta
                {
                    Name = "title",
                    Content = SiteSearchItemName.GetGsaItemName(downloadItem)
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "gsadescription",
                    Content = SiteSearchMetaDescription.GetGsaItemMetaDescription(downloadItem)
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                var area = SiteSearchArea.GetGsaArea(downloadItem);
                meta = new HtmlMeta
                {
                    Name = "area",
                    Content = area
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                var coveoarea = SiteSearchArea.GetGsaArea(downloadItem, Language.Parse("en"));
                meta = new HtmlMeta
                {
                    Name = "coveoarea",
                    Content = coveoarea
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);

                //meta = new HtmlMeta
                //{
                //    Name = "downloadURL",
                //    Content = HttpContext.Current.Request.Url.AbsoluteUri
                //        //DownloadItem.LogOnRequired 
                //        //        ? "/Sign-In.aspx?" + QSKeys.ReturnURL + "=" + HttpContext.Current.Request.Url.AbsoluteUri 
                //        //        : HttpContext.Current.Request.Url.AbsoluteUri
                //        //DownloadItem.FilePath != null
                //        //    ? string.Format("//{0}/{1}", ConfigurationManager.AppSettings["DownloadDistributionList"]
                //        //        , DownloadItem.FilePath)
                //        //    : ""
                //};
                //ltMetaTags.Controls.Add(meta);
                //meta = new HtmlMeta
                //{
                //    Name = "isLoginRequired",
                //    Content =
                //        DownloadItem.LogOnRequired != null
                //            ? DownloadItem.LogOnRequired.ToString() == "1" ? "true" : "false"
                //            : ""
                //};
                //if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "metaFileExt",
                    Content =
                        DownloadItem.FileType ?? ""
                };
                ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "metaFileSize",
                    Content =
                        DownloadItem.Size ?? ""
                };
                ltMetaTags.Controls.Add(meta);
                var downloadReleaseDate = DownloadItem.ReleaseDate;
                meta = new HtmlMeta
                {
                    Name = "publication_date",
                    Content = downloadReleaseDate.ToString("yyyy-MM-dd")
                };
                ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "metaReleaseDate",
                    //Content = downloadReleaseDate.ToString("MM/dd/yyyy")
                    Content = downloadReleaseDate.Date.ToFormattedDate()
                };
                ltMetaTags.Controls.Add(meta);

                //Adding download release date with 'last-modified' meta tag for GSA sorting purpose
                meta = new HtmlMeta
                {
                    Name = "last-modified",
                    Content = downloadReleaseDate.ToString("yyyy-MM-dd")
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "Version",
                    Content = DownloadItem.Version
                };
                ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "IsNewVersionAvailable",
                    Content = DownloadItem.IsNewVersionAvailable != null
                            ? DownloadItem.IsNewVersionAvailable.ToString() == "1" ? "true" : "false"
                            : ""
                };
                ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "keywords",
                    Content = DownloadItem.Keyword
                };
                ltMetaTags.Controls.Add(meta);

                //add Registration required url to the GSA meta
                if (DownloadItem.RedirectToMyAnritsu)
                {
                    meta = new HtmlMeta
                    {
                        Name = "registrationRequired",
                        Content = Settings.GetSetting("RedirectToMyAnritsu")
                    };
                    ltMetaTags.Controls.Add(meta);
                }

                var categories = SiteSearchItemCategory.GetGsaItemCategory(downloadItem);
                foreach (var category in categories)
                {
                    meta = new HtmlMeta
                    {
                        Name = "category",
                        Content = !string.IsNullOrWhiteSpace(category) ? area.Substring(0, 2) + GsaMetaStringSeperators.AreaCategorySeparator + category : string.Empty
                    };
                    if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                }

                var subcategories = SiteSearchItemSubcategory.GetGsaItemSubcategory(downloadItem);
                foreach (var subcategory in subcategories)
                {
                    var subCatgSeparateIndex = subcategory.IndexOf(GsaMetaStringSeperators.CategorySubCategorySeparator, StringComparison.InvariantCultureIgnoreCase);
                    var metaRegSeparateIndex = subcategory.IndexOf(GsaMetaStringSeperators.SubCategoryMetaRegSeparator, StringComparison.InvariantCultureIgnoreCase);

                    meta = new HtmlMeta
                    {
                        Name = "subcategory",
                        Content = !string.IsNullOrWhiteSpace(subcategory) ? area.Substring(0, 2) + GsaMetaStringSeperators.AreaCategorySeparator
                                        + subcategory.Substring(0, metaRegSeparateIndex) : string.Empty
                    };
                    if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                    meta = new HtmlMeta
                    {
                        Name = "metaRegDownloadType",
                        Content = !string.IsNullOrWhiteSpace(subcategory) ? subcategory.Substring(subCatgSeparateIndex + GsaMetaStringSeperators.CategorySubCategorySeparator.Length,
                                        metaRegSeparateIndex - subCatgSeparateIndex - GsaMetaStringSeperators.SubCategoryMetaRegSeparator.Length) : string.Empty
                    };
                    if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                    meta = new HtmlMeta
                    {
                        Name = "metaDownloadType",
                        Content = !string.IsNullOrWhiteSpace(subcategory) ? subcategory.Substring(metaRegSeparateIndex
                                        + GsaMetaStringSeperators.SubCategoryMetaRegSeparator.Length) : string.Empty // this should not be localized content
                    };
                    if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                   
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " " + ex.StackTrace, "GsaMetaInfoAddition");
            }
        }

        protected string GetContentLength(string value)
        {
            if (!string.IsNullOrEmpty(value) && Convert.ToInt32(value) >= 0)
            {
                int size = Convert.ToInt32(value);
                string[] sizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

                if (size == 0)
                {
                    return "0.0 bytes";
                }

                var mag = (int)Math.Log(size, 1024);
                var adjustedSize = (decimal)size / (1L << (mag * 10));

                return string.Format("{0:n1} {1}", adjustedSize, sizeSuffixes[mag]);
            }
            return string.Empty;
        }

        protected string GetFileName(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Substring(value.LastIndexOf('/') + 1);
            }
            return string.Empty;
        }

        private void BindDownloadOptionalLinks()
        {
            if (DownloadItem == null) return;
            var currentItem = SitecoreContext.GetItem<Item>(DownloadItem.ItemId.ToString());
            if (currentItem.Fields["OptionalLinks"] != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(currentItem, "OptionalLinks");
                DownloadOptionalLinks.DataSource = relatedGeneralLinks.LinkItems;
                DownloadOptionalLinks.DataBind();
            }
            HideOptionalLinks = DownloadOptionalLinks.Items.Count > 0 ? string.Empty : "hiddentxt";
        }

        protected void DownloadOptionalLinks_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("otherLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }

        protected void MyAnritsuLogOn_OnClick(object sender, EventArgs e)
        {
            Response.Redirect(Settings.GetSetting("RedirectToMyAnritsu"));
        }

        protected string GetItemUrl(string itemPath)
        {
            var position = itemPath.LastIndexOf("/", StringComparison.Ordinal) + 1;
            return string.Format("/" + Sitecore.Context.Language.Name + "/test-measurement/support/downloads/software/{0}", itemPath.Substring(position, itemPath.Length - position));
        }

        public static bool IsExternalUrl(string source)
        {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
        }

        protected void RedirectToLogin(string replyUrl)
        {
            Response.Redirect("/Sign-In.aspx?" + QSKeys.ReturnURL + "=" + replyUrl);
        }
    }
}