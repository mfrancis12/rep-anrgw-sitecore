﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using Glass.Mapper.Sc;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
    public partial class DownloadSearch : UserControl
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected void Page_Load(object sender, EventArgs e)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var downloadList = new List<DownloadSearchItem>();

            var query = DownloadSearchContext.GetQueryable<DownloadSearchItem>().Filter(x => x.Language.Equals(Sitecore.Context.Language.Name));

            foreach (var item in query)
            {
                sitecoreService.Map(item);
                downloadList.Add(item);
            }
            downloadList = downloadList.Where(x => x != null && x.Subcategory != null && 
                !x.Subcategory.Key.Equals(StaticVariables.DriversSoftwareDownloads)).OrderByDescending(x => x.ReleaseDate).ToList();
            foreach (var item in downloadList)
            {
                var position = item.Path.LastIndexOf("/", StringComparison.Ordinal) + 1;
                var categoryItem = Sitecore.Context.Database.GetItem(item.DownloadCategoryId).Parent;

                Response.Write(string.Format("<a href='{0}' style='{2}'>{1}</a>", "/" + Sitecore.Context.Language.Name +
                                   string.Format("/test-measurement/support/downloads/{0}/{1}",
                                   categoryItem != null ? 
                                    SiteSearchHelper.GetSitecoreFallbackItem(categoryItem, categoryItem.Fields["Key"]).Fields["Key"].Value : "download-category",
                                     item.Path.Substring(position, item.Path.Length - position))
                                       , item.Title, "font-weight:bold") + "<br/>");
            }
        }

        private IProviderSearchContext _downloadcontext;
        protected IProviderSearchContext DownloadSearchContext
        {
            get
            {
                return _downloadcontext ?? (_downloadcontext = ContentSearchManager.GetIndex(StaticVariables.DownloadsIndex).CreateSearchContext());
            }
        }
    }
}