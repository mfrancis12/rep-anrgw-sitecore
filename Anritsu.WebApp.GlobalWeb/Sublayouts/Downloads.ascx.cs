﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Search;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using FacetValue = Sitecore.ContentSearch.Linq.FacetValue;
using SearchFacetValue = Anritsu.WebApp.GlobalWeb.Search.FacetValue;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Downloads : GlassUserControl<IProductRegional>
    {
        #region Properties

        public string SelectedDownloadCategory
        {
            get
            {
                object o = ViewState["SelectedDownloadCategory"];
                if (o == null)
                {
                    return string.Empty;
                }
                return (string)o;
            }
            set { ViewState["SelectedDownloadCategory"] = value; }
        }

        public string SelectedQuestionCategory
        {
            get
            {
                object o = ViewState["SelectedQuestionCategory"];
                if (o == null)
                {
                    return string.Empty;
                }
                return (string)o;
            }
            set { ViewState["SelectedQuestionCategory"] = value; }
        }

        public string SelectedQuestionSubcategory
        {
            get
            {
                object o = ViewState["SelectedQuestionSubcategory"];
                if (o == null)
                {
                    return string.Empty;
                }
                return (string)o;
            }
            set { ViewState["SelectedQuestionSubcategory"] = value; }
        }

        public string SelectedQuestionFamily
        {
            get
            {
                object o = ViewState["SelectedQuestionFamily"];
                if (o == null)
                {
                    return string.Empty;
                }
                return (string)o;
            }
            set { ViewState["SelectedQuestionFamily"] = value; }
        }

        private IProviderSearchContext _downloadcontext;
        protected IProviderSearchContext DownloadSearchContext
        {
            get
            {
                return _downloadcontext ?? (_downloadcontext = ContentSearchManager.GetIndex(StaticVariables.DownloadsIndex).CreateSearchContext());
            }
        }

        private IProviderSearchContext _frequentquestionscontext;
        protected IProviderSearchContext FrequentQuestionsSearchContext
        {
            get
            {
                return _frequentquestionscontext ?? (_frequentquestionscontext = ContentSearchManager.GetIndex(StaticVariables.FrequentQuestionsIndex).CreateSearchContext());
            }
        }

        public int DownloadLibraryTotalRecords
        {
            get
            {
                object o = ViewState["DownloadLibraryTotalRecords"];
                if (o == null)
                    return 0;
                return (int)o;
            }
            set { ViewState["DownloadLibraryTotalRecords"] = value; }
        }

        public int DownloadLibraryPageIndex
        {
            get
            {
                object o = ViewState["DownloadLibraryPageIndex"];
                if (o == null)
                    return 1;
                return (int)o;
            }
            set { ViewState["DownloadLibraryPageIndex"] = value; }
        }

        public int DownloadSoftwareTotalRecords
        {
            get
            {
                object o = ViewState["DownloadSoftwareTotalRecords"];
                if (o == null)
                    return 0;
                return (int)o;
            }
            set { ViewState["DownloadSoftwareTotalRecords"] = value; }
        }

        public int DownloadSoftwarePageIndex
        {
            get
            {
                object o = ViewState["DownloadSoftwarePageIndex"];
                if (o == null)
                    return 1;
                return (int)o;
            }
            set { ViewState["DownloadSoftwarePageIndex"] = value; }
        }

        public int FaqTotalRecords
        {
            get
            {
                object o = ViewState["FaqTotalRecords"];
                if (o == null)
                    return 0;
                return (int)o;
            }
            set { ViewState["FaqTotalRecords"] = value; }
        }

        public int FaqPageIndex
        {
            get
            {
                object o = ViewState["FaqPageIndex"];
                if (o == null)
                    return 1;
                return (int)o;
            }
            set { ViewState["FaqPageIndex"] = value; }
        }

        public int RecordsPerPage
        {
            get
            {
                object o = ViewState["RecordsPerPage"];
                if (o == null)
                    return 20;
                return (int)o;
            }
            set { ViewState["RecordsPerPage"] = value; }
        }

        string indexName = "anritsu_products";

        #endregion

        //protected void Page_PreRender(object sender, EventArgs e)
        //{
        //    Response.Redirect(
        //        string.Format(ConfigurationManager.AppSettings["SearchDownloadCenterUrl"], Sitecore.Context.Language));
        //}

        private void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DownloadLibraryPageIndex = 1;
                DownloadSoftwarePageIndex = 1;
                FaqPageIndex = 1;

                txtProductNumber.Attributes["placeholder"] = Translate.TextByDomain("GlobalDictionary", "entermodelnumber");
                if (!string.IsNullOrEmpty(Request.QueryString["category"]))
                {
                    BindDownloads(string.Empty, Request.QueryString["category"]);
                    BindDownloadSoftwares();
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["model"]))
                {
                    string modelNumber = Server.UrlDecode(Request.QueryString["model"]).Replace("<br>", "/");
                    txtProductNumber.Text = modelNumber;

                    //using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
                    //{
                    //    var query = context.GetQueryable<ProductSearchItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(i => i.Language == Sitecore.Context.Language.Name
                    //        && i.ModelNumber.Equals(txtProductNumber.Text, StringComparison.OrdinalIgnoreCase));
                    //    if (!query.Any())
                    //    {
                    //        //Response.Redirect(SitecoreContext.GetItem<IModelBase>(ItemIds.Search).Url + "?q=" + txtProductNumber.Text);
                    //        Response.Redirect(
                    //            string.Format(ConfigurationManager.AppSettings["SearchDownloadCenterUrl"],
                    //                Sitecore.Context.Language) + "?q=" + txtProductNumber.Text);
                    //    }
                    //}

                    BindDownloads(txtProductNumber.Text);
                    BindDownloadSoftwares(txtProductNumber.Text);
                    if (!Sitecore.Context.Language.Name.ToLower(CultureInfo.InvariantCulture).Equals("ja-jp"))
                    {
                        BindFaqs(txtProductNumber.Text);
                    }
                }
                else
                {
                    BindDownloads();
                    BindDownloadSoftwares();

                    if (!Sitecore.Context.Language.Name.ToLower(CultureInfo.InvariantCulture).Equals("ja-jp"))
                    {
                        BindFaqs();
                    }
                }
            }

            if (DownloadLibraryTotalRecords > 0)
            { CreateDownloadLibraryPagingControl(); }

            if (DownloadSoftwareTotalRecords > 0)
            { CreateDownloadSoftwarePagingControl(); }

            if (FaqTotalRecords > 0)
            { CreateFaqPagingControl(); }
        }

        #region Download Library

        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        private void BindDownloads(string productModelNumber = null, string downloadCategory = null, bool facet = true)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var downloadList = new List<DownloadSearchItem>();

            var query = DownloadSearchContext.GetQueryable<DownloadSearchItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(x => x.Language.Equals(Sitecore.Context.Language.Name));

            query = query.Where(x => !x.DownloadCategory.Equals(StaticVariables.DriversSoftwareDownloads));

            if (!string.IsNullOrEmpty(productModelNumber))
            {
                query = query.Where(x => x.ProductModelNumbers.Contains(txtProductNumber.Text));
            }

            if (!string.IsNullOrEmpty(downloadCategory))
            {
                query = query.Where(x => x.DownloadCategory.Equals(downloadCategory));
            }

            var searchResults = query.OrderByDescending(x => x.DownloadReleaseDate).Page(DownloadLibraryPageIndex - 1, RecordsPerPage).GetResults();
            DownloadLibraryTotalRecords = searchResults.TotalSearchResults;

            if (facet)
            {
                var categoryFacetResultsType =
                    query.FacetPivotOn((x => x.FacetOn(y => y.DownloadCategoryId))).GetFacets();

                var firstOrDefault = CollectFacets(categoryFacetResultsType.Categories).FirstOrDefault();
                if (firstOrDefault != null)
                {
                    var lstFacets = firstOrDefault.FacetValues.Where(x => !string.IsNullOrEmpty(GetCategoryKey(x.Value)) && !GetCategoryKey(x.Value).ToLower(CultureInfo.InvariantCulture).Equals(StaticVariables.DriversSoftwareDownloads)).OrderBy(x => x.DisplayName).ToList();
                    DownloadLibraryTypes.DataSource = lstFacets;
                    DownloadLibraryTypes.DataBind();
                }
            }

            foreach (var item in searchResults.Select(x => x.Document))
            {
                sitecoreService.Map(item);
                downloadList.Add(item);
            }

            if (DownloadLibraryTotalRecords > 0)
            {
                CreateDownloadLibraryPagingControl();
            }

            DownloadLibraryList.DataSource = downloadList.OrderByDescending(x => x.DownloadReleaseDate).ToList();
            DownloadLibraryList.DataBind();
        }

        #endregion

        #region Download Software

        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        private void BindDownloadSoftwares(string productModelNumber = null, bool facet = true)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var downloadList = new List<DownloadSearchItem>();

            var query = DownloadSearchContext.GetQueryable<DownloadSearchItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(i => i.Language == Sitecore.Context.Language.Name);

            if (!string.IsNullOrEmpty(productModelNumber))
            {
                query = query.Where(x => x.ProductModelNumbers.Contains(txtProductNumber.Text));
            }

            query = query.Where(x => x.DownloadCategory.Equals(StaticVariables.DriversSoftwareDownloads));

            var searchResults = query.OrderByDescending(x => x.DownloadReleaseDate).Page(DownloadSoftwarePageIndex - 1, RecordsPerPage).GetResults();
            DownloadSoftwareTotalRecords = searchResults.TotalSearchResults;

            if (DownloadSoftwareTotalRecords > 0)
            {
                CreateDownloadSoftwarePagingControl();
            }

            List<string> lstSubCategories = new List<string>();
            if (facet)
            {
                var categoryFacetResultsType =
                    query.FacetPivotOn((x => x.FacetOn(y => y.DownloadCategoryId))).GetFacets();

                var firstOrDefault = CollectFacets(categoryFacetResultsType.Categories).FirstOrDefault();
                if (firstOrDefault != null)
                {
                    var lstFacets = firstOrDefault.FacetValues.Where(x => !string.IsNullOrEmpty(GetCategoryKey(x.Value)) && GetCategoryKey(x.Value).ToLower(CultureInfo.InvariantCulture).Equals(StaticVariables.DriversSoftwareDownloads)).OrderBy(x => x.DisplayName).ToList();
                    lstSubCategories = lstFacets.Select(f => f.DisplayName).ToList<string>();
                    DownloadSoftwareTypes.DataSource = lstFacets;
                    DownloadSoftwareTypes.DataBind();
                }
            }


            foreach (var item in searchResults.Select(x => x.Document))
            {
                sitecoreService.Map(item);
                downloadList.Add(item);
            }

            DownloadSoftwareList.DataSource = downloadList.OrderByDescending(x => x.DownloadReleaseDate).ToList();
            DownloadSoftwareList.DataBind();
        }

        #endregion

        #region Faqs

        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        private void BindFaqs(string productModelNumber = null, string productCategory = null, string productSubCategory = null, string productFamily = null, bool facet = true)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var faqList = new List<FrequentlyAskedQuestionSearchItem>();

            var query = FrequentQuestionsSearchContext.GetQueryable<FrequentlyAskedQuestionSearchItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(i => i.Language == Sitecore.Context.Language.Name);

            if (!string.IsNullOrEmpty(productModelNumber))
            {
                query = query.Where(x => x.ProductModelNumbers.Contains(txtProductNumber.Text));
            }

            if (!string.IsNullOrEmpty(productCategory))
            {
                query = query.Where(x => x.RelatedCategories.Contains(productCategory));
            }

            if (!string.IsNullOrEmpty(productSubCategory))
            {
                query = query.Where(x => x.RelatedSubcategories.Contains(productSubCategory));
            }

            if (!string.IsNullOrEmpty(productFamily))
            {
                query = query.Where(x => x.RelatedFamilies.Contains(productFamily));
            }

            var searchResults = query.Page(FaqPageIndex - 1, RecordsPerPage).GetResults();
            FaqTotalRecords = searchResults.TotalSearchResults;

            if (FaqTotalRecords > 0)
            {
                CreateFaqPagingControl();
            }

            foreach (var item in searchResults.Select(x => x.Document))
            {
                sitecoreService.Map(item);
                faqList.Add(item);
            }

            faqList = faqList.Where(x => x != null).ToList();

            DownloadFrequentQuestionList.DataSource = faqList;
            DownloadFrequentQuestionList.DataBind();

            if (!facet) return;
            var categoryFacets = query.FacetPivotOn(x => x.FacetOn(y => y.RelatedCategories).FacetOn(y => y.RelatedSubcategories).FacetOn(z => z.RelatedFamilies)).GetFacets();

            var firstOrDefault = CollectFacets(categoryFacets.Categories).FirstOrDefault();

            if (firstOrDefault == null) return;
            DownloadFAQCategories.DataSource = firstOrDefault.FacetValues;
            DownloadFAQCategories.DataBind();
        }

        protected void LinkButtonDownloadFrequentQuestionCategoryFilter_OnClick(object sender, EventArgs e)
        {
            SelectedQuestionFamily = null;
            SelectedQuestionSubcategory = null;
            SelectedQuestionCategory = ((LinkButton)sender).CommandArgument;

            FaqPageIndex = 1;
            BindFaqs(txtProductNumber.Text, SelectedQuestionCategory, SelectedQuestionSubcategory, SelectedQuestionFamily, false);
        }

        protected void LinkButtonDownloadFrequentQuestionSubcategoryFilter_OnClick(object sender, EventArgs e)
        {
            SelectedQuestionFamily = null;
            SelectedQuestionCategory = null;
            SelectedQuestionSubcategory = ((LinkButton)sender).CommandArgument;

            FaqPageIndex = 1;
            BindFaqs(txtProductNumber.Text, SelectedQuestionCategory, SelectedQuestionSubcategory, SelectedQuestionFamily, false);
        }

        protected void LinkButtonDownloadFrequentQuestionFamilyFilter_OnClick(object sender, EventArgs e)
        {
            SelectedQuestionCategory = null;
            SelectedQuestionSubcategory = null;
            SelectedQuestionFamily = ((LinkButton)sender).CommandArgument;

            FaqPageIndex = 1;
            BindFaqs(txtProductNumber.Text, SelectedQuestionCategory, SelectedQuestionSubcategory, SelectedQuestionFamily, false);
        }

        #endregion

        #region Facets

        private IEnumerable<IFacet> CollectFacets(IEnumerable<FacetCategory> facetCategoryList)
        {

            var list = new List<Facet>();
            try
            {
                list.AddRange(from resultFacetCategory in facetCategoryList
                              let neestedFacetNames = resultFacetCategory.Name.Split(',')
                              select new Facet()
                              {
                                  DisplayName = neestedFacetNames[0],
                                  FieldName = neestedFacetNames[0],
                                  FacetValues = CollectFacetValues(resultFacetCategory.Values, neestedFacetNames, 0, new List<string>()),
                              });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }
            return list;
        }

        private IEnumerable<IFacet> CollectNestedFacets(List<FacetValue> facetValueList, string[] neestedFacetNames, int level, List<string> parentPathList)
        {
            var list = new List<Facet>();
            try
            {
                var parentPath = string.Join("/", parentPathList.ToArray());

                list.AddRange(facetValueList.Where(d => d.Name.StartsWith(parentPath, StringComparison.OrdinalIgnoreCase))
                    .Select(d => d.Name)
                    .Select(s => s.Substring(parentPath.Length))
                    .Where(s => s.Length > 0)
                    .Select(s => s.Substring(0, (s.IndexOf('/') < 0) ? s.Length : s.IndexOf('/')))
                    .Distinct().Select(resultFacetCategory => new Facet()
                    {
                        DisplayName = neestedFacetNames[level], // todo: use name resolver
                        FieldName = neestedFacetNames[level],
                        FacetValues = CollectFacetValues(facetValueList, neestedFacetNames, level, parentPathList)
                    }));
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }

            return list;
        }

        private IEnumerable<SearchFacetValue> CollectFacetValues(List<FacetValue> facetValueList, string[] neestedFacetNames, int level, List<string> parentPathList)
        {
            var list = new List<SearchFacetValue>();
            try
            {
                var parentPath = string.Join("/", parentPathList.ToArray());

                if (level >= neestedFacetNames.Length)
                    return list;

                list.AddRange(from resultFacetValue in facetValueList.Where(d => d.Name.StartsWith(parentPath, StringComparison.OrdinalIgnoreCase)).Select(d => d.Name.Split('/')).Where(l => l.Count() > parentPathList.Count()).Select(l => l.Skip(parentPathList.Count()).First()).Distinct()
                              let nextLevelPathList = new List<string>(parentPathList) { resultFacetValue }
                              let nextLevelPath = string.Join("/", nextLevelPathList.ToArray())
                              let sum = facetValueList.Where(d => d.Name.StartsWith(nextLevelPath, StringComparison.OrdinalIgnoreCase)).Select(d => d.AggregateCount).Sum()
                              select new SearchFacetValue()
                              {
                                  DisplayName = ResolveFacetValueName(resultFacetValue),
                                  Value = resultFacetValue,
                                  ParentId = ResolveFacetParent(resultFacetValue),
                                  DocumentCount = sum,
                                  NestedFacets = CollectNestedFacets(facetValueList, neestedFacetNames, level + 1, nextLevelPathList)
                              });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }

            return list;
        }

        public string ResolveFacetValueName(string name)
        {
            Guid id;
            if (!Guid.TryParse(name, out id)) { return Sitecore.Context.Culture.TextInfo.ToTitleCase(name); }
            var item = Sitecore.Context.Database.GetItem(new ID(id));
            if (item != null && item.TemplateID.ToString().Equals("{E7B62AFB-B727-4257-B6EE-67FA097E8417}"))
            {
                return item["Value"];
            }
            return Sitecore.Context.Culture.TextInfo.ToTitleCase(item != null ? item.Name : name);
        }

        public string ResolveFacetParent(string id)
        {
            var parentid = string.Empty;

            if (string.IsNullOrEmpty(id)) return parentid;
            var itemId = new Guid(id);
            var item = Sitecore.Context.Database.GetItem(new ID(itemId));
            if (item != null)
            {
                parentid = item.ParentID.ToGuid().ToString();
            }

            return parentid;
        }


        #endregion

        private string GetCategoryKey(string categoryValue)
        {
            var selectedCategory = new Guid(categoryValue);

            var category = SitecoreContext.GetItem<IDropListItem>(selectedCategory);
            return category != null ? category.Key : string.Empty;
        }

        public string CalculateContentLength(string value)
        {
            if (string.IsNullOrEmpty(value) || Convert.ToInt32(value) < 0) return string.Empty;
            var size = Convert.ToInt32(value);
            string[] sizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

            if (size == 0)
            {
                return "0.0 bytes";
            }

            var mag = (int)Math.Log(size, 1024);
            var adjustedSize = (decimal)size / (1L << (mag * 10));

            return string.Format("{0:n1} {1}", adjustedSize, sizeSuffixes[mag]);
        }

        public IEnumerable<IFacetValue> GetHierarchicalFacets(SearchFacetValue facetValue)
        {
            var firstOrDefault = facetValue.NestedFacets.FirstOrDefault();
            return firstOrDefault != null ? firstOrDefault.FacetValues.Where(x => x.ParentId.Equals(facetValue.Value)) : null;
        }

        protected string GetFrequentlyAskedQuestionUrl(string itemPath)
        {
            if (string.IsNullOrEmpty(itemPath)) return string.Empty;
            var position = itemPath.LastIndexOf("/", StringComparison.Ordinal) + 1;
            return
                string.Format("/" + Sitecore.Context.Language.Name + "/test-measurement/support/downloads/faq/{0}",
                    itemPath.Substring(position, itemPath.Length - position));
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected void SearchButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(LinkManager.GetItemUrl(Sitecore.Context.Item) + "?model=" + txtProductNumber.Text);
        }

        private void Page_Unload(object sender, EventArgs e)
        {
            DownloadSearchContext.Dispose();
            FrequentQuestionsSearchContext.Dispose();
        }

        #region pagination

        private void CreateDownloadLibraryPagingControl()
        {
            placeHolderDownloadLibraryPager.Controls.Clear();

            if (DownloadLibraryPageIndex > 1)
            {
                LinkButton button = new LinkButton();
                button.ID = "lnkDownloadLibraryPrevPage";
                button.Text = "<";
                button.Command += new CommandEventHandler(DownloadLibraryPagination_Click);
                button.CommandName = "previous";
                placeHolderDownloadLibraryPager.Controls.Add(button);
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderDownloadLibraryPager.Controls.Add(spacer);
            }

            int Pages = DownloadLibraryTotalRecords / this.RecordsPerPage;
            int PageLenth = 9;
            PageLenth = (PageLenth > Pages) ? Pages : PageLenth;
            int j = 0;

            if (DownloadLibraryPageIndex / 10 >= 1)
            {
                PageLenth = DownloadLibraryPageIndex + 1;
                j = PageLenth - 10;
            }

            if (PageLenth >= Pages)
                PageLenth = Pages;

            if (DownloadLibraryTotalRecords.Equals(this.RecordsPerPage))
                PageLenth = 0;

            if ((DownloadLibraryTotalRecords % this.RecordsPerPage) == 0)
                PageLenth = PageLenth - 1;

            for (int i = j; i < PageLenth + 1; i++)
            {
                if (DownloadLibraryPageIndex.Equals(i + 1))
                {
                    Label lnk = new Label();
                    lnk.Text = (i + 1).ToString();
                    lnk.CssClass = "active floatLft";
                    placeHolderDownloadLibraryPager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderDownloadLibraryPager.Controls.Add(spacer);
                }
                else
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnkDownloadLibraryPage" + (i + 1).ToString();
                    lnk.Text = (i + 1).ToString();
                    lnk.Command += new CommandEventHandler(DownloadLibraryPagination_Click);
                    lnk.CommandArgument = (i + 1).ToString();
                    placeHolderDownloadLibraryPager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderDownloadLibraryPager.Controls.Add(spacer);
                }
            }

            if (DownloadLibraryPageIndex * this.RecordsPerPage < DownloadLibraryTotalRecords)
            {
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderDownloadLibraryPager.Controls.Add(spacer);
                LinkButton lnk = new LinkButton();
                lnk.ID = "lnkDownloadLibraryNextPage";
                lnk.Text = ">";
                lnk.Command += new CommandEventHandler(DownloadLibraryPagination_Click);
                lnk.CommandName = "next";
                placeHolderDownloadLibraryPager.Controls.Add(lnk);
            }
        }

        protected void DownloadLibraryPagination_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName.Equals("previous"))
            {
                DownloadLibraryPageIndex = DownloadLibraryPageIndex - 1;
            }
            else if (e.CommandName.Equals("next"))
            {
                DownloadLibraryPageIndex = DownloadLibraryPageIndex + 1;
            }
            else
            {
                DownloadLibraryPageIndex = Convert.ToInt32(e.CommandArgument.ToString());
            }

            BindDownloads(txtProductNumber.Text, SelectedDownloadCategory, false);
        }

        protected string GetDownloadLibraryPaginationName()
        {
            return (DownloadLibraryTotalRecords > 0 ? (((DownloadLibraryPageIndex - 1) * RecordsPerPage) + 1) : 0) + " - " +
                ((DownloadLibraryPageIndex * RecordsPerPage) > DownloadLibraryTotalRecords ? DownloadLibraryTotalRecords :
                (DownloadLibraryPageIndex * RecordsPerPage)) + " " + Translate.TextByDomain("GlobalDictionary", "ofabout") + " " + DownloadLibraryTotalRecords + " " + Translate.TextByDomain("GlobalDictionary", "results");
        }

        private void CreateDownloadSoftwarePagingControl()
        {
            placeHolderDownloadSoftwarePager.Controls.Clear();

            if (DownloadSoftwarePageIndex > 1)
            {
                LinkButton button = new LinkButton();
                button.ID = "lnkDownloadSoftwarePrevPage";
                button.Text = "<";
                button.Command += new CommandEventHandler(DownloadSoftwarePagination_Click);
                button.CommandName = "previous";
                placeHolderDownloadSoftwarePager.Controls.Add(button);
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderDownloadSoftwarePager.Controls.Add(spacer);
            }

            int Pages = DownloadSoftwareTotalRecords / this.RecordsPerPage;
            int PageLenth = 9;
            PageLenth = (PageLenth > Pages) ? Pages : PageLenth;
            int j = 0;

            if (DownloadSoftwarePageIndex / 10 >= 1)
            {
                PageLenth = DownloadSoftwarePageIndex + 1;
                j = PageLenth - 10;
            }

            if (PageLenth >= Pages)
                PageLenth = Pages;

            if (DownloadSoftwareTotalRecords.Equals(this.RecordsPerPage))
                PageLenth = 0;

            if ((DownloadSoftwareTotalRecords % this.RecordsPerPage) == 0)
                PageLenth = PageLenth - 1;

            for (int i = j; i < PageLenth + 1; i++)
            {
                if (DownloadSoftwarePageIndex.Equals(i + 1))
                {
                    Label lnk = new Label();
                    lnk.Text = (i + 1).ToString();
                    lnk.CssClass = "active floatLft";
                    placeHolderDownloadSoftwarePager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderDownloadSoftwarePager.Controls.Add(spacer);
                }
                else
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnkDownloadSoftwarePage" + (i + 1).ToString();
                    lnk.Text = (i + 1).ToString();
                    lnk.Command += new CommandEventHandler(DownloadSoftwarePagination_Click);
                    lnk.CommandArgument = (i + 1).ToString();
                    placeHolderDownloadSoftwarePager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderDownloadSoftwarePager.Controls.Add(spacer);
                }
            }

            if (DownloadSoftwarePageIndex * this.RecordsPerPage < DownloadSoftwareTotalRecords)
            {
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderDownloadSoftwarePager.Controls.Add(spacer);
                LinkButton lnk = new LinkButton();
                lnk.ID = "lnkDownloadSoftwareNextPage";
                lnk.Text = ">";
                lnk.Command += new CommandEventHandler(DownloadSoftwarePagination_Click);
                lnk.CommandName = "next";
                placeHolderDownloadSoftwarePager.Controls.Add(lnk);
            }
        }

        protected void DownloadSoftwarePagination_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName.Equals("previous"))
            {
                DownloadSoftwarePageIndex = DownloadSoftwarePageIndex - 1;
            }
            else if (e.CommandName.Equals("next"))
            {
                DownloadSoftwarePageIndex = DownloadSoftwarePageIndex + 1;
            }
            else
            {
                DownloadSoftwarePageIndex = Convert.ToInt32(e.CommandArgument.ToString());
            }

            BindDownloadSoftwares(txtProductNumber.Text, false);
        }

        protected string GetDownloadSoftwarePaginationName()
        {
            return (DownloadSoftwareTotalRecords > 0 ? (((DownloadSoftwarePageIndex - 1) * RecordsPerPage) + 1) : 0) + " - " + ((DownloadSoftwarePageIndex * RecordsPerPage) > DownloadSoftwareTotalRecords ? DownloadSoftwareTotalRecords : (DownloadSoftwarePageIndex * RecordsPerPage)) + " " + Translate.TextByDomain("GlobalDictionary", "ofabout") + " " + DownloadSoftwareTotalRecords + " " + Translate.TextByDomain("GlobalDictionary", "results");
        }

        private void CreateFaqPagingControl()
        {
            placeHolderFaqPager.Controls.Clear();

            if (FaqPageIndex > 1)
            {
                LinkButton button = new LinkButton();
                button.ID = "lnkFaqPrevPage";
                button.Text = "<";
                button.Command += new CommandEventHandler(FaqPagination_Click);
                button.CommandName = "previous";
                placeHolderFaqPager.Controls.Add(button);
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderFaqPager.Controls.Add(spacer);
            }

            int Pages = FaqTotalRecords / this.RecordsPerPage;
            int PageLenth = 9;
            PageLenth = (PageLenth > Pages) ? Pages : PageLenth;
            int j = 0;

            if (FaqPageIndex / 10 >= 1)
            {
                PageLenth = FaqPageIndex + 1;
                j = PageLenth - 10;
            }

            if (PageLenth >= Pages)
                PageLenth = Pages;

            if (FaqTotalRecords.Equals(this.RecordsPerPage))
                PageLenth = 0;

            if ((FaqTotalRecords % this.RecordsPerPage) == 0)
                PageLenth = PageLenth - 1;

            for (int i = j; i < PageLenth + 1; i++)
            {
                if (FaqPageIndex.Equals(i + 1))
                {
                    Label lnk = new Label();
                    lnk.Text = (i + 1).ToString();
                    lnk.CssClass = "active floatLft";
                    placeHolderFaqPager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderFaqPager.Controls.Add(spacer);
                }
                else
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnkFaqPage" + (i + 1).ToString();
                    lnk.Text = (i + 1).ToString();
                    lnk.Command += new CommandEventHandler(FaqPagination_Click);
                    lnk.CommandArgument = (i + 1).ToString();
                    placeHolderFaqPager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderFaqPager.Controls.Add(spacer);
                }
            }

            if (FaqPageIndex * this.RecordsPerPage < FaqTotalRecords)
            {
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderFaqPager.Controls.Add(spacer);
                LinkButton lnk = new LinkButton();
                lnk.ID = "lnkFaqNextPage";
                lnk.Text = ">";
                lnk.Command += new CommandEventHandler(FaqPagination_Click);
                lnk.CommandName = "next";
                placeHolderFaqPager.Controls.Add(lnk);
            }
        }

        protected void FaqPagination_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName.Equals("previous"))
            {
                FaqPageIndex = FaqPageIndex - 1;
            }
            else if (e.CommandName.Equals("next"))
            {
                FaqPageIndex = FaqPageIndex + 1;
            }
            else
            {
                FaqPageIndex = Convert.ToInt32(e.CommandArgument.ToString());
            }

            BindFaqs(txtProductNumber.Text, SelectedQuestionCategory, SelectedQuestionSubcategory, SelectedQuestionFamily, false);
        }

        protected string GetFaqPaginationName()
        {
            return (FaqTotalRecords > 0 ? (((FaqPageIndex - 1) * RecordsPerPage) + 1) : 0) + " - " + ((FaqPageIndex * RecordsPerPage) > FaqTotalRecords ? FaqTotalRecords : (FaqPageIndex * RecordsPerPage)) + " " + Translate.TextByDomain("GlobalDictionary", "ofabout") + " " + FaqTotalRecords + " " + Translate.TextByDomain("GlobalDictionary", "results");
        }


        #endregion

        protected void DownloadLibraryTypes_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (!e.CommandName.Equals("Click")) return;

            var selectedCategory = new Guid(e.CommandArgument.ToString());
            var libraryCategory = SitecoreContext.GetItem<IDropListItem>(selectedCategory);
            SelectedDownloadCategory = libraryCategory.Key;
            DownloadLibraryPageIndex = 1;
            BindDownloads(txtProductNumber.Text, SelectedDownloadCategory, false);

            var selectedIndex = e.Item.ItemIndex;
            foreach (RepeaterItem item in DownloadLibraryTypes.Items)
            {
                ((LinkButton)item.FindControl("LinkButtonDownloadLibraryCategoryFilter")).CssClass = item.ItemIndex == selectedIndex ? "currentActiveEle" : string.Empty;

            }
        }

        protected void DownloadSoftwareTypes_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (!e.CommandName.Equals("Click")) return;

            DownloadSoftwarePageIndex = 1;
            BindDownloadSoftwares(txtProductNumber.Text, false);

            var selectedIndex = e.Item.ItemIndex;
            foreach (RepeaterItem item in DownloadSoftwareTypes.Items)
            {
                ((LinkButton)item.FindControl("LinkButtonDownloadSoftwareCategoryFilter")).CssClass = item.ItemIndex == selectedIndex ? "currentActiveEle" : string.Empty;
            }
        }

        protected string GaTrackingScript(string itemUrl)
        {
            return string.Format(ConfigurationManager.AppSettings["GaTrackingScript"]
                , Convert.ToString(itemUrl.Substring(itemUrl.IndexOf("www.anritsu.com/") + 1)));
        }

    }
}