﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using System;
using System.Web.UI.HtmlControls;
using System.Linq;
using Sitecore.Data.Items;

    public partial class EcoProducts : GlassUserControl<IEcoProduct>
    {
        
        private void Page_Load(object sender, EventArgs e)
        {
                      
            ecoProducts.DataSource = SitecoreContext.GetItem<IModelBase>(ItemIds.EcoProductDetails).GetChildren<IEcoProductDetails>();
            ecoProducts.DataBind();

            BindOptionalLinks();
        }

        

        

        public void BindOptionalLinks()
        {
            IStaticPageWithOptionalLinks currentItem = SitecoreContext.GetItem<IStaticPageWithOptionalLinks>(SitecoreContext.GetCurrentItem<Item>().ID.ToString());
            if (currentItem.Links != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(SitecoreContext.Database.GetItem(currentItem.Id.ToString()), "Links");
                optionalLinks.DataSource = relatedGeneralLinks.LinkItems;
                optionalLinks.DataBind();
            }
        }

        protected void optionalLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            {
                GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
                HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
                relatedAnchor.InnerText = linkItem.LinkText;
                relatedAnchor.HRef = linkItem.Url;
                relatedAnchor.Target = linkItem.Target;
            }
        }

       

       
    }
}
   