﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EloquaSubLayout.ascx.cs"
    Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.EloquaSubLayout" %>

<div class="container main-container-padding">
    <div class="contact">
        <h1><%= Sitecore.Context.Item.Fields["PageTitle"] %></h1>
    </div>
    <form runat="server">
        <asp:Panel ID="pnlElqFrm" CssClass="eloquaFormFields" runat="server" Style="margin-top: 20px" />
    </form>
    <div id="divEloqua" runat="server" class="divEloqua">
        <asp:Literal ID="ltrLndngPgBody" runat="server"></asp:Literal>
    </div>
</div>

