﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Configuration;
using BLL = Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.UfaSdk;
using Anritsu.WebApp.GlobalWeb.UfaSdk.EloquaWS;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class EloquaSubLayout : UserControl
    {
        private EloquaRestApi _client;
        private Form _formInfo;
        private StructuredLandingPage _landingPageInfo;
        //private LandingPage _landingPageInfoNew;
        private string _redirectUrl;
        private string _eloquaCompanyName;
        private string _eloquaUserName;
        private string _eloquaPassword;

        protected void Page_Load(object sender, EventArgs e)
        {
            InitEloquaContent();
        }

        private void GetEloquaInstance()
        {
            // Get Eloqua Instance details
            var dRow =
                BLL.UniversalForm.GetEloquaInstanceById(Convert.ToInt16(Sitecore.Context.Item.Fields["EloquaInstance"].Value));
            _eloquaCompanyName = dRow["CompanyName"].ToString();
            _eloquaPassword = CryptUtility.Decrypt(dRow["Password"].ToString(), dRow["UserName"].ToString());
            _eloquaUserName = CryptUtility.Decrypt(dRow["UserName"].ToString(), Settings.GetSetting("EncryptionKey"));
        }

        private void InitEloquaContent()
        {
            // Get Eloqua Instance details
           GetEloquaInstance();

            _client = new EloquaRestApi(Settings.GetSetting("EloquaRestAPI"),
                _eloquaCompanyName, _eloquaUserName, _eloquaPassword);

            // Load Eloqua Content based on FormType
            if (string.IsNullOrEmpty(Sitecore.Context.Item.Fields["FormId"].Value)) return;
            var formId = Convert.ToInt32(Sitecore.Context.Item.Fields["FormId"].Value);
            var pageType = Sitecore.Context.Item.Fields["FormType"].Value;
            if (pageType != null)
            {
                LoadEloquaContent(formId, pageType.Trim());
            }
            else
            {
                throw new HttpException(404, "Page not found.");
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Couldnt")]
        private void LoadEloquaContent(int? formId, string pageType)
        {
            string strDynamicScript;
            switch (pageType.ToLower(CultureInfo.InvariantCulture))
            {
                case "form":
                    _formInfo = _client.GetFormFieldsById(formId);
                    if (_formInfo != null && _formInfo.Type.ToLower(CultureInfo.InvariantCulture) == "form")
                    {
                        strDynamicScript = Settings.GetSetting("SelectScriptReference") + Settings.GetSetting("FormScriptReference");
                        
                        foreach (var formElement in _formInfo.Elements)
                        {
                            CreateEloquaForm(formElement);
                        }

                        // to set the redirectPage after submit
                        var redirectResult = (from ProcessingStep processingStep in _formInfo.ProcessingSteps
                                              where processingStep.Type.ToLower(CultureInfo.InvariantCulture) == "formstepredirecttowebpage"
                                                 && processingStep.PageUrl != null
                                              select processingStep).FirstOrDefault();
                        _redirectUrl = redirectResult != null && redirectResult.PageUrl != null ?
                                              redirectResult.PageUrl.ConstantValue : string.Empty;
                    }
                    else
                    {
                        throw new HttpException(404, "Page not found.");
                    }
                    break;
                case "landingpage":
                    _landingPageInfo = _client.GetStructuredLandingPage(formId);
                    //_landingPageInfoNew = _client.GetLandingPage(formId);
                    if (_landingPageInfo != null
                        && _landingPageInfo.Type.Trim().ToLower(CultureInfo.InvariantCulture) == "landingpage")
                    {
                        if (IsValidScript(_landingPageInfo.HtmlContent.CssHeader)
                            && IsValidScript(_landingPageInfo.HtmlContent.javascriptHeader)
                            && IsValidScript(_landingPageInfo.HtmlContent.HtmlBody))
                        {
                            var ltrControl = new LiteralControl
                            {
                                Text =
                                    _landingPageInfo.HtmlContent.CssHeader +
                                    _landingPageInfo.HtmlContent.javascriptHeader
                            };
                            Page.Header.Controls.Add(ltrControl);

                            ltrLndngPgBody.Text =
                                _landingPageInfo.HtmlContent.HtmlBody.Replace("<body>", "").Replace("</body>", "");

                            strDynamicScript = Settings.GetSetting("LandingPgScriptReference");
                        }
                        else
                        {
                            throw new HttpException(404, "Page not found.");
                        }
                    }
                    else
                    {
                        throw new HttpException(404, "Page not found.");
                    }
                    break;
                default:
                    throw new HttpException(404, "Page not found.");
            }

            // add script dynamically in master page literal based on form type.
            var mpLiteral = (Literal)Page.FindControl("ltrDynamicScript");
            if (mpLiteral != null)
            {
                mpLiteral.Text = strDynamicScript;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private void CreateEloquaForm(Element element)
        {
            var htmlDiv = new HtmlGenericControl("DIV");

            switch (element.DisplayType)
            {
                case "text":
                {
                    var textBoxControl = new TextBox();
                    if (element.Validations.Count > 0)
                    {
                        htmlDiv.Attributes["class"] = "group input-text required";
                    }
                    else
                    {
                        htmlDiv.Attributes["class"] = "group input-text";
                    }
                    pnlElqFrm.Controls.Add(htmlDiv);
                    textBoxControl.ID = element.HtmlName;
                    textBoxControl.TextMode = TextBoxMode.SingleLine;
                    textBoxControl.MaxLength = 100; //eloqua limit
                    if (!string.IsNullOrEmpty(element.DefaultValue))
                        textBoxControl.Text = element.DefaultValue;
                    var lblName = new Label
                    {
                        Text = textBoxControl.Attributes["placeholder"] = element.Validations.Count > 0
                            ? element.Name + "*"
                            : element.Name,
                        AssociatedControlID = element.HtmlName
                    };
                    htmlDiv.Controls.Add(lblName);
                    if (element.Validations.Count > 0)
                    {
                        EloquaValidationRule(htmlDiv, element);
                    }
                    htmlDiv.Controls.Add(textBoxControl);
                }
                    break;
                case "textArea":
                {
                    var textBoxControl = new TextBox();
                    htmlDiv.Attributes["class"] = "group input-textarea";
                    pnlElqFrm.Controls.Add(htmlDiv);
                    textBoxControl.ID = element.HtmlName;
                    textBoxControl.TextMode = TextBoxMode.MultiLine;
                    textBoxControl.MaxLength = 2000;
                    if (!string.IsNullOrEmpty(element.DefaultValue))
                        textBoxControl.Text = element.DefaultValue;
                    var lblName = new Label
                    {
                        Text = textBoxControl.Attributes["placeholder"] = element.Validations.Count > 0
                            ? element.Name + "*"
                            : element.Name,
                        AssociatedControlID = element.HtmlName
                    };
                    htmlDiv.Controls.Add(lblName);
                    if (element.Validations.Count > 0)
                    {
                        EloquaValidationRule(htmlDiv, element);
                    }
                    htmlDiv.Controls.Add(textBoxControl);
                }
                    break;
                case "hidden":
                {
                    var hiddenControl = new HiddenField {ID = element.HtmlName};
                    if (!string.IsNullOrEmpty(element.DefaultValue))
                        hiddenControl.Value = element.DefaultValue;
                    pnlElqFrm.Controls.Add(hiddenControl);
                }
                    break;
                case "singleSelect":
                {
                    var dropdownListControl = new DropDownList();
                    var optionsList = new Collection<Item>();

                    var innerDiv = new HtmlGenericControl("DIV");
                    innerDiv.Attributes["style"] = "padding-top:5px";
                    innerDiv.Controls.Add(new Label
                    {
                        Text = element.Validations.Count > 0
                            ? element.Name + "*"
                            : element.Name,
                        AssociatedControlID = element.HtmlName
                    });
                    pnlElqFrm.Controls.Add(innerDiv);

                    dropdownListControl.ID = element.HtmlName;
                    if (!string.IsNullOrEmpty(element.OptionListId))
                    {
                        optionsList = _client.GetOptionListById(element.OptionListId);
                        dropdownListControl.DataSource = optionsList;
                        dropdownListControl.DataTextField = "displayName";
                        dropdownListControl.DataValueField = "value";
                        dropdownListControl.DataBind();
                        if (!string.IsNullOrEmpty(element.DefaultValue))
                            dropdownListControl.SelectedValue = element.DefaultValue;
                    }
                    htmlDiv.Attributes["class"] = "group input-select";
                    if (element.Validations.Count > 0 && optionsList.Count > 1)
                    {
                        dropdownListControl.Items[0].Text = dropdownListControl.Items[0].Text + "*";
                        EloquaValidationRule(htmlDiv, element, optionsList.First().Value);
                    }
                    htmlDiv.Controls.Add(dropdownListControl);
                    pnlElqFrm.Controls.Add(htmlDiv);
                }
                    break;
                case "multiSelect":
                {
                    if (!string.IsNullOrEmpty(element.OptionListId))
                    {
                        var innerDiv = new HtmlGenericControl("DIV");
                        var listBoxControl = new ListBox
                        {
                            ID = element.HtmlName,
                            SelectionMode = ListSelectionMode.Multiple,
                            DataSource = _client.GetOptionListById(element.OptionListId),
                            DataTextField = "displayName",
                            DataValueField = "value"
                        };
                        listBoxControl.DataBind();
                        if (!string.IsNullOrEmpty(element.DefaultValue))
                            listBoxControl.SelectedValue = element.DefaultValue;
                        else
                            listBoxControl.SelectedIndex = 0;
                        innerDiv.Controls.Add(new Label()
                        {
                            Text = element.Name,
                            AssociatedControlID = element.HtmlName
                        });
                        innerDiv.Attributes["style"] = "padding-top:5px";
                        pnlElqFrm.Controls.Add(innerDiv);
                        //htmlDiv.Attributes["class"] = "group input-select";
                        htmlDiv.Controls.Add(listBoxControl);
                        pnlElqFrm.Controls.Add(htmlDiv);
                    }
                }
                    break;
                case "checkbox":
                {
                    var innerDiv = new HtmlGenericControl("DIV");
                    htmlDiv.Attributes["class"] = "group input-checkbox";
                    if (!string.IsNullOrEmpty(element.OptionListId))
                    {
                        var checkBoxListControl = new CheckBoxList
                        {
                            ID = element.HtmlName,
                            DataSource = _client.GetOptionListById(element.OptionListId),
                            DataTextField = "displayName",
                            DataValueField = "value"
                        };
                        checkBoxListControl.DataBind();
                        if (!string.IsNullOrEmpty(element.X_E10_PrecheckCheckbox))
                            checkBoxListControl.SelectedValue = element.X_E10_PrecheckCheckbox;
                        checkBoxListControl.CssClass = "checkbox-list";
                        innerDiv.Controls.Add(new Label()
                        {
                            Text = element.Name,
                            //AssociatedControlID = element.HtmlName
                        });
                        htmlDiv.Controls.Add(innerDiv);
                        htmlDiv.Controls.Add(checkBoxListControl);
                        pnlElqFrm.Controls.Add(htmlDiv);
                    }
                    else
                    {
                        var checkBoxControl = new CheckBox
                        {
                            ID = element.HtmlName,
                            //Text = element.Name
                        };
                        if (!string.IsNullOrEmpty(element.X_E10_PrecheckCheckbox) &&
                            element.X_E10_PrecheckCheckbox.Equals("checked"))
                            checkBoxControl.Checked = true;
                        innerDiv.Controls.Add(checkBoxControl);
                        innerDiv.Controls.Add(new Label() {Text = element.Name});
                        htmlDiv.Controls.Add(innerDiv);
                        pnlElqFrm.Controls.Add(htmlDiv);
                    }
                }
                    break;
                case "radio":
                {
                    htmlDiv.Attributes["class"] = "group input-radio";
                    if (!string.IsNullOrEmpty(element.OptionListId))
                    {
                        var innerDiv = new HtmlGenericControl("DIV");
                        var radioButtonListControl = new RadioButtonList
                        {
                            ID = element.HtmlName,
                            DataSource = _client.GetOptionListById(element.OptionListId),
                            DataTextField = "displayName",
                            DataValueField = "value"
                        };
                        radioButtonListControl.DataBind();
                        if (!string.IsNullOrEmpty(element.DefaultValue))
                            radioButtonListControl.SelectedValue = element.DefaultValue;
                        radioButtonListControl.CssClass = "radio-button-list";
                        innerDiv.Controls.Add(new Label()
                        {
                            Text = element.Name,
                            //AssociatedControlID = element.HtmlName
                        });
                        htmlDiv.Controls.Add(innerDiv);
                        htmlDiv.Controls.Add(radioButtonListControl);
                        pnlElqFrm.Controls.Add(htmlDiv);

                    }
                    else
                    {
                        var innerDiv = new HtmlGenericControl("DIV");
                        var radioButtonControl = new RadioButton
                        {
                            //Text = element.Name,
                            ID = element.HtmlName
                        };
                        innerDiv.Controls.Add(radioButtonControl);
                        innerDiv.Controls.Add(new Label() {Text = element.Name});
                        htmlDiv.Controls.Add(innerDiv);
                        pnlElqFrm.Controls.Add(htmlDiv);
                    }
                }
                    break;
                case "submit":
                {
                    var textBoxControl = new TextBox
                    {
                        ID = "tbxColor",
                        Text = "green",
                        ClientIDMode = ClientIDMode.Static
                    };
                    textBoxControl.Attributes["style"] = "display:none";
                    var innerDiv = new HtmlGenericControl("DIV");
                    innerDiv.Attributes.Add("class", "eloquaSubmitDIV");
                    var buttonControl = new Button();
                    buttonControl.Attributes.Add("class", "button");
                    buttonControl.ID = element.HtmlName;
                    buttonControl.Text = element.Name;
                    buttonControl.Click += btnSubmit_Click;
                    htmlDiv.Attributes["class"] = "group";
                    htmlDiv.Controls.Add(textBoxControl);
                    innerDiv.Controls.Add(buttonControl);
                    htmlDiv.Controls.Add(innerDiv);
                    pnlElqFrm.Controls.Add(htmlDiv);
                }
                    break;
                default:
                {
                    if (!string.IsNullOrWhiteSpace(element.Name))
                    {
                        var innerDiv = new HtmlGenericControl("DIV");
                        //var lblCustom = new Label {Text = element.Name};
                        //lblCustom.Font.Bold = true;
                        var ltrCustom = new LiteralControl("<p style='font-weight:bold'>" + element.Name + "</p>");
                        innerDiv.Controls.Add(ltrCustom);
                        htmlDiv.Controls.Add(innerDiv);
                        pnlElqFrm.Controls.Add(htmlDiv);
                    }

                    if (element.Fields != null)
                    {
                        foreach (var fieldElement in element.Fields.Select(field => new Element
                        {
                            DisplayType = field.DisplayType,
                            Validations = field.Validations,
                            HtmlName = field.HtmlName,
                            DefaultValue = "",
                            Name = field.Name,
                            OptionListId = field.OptionListId
                        }))
                        {
                            CreateEloquaForm(fieldElement);
                        }
                    }
                }
                    break;
            }
        }

        private void EloquaValidationRule(HtmlGenericControl container, Element element, string initialValue = null)
        {
            var requiredValidator = new RequiredFieldValidator();
            var expressionValidator = new RegularExpressionValidator();

            requiredValidator.Display = ValidatorDisplay.Dynamic;
            expressionValidator.Display = ValidatorDisplay.Dynamic;

            foreach (var item in element.Validations)
            {
                var htmldiv = new HtmlGenericControl("DIV");
                htmldiv.Attributes.Add("class", "error-message");
                if (item.Condition.Type.Equals("IsRequiredCondition"))
                {
                    requiredValidator.ControlToValidate = element.HtmlName;
                    requiredValidator.ErrorMessage = item.Message;
                    if (element.DisplayType.Equals("singleSelect"))
                        requiredValidator.InitialValue = initialValue;
                    htmldiv.Controls.Add(requiredValidator);
                }
                if (item.Condition.Type.Equals("TextLengthCondition"))
                {
                    requiredValidator.ControlToValidate = element.HtmlName;
                    requiredValidator.ErrorMessage = item.Message;
                    expressionValidator.ControlToValidate = element.HtmlName;
                    expressionValidator.ValidationExpression = @"^\s*([^\s]\s*){" + item.Condition.Minimum + "," +
                                                               item.Condition.Maximum + "}$";
                    expressionValidator.ErrorMessage = item.Message;
                    htmldiv.Controls.Add(requiredValidator);
                    htmldiv.Controls.Add(expressionValidator);
                }
                if (item.Condition.Type.Equals("IsEmailAddressCondition"))
                {
                    requiredValidator.ControlToValidate = element.HtmlName;
                    requiredValidator.ErrorMessage = item.Message;
                    expressionValidator.ControlToValidate = element.HtmlName;
                    expressionValidator.ValidationExpression =
                        @"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$";
                    expressionValidator.ErrorMessage = item.Message;
                    htmldiv.Controls.Add(requiredValidator);
                    htmldiv.Controls.Add(expressionValidator);
                }
                container.Controls.Add(htmldiv);
            }
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var tbxColor = (TextBox) pnlElqFrm.FindControl("tbxColor");
            if (!tbxColor.Text.Equals("blue", StringComparison.CurrentCultureIgnoreCase)) return;
            var fields = new DynamicEntityFields
            {
                {"FormID", _formInfo.Id},
                {"formName", _formInfo.Name},
                {"formType", _formInfo.Type}
            };

            foreach (var formField in _formInfo.Elements)
            {
                if (formField.DisplayType == null && formField.Fields == null) continue;
                if (formField.Fields != null)
                {
                    foreach (var field in formField.Fields)
                    {
                        var fieldElement = new Element
                        {
                            DisplayType = field.DisplayType,
                            Validations = field.Validations,
                            HtmlName = field.HtmlName,
                            DefaultValue = "",
                            Name = field.Name,
                            OptionListId = field.OptionListId
                        };
                        fields = BuildFieldValues(fieldElement, fields);
                    }
                }
                else
                {
                    fields = BuildFieldValues(formField, fields);
                }
            }

            var service = new EloquaInstance(_eloquaCompanyName, _eloquaUserName, _eloquaPassword);
            if (service.PostToEloqua(fields))
            {
                //if redirectUrl is not available redirect to common thankyou page
                Response.Redirect(!string.IsNullOrWhiteSpace(_redirectUrl) ? _redirectUrl : "ThankYou.aspx");
            }
        }


        protected DynamicEntityFields BuildFieldValues(Element formField, DynamicEntityFields fields)
        {
            if (formField.DisplayType.Equals("text") || formField.DisplayType.Equals("textArea"))
            {
                var textBox = (TextBox)pnlElqFrm.FindControl(formField.HtmlName);
                fields.Add(formField.HtmlName, textBox.Text);
            }
            else if (formField.DisplayType.Equals("hidden"))
            {
                var hiddenField = (HiddenField)pnlElqFrm.FindControl(formField.HtmlName);
                fields.Add(formField.HtmlName, hiddenField.Value);
            }
            else if (formField.DisplayType.Equals("singleSelect"))
            {
                var dropDownList = (DropDownList)pnlElqFrm.FindControl(formField.HtmlName);
                fields.Add(formField.HtmlName, dropDownList.SelectedValue);
            }
            else if (formField.DisplayType.Equals("checkbox"))
            {
                if (!string.IsNullOrEmpty(formField.OptionListId))
                {
                    var selectedItems = string.Empty;
                    var checkBoxList = (CheckBoxList)pnlElqFrm.FindControl(formField.HtmlName);
                    selectedItems = checkBoxList.Items.Cast<ListItem>().Where(listitem => listitem.Selected)
                        .Aggregate(selectedItems, (current, listitem) => current + (listitem.Value + ","));
                    selectedItems = selectedItems.TrimEnd(',');
                    fields.Add(formField.HtmlName, selectedItems);
                }
                else
                {
                    var checkBox = (CheckBox)pnlElqFrm.FindControl(formField.HtmlName);
                    fields.Add(formField.HtmlName, Convert.ToString(checkBox.Checked));
                }
            }
            else if (formField.DisplayType.Equals("multiSelect"))
            {
                if (string.IsNullOrEmpty(formField.OptionListId)) return fields;
                var selectedItems = string.Empty;
                var listBox = (ListBox)pnlElqFrm.FindControl(formField.HtmlName);
                selectedItems = listBox.Items.Cast<ListItem>().Where(listitem => listitem.Selected)
                    .Aggregate(selectedItems, (current, listitem) => current + (listitem.Value + ","));
                selectedItems = selectedItems.TrimEnd(',');
                fields.Add(formField.HtmlName, selectedItems);
            }
            else if (formField.DisplayType.Equals("radio"))
            {
                if (string.IsNullOrEmpty(formField.OptionListId)) return fields;
                var selectedItems = string.Empty;
                var radioButtonList =
                    (RadioButtonList)pnlElqFrm.FindControl(formField.HtmlName);
                selectedItems = radioButtonList.Items.Cast<ListItem>().Where(listitem => listitem.Selected)
                    .Aggregate(selectedItems, (current, listitem) => current + (listitem.Value + ","));
                selectedItems = selectedItems.TrimEnd(',');
                fields.Add(formField.HtmlName, selectedItems);
            }
            return fields;
        }

        protected bool IsValidScript(string htmlScript)
        {
            if (string.IsNullOrWhiteSpace(htmlScript)) return true;
            var validScriptContent = Settings.GetSetting("ValidScriptContent").Split(',');
            return (from Match match in Regex.Matches
                (htmlScript, "<script(.*?)</script>")
                select match.Groups[1].ToString())
                .Select(scriptContent => validScriptContent
                    .Any(
                        validElement =>
                            scriptContent.IndexOf(validElement, StringComparison.CurrentCultureIgnoreCase) > -1))
                .All(isValidScript => isValidScript);
        }
    }
}