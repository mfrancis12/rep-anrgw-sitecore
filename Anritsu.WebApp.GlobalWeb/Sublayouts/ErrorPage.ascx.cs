﻿using System;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ErrorPage : System.Web.UI.UserControl
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["AppendStatus"]) && Request.QueryString["AppendStatus"].ToUpperInvariant().Equals("FALSE"))
            {
                return;
            }
            if (string.IsNullOrEmpty(Request.QueryString["InvalidRequest"]))
            {
                Response.StatusCode = 404;
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
			try{
             divContent.Visible = false;
            if (!string.IsNullOrEmpty(Request.QueryString["InvalidRequest"]))
            {
                divContent.Visible = true;
                if (Sitecore.Context.Item.Fields["Content"] != null)
                {
                    string content= Sitecore.Context.Item.Fields["Content"].ToString();
                    if (Session["INVALIDDOMAIN"] != null)
                    {
                        content = content.Replace("<@DOMAIN>", Session["INVALIDDOMAIN"].ToString());
                    }
                    else
                    {
                        content = content.Replace("<@DOMAIN>","");
                    }
                    ltrLndngPgBody.Text = content;
                }
            }
			}
			catch(Exception ex)
			{
				
			}
        }
    }
}