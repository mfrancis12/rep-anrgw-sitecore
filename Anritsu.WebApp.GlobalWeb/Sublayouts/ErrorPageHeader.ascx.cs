﻿using System;
using System.Configuration;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ErrorPageHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected string CDNPath
        {
            get { return string.Format("//{0}", ConfigurationManager.AppSettings["AWSCdnPath"]); }
        }
    }
}