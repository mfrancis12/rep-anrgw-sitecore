﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorPageSideMenu.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ErrorPageSideMenu" %>

<div id="errpage" class="aside-menu-trigger">
			<div class="icon icon-burguer"></div>
		</div>
		<div class="pusher">
			<div class="aside-menu">
				<div data-tag="/" class="item">
					<div class="menu-title">
						<span href="/en-US">HOME</span>
					</div>
					<div class="menu-item">
						<a href="/products">Products</a>
						<a href="/support">Support</a>
						<a href="/about-anritsu">About Anritsu</a>
						<a href="/contact-us">Contact Us</a>
					</div>
				</div>
			</div>
		</div>
