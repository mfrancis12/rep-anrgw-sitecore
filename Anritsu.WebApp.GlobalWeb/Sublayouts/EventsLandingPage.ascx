﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventsLandingPage.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.EventsLandingPage" %>
<%@ Import Namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:ScriptManager ID="EventsScriptManager" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="EventsUpdatePanel" runat="server" >
                <ContentTemplate>
<div class="container">
    <div  class="multi-promo-filter-items">
        <div  id="divFilter" runat="server" class="multi-promo-filter">
            <div class="select-group" id="divView" runat="server">
                <span class="filter-name"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "View") %></span>
                <div style="border: 0" class="input-select select-filter">
                    <asp:DropDownList ID="ViewFilters" runat="server" OnSelectedIndexChanged="ViewFilters_SelectedIndexChanged" AutoPostBack="true" >
                    </asp:DropDownList>
                </div>
            </div>
            <div class="select-group select-group-right">
                <span class="filter-name"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "From") %></span>
                <div style="border: 0" class="input-select select-filter">
                    <asp:DropDownList ID="YearFilter" runat="server" OnSelectedIndexChanged="YearFilter_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                <a href="javascript:void(0)" class="rss-img"></a>
            </div>
        </div>
        <asp:ListView ID="Events" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.ITradeshows" OnPagePropertiesChanging="Events_PagePropertiesChanging">
            <ItemTemplate>
                <div>
				 <div class="<%#Item.Image!=null && !string.IsNullOrEmpty(Item.Image.Src)?"item":"item-noimg" %>">
                    <div class="<%#Item.Image!=null&& !string.IsNullOrEmpty(Item.Image.Src)?"img":"hiddentxt" %>"><a href="<%#Item.Image!=null&&Item.ImageUrl!=null?Item.ImageUrl.Url:""%>"><%# RenderImage(Item,x=>x.Image) %></a></div>
                    <div class="text">
                         <% if (SitecoreContext.GetCurrentItem<Sitecore.Data.Items.Item>().ID.ToString().Equals(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.HomeEventsPage)==false)  { %>
               <p class="event-type-landing"> <span class="event-type-style"><%# Editable(Item,x=>x.EventType.Value) %></span></p> 
			     <% } %>
                        <h2><a href="<%# Item.IsExternalUrl()? Item.ExternalLandingLink.Url:Item.Url %>" target="<%# Item.IsExternalUrl()?Item.ExternalLandingLink.Target:string.Empty %>"><%# Editable(Item,x=>x.Title) %></a></h2>
                        <p class="item-date"><%# Editable(Item,x=>x.StartDate,x=>x.StartDate.ToLocalTime().ToFormattedDate())+ (Item.EndDate.Equals(DateTime.MinValue)?"":" - "+ Editable(Item,x=>x.EndDate,x=>x.EndDate.ToLocalTime().ToFormattedDate())) %></p>
                        <p><%# Editable(Item,x=>x.ShortDescription) %></p>
                        <p class="link"><a href="<%# Item.IsExternalUrl() ? Item.ExternalLandingLink.Url:Item.Url %>" target="<%# Item.IsExternalUrl()?Item.ExternalLandingLink.Target:string.Empty %>"><b><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ReadMore") %></b><i class="icon icon-arrow-blue"></i></a></p>
                    </div>
                </div>
              </div>
            </ItemTemplate>
        </asp:ListView>
        <div id="divItems" runat="server"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "NoItems") %></div>
        <div class="pager">
            <span>
                <asp:DataPager ID="EventsDataPager" runat="server" PagedControlID="Events" PageSize="20">
                    <Fields>
                        <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active" NextPageText=">" PreviousPageText="<" NextPreviousButtonCssClass="linkcolor" />
                    </Fields>
                </asp:DataPager>
            </span>
        </div>
    </div>
    <sc:Placeholder runat="server" Key="OptionalLinks" />
</div>
</ContentTemplate></asp:UpdatePanel>