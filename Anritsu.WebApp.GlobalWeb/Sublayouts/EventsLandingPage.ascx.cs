﻿using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Items;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using System.Web.UI.WebControls;
    using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
    using Sitecore.Data;
    using Anritsu.WebApp.GlobalWeb.ContentSearch;
    public partial class EventsLandingPage : GlassUserControl<IModelBase>
    {
        #region Global Variables
        string indexName = "anritsu_events";
        //var newsList = new List<SearchNewsItem>();
        string eventsYear = string.Empty;
        #endregion

        private EventsRepository _eventsRepository { get; set; }

        public EventsRepository EventsRepository
        {
            get
            {

                if (_eventsRepository == null)
                {
                    _eventsRepository = new EventsRepository();
                }

                return _eventsRepository;
            }
        }

        /// <summary>
        /// Read TM and SA Events
        /// </summary>
        private void bindTMandSALandingPageEvents()
        {
            if (DataSource != null)
            {
                //Read the data source (TM or SA Group company Events)
                Item eventstab = Sitecore.Context.Database.GetItem(DataSource);
                if (eventstab != null)
                {
                    //Read the TreeList Values
                    var events = eventstab.Fields["Events Landing"].Value;
                    List<ITradeshows> lstEvents = new List<ITradeshows>();
                    if (string.IsNullOrEmpty(events) == false)
                    {
                        //Split the values
                        foreach (string guid in events.Split('|'))
                        {
                            //Check the value, make sure it is not null
                            ITradeshows eventitem = SitecoreContext.GetItem<ITradeshows>(guid);
                          
                                if (eventitem != null)
                                    lstEvents.Add(eventitem);
                            
                        }
                        //Assign the values to Repeater control
                        Events.DataSource = lstEvents;
                        Events.DataBind();
                    }
                }
            }
            divItems.Visible = Events.Items.Count == 0;
           

        }

        void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindViewFilter();
                if (ViewFilters.Items.Count > 0)
                {
                    ViewFilters.SelectedIndex = 0;
                }
                BindYearFilters();
                if (YearFilter.Items.Count > 0)
                {
                    YearFilter.SelectedIndex = 0;
                }
            }
            BindEvents();
          

        }

        protected void BindViewFilter()
        {
            if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.HomeEventsPage))
            {
                ViewFilters.DataSource = SitecoreContext.GetCurrentItem<Item>().GetChildren().Where(x => x.Versions.Count > 0 && x.TemplateID.ToString().Equals(TemplateIds.RedirectLink)).Select(x => SitecoreContext.GetItem<IRedirectLink>(x.ID.ToString()));
            }
            else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.TMPastEventsPage))
            {
                ViewFilters.DataSource = SitecoreContext.GetItem<Item>(ItemIds.TMEventsPage).GetChildren().Where(x => x.Versions.Count > 0 && x.TemplateID.ToString().Equals(TemplateIds.RedirectLink)).Select(x => SitecoreContext.GetItem<IRedirectLink>(x.ID.ToString()));
            }
            else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.SAPastEventsPage))
            {
                ViewFilters.DataSource = SitecoreContext.GetItem<Item>(ItemIds.SAEventsPage).GetChildren().Where(x => x.Versions.Count > 0 && x.TemplateID.ToString().Equals(TemplateIds.RedirectLink)).Select(x => SitecoreContext.GetItem<IRedirectLink>(x.ID.ToString()));
            }
            else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.SDPastEventsPage))
            {
                ViewFilters.DataSource = SitecoreContext.GetItem<Item>(ItemIds.SDEventsPage).GetChildren().Where(x => x.Versions.Count > 0 && x.TemplateID.ToString().Equals(TemplateIds.RedirectLink)).Select(x => SitecoreContext.GetItem<IRedirectLink>(x.ID.ToString()));
            }

            ViewFilters.DataTextField = "MenuTitle";
            ViewFilters.DataValueField = "Id";
            ViewFilters.DataBind();
        }

        protected void BindYearFilters()
        {
            if (ViewFilters.Items.Count > 0)
            {
                if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.HomeEventsPage))
                    YearFilter.DataSource = Sitecore.Context.Database.GetItem(ViewFilters.SelectedValue.ToString()).GetChildren().Where(x => x.Versions.Count > 0 && x.TemplateID.ToString().Equals(TemplateIds.YearFolder)).OrderByDescending(x => Convert.ToInt32(x.Name));
                else
                    YearFilter.DataSource = Sitecore.Context.Database.GetItem(ViewFilters.SelectedValue.ToString()).GetChildren().Where(x => x.Versions.Count > 0 && x.Name != DateTime.Now.Year.ToString() && x.TemplateID.ToString().Equals(TemplateIds.YearFolder)).OrderByDescending(x => Convert.ToInt32(x.Name));
                YearFilter.DataTextField = "Name";
                YearFilter.DataValueField = "Id";
                YearFilter.DataBind();
            }
        }

        protected void BindEvents()
        {
            //OLD CODE REPLACED BY 3chillies AW 19.05.2016
            //if (YearFilter.Items.Count > 0)
            //{
            //    var eventsList = SitecoreContext.GetItem<Item>(YearFilter.SelectedValue).GetChildren().Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<ITradeshows>(x.ID.ToString())).ToList();


            //    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.HomeEventsPage))
            //    {
            //        var tmquery = string.Format("fast:{0}/{1}//*[@@templateid='{2}' and @ShowOnHome='1']",
            //            StringExtensions.EscapePath(SitecoreContext.GetItem<Item>(ItemIds.TradeshowsTM).Paths.FullPath),
            //            YearFilter.SelectedItem.Text, TemplateIds.Tradeshow);

            //        eventsList.AddRange(SitecoreContext.Query<Item>(tmquery)
            //            .Where(x => x.Versions.Count > 0)
            //            .Select(x => SitecoreContext.GetItem<ITradeshows>(x.ID.Guid)).ToList());
            //    }

            //    Events.DataSource = eventsList.Where(x => x != null && !x.StartDate.Equals(DateTime.MinValue)).OrderByDescending(x => x.StartDate).ToList();
            //    Events.DataBind();
            //    EventsDataPager.Visible = true;
            //}


          
             if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.TMEventsPage) || SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.SAEventsPage)|| SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.SDEventsPage))
            {
                divFilter.Visible = false;
                bindTMandSALandingPageEvents();
            }
            else
            {
                
                divFilter.Visible = true;
                //NEW CODE BY 3chillies AW 19.05.2016
                if (YearFilter.Items.Count > 0)
                {
                    Item yearFolder = SitecoreContext.GetItem<Item>(YearFilter.SelectedValue);

                    List<ITradeshows> eventsList = new List<ITradeshows>();

                    //this is to support getting news from two different sections one with the "showonhome" field checked and one without :S.
                    //this is a mistake surely????
                    if (yearFolder != null)
                    {
                        var aboutEvents = yearFolder.GetChildren().Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<ITradeshows>(x.ID.ToString())).ToList();

                        if (aboutEvents.Any())
                        {
                            eventsList.AddRange(aboutEvents);
                        }
                    }

                    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.HomeEventsPage))
                    {
                        EventsSearchContract eventsContract = new EventsSearchContract();
                        eventsContract.TemplateRestrictions.Add(TemplateIds.Tradeshow);
                        eventsContract.RootRestrictions.Add(new ID(ItemIds.TradeshowsTM));
                        eventsContract.RootRestrictions.Add(new ID(ItemIds.TradeshowsSA));

                        int year;
                        if (int.TryParse(YearFilter.SelectedItem.Text, out year))
                        {
                            var start = new DateTime(year, 1, 1);
                            var end = start.AddMonths(12).AddDays(-1);

                            eventsContract.StartDate = start;
                            eventsContract.EndDate = end;
                        }

                        eventsList.AddRange(EventsRepository.SearchEventsByContract(eventsContract)
                            .Where(x => x.Document.GetItem() != null)
                            .Select(x => SitecoreContext.GetItem<ITradeshows>(x.Document.ItemId.ToGuid())).Where(x => x.ShowOnHome));
                       
                    }

                    Events.DataSource = eventsList.Where(x => x != null && !x.StartDate.Equals(DateTime.MinValue)).OrderByDescending(x => x.StartDate).Distinct().ToList();
                    Events.DataBind();
                    EventsDataPager.Visible = true;

                }
            }


            if (Events.Items.Count == 0)
            {
                EventsDataPager.Visible = false;
                divItems.Visible = true;
            }
            else
            {
                divItems.Visible = false;
            }
        }

        protected void YearFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            EventsDataPager.SetPageProperties(0, EventsDataPager.PageSize, true);
            BindEvents();
        }

        protected void ViewFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindYearFilters();
            if (YearFilter.Items.Count > 0)
            {
                YearFilter.SelectedIndex = 0;
            }
            EventsDataPager.SetPageProperties(0, EventsDataPager.PageSize, true);
            BindEvents();
        }

        protected void Events_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            EventsDataPager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindEvents();
        }
    }
}