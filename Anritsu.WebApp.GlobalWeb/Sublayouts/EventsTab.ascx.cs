﻿﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.ContentSearch;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Anritsu.WebApp.SitecoreUtilities.Helpers;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Events : GlassUserControl<IBaseTemplate>
    {
        private EventsRepository _eventsRepository { get; set; }

        public EventsRepository EventsRepository
        {
            get
            {
                if (_eventsRepository == null)
                {
                    _eventsRepository = new EventsRepository();
                }

                return _eventsRepository;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Model == null)
                return;
            try
            {
               BindEvents();
               
            }
            catch (Exception ex)
            {
                //Response.Write("EventsTab Says<br/>" + ex.Message);
                Log.Error(ex.StackTrace, ex, GetType());
            }

        }
        /// <summary>
        /// Read TM and SA Events
        /// </summary>
        private void bindTMandSAtabevents()
        {

            if (DataSource != null)
            {

                //Read the data source (TM or SA Group company Events)
                Item eventstab = Sitecore.Context.Database.GetItem(DataSource);
                if (eventstab != null)
                {
                    //Read the TreeList Values
                    var events = eventstab.Fields["Events Tab"].Value;
                    List<IEvents> lstEvents = new List<IEvents>();
                    if (string.IsNullOrEmpty(events) == false)
                    {
                        //Split the values
                        foreach (string guid in events.Split('|'))
                        {
                            //Check the value, make sure it is not null
                            IEvents eventitem = SitecoreContext.GetItem<IEvents>(guid);
                            if (eventitem != null)
                                lstEvents.Add(eventitem);
                        }
                        //Assign the values to Repeater control
                        EventsList.DataSource = lstEvents;
                        EventsList.DataBind();
                    }
                }
            }
            divItems.Visible = EventsList.Items.Count == 0;

        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private void BindEvents()
        {
            #region OLD CODE Replaced: 2016.05.19 3chillies by AW

            //////old code
            //var currentItem = SitecoreContext.GetItem<Item>(Model.Id);
            //var query = string.Format("fast:{0}//*[(@@templateid='{1}' or @@templateid='{2}' or @@templateid='{3}' or @@templateid='{4}')]", StringExtensions.EscapePath(currentItem.Paths.FullPath), TemplateIds.Tradeshow, TemplateIds.CorporateEvent, TemplateIds.Webinar, TemplateIds.TMEvent);
            //var eventList = SitecoreContext.Query<Item>(query).Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<IEvents>(x.ID.Guid)).Where(x => x.EndDate.Date >= DateTime.Now.Date).OrderBy(x => x.StartDate).Take(5).ToList();
            ////end date>=today order by satrt date take 5
            //Response.Write(query + "<br/>");
            //Response.Write("<br/>A IDS: " + string.Join(", ", eventList.Select(c => c.Id.ToString())) + "<br/>");


            ////check in all of them for this: .. and replace with:
            //if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.Home))
            //{
            //    var item = SitecoreContext.GetItem<Item>(ItemIds.TradeshowsTM);
            //    if (item != null)
            //    {
            //        var tmquery = string.Format("fast:{0}//*[@@templateid='{1}' and @ShowOnHome='1']", StringExtensions.EscapePath(item.Paths.FullPath), TemplateIds.Tradeshow);
            //        eventList.AddRange(SitecoreContext.Query<Item>(tmquery).Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<IEvents>(x.ID.Guid)).Where(x => x.EndDate.Date >= DateTime.Now.Date).OrderBy(x => x.StartDate).Take(5).ToList());

            //        Response.Write("<br/>B IDS: " + string.Join(", ", eventList.Select(c => c.Id.ToString())) + "<br/>");

            //    }
            //}
            //var events = eventList.Where(x => x != null).OrderBy(x => x.EndDate).ThenBy(x => x.StartDate).Take(5).ToList();

            #endregion
          
            //if we are on the websites home page
             if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.Home))
            {


                //3chillies AW: NEW CODE 2016.05.19  

                //create new search contract
                EventsSearchContract eventsContract = new EventsSearchContract()
                {
                    //add template(s) 
                    TemplateRestrictions = new List<String>() { TemplateIds.Tradeshow, TemplateIds.CorporateEvent, TemplateIds.Webinar, TemplateIds.TMEvent },
                    //add root restrictions
                    RootRestrictions = new List<ID>() { new ID(Model.Id) }
                };

                //pass contract to content search method SearchByContract()
                var eventResults = EventsRepository.SearchEventsByContractForTab(eventsContract, 5, 0)
                    //where the sitecore item is not null
                    .Where(x => x.Document.GetItem() != null)
                    //convert content search result items to IEvents glass item
                    .Select(x => SitecoreContext.GetItem<IEvents>(x.Document.GetItem().ID.ToGuid()))
                    //order by start date 
                    .OrderBy(x => x.StartDate)
                    //list it
                    .ToList();


                //get the tradeshow sitecore item: /sitecore/content/GlobalWeb/home/test-measurement/events/tradeshows
                ID TMid;
                ID SAid;
                if (Sitecore.Data.ID.TryParse(ItemIds.TradeshowsTM, out TMid) && Sitecore.Data.ID.TryParse(ItemIds.TradeshowsSA, out SAid))
                {
                    //create new search contract
                    EventsSearchContract eventsTradeshowContract = new EventsSearchContract()
                    {
                        //add template(s)
                        TemplateRestrictions = new List<String>() { TemplateIds.Tradeshow },
                        //check show on home
                        CheckShowOnHome = true,
                        //add root restrictions
                        RootRestrictions = new List<ID>() { TMid ,SAid}
                    };

                    var eventTradeshowResults = EventsRepository.SearchEventsByContractForTab(eventsTradeshowContract, 5, 0)
                        //where the sitecore item is not null
                        .Where(x => x.Document.GetItem() != null)
                        //convert content search result items to IEvents glass item
                        .Select(x => SitecoreContext.GetItem<IEvents>(x.Document.GetItem().ID.ToGuid()))
                        //order results
                        .OrderBy(x => x.StartDate);


                    if (eventTradeshowResults.Any())
                    {
                        eventResults.AddRange(eventTradeshowResults);
                    }
                  
                }
                //eventList
                EventsList.DataSource = eventResults.Where(x => x != null).OrderBy(x => x.EndDate).ThenBy(x => x.StartDate).Distinct().Take(5);
                EventsList.DataBind();
                divItems.Visible = EventsList.Items.Count == 0;


            }
            else 
            {
                bindTMandSAtabevents();
            }


        }

        public string GetMoreLink()
        {
            string url = string.Empty;
            if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.Home)
            {
                var item = SitecoreContext.GetItem<IModelBase>(ItemIds.HomeEventsPage);
                if (item != null) return item.Url;
            }
            else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.TestMeasurement)
            {
                var item = SitecoreContext.GetItem<IModelBase>(ItemIds.TMEventsPage);
                if (item != null) return item.Url;
            }
            else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.ServiceAssurance)
            {
                var item = SitecoreContext.GetItem<IModelBase>(ItemIds.SAEventsPage);
                if (item != null) return item.Url;
            }
            else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.SensingDevices)
            {
                var item = SitecoreContext.GetItem<IModelBase>(ItemIds.SDEventsPage);
                if (item != null) return item.Url;
            }
            return url;
        }
    }
}