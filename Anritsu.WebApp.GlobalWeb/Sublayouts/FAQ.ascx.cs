﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using SessionVariables = Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SessionVariables;
using System.Web;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using Sitecore.Globalization;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class FAQ : GlassUserControl<DownloadSearchItem>
    {
        private IProviderSearchContext _frequentquestionscontext;

        protected IProviderSearchContext FrequentQuestionsSearchContext
        {
            get
            {
                return _frequentquestionscontext ??
                       (_frequentquestionscontext =
                           ContentSearchManager.GetIndex(StaticVariables.FrequentQuestionsIndex).CreateSearchContext());
            }
        }

        private FrequentlyAskedQuestionSearchItem _frequentlyAskedQuestion;

        protected FrequentlyAskedQuestionSearchItem FrequentlyAskedQuestion
        {
            get
            {
                try
                {
                    if (_frequentlyAskedQuestion != null) return _frequentlyAskedQuestion;

                    var sitecoreService = new SitecoreService(Sitecore.Context.Database);

                    var query = FrequentQuestionsSearchContext.GetQueryable<FrequentlyAskedQuestionSearchItem>()
                        .Filter(i => i.Language == Sitecore.Context.Language.Name);

                    object faqItem = null;

                    foreach (var item in query)
                    {
                        if (
                            !item.Name.Equals(Request.Url.Segments[Request.Url.Segments.Length - 1],
                                StringComparison.OrdinalIgnoreCase)) continue;
                        sitecoreService.Map(item);
                        faqItem = item;
                        break;
                    }

                    if (faqItem != null)
                    {
                        _frequentlyAskedQuestion = (FrequentlyAskedQuestionSearchItem) faqItem;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message, GetType());
                }
                return _frequentlyAskedQuestion;
            }
            set { _frequentlyAskedQuestion = value; }
        }

        private IEnumerable<DownloadSearchItem> _frequentQuestionDownloadItems;

        protected IEnumerable<DownloadSearchItem> FrequentQuestionDownloadItems
        {
            get
            {
                if (_frequentQuestionDownloadItems != null) return _frequentQuestionDownloadItems;

                if (FrequentlyAskedQuestion != null)
                {
                    var regionalProducts = new List<Item>();
                    var frequentQuestion = SitecoreContext.GetItem<Item>(FrequentlyAskedQuestion.ItemId.ToString());


                    if (((MultilistField) frequentQuestion.Fields["RelatedProducts"]) != null &&
                        ((MultilistField) frequentQuestion.Fields["RelatedProducts"]).GetItems().Any())
                    {
                        regionalProducts.AddRange(
                            ((MultilistField) frequentQuestion.Fields["RelatedProducts"]).GetItems().ToList());
                    }

                    var downloads = new List<Item>();

                    foreach (var product in regionalProducts)
                    {
                        downloads.AddRange(product.GetLinkedItems(Sitecore.Context.Database, Sitecore.Context.Language,
                            TemplateIds.Downloads));
                    }

                    if (downloads.Count > 0)
                    {
                        _frequentQuestionDownloadItems = downloads.Distinct(new ItemEqualityComparer())
                            .Select(z => SitecoreContext.GetItem<DownloadSearchItem>(z.ID.ToString()));
                    }
                }
                return _frequentQuestionDownloadItems;
            }
            set { _frequentQuestionDownloadItems = value; }
        }

        protected string HideOptionalLinks = string.Empty;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                //Adding <title>
                if (FrequentlyAskedQuestion == null) return;
                var phTitle = (PlaceHolder)Parent.FindControl("TitlePlaceHolder");
                if (phTitle == null) return;

                var htmlTitle = new HtmlTitle
                {
                    Text = FrequentlyAskedQuestion.Question + "- " +
                           Translate.TextByDomain("GlobalDictionary", "anritsucompany")
                };
                phTitle.Controls.Add(htmlTitle);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison",
            MessageId = "System.String.StartsWith(System.String)"),
         System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo",
             MessageId = "System.String.ToLower")]
        private void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                if (GsaHelpers.IsGsaCrawler())
                    AddGsaMetaInfo();

            if (FrequentlyAskedQuestion != null)
            {
                if (!FrequentlyAskedQuestion.Name.Trim().ToLower().StartsWith("faqs"))
                {
                    hdnFaqId.Value = FrequentlyAskedQuestion.Name.ToLower().Replace("faq", "");
                    hdnQuestion.Value = HttpUtility.UrlEncode(FrequentlyAskedQuestion.Question);
                    hdnAnswer.Value = HttpUtility.UrlEncode(FrequentlyAskedQuestion.Answer);

                    // Put user code to initialize the page here
                    BindFaqOptionalLinks();
                    BindDownloadCategories();
                }
                else
                {
                    throw new HttpException(404, "Page not found.");
                }
            }
            else
            {
                throw new HttpException(404, "Page not found.");
            }
        }

        private void BindDownloadCategories()
        {
            if (FrequentlyAskedQuestion == null) return;
            if (FrequentQuestionDownloadItems == null || !FrequentQuestionDownloadItems.Any())
            {
                relatedDocument.Visible = false;
                return;
            }

            var downloadCategories = FrequentQuestionDownloadItems.Where(
                x => x != null && x.Subcategory != null && x.Subcategory.ParentItem != null)
                .Select(x => SitecoreContext.GetItem<IDropListItem>(x.Subcategory.ParentItem.Id.ToString()));

            var dropListItems = downloadCategories as IList<IDropListItem> ?? downloadCategories.ToList();

            if (!dropListItems.Any()) return;

            var result = new List<IDropListItem>();
            foreach (var download in dropListItems.Where(download => !result.Any(x => x.Key.Equals(download.Key))))
            {
                result.Add(download);
            }

            DownloadCategoryTypes.DataSource = result.Count > 0
                ? result.Where(
                    x => SitecoreContext.GetItem<Item>(x.Id.ToString()).ID.ToString().Equals(ItemIds.DownloadManuals)
                         ||
                         SitecoreContext.GetItem<Item>(x.Id.ToString()).ID.ToString().Equals(ItemIds.DownloadSoftware)
                         ||
                         SitecoreContext.GetItem<Item>(x.Id.ToString()).ID.ToString().Equals(ItemIds.DownloadBrochures))
                : null;

            DownloadCategoryTypes.DataBind();

            if (result.Count <= 0)
            {
                OtherDownloadCategoryTypes.Visible = false;

                return;
            }
            OtherDownloadCategoryTypes.DataSource =
                result.Where(
                    x => !SitecoreContext.GetItem<Item>(x.Id.ToString()).ID.ToString().Equals(ItemIds.DownloadManuals)
                         &&
                         !SitecoreContext.GetItem<Item>(x.Id.ToString()).ID.ToString().Equals(ItemIds.DownloadSoftware)
                         &&
                         !SitecoreContext.GetItem<Item>(x.Id.ToString()).ID.ToString().Equals(ItemIds.DownloadBrochures));

            OtherDownloadCategoryTypes.DataBind();
            if (OtherDownloadCategoryTypes.Items.Count <= 0)
            {
                relatedDocument.Visible = false;
            }
        }

        private void BindFaqOptionalLinks()
        {
            if (FrequentlyAskedQuestion != null)
            {
                Item currentItem = SitecoreContext.GetItem<Item>(FrequentlyAskedQuestion.ItemId.ToString());
                if (currentItem == null)
                    return;
                if (currentItem.Fields["OptionalLinks"] != null)
                {
                    GeneralLinks relatedGeneralLinks = new GeneralLinks(currentItem, "OptionalLinks");
                    FaqOptionalLinks.DataSource = relatedGeneralLinks.LinkItems;
                    FaqOptionalLinks.DataBind();
                }
                HideOptionalLinks = FaqOptionalLinks.Items.Count > 0 ? string.Empty : "hiddentxt";
            }
        }

        protected void FaqOptionalLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem) e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor) e.Item.FindControl("otherLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }

        protected IEnumerable<IDropListItem> BindDownloads(string categoryType)
        {

            var downloadCategories = FrequentQuestionDownloadItems.Where(x => x != null && x.Subcategory != null)
                .Select(x => SitecoreContext.GetItem<IDropListItem>(x.Subcategory.Id.ToString()))
                .Where(x => SitecoreContext.GetItem<IDropListItem>(x.ParentItem.Id.ToString()).Key.Equals(categoryType));

            var result = new List<IDropListItem>();
            foreach (var download in downloadCategories.Where(download => !result.Any(x => x.Key.Equals(download.Key))))
            {
                result.Add(download);
            }
            return result;
        }

        protected IEnumerable<DownloadSearchItem> BindDownloadList(string categoryKey)
        {
            if (FrequentQuestionDownloadItems == null || !FrequentQuestionDownloadItems.Any())
                return Enumerable.Empty<DownloadSearchItem>();

            var downloadList =
                FrequentQuestionDownloadItems.Where(
                    x =>
                        x != null && x.Subcategory != null && x.Subcategory.Key != null &&
                        x.Subcategory.Key.Equals(categoryKey)).ToList();
            if (downloadList.Count <= 0)
            {
                relatedDocument.Visible = false;
            }
            return downloadList;
        }

        protected string GetFileName(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                return "http://" + ConfigurationManager.AppSettings["DownloadDistributionList"] + "/" + filePath;
            }
            return string.Empty;
        }

        private void AddGsaMetaInfo()
        {
            try
            {
                var ltMetaTags = (PlaceHolder) Parent.FindControl("GsaMetaPlaceHolder");
                if (ltMetaTags == null) return;
                var faqItem = Sitecore.Context.Database.GetItem(FrequentlyAskedQuestion.ItemId);
                var meta = new HtmlMeta
                {
                    Name = "title",
                    Content = SiteSearchItemName.GetGsaItemName(faqItem)
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "gsadescription",
                    Content = SiteSearchMetaDescription.GetGsaItemMetaDescription(faqItem)
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                var area = SiteSearchArea.GetGsaArea(faqItem);
                meta = new HtmlMeta
                {
                    Name = "area",
                    Content = area
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                var coveoarea = SiteSearchArea.GetGsaArea(faqItem, Language.Parse("en"));
                meta = new HtmlMeta
                {
                    Name = "coveoarea",
                    Content = coveoarea
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "keywords",
                    Content = FrequentlyAskedQuestion.Keyword
                };
                ltMetaTags.Controls.Add(meta);

                var categories = SiteSearchItemCategory.GetGsaItemCategory(faqItem);
                foreach (var category in categories)
                {
                    meta = new HtmlMeta
                    {
                        Name = "category",
                        Content = !string.IsNullOrWhiteSpace(category) ? area.Substring(0,2) + GsaMetaStringSeperators.AreaCategorySeparator + category : string.Empty
                    };
                    if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                }

                var subcategories = SiteSearchItemSubcategory.GetGsaItemSubcategory(faqItem);
                foreach (var subcategory in subcategories)
                {
                    meta = new HtmlMeta
                    {
                        Name = "subcategory",
                        Content = !string.IsNullOrWhiteSpace(subcategory) ? area.Substring(0,2) + GsaMetaStringSeperators.AreaCategorySeparator + subcategory : string.Empty
                    };
                    if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                }
               
                //meta = new HtmlMeta
                //{
                //    Name = "last-modified",
                //    Content = FrequentlyAskedQuestion.Updated.ToString("yyyy-MM-dd")
                //};
                //if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " " + ex.StackTrace, "GsaMetaInfoAddition");
            }
        }
    }
}