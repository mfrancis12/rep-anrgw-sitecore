﻿using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using System;

    public partial class FaqSearch : System.Web.UI.UserControl
    {
        private IProviderSearchContext _frequentquestionscontext;
        protected IProviderSearchContext FrequentQuestionsSearchContext
        {
            get
            {
                return this._frequentquestionscontext ?? (this._frequentquestionscontext = ContentSearchManager.GetIndex(StaticVariables.FrequentQuestionsIndex).CreateSearchContext());
            }
        }
        private void Page_Load(object sender, EventArgs e)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var faqList = new List<FrequentlyAskedQuestionSearchItem>();

            var query = FrequentQuestionsSearchContext.GetQueryable<FrequentlyAskedQuestionSearchItem>().Filter(i => i.Language == Sitecore.Context.Language.Name);

            foreach (var item in query)
            {
                sitecoreService.Map(item);
                faqList.Add(item);
            }
            faqList = faqList.Where(x => x != null).ToList();

            foreach (var item in faqList)
            {
                var position = item.Path.LastIndexOf("/", StringComparison.Ordinal) + 1;
                Response.Write(string.Format("<a href='{0}' style='{2}'>{1}</a>", "/" + Sitecore.Context.Language.Name + "/test-measurement/support/downloads/faq/" +
                    item.Path.Substring(position, item.Path.Length - position), item.Question, "font-weight:bold") + "<br/>");
            }
        }
    }
}