﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using System.Web;
using Anritsu.Library.GWCMSDK;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Footer : GlassUserControl<IModelBase>
    {
        private string _dataSource;

        public string DataSource
        {

            get
            {
                if (_dataSource == null)
                {
                    var parent = Parent as Sublayout;
                    if (parent != null)
                        _dataSource = (parent.DataSource);
                }
                return _dataSource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                FooterLinks.DataSource = SitecoreContext.GetItem<IModelBase>(ItemIds.FooterLinks).GetChildren<ILink>();
                FooterLinks.DataBind();

                string[] msplit = DataSource.Split('_');
                string fatfooterdatasource = "";
                string snsfooterdatasource = "";
                if(msplit.Count() > 1)
                {
                    fatfooterdatasource = msplit[0];
                    snsfooterdatasource = msplit[1];
                }
                else
                {
                    fatfooterdatasource = msplit[0];
                }

                List<IDropListItem> fatfooterTitle = new List<IDropListItem>();
                string[] split = fatfooterdatasource.Split('|');
                foreach (string datasource in split)
                {

                    fatfooterTitle.Add(SitecoreContext.GetItem<IDropListItem>(datasource));
                    FatFooter.DataSource = fatfooterTitle;
                    FatFooter.DataBind();
                }
                if (snsfooterdatasource != "")
                {
                    List<IImageTitleWithLink> snsfooterTitle = new List<IImageTitleWithLink>();
                    string[] snssplit = snsfooterdatasource.Split('|');
                    foreach (string datasource in snssplit)
                    {
                        try
                        {
                            IImageTitleWithLink lnk = SitecoreContext.GetItem<IImageTitleWithLink>(datasource, Sitecore.Context.Language);
                            if (lnk != null && lnk.Image.Src !=null && lnk.Link.Url != null)
                            {
                                snsfooterTitle.Add(lnk);
                                SNSFooter.DataSource = snsfooterTitle;
                                SNSFooter.DataBind();
                            }
                        }
                        catch(Exception ex)
                        {
                            Sitecore.Diagnostics.Log.Error(ex.Message, ex);
                        }
                    }
                }

                //displayToolbar();

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message,ex);
                Visible = false;
            }

        }
        public IEnumerable<ILink> BindData(IModelBase item)
        {
            var links = item.GetChildren<ILink>().Take(7);
            return links;
        }

        //private void displayToolbar()
        //{
        //    try
        //    {
        //        var url = HttpContext.Current.Request.Url.AbsolutePath;

        //        if (GWCMServiceManager.ShowToolbar(url))
        //        {
        //            toolbar.Visible = true;
        //        }
        //        else
        //        {
        //            toolbar.Visible = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Sitecore.Diagnostics.Log.Error(ex.Message,this);

        //    }
        //}
        protected string GetShareLink(string social)
        {
            string link = "";
            if(social.ToUpper()=="LINKEDIN")
            {
                link = "http://www.linkedin.com/shareArticle?mini=true&url=" + HttpContext.Current.Request.Url.AbsoluteUri;
            }
            else if (social.ToUpper() == "TWITTER")
            {
                link = "http://twitter.com/share?url=" + HttpContext.Current.Request.Url.AbsoluteUri;
            }
            else if (social.ToUpper() == "FACEBOOK")
            {
                link = "http://www.facebook.com/sharer.php?u=" + HttpContext.Current.Request.Url.AbsoluteUri;
            }
            else if (social.ToUpper() == "EMAIL")
            {
                link = "mailto:?body=" + HttpContext.Current.Request.Url.AbsoluteUri;
            }
            return link;

        }
        protected string GetClass(ILink item)
        {
            if (item.Link != null && !string.IsNullOrEmpty(item.Link.Target))
            {
                if (item.Link.Target.ToUpperInvariant().Equals("NEW BROWSER"))
                {
                    return "icon icon-outbound";
                }
            }
            return string.Empty;
        }


    }

}
