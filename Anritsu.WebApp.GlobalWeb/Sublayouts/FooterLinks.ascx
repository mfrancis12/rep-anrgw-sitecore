﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterLinks.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.FooterLinks" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<%@ Import Namespace="Sitecore.Data.Items" %>

<!--googleoff: all-->
<div class="footer">
    <div class="container">
        <div class="min-container">
             <div class="site-info">
                <div class="left">
                    <asp:Repeater runat="server" ID="Footer" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.ILink">
                        <ItemTemplate>
                           
                            <%# RenderLink(Item,x=>x.Link, isEditable: true)%>
                        </ItemTemplate>
                    </asp:Repeater>

                     <%--<sc:Text runat="server" ID="addThis" Field="Script" DataSource="/sitecore/content/GlobalWeb/components/Scripts/share"></sc:Text>--%>
                </div>

                <div class="copy-right">
                    <div class="icon icon-logo-small"></div>
                    <br>
                    <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Copyright")%> <%=DateTime.Now.ToString("yyyy") %>                   
                    <br>
                    <%if (SitecoreContext.GetCurrentItem<Sitecore.Data.Items.Item>()!=null 
                    && SitecoreContext.GetCurrentItem<Sitecore.Data.Items.Item>().Language.Name.ToUpperInvariant().Equals("ZH-CN") 
                    && !string.IsNullOrEmpty(SitecoreContext.GetItem<Sitecore.Data.Items.Item>(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.ICPNumber)["Phrase"]))
                  { %>
                <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "chinese-only-contact-group-company-information") %>
                <% }  %>
                </div>
            </div>
        </div>
    </div>
</div>
<!--googleon: all-->

