﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System;
using System.Linq;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data;
using Anritsu.WebApp.GlobalWeb.SiteSearch;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class FooterLinks : GlassUserControl<IModelBase>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (SiteSearchHelper.IsSearchCrawler(Request))
                return;

            var parameters = GetRenderingParameters<IStandardParameters>();
            List<ILink> links = SitecoreContext.GetItem<IModelBase>(ItemIds.FooterLinks).GetChildren<ILink>().ToList();
            if (parameters != null && parameters.UseDomainUrl)
            {               
                foreach (ILink link in links)
                {
                    if (link !=null && link.Link != null)
                    {   
                        string[] segments = link.Link.Url.Split('/');
                        if(segments.Count()>0)
                        link.Link.Url = this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/" + Sitecore.Context.Language +"/"+ segments[segments.Count()-1];                       
                    }
                }
            }
            Footer.DataSource = links;
            Footer.DataBind();          
        }
    }    
}