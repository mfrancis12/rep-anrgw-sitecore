﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormContainer.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.FormContainer" %>

<!--googleoff: all-->
<div class="container">
    <div class="content-form">
        <div class="content-detail">
            <sc:Placeholder runat="server" Key="phForm" />
        </div>
        <div class="content-aside">
            <asp:Repeater ID="OptionalLinks" runat="server" OnItemDataBound="OptionalLinks_ItemDataBound" OnPreRender="OptionalLinks_PreRender">
                <HeaderTemplate>
                    <h3>  <%=GetLinksTitle%></h3>
                </HeaderTemplate>
                <ItemTemplate>
                    <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
<!--googleon: all-->