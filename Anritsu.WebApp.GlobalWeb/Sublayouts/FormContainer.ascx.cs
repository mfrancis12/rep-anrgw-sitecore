﻿using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using System;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections.Generic;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class FormContainer : GlassUserControl<IOptionalLinks>
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            // check if secure connection used
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                // redirect visitor to SSL connection
                Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Bind Optional Links

            if (Model != null && Model.Links != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(SitecoreContext.Database.GetItem(Model.Id.ToString()), "Links");

                if (relatedGeneralLinks != null && relatedGeneralLinks.LinkItems.Count > 0)
                {
                    OptionalLinks.DataSource = relatedGeneralLinks.LinkItems;
                    OptionalLinks.DataBind();
                }
            }
            #endregion
        }

        public string GetLinksTitle
        {
            get
            {
                string title = string.Empty;
                Item item = SitecoreContext.Database.GetItem(Model.Id.ToString());
                if (item != null)
                    title = item["LinksTitle"];
                return title;
            }
        }

        protected void OptionalLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
            if (linkItem != null && relatedAnchor != null)
            {
                relatedAnchor.InnerText = linkItem.LinkText;
                relatedAnchor.HRef = linkItem.Url;
                relatedAnchor.Target = linkItem.Target;
            }
        }

        protected void OptionalLinks_PreRender(object sender, EventArgs e)
        {
            if (OptionalLinks.Items.Count == 0)
            {
                OptionalLinks.Visible = false;
            }
        }
    }
}