﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalSearch.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.GlobalSearch" %>
<%@ Import Namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Constants" %>

<asp:ScriptManager ID="searchScriptManager" runat="server"></asp:ScriptManager>
<div class="full-container global-search nogap">
    <div class="container nogap">
        <div class="hd-left">
            <h1><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "searchresult") %></h1>
        </div>
        <div class="hd-right">
            <asp:TextBox ID="SearchTextInput" runat="server" placeholder="Enter Keywords"></asp:TextBox>
            <asp:Button ID="SearchButton" runat="server" CssClass="searchResBtn" OnClick="SearchButton_Click" />
        </div>
    </div>
</div>

<div class="full-container search-result-body">
    <div class="container nogap">

        <div class="search-filter-left">
            <div class="result-filter">
                <div class="hd"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "sitesection") %></div>
                <ul class="bd searchLevel globalfacet">
                    <asp:Repeater ID="SearchFacetAreaList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                        <ItemTemplate>
                            <li class="level1">
                                <div class="level1-item">
                                    <asp:HiddenField ID="hiddenAreaFacet" runat="server" Value="<%#Item.Value%>" />
                                    <a class="filterLinks">
                                        <label>
                                            <asp:CheckBox ID="checkboxAreaFacet" runat="server" CssClass="areaFilter" />
                                            <span class="level1hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span>
                                        </label>
                                    </a>
                                    <div class="icon icon-arrow-down-grey"></div>
                                </div>
                                <ul class="level2_ul">
                                    <asp:Repeater ID="SearchFacetAreaCategoryList" runat="server" DataSource='<%# GetCategoryFacets(Item) %>' ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                        <ItemTemplate>
                                            <li class="level2">
                                                <asp:HiddenField ID="hiddenAreaCategoryFacet" runat="server" Value="<%#Item.Value%>" />
                                                <a class="filterLinks">
                                                    <label>
                                                        <asp:CheckBox ID="checkboxAreaCategoryFacet" runat="server" CssClass="areaFilter" />
                                                        <span class="level2hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span>
                                                    </label>
                                                </a>
                                                <span class="icon icon-plus-grey iconStyles"></span>
                                                <ul class="level3_ul">
                                                    <asp:Repeater ID="SearchFacetAreaSubcategoryList" runat="server" DataSource='<%# GetSubcategoryFacets(Item) %>' ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                                        <ItemTemplate>
                                                            <li class="level3">
                                                                <asp:HiddenField ID="hiddenAreaSubcategoryFacet" runat="server" Value="<%#Item.Value%>" />
                                                                <a class="filterLinks">
                                                                    <label>
                                                                        <asp:CheckBox ID="checkboxAreaSubcategoryFacet" runat="server" CssClass="areaFilter" />
                                                                        <span class="level3hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span>
                                                                    </label>
                                                                </a>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <div class="clearall align-right"><a href="javascript:void(0)" id="searchClearAll"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "clearall") %></a></div>
            </div>
        </div>

        <asp:UpdatePanel ID="searchFacetUpdatePanel" runat="server">
            <ContentTemplate>
                <div class="search-result-right">
                    <div style="border-bottom: 0">
                        <asp:ListView ID="SearchResultList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.SiteSearch.SiteSearchResultItem">
                            <ItemTemplate>
                                <div class="item">
                                    <h2 class="search_Heading">
                                        <img alt="pdf" src="<%= CDNPath %>/appfiles/img/icons/pdf.gif"
                                            class="<%# Item.ItemUrl.EndsWith("pdf",StringComparison.OrdinalIgnoreCase) ||
                                            (Item.TemplateId.ToString().Equals(TemplateIds.Downloads) && !string.IsNullOrEmpty(Item.SearchDownloadFileExtension)
                                            && Item.SearchDownloadFileExtension.EndsWith("pdf", StringComparison.OrdinalIgnoreCase))? string.Empty : "visibleHide" %>" />
                                        <a class="search_Heading_Link" target="<%# Item.TemplateId.ToString().Equals(TemplateIds.Downloads) ? "_blank":"_self" %>" href="<%# Item.ItemUrl %>"><%# Item.ItemName %></a></h2>
                                    <p class="paddingLft25"><%# GetSearchResultDescription(Item) %></p>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div class="pagination">
                        <span class="page">
                            <span class="info"><%= GetPaginationName() %></span>
                            <asp:PlaceHolder ID="placeHolderPager" runat="server"></asp:PlaceHolder>
                        </span>
                    </div>
                </div>
                <asp:Button ID="buttonFacetSearch" ClientIDMode="Static" CssClass="dispNone" runat="server" Text="search" OnClick="buttonFacetSearch_Click" />
            </ContentTemplate>

        </asp:UpdatePanel>
    </div>
</div>
<div class="search-empty dispNone">
    <%=Editable(x=>x.Content) %>
</div>

