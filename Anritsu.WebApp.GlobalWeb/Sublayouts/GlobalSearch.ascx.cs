﻿using Sitecore.ContentSearch;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using FacetValue = Sitecore.ContentSearch.Linq.FacetValue;
using SearchFacetValue = Anritsu.WebApp.GlobalWeb.Search.FacetValue;
using Anritsu.WebApp.GlobalWeb.Search;
using Sitecore.Diagnostics;
using System.Web.UI.WebControls;
using Sitecore.ContentSearch.Linq.Utilities;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using System.Linq.Expressions;
using Sitecore.ContentSearch.Security;
using Glass.Mapper.Sc.Web.Ui;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Constants;
using System.Web;
using Lucene.Net.Search;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using System.Text.RegularExpressions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class GlobalSearch : GlassUserControl<IStaticText>
    {
        #region Global Variables

        private List<string> _SelectedFacets;
        public List<string> SelectedFacets
        {
            get
            {
                if (_SelectedFacets == null)
                {
                    List<string> facets = new List<string>();
                    return facets;
                }
                return (List<string>)_SelectedFacets;
            }
            set { _SelectedFacets = value; }
        }

        private List<SearchFacetValue> _CategoryFacets;
        public List<SearchFacetValue> CategoryFacets
        {
            get
            {
                if (_CategoryFacets == null)
                {
                    List<SearchFacetValue> facets = new List<SearchFacetValue>();
                    return facets;
                }
                return (List<SearchFacetValue>)_CategoryFacets;
            }
            set { _CategoryFacets = value; }
        }

        private List<SearchFacetValue> _SubcategoryFacets;
        public List<SearchFacetValue> SubCategoryFacets
        {
            get
            {
                if (_SubcategoryFacets == null)
                {
                    List<SearchFacetValue> facets = new List<SearchFacetValue>();
                    return facets;
                }
                return (List<SearchFacetValue>)_SubcategoryFacets;
            }
            set { _SubcategoryFacets = value; }
        }

        private IProviderSearchContext _globalsearchcontext;
        protected IProviderSearchContext GlobalSearchContext
        {
            get
            {
                return this._globalsearchcontext ?? (this._globalsearchcontext = ContentSearchManager.GetIndex("anritsu_globalsearch").CreateSearchContext(SearchSecurityOptions.DisableSecurityCheck));
            }
        }

        public string SearchTerm
        {
            get
            {
                if (Request.QueryString["q"] != null)
                {
                    return HttpUtility.HtmlDecode(HttpUtility.UrlDecode(Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.IndexOf("q=") + 2, Request.Url.AbsoluteUri.Length - Request.Url.AbsoluteUri.IndexOf("q=") - 2)));
                }
                else return null;
            }
        }

        public int TotalRecords
        {
            get
            {
                object o = ViewState["TotalRecords"];
                if (o == null)
                    return 0;
                return (int)o;
            }
            set { ViewState["TotalRecords"] = value; }
        }

        public int PageIndex
        {
            get
            {
                object o = ViewState["PageIndex"];
                if (o == null)
                    return 1;
                return (int)o;
            }
            set { ViewState["PageIndex"] = value; }
        }

        public int RecordsPerPage
        {
            get
            {
                object o = ViewState["RecordsPerPage"];
                if (o == null)
                    return 20;
                return (int)o;
            }
            set { ViewState["RecordsPerPage"] = value; }
        }

        protected string CDNPath
        {
            get { return string.Format("//{0}", ConfigurationManager.AppSettings["AWSCdnPath"]); }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PageIndex = 1;
                if (!string.IsNullOrEmpty(Request.QueryString["q"]))
                {
                    SearchTextInput.Text = SearchTerm;
                    BindSearchResults();
                }
            }

            if (TotalRecords > 0)
            { CreatePagingControl(); }

            SearchButton.Text = Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Search");
        }

        #region Get Search Results

        private void BindSearchResults(bool facets = true)
        {
            if (SearchTerm.Length < 2)
                return;

            var query = GlobalSearchContext.GetQueryable<SiteSearchResultItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(x => x.Language.Equals(Sitecore.Context.Language.Name));

            int maxClauseCount = 1024;
            int.TryParse(Sitecore.Configuration.Settings.GetSetting("GlobalSearch.LuceneQueryClauseCount", "1024"), out maxClauseCount);
            BooleanQuery.MaxClauseCount = maxClauseCount;

            var predicate = GetSiteSearchRefinementPredicate();
            query = query.Where(predicate);

            var searchResults = query.Page(PageIndex - 1, RecordsPerPage).GetResults();
            TotalRecords = searchResults.TotalSearchResults;

            if (TotalRecords > 0)
            {
                CreatePagingControl();
            }

            SearchResultList.DataSource = searchResults.Hits.Select(x => x.Document).OrderBy(p => p.IsDiscontinuedProduct).ToList();
            SearchResultList.DataBind();

            if (facets)
            {
                var areaFacets = query.FacetOn(y => y.SiteArea).GetFacets();
                CategoryFacets = CollectFacets(query.FacetPivotOn(x => x.FacetOn(y => y.SiteArea).FacetOn(y => y.SiteAreaCategory)).GetFacets().Categories).FirstOrDefault().FacetValues.ToList();
                SubCategoryFacets = CollectFacets(query.FacetPivotOn(x => x.FacetOn(y => y.SiteArea).FacetOn(y => y.SiteAreaCategory).FacetOn(y => y.SiteAreaSubcategory)).GetFacets().Categories).FirstOrDefault().FacetValues.ToList();

                var firstOrDefault = CollectFacets(areaFacets.Categories).FirstOrDefault();
                if (firstOrDefault != null)
                {
                    List<string> sortAreaList = Sitecore.Context.Database.GetItem("{5F38D725-4DF7-4522-A668-8DF8F56BD873}").Children.Select(x => x["Value"]).ToList();

                    var results = firstOrDefault.FacetValues;
                    results = results.OrderBy(d => sortAreaList.IndexOf(d.DisplayName)).ToList();

                    SearchFacetAreaList.DataSource = results.Where(x => !x.DisplayName.Equals("Other"));
                    SearchFacetAreaList.DataBind();
                }
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Item) + "?q=" + SearchTextInput.Text);
        }

        protected Expression<Func<SiteSearchResultItem, bool>> GetSiteSearchRefinementPredicate()
        {
            string searchTerm = SearchTerm.ToLowerInvariant().Trim();
            var predicate = PredicateBuilder.True<SiteSearchResultItem>();

            if (searchTerm.Length > 1)
            {
                var predicateSearchTerm = PredicateBuilder.False<SiteSearchResultItem>();

                predicateSearchTerm = predicateSearchTerm.Or(p => p.ItemName.Contains(searchTerm).Boost(10.0f))
                                                        .Or(p => p.ItemMetaDescription.Contains(searchTerm).Boost(6.0f))
                                                        .Or(p => p.ItemMetaKeywords.Contains(searchTerm).Boost(4.0f))
                                                        .Or(p => p.ItemContent.Matches(searchTerm).Boost(2f));
                                                        //.Or(p => (p.IsDiscontinuedProduct == false).Boost(20.0f));

                if (searchTerm.ToLowerInvariant().Trim().Split(' ').Length > 1)
                {
                    foreach (var t in searchTerm.ToLowerInvariant().Trim().Split(' '))
                    {
                        if (t.Length > 2 && !IsStopWord(t))
                        {
                            predicateSearchTerm = predicateSearchTerm.Or(p => p.ItemName.Contains(t).Boost(8.0f))
                                                                .Or(p => p.ItemMetaDescription.Contains(t).Boost(5.0f))
                                                                .Or(p => p.ItemMetaKeywords.Contains(t).Boost(3.0f))
                                                                .Or(p => p.ItemContent.Matches(Regex.Replace(t, "[^a-zA-Z0-9]+", "")).Boost(1f));
                                                                //.Or(p => (p.IsDiscontinuedProduct == false).Boost(20.0f));
                        }
                    }
                }

                predicate = predicate.And(predicateSearchTerm);
            }

            List<string> areaList = new List<string>();
            List<string> categoryList = new List<string>();
            List<string> subcategoryList = new List<string>();

            var predicateArea = PredicateBuilder.True<SiteSearchResultItem>();
            foreach (string fItem in SelectedFacets)
            {
                if (fItem.Split('/').Count() > 2)
                {
                    string areaName = fItem.Split('/')[0].ToString();
                    predicateArea = predicateArea.Or(p => p.SiteArea.Equals(areaName));

                    var predicateCategory = PredicateBuilder.True<SiteSearchResultItem>();
                    foreach (var category in fItem.Split('/')[1].ToString().Split('|').Where(x => !x.Equals("category")))
                    {
                        predicateCategory = predicateCategory.Or(p => p.SiteAreaCategory.Contains(category));
                    }
                    predicateArea = predicateArea.And(predicateCategory);

                    var predicatesubcategoryList = PredicateBuilder.True<SiteSearchResultItem>();
                    foreach (var subcategory in fItem.Split('/')[2].ToString().Split('|').Where(x => !x.Equals("subcategory")))
                    {
                        predicatesubcategoryList = predicatesubcategoryList.Or(p => p.SiteAreaSubcategory.Contains(subcategory));
                    }
                    predicateArea = predicateArea.And(predicatesubcategoryList);
                }
                else
                {
                    predicateArea = predicateArea.Or(p => p.SiteArea.Equals(fItem));
                }
            }

            predicate = predicate.And(predicateArea);

            return predicate;
        }

        #endregion

        #region Facets

        private IEnumerable<IFacet> CollectFacets(IEnumerable<FacetCategory> facetCategoryList)
        {

            var list = new List<Facet>();
            try
            {
                list.AddRange(from resultFacetCategory in facetCategoryList
                              let neestedFacetNames = resultFacetCategory.Name.Split(',')
                              select new Facet()
                              {
                                  DisplayName = neestedFacetNames[0],
                                  FieldName = neestedFacetNames[0],
                                  FacetValues = CollectFacetValues(resultFacetCategory.Values, neestedFacetNames, 0, new List<string>()),
                              });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }
            return list;
        }

        private IEnumerable<IFacet> CollectNestedFacets(List<FacetValue> facetValueList, string[] neestedFacetNames, int level, List<string> parentPathList)
        {
            var list = new List<Facet>();
            try
            {
                var parentPath = string.Join("/", parentPathList.ToArray());

                list.AddRange(facetValueList.Where(d => d.Name.StartsWith(parentPath, StringComparison.OrdinalIgnoreCase))
                    .Select(d => d.Name)
                    .Select(s => s.Substring(parentPath.Length))
                    .Where(s => s.Length > 0)
                    .Select(s => s.Substring(0, (s.IndexOf('/') < 0) ? s.Length : s.IndexOf('/')))
                    .Distinct().Select(resultFacetCategory => new Facet()
                    {
                        DisplayName = neestedFacetNames[level], // todo: use name resolver
                        FieldName = neestedFacetNames[level],
                        FacetValues = CollectFacetValues(facetValueList, neestedFacetNames, level, parentPathList)
                    }));
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }

            return list;
        }

        private IEnumerable<SearchFacetValue> CollectFacetValues(List<FacetValue> facetValueList, string[] neestedFacetNames, int level, List<string> parentPathList)
        {
            var list = new List<SearchFacetValue>();
            try
            {
                var parentPath = string.Join("/", parentPathList.ToArray());

                if (level >= neestedFacetNames.Length)
                    return list;

                foreach (var resultFacetValue in
                    facetValueList
                        .Where(d => d.Name.StartsWith(parentPath, StringComparison.OrdinalIgnoreCase))
                        .Select(d => d.Name.Split('/'))
                        .Where(l => l.Count() > parentPathList.Count())
                        .Select(l => l.Skip(parentPathList.Count()).First())
                        .Distinct())
                {
                    var nextLevelPathList = new List<string>(parentPathList);
                    nextLevelPathList.Add(resultFacetValue);
                    var nextLevelPath = string.Join("/", nextLevelPathList.ToArray());

                    var sum = facetValueList.Where(d => d.Name.StartsWith(nextLevelPath, StringComparison.OrdinalIgnoreCase)).Select(d => d.AggregateCount).Sum();

                    list.Add(new Search.FacetValue()
                    {
                        DisplayName = ResolveFacetValueName(resultFacetValue),
                        Value = resultFacetValue,
                        ParentId = ResolveFacetParent(resultFacetValue),
                        SiteArea = parentPath.Split('/')[0],
                        DocumentCount = sum,
                        NestedFacets = CollectNestedFacets(facetValueList, neestedFacetNames, level + 1, nextLevelPathList)
                    });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }

            return list;
        }

        public string ResolveFacetValueName(string name)
        {
            Guid id;
            if (!Guid.TryParse(name, out id)) { return Sitecore.Context.Culture.TextInfo.ToTitleCase(name); }

            var item = Sitecore.Context.Database.GetItem(new ID(id));
            if (item != null && item.TemplateID.ToString().Equals("{E7B62AFB-B727-4257-B6EE-67FA097E8417}"))
            {
                return item["Value"];
            }
            else if (item != null && item.TemplateID.ToString().Equals(TemplateIds.DropListItem) && !string.IsNullOrEmpty(item["Value"]))
            {
                return item["Value"];
            }
            else if (item != null && item.Fields["PageTitle"] != null && !string.IsNullOrEmpty(item["PageTitle"]))
            {
                return item["PageTitle"];
            }
            else if (item != null && item.Fields["MenuTitle"] != null && !string.IsNullOrEmpty(item["MenuTitle"]))
            {
                return item["MenuTitle"];
            }

            return Sitecore.Context.Culture.TextInfo.ToTitleCase(item != null ? item.Name : name);
        }

        public string ResolveFacetParent(string id)
        {
            string parentid = string.Empty;

            if (!string.IsNullOrEmpty(id))
            {
                Guid gid;
                if (!Guid.TryParse(id, out gid)) { return parentid; }

                Item item = Sitecore.Context.Database.GetItem(new ID(gid));
                if (item != null && item.ParentID.ToString() != null)
                {
                    parentid = item.ParentID.ToShortID().ToString().ToLowerInvariant();
                }
            }

            return parentid;
        }

        #endregion

        #region Search Filters

        private void GetSelectedFacets()
        {
            List<string> selectedFacets = new List<string>();

            foreach (RepeaterItem item in SearchFacetAreaList.Items)
            {
                string area = string.Empty;
                string category = string.Empty;
                string subcategory = string.Empty;

                CheckBox areaCheckbox = (CheckBox)item.FindControl("checkboxAreaFacet");
                if (areaCheckbox.Checked)
                {
                    selectedFacets.Add(((HiddenField)item.FindControl("hiddenAreaFacet")).Value);
                    continue;
                }
                else
                {
                    area = ((HiddenField)item.FindControl("hiddenAreaFacet")).Value;
                }

                Repeater categoryRepeater = (Repeater)item.FindControl("SearchFacetAreaCategoryList");
                foreach (RepeaterItem categoryItem in categoryRepeater.Items)
                {
                    CheckBox categoryCheckbox = (CheckBox)categoryItem.FindControl("checkboxAreaCategoryFacet");
                    if (categoryCheckbox.Checked)
                    {
                        string id = ((HiddenField)categoryItem.FindControl("hiddenAreaCategoryFacet")).Value;
                        if (!string.IsNullOrEmpty(id))
                        {
                            if (string.IsNullOrEmpty(category))
                                category = id;
                            else
                                category = category + "|" + id;
                        }
                    }

                    Repeater subcategoryRepeater = (Repeater)categoryItem.FindControl("SearchFacetAreaSubcategoryList");
                    foreach (RepeaterItem subcategoryItem in subcategoryRepeater.Items)
                    {
                        CheckBox subcategoryCheckbox = (CheckBox)subcategoryItem.FindControl("checkboxAreaSubcategoryFacet");
                        if (subcategoryCheckbox.Checked)
                        {
                            string id = ((HiddenField)subcategoryItem.FindControl("hiddenAreaSubcategoryFacet")).Value;
                            if (!string.IsNullOrEmpty(id))
                            {
                                if (string.IsNullOrEmpty(subcategory))
                                    subcategory = id;
                                else
                                    subcategory = subcategory + "|" + id;
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(category) && !string.IsNullOrEmpty(subcategory))
                    selectedFacets.Add(area + "/" + category + "/" + subcategory);
                else if (!string.IsNullOrEmpty(category))
                    selectedFacets.Add(area + "/" + category + "/subcategory");
                else if (!string.IsNullOrEmpty(subcategory))
                    selectedFacets.Add(area + "/category/" + subcategory);
            }

            SelectedFacets = selectedFacets;
        }

        public IEnumerable<SearchFacetValue> GetCategoryFacets(SearchFacetValue facetValue)
        {
            List<SearchFacetValue> categoryList = new List<SearchFacetValue>();

            foreach (SearchFacetValue facet in CategoryFacets)
            {
                categoryList.AddRange(facet.NestedFacets.FirstOrDefault().FacetValues.Where(x => x.SiteArea.Equals(facetValue.Value) && !x.Value.Equals("category")));
            }

            return categoryList.Count > 0 ? categoryList : null;
        }

        public IEnumerable<SearchFacetValue> GetSubcategoryFacets(SearchFacetValue facetValue)
        {
            List<SearchFacetValue> subcategoryList = new List<SearchFacetValue>();

            foreach (SearchFacetValue facet in SubCategoryFacets)
            {
                foreach (SearchFacetValue subfacet in facet.NestedFacets.FirstOrDefault().FacetValues)
                {
                    subcategoryList.AddRange(subfacet.NestedFacets.FirstOrDefault().FacetValues.Where(x => x.ParentId.Equals(facetValue.Value) && x.SiteArea.Equals(facetValue.SiteArea) && !x.Value.Equals("subcategory")));
                }
            }

            return subcategoryList.Count > 0 ? subcategoryList.Distinct(new FacetValueEqualityComparer()) : null;
        }

        #endregion

        private void Page_Unload(object sender, EventArgs e)
        {
            this.GlobalSearchContext.Dispose();
        }

        private void CreatePagingControl()
        {
            placeHolderPager.Controls.Clear();

            if (this.PageIndex > 1)
            {
                LinkButton button = new LinkButton();
                button.ID = "lnkPrevPage";
                button.Text = "<";
                button.Command += new CommandEventHandler(Pagination_Click);
                button.CommandName = "previous";
                placeHolderPager.Controls.Add(button);
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderPager.Controls.Add(spacer);
            }

            int Pages = this.TotalRecords / this.RecordsPerPage;
            int PageLenth = 9;
            PageLenth = (PageLenth > Pages) ? Pages : PageLenth;
            int j = 0;

            if (this.PageIndex / 10 >= 1)
            {
                PageLenth = this.PageIndex + 1;
                j = PageLenth - 10;
            }

            if (PageLenth >= Pages)
                PageLenth = Pages;

            if (this.TotalRecords.Equals(this.RecordsPerPage))
                PageLenth = 0;

            if ((this.TotalRecords % this.RecordsPerPage) == 0)
                PageLenth = PageLenth - 1;

            for (int i = j; i < PageLenth + 1; i++)
            {
                if (this.PageIndex.Equals(i + 1))
                {
                    Label lnk = new Label();
                    lnk.Text = (i + 1).ToString();
                    lnk.CssClass = "active floatLft";
                    placeHolderPager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderPager.Controls.Add(spacer);
                }
                else
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnkPage" + (i + 1).ToString();
                    lnk.Text = (i + 1).ToString();
                    lnk.Command += new CommandEventHandler(Pagination_Click);
                    lnk.CommandArgument = (i + 1).ToString();
                    placeHolderPager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderPager.Controls.Add(spacer);
                }
            }

            if (this.PageIndex * this.RecordsPerPage < this.TotalRecords)
            {
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderPager.Controls.Add(spacer);
                LinkButton lnk = new LinkButton();
                lnk.ID = "lnkNextPage";
                lnk.Text = ">";
                lnk.Command += new CommandEventHandler(Pagination_Click);
                lnk.CommandName = "next";
                placeHolderPager.Controls.Add(lnk);
            }
        }

        protected void buttonFacetSearch_Click(object sender, EventArgs e)
        {
            PageIndex = 1;
            GetSelectedFacets();
            BindSearchResults(false);
        }

        protected void Pagination_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName.Equals("previous"))
            {
                PageIndex = PageIndex - 1;
            }
            else if (e.CommandName.Equals("next"))
            {
                PageIndex = PageIndex + 1;
            }
            else
            {
                PageIndex = Convert.ToInt32(e.CommandArgument.ToString());
            }

            GetSelectedFacets();
            BindSearchResults(false);
        }

        protected string GetPaginationName()
        {
            return (TotalRecords > 0 ? (((PageIndex - 1) * RecordsPerPage) + 1) : 0) + " - " + ((PageIndex * RecordsPerPage) > TotalRecords ? TotalRecords : (PageIndex * RecordsPerPage)) + " " + Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ofabout") + " " + TotalRecords + " " + Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "results");
        }

        protected string GetSearchResultDescription(SiteSearchResultItem item)
        {
            if (!string.IsNullOrEmpty(item.ItemMetaDescription))
            {
                return item.ItemMetaDescription.StripHtml();
            }
            else
            {
                if (!string.IsNullOrEmpty(item.ItemContent))
                {
                    return item.ItemContent.StripHtml().Length > 200 ? item.ItemContent.StripHtml().Substring(0, 200) + "..." : item.ItemContent.StripHtml();
                }
            }
            return string.Empty;
        }

        private bool IsStopWord(string word)
        {
            string[] stopWords = new string[] { "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into", "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they", "this", "to", "was", "will", "with" };
            if (stopWords.Contains(word))
                return true;

            return false;
        }
        
    }
}