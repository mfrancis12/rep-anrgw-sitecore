﻿using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using System;
using System.Linq;
using System.Web;
using Anritsu.WebApp.SitecoreUtilities.UserSession;
using Sitecore.Data.Fields;
using System.Configuration;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Header : GlassUserControl<IModelBase>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
                    
            SelectedCountryName.DataSource = GetSelectedCountryDataSource();
            ConfigureGroupHeader();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
               LogOnLogOffDisplay();
            }
            catch
            {
                myanritsumenu.Visible = false;
            }
        }

        private bool GetLoginstatus()
        {
           
            bool login = false;
            UserSessionRefresh.SetSession();
            var userLoggedIn = HttpContext.Current.Session[Constants.SSOUtilities.SessionVariables.UserLoggedIn]
                 as GWSsoUserType;
            if (((Context.User.Identity.IsAuthenticated) && (userLoggedIn != null)))
            {
                login = true;
            }

           // Response.Write("Context.User.Identity.IsAuthenticated: " + Context.User.Identity.IsAuthenticated);
           // Response.Write("userLoggedIn: " + userLoggedIn);
            return login;
        }
        //This method switches the login / logout links depending on the user session
        public void LogOnLogOffDisplay()
        {
          
    
            if (GetLoginstatus())
            {
                myanritsuicon.Src = CDNPath + "/appfiles/img/icons/angw-h-nav-sprite-myanritsu-loggedin.png";
                myanritsumenu.Attributes["class"] = "util__my-anritsu--is-active";
            }
            else
            {
                myanritsuicon.Src = CDNPath + "/appfiles/img/icons/angw-h-nav-sprite-myanritsu-logout.png";
                myanritsumenu.Attributes["class"] = "util__my-anritsu";
            }
         

        }

        public string GetLoggedInUserName()
        {
            string username = "";
            try
            {
                string lang = "AnrSso.GWIdp.LoginGreeting_EN-EN";
                username = ConfigUtility.AppSettingGetValue(lang);

                var userLoggedIn = HttpContext.Current.Session[Constants.SSOUtilities.SessionVariables.UserLoggedIn]
                    as GWSsoUserType;
                if (((Context.User.Identity.IsAuthenticated) && (userLoggedIn != null)))
                {
                    lang = "AnrSso.GWIdp.LoginGreeting_" + Sitecore.Context.Language.Name.ToString().ToUpper();
                    username = ConfigUtility.AppSettingGetValue(lang);
                    username = username.Replace("[FIRSTNAME]", userLoggedIn.FirstName).Replace("[LASTNAME]", userLoggedIn.LastName);

                }
            }
            catch
            {
                username = "My Anritsu";
            }
            
            return username;
        }
        
   


        protected string GetHomeItem
        {
            get
            {
                return (Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(ItemIds.Home)));
            }
        }

        private string GetSelectedCountryDataSource()
        {
            string countryCode = string.Empty;
            if (Request.Cookies[CookieVariables.UserSelectedCountry] != null && !string.IsNullOrEmpty(Request.Cookies[CookieVariables.UserSelectedCountry].Value))
            {
                countryCode = Request.Cookies[CookieVariables.UserSelectedCountry].Value;
            }
            else if (Request.Cookies[CookieVariables.UserCountryCode] != null && !string.IsNullOrEmpty(Request.Cookies[CookieVariables.UserCountryCode].Value))
            {
                countryCode = Request.Cookies[CookieVariables.UserCountryCode].Value;
            }
            Item countryItem = (Item)Sitecore.Context.Database.GetItem(ItemIds.ISOCountries).Children
                            .Where(country => country.Fields["CountryCode"] != null && country.Fields["CountryCode"].Value.ToUpperInvariant().Equals(countryCode.ToUpperInvariant())).FirstOrDefault();
            if (countryItem != null)
            {
                return countryItem.Paths.FullPath;
            }
            return string.Empty;
        }
        public string buildmyanritsumenuitem(string url,string text, string icon)
        {

            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            if (url != string.Empty && text != string.Empty)
            {
                builder.Append("<li class='util__my-anritsu__list__item'>");
                builder.Append("<a href='" + url + "' class='' >");
                if(icon!="")
                builder.Append("<img src='" + icon + "' width='30' height='30'>");
                builder.Append("<span>" + text + "</span>");
                builder.Append("</a>");
                builder.Append("</li>");
            }
            return builder.ToString();
        }

        protected string CDNPath
        {
            get { return string.Format("//{0}", ConfigurationManager.AppSettings["AWSCdnPath"]); }
        }
        public string buildmyanritsumenu()
        {
            bool login= GetLoginstatus();
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.Append("<ul class='util__my-anritsu__list'>");
            var spSignouturl = ConfigUtility.AppSettingGetValue("AnrSso.GWSp.Logout");
            spSignouturl = (string.IsNullOrEmpty(spSignouturl)) ? "/sso/sign-out.aspx" : spSignouturl.Replace("~", "");
            var spSigninurl = ConfigUtility.AppSettingGetValue("MyAnritsu") + "?lang=" + Sitecore.Context.Language;
            var myaccounturl = ConfigUtility.AppSettingGetValue("AnrSso.GWIdp.MyAccount") + "?lang=" + Sitecore.Context.Language;
            var createaccount = ConfigUtility.AppSettingGetValue("AnrSso.GWIdp.CreateAccount") + "?lang=" + Sitecore.Context.Language;

            if (login == true)
            {
                builder.Append(buildmyanritsumenuitem(spSigninurl, "My Anritsu", ""));
                builder.Append(buildmyanritsumenuitem(myaccounturl, Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "myaccount"), CDNPath + "/appfiles/img/icons/angw-h-nav-sprite-myaccount.png"));
              

                if (Sitecore.Context.Language.Name.ToLower() == "ja-jp")
                {
                    builder.Append(buildmyanritsumenuitem("", "", ""));
                    builder.Append(buildmyanritsumenuitem("", "", ""));
                    builder.Append(buildmyanritsumenuitem("", "", ""));
                }
                builder.Append(buildmyanritsumenuitem(spSignouturl, Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Logout"), CDNPath + "/appfiles/img/icons/angw-h-nav-sprite-logout.png"));

            }
            else
            {
                builder.Append(buildmyanritsumenuitem(spSigninurl, Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Login"), CDNPath + "/appfiles/img/icons/angw-h-nav-sprite-login.png"));
                builder.Append(buildmyanritsumenuitem(myaccounturl, Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "myaccount"), CDNPath + "/appfiles/img/icons/angw-h-nav-sprite-myaccount.png"));
                builder.Append(buildmyanritsumenuitem(createaccount, Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "createaccount"), ""));

                if (Sitecore.Context.Language.Name.ToLower() == "ja-jp")
                {
                    builder.Append(buildmyanritsumenuitem("", "", ""));
                    builder.Append(buildmyanritsumenuitem("", "", ""));
                    builder.Append(buildmyanritsumenuitem("", "", ""));
                }
            }
             builder.Append("</ul>");
            return builder.ToString();
        }
       // Configure group header name & other elemets based on group header
        protected void ConfigureGroupHeader()
        {
            try
            {
                Layouts.BasePage basePg = new Layouts.BasePage();
                Item groupHeaderItem = basePg.GetGroupHeaderItem();
                basePg.Dispose();
                           

                if (groupHeaderItem != null)
                {
                    hdnIsCorporate.Value = groupHeaderItem.ID.ToString() == ItemIds.CorporateHeader ? "true" : "false";
                    hdnGroupHeader.Value = basePg.IsCAGroupHeader(groupHeaderItem) ? basePg.GetTMGroupHeaderName() : groupHeaderItem.Name;

                    if (groupHeaderItem.Fields["ShowMyAnritsu"].Value == "1")
                    {
                        myanritsumenu.Visible = true;
                       
                    }
                    else
                    {
                        myanritsumenu.Visible = false;
                    }

                    LinkField lf = groupHeaderItem.Fields["EnterContactUsLink"];
                    linkContactUsDesktop.HRef = GetMediaUrl(lf); //linkContactUs.HRef =
                
                }
            }
            catch
            {
                linkContactUsDesktop.HRef = Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(ItemIds.CorporateContact));
            }

        }

        private string GetMediaUrl(LinkField lnkField)
        {
            switch (lnkField.LinkType.ToUpperInvariant())
            {
                case "INTERNAL":
                    // Use LinkMananger for internal links, if link is not empty
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;
                case "MEDIA":
                    // Use MediaManager for media links, if link is not empty
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;
                case "EXTERNAL":
                    // return external links
                    return lnkField.Url;
                case "ANCHOR":
                    // Prefix anchor link with # if link if not empty
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;
                case "MAILTO":
                    // return mailto link
                    return lnkField.Url;
                case "JAVASCRIPT":
                    // return javascript
                    return lnkField.Url;
                default:
                    // default value
                    return lnkField.Url;
            }
        }
    }
}


   