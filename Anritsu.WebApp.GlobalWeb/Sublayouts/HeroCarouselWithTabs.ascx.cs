﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class HeroCarouselWithTabs : GlassUserControl<IHeroCarouselMapping>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // bind hero carousels from Content mapping
            var item = SitecoreContext.GetCurrentItem<IHeroCarouselMapping>();
            if (item != null && item.Content != null && item.Content.HeroCarousel.Any())
            {
                sliders.DataSource = item.Content.HeroCarousel;
                sliders.DataBind();
                categories.DataSource = item.Content.HeroCarousel;
                categories.DataBind();
                carouselheader.Visible = true;
            }
            else
            {
                carouselheader.Visible = false;
            }

            // bind Tabs from DataSource
            if (Model == null)
                return;
            Tabs.DataSource = Model.GetChildren<ITitleWithDescription>();
            Tabs.DataBind();

            TabsDescription.DataSource = Model.GetChildren<ITitleWithDescription>();
            TabsDescription.DataBind();
        }
    }
}