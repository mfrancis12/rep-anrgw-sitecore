﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IRNews.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.IRNews" %>
<%@ import namespace="Anritsu.WebApp.GlobalWeb.Models.Components" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div id="section2" class="section">
    <asp:Repeater id="IRNewsList" runat="server" ItemType="IIRNews" >
        <ItemTemplate>   
            <h3><span class="date"><%#Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToLocalTime().ToFormattedDate())%></span>  <%# RenderLink(Item, x=>x.Link, isEditable: true, contents:Editable(Item,x=>x.Title,x=>x.Title.Truncate(100,"...")))%> </h3>   
        </ItemTemplate>
    </asp:Repeater>
    <p class="align-right"><a href="<%=GetMoreLink()%>" class="more"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "More") %> <i class="icon icon-arrow-blue"></i></a></p>
</div>