﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IRNewsLandingPage.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.IRNewsLandingPage" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<asp:ScriptManager ID="IRNewsScriptManager" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="IRNewsUpdatePanel" runat="server" >
                <ContentTemplate>
<div class="container">
    <div class="multi-promo-filter-items">
        <div class="multi-promo-filter">
            <div class="select-group select-group-right">
                <span class="filter-name"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "From") %></span>
                <div style="border: 0" class="input-select select-filter">
                    <asp:DropDownList ID="YearFilter" runat="server" OnSelectedIndexChanged="YearFilter_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                 <a target="_blank" alt="Subscribe using any feed reader!" class="rss-img" onclick="return addthis_open(this, 'feed', '<%=RssFeed%>')" href="<%=RssFeed%>">
                    <img src="/GlobalWeb/Resources/img/icons/RSS-2x.png" alt="rssfeeds" />
                </a>
            </div>
        </div>

        <asp:ListView ID="IRNews" runat="server" OnPagePropertiesChanged="IRNews_PagePropertiesChanged" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IIRNews">
            <ItemTemplate>
                <div class="item-noimg">
                    <div class="text">
                        <h2> <%# RenderLink(Item, x=>x.Link, isEditable: true, contents:Editable(Item,x=>x.Title))%></h2>
                        <p class="item-date"><%# Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToLocalTime().ToFormattedDate())%></p>
                    </div>
                </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <div><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "NoItems") %></div>
            </EmptyDataTemplate>
        </asp:ListView>        
        <div class="pager">
            <span>
                <asp:DataPager ID="IRNewsDataPager" runat="server" PagedControlID="IRNews" PageSize="20">
                    <Fields>                       
                        <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active" NextPageText=">" PreviousPageText="<" NextPreviousButtonCssClass="linkcolor" />
                    </Fields>
                </asp:DataPager>
            </span>
        </div>
    </div>
    <sc:Placeholder runat="server" Key="OptionalLinks" />
</div>
</ContentTemplate></asp:UpdatePanel>