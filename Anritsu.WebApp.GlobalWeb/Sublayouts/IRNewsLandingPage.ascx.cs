﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Anritsu.WebApp.SitecoreUtilities.Extensions;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Items;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    public partial class IRNewsLandingPage : GlassUserControl<IIRNews>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYearFilters();
                if (YearFilter.Items.Count > 0)
                {
                    YearFilter.SelectedIndex = 0;
                }
            }
            BindIRNews();          
        }
        protected void BindYearFilters()
        {
            YearFilter.DataSource = SitecoreContext.GetItem<Item>(Model.Id.ToString()).GetChildren().Where(x =>x.Versions.Count>0 && x.TemplateID.ToString().Equals(TemplateIds.YearFolder)).OrderByDescending(x => Convert.ToInt32(x.Name));
            YearFilter.DataTextField = "Name";
            YearFilter.DataValueField = "Id";
            YearFilter.DataBind();
        }
        public void BindIRNews()
        {
            var irnewsList = new List<IIRNews>();
            irnewsList = SitecoreContext.GetItem<Item>(YearFilter.SelectedValue).GetChildren().Select(x => SitecoreContext.GetItem<IIRNews>(x.ID.ToString())).ToList();
            IRNews.DataSource = irnewsList.Where(x => x != null && !string.IsNullOrEmpty((x.ReleaseDate).ToString())).OrderByDescending(x => x.ReleaseDate).ToList();//.Where(x => !string.IsNullOrEmpty((x.ReleaseDate).ToString()))
            IRNews.DataBind();
        }
        protected void YearFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindIRNews();
            IRNewsDataPager.SetPageProperties(0, IRNewsDataPager.PageSize, true);            
        }
        protected void IRNews_PagePropertiesChanged(object sender, EventArgs e)
        {
            BindIRNews();
        }
        public string RssFeed
        {
            get
            {
                if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.IRNewsPage))
                {
                    return SitecoreContext.GetItem<IModelBase>(ItemIds.IRNewsRssFeed) != null ? Request.Url.Scheme + "://" + Request.Url.Host + SitecoreContext.GetItem<IModelBase>(ItemIds.IRNewsRssFeed).Url : "";
                }
              
                return "";
            }
        }
    }
}