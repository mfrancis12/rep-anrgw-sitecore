﻿using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using System;


namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class IconsWithLinks : GlassUserControl<IModelBase>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            rptLandingHelp.DataSource = Model.GetChildren<IIconLinkWithTitle>();
            rptLandingHelp.DataBind();
        }

        public string GetCssClass(string itemId)
        {
            Item item = Sitecore.Context.Database.GetItem(itemId);
            if (item == null)
                return "";
            else
                return item.Fields["Value"].ToString();
        }
    }
}