﻿using System;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Constants;
using System.Collections;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ImageTitleWithDescription : GlassUserControl<IBannerWithContentRegional>
    {
      
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null) return;
            PromotionsList.DataSource = Model.Content.TryParseToGlass<IImageBlockWithContent>();
            PromotionsList.DataBind();
        }
       
        protected string GetLinkClass(IImageBlockWithContent item)
        {
            return item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Url) ? "link" : "hiddentxt";
        }

        protected string GetTitleLinkClass(IImageBlockWithContent item)
        {
            return item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Url) ? "anchorColor" : "no-link";
        }

        protected string GetLink(IImageBlockWithContent item)
        {
            return item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Url) ? item.RedirectLink.Url : "javascript:void(0);";
        }

        protected string GetLinkText(IImageBlockWithContent item)
        {
            return item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Url) ? item.RedirectLink.Text : "";
        }

        protected string GetLinkIcon(IImageBlockWithContent item)
        {
            return item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Text) ? "icon icon-arrow-blue" : "hiddentxt";
        }

        protected string GetTarget(IImageBlockWithContent item)
        {
            return item.RedirectLink != null ? item.RedirectLink.Target : "";
        }
    }
}