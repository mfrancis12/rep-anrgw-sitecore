﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using System;
    using Anritsu.WebApp.GlobalWeb.Models;
    using System.Linq;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Sitecore.Data.Items;
    using Sitecore.Data.Fields;
    using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
    using System.Web.UI.HtmlControls;
    using Sitecore.SharedSource.FieldSuite.Types;
    public partial class IndustryDetail : GlassUserControl<IIndustry>
    {
        public int itemCount = 0;

        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;

            var products = Model.RelatedProducts.Where(x => x.SelectProduct != null && x.SelectProduct.IsDiscontinued == false);
            Products.DataSource = products;
            Products.DataBind();

            itemCount = Model.GetChildren<ITitleWithDescription>().Count();


            tabsDescription.DataSource = Model.GetChildren<ITitleWithDescription>();
            tabsDescription.DataBind();

            BindOptionalLinks();
        }

        protected void OptionalLinks_PreRender(object sender, EventArgs e)
        {
            if (optionalLinks.Items.Count == 0)
            {
                optionalLinks.Visible = false;
            }
        }


        public void BindOptionalLinks()
        {
            if (Model.OptionalLinks != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(SitecoreContext.GetItem<Item>(Model.Id), "OptionalLinks");
                optionalLinks.DataSource = relatedGeneralLinks.LinkItems;
                optionalLinks.DataBind();
            }
        }

        protected void optionalLinks_ItemDataBound1(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            if (linkItem != null)
            {
                HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
                relatedAnchor.InnerText = linkItem.LinkText;
                relatedAnchor.HRef = linkItem.Url;
                relatedAnchor.Target = linkItem.Target;
            }
        }
    }
}