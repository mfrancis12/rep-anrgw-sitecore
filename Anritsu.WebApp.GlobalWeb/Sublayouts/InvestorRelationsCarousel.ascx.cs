﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class InvestorRelationsCarousel : GlassUserControl<IInvestorRelationsMapping>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model.Content.HeroCarousel.Any())
            {
                sliders.DataSource = Model.Content.HeroCarousel;
                sliders.DataBind();
                
            }
            else
            {
                Visible = false;
            }
        }
    }
}