﻿using System;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Constants;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class LandingHelp : GlassUserControl<IModelBase>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            RepeaterHelpIcons.DataSource = SitecoreContext.GetItem<IModelBase>(ItemIds.HelpIcons).GetChildren<IIconLinkWithTitle>();
            RepeaterHelpIcons.DataBind();
        }
        //protected string GetItemClass(int itemIndex)
        //{
        //    if (itemIndex == RepeaterHelpIcons.Items.Count - 1)
        //        return "item:last-child";
        //    else
        //        return "item ";
        //}
        protected string GetClass(string itemId)
        {
            Item item = Sitecore.Context.Database.GetItem(itemId);
            if (item == null)
                return "";
            else
                return item.Fields["Value"].ToString();
        }

    }
}
