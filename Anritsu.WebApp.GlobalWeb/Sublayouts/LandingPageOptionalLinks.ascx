﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandingPageOptionalLinks.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.LandingPageOptionalLinks" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="multi-promo-filter-help">
        <asp:Repeater runat="server" ID="optionalLinks" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.ILink" >
            <ItemTemplate>
        <div class="item">
            <!--i.icon.icon-landing-1-->
            <%--<a href="#" style="padding: 0" class="button">CTA BUTTON</a>--%>
             <%# RenderLink(Item, x=>x.Link,new { @class = "button paddingZero"}, isEditable: true, contents:Editable(Item,x=>x.Title))%>
        </div>
                </ItemTemplate>
        </asp:Repeater>
        
    </div>