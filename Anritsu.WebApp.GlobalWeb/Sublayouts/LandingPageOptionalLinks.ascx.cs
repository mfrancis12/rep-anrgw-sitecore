﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Common;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using System;

    public partial class LandingPageOptionalLinks : GlassUserControl<IModelBase>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            optionalLinks.DataSource=Model.GetChildren<ILink>();
            optionalLinks.DataBind();
        }
    }
}