﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class LeftSidebar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindTeasers();
        }
        /// <summary>
        /// Binding teasers for the current region.
        /// </summary>
        private void BindTeasers()
        {
            var productName = Sitecore.Web.WebUtil.GetUrlName(0);
            var productItem = Sitecore.Context.Item;
            Item targetItem = null;
            MultilistField multiListField = productItem.Fields["Select Product"];

            if (multiListField != null && multiListField.GetItems().Any())
            {
                foreach (var item in multiListField.GetItems().Where(item => item.Name.Equals(productName)))
                {
                    targetItem = item;
                }
            }

            var lstTeasers = new List<Item>();
            if (targetItem != null)
            {
                var langmutlilst = (MultilistField)targetItem.Fields["Teasers"];
                if (langmutlilst != null)
                {
                    lstTeasers.AddRange(langmutlilst.GetItems().Where(item => !string.IsNullOrEmpty(item["Image"])));
                }
            }

            Teasers.DataSource = lstTeasers;
            Teasers.DataBind();
        }
    }
}