﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using Anritsu.WebApp.SitecoreUtilities.DataAccess;
using Sitecore.Configuration;
using System.Configuration;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class LinkManagerPromotions : UserControl
    {
        private int _cultureGroupId;
        private string _absolutePath;
        public string AbsolutePath
        {
            get
            {
                _absolutePath = HttpContext.Current.Request.Url.AbsolutePath;
                return _absolutePath;
            }
            set
            {
                _absolutePath = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["Language"] != null && Session["Language"].ToString() == Sitecore.Context.Language.Name)
                {
                    _cultureGroupId = CommonLinks.GetCurrentCultureGroupId();
                }
                else
                {
                    _cultureGroupId = CommonLinks.GetCultureGroupIdForCultureCode(Sitecore.Context.Language.Name);
                    Session["Language"] = Sitecore.Context.Language.Name;
                    Session["CultureGroupId"]= _cultureGroupId;
                }
                int siteId = Convert.ToInt32(Settings.GetSetting("LinksAdmin.Site.GlobalWeb"));
                DateTime today = DateTime.Today;
                DataTable dataTable = CommonLinks.SelectLeftLinks(_cultureGroupId, AbsolutePath, siteId, today);
                if (dataTable.Rows.Count > 0)
                {
                    RepeaterLinks.DataSource = dataTable.DefaultView;
                    RepeaterLinks.DataBind();
                }
                else
                {
                    Visible = false;
                }
            }
            catch
            {
                Visible = false;
            }


        }

        protected string GetImageUrl(string imagePath)
        {
            return !string.IsNullOrEmpty(imagePath)?string.Format("https://" + ConfigurationManager.AppSettings["AWSCdnPath"] + "{0}", imagePath):"";
        }
    }
}