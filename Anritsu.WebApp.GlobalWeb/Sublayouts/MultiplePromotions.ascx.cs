﻿using System;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Constants;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class MultiplePromotions : GlassUserControl<IAboutGlobalMap>
    {
        protected string HasImage { get; set; }
        protected string HasLink { get; set; }
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;

            if (Model.Content !=null)
            {
                PromotionsList.DataSource = Model.Content.Content.TryParseToGlass<IImageBlockWithContent>();
                PromotionsList.DataBind();
            }
        }
        protected string GetLinkClass(IImageBlockWithContent item)
        {
            if (item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Url))
            {
                HasLink = "link";
            }
            else
            {
                HasLink = "hiddentxt";
            }
            return HasLink;
        }
        protected string GetClass(string itemId)
        {
            Item item = Sitecore.Context.Database.GetItem(itemId);
            if (item == null)
                return "";
            else
                return item.Fields["Value"].ToString();
        }
        protected string GetTitleLinkClass(IImageBlockWithContent item)
        {
            if (item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Url))
            {
                return string.Empty;
            }
            else
            {
                return "no-link";
            }
        }

        protected string GetLink(IImageBlockWithContent item)
        {
            if (item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Url))
            {
                HasLink = item.RedirectLink.Url;
            }
            else
            {
                HasLink = "javascript:void(0);";
            }
            return HasLink;

        }
        protected string GetLinkText(IImageBlockWithContent item)
        {
            if (item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Url))
            {
                return HasLink = item.RedirectLink.Text;
            }
            else
            {
                return "javascript:void(0);";
            }
        }
        protected string GetLinkIcon(IImageBlockWithContent item)
        {
            if (item.RedirectLink != null && !string.IsNullOrEmpty(item.RedirectLink.Text))
            {
                return "icon icon-arrow-blue";
            }
            else
            {
                return "hiddentxt";
            }
        }
    }
}

