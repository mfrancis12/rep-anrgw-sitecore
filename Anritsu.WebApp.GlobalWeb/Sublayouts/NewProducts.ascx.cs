﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class NewProducts : GlassUserControl<IProductCategory>
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Model.NewProducts.Any())
                {
                    ProductList.DataSource = Model.NewProducts;
                    ProductList.DataBind();
                }
                else
                {
                    Visible = false;
                }
            }
            catch(Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, ex, GetType());

            }
        }

        //private  List<Anritsu.WebApp.GlobalWeb.Models.Pages.IProductRegional> GetNewProducts()
        //{

        //    var regionalNewProducts = new List<Anritsu.WebApp.GlobalWeb.Models.Pages.IProductRegional>();
        //    var regionalSelectedProducts = string.IsNullOrWhiteSpace(ContextItem.Fields["New Products"].Value) ?
        //            null : ContextItem.Fields["New Products"].Value.Split('|');

        //    if (regionalSelectedProducts == null)
        //        return null;

        //    foreach (string itemId in regionalSelectedProducts)
        //    {
        //        var regionalSelectedProduct = Sitecore.Context.Database.GetItem(itemId);
        //        if(regionalSelectedProduct != null)
        //        regionalNewProducts.Add (new SitecoreContext().Cast<Anritsu.WebApp.GlobalWeb.Models.Pages.IProductRegional>(regionalSelectedProduct));
            
        //    }
        //    return regionalNewProducts;
        //}

    }
}