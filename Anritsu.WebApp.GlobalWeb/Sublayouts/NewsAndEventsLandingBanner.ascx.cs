﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using System;

    public partial class NewsAndEventsLandingBanner : BaseGlassUserControl<IStaticPageWithBanner>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
        }
    }
}