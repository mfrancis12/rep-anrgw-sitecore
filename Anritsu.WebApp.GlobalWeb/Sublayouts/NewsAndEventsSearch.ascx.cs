﻿using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Sitecore.ContentSearch;
using Sitecore.Data.Items;
using Sitecore.ContentSearch.Linq;
using System;
namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class NewsAndEventsSearch : System.Web.UI.UserControl
    {
        private IProviderSearchContext _newsandeventscontext;
        protected IProviderSearchContext NewsAndEventsSearchContext
        {
            get
            {
                return this._newsandeventscontext ?? (this._newsandeventscontext = ContentSearchManager.GetIndex(StaticVariables.NewsAndEvents).CreateSearchContext());
            }
        }


        public bool DoesSitecoreItemHavePresentation(Item item)
        {
            return item.Fields[Sitecore.FieldIDs.LayoutField] != null
            && item.Fields[Sitecore.FieldIDs.LayoutField].Value != String.Empty;
        }

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 var sitecoreService = new SitecoreService(Sitecore.Context.Database);
                var newslist = new List<SearchItem>();

                var query = NewsAndEventsSearchContext.GetQueryable<SearchItem>().Filter(i => i.Language == Sitecore.Context.Language.Name); 

                foreach (var item in query)
                {
                     sitecoreService.Map(item);
                     newslist.Add(item);
                    
                }
                Sitecore.Data.Database context =  Sitecore.Context.Database;
                newslist = newslist.Where(x => x != null).ToList();
                foreach (var item in newslist)
                {
                  
                    Sitecore.Data.Items.Item sitecoreitem = context.GetItem(item.ItemId);
                    if (sitecoreitem != null)
                    {
                        if(sitecoreitem.TemplateID.ToString() != TemplateIds.IRNews)
                        if (DoesSitecoreItemHavePresentation(sitecoreitem))
                        {
                            string url = Sitecore.Links.LinkManager.GetItemUrl(sitecoreitem);
                            var position = item.Path.LastIndexOf("/", StringComparison.Ordinal) + 1;
                            string name = item.Path.Substring(position, item.Path.Length - position);
                            Response.Write(string.Format("<a href='{0}' style='{2}'>{1}</a>", url, name, "font -weight:bold") + "<br/>");
                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }
}