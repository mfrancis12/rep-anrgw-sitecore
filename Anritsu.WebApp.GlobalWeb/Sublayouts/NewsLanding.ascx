﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsLanding.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.NewsLanding" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>
<asp:ScriptManager ID="NewsScriptManager" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="NewsUpdatePanel" runat="server" >
                <ContentTemplate>
<div class="container">
    <div class="multi-promo-filter-items">
        <div class="multi-promo-filter">
            <div class="select-group" id="divView" runat="server">
                <span class="filter-name"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "View") %></span>
                <div style="border: 0" class="input-select select-filter">
                    <asp:DropDownList ID="ViewFilters" runat="server" OnSelectedIndexChanged="ViewFilters_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="select-group select-group-right">
                <span class="filter-name"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "From") %></span>
                <div style="border: 0" class="input-select select-filter">
                    <asp:DropDownList ID="YearFilter" runat="server" OnSelectedIndexChanged="YearFilter_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>               
                <a target="_blank" alt="Subscribe using any feed reader!" class="rss-img" onclick="return addthis_open(this, 'feed', '<%=RssFeed%>')" href="<%=RssFeed%>">
                    <img src="/GlobalWeb/Resources/img/icons/RSS-2x.png" alt="rssfeeds" />
                </a>


            </div>
        </div>
        <asp:ListView ID="NewsReleases" runat="server"  ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.INewsRelease" OnPagePropertiesChanging="NewsReleases_PagePropertiesChanging">
            <ItemTemplate>
                <div class="<%# Item.Image!=null&&!string.IsNullOrEmpty(Item.Image.Src)?"item":"item-noimg" %>">
                    <div class="<%# Item.Image!=null&&!string.IsNullOrEmpty(Item.Image.Src)?"img":"hiddentxt" %>"><a href="<%#Item.Url%>"><%# RenderImage(Item,x=>x.Image, new { Width = 275, Height = 140}) %></a></div>
                    <div class="text">
                        <h2><a href="<%# Item.Url %>"><%# Editable(Item,x=>x.Title) %></a></h2>
                        <p class="item-date"><%# Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToLocalTime().ToFormattedDate()) %></p>
                        <p><%# Editable(Item,x=>x.ShortDescription) %></p>
                        <p class="link"><a href="<%# Item.Url %>"><b><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ReadMore") %></b><i class="icon icon-arrow-blue"></i></a></p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
        <asp:ListView ID="Announcements" runat="server"  ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IAnnouncement" OnPagePropertiesChanging="Announcements_PagePropertiesChanging">
            <ItemTemplate>
                <div class="item-noimg">
                    <div class="text">
                        <h2><a href="<%# Item.Url %>"><%# Editable(Item,x=>x.Title) %></a></h2>
                        <p class="item-date"><%# Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToLocalTime().ToFormattedDate()) %></p>
                        <p><%# Editable(Item,x=>x.Description,x=>x.Description.Truncate(300,"...")) %></p>
                        <p class="link"><a href="<%# Item.Url %>"><b><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ReadMore") %></b><i class="icon icon-arrow-blue"></i></a></p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
        <div id="divItems" runat="server"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "NoItems") %></div>
        <div class="pager">
            <span>
                <asp:DataPager ID="NewsReleaseDataPager" runat="server" PagedControlID="NewsReleases" PageSize="20">
                    <Fields>
                        <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active" NextPageText=">" PreviousPageText="<" NextPreviousButtonCssClass="linkcolor" />
                    </Fields>
                </asp:DataPager>
                <asp:DataPager ID="AnnouncementsDataPager" runat="server" PagedControlID="Announcements" PageSize="20">
                    <Fields>
                        <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active" NextPageText=">" PreviousPageText="<" NextPreviousButtonCssClass="linkcolor" />
                    </Fields>
                </asp:DataPager>
            </span>
        </div>
    </div>
    <sc:Placeholder runat="server" Key="OptionalLinks" />
</div>
</ContentTemplate></asp:UpdatePanel>