﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.ContentSearch;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Sitecore.Data;
using System.Collections.Generic;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class NewsLanding : GlassUserControl<IStaticPageWithBanner>
    {
        private NewsRepository _newsRepository { get; set; }

        public NewsRepository NewsRepository
        {
            get
            {

                if (_newsRepository == null)
                {
                    _newsRepository = new NewsRepository();
                }

                return _newsRepository;
            }
        }
        /// <summary>
        /// This method will help to match with dropdown selected value
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        private string DecodeItemID(string ID)
        {
            return ID.ToLower().Replace("{", "").Replace("}", "").TrimStart().TrimEnd();
        }
        private void Page_Load(object sender, EventArgs e)
        {
            //OLD CODE REPLACED BY 3chillies AW 19.05.2016
            //var announcementsQuery = string.Format("fast:{0}//*[@@templateid='{1}']", StringExtensions.EscapePath(SitecoreContext.GetCurrentItem<Item>().Paths.FullPath), TemplateIds.Announcement);
            //var announcmentList = SitecoreContext.Query<Item>(announcementsQuery).Where(x => x.Versions.Count > 0).ToList();
            //var newsQuery = string.Format("fast:{0}//*[@@templateid='{1}']", StringExtensions.EscapePath(SitecoreContext.GetCurrentItem<Item>().Paths.FullPath), TemplateIds.NewsRelease);
            //var newsList = SitecoreContext.Query<Item>(newsQuery).Where(x => x.Versions.Count > 0).ToList();

            //NEW CODE BY 3chillies AW 19.05.2016
            //announcements
            var announcementsContract = new NewsSearchContract();
            announcementsContract.TemplateRestrictions.Add(TemplateIds.Announcement);
            announcementsContract.RootRestrictions.Add(SitecoreContext.GetCurrentItem<Item>().ID);

            //README
            //.TotalSearchResults this is the total number of results that the index has found, we do not need to return any results from index
            //we just need to know that there are some. passing '1' in to the .SearchNewsByContract will save time.

            //any announcements?? bool.
            var hasAnnouncments = NewsRepository.SearchNewsByContract(announcementsContract, 1).TotalSearchResults > 0;

            //announcements
            var newsContract = new NewsSearchContract();
            newsContract.TemplateRestrictions.Add(TemplateIds.NewsRelease);
            newsContract.RootRestrictions.Add(SitecoreContext.GetCurrentItem<Item>().ID);

            //README
            //.TotalSearchResults this is the total number of results that the index has found, we do not need to return any results from index
            //we just need to know that there are some. passing '1' in to the .SearchNewsByContract will save time.
            var hasNews = NewsRepository.SearchNewsByContract(newsContract, 1).TotalSearchResults > 0;

            if (!IsPostBack)
            {
                BindViewFilter();

                if (ViewFilters.Items.Count > 0)
                {
                    ViewFilters.SelectedIndex = 0;
                }

                if (!hasAnnouncments && hasNews)
                {
                    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.HomeNewsPage)
                        ViewFilters.SelectedValue = DecodeItemID(ItemIds.HomeNewsRelease);
                    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.TMNewsPage)
                        ViewFilters.SelectedValue = DecodeItemID(ItemIds.TMNewsRelease);
                    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.SANewsPage)
                        ViewFilters.SelectedValue = DecodeItemID(ItemIds.SANewsRelease);


                    divView.Visible = false;
                }

                if (hasAnnouncments && !hasNews)
                {
                    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.HomeNewsPage)
                        ViewFilters.SelectedValue = DecodeItemID(ItemIds.HomeAnnouncement);
                    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.TMNewsPage)
                        ViewFilters.SelectedValue = DecodeItemID(ItemIds.TMAnnouncement);
                    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.SANewsPage)
                        ViewFilters.SelectedValue = DecodeItemID(ItemIds.SAAnnouncement);
            
                    divView.Visible = false;
                }

                BindYearFilters();

                if (YearFilter.Items.Count > 0)
                {
                    YearFilter.SelectedIndex = 0;
                }

            }
            var controlName = Page.Request.Params.Get("__EVENTTARGET");
            if (ViewFilters.UniqueID != controlName && YearFilter.UniqueID != controlName)
            {
                BindNews();
            }

        }
        protected void BindViewFilter()
        {
            ViewFilters.DataSource = SitecoreContext.GetCurrentItem<Item>().GetChildren().Where(x => x.Versions.Count > 0 && x.TemplateID.ToString().Equals(TemplateIds.RedirectLink)).Select(x => SitecoreContext.GetItem<IRedirectLink>(x.ID.ToString()));
            ViewFilters.DataTextField = "MenuTitle";
            ViewFilters.DataValueField = "Id";
            ViewFilters.DataBind();

        }
        protected void BindYearFilters()
        {
            YearFilter.Items.Clear();
            if (ViewFilters.Items.Count <= 0) return;
            YearFilter.DataSource = Sitecore.Context.Database.GetItem(ViewFilters.SelectedValue).GetChildren().Where(x => x.Versions.Count > 0 && x.TemplateID.ToString().Equals(TemplateIds.YearFolder)).OrderByDescending(x => Convert.ToInt32(x.Name));
            YearFilter.DataTextField = "Name";
            YearFilter.DataValueField = "Id";
            YearFilter.DataBind();
        }

        protected void BindNews()
        {
            if (YearFilter.Items.Count > 0)
            {
                switch (SitecoreContext.GetItem<Item>(ViewFilters.SelectedValue).ID.ToString())
                {
                    case ItemIds.HomeNewsRelease:
                    case ItemIds.TMNewsRelease:
                        {
                            BindNewsReleases();
                            break;
                        }
                    case ItemIds.SANewsRelease:
                        {
                            BindNewsReleases();
                            break;
                        }
                    case ItemIds.SDNewsRelease:
                        {
                            BindNewsReleases();
                            break;
                        }
                    case ItemIds.HomeAnnouncement:
                    case ItemIds.TMAnnouncement:
                        {
                            BindAnnouncements();
                            break;
                        }
                    case ItemIds.SAAnnouncement:
                        {
                            BindAnnouncements();
                            break;
                        }
                }
            }

            if (NewsReleases.Items.Count == 0 && Announcements.Items.Count == 0)
            {
                NewsReleaseDataPager.Visible = false;
                AnnouncementsDataPager.Visible = false;
                divItems.Visible = true;
            }
            else
            {
                divItems.Visible = false;
            }
        }

        private void BindNewsReleases()
        {
            #region OLD CODE Replaced: 2016.05.19 3chillies by AW
            //var newsList = SitecoreContext.GetItem<Item>(YearFilter.SelectedValue).GetChildren().Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<INewsRelease>(x.ID.ToString())).ToList();

            //if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.HomeNewsPage))
            //{
            //    var tmquery = string.Format("fast:{0}/{1}//*[@@templateid='{2}' and @ShowOnHome='1']",
            //        StringExtensions.EscapePath(SitecoreContext.GetItem<Item>(ItemIds.TMNewsRelease).Paths.FullPath),
            //        YearFilter.SelectedItem.Text, TemplateIds.NewsRelease);
            //    newsList.AddRange(
            //        SitecoreContext.Query<Item>(tmquery)
            //            .Where(x => x.Versions.Count > 0)
            //            .Select(x => SitecoreContext.GetItem<INewsRelease>(x.ID.Guid))
            //            .Where(x => x.ReleaseDate.Year.Equals(Convert.ToInt32(YearFilter.SelectedItem.Text)))
            //            .OrderByDescending(x => x.ReleaseDate)
            //            .ToList());
            //}
            #endregion

            //3chillies AW: NEW CODE 2016.05.19  

            //##removethisto - remove this line
            //the year folder ID relates to: /sitecore/content/GlobalWeb/home/about-anritsu/news/news-releases/<year folder>
            Item yearFolder = SitecoreContext.GetItem<Item>(YearFilter.SelectedValue);

            List<INewsRelease> newsList = new List<INewsRelease>();

            //this is to support getting news from two different sections one with the "showonhome" field checked and one without :S.
            //this is a mistake surely????
            //if after meeting on 19.05.2016 it turns out it is a mistake that remove this if statement...
            //ctrl + f for: ##removethisto 
            //add follow instructions...
            if (yearFolder != null)
            {
                var aboutNews = yearFolder.GetChildren().Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<INewsRelease>(x.ID.ToString())).ToList();

                if (aboutNews.Any())
                {
                    newsList.AddRange(aboutNews);
                }
            }

            if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.HomeNewsPage))
            {
                NewsSearchContract newsContract = new NewsSearchContract();
                newsContract.TemplateRestrictions.Add(TemplateIds.NewsRelease);
                newsContract.RootRestrictions.Add(new ID(ItemIds.TMNewsRelease));
                newsContract.RootRestrictions.Add(new ID(ItemIds.SANewsRelease));

                //##removethisto - comment this line back in
                //newsContract.RootRestrictions.Add(yearFolder.ID);

                int year;
                if (int.TryParse(YearFilter.SelectedItem.Text, out year))
                {
                    var start = new DateTime(year, 1, 1);
                    var end = start.AddMonths(12).AddDays(-1);

                    newsContract.StartReleaseDate = start;
                    newsContract.EndReleaseDate = end;
                }


                //##removethisto - remove the where close from this linq query: .Where(x => x.ShowOnHome)
                newsList.AddRange(NewsRepository.SearchNewsByContract(newsContract)
                   .Where(x => x.Document.GetItem() != null)
                   .Select(x => SitecoreContext.GetItem<INewsRelease>(x.Document.ItemId.ToGuid())).Where(x => x.ShowOnHome));
            }

            NewsReleases.DataSource = newsList.Where(x => x != null && !x.ReleaseDate.Equals(DateTime.MinValue)).OrderByDescending(x => x.ReleaseDate).Distinct().ToList();
            NewsReleases.DataBind();
            NewsReleaseDataPager.Visible = true;
            AnnouncementsDataPager.Visible = false;
            NewsReleases.Visible = true;
            Announcements.Visible = false;

        }

        private void BindAnnouncements()
        {
            var announcementList = SitecoreContext.GetItem<Item>(YearFilter.SelectedValue).GetChildren().Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<IAnnouncement>(x.Paths.FullPath)).ToList();
            Announcements.DataSource = announcementList.Where(x => x != null && !x.ReleaseDate.Equals(DateTime.MinValue)).OrderByDescending(x => x.ReleaseDate).ToList();
            Announcements.DataBind();
            NewsReleaseDataPager.Visible = false;
            AnnouncementsDataPager.Visible = true;
            NewsReleases.Visible = false;
            Announcements.Visible = true;
        }

        protected void YearFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            NewsReleaseDataPager.SetPageProperties(0, NewsReleaseDataPager.PageSize, true);
            AnnouncementsDataPager.SetPageProperties(0, AnnouncementsDataPager.PageSize, true);
            BindNews();
        }

        protected void ViewFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindYearFilters();
            if (YearFilter.Items.Count > 0)
            {
                YearFilter.SelectedIndex = 0;
            }
            NewsReleaseDataPager.SetPageProperties(0, NewsReleaseDataPager.PageSize, true);
            AnnouncementsDataPager.SetPageProperties(0, AnnouncementsDataPager.PageSize, true);
            BindNews();
        }

        public string RssFeed
        {
            get
            {
                if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.HomeNewsPage))
                {
                    return Request.Url.Scheme + "://" + Request.Url.Host + SitecoreContext.GetItem<IModelBase>(ItemIds.HomeNewsRSSFeeds).Url;
                }
                else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.TMNewsPage))
                {
                    return Request.Url.Scheme + "://" + Request.Url.Host + SitecoreContext.GetItem<IModelBase>(ItemIds.TMNewsRSSFeeds).Url;
                }
                return "";
            }
        }

        protected void NewsReleases_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            NewsReleaseDataPager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindNews();
        }

        protected void Announcements_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            AnnouncementsDataPager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindNews();
        }
    }
}

