﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsReleaseDetailPage.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.NewsReleaseDetailPage" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="container">
    <div class="content-form <%= (Model.Link== null || string.IsNullOrEmpty(Model.Link.Url))?"width100":"" %>">
        <div class="content-detail">
            <h1><%= Editable(x=>x.Title) %></h1>
            <p>
                <%= Editable(x=>x.ReleaseDate,x=>x.ReleaseDate.ToLocalTime().ToFormattedDate()) %>
                <br />
                <%= Editable(x=>x.ShortDescription) %>
            </p>
            <p>
                <%= RenderImage(x=>x.Image,Model.MakeImageResponsive=="1"? new {@class="img100"}: new {@class=""}) %>
            </p>
        
            <div class="faq">
                <p><%= Editable(x=>x.LongDescription) %></p>
            </div>
        </div>
          <div class="content-aside <%= (Model.Link==null || string.IsNullOrEmpty(Model.Link.Url))?"hiddentxt":"" %>">
            <h3><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
             <p><%= RenderLink(x=>x.Link, isEditable: true ,contents:Editable(x=>x.Link.Text)  + " " + "<i class='icon icon-arrow-blue'></i>")%></p>
        </div>
    </div>

</div>
