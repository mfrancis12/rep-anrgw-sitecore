﻿using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using System;

    public partial class NewsReleaseDetailPage : GlassUserControl<INewsRelease>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            //if (Page.IsPostBack) return;
            //if (GsaHelpers.IsGsaCrawler())
            //    AddGsaMetaInfo();
        }

        //public void GetClass
        //{
        //    get {
        //        if(Model.MakeImageResponsive=="1")
        //        {

        //        }
        //    }
        //}

        private void AddGsaMetaInfo()
        {
            try
            {
                var ltMetaTags = (PlaceHolder) Parent.FindControl("GsaMetaPlaceHolder");
                if (ltMetaTags == null) return;

                var meta = new HtmlMeta
                {
                    Name = "last-modified",
                    Content = Model.ReleaseDate.ToString("yyyy-MM-dd")
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message + " " + ex.StackTrace, "GsaMetaInfoAddition");
            }
        }
    }
}