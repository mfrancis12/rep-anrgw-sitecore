﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Anritsu.WebApp.SitecoreUtilities.Helpers;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Sitecore.Data;
using Anritsu.WebApp.GlobalWeb.ContentSearch;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{

    public partial class NewsTab : GlassUserControl<IBaseTemplate>
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            try
            {
                BindNews();
            }
            catch (Exception ex)
            {
                Response.Write("NewsTab Says<br/>" + ex.Message);
                Log.Error(ex.StackTrace, GetType());
            }
        }

        public void BindNews()
        {
            #region OLD CODE Replaced: 2016.05.19 3chillies by AW

            //var currentItem = SitecoreContext.GetItem<Item>(Model.Id);
            //var query = string.Format("fast:{0}//*[@@templateid='{1}']", StringExtensions.EscapePath(currentItem.Paths.FullPath), TemplateIds.NewsRelease);
            //var newsList = SitecoreContext.Query<Item>(query).Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<INewsRelease>(x.ID.Guid)).ToList().OrderByDescending(x => x.ReleaseDate).Take(5).ToList();
            //if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.Home))
            //{
            //    var item = SitecoreContext.GetItem<Item>(ItemIds.TMNewsRelease);
            //    if (item != null)
            //    {
            //        var tmquery = string.Format("fast:{0}//*[@@templateid='{1}' and @ShowOnHome='1']", StringExtensions.EscapePath(SitecoreContext.GetItem<Item>(ItemIds.TMNewsRelease).Paths.FullPath), TemplateIds.NewsRelease);
            //        newsList.AddRange(SitecoreContext.Query<Item>(tmquery).Where(x => x.Versions.Count > 0).Select(x => SitecoreContext.GetItem<INewsRelease>(x.ID.Guid)).ToList().OrderByDescending(x => x.ReleaseDate).Take(5).ToList());
            //    }
            //}
            //var news = newsList.Where(x => x != null && !x.ReleaseDate.Equals(DateTime.MinValue)).OrderByDescending(x => x.ReleaseDate).Take(5).ToList();
            #endregion

            //Model.Id can be a datasoruce..

            ////3chillies AW: NEW CODE 2016.05.19
            NewsSearchContract newsContract = new NewsSearchContract()
            {
                TemplateRestrictions = new List<String>() { TemplateIds.NewsRelease },
                //add root restrictions
                RootRestrictions = new List<ID>() { new ID(Model.Id) }
            };

            var newsRepository = new NewsRepository();

            //get ordered results from repo/index select 5
            var newsResults = newsRepository.SearchNewsByContract(newsContract, 5, 0, true)
                .Where(x => x.Document.GetItem() != null)
                .Select(x => SitecoreContext.GetItem<INewsRelease>(x.Document.ItemId.ToGuid()))
                .ToList();


            //AW I do not really understand why we are doing this, I have put it purely because it was as per old the code
            //it produces the same results, I think we can safely get rid of this if statement and everything inside.
            if (SitecoreContext.GetCurrentItem<Item>().ID.ToString().Equals(ItemIds.Home))
            {
                ID TMid;
                ID SAid;
                if (Sitecore.Data.ID.TryParse(ItemIds.TMNewsRelease, out TMid))
                {
                    //create new search contract
                    NewsSearchContract newsTradeshowContract = new NewsSearchContract()
                    {
                        //add template(s)
                        TemplateRestrictions = new List<String>() { TemplateIds.NewsRelease },
                        //check show on home
                        CheckShowOnHome = true,
                        //add root restrictions
                        RootRestrictions = new List<ID>() { TMid }
                    };

                    var newsTradeshowResults = newsRepository.SearchNewsByContract(newsTradeshowContract, 5, 0, true)
                        .Select(x => SitecoreContext.GetItem<INewsRelease>(x.Document.ItemId.ToGuid()))
                        .Where(x => x.ShowOnHome)
                        .ToList();

                    if (newsTradeshowResults.Any())
                    {
                        newsResults.AddRange(newsTradeshowResults);
                    }
                }
                if(Sitecore.Data.ID.TryParse(ItemIds.SANewsRelease, out SAid))
                { 
                    NewsSearchContract newsSAContract = new NewsSearchContract()
                    {
                        //add template(s)
                        TemplateRestrictions = new List<String>() { TemplateIds.NewsRelease },
                        //check show on home
                        CheckShowOnHome = true,
                        //add root restrictions
                        RootRestrictions = new List<ID>() { SAid }
                    };

                    var newsSAshowResults = newsRepository.SearchNewsByContract(newsSAContract, 5, 0, true)
                        .Select(x => SitecoreContext.GetItem<INewsRelease>(x.Document.ItemId.ToGuid()))
                        .Where(x => x.ShowOnHome)
                        .ToList();

                    if (newsSAshowResults.Any())
                    {
                        newsResults.AddRange(newsSAshowResults);
                    }

                }
            }

            NewsReleases.DataSource = newsResults.OrderByDescending(x => x.ReleaseDate).Distinct().Take(5);
            NewsReleases.DataBind();
        }
        public string GetMoreLink
        {
            get
            {
                string url = string.Empty;
                try
                {
                    if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.Home && SitecoreContext.GetItem<IModelBase>(ItemIds.HomeNewsPage) != null)
                    {
                        url = SitecoreContext.GetItem<IModelBase>(ItemIds.HomeNewsPage).Url;
                    }
                    else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.TestMeasurement && SitecoreContext.GetItem<IModelBase>(ItemIds.TMNewsPage) != null)
                    {
                        url = SitecoreContext.GetItem<IModelBase>(ItemIds.TMNewsPage).Url;
                    }
                    else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.ServiceAssurance && SitecoreContext.GetItem<IModelBase>(ItemIds.SANewsPage) != null)
                    {
                        url = SitecoreContext.GetItem<IModelBase>(ItemIds.SANewsPage).Url;
                    }
                    else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.SensingDevices && SitecoreContext.GetItem<IModelBase>(ItemIds.SDNewsPage) != null)
                    {
                        url = SitecoreContext.GetItem<IModelBase>(ItemIds.SDNewsPage).Url;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.StackTrace, GetType());
                }
                return url;
            }
        }
    }
}