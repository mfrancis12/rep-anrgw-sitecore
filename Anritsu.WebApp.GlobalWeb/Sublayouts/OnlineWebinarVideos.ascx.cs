﻿using System.Text.RegularExpressions;
using System.Web;
using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using Anritsu.WebApp.GlobalWeb.Constants;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Data.Managers;
    using System;

    public partial class OnlineWebinarVideos : GlassUserControl<IWebinarVideo>
    {
        #region Global Variables
        Database db = Sitecore.Context.Database;
        string indexName = "anritsu_brightcovevideotemplates";
        //public Item videoItem;
        #endregion

        private void Page_Load(object sender, EventArgs e)
        {
            //Launch Click
            var userLoggedIn = HttpContext.Current.Session[SessionVariables.UserLoggedIn] as GWSsoUserType;
            if (Context.User.Identity.IsAuthenticated && userLoggedIn != null && Sitecore.Context.PageMode.IsNormal)
            {
                LogWebinarInfo(userLoggedIn);
            }
            
            BrightcoveMediaControl.DataSource = !string.IsNullOrEmpty(Model.Video) ? VideoItem(Model.Video).Id.ToString() : string.Empty;
        }
        public IBrightcoveVideo VideoItem(string videoId)
        {
            if (videoId != null)
            {
                var contextService = new SitecoreContext();
                return contextService.GetItem<IBrightcoveVideo>(videoId, LanguageManager.GetLanguage("en"));
            }
            return null;
        }

        private WebinarLog GetWebinarLogInfo(GWSsoUserType userLoggedIn)
        {
            var webLog = new WebinarLog
            {
                EventName = SitecoreContext.GetItem<Item>(Model.Id.ToString()).Name,
                Title = Model.MetaTitle,
                CultureGroupId = BasePage.GetCultureGroupId(Sitecore.Context.Language.Name),
                UserID = userLoggedIn.UserId,
                ClientIP = Request.UserHostAddress,
                CreatedOnUtc = DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss")
            };

            return webLog;
        }

        private void LogWebinarInfo(GWSsoUserType userLoggedIn)
        {
            //log webinar request with user info
            var webinarLogInfo = GetWebinarLogInfo(userLoggedIn);
            BllWebinar.Webinar_AddLog(webinarLogInfo);
        }

    }
}