﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnlineWebinars.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.OnlineWebinars" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="container">
    <div class="content-form">
        <div class="content-detail width100">
           
		   <p class="event-type-detail"><%= Editable(x=>x.EventType.Value) %></p> 
		    <h1><%= Editable(x=>x.Title) %></h1>
		   <%= Editable(x=>x.StartDate,x=>x.StartDate.ToLocalTime().ToFormattedDate()) %><%= Editable(x=>x.StartTime, x=>x.GetCommaPrefixStartTime()) %> <%= Editable(x=>x.Duration,x=>x.GetCommaPrefixDuration()) %>
            <div class="faq">
                <p><%= Editable(x=>x.EventBodyText) %></p>
                <asp:panel runat="server"  ID="ButtonSection">
                    <asp:HyperLink runat="server" CssClass="button float-right color-white" id="RegisterOrLaunch"><%= Editable(x=>x.RegisterOrLaunchTitle.Value) %></asp:HyperLink>
                </asp:panel>
            </div>
        </div>
    </div>
</div>
