﻿using System.Globalization;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.Library.SSOSDK.SSOApiClient;
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Items;
    using System;
    using System.Web;

    public partial class OnlineWebinars : GlassUserControl<IOnlineWebinars>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Model == null)
                return;
            if (Model.RegisterOrLaunchTitle == null || Model.RegisterOrLaunchLink == null || string.IsNullOrEmpty(Model.RegisterOrLaunchLink.Url))
            {
                ButtonSection.Visible = false;
                return;
            }
           
            RegisterOrLaunch.Target = Model.RegisterOrLaunchLink.Target;
            RegisterOrLaunch.NavigateUrl = Model.RegisterOrLaunchLink.Url;
           
        }
        public static void RefreshUserSession()
        {
            var redirectLoginUrl = string.Format("/Sign-In.aspx?ReturnURL={0}", HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
            HttpContext.Current.Response.Redirect(redirectLoginUrl);
        }
    }
}