﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ProductCategoryCarousel : GlassUserControl<IProductCategory>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model.HeroCarousel.Any())
            {
                button.DataSource =
                    SitecoreContext.GetItem<IModelBase>(ItemIds.ProductCategoryButtons).GetChildren<ILink>();
                button.DataBind();
                if (SitecoreContext.GetCurrentItem<Sitecore.Data.Items.Item>().ID.ToString() == ItemIds.TestMeasurement)
                {
                    button.Visible = true;
                }
                else
                {
                    button.Visible = false;
                }
                sliders.DataSource = Model.HeroCarousel;
                sliders.DataBind();
                categories.DataSource = Model.HeroCarousel;
                categories.DataBind();
            }
            else
            {
                Visible = false;
            }

        }
    }
}