﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductContent.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ProductContent" %>
<div class="inner">
    <h2 class="title">
        <%= RenderLink(x=>x.Link, isEditable: true, contents:Editable(x=>x.Title) +"<div class='" + GetClass(Model.Link) + "'></div>")%>
    </h2>
    <div class="text">
        <div class="img">
            <% using (BeginRenderLink(x => x.Link, isEditable: true))
               { %>
            <%=RenderImage(x=>x.Image) %>
            <% } %>
        </div>
        <p>
            <b><%=Editable(x=> x.Subtitle) %></b>
            <%=Editable(x=> x.Description) %>
        </p>
        <p class="align-right">
			<b>
				<%= RenderLink(x=>x.Link, new { @class = "learnmore"}, true ,contents:Editable(x=>x.Link.Text) + " " + "<i class='icon icon-arrow-blue'></i>")%>
			</b>            
       </p>
    </div>
</div>
