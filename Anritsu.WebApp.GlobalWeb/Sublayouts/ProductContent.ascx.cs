﻿using System;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Fields;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ProductContent : BaseGlassUserControl<IImageBlockWithSubtitle>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DataSourceItem == null)
            {
                SetErrorState();
            }

        }
        protected string GetClass(Link link)
        {
            return (link != null && link.Type.ToString().ToUpperInvariant().Equals("EXTERNAL")) ? "icon icon-outbound-grey iconMargin" : string.Empty;
        }

        
    }
}