﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductContentDetail.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ProductContentDetail" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="product-detail">
    <div class="container">
        <div class="row">
            <div class="right">
                <div class="img">
                    <sc:Image runat="server" ID="productImage" Field="Image"/>
                </div>
            </div>
            <div class="left">
                <div class="inner">
                    <div class="eco <%= GetStyleClass("ShowEcoIcon",Model.Id.ToString()) %>">
                        <%--<img src="static/img/category/eco.png">--%>
                    </div>
                </div>
                <h1 class="cate"><%= GetProductName("ProductName",Model.Id.ToString())%></h1>
                <div class="name"><%= GetProductNumber("ModelNumber",Model.Id.ToString())%></div>
                <div class="tip <%= GetStyleClass("IsDiscontinued",Model.Id.ToString()) %>"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "downloads") %><a href="<%=Editable(x=> x.ReplacementProduct.Url)%>"><%=Editable(x=> x.ReplacementProduct.Text)%></a></div>
                <div class="action">
                    <div class="mrow">
                        <a href="/" class="button <%= GetStyleClass("ShowRequestQuote",Model.Id.ToString()) %>"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "requestquote") %></a>
                        <a href="/" class="button <%= GetStyleClass("ShowRequestDemo",Model.Id.ToString()) %>"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "requestdemo") %></a>
                    </div>
                    <div class="mrow">
                        <a href="/" class="button <%= GetStyleClass("ShowDownloads",Model.Id.ToString()) %>"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "downloads") %></a>
                        <a href="/" class="button <%= GetStyleClass("ShowFaqs",Model.Id.ToString()) %>"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "faqs") %></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="status">
            <%-- <sc:Image runat="server" Field="Image" Item="<%#Sitecore.Context.Database.GetItem(Model.Status.ToString())%>" /></div>--%>
        </div>
    </div>
</div>
