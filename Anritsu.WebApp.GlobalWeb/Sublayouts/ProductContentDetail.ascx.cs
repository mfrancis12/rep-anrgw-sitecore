﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using System;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;

    public partial class ProductContentDetail : GlassUserControl<IProductRegional>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            Item imageField = ((ReferenceField)Sitecore.Context.Database.GetItem(Sitecore.Context.Item.ID.ToString()).Fields["SelectProduct"]).TargetItem;

            productImage.DataSource = imageField != null ? imageField.Paths.FullPath : string.Empty;
        }

        public string GetStyleClass(string fieldName, string itemId)
        {
            return ((Sitecore.Context.Database.GetItem(itemId).Fields[fieldName] != null) &&
                ((CheckboxField)Sitecore.Context.Database.GetItem(itemId).Fields[fieldName]).Checked) ? string.Empty : "hiddentxt";
        }

        public string GetProductName(string fieldName, string itemId)
        {
            Item productGlobalItem = ((ReferenceField)Sitecore.Context.Database.GetItem(itemId).Fields["SelectProduct"]).TargetItem;
            return GetFieldValue(fieldName, productGlobalItem);
        }

        public string GetProductNumber(string fieldName, string itemId)
        {
            Item productGlobalItem = ((ReferenceField)Sitecore.Context.Database.GetItem(itemId).Fields["SelectProduct"]).TargetItem;
            return GetFieldValue(fieldName, productGlobalItem);
        }

        private string GetFieldValue(string fieldName, Item globalItem)
        {
            return globalItem != null ? globalItem[fieldName] : string.Empty;
        }
    }
}