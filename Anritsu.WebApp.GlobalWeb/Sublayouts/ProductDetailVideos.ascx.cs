﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.ExternalWebApi.Api;
using Anritsu.WebApp.GlobalWeb.ExternalWebApi.Models.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ProductDetailVideos : GlassUserControl<IProductRegional>
    {
        public class BrightcoveJapanVideoThumbnailItem
        {
            public string Title { get; set; }
            public string Description { get; set; }
            public string Thumbnail { get; set; }
            public string VideoPortalUri { get; set; }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            RepeaterVideos.DataSource = Model.Videos.TryParseToGlass<IVideo>();
            RepeaterVideos.DataBind();

            // Implement for Brightcove Japan Video Thumbnail.
            if (Sitecore.Context.Language.Name.ToLower() == "ja-jp")
            {
                var jpVideoList = GetJapanVideoPortalsItem();
                if (jpVideoList?.Any() == true)
                {
                    var jpVideoThumbnails = GetBrightcoveJapanVideos(jpVideoList).Result;
                    if (jpVideoThumbnails != null && jpVideoThumbnails.Count != 0)
                    {
                        RepeaterJPVideos.DataSource = jpVideoThumbnails;
                        RepeaterJPVideos.DataBind();
                    }
                }
            }
        }

        protected string GetVideoThumbnailUrl(string videoId)
        {
            var thumnbailPath = string.Empty;
            var brightcoveVideoItem = SitecoreContext.GetItem<IBrightcoveVideo>(videoId, LanguageManager.GetLanguage("en"));

            if (!string.IsNullOrEmpty(videoId) && brightcoveVideoItem != null && !string.IsNullOrEmpty(brightcoveVideoItem.ThumbnailURL))
            {
                thumnbailPath = brightcoveVideoItem.ThumbnailURL;
            }
            return thumnbailPath;
        }

        protected string GetVideoAlt(string videoId)
        {
            var videoAltText = string.Empty;
            var brightcoveVideoItem = SitecoreContext.GetItem<IBrightcoveVideo>(videoId, LanguageManager.GetLanguage("en"));

            if (!string.IsNullOrEmpty(videoId) && brightcoveVideoItem != null && !string.IsNullOrEmpty(brightcoveVideoItem.Name))
            {
                videoAltText = brightcoveVideoItem.Name;
            }
            return videoAltText;
        }

        /// <summary>
        /// Get Brightcove Japan Video Meta data.
        /// </summary>
        /// <param name="videos">AccountID and VideoID of Brightcove Japan Videos</param>
        /// <returns></returns>
        private async Task<List<BrightcoveJapanVideoThumbnailItem>> GetBrightcoveJapanVideos(IEnumerable<IBrightcoveJapanVideo> videos)
        {
            if (videos == null)
                return new List<BrightcoveJapanVideoThumbnailItem>();

            var webApi = new BrightcoveWebApi();
            var videoList = new List<BrightcoveJapanVideoThumbnailItem>();
            foreach (var video in videos)
            {
                GetVideoResponse videoInfo;
                try
                {
                    // Get Video Meta data(Title, Description, Thumbnail URI).
                    videoInfo = await webApi.GetVideoInformation(video.AccountId, video.VideoId);
                }
                catch (FileNotFoundException e)
                {
                    // This occurs in cases where the VideoID number entered by the content editor is wrong.
                    // Record it in Log and send the next Request.
                    Log.Warn($"Brightcove PlaybackAPI returns 404 NotFound. Please check AccountId/VideoId of the video item. Message: {e.Message}", e, this);
                    continue;
                }
                catch (Exception e)
                {
                    // Timeout, ServerError etc. No need to retry
                    Log.Error($"{e.Message}", e, this);
                    return new List<BrightcoveJapanVideoThumbnailItem>();
                }

                if (videoInfo == null) continue;
                videoList.Add(new BrightcoveJapanVideoThumbnailItem()
                {
                    Title = videoInfo.Name,
                    Description = videoInfo.Description,
                    Thumbnail = videoInfo.Thumbnail.AbsoluteUri,
                    VideoPortalUri = video.VideoPortalUrl
                });
            }
            return videoList;
        }

        private List<IBrightcoveJapanVideo> GetJapanVideoPortalsItem()
        {
            try
            {
                // FIXME: Absolutely have no clue why this is not working.
                // Model.Videos.TryParseToGlass<IBrightcoveJapanVideo>();
                var list = Sitecore.Context.Item["JapanVideoPortals"].Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                return list.Length != 0 ? list.Select(i => Sitecore.Context.Database.Items[Sitecore.Data.ID.Parse(i)]).Select(i => i.GlassCast<IBrightcoveJapanVideo>()).ToList() : new List<IBrightcoveJapanVideo>();
            }
            catch (Exception e)
            {
                Log.Error($"{nameof(GetJapanVideoPortalsItem)} Error.", e, this);
                return new List<IBrightcoveJapanVideo>();
            }
        }
    }
}