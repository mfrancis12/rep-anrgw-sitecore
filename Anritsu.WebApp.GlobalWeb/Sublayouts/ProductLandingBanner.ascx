﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductLandingBanner.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ProductLandingBanner" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>

<% if (!string.IsNullOrEmpty(Model.BannerImage.Src))
   { %>
<div class="full-container nogap multiProCls">
    <div class="hero-banner-3" style="background-image: url('<%=Model.BannerImage.Src%>')">
        <div class="hero-content">
            <h1 class="heroEle <%=Editable(x=>x.GetTextColor()) %>"><%=Editable(x=> x.BannerTitle) %></h1>
            <h3 class="heroEle <%=Editable(x=>x.GetTextColor()) %>"><%=Editable(x=> x.BannerSubtitle) %></h3>
            <p class="heroEle <%=Editable(x=>x.GetTextColor()) %>"><%=Editable(x=> x.BannerDescription) %></p>
        </div>
    </div>
</div>
<% } %>
<%else
   {%>
<div class="full-container container">
    <div class="products-heading">
        <div class="products-title">
            <h1><%=Editable(x=> x.BannerTitle) %></h1>
        </div>
        <div class="products-description"><%=Editable(x=> x.BannerDescription) %></div>
    </div>
</div>
<% }
%>

