﻿using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ProductLandingBanner : BaseGlassUserControl<IStaticPageWithBanner>
    {
        protected string HasImage { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null || Model.BannerImage == null)
            {
                SetErrorState();
            }
            //RepeaterCarousel.DataSource = Model.HeroCarousel.TryParseToGlass<IImageLinkWithDescription>();
            //RepeaterCarousel.DataBind();

            //productContent.DataSource = SitecoreContext.GetItem<IModelBase>("{ED1DF12E-1796-4CC8-A728-499F1D88F4C6}").GetChildren<IStaticPageWithBanner>();
            //productContent.DataBind();

            //productMiniContent.DataSource = SitecoreContext.GetItem<IModelBase>("{46407469-989F-4A01-BCDC-F7352B69BB91}").GetChildren<IStaticPageWithBanner>();
            //productMiniContent.DataBind();

            //productContent.
        }
        protected string GetDivImgClass(string imagePath)
        {
           return imagePath == null ? "hiddentxt" : "";
            
        }
        //protected void ProductMiniContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    Repeater r=e.Item.FindControl("productMiniContent") as Repeater;
        //    r.DataSource = SitecoreContext.GetItem<IModelBase>("{46407469-989F-4A01-BCDC-F7352B69BB91}").GetChildren<IStaticPageWithBanner>();
        //    r.DataBind();
        //}
    }

}