﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSubcategory.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ProductSubcategory" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<div class="full-container">
    <div class="products-info" id="ProductCategory">
        <div class="inner">
            <div>
                <% using (BeginRenderLink(x => x.Link, new { @class= "action" }, isEditable: true))
                   { %>
                <%=Editable(x=>x.Link.Text) %>
                <% } %>
            </div>
            <h2 class="title">
                <span id="goto_products" class="go-to-products">
                <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "products") %>
                    </span></h2>
        </div>
    </div>
    <div class="products-tech">
        <asp:Repeater ID="repeaterProductSubcategory" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IProductSubcategory">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <h3 class="title"><%#Editable(Item, x=>x.Title) %></h3>
                        <div class="content">
                            <div class="text">
                                <div class="img">
                                    
                                    <%#Editable(Item,x=>x.Image,new {@class="img100"} ) %>
                                        
                                </div>
                                <p class="more">
                                    <%#Editable(Item, x=>x.Description)%>
                                </p>
                            </div>
                            <div class="device">
                                <asp:Repeater ID="ProductFamilies" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IProductFamily" DataSource="<%#GetProductFamilies(Item) %>">
                                    <ItemTemplate>
                                        <a href="<%#Item.Url %>"><%#Item.PageTitle %></a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Repeater ID="DiscontinuedModels" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IStaticPageWithContentRegional" DataSource="<%#GetDiscontunedModel(Item) %>">
                                    <ItemTemplate>
                                        <a href="<%#Item.Url %>"><%#Item.PageTitle %></a>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
