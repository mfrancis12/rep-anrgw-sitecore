﻿using System;
using System.Collections.Generic;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System.Linq;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ProductSubcategory : GlassUserControl<IProductCategory>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;

            repeaterProductSubcategory.DataSource = Model.ProductSubcategory;// Model.GetChildren<IProductSubcategory>();
            repeaterProductSubcategory.DataBind();
          
        }

        public IEnumerable<IProductFamily> GetProductFamilies(IModelBase item)
        {
           var links =item.GetChildren<IProductFamily>();
           return links;
        }
        public IEnumerable<IStaticPageWithContentRegional> GetDiscontunedModel(IModelBase item)
        {
            return item.GetChildren<IStaticPageWithContentRegional>();
        }
    }
}