﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using SessionVariables = Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SessionVariables;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ProductTabs : GlassUserControl<IProductRegional>
    {
        public bool HasCount;
        public string ShowHideLibrary;

        private IProviderSearchContext _downloadcontext;
        protected IProviderSearchContext DownloadSearchContext
        {
            get
            {
                return _downloadcontext ?? (_downloadcontext = ContentSearchManager.GetIndex(StaticVariables.DownloadsIndex).CreateSearchContext());
            }
        }

        private List<DownloadSearchItem> _downloadList;

        protected List<DownloadSearchItem> DownloadList
        {
            get
            {
                var sitecoreService = new SitecoreService(Sitecore.Context.Database);
                _downloadList = new List<DownloadSearchItem>();

                var query = DownloadSearchContext.GetQueryable<DownloadSearchItem>().Filter(x => x.Language.Equals(Sitecore.Context.Language.Name));

                query = query.Where(x => !x.DownloadCategory.Equals(StaticVariables.DriversSoftwareDownloads));

                if (!string.IsNullOrEmpty(Model.SelectProduct.ModelNumber))
                {
                    query = query.Where(x => x.ProductModelNumbers.Contains(Model.SelectProduct.ModelNumber.Replace("<br>", "/")));
                }

                var searchResults = query.OrderByDescending(x => x.ReleaseDate).GetResults();
                foreach (var item in searchResults.Select(x => x.Document))
                {
                    sitecoreService.Map(item);
                    _downloadList.Add(item);
                }

                return _downloadList != null && _downloadList.Count > 0 ? _downloadList : null;
            }
            private set
            {
                if (value == null) throw new ArgumentNullException("value");
                _downloadList = value;
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {


                if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
                {
                    ProductTabsList.DataSource = Model.SelectProduct.GetChildren<ITitleWithDescription>().Where(x => x.GetChildren<ITitleWithDescription>().Count() == 0).Take(5);
                    ProductTabsList.DataBind();

                }
                else
                {
                    ProductTabsList.DataSource = Model.GetChildren<ITitleWithDescription>().Where(x => x.GetChildren<ITitleWithDescription>().Count() == 0).Take(5);
                    ProductTabsList.DataBind();
                }

                List<ITitleWithDescription> itemWithChildList = new List<ITitleWithDescription>();
                List<ITitleWithDescription> itemWithOutChildList = new List<ITitleWithDescription>();
                IEnumerable<ITitleWithDescription> tabItems;
                if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
                {
                    tabItems = Model.SelectProduct.GetChildren<ITitleWithDescription>();
                }
                else
                {
                    tabItems = Model.GetChildren<ITitleWithDescription>();
                }
                foreach (ITitleWithDescription item in tabItems)
                {
                    var firstOrDefault = item.ChildItems.FirstOrDefault();
                    if (firstOrDefault != null && (Sitecore.Context.Database.GetItem(item.Id.ToString()).HasChildren && !firstOrDefault.TemplateId.ToString().Equals(TemplateIds.Link)))
                    {
                        itemWithChildList.Add(item);
                        HasCount = true;
                    }
                    else
                    {
                        itemWithOutChildList.Add(item);
                    }
                }

                OverviewItems.DataSource = itemWithChildList;
                OverviewItems.DataBind();
                ProductTabsDescription.DataSource = itemWithOutChildList;
                ProductTabsDescription.DataBind();
                //DownloadList.DataSource = BindDownloads();
                //DownloadList.DataBind();          

                //Bind library content
                productDownloads.DataSource = GetProductDownloadsCategories();
                productDownloads.DataBind();
                ProductsDownloadsMobile.DataSource = GetProductDownloadsCategories();
                ProductsDownloadsMobile.DataBind();
                if (productDownloads.Items.Count <= 0)
                {
                    ShowHideLibrary = "hiddentxt";
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, ex, GetType());
            }
        }
        public Item LibraryItem
        {
            get
            {
                if (Model.GetChildren<ILibrary>().SingleOrDefault() != null)
                {
                    var singleOrDefault = Model.GetChildren<ILibrary>().SingleOrDefault();
                    if (singleOrDefault != null)
                        return Sitecore.Context.Database.GetItem(singleOrDefault.Id.ToString());
                }
                return null;
            }
        }
        public bool HasTechnologies
        {
            get
            {
                if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
                {

                    return (Model.SelectProduct.RelatedTechnologies.Count() > 0);
                }
                else
                    return false;
            }
        }
        public IEnumerable<Item> GetOverviewContent(IModelBase item)
        {
            Item overViewItem = SitecoreContext.GetItem<Item>(item.Id.ToString());
            bool isdescriptionRequried = true;
            if (overViewItem != null)
            {
                var listItems = overViewItem.Children.ToList();
                foreach (var currentItem in listItems.Where(currentItem => currentItem.TemplateID.ToString().Equals(TemplateIds.ProductDescriptionTab)))
                {
                    isdescriptionRequried = false;
                }
                if (isdescriptionRequried)
                    listItems.Insert(0, SitecoreContext.GetItem<Item>(ItemIds.Description));

                return listItems;
            }
            return null;
        }
        public Collection<ILink> GetRelatedLinks(IModelBase item)
        {
            Collection<ILink> listItems = new Collection<ILink>();
            foreach (var childItem in item.GetChildren<ILink>())
            {
                listItems.Add(childItem);
            }

            return listItems;
        }

        public string GetDescriptionTab()
        {
            if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
            {

                return Translate.TextByDomain("GlobalDictionary", "Description");
            }
            else
            {
                return "";
            }
        }

        public string GetTechnologyTab()
        {
            return Translate.TextByDomain("Products", "Technologies");
        }

        public string GetOverViewTab()
        {
            string strTab = string.Empty;
            ITitleWithDescription overViewTabItem;
            if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
                overViewTabItem = Model.SelectProduct.GetChildren<ITitleWithDescription>().FirstOrDefault(x => x.GetChildren<ITitleWithDescription>().Any() || x.GetChildren<IProductDescriptionTab>().Any());
            else
                overViewTabItem = Model.GetChildren<ITitleWithDescription>().FirstOrDefault(x => x.GetChildren<ITitleWithDescription>().Any() || x.GetChildren<IProductDescriptionTab>().Any());
            if (overViewTabItem != null)
                strTab = "<a class=\"active\" href=\"#" + overViewTabItem.Id + "\"><span>" + Translate.TextByDomain("GlobalDictionary", "overview") + "</span></a>";

            return strTab;
        }

        public string GetDescriptionTabContent(IModelBase item)
        {
            if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
            {

                var tabItem = item.GetChildren<IProductDescriptionTab>().FirstOrDefault();
                string description = (tabItem != null) ? tabItem.Description : "";
                return Model.SelectProduct.ShortDescription + description;
            }
            else
            {
                return "";
            }
        }

        public string GetCssClass
        {
            get { return !HasCount ? "active" : ""; }
        }
        public string GetTabClass
        {
            get { return Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion) ? "product-detail-tab" : "content-tab"; }
        }
        public string HideRelatedLinks(IModelBase itm)
        {
            return Sitecore.Context.Database.GetItem(itm.Id.ToString()).HasChildren ? "related-link" : "hiddentxt";
        }

        public IEnumerable<DownloadSearchItem> GetProductLibraryContent(string downloadCategory)
        {
            try
            {
                var downloadLibraryList = DownloadList;

                downloadLibraryList = downloadLibraryList.Where(x => x != null && x.Subcategory != null && x.Subcategory.Key != null && x.Subcategory.Key.Equals(downloadCategory)).ToList();
                return downloadLibraryList.OrderByDescending(x => x.ReleaseDate);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, ex, GetType());
            }
            return Enumerable.Empty<DownloadSearchItem>();
        }

        protected IEnumerable<IDropListItem> GetProductDownloadsCategories()
        {
            if (DownloadList == null || DownloadList.Count <= 0) return Enumerable.Empty<IDropListItem>();

            var listDownloads = DownloadList.Where(x => x != null && x.Subcategory != null)
                .Select(x => x.Subcategory).Distinct(new DownloadComparer())
                .Select(x => SitecoreContext.GetItem<Item>(x.Id.ToString())).ToList();

            var orderedList = SitecoreDefaultSort(listDownloads).ToList();
            return orderedList.Select(x => SitecoreContext.GetItem<IDropListItem>(x.ID.ToString())).ToList();
        }

        public static Item[] SitecoreDefaultSort(List<Item> itemList)
        {
            // Sort by sortorder, if sortorder value is empty, assume 100. 

            itemList.Sort(
                (a, b) =>
                (
                    (a.Fields["__sortorder"].Value == "" ? "100" : a.Fields["__sortorder"].Value)
                     ==
                    (b.Fields["__sortorder"].Value == "" ? "100" : b.Fields["__sortorder"].Value)
                )
                ?
                (String.CompareOrdinal(a.Name, b.Name))
                :
                (int.Parse(a.Fields["__sortorder"].Value == "" ? "100" : a.Fields["__sortorder"].Value).CompareTo(int.Parse(b.Fields["__sortorder"].Value == "" ? "100" : b.Fields["__sortorder"].Value)))
                );
            return itemList.ToArray();
        }

        private static Func<Item, int> GetSortOrderValue(List<Item> i)
        {
            return x => string.IsNullOrEmpty(i[0][FieldIDs.Sortorder]) ? 0 : int.Parse(i[0][FieldIDs.Sortorder]);
        }

        protected string GetTitle(object item)
        {
            Item tabItem = (Item)item;

            return tabItem.TemplateID.ToString().Equals(TemplateIds.TitleWithDescription) ? tabItem["Title"] :
                Translate.TextByDomain("GlobalDictionary", "Description");
        }


        protected string GetDescription(object item)
        {
            var tabItem = (Item)item;
            var description = string.Empty;

            if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
            {
                if (tabItem.TemplateID.ToString().Equals(TemplateIds.ProductDescriptionTab))
                {
                    return Model.SelectProduct.ShortDescription + SitecoreContext.GetItem<IProductDescriptionTab>(tabItem.ID.ToString()).Description;
                }
                if (tabItem.ID.ToString().Equals(ItemIds.Description))
                {
                    return Model.SelectProduct.ShortDescription;
                }
                if (tabItem.TemplateID.ToString().Equals(TemplateIds.TitleWithDescription))
                {
                    return SitecoreContext.GetItem<ITitleWithDescription>(tabItem.ID.ToString()) != null ? SitecoreContext.GetItem<ITitleWithDescription>(tabItem.ID.ToString()).Description : string.Empty;
                }
            }
            return string.Empty;
        }

        protected string GaTrackingScript(string itemUrl)
        {
            return string.Format(ConfigurationManager.AppSettings["GaTrackingScript"]
                , System.Convert.ToString(itemUrl.Substring(itemUrl.IndexOf("www.anritsu.com/") + 1)));
        }
    }

    public class DownloadComparer : IEqualityComparer<IDropListItem>
    {
        public bool Equals(IDropListItem x, IDropListItem y)
        {
            return x.Key == y.Key;
        }

        public int GetHashCode(IDropListItem obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}