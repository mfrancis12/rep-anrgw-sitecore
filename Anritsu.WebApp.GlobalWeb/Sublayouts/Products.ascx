﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Products.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Products" %>
<%@ Register tagPrefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel"  %>
<div class="pagetitle">

    <div class="pagetitleleft">
        <h1>
            <sc:Text ID="Title" runat="server" Field="Product Name" />
        </h1>
    </div>
    <div class="pagetitleright">
        &nbsp;
    </div>
</div>
<div class="clearfloat"></div>
<sc:Placeholder runat="server" Key="LeftSidebar" />
<div class="rightside">
    <div id="div_toprightlinks" class="toprightlinks">
        <ul>
            <asp:Repeater ID="Links" runat="server"><ItemTemplate>
            <li>
                <img src="http://static.cdn-anritsu.com/images/li.gif" alt=""/><sc:Link runat="server" Field="Link" Item="<%#(Container.DataItem as Sitecore.Data.Items.Item) %>"><sc:Text runat="server" Field="Link Title" Item="<%#(Container.DataItem as Sitecore.Data.Items.Item) %>" /> </sc:Link>&nbsp;&nbsp;|&nbsp;&nbsp;

            </li>
            </ItemTemplate></asp:Repeater>
        </ul>
    </div>
    <div class="table">
        <div class="tablerow">
            <div class="tablecell left-radius">               
                <sc:Image ID="ProductImage" runat="server" Field="Image" CssClass="topbox1image" />
            </div>
            <div class="tablecell right-radius">
                <div class="table-titlebar top-right-radius">
                    <sc:Text runat="server" Field="Text" DataSource="/sitecore/content/GlobalWeb/settings/Resource Strings/features" />
                </div>
                <div class="contentbox">
                    <sc:Text ID="Features" runat="server" Field="Features" />
                </div>
            </div>
        </div>
    </div>
    <div class="tabwindow">
        <ul class="tabwindowtabs">
            <li title="What's New" onclick="selectTab('tab01'); changeButton('tab01');" id="tabtab01" class="tabname"><a href="javascript:selectTab('tab01')" title="Description"><sc:Text runat="server" Field="Text" DataSource="/sitecore/content/GlobalWeb/settings/Resource Strings/description" /></a></li>

        </ul>
        <div class="clearfloat"></div>
        <div id="tabwindowitems" class="tabwindowitems">
            <div id="tab01" class="news">
                <sc:Text ID="Description" runat="server" Field="Description" />
            </div>
        </div>
        <div class="clearfloat"></div>
        <div id="tabwindowfooter" class="tabwindowfooter">
            <div>
                <span class="toptabftrl">&nbsp;</span>
            </div>
            <div class="movetoright">
                <span class="toptabftrr">&nbsp;</span>
            </div>
            <input type="hidden" value="tab03" id="hfTab">
        </div>
        <div class="clearfloat"></div>

    </div>
</div>
<div class="clearfloat"></div>