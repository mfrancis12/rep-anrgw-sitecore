﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Products : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string key = string.Empty;
            //key = ItemDetails.GetLanguageCode;
            BindProductDetails();
        }

        /// <summary>
        /// Binding product details in context language
        /// </summary>
        private void BindProductDetails()
        {
            string productName = Sitecore.Web.WebUtil.GetUrlName(0);
            Item productItem = Sitecore.Context.Item;
            Item targetItem = null;
            MultilistField multiListField = productItem.Fields["Select Product"];

            if (multiListField != null && multiListField.GetItems().Any())
            {
                foreach (var item in multiListField.GetItems())
                {
                    if (item.Name.Equals(productName))
                    {
                        targetItem = item;
                    }
                }
                Title.Item = targetItem;
                ProductImage.Item = targetItem;
                Description.Item = targetItem;
                Features.Item = targetItem;
            }
            BindLinks(targetItem);
        }
        /// <summary>
        /// Binding links for the current region
        /// </summary>
        private void BindLinks(Item currentItem)
        {
            var lstLinks = GetLinks(currentItem);
            if (lstLinks.Count <= 0) return;
            Links.DataSource = lstLinks;
            Links.DataBind();
        }

        private List<Item> GetLinks(Item currentProduct)
        {
            var lstLinks = new List<Item>();
            var langmutlilst = (MultilistField)currentProduct.Fields["Links"];
            if (langmutlilst != null)
            {
                lstLinks.AddRange(langmutlilst.GetItems().Where(item => !string.IsNullOrEmpty(item["Link Title"])));
            }
            return lstLinks;
        }
    }
}