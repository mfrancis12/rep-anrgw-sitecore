﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using System;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Constants;
    using System.Linq;
    using System.Collections.Generic;
    using Anritsu.WebApp.SitecoreUtilities.Extensions;
    using Sitecore.Data.Items;

    public partial class ProfessionalServices : GlassUserControl<IStaticPageWithBannerAndThumbnail>
    {
        
        
        private void Page_Load(object sender, EventArgs e)
        {
            contentBlock.DataSource = Model.GetChildren<IContentBlockTitleWithLink>().Where(x => x.Id.ToString()!= null);
            contentBlock.DataBind();
           
        }       
        
    }
}