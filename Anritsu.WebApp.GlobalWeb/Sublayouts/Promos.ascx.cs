﻿using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Promos : GlassUserControl<IHome>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            promos.DataSource = Model.Highlights.TryParseToGlass<IPromo>();
            promos.DataBind();
        }
    }
}