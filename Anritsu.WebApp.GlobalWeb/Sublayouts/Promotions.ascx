﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Promotions.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Promotions" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="promobg">
    <!-- promotion area -->
    <asp:Repeater ID="PromotionsList" runat="server">
        <ItemTemplate>
            <sc:Link runat="server" Field="Link" Item="<%# (Container.DataItem as Sitecore.Data.Items.Item) %>">
                <sc:Image runat="server" ID="promoicon0" CssClass="promofirst" Field="Thumbnail" Item="<%#(Container.DataItem as Sitecore.Data.Items.Item) %>"></sc:Image>
            </sc:Link>
        </ItemTemplate>
    </asp:Repeater>
    <!-- end of promobg -->
</div>
