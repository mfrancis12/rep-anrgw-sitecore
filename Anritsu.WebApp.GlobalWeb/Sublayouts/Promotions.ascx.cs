﻿using System;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Anritsu.WebApp.GlobalWeb.Constants;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class Promotions : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindPromotions();
        }
        /// <summary>
        /// Binding promotions in context region
        /// </summary>
        private void BindPromotions()
        {
            //List<Item> lstPromotions = GetPromotions(Sitecore.Context.Language);
            var lstPromotions = GetPromotions();
            //if (lstPromotions.Count == 0)
            //{
            //    //Fallback to en-za region
            //    Sitecore.Globalization.Language genericLang = Sitecore.Globalization.Language.Parse("en-ZA");
            //    lstPromotions = GetPromotions(genericLang);
            //}
            if (lstPromotions.Count <= 0) return;
            PromotionsList.DataSource = lstPromotions;
            PromotionsList.DataBind();
        }
        private List<Item> GetPromotions()      
        {
            var homeItm = Sitecore.Context.Database.GetItem(ItemIds.Home);
            var langmutlilst = (MultilistField)homeItm.Fields["Promotions"];
            var lstPromotions = new List<Item>();

            if (langmutlilst == null) return lstPromotions;
            lstPromotions.AddRange(langmutlilst.GetItems());
            return lstPromotions;
        }
    }
}