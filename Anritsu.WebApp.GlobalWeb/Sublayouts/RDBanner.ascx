﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RDBanner.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.RDBanner" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="full-container">
    <div style="background-image: url('<%=Model.Content.BannerImage.Src%>')" class="content-hero-banner">
        <div class="banner-text">
            <h1><%= Editable(x=>x.Content.BannerTitle) %></h1>
        </div>
    </div>
</div>
