﻿using Sitecore.Data.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class RedirectLink : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Sitecore.Context.Item.Fields["RedirectLink"] != null)
            {
                LinkField lf = Sitecore.Context.Item.Fields["RedirectLink"];
                Response.Redirect(GetMediaUrl(lf));
            }
        }

        private string GetMediaUrl(LinkField lnkField)
        {
            switch (lnkField.LinkType.ToUpperInvariant())
            {
                case "INTERNAL":
                    // Use LinkMananger for internal links, if link is not empty
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;
                case "MEDIA":
                    // Use MediaManager for media links, if link is not empty
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;
                case "EXTERNAL":
                    // return external links
                    return lnkField.Url;
                case "ANCHOR":
                    // Prefix anchor link with # if link if not empty
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;
                case "MAILTO":
                    // return mailto link
                    return lnkField.Url;
                case "JAVASCRIPT":
                    // return javascript
                    return lnkField.Url;
                default:
                    // default value
                    return lnkField.Url;
            }
        }
    }
}