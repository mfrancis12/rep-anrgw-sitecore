﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedProducts.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.RelatedProducts" %>
<div class="container">
    <div class="slider-title"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","RelatedProduct") %></div>
    <div class="product-related-slider slick">
        <asp:Repeater ID="RelatedProductsList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IProductRegional">
            <ItemTemplate>
                <div class="slider productItemHeight">
                    <a href="<%#Editable(Item,x=>x.Url) %>">
                        <%#RenderImage(Item,x=>x.SelectProduct.Thumbnail, new { Width = 116, Height = 86})%></a>
                    <div class="description">
                        <%#Editable(Item,x=>x.SelectProduct.ModelNumber)%>
                        <br>
                        <%#Editable(Item,x=>x.SelectProduct.ProductDescriptor)%>
                        <br>
                        <%#Editable(Item,x=>x.SelectProduct.ProductName)%>
                    </div>
                    <div class="status">
                        <%# (Item.SelectProduct.Status !=null && Item.SelectProduct.IsDiscontinued == false) ? RenderImage(Item,x=>x.SelectProduct.Status.Image,isEditable:true) : "" %>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
