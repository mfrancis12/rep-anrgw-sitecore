﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class RelatedProducts : GlassUserControl<IProductRegional>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model.RelatedProducts.Any())
            {
                RelatedProductsList.DataSource = Model.RelatedProducts.Where(x=>x.SelectProduct!=null);//.Where(x => !x.SelectProduct.IsDiscontinued);
                RelatedProductsList.DataBind();
            }
            else
            {
                Visible = false;
            }
        }
    }
}