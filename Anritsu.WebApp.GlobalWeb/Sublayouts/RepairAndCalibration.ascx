﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RepairAndCalibration.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.RepairAndCalibration" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Pages" %>
<div class="container">
    <div class="content-detail-table-form">
        <div class="content-detail-table Search-Content" id="divSearchProduct" runat="server">
            <%=Editable(x=> x.ShortDescription)%>
            <p><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "PriceNote") %></p>
            <hr />
            <p class="tip"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "model-number-search-tip") %></p>
            <asp:TextBox ID="SearchBox" runat="server" CssClass="group" />
            <asp:Button ID="Search" runat="server" Text="Search" CssClass="search-button" OnClick="Search_Click"></asp:Button>
           
            <div id="divNotFoundText" runat="server" style="display: none;">
                <font color="red"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "RepairCalibrationProductsNotAvailable") %><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "RequestServiceQuote") %></font><a href="<%=SitecoreContext.GetItem<Anritsu.WebApp.GlobalWeb.Models.IModelBase>(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.RequestServiceQuote).Url %>?model=[[MDLNUM]]"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ClickHere") %></a>
            </div>
            <div id="divSearchValidation" runat="server" style="display:none;">
                <p><font color="red"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "SearchValidation") %></font></p>
            </div>
            <div id="divNoSearchResults" runat="server" style="display:none;">
            <div id="divSearchResults" runat="server" style="display: none;">
                <h2><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "SearchResults") %></h2>
                <table border="0">
                    <tbody>
                        <tr class="table-head">
                            <th align="left"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ModelNumber") %></th>
                            <th align="left"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Description") %></th>
                        </tr>
                        <asp:ListView ID="Products" runat="server" OnPagePropertiesChanging="Products_PagePropertiesChanging">
                            <ItemTemplate>
                                <tr>
                                    <td class="col1">
                                        <%--<asp:LinkButton ID="LinkProduct" runat="server" OnClick="LinkProduct_Click" CommandArgument='<%#Model.Url %>?model=<%#Item.SelectProduct.ModelNumber %>&desc=<%#Item.SelectProduct.ProductDescriptor %>"></asp:LinkButton>--%>
                                        <a href="<%#Model.Url %>?model=<%#Eval("ModelNumber") %>&desc=<%#Eval("Description") %>"><span><%#Eval("ModelNumber") %></span></a></td>
                                    <td class="col2">
                                        <span><%#Eval("Description") %></span></td>
                                </tr>
                            </ItemTemplate>

                        </asp:ListView>
                    </tbody>
                </table>
            </div>
            <div style="padding-top: 20px; height: 1px;" class="seprate-line"></div>
            <div class="pagination" id="divPagination" runat="server">
                <span class="page">
                    <asp:DataPager ID="ProductsDataPager" runat="server" PagedControlID="Products" PageSize="20">
                        <Fields>
                            <asp:TemplatePagerField>
                                <PagerTemplate>
                                    <span class="info">
                                        <%# Container.TotalRowCount > 0 ? (Container.StartRowIndex + 1) : 0 %>-<%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%> / <%# Container.TotalRowCount %> Results                                   
                                    </span>
                                </PagerTemplate>
                            </asp:TemplatePagerField>
                            <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active floatLft" NextPageText=">" PreviousPageText="<" />
                        </Fields>
                    </asp:DataPager>
                </span>
            </div>                
                </div>
        </div>
         <%if(Model.OptionalLinks.Any())
              { %>
        <div class="sidebar" id="divSidebar" runat="server" style="display: none;">
            <p class="desktop"><%= RenderImage(x => x.Image, new { @class = "img100" }, isEditable: true)%></p>
            <p class="mobile"><%= RenderImage(x => x.Image, isEditable: true)%></p>
            <%--<img src="./static/img/content-text/optional_links.png"></p>--%>
            <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
            <asp:Repeater ID="OptionalLinks" runat="server" OnItemDataBound="OptionalLinks_ItemDataBound">
                <ItemTemplate>
                    <p><a id="Sidebarlink" runat="server"></a>&nbsp;<i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
            <div id="divBackToSearch" class="Back-To-Search-Content" runat="server" style="display:none;">
                <p><a id="backtosearch" href="<%=Model.Url %>">
                    <%=Sitecore.Globalization.Translate.TextByDomain("SupportServices", "Back2Search") %>
                </a>&nbsp;<i class="icon icon-arrow-blue"></i></p>
            </div>
        </div>
        <%} %>
        <div class="content-detail Price-Details-Content" runat="server" id="divProductDetails" style="display: none;">
            <h1 style="color: #289b7d"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Title") %></h1>
            <h2 style="color: #289b7d"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ModelNumber") %>
                <span>
                    <asp:Literal ID="ModelNumber" runat="server"></asp:Literal></span>-<span><asp:Literal ID="ModelDescription" runat="server"></asp:Literal></span>
            </h2>
            <table class="calibration-price">
                <asp:ListView ID="CalibrationPrices" runat="server">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><span>
                                <asp:Label ID="CalibrationDescription" runat="server" Text='<%# Eval("CalibrationText") %>'></asp:Label></span></td>
                            <td>&nbsp;&nbsp;&nbsp;<span>
                                <asp:Label ID="CalibrationPrice" runat="server" Text='<%# Eval("CalPrice") %>'></asp:Label></span></td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <tr>
                            <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "nopriceinformation") %>
                        </tr>
                    </EmptyDataTemplate>
                </asp:ListView>
            </table>
            <% if(lang== "en-us"){ %>
            <h3 style="color: #289b7d"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ProductNotes") %></h3>
            <%} %>
            <asp:ListView ID="ProductNotes" runat="server">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                <ItemTemplate>
                    <p style="width: 60%">
                        <asp:Label ID="FootNote" runat="server" Text='<%# Eval("FootNote") %>'></asp:Label>
                    </p>
                    <hr class="hr" />
                </ItemTemplate>
                <EmptyDataTemplate>
                    <p style="width: 60%">
                        <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "nonotesformodelnumber") %>
                    </p>
                    <hr class="hr" />
                </EmptyDataTemplate>
            </asp:ListView>
            <div class="faq">
                <%=Editable(x=> x.LongDescription)%>
            </div>

            <%--<div class="content-form" runat="server" id="divProductDetails" style="display:none;">
            
            </div>--%>
        </div>
    </div>
</div>
