﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data.Items;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using Anritsu.WebApp.SitecoreUtilities.DataAccess;
using BL = Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class RepairAndCalibration : GlassUserControl<IRepairCalibrationPricing>
    {
        public string lang = GetCultureFromUrl().ToLower();
        //string indexName = "anritsu_products";
        private void Page_Load(object sender, EventArgs e)
        {
            DisplayDetails();           
        }
        private void BindData(string searchText)
        {
            string text=searchText.ToLower(CultureInfo.InvariantCulture);
            var dt = BL.Calibration.GetAllProductDetails(lang, text);
            if (dt != null)
            {
                divNoSearchResults.Visible = true;
                divSearchResults.Visible = true;
                Products.DataSource = dt;
                Products.DataBind();
            }
            else
            {
                //divSearchResults.Visible = false;
                //divNoSearchResults.Visible = false;
                //divNotFoundText.Visible = true;
                divSearchResults.Style.Add("display", "none");
                divNoSearchResults.Style.Add("display", "none");
                divNotFoundText.Style.Add("display", "block");
            }
            BindOptionalLinks();
            
            //var sitecoreService = new SitecoreService(SitecoreContext.Database.Name);
            //var productList = new List<ProductSearchItem>();
            //using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
            //{
            //    var query = context.GetQueryable<ProductSearchItem>().Filter(i => i.Language == Sitecore.Context.Language.Name);
            //    foreach (var item in query)
            //    {
            //        sitecoreService.Map(item);
            //        productList.Add(item);
            //    }
            //    productList = productList.Where(x => x.SelectProduct != null && x.SelectProduct.ModelNumber.ToLower(CultureInfo.InvariantCulture).Contains(searchText.ToLower(CultureInfo.InvariantCulture))).ToList();
            //    Products.DataSource = productList;
            //    Products.DataBind();
            //    BindOptionalLinks();

            //    divSearchResults.Visible = productList.Any();
                
            //}
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            if (SearchBox.Text.Count() >= 3)
            {
                BindData(SearchBox.Text);
                divSearchResults.Style.Add("display", "block");
                divSidebar.Style.Add("display", "block");
            }
            else
            {
                divSearchValidation.Style.Add("display", "block");
                divNoSearchResults.Style.Add("display", "none");
            }
        }
        protected void Products_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            ProductsDataPager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindData(SearchBox.Text);

            divProductDetails.Style.Add("display", "none");
            divSearchProduct.Style.Add("display", "block");
        }
        
        protected void OptionalLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var linkItem = (GeneralLinkItem)e.Item.DataItem;
            var relatedAnchor = (HtmlAnchor)e.Item.FindControl("Sidebarlink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url + "?model=" +(!string.IsNullOrEmpty(Request.QueryString["model"])?Request.QueryString["model"]:SearchBox.Text) + "&SerialNumber=";
            relatedAnchor.Target = linkItem.Target;
        }
        public void BindOptionalLinks()
        {
            if (SitecoreContext.GetCurrentItem<IStaticPageWithContentRegional>().OptionalLinks != null)
            {
                var relatedGeneralLinks = new GeneralLinks(SitecoreContext.GetCurrentItem<Item>(), "OptionalLinks");
                OptionalLinks.DataSource = relatedGeneralLinks.LinkItems;
                OptionalLinks.DataBind();
            }
        }
        private static string GetCultureFromUrl()
        {
            var language = string.Empty;

            var url = HttpContext.Current.Request.Url.AbsolutePath;
            //url = url.Replace("www-", string.Empty).Replace("?", "/");
            var regexMatch = System.Text.RegularExpressions.Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

            if (!string.IsNullOrEmpty(regexMatch.Value))
            {
                language = regexMatch.Value.Replace("/", string.Empty);
            }
            return language;

        }

        private void DisplayDetails()
        {
            divNotFoundText.Style.Add("display", "none");
            divSearchValidation.Style.Add("display", "none");
            if (Request.QueryString.AllKeys.Length > 0 && !string.IsNullOrEmpty(Request.QueryString["model"]))
            {
                divSearchProduct.Style.Add("display", "none");
                divProductDetails.Style.Add("display", "block");
                divSidebar.Style.Add("display", "block");
                BindOptionalLinks();
                divBackToSearch.Style.Add("display", "block");
                ModelNumber.Text = Request.QueryString["model"];
                ModelDescription.Text = Request.QueryString["desc"] ?? "";
                var dt = BL.Calibration.GetCertificatePricesForPart(lang,ModelNumber.Text);
                CalibrationPrices.DataSource = dt;
                CalibrationPrices.DataBind();
                if (lang == "en-us")
                {
                    dt = BL.Calibration.GetProductNotesForPart(ModelNumber.Text);
                    ProductNotes.DataSource = dt;
                    ProductNotes.DataBind();
                }

            }
            else
            {
                divNoSearchResults.Style.Add("display", "block");
                divSearchProduct.Style.Add("display", "block");
                divBackToSearch.Style.Add("display", "none");
                //divSidebar.Style.Add("display", "block");
                divProductDetails.Style.Add("display", "none");
            }
        }
       

    }
}

