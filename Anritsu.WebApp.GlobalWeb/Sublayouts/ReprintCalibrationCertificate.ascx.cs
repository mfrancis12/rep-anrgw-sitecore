﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Library.EmailSDK;
using Anritsu.Library.EmailSDK.EmailWebRef;
using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.SitecoreUtilities.WebForms.SaveActions;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Globalization;
using SessionVariables = Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SessionVariables;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ReprintCalibrationCertificate : GlassUserControl<IModelBase>
    {
        #region Private Variables

        private GWSsoUserType _userLoggedIn;
        protected string CdnPath
        {
            get { return string.Format("//{0}", ConfigurationManager.AppSettings["AWSCdnPath"]); }
        }

        #endregion

        #region Page Level Methods

        protected void Page_Init(object sender, EventArgs e)
        {
            // All login required pages should run on secure connection
            if (!Request.IsSecureConnection)
            {
                WebUtility.ForceSecureConnection();
            }

            // Below code should be placed in a single class for authenticated pages
            if (HttpContext.Current.User.Identity.IsAuthenticated) return;

            //Session[Constants.SessionVariables.UserLoggedIn] = null;
            Session[SessionVariables.UserLoggedIn] = null;
            Response.Redirect(string.Format("/Sign-In.aspx?ReturnURL={0}"
                , HttpUtility.UrlEncode(Request.RawUrl)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            ClearControls();

            // Moving the below code from ascx to code as we are not able to assign resource values to asp controls
            bttSearch.Text = Translate.TextByDomain("Glossary", "Search");
            btnCertReqSubmit.Text = Translate.TextByDomain("CommonFormStrings", "Submit");
            dgvCertificates.PagerSettings.NextPageText = Translate.TextByDomain("CommonFormStrings", "Next");
            dgvCertificates.PagerSettings.PreviousPageText = Translate.TextByDomain("CommonFormStrings", "Previous");
            lblThankYou.Text = Translate.TextByDomain("GlobalDictionary", "calibrationCertThankMsg");
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "btt"),
         SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "btt")]
        protected void bttSearch_Click(object sender, EventArgs e)
        {
            if (SearchCalCerts())
            {
                divMainBody.Style.Add("display", "block");
                divSearchNoResults.Visible = true;
                divSearchResults.Visible = false;
            }
            else
            {
                divSearchResults.Visible = true;
            }
            txtMdlNum1.Text = txtMdlNum.Text;
            txtSlNum1.Text = txtSlNum.Text;
            _userLoggedIn = BasePage.LoggedInUser();
            txtUserLogin.Text = _userLoggedIn.EmailAddress;
            txtUserName.Text = _userLoggedIn.FirstName + @" " + _userLoggedIn.LastName;
            txtUserPhNum.Text = _userLoggedIn.PhoneNumber;
            hfCmp.Value = _userLoggedIn.CompanyName;
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "btn"), SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Req"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "btn")]
        protected void btnCertReqSubmit_Click(object sender, EventArgs e)
        {
            _userLoggedIn = BasePage.LoggedInUser();
            var clientIp = (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]);
            Calibration.InsertCalCertRequestsLog(_userLoggedIn.UserId, txtMdlNum1.Text, txtSlNum1.Text, clientIp);

            SendMail();
            ClearControls();
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "dgv"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "dgv")]
        protected void dgvCertificates_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "lbtt"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "lbtt")]
        protected void lbttDownloadCert_Click(object sender, EventArgs e)
        {

            var mCertNumber = ((LinkButton) (sender)).Text.Replace(".pdf", "");
            var commandArgs = ((LinkButton) (sender)).CommandArgument.Split(',');

            var mModelNumber = commandArgs[0];
            var mSerialNumber = commandArgs[1];
            var row =
                Calibration.GetCalibrationCerttificateFile(
                    ((GWSsoUserType) Session[SessionVariables.UserLoggedIn]).UserId.ToString(),
                    mModelNumber, mSerialNumber,
                    (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]));

            if (row == null)
            {
                lblMessage.Text = Translate.TextByDomain("ErrorMessage", "FileNotFound");
                //lblMessage.Text = GetGlobalResourceObject("ErrorMessage", "FileNotFound").ToString();
                return;
            }

            var mCertImage = (byte[]) row["Cal_Pdf"];
            var mFileSize = mCertImage.LongLength;
            var mFileName = string.Format("{0}.pdf", mCertNumber);
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(mCertImage);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + mFileName
                                                      + "; size=" + mFileSize);
            Response.End();
        }

        #endregion

        #region User Defined Methods

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate"),
         SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "URL")]
        public string GetURL()
        {
            return string.Format("{0}?{1}={2}&{3}={4}"
                , SitecoreContext.GetItem<IModelBase>(ItemIds.RequestServiceQuote).Url
                , QueryStringVariables.ModelId
                , Eval("ModelNumber")
                , QueryStringVariables.SerialNumber
                , Eval("SerialNumber"));
        }
        public string GetFormattedDate(object date)
        {
            try
            {
                if (date == null) return string.Empty;
                return (Convert.ToDateTime(date).ToFormattedDate());
            }
            catch
            {
                return string.Empty;
            }            
        }

        #region Sending Mail

        protected void SendMail()
        {
            var myRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.CalibrationCertificateRequestDistributionList,
                CultureGroupId = BasePage.GetCultureGroupId(Sitecore.Context.Language.Name),
                DistributionListKey =
                    "Calibration_Certificate_Request_DistributionList_" + Sitecore.Context.Language.Name.Substring(3, 2)
            };

            #region body replacement values -- all keys and values

            var bodyKeys = new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERID]]",
                ReplacementValue = _userLoggedIn.UserId.ToString(CultureInfo.InvariantCulture)
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[USERNAME]]", ReplacementValue = txtUserName.Text };
            bodyKeys.Add(keyValue);
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[USERLOGIN]]", ReplacementValue = txtUserLogin.Text };
            bodyKeys.Add(keyValue);
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[USERPHONE]]", ReplacementValue = txtUserPhNum.Text };
            bodyKeys.Add(keyValue);
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[MODELNUMBER]]", ReplacementValue = txtMdlNum1.Text };
            bodyKeys.Add(keyValue);
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[SERIALNUMBER]]", ReplacementValue = txtSlNum1.Text };
            bodyKeys.Add(keyValue);
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[USERCOMPANY]]", ReplacementValue = hfCmp.Value };
            bodyKeys.Add(keyValue);
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[COMMENTS]]", ReplacementValue = txtComments1.Text };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERIP]]",
                ReplacementValue =
                    (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"])
            };
            bodyKeys.Add(keyValue);

            #endregion

            myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();

            var myResponse = EmailServiceManager.SendDistributedTemplatedEmail(myRequest);
            if (myResponse.StatusCode.Equals(0))
            {
                var myRequest1 = new SendTemplatedEmailCallRequest
                {
                    EmailTemplateKey = EmailTemplates.CalibrationCertificateRequestToUser,
                    ToEmailAddresses = new[] { txtUserLogin.Text },
                    CultureGroupId = BasePage.GetCultureGroupId(Sitecore.Context.Language.Name)
                };

                bodyKeys.Clear();

                keyValue = new ReplaceKeyValueType { TemplateKey = "[[USERNAME]]", ReplacementValue = txtUserName.Text };
                bodyKeys.Add(keyValue);


                myRequest1.BodyReplacementKeyValues = bodyKeys.ToArray();

                var myResponse1 = EmailServiceManager.SendTemplatedEmail(myRequest1);
                if (myResponse1.StatusCode != 0)
                {
                    throw new ArgumentException(myResponse1.ResponseMessage);
                }
                divMainBody.Style.Add("display", "none");
                divSearchNoResults.Style.Add("display", "none");
                divSearchResults.Style.Add("display", "none");
                divThankyou.Style.Add("display", "block");
            }
            else
            {
                divMainBody.Style.Add("display", "none");
                divSearchNoResults.Style.Add("display", "none");
                divSearchResults.Style.Add("display", "none");
                throw new ArgumentException(myResponse.ResponseMessage);
            }
        }

        #endregion

        private bool SearchCalCerts()
        {
            var dtTe = Calibration.GetAllCertificates(txtMdlNum.Text.Trim(), txtSlNum.Text.Trim());
            if (dtTe == null || dtTe.Rows == null || dtTe.Rows.Count <= 0) return true;
            divMainBody.Style.Add("display", "none");
            divSearchResults.Style.Add("display", "block");
            dgvCertificates.Columns[0].HeaderText = Translate.TextByDomain("Products", "ModelNumber");
            dgvCertificates.Columns[1].HeaderText = Translate.TextByDomain("MyAnritsu", "SerialNumber");
            dgvCertificates.DataSource = dtTe;
            dgvCertificates.DataBind();
            return false;
        }

        private void ClearControls()
        {
            foreach (Control ctl in Page.Form.Controls)
            {
                var strTe = ctl.GetType().ToString();
                strTe = strTe.Substring(strTe.LastIndexOf('.') + 1);
                switch (strTe)
                {
                    case "TextBox":
                        ((TextBox) ctl).Text = "";
                        break;
                    case "CheckBox":
                        ((CheckBox) ctl).Checked = false;
                        break;
                }
            }
        }

        #endregion
    }
}