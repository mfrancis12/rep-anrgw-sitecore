﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Web.UI.WebControls;
using Sitecore.Data.Items;
using System.Collections;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Anritsu.Library.EmailSDK.EmailWebRef;
using EmailSDK = Anritsu.Library.EmailSDK;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class RestrictPage : GlassUserControl<IModelBase>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
             
                Uri myReferrer = Request.UrlReferrer;
                if (myReferrer != null)
                {
                    string actualhost = myReferrer.Host.ToString();
                    var list = (SitecoreContext.GetItem<IModelBase>(ItemIds.RestrictLinksAllowedDomains).GetChildren<ILink>()).ToList();
                    if (list.Count > 0)
                    {
                        ArrayList lstallowedurls = new ArrayList();
                        foreach (ILink lnk in list)
                        {
                            LinkField lnkfield = SitecoreContext.GetItem<Item>(lnk.Id).Fields["Link"];
                            Uri myUri = new Uri(lnkfield.Url);
                            string host = myUri.Host;
                            lstallowedurls.Add(host);

                        }
                        if (lstallowedurls.Contains(actualhost) == false)
                        {
                            try
                            {
                                SendEmailCallRequest request = new SendEmailCallRequest();
                                request.EmailSubject = "Referral URL Not exists in AllowedDomains for RestrictPages(Privacy Policy/Statements)";
                                request.ToEmailAddresses = new string[] { "notifications@anrgw.com" };
                                request.EmailBody = "Actualhost: " + actualhost + "<br>" + "Referrer: " + Request.UrlReferrer.ToString();
                                request.IsHtmlEmailBody = true;
                                var emailCallResponse = EmailSDK.EmailServiceManager.SendEmail(request);
                            }
                            catch 
                            {

                            }
                            finally
                            {
                                Session["INVALIDDOMAIN"] = myReferrer.Host;
                                LinkField lf = SitecoreContext.GetItem<Item>(ItemIds.RestrictLinksAllowedDomains).Fields["RedirectLink"];
                                string redirect = lf.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lf.TargetItem) : string.Empty;
                                Response.Redirect(redirect + "?InvalidRequest=true");
                            }
                        }
                     

                    }
                }
            }
            catch
            {

            }

        }


    }





}