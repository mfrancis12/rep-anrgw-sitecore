﻿using System;
using System.Linq;
using System.Text;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ScriptRenderer : GlassUserControl<IModelBase>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (DataSource == null) return;

            var parentItem = DataSourceItem;

            if (parentItem == null) return;

            var scriptContent = new StringBuilder();

            if (parentItem.HasChildren)
            {
                foreach (var item in parentItem.GetChildren().ToList()
                    .Where(x => x != null && x.TemplateID.ToString().Equals(TemplateIds.Script))
                    .Select(x => SitecoreContext.GetItem<Item>(x.ID.ToString())))
                {
                    scriptContent.Append(item["Script"]);
                }
            }
            else if (parentItem.TemplateID.ToString().Equals(TemplateIds.Script))
            {
                scriptContent.Append(parentItem["Script"]);
            }

            RenderScripts.Text =
                scriptContent.ToString().Contains("<%= System.Threading.Thread.CurrentThread.CurrentUICulture.Name %>")
                    ? scriptContent.ToString()
                        .Replace("<%= System.Threading.Thread.CurrentThread.CurrentUICulture.Name %>",
                            Sitecore.Context.Language.Name)
                    : scriptContent.ToString();
        }
    }
}