﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchPage.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.SearchPage" %>

<style>
    #page_navigation a{
      padding:5px 3px 3px 3px;
      border:1px solid #fff;
      margin:2px;
     
      text-decoration:none;
      width: 20px;
      height:20px;
      display: inline-block; 
      text-align: center; 
    }

    #page_navigation .previous_link,#page_navigation .next_link{
      width: 40px;
      position: relative;
      top: -8px;
      text-align: center;
    }

    a.disabled {
      opacity: 0.5;
      pointer-events: none;
      cursor: default;
    }
    .active_page{
       font-weight: bold;
      color:#2b7690;
    }

    #page_navigation .pagination-wrap{
      overflow: hidden;
      width: 320px;
      padding:10px 0;
      display: inline-block;
      height: 20px;
    }
    .pagination-inner{
      position: relative;
      left:0;
      width: 1000px;
      display: inline-block;
    }

#page_navigation a.disabled {display:none;}
 
.video-filter-left .result-filter .level3 .level3-item{ padding: 5px 20px 5px 40px; position: relative;}
.video-filter-left .result-filter .level4 .level4-item{ padding: 5px 20px 5px 60px;color: #a2a2a2;}
.result-filter .level3 .level3-item .icon{cursor: pointer; position: absolute;right: 8px;top: 10px;}
.result-filter .level3.on .level4{display:block;}
.result-filter .level4{display:none;}
.result-filter input[type=checkbox]{
margin-right:8px;
}
.iconStyles{cursor: pointer;
margin:5px 10px 0 0;
float:right;
    
}

.level2hd{color:#6b6b6b;}
.level3hd{color:#a2a2a2;}
.result-filter .level1_ul li{padding-top:5px;}
.result-filter .level1_ul{padding-left: 20px;padding-bottom:8px;}
.searchLevel  li{list-style:none;}
.searchLevel .level1 li{padding:2px 0;}
.level1hd,.level2hd,.level3hd{display: inline-block;
    vertical-align: top;
    width: 235px;}   
.dispNone{display:none;}

 .resultsCount {
      position: relative;
      top: -7px;
    } 
 
 .spellFont{width:60px;display:inline-block;}
 .spellFont a{color:#2b7690;cursor: pointer;}
 .meanTxt{font-weight:bold;padding-right:20px;padding-left: 121px;}
 .search-empty{  margin: 4px 0 30px 100px;float:left}
 .search-empty h1{color:#1f9c7d}
 .global-search .hd-right .searchResBtn {
    background: #434343 none repeat scroll 0 0;
    border: medium none;
    border-radius: 3px;
    box-sizing: border-box;
    padding: 11px 20px;
    width: 81px;
}
.search-result-body a{cursor:pointer}
.noReslts{color:#f5a623}
.searchPagination{margin:30px 0}
.filterLinks a{color:#000}
.filterLinks a:hover{color:#000}
.ac{margin-top:-10px;}
</style>
<div class="full-container global-search nogap">
    <div class="container nogap">
        <div class="hd-left">
            <h1><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "searchresult") %></h1>
        </div>
        <div class="hd-right">
            <input type="text" id="searchTxtBx" placeholder="Enter Keywords">
            <input type="button" class="searchResBtn" value="Search" />
            <asp:HiddenField ID="HiddenRegion" runat="server" ClientIDMode="Static" />
        </div>
    </div>
</div>
<div class="full-container search-result-body">
    <div class="container nogap">
        <div class="search-filter-left">
            <div class="result-filter">
                <div class="hd dispNone"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "sitesection") %></div>
                <ul class="bd searchLevel dispNone">
                </ul>
                <div class="clearall align-right dispNone"><a href="javascript:void(0)" id="clearAll"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "clearall") %></a></div>
            </div>

        </div>
        <div class="search-result-right">
            <div class="item" style="border-bottom: 0">

                <div class="searchContent" id="itemContainer">
                </div>
            </div>
            <!-- the input fields that will hold the variables we will use -->
            <input type='hidden' id='current_page' />
            <input type='hidden' id='show_per_page' />
            <div class="searchPagination">
                <!-- An empty div which will be populated using jQuery -->
                <span class="resultsCount"></span>
                <div id="page_navigation" style="display: inline-block"></div>
            </div>
        </div>
    </div>
</div>
<div class="suggetionCnt" id="spellContent">
    <span class="dispNone meanTxt"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "didyoumean") %></span>
</div>
<div class="search-empty dispNone">
    <%=Editable(x=>x.Content) %>
</div>
