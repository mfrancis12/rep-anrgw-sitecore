﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class SearchPage : GlassUserControl<IStaticText>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "func", "SearchInit();", true);
            }
            GetSelectedRegionAndCountryName(GetCountryItemIdByCulture());
        }
       
        private void GetSelectedRegionAndCountryName(string cultureItemId)
        {
            HiddenRegion.Value = SitecoreContext.GetItem<IISOCountry>(cultureItemId).CultureGroup != null ? SitecoreContext.GetItem<IDropListItem>(SitecoreContext.GetItem<IISOCountry>(cultureItemId).CultureGroup.ToString()).Key : "";

        }
        private string GetCountryItemIdByCulture()
        {
            string culture = Sitecore.Context.Language.Name;
            if (!string.IsNullOrEmpty(culture) && culture.IndexOf('-') > 0)
            {
                var cultureCode = culture.Substring(3, 2);
                return SitecoreContext.GetItem<IModelBase>(ItemIds.ISOCountries).GetChildren<IISOCountry>().Where(y => y.CountryCode.Equals(cultureCode)).FirstOrDefault() != null ? SitecoreContext.GetItem<IModelBase>(ItemIds.ISOCountries).GetChildren<IISOCountry>().Where(y => y.CountryCode.Equals(cultureCode)).FirstOrDefault().Id.ToString() : SitecoreContext.GetItem<IModelBase>(ItemIds.UnitedStates).Id.ToString();
            }
            return SitecoreContext.GetItem<IModelBase>(ItemIds.UnitedStates).Id.ToString();
        }
    }
}