﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServiceAssuranceHome.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ServiceAssuranceHome" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="full-container nogap">
    <div class="page-heading">
        <div class="inner">
            <div class="heading-text">
                <h1 class="title"><%=Editable(x=> x.PageTitle) %></h1>
            </div>
        </div>
    </div>
</div>
<div class="full-container nogap">
    <div class="category-slider slick">
        <asp:Repeater ID="CategoryCarousel" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICarousel">
            <ItemTemplate>
                <div class="slider">
                    <div class="product-image">
                        <%#RenderImage(Item,x=>x.Image,isEditable:true) %>
                    </div>
                    <div class="text">
                        <div class="inner">
                            <div class="name"><%#Editable(Item, x=>x.Title) %></div>
                            <div class="sub-title"><%#Editable(Item, x=>x.Subtitle) %></div>
                            <div class="description">
                                <%#Editable(Item, x=>x.Description) %>
                            </div>
                            <div class="link">
                                <%# RenderLink(Item, x=>x.Link,new { @class ="button"}, true,Editable(Item, x=>x.Link.Text))%>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="category-slider-tab">
        <asp:Repeater ID="CategoryPanel" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICarousel">
            <ItemTemplate>
                <div class="item <%#Container.ItemIndex==0?"active":"" %>">
                    <a href="javascript:void(0)"><%#Editable(Item, x=>x.PanelName) %></a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
<div class="full-container">
    <div class="products-info">
        <div class="inner">
            <h2 class="title"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "products") %></h2>
        </div>
    </div>
    <div class="products-tech">
        <asp:Repeater ID="ServiceAssuranceProducts" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IContentImageBlockWithLink">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <h3 class="title"><%#Editable(Item, x=>x.Title) %></h3>
                        <div class="content">
                            <div class="text">
                                <div class="img">
                                    <%#Editable(Item,x=>x.Image,new {@class="img100"} ) %>
                                </div>
                                <p class="more">
                                    <%#Editable(Item, x=>x.Description)%>
                                </p>
                            </div>
                            <div class="device">
                                <a href="<%#Item.Link.Url %>"><%#Item.Link.Text %></a>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
