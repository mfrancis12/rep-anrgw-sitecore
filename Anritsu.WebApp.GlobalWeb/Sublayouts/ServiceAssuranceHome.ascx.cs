﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using System;
    using System.Linq;

    public partial class ServiceAssuranceHome : GlassUserControl<IServiceAssuranceHome>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            ServiceAssuranceProducts.DataSource = Model.RelatedProducts;
            ServiceAssuranceProducts.DataBind();

            CategoryCarousel.DataSource = Model.HeroCarousel;
            CategoryCarousel.DataBind();

            CategoryPanel.DataSource = Model.HeroCarousel;
            CategoryPanel.DataBind();
        }
    }
}