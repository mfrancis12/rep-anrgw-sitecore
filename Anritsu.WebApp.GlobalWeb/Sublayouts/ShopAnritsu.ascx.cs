﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ShopAnritsu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.Transfer(
               string.Format("/SSO/Concept.aspx?frmGW=true&TargetURL={0}",
               ConfigurationManager.AppSettings["ConfigureOneURL"])
           );
        }
    }
}