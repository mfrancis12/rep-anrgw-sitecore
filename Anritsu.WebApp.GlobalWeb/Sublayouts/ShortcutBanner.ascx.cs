﻿using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models;
namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ShortcutBanner : GlassUserControl<ISustainabilityMapping>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            promos.DataSource = Model.GetChildren<IPromo>();
            promos.DataBind();
        }
    }
}