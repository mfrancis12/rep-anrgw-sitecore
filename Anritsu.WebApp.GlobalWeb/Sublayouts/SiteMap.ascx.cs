﻿
namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Linq;
    using Glass.Mapper.Sc;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Common;
    using Sitecore.Collections;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Fields;
    using Anritsu.WebApp.GlobalWeb.Constants;
    public partial class SiteMap : GlassUserControl<IModelBase>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            //ParentNodes.DataSource = SitecoreContext.GetItem<IModelBase>("{F30528F8-C34D-4BC9-AABE-F121FA8D7F88}").GetChildren<ILink>();
            //ParentNodes.DataBind();
            BindItems();
           
            //int ho = list.Count / 2;
           
        }
        public void BindItems()
        {
            
            var list = (SitecoreContext.GetItem<IModelBase>(ItemIds.SiteMap).GetChildren<ILink>()).ToList();
            var count = list.Count;
            var leftList = new List<ILink>();
            var rightList = new List<ILink>();
            for (int i = 0; i < count; i++)
            {
                if (i < Math.Ceiling(count / 2.0))
                {
                    leftList.Add(list[i]);


                }
                else
                {
                    rightList.Add(list[i]);

                }
            }
            LeftRepeater.DataSource = leftList;
            LeftRepeater.DataBind();
            RightRepeater.DataSource = rightList;
            RightRepeater.DataBind();

        }

        private void BindTreeViewControl(ILink item, TreeView tree)
        {
            try
            {
                TreeNode root = new TreeNode(item.Title);
                root.SelectAction = TreeNodeSelectAction.None;
                root.Collapse();
                tree.Nodes.Add(root);
                if (item.Link != null)
                {
                    root.Text = "<a href=" + item.Link.Url + ">" + root.Text + "</a>";
                }
                List<ILink> lstItems = item.GetChildren<ILink>().ToList();

                foreach (ILink childItem in lstItems)
                {
                    TreeNode croot = new TreeNode(childItem.Title);
                    croot.SelectAction = TreeNodeSelectAction.None;
                    if (childItem.Link != null)
                    {
                        croot.Text = "<a href=" + childItem.Link.Url + ">" + croot.Text + "</a>";
                    }
                    croot.Collapse();
                    if (SitecoreContext.GetItem<Item>(childItem.Id).HasChildren)
                    {
                        this.CreateNode(croot, childItem);
                    }

                    root.ChildNodes.Add(croot);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void CreateNode(TreeNode node,ILink childItem)
        {
            List<ILink> listItems = childItem.GetChildren<ILink>().ToList();
            foreach (ILink itm in listItems)
            {
                TreeNode childnode = new TreeNode(itm.Title);
                if (childItem.Link != null)
                {
                    childnode.Text = "<a href=" + childItem.Link.Url + ">" + childnode.Text + "</a>";
                }
                childnode.SelectAction = TreeNodeSelectAction.None;
                childnode.Collapse();
                node.ChildNodes.Add(childnode);
                if (SitecoreContext.GetItem <Item>(itm.Id).HasChildren)
                {
                    this.CreateNode(childnode, itm);
                }
            }
        }

        

        protected void LeftRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ILink item = (ILink)e.Item.DataItem;

            TreeView tree = (TreeView)e.Item.FindControl("LeftTree");
            
                BindTreeViewControl(item, tree);
           
        }

        protected void RightRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ILink item = (ILink)e.Item.DataItem;

            TreeView tree = (TreeView)e.Item.FindControl("RightTree");
            
                BindTreeViewControl(item, tree);
            
        }

    }
}