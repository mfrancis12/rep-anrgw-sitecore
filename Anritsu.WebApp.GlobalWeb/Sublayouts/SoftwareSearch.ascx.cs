﻿using System.Collections.Generic;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using System.Linq;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using System;

    public partial class SoftwareSearch : System.Web.UI.UserControl
    {
        private IProviderSearchContext _downloadcontext;
        protected IProviderSearchContext DownloadSearchContext
        {
            get
            {
                return _downloadcontext ?? (_downloadcontext = ContentSearchManager.GetIndex(StaticVariables.DownloadsIndex).CreateSearchContext());
            }
        }
        private void Page_Load(object sender, EventArgs e)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var downloadList = new List<DownloadSearchItem>();

            var query = DownloadSearchContext.GetQueryable<DownloadSearchItem>().Filter(x => x.Language.Equals(Sitecore.Context.Language.Name));

            foreach (var item in query)
            {
                sitecoreService.Map(item);
                downloadList.Add(item);
            }
            downloadList = downloadList.Where(x => x != null && x.Subcategory != null && x.Subcategory.Key.Equals(StaticVariables.DriversSoftwareDownloads)).OrderByDescending(x => x.ReleaseDate).ToList();
            foreach (var item in downloadList)
            {
                var position = item.Path.LastIndexOf("/", StringComparison.Ordinal) + 1;
                Response.Write(string.Format("<a href='{0}' style='{2}'>{1}</a>", "/" + Sitecore.Context.Language.Name + "/test-measurement/support/downloads/software/" +
                    item.Path.Substring(position, item.Path.Length - position), item.Title, "font-weight:bold") + "<br/>");
            }
        }
    }
}