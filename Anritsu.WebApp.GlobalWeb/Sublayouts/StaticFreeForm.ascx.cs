﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc;
    using Glass.Mapper.Sc.Web.Ui;
    using System;
    using System.Text;
    using GlobalWeb.Utilities;

    public partial class StaticFreeForm : GlassUserControl<IStaticText>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (DataSource != null)
                {
                  
                    Anritsu.WebApp.GlobalWeb.Utilities.QuickLinks qlinks = new Anritsu.WebApp.GlobalWeb.Utilities.QuickLinks();
                    var quickLinkItem = Sitecore.Context.Database.GetItem(DataSource);
                    StringBuilder quickLinkBuilder = new StringBuilder();
                    if (quickLinkItem != null)
                    {

                        Model.Content = qlinks.BuildMegaMenu(quickLinkItem, quickLinkBuilder, "").ToString();
                       
                    }
                }
            }
            catch(Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, ex);

            }
            
        }
    }
}