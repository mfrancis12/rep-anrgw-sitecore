﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
    using Sitecore.SharedSource.FieldSuite.Types;
    using System;
    using System.Web.UI.HtmlControls;

    public partial class StaticFreeformRegional : GlassUserControl<IStaticFreeformRegional>
    {
        public string ImageUrl = string.Empty;
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Model == null)
                return;
            BindOptionalLinks();
        }
        protected void OptionalLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
            if (linkItem != null && relatedAnchor != null)
            {
                relatedAnchor.InnerText = linkItem.LinkText;
                relatedAnchor.HRef = linkItem.Url;
                relatedAnchor.Target = linkItem.Target;
            }
        }

        protected void OptionalLinks_PreRender(object sender, EventArgs e)
        {
            if (OptionalLinks.Items.Count == 0)
            {
                OptionalLinks.Visible = false;
            }
        }
        public void BindOptionalLinks()
        {
            IStaticFreeformGlobal currentItem = GetContentItem;
            if (currentItem != null && currentItem.OptionalLinks != null)
            {
                GeneralLinks optionalLinks = new GeneralLinks(SitecoreContext.Database.GetItem(currentItem.Id.ToString()), "OptionalLinks");
                OptionalLinks.DataSource = optionalLinks.LinkItems;
                OptionalLinks.DataBind();
            }
            else
            {
                IStaticFreeformGlobal item = SitecoreContext.GetItem<IStaticFreeformGlobal>(SitecoreContext.GetCurrentItem<Item>().ID.ToString());
                GeneralLinks optionalLinks = new GeneralLinks(SitecoreContext.Database.GetItem(item.Id.ToString()), "OptionalLinks");
                OptionalLinks.DataSource = optionalLinks.LinkItems;
                OptionalLinks.DataBind();
            }
        }
        public string GetContent
        {
            get
            {
            IStaticFreeformGlobal contentItem = GetContentItem;
            if (contentItem != null)
            {
                ImageUrl = contentItem.Image.Src;
                return contentItem.Content;
            }
            else
            {
                IStaticFreeformGlobal item = SitecoreContext.GetItem<IStaticFreeformGlobal>(SitecoreContext.GetCurrentItem<Item>().ID.ToString());
                ImageUrl = item.Image.Src;
                return item.Content;
            }
            }
        }
        public IStaticFreeformGlobal GetContentItem
        {
            get
            {
                Item item = SitecoreContext.GetCurrentItem<Item>();
                ReferenceField referenceField = item.Fields["Content"];

                if (referenceField != null && referenceField.TargetItem != null)
                {
                    var contentItem = SitecoreContext.GetItem<IStaticFreeformGlobal>(referenceField.TargetID.Guid, false, false);
                    return contentItem;
                }
                return null;
            }
        }

    }
}