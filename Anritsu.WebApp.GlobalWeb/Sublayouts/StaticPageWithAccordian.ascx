﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaticPageWithAccordian.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.StaticPageWithAccordian" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>


<div class="container accordianclr">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <% if (Model.Content != null)
               { %>
            <h1><%=Editable(x=>x.Content.Title)%></h1>
            <% if (!string.IsNullOrEmpty(Model.GetGlobalImagePath()))
               { %>
            <p>
                <%-- <img src="<%=Model.Content.BannerImage.Src%>" class="img100">--%>

                <%=RenderImage(x=>x.Content.BannerImage,new {@class="img100"},isEditable:true) %>
            </p>
            <% } %>
            <%=Editable(x=>x.Content.Description) %>
            <% } %>
            <div class="accordion">

                <asp:Repeater ID="tabsDescription" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
                    <ItemTemplate>
                        <div class="accordion-item <%#GetClass(Container.ItemIndex)%>">
                            <div class="accordion-title">
                                <span><%#Editable(Item,x=>x.Title) %></span>
                                <div class="accordion-icon">
                                    <div class="<%#GetIconClass(Container.ItemIndex)%>"></div>
                                </div>
                            </div>
                            <div class="accordion-content">
                                <%#Editable(Item,x=>x.Description) %>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

            <% if (Model.Content != null && Model.Content.RelatedProducts != null && Model.Content.RelatedProducts.Any())
               { %>
            <h2 class="margin-top50"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Products") %></h2>
            <div class="products-intro">
                <asp:ListView ID="Products" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.IProductRegional" OnPagePropertiesChanged="Products_PagePropertiesChanged">
                    <ItemTemplate>
                        <div class="products-item">
                            <a href="<%#Item.Url%>">
                                <img src="<%#Editable(Item, x=>x.SelectProduct.Thumbnail.Src) %>"></a>
                            <h3><%#Editable(Item, x=>x.SelectProduct.ModelNumber) %></h3>
                            <p><%#Editable(Item, x=>x.SelectProduct.ProductDescriptor) %></p>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <div class="seprate-line"></div>
            <div class="pagination">
                <span class="page">
                    <asp:DataPager ID="ProductsDataPager" runat="server" PagedControlID="Products" PageSize="6">
                        <Fields>
                            <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active" NextPageText=">" PreviousPageText="<" />
                        </Fields>
                    </asp:DataPager>
                </span>
            </div>
            <% } %>
        </div>


        <div class="content-aside">
            <% if (Model.Content != null)
               { %>
            <p class="mobile">
                <%=RenderImage(x=>x.Content.Image,isEditable:true) %>
            </p>
            <asp:Repeater ID="optionalLinks" runat="server" OnItemDataBound="optionalLinks_ItemDataBound1" OnPreRender="OptionalLinks_OnPreRender">
                <HeaderTemplate>
                    <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
                </HeaderTemplate>
                <ItemTemplate>
                    <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <% } %>
    </div>
</div>
