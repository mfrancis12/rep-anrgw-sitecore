﻿using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using System;
using System.Web.UI.HtmlControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class StaticPageWithContent : GlassUserControl<IStaticPageWithContent>
    {
        public string ImageSrc=string.Empty;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            BindOptionalLinks();
        }

        protected void OptionalLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
            if (linkItem != null && relatedAnchor != null)
            {
                relatedAnchor.InnerText = linkItem.LinkText;
                relatedAnchor.HRef = linkItem.Url;
                relatedAnchor.Target = linkItem.Target;
            }
        }

        protected void OptionalLinks_PreRender(object sender, EventArgs e)
        {
            if (OptionalLinks.Items.Count == 0)
            {
                OptionalLinks.Visible = false;
            }
        }

        public string GetContent()
        {
            IStaticPageWithContent contentItem = GetContentItem();
            if (contentItem != null)
            {
                ImageSrc = contentItem.Image.Src;
                return contentItem.Content;
            }
            else
            {
                IStaticPageWithContent item = SitecoreContext.GetItem<IStaticPageWithContent>(SitecoreContext.GetCurrentItem<Item>().ID.ToString());
                
                if (item.Image != null && item.Image.Src != null)
                {
                    ImageSrc = item.Image.Src;
                }
                else
                {
                    ImageSrc = null;
                }
                return item.Content;
            }
        }

        public void BindOptionalLinks()
        {
            IStaticPageWithContent currentItem = GetContentItem();
            if (currentItem != null && currentItem.OptionalLinks != null)
            {
                GeneralLinks optionalLinks = new GeneralLinks(SitecoreContext.Database.GetItem(currentItem.Id.ToString()), "OptionalLinks");
                OptionalLinks.DataSource = optionalLinks.LinkItems;
                OptionalLinks.DataBind();
            }
            else
            {
                IStaticPageWithContent item = SitecoreContext.GetItem<IStaticPageWithContent>(SitecoreContext.GetCurrentItem<Item>().ID.ToString());
                if (SitecoreContext.GetCurrentItem<IStaticPageWithContent>().OptionalLinks != null)
                {
                    GeneralLinks optionalLinks = new GeneralLinks(SitecoreContext.Database.GetItem(item.Id.ToString()), "OptionalLinks");
                    OptionalLinks.DataSource = optionalLinks.LinkItems;
                    OptionalLinks.DataBind();
                }
            }
        }

        public IStaticPageWithContent GetContentItem()
        {
            Item item = SitecoreContext.GetCurrentItem<Item>();
            ReferenceField referenceField = item.Fields["Content"];

            if (referenceField != null && referenceField.TargetItem != null)
            {
                var contentItem = SitecoreContext.GetItem<IStaticPageWithContent>(referenceField.TargetID.Guid, false, false);
                return contentItem;
            }
            return null;
        }
    }
}