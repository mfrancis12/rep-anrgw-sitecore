﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class SupportLanding : GlassUserControl<IStaticText>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSupportBlocks();           
        }
        private void BindSupportBlocks()
        {
            SupportBlocks.DataSource = Model.GetChildren<ISupportBlock>();
            SupportBlocks.DataBind();
        }
        public List<GeneralLinkItem> GetSupportLinks(ISupportBlock item)
        {
            var supportLinks = new List<GeneralLinkItem>();
            //ISupportBlock currentItem = SitecoreContext.GetItem<ISupportBlock>(item.Id.ToString());
            if (item.OptionalLinks != null)
            {
                GeneralLinks supportGeneralLinks = new GeneralLinks(SitecoreContext.GetItem<Item>(item.Id.ToString()), "OptionalLinks");
                supportLinks = supportGeneralLinks.LinkItems;                
            }
            return supportLinks;
        }

        protected void SupportLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor supportAnchor = (HtmlAnchor)e.Item.FindControl("SupportLink");
            supportAnchor.InnerText = linkItem.LinkText;
            supportAnchor.HRef = linkItem.Url;
            supportAnchor.Target = linkItem.Target;
        }
    }
}