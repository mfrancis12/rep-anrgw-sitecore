﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class SustainabilityCarousel : GlassUserControl<ISustainabilityMapping>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model != null && Model.Content.HeroCarousel.Any())
            {
                buttons.DataSource = Model.GetChildren<ILink>();
                buttons.DataBind();
                sliders.DataSource = Model.Content.HeroCarousel;
                sliders.DataBind();
                categories.DataSource = Model.Content.HeroCarousel;
                categories.DataBind();
            }
            else
            {
                Visible = false;
            }
        }
    }
}