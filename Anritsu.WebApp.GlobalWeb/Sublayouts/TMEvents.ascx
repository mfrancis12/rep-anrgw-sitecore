﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMEvents.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.TMEvents" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="container">
    <div class="content-form">
        <div class="content-detail width100">
	         <p class="event-type-detail"><%= Editable(x=>x.EventType.Value) %></p> 
            <h1><%= Editable(x=>x.Title) %></h1>
            <p>
                 <% if ( string.IsNullOrEmpty(Editable(x=>x.Location)))
               {%>
                <%= Editable(x=>x.StartDate,x=>x.StartDate.ToLocalTime().ToFormattedDate()) %> -  <%= Editable(x=>x.EndDate,x=>x.EndDate.ToLocalTime().ToFormattedDate()) %> 
                 <% } %>
                 <% else 
                    {  %>
                <%= Editable(x=>x.StartDate,x=>x.StartDate.ToLocalTime().ToFormattedDate()) %> -  <%= Editable(x=>x.EndDate,x=>x.EndDate.ToLocalTime().ToFormattedDate()) %> ,   <%= Editable(x=>x.Location) %>
                <% } %>
            </p>

           
            <div class="faq">


                <p><%= Editable(x=>x.EventBodyText) %></p>
              <% if (Model.AdditionalLink != null)
                   {%>
                <%= RenderLink(x=>x.AdditionalLink, isEditable: true, contents:Editable(x=>x.AdditionalTitle))%>
                <% } %>
                 <% if (Model.RegisterOrLaunchLink != null)
                   {%>
                <%= RenderLink(x=>x.RegisterOrLaunchLink,new { @class ="button float-right color-white"}, true, contents:Editable(x=>x.RegisterOrLaunchTitle.Key))%> 
                 <% } %>
            </div>
        </div>

    </div>
</div>
