﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class TabContent : GlassUserControl<IStaticPage>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
       
        public string GetTabTitle
        {
            get
            {
                if (Sitecore.Context.Item.ID.ToString().Equals(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.Home) || Sitecore.Context.Item.ID.ToString().Equals(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.InvestorRelations))
                {
                    return Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "IRNews");
                }
                if (Sitecore.Context.Item.ID.ToString().Equals(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.TestMeasurement))
                {
                    return Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "NewProducts");
                }
                if (Sitecore.Context.Item.ID.ToString().Equals(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.SensingDevices))
                {
                    return Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "NewProducts");
                }
                return string.Empty;
            }
        }
    }
}