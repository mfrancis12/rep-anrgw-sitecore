﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc;
    using Glass.Mapper.Sc.Web.Ui;
    using System;
    using System.Collections.Generic;
    
    using System;

    public partial class TabsRenderer : GlassUserControl<IAboutDropLink>
    {
        private void Page_Load(object sender, EventArgs e)
        {         
            Tabs.DataSource = Model.Content.GetChildren<ITitleWithDescription>();
            Tabs.DataBind();

            TabsDescription.DataSource = Model.Content.GetChildren<ITitleWithDescription>();
            TabsDescription.DataBind();                  
        }
    }
}