﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Items;
    using System;

    public partial class TechnologyHome : GlassUserControl<IStaticPageWithBannerAndThumbnail>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            contentBlock.DataSource = Model.GetChildren<ITechnology>();            
            contentBlock.DataBind();
        }
    }
}