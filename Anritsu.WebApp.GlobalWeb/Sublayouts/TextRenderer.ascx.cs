﻿using Anritsu.WebApp.GlobalWeb.Models;
using Glass.Mapper.Sc.Web.Ui;
using System;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class TextRenderer : GlassUserControl<IModelBase>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ShanonScript.Item = DataSourceItem;
        }
    }
}