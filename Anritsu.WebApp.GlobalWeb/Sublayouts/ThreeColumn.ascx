﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThreeColumn.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.ThreeColumn" %>
<div class="container">
    <div class="site-guide">
        <div class="row">
            <div class="item col1">
                <sc:placeholder id="ThreeColumn1" key="ThreeColumn1" runat="server" />
            </div>

            <div class="item col1">
                <sc:placeholder id="ThreeColumn2" key="ThreeColumn2" runat="server" />
            </div>

            <div class="item col1">
                <sc:placeholder id="ThreeColumn3" key="ThreeColumn3" runat="server" />
            </div>

        </div>
    </div>
</div>
