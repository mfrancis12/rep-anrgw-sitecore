﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TitleAndDescription.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.TitleAndDescription" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div id="<%=Model.Id.ToString()%>" class="section">
    <div class="section-title"><span><%=Editable(Model,x=>x.Title) %></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
    <div class="section-content">
        <%=Editable(Model,x=>x.Description) %>
    </div>
</div>


