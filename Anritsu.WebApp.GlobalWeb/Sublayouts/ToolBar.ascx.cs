﻿using System;
using System.Linq;
//using Amazon.SimpleDB.Model;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data;
using Sitecore.Data.Items;
using Anritsu.Library.GWCMSDK;
using System.Data;
using System.Text.RegularExpressions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class ToolBar : GlassUserControl<IModelBase>
    {
        protected string HasLink { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (SitecoreContext.GetCurrentItem<Item>().TemplateID.ToString().Equals(TemplateIds.ProductRegion))
            //{

            //    BindItems();
            //}
            GetToolbarsFromCMS();
        }
        protected string GetShareLink(string social)
        {
            string link = "";
            if (social.ToUpper() == "LINKEDIN")
            {
                link = "http://www.linkedin.com/shareArticle?mini=true&url=" + HttpContext.Current.Request.Url.AbsoluteUri;
            }
            else if (social.ToUpper() == "TWITTER")
            {
                link = "http://twitter.com/share?url=" + HttpContext.Current.Request.Url.AbsoluteUri;
            }
            else if (social.ToUpper() == "FACEBOOK")
            {
                link = "http://www.facebook.com/sharer.php?u=" + HttpContext.Current.Request.Url.AbsoluteUri;
            }
            else if (social.ToUpper() == "EMAIL")
            {
                link = "mailto:?body=" + HttpContext.Current.Request.Url.AbsoluteUri;
            }
            return link;

        }

        private void GetToolbarsFromCMS()
        {
            try
            {
                var url = HttpContext.Current.Request.Url.AbsolutePath;
                fulltoolbar.Visible = false;
                sharedtoolbar.Visible = false;
                fullsharedtoolbar.Visible = false;
                DataTable dt = null;
                dt = GWCMServiceManager.GetToolbars(url);

                if (dt != null && dt.Rows.Count > 0)
                {

                    if (dt.Rows.Count > 1)
                    {
                        for (int i = dt.Rows.Count - 1; i >= 0; i--)
                        {
                            DataRow dr = dt.Rows[i];
                            if (dr["ToolbarType"] != null)
                            {
                                if (dr["ToolbarType"].ToString() == "SHARED")
                                {
                                    fullsharedtoolbar.Visible = true;
                                    dr.Delete();
                                }
                                else if (dr["ToolbarType"].ToString() != "SHARED")
                                {
                                    fulltoolbar.Visible = true;
                                }
                            }
                        }
                        dt.AcceptChanges();
                    }
                    else if (dt.Rows.Count == 1)
                    {
                        for (int i = dt.Rows.Count - 1; i >= 0; i--)
                        {
                            DataRow dr = dt.Rows[i];
                            if (dr["ToolbarType"] != null)
                            {
                                if (dr["ToolbarType"].ToString() == "SHARED")
                                {
                                    sharedtoolbar.Visible = true;
                                    dr.Delete();
                                }
                                else if (dr["ToolbarType"].ToString() != "SHARED")
                                {
                                    fulltoolbar.Visible = true;
                                }
                            }
                        }
                        dt.AcceptChanges();

                    }

                    repeaterToolBar.DataSource = dt;
                    repeaterToolBar.DataBind();
                }
                else
                {
                    fulltoolbar.Visible = false;
                    sharedtoolbar.Visible = false;
                    fullsharedtoolbar.Visible = false;
                }


               
               


            }
            catch (Exception ex)
            {
                fulltoolbar.Visible = false;
                sharedtoolbar.Visible = false;
                fullsharedtoolbar.Visible = false;
                Sitecore.Diagnostics.Log.Error("Ticker Error" + ex.Message, this);
               
            }


        }

        private string GetCultureFromUrl()
        {
            var language = string.Empty;

            var url = HttpContext.Current.Request.Url.AbsolutePath;
            //url = url.Replace("www-", string.Empty).Replace("?", "/");
            var regexMatch = Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

            if (!string.IsNullOrEmpty(regexMatch.Value))
            {
                language = regexMatch.Value.Replace("/", string.Empty);
            }
            return language;

        }
        protected string GetCTALink(string toolbartype,string link,string businessunit)
        {
            string ctalink = "";
            if(string.IsNullOrEmpty(link)==false)
            {
                if(link.Contains("https")==false)
                {
                    ctalink = "/" + GetCultureFromUrl() + link;
                }
                else
                {
                    ctalink = link;
                }
            }
            if (string.IsNullOrEmpty(toolbartype) == false && string.IsNullOrEmpty(businessunit) == false)
            {
                if (toolbartype == "Request a Quote" && businessunit == "Test & Measurement")
                {
                    string modelNumber = "";
                    try
                    {
                        modelNumber = SitecoreContext.GetCurrentItem<IProductRegional>().SelectProduct.ModelNumber;
                        modelNumber = modelNumber.Replace("<br>", "/");
                    }
                    catch
                    {
                        modelNumber = "";
                    }
                    if (modelNumber != "")
                        ctalink = string.Format("{0}?model={1}&cta=QUOTE", ctalink, modelNumber);

                }
            }
          return ctalink;
        }
        protected string GetClass(string toolbartype)
        {
            string cssClass = "";
            toolbartype = toolbartype.ToUpper();
            if (toolbartype=="BUY")
            {
                cssClass = "icon-helper3";
            }
            else if (toolbartype == "SUPPORT")
            {
                cssClass = "icon-helper2";
            }
           else if (toolbartype == "QUOTE")
            {
                cssClass = "icon-helper1";
            }
            else
            {
                cssClass = toolbartype;
            }
            return cssClass;
          
        }
        protected string GetImage(string imagesrc)
        {
            if (!string.IsNullOrEmpty(imagesrc))
            {
                return string.Format("<img style=\"max-width:50px; max-height:50px; display: block;\" src={0}?h=50&w=50 />", imagesrc);
            }
            return string.Empty;
        }
        public void BindItems()
        {
            //var list = (SitecoreContext.GetItem<IModelBase>(ItemIds.ToolBarFolder).GetChildren<IIconLinkWithTitle>()).ToList();
            //repeaterToolBar.DataSource = list;
            //repeaterToolBar.DataBind();
        }
        protected string GetLink(IIconLinkWithTitle item)
        {
            var sublayout = Parent as Sublayout;
            if (sublayout == null) return null;

            if (item.Id.Equals(new Guid(ItemIds.RequestAQuote)))
            {
                var requestQuoteCtaItemId = sublayout.Attributes["RequestQuoteCTA"];
                if (!string.IsNullOrEmpty(requestQuoteCtaItemId))
                {
                    var quoteItem = SitecoreContext.GetItem<ILink>(requestQuoteCtaItemId);
                    //if the current item is product
                    if (SitecoreContext.GetCurrentItem<IModelBase>()
                            .TemplateId.ToString()
                            .Equals(TemplateIds.ProductRegion))
                    {
                        return quoteItem.Link != null
                                    ? string.Format("{0}?model={1}&cta=QUOTE", quoteItem.Link.Url,
                                    SitecoreContext.GetCurrentItem<IProductRegional>().SelectProduct.ModelNumber.Replace("<br>", "/")) : string.Empty;
                    }
                    return quoteItem.Link != null ? quoteItem.Link.Url : string.Empty;
                }
            }
            else if (item.Id.Equals(new Guid(ItemIds.GetSupport)))
            {
                var getSupportCtaItemId = sublayout.Attributes["SupportCTA"];
                if (string.IsNullOrEmpty(getSupportCtaItemId)) return string.Empty;

                var quoteItem = SitecoreContext.GetItem<ILink>(getSupportCtaItemId);
                return quoteItem.Link != null ? quoteItem.Link.Url : string.Empty;
            }
            else if (item.Id.Equals(new Guid(ItemIds.BuyCta)))
            {
                var buyCtaItemId = sublayout.Attributes["BuyCTA"];
                if (string.IsNullOrEmpty(buyCtaItemId)) return string.Empty;

                var quoteItem = SitecoreContext.GetItem<ILink>(buyCtaItemId);
                return quoteItem.Link != null ? quoteItem.Link.Url : string.Empty;
            }
            else if (item.Link != null && !string.IsNullOrEmpty(item.Link.Url))
            {
                HasLink = item.Link.Url;
            }
            return string.Empty;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected string GetCurrentPageUrl()
        {
            var domain = HttpContext.Current.Request.Url.GetComponents(
               UriComponents.Scheme | UriComponents.Host, UriFormat.Unescaped);
            return domain + Sitecore.StringUtil.EnsurePrefix('/', LinkManager.GetItemUrl(Sitecore.Context.Item));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        protected string GetShareLink(IIconLinkWithTitle item)
        {
            Sitecore.Data.Items.Item shareItem = Sitecore.Context.Database.GetItem(item.Id.ToString());
            LinkField lf = shareItem.Fields["Link"];
            if (lf != null && lf.Url != null)
            {
                return lf.Url + GetCurrentPageUrl();
            }
            return string.Empty;
        }
    }
}
