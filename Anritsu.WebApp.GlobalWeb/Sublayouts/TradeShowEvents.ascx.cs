﻿namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using System;

    public partial class TradeShowEvents :GlassUserControl<ITradeshows>
    {
        protected string HasImage { get; set; }
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (Model == null)
                return;

        }
        protected string GetDivImgClass(string imagePath)
        {
            HasImage = imagePath == null ? "item-noimg" : "item";
            return HasImage;
        }
    }
}