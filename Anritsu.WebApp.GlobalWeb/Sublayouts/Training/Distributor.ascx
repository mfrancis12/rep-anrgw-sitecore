﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Distributor.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.Distributor" %>
<div class="container accordianclr">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.PageTitle) %></h1>
            <p><%=Editable(x=>x.Content) %></p>
    <div class="divcenter">
    <b><asp:Literal ID="ltrClassTitle" runat="server"></asp:Literal></b><br /><br />
    <asp:Literal ID="ltrClassInfo" runat="server"></asp:Literal><br /><br />
    </div>
    <div>
    <asp:Label ID="lblClassDec" runat="server"></asp:Label><br /><br />
    <asp:Label id="lblDistDesc" runat="server"></asp:Label>
    </div><br />
    <div class="divcenter"><asp:HyperLink ID="lnkLogo" runat="server"><asp:Image ID="imgLogo" runat="server" /></asp:HyperLink>
    </div>
        </div>
        <div class="content-aside">
            <sc:placeholder runat="server" key="RightRail" />
        </div>
    </div>
</div>
