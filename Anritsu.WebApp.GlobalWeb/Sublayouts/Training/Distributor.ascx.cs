﻿using System;
using System.Data;
using System.Text;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using BLL = Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{
    public partial class Distributor : GlassUserControl<IStaticText>
    {
        private int _eventId;
        private int _cultureGroupId;
        private DataTable _dt;
        private void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString[Constants.QueryStringVariables.EventId] == null || !int.TryParse(Request.QueryString[Constants.QueryStringVariables.EventId].HtmlDecode(), out _eventId))
            {
                throw new System.Web.HttpException(404, "File not found.");
            }

            var str = new StringBuilder();

            _cultureGroupId = BLL.Common.GetCultureGroupIdForCultureCode(Sitecore.Context.Language.Name);
            _dt = BLL.Training.GetDistributorInfo(_eventId, _cultureGroupId);

            if (_dt.Rows.Count <= 0) return;
            ltrClassTitle.Text = _dt.Rows[0]["ClassName"].ToString();
            var dateFrom = DateTime.Parse(_dt.Rows[0]["DateFrom"].ToString());
            var dateTo = DateTime.Parse(_dt.Rows[0]["DateTo"].ToString());
            str.Append(dateFrom.ToFormattedDate());
            str.Append("-");
            str.Append(dateTo.ToFormattedDate());
            str.Append(", ");
            str.Append(_dt.Rows[0]["Location"]);
            str.Append(", ");
            str.Append(_dt.Rows[0]["DistributorName"]);
            ltrClassInfo.Text = str.ToString();
            lblClassDec.Text = _dt.Rows[0]["ClassDescription"].ToString();
            lblDistDesc.Text = _dt.Rows[0]["AdditionalText"].ToString();
            imgLogo.ImageUrl = _dt.Rows[0]["DistributorLogo"].ToString();
            lnkLogo.NavigateUrl = _dt.Rows[0]["DistributorURL"].ToString();
        }
    }
}