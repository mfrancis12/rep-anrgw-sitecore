﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using Anritsu.Library.EmailSDK.EmailWebRef;
using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using BLL = Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using BO = Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using SSOSDK = Anritsu.Library.SSOSDK;
using CKSDK = Anritsu.Library.CheckoutSDK;
using EmailSDK = Anritsu.Library.EmailSDK;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Utilities;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Globalization;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{

    public partial class ReviewOrder : GlassUserControl<IStaticText>
    {
        private string _strState;
        private int _registrationId;
        private int _cultureGroupId = BLL.Common.GetCultureGroupIdForCultureCode(Sitecore.Context.Language.Name);
        public Guid TrainingTransactionKey
        {
            get
            {
                var trtk = Request[Constants.QueryStringVariables.TransactionKey];
                if (String.IsNullOrEmpty(trtk)) throw new HttpException(404, "Page not found.");
                var trainingTranKey = new Guid(trtk);
                if (trainingTranKey== Guid.Empty) throw new HttpException(404, "Page not found.");
                return trainingTranKey;
            }
        }

        public Guid CheckoutTransactionKey
        {
            get
            {
                var ctk = Request["ctk"];
                if (String.IsNullOrEmpty(ctk)) throw new HttpException(404, "Page not found.");
                var ctkGuid = new Guid(ctk);
                if (ctkGuid== Guid.Empty) throw new HttpException(404, "Page not found.");
                return ctkGuid;
            }
        }

        public TrainingAfterCheckoutSession BackButtonTrack
        {
            get { return Session[Constants.SessionVariables.TrainingBackPageTrack] as TrainingAfterCheckoutSession; }
            set { Session[Constants.SessionVariables.TrainingBackPageTrack] = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //DisableDownloadCache();
            Session[Constants.SessionVariables.CartItems] = null;
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                // redirect visitor to SSL connection
                Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            cntrlState.Text = _strState;
        }

        private void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (BackButtonTrack != null)
            {
                if (BackButtonTrack.TrainingTransactionKey == TrainingTransactionKey && BackButtonTrack.IsProcessed)
                {
                    //RedirectToFriendlyMessagePage(
                    //    HttpContext.GetGlobalResourceObject("ErrorMessage", "CourseRegistrationReSubmitError")
                    //        .ToString());
                    return;
                }
                BackButtonTrack = null; //different tran so just clear it
            }

            // default to US
            ddlCountry.Text = @"US";
            ProcessTransactionKey(TrainingTransactionKey);
        }

        private void ProcessTransactionKey(Guid trtk)
        {
            if (trtk== Guid.Empty) return;
            var user = LoggedInUser;
            var ckClient = new CKSDK.CheckoutClient();
            var resp = ckClient.MakeGetTransactionCall(TrainingTransactionKey);

            #region " validate response "

            if (resp == null
                || resp.StatusCode != CKSDK.ARCKRefPaymentManagementAPI.ApiCallStatusCodeType.SUCCESS
                || resp.CheckoutTransaction == null
                || String.IsNullOrEmpty(resp.CheckoutTransaction.ClientTranReference)
                ||
                (resp.CheckoutTransaction.TransactionStatusCode !=
                 CKSDK.ARCKRefPaymentManagementAPI.CheckoutTransactionStatusCodeType.PAYMENT_AUTHORIZED
                 &&
                 resp.CheckoutTransaction.TransactionStatusCode !=
                 CKSDK.ARCKRefPaymentManagementAPI.CheckoutTransactionStatusCodeType.PAYMENT_RECEIVED)
                //do we have this option??
                )
            {
                //**Added in Aug-2014 build to show userfriendly message on cancelling the transaction(ticket:GWS-1156) 
                if (string.IsNullOrEmpty(Request.QueryString["pfmode"]))
                    throw new HttpException(404, "Page not found.");
                if (Request.QueryString["pfmode"].ToLower() == "expresscheckoutcancelled" && Request.QueryString["ctk"] != null &&Request.QueryString["mctk"]!=null)
                {                
                    string paymentUrlWithTokens = string.Format("{0}?{1}={2}&{3}={4}"
                       , "~/Checkout/Secured/Payment-Items.aspx"
                       , "mctk"
                       , Request.QueryString["mctk"]
                       , "ctk"
                       , Request.QueryString["ctk"]
                       );
                   Response.Redirect(paymentUrlWithTokens);

                }
                //** end 
                //hack or bad transaction
                throw new HttpException(404, "Page not found.");
            }

            #endregion

            var ckTran = resp.CheckoutTransaction;

            var trainingSession = new TrainingAfterCheckoutSession();
            trainingSession.TrainingTransactionKey = TrainingTransactionKey;
            trainingSession.CheckoutTransactionKey = CheckoutTransactionKey;
            trainingSession.LastPageName = "revieworder.aspx";
            trainingSession.CKTran = ckTran;
            trainingSession.IsProcessed = false;
            if (ckTran.ShippingAddress != null)
            {
                #region " shipping "

                txtTitle.Text = ckTran.ShippingAddress.JobTitle;
                txtFirstName.Text = ckTran.ShippingAddress.FirstName;
                txtLastName.Text = ckTran.ShippingAddress.LastName;
                txtAddress1.Text = ckTran.ShippingAddress.Address1;
                txtAddress2.Text = ckTran.ShippingAddress.Address2;
                ddlCountry.Text = ckTran.ShippingAddress.CountryCode;
                txtCompany.Text = ckTran.ShippingAddress.CompanyName;
                txtEmail.Text = ckTran.ShippingAddress.EmailAddress;
                txtCity.Text = ckTran.ShippingAddress.City;
                txtPhone.Text = ckTran.ShippingAddress.PhoneNumber;
                _strState = ckTran.ShippingAddress.State;
                cntrlState.Text = ckTran.ShippingAddress.State;
                txtFax.Text = ckTran.ShippingAddress.FaxNumber;
                txtPostalCode.Text = ckTran.ShippingAddress.PostalCode;

                #endregion
            }
            lblAmountDue.Text = ckTran.SubTotal.ToString("$###,###.00");

            #region " payment info "

            CKSDK.ARCKRefPaymentManagementAPI.PaymentDataVoucher_GetByTidRespType voucher = null;
            CKSDK.ARCKRefPaymentManagementAPI.PaymentDataPurchaseOrder_GetByTidRespType po = null;
            CKSDK.ARCKRefPaymentManagementAPI.PaymentDataPayPal_GetByTidRespType pp = null;

            switch (ckTran.PaymentMethodCode)
            {
                case CKSDK.ARCKRefPaymentManagementAPI.PaymentMethodCodeType.Voucher:
                    voucher = ckClient.MakePaymentDataVoucher_GetByTid(ckTran.Tid);
                    break;
                case CKSDK.ARCKRefPaymentManagementAPI.PaymentMethodCodeType.PO:
                    po = ckClient.MakePaymentDataPurchaseOrder_GetByTid(ckTran.Tid);
                    break;
                case CKSDK.ARCKRefPaymentManagementAPI.PaymentMethodCodeType.CreditCard:
                    pp = ckClient.MakePaymentDataPayPal_GetByTid(ckTran.Tid);
                    break;
            }

            pnlBillingInfo.Visible = false;

            if (pp != null)
            {
                //Billing Information
                lblBillingTitle.Text = pp.BillingAddress.JobTitle;
                lblBillingName.Text = pp.BillingAddress.FirstName + @" " + pp.BillingAddress.LastName;
                lblBillingAddress1.Text = pp.BillingAddress.Address1;
                lblBillingAddress2.Text = pp.BillingAddress.Address2;
                lblBillingCountry.Text = pp.BillingAddress.CountryCode;
                lblBillingCompany.Text = pp.BillingAddress.CompanyName;
                lblBillingEmail.Text = pp.BillingAddress.EmailAddress;
                lblBillingCity.Text = pp.BillingAddress.City;
                lblBillingPhone.Text = pp.BillingAddress.PhoneNumber;
                lblBillingState.Text = pp.BillingAddress.State;
                lblBillingFax.Text = pp.BillingAddress.FaxNumber;
                lblBillingPostalCode.Text = pp.BillingAddress.PostalCode;
                pnlBillingInfo.Visible = true;
            }
            else if (voucher != null)
            {
                lblPO_VoucherNumberCaption.Text = "VoucherNumber" + @":";
                lblPO_VoucherNumber.Text = voucher.Voucher.VoucherNumber;
            }
            else if (po != null)
            {
                lblPO_VoucherNumberCaption.Text = "PONumberLabel" + @":";
                lblPO_VoucherNumber.Text = po.PurchaseOrder.PONumber;
            }

            #endregion

            #region " training cart info "

            var isClassFull = false;
            var cartKey = trainingSession.CKTran.ClientTranReference;
                //Session[Constants.SessionVariables.TrainingCartKey] as string;
            var cartItems = BLL.Training.GetItemsInCart(cartKey, _cultureGroupId);
            var eventIDsInCart = new List<int>(1); // expected only 1 event in cart
            var cartAttendees = new DataTable();

            foreach (DataRow cartRow in cartItems.Rows)
            {
                var tempEventId = (int) cartRow["eventID"];

                if (eventIDsInCart.Contains(tempEventId)) continue;
                eventIDsInCart.Add(tempEventId);

                var availableSeatsDr = BLL.Training.GetClassEventAvailableSeats(_cultureGroupId, tempEventId);
                if (availableSeatsDr == null) continue;
                var availableSeats = (int) availableSeatsDr["AvailableSeats"];
                if (availableSeats >= (int) cartRow["Attendees"]) continue;
                LabelMessage.Text =
                    string.Format(
                        "We are sorry but this class no longer has the availability that you require.  There are {0} seat(s) left for this particular event: {1}",
                        availableSeats, cartRow["ClassName"] as string);
                isClassFull = true;
            }

            foreach (var eventId in eventIDsInCart)
            {
                var temp = BLL.Training.GetClassAttendeesInCart(cartKey, eventId);
                temp.Columns.Add("EventID", typeof (int));
                for (var i = 0; i < temp.Rows.Count; i++)
                {
                    temp.Rows[i]["EventID"] = eventId;
                }
                cartAttendees.Merge(temp);
            }

            #endregion

            // Attendee related information
            //rptAttendees.DataSource = cartAttendees;
            //rptAttendees.DataBind();

            BackButtonTrack = trainingSession;

            if (isClassFull)
            {
                btnTrainingCart.Visible = true;
            }
            else
            {
                CompleteOrder();
                //lblWarning.Visible = true;
            }
        }

        private void SendAdminMail(CKSDK.ARCKRefPaymentManagementAPI.CheckoutTransactionType transaction,
            DataSet cartInfo, SSOSDK.SSOApiClient.GWSsoUserType user)
        {
            if (transaction == null || cartInfo == null || cartInfo.Tables.Count < 1 || user == null)
                return;

            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = Constants.StaticVariables.TrainingCheckoutAdmin,
                DistributionListKey = "Training_Checkout_Admin_" +(Sitecore.Context.Language.Name.Length>2? Sitecore.Context.Language.Name.Substring(3, 2):Sitecore.Context.Language.Name),
                CultureGroupId = _cultureGroupId
            };

            #region body replacement values


            var bodyKeys = new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = transaction.ShippingAddress.FirstName
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = transaction.ShippingAddress.LastName
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = transaction.ShippingAddress.EmailAddress
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS1]]",
                ReplacementValue = transaction.ShippingAddress.Address1
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS2]]",
                ReplacementValue = transaction.ShippingAddress.Address2
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CITY]]",
                ReplacementValue = transaction.ShippingAddress.City
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = transaction.ShippingAddress.State
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = transaction.ShippingAddress.PhoneNumber
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYCODE]]",
                ReplacementValue = transaction.ShippingAddress.CountryCode
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = transaction.ShippingAddress.CompanyName
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = transaction.ShippingAddress.FaxNumber
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[JOBTITLE]]",
                ReplacementValue = transaction.ShippingAddress.JobTitle
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSTALCODE]]",
                ReplacementValue = transaction.ShippingAddress.PostalCode
            };
            bodyKeys.Add(keyValue);

            var body = new StringBuilder();
            foreach (DataRow dr in cartInfo.Tables[0].Rows)
            {
                body.Append(
                    string.Format(
                        "<tr><td><font face=\"VERDANA\" SIZE=\"1\" >{0}</font></td><td><font face=\"VERDANA\" SIZE=\"1\" >{1}</font></td><td><font face=\"VERDANA\" SIZE=\"1\" >{2:d} - {3:d}</font></td><td><font face=\"VERDANA\" SIZE=\"1\" >{4}</font></td></tr>"
                        , dr["ClassName"], dr["PartNumber"], ((DateTime) dr["DateFrom"]).ToFormattedDate(),
                        ((DateTime) dr["DateTo"]).ToFormattedDate(), dr["Location"]));
            }

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REGISTRATIONDETAILS]]",
                ReplacementValue = body.ToString().Replace("<p>", "<p style=\"margin:0;padding:0;\">")
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PAYMENTMETHOD]]",
                ReplacementValue = transaction.PaymentMethodCode.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SUBTOTAL]]",
                ReplacementValue = transaction.SubTotal.ToString(CultureInfo.InvariantCulture)
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ESTIMATEDTAXES]]",
                ReplacementValue = transaction.SaleTax.ToString(CultureInfo.InvariantCulture)
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PAYMENTTOTAL]]",
                ReplacementValue = transaction.TotalReceived.ToString(CultureInfo.InvariantCulture)
            };
            bodyKeys.Add(keyValue);


            #endregion

            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();

            var emailCallResponse = EmailSDK.EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode != 0)
                throw new ArgumentException(emailCallResponse.ResponseMessage);
        }

        private void SendUserMail(CKSDK.ARCKRefPaymentManagementAPI.CheckoutTransactionType transaction,
            DataSet cartInfo, SSOSDK.SSOApiClient.GWSsoUserType user)
        {
            if (transaction == null || cartInfo == null || cartInfo.Tables.Count < 1 || user == null)
                return;

            var templatedReq = new SendTemplatedEmailCallRequest();
            templatedReq.EmailTemplateKey = Constants.StaticVariables.TrainingCheckoutUser;
            templatedReq.CultureGroupId = _cultureGroupId;

            #region body replacement values

            var bodyKeys = new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = transaction.ShippingAddress.FirstName
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = transaction.ShippingAddress.LastName
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[TRANSACTIONID]]",
                ReplacementValue = transaction.Tid.ToString(CultureInfo.InvariantCulture)
            };
            bodyKeys.Add(keyValue);

            var body = new StringBuilder();
            var i = 1;
            foreach (DataRow dr in cartInfo.Tables[0].Rows)
            {
                body.Append(
                    string.Format(
                        "<tr><td colspan=\"2\" bgcolor=\"#289b7d\" height=\"30\"><font face=\"VERDANA\" size=\"2\" color=\"WHITE\"><b>&nbsp;Class {0}</b></font></td></tr>",
                        i));
                body.Append("<tr><td><table width=\"550\" border=\"0\">");
                body.Append(
                    string.Format(
                        "<tr><td width=\"200\" bgcolor=\"#EEEEEE\"><font face=\"VERDANA\" size=\"1\"><b>PartNumber</b></font></td><td width=\"500\"><font face=\"VERDANA\" size=\"1\">{0}</font></td></tr>",
                        dr["PartNumber"]));
                body.Append(
                    string.Format(
                        "<tr><td width=\"200\" bgcolor=\"#EEEEEE\"><font face=\"VERDANA\" size=\"1\"><b>Course</b></font></td><td width=\"500\"><font face=\"VERDANA\" size=\"1\">{0}</font></td></tr>",
                        dr["ClassName"]));
                body.Append(
                    string.Format(
                        "<tr><td width=\"200\" bgcolor=\"#EEEEEE\"><font face=\"VERDANA\" size=\"1\"><b>Date</b></font></td><td width=\"500\"><font face=\"VERDANA\" size=\"1\">{0} - {1}</font></td></tr>",
                        ((DateTime) dr["DateFrom"]).ToFormattedDate(),
                        ((DateTime) dr["DateTo"]).ToFormattedDate()));
                body.Append(
                    string.Format(
                        "<tr><td width=\"200\" bgcolor=\"#EEEEEE\"><font face=\"VERDANA\" size=\"1\"><b>Location</b></font></td><td width=\"500\"><font face=\"VERDANA\" size=\"1\">{0}</font></td></tr>",
                        dr["Location"]));
                body.Append(
                    "<tr><td colspan=\"2\" cellspacing=\"5\" cellpadding=\"5\"><table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" bordercolor=\"LIGHTGREY\" bordercolordark=\"WHITE\" width=\"100%\">");
                body.Append(
                    "<tr><td Bgcolor=\"#aaaaaa\" colspan=\"2\"><font face=\"VERDANA\" SIZE=\"1\" ><b>Attendees</b></font></td></tr>");
                body.Append(
                    "<tr><td colspan=\"2\" cellspacing=\"2\" cellpadding=\"2\"><table border=\"0\" width=\"80%\">");

                if (cartInfo.Tables.Count > 1)
                {
                    cartInfo.Tables[1].DefaultView.RowFilter = "eventid=" + dr["eventid"];
                    foreach (DataRow drAttendees in cartInfo.Tables[1].DefaultView.ToTable().Rows)
                    {
                        body.Append(
                            string.Format(
                                "<tr><td width=\"15px\">&nbsp;</td><td><font face=\"VERDANA\" SIZE=\"1\">{0}</font></td><td><font face=\"VERDANA\" SIZE=\"1\">{1}</font></td><td><font face=\"VERDANA\" SIZE=\"1\">{2}</font></td><td><font face=\"VERDANA\" SIZE=\"1\">{3}</font></td></tr>",
                                drAttendees["FirstName"], drAttendees["LastName"], drAttendees["PhoneNumber"],
                                drAttendees["EmailID"]));
                    }
                }

                body.Append("</table></td></tr></table> </td></tr></table>");
                i++;
            }
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REGISTRATIONDETAILS]]",
                ReplacementValue = body.ToString().Replace("<p>", "<p style=\"margin:0;padding:0;\">")
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PAYMENTMETHOD]]",
                ReplacementValue = transaction.PaymentMethodCode.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SUBTOTAL]]",
                ReplacementValue = string.Format("{0:C}", transaction.SubTotal)
            };
            bodyKeys.Add(keyValue);

            #endregion

            var customerEmails = new List<string> {transaction.ShippingAddress.EmailAddress};
            if (user.EmailAddress != transaction.ShippingAddress.EmailAddress)
                customerEmails.Add(user.EmailAddress);
            templatedReq.ToEmailAddresses = customerEmails.ToArray();
            templatedReq.BccEmailAddresses = new[] {ConfigurationManager.AppSettings["USTrainingEmailAddress"]};

            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();

            var emailCallResponse = EmailSDK.EmailServiceManager.SendTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode != 0)
                throw new ArgumentException(emailCallResponse.ResponseMessage);
        }

        protected void btnTrainingCart_Click(object sender, EventArgs e)
        {
            Response.Redirect(SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingCart).Url);
        }

        private void CompleteOrder()
        {
            var user = LoggedInUser;
            var trainingSession = BackButtonTrack;
            if (trainingSession == null
                || trainingSession.IsProcessed
                || trainingSession.CKTran == null
                || BLL.Training.Training_Payment_Transaction_IsExist(trainingSession.CKTran.TransactionKey)
                )
            {
                //RedirectToFriendlyMessagePage(
                //    HttpContext.GetGlobalResourceObject("ErrorMessage", "CourseRegistrationReSubmitError").ToString());
                return;
            }

            var trainingCartKey = trainingSession.CKTran.ClientTranReference;
            Session[Constants.SessionVariables.TrainingCartKey] = trainingSession.CKTran.ClientTranReference;
            var cartDt = BLL.Training.GetCart(trainingCartKey);
            var referredBy = (cartDt != null && cartDt.Rows.Count > 0)
                ? cartDt.Rows[0]["ReferredBy"] as string
                : string.Empty;
            //CKSDK.ARCKRefPaymentManagementAPI.CheckoutTransactionType transaction = trainingSession.CKTran;

            var registration = new BO.TraningRegistration
            {
                UserId = user.UserId,
                EmailId = txtEmail.Text,
                Company = txtCompany.Text,
                JobTitle = txtTitle.Text,
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                Address1 = txtAddress1.Text,
                Address2 = txtAddress2.Text,
                CountryName = ddlCountry.Text,
                City = txtCity.Text,
                PhoneNumber = txtPhone.Text,
                State = cntrlState.Text,
                PostalCode = txtPostalCode.Text,
                FaxNumber = txtFax.Text,
                Amount = decimal.Parse(trainingSession.CKTran.SubTotal.ToString(CultureInfo.InvariantCulture)),
                ReferredBy = referredBy
            };

            BLL.Training.InsertRegistrationDetails(registration, out _registrationId);
            trainingSession.CartData = BLL.Training.UpdateCartItemsDetails(user.UserId
                , trainingSession.CKTran.Tid
                , _registrationId
                , trainingSession.CKTran.TransactionKey.ToString().ToUpper()
                , trainingCartKey);
            registration.RegistrationId = _registrationId;
            trainingSession.TraningRegInfo = registration;
            trainingSession.IsProcessed = true;
            BackButtonTrack = trainingSession;

            try
            {
                SendAdminMail(trainingSession.CKTran, trainingSession.CartData, user);
                SendUserMail(trainingSession.CKTran, trainingSession.CartData, user);
            }
            catch (Exception ex)
            {
                //because registration is not rely on email status, just log the error if there is some email issue.

                Sitecore.Diagnostics.Log.Error(ex.StackTrace,ex,GetType());
            }
            Confirmation();
        }

        #region Moved from old Confirmation.aspx page

        private void Confirmation()
        {
            pnlConfirmation.Visible = true;
            pnlConfirmationText.Visible = true;
            ltrUserEmailId.Text = LoggedInUser.EmailAddress;
            lnkPrint.NavigateUrl =
                string.Format("{0}?{1}={2}&{3}={4}&p=1",Model.Url,
                    Constants.QueryStringVariables.TransactionKey,
                    BackButtonTrack.CKTran.TransactionKey.ToString().ToUpperInvariant(),
                    Constants.QueryStringVariables.RegistrationId,
                    _registrationId);

            ltrText.Text = Translate.TextByDomain("Training","Confirmation").Replace("[[TID]]", BackButtonTrack.CKTran.Tid.ToString(CultureInfo.InvariantCulture));
            lstvRegistrationDetails.DataSource = BackButtonTrack.CartData;
            lstvRegistrationDetails.DataBind();
        }

        //protected void Page_PreInit(object sender, EventArgs e)
        //{
        //    if (Request.QueryString["print"] != "true" || BackButtonTrack == null || BackButtonTrack.CartData == null)
        //        return;
        //    var xml = BackButtonTrack.CartData.GetXml();

        //    var xmlInput = new StringReader(xml);
        //    var xmlReader = new XmlTextReader(xmlInput);

        //    // Create required writer for output   
        //    var stringWriter = new StringWriter();
        //    var transformedXml = new XmlTextWriter(stringWriter);

        //    // Create a XslCompiledTransform to perform transformation   
        //    var xsltTransform = new XslCompiledTransform();

        //    try
        //    {
        //        xsltTransform.Load(Server.MapPath("ConfirmationPrint.xslt"));
        //        xsltTransform.Transform(xmlReader, transformedXml);
        //    }
        //    catch (XmlException xmlEx)
        //    {
        //        // TODO : log - "Could not load XSL transform: \n\n" + xmlEx.Message   
        //        throw;
        //    }
        //    catch (XsltException xsltEx)
        //    {
        //        // TODO : log - "Could not process the XSL: \n\n" + xsltEx.Message + "\nOn line " + xsltEx.LineNumber + " @ " + xsltEx.LinePosition)   
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        // TODO : log   
        //        throw;
        //    }

        //    var result = stringWriter.ToString();

        //    Response.Clear();
        //    Response.Write(result);
        //    Response.End();

        //    //base.OnPreInit(e);
        //}

        protected DataView GetAttendees()
        {
            if (BackButtonTrack == null || BackButtonTrack.CartData == null || BackButtonTrack.CartData.Tables.Count < 2)
                return null;
            BackButtonTrack.CartData.Tables[1].DefaultView.RowFilter = "eventid=" + Eval("eventid");
            return BackButtonTrack.CartData.Tables[1].DefaultView;
        }

        protected string GetScheduleDate()
        {
            var dateFrom = (DateTime) Eval("DateFrom");
            var dateTo = (DateTime) Eval("DateTo");
            var str = string.Format("{0} - {1}", dateFrom.ToFormattedDate(), dateTo.ToFormattedDate());
            return str;
        }

        
        private static GWSsoUserType LoggedInUser
        {
            get
            {
                var usr = HttpContext.Current.Session[SessionVariables.UserLoggedIn]
                    as GWSsoUserType;

                if (usr == null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    usr = RefreshUserSession();
                }
                return usr;
            }
        }

        public static GWSsoUserType RefreshUserSession()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;

            string redirectLoginUrl = string.Format("/{0}/Sign-In.aspx?ReturnURL={1}"
                , BasePage.GetCurrentLanguage(), HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
            HttpContext.Current.Response.Redirect(redirectLoginUrl);

            return null;
        }

        #endregion    
    }
}