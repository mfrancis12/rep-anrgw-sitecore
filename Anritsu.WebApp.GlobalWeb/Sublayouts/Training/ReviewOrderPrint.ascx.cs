﻿using System;
using System.Data;
using Anritsu.WebApp.GlobalWeb.Utilities;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{
    public partial class ReviewOrderPrint : System.Web.UI.UserControl
    {

        public TrainingAfterCheckoutSession BackButtonTrack
        {
            get { return Session[Constants.SessionVariables.TrainingBackPageTrack] as TrainingAfterCheckoutSession; }
            set { Session[Constants.SessionVariables.TrainingBackPageTrack] = value; }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            if (BackButtonTrack != null && BackButtonTrack.IsProcessed&&BackButtonTrack.CartData!=null)
            {
                lstvRegistrationDetails.DataSource = BackButtonTrack.CartData;
                lstvRegistrationDetails.DataBind();
            }
        }
        protected DataView GetAttendees()
        {
            if (BackButtonTrack == null || BackButtonTrack.CartData == null || BackButtonTrack.CartData.Tables.Count < 2)
                return null;
            BackButtonTrack.CartData.Tables[1].DefaultView.RowFilter = "eventid=" + Eval("eventid");
            return BackButtonTrack.CartData.Tables[1].DefaultView;
        }

        protected string GetScheduleDate()
        {
            var dateFrom = (DateTime)Eval("DateFrom");
            var dateTo = (DateTime)Eval("DateTo");
            var str = string.Format("{0} - {1}", dateFrom.ToFormattedDate(), dateTo.ToFormattedDate());
            return str;
        }

    }
}