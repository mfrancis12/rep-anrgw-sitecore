﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingCart.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.TrainingCart" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<div class="container">
    <div class="content-form">
        <div class="content-detail">
            <h1><%=Editable(x=>x.PageTitle) %></h1>
            <p><%=Editable(x=>x.Content) %></p>
            <div class="form-container">
                <div class="group">
                    <div class="detail-table">
                        <asp:GridView ID="GridCart" runat="server" Width="100%"
                            AutoGenerateColumns="false" EmptyDataText="No items added to cart">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkDelete" runat="server" CommandArgument='<%# Eval("eventID").ToString() %>' OnClick="LinkDelete_OnClick"><%=Translate.TextByDomain("Glossary", "Delete") %></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate><%=Translate.TextByDomain("Training", "TrainingClass") %></HeaderTemplate>
                                    <ItemTemplate><%#Eval("ClassName")%></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate><%=Translate.TextByDomain("Training", "Rate") %></HeaderTemplate>
                                    <ItemTemplate><%#string.Format("{0:C2}",Eval("Price"))%></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate><%=Translate.TextByDomain("Training", "NoOfAttendees") %></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAttendees" runat="server" Text='<%#Eval("Attendees")%>'></asp:Label>
                                        <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%#GetUrl(Eval("eventID").ToString()) %>' Font-Size="XX-Small" ToolTip="Edit"><%=Translate.TextByDomain("Glossary", "Edit") %></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate><%=Translate.TextByDomain("Training", "Amount") %></HeaderTemplate>
                                    <ItemTemplate><%#string.Format("{0:C2}",Eval("Amount"))%></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div class="group input-text">
                    <label><%=Translate.TextByDomain("Training", "Amount") %></label>
                    <asp:TextBox runat="server" ID="TextTotal" MaxLength="10" ReadOnly="True"></asp:TextBox>
                </div>
                <div class="group">
                    <asp:Button runat="server" ID="ReviewOrder" Text="Review Order" CssClass="button" OnClick="ReviewOrder_OnClick" />
                    <asp:HyperLink runat="server" ID="LinkAddMore" CssClass="button"><%=Translate.TextByDomain("Training", "AddMoreClasses") %></asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="content-aside">
        </div>
    </div>
</div>
