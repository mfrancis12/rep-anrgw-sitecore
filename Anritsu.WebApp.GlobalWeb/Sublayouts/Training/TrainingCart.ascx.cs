﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using BO = Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{
    public partial class TrainingCart : GlassUserControl<IStaticText>
    {
        private DataTable _dt;
        private int _cultureGroupId;

        protected void Page_Init(object sender, EventArgs e)
        {
            //DisableDownloadCache();
            // check if secure connection used
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                // redirect visitor to SSL connection
                Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }
        }
        private void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (!string.IsNullOrEmpty(Request.QueryString[QueryStringVariables.Mode]) && Request.QueryString[QueryStringVariables.Mode].ToLower(CultureInfo.InvariantCulture).Equals("checkout"))
            {
                ReviewOrder_OnClick(this, e);
                return;
            }

            var cartKey = Session[SessionVariables.TrainingCartKey] as string;
            _cultureGroupId = BusinessLayer.BLL.Common.GetCultureGroupIdForCultureCode(Sitecore.Context.Language.Name);
            if (!string.IsNullOrEmpty(cartKey))
            {
                _dt = BusinessLayer.BLL.Training.GetItemsInCart(cartKey, _cultureGroupId);
                GridCart.DataSource = _dt;
                Session[SessionVariables.CartItems] = _dt;
                GridCart.DataBind();
                TextTotal.Text = GetTotal();
            }


            if (string.IsNullOrEmpty(cartKey) || (_dt == null || _dt.Rows.Count < 1))
                ReviewOrder.Visible = false;
            LinkAddMore.NavigateUrl = SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingIndex).Url;
        }

        protected void ReviewOrder_OnClick(object sender, EventArgs e)
        {
            Response.Redirect(SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingOrderSummary).Url);
        }

        protected void LinkDelete_OnClick(object sender, EventArgs e)
        {
            int eventId;
            var lnkDelete = (LinkButton)sender;
            var cartKey = Session[SessionVariables.TrainingCartKey] as string;

            if (int.TryParse(lnkDelete.CommandArgument, out eventId))
            {
                BusinessLayer.BLL.Training.DeleteShopCartItems(eventId, cartKey);

                // refresh datasource and grid
                _dt = BusinessLayer.BLL.Training.GetItemsInCart(cartKey, _cultureGroupId);
                GridCart.DataSource = _dt;
                Session[SessionVariables.CartItems] = _dt;
                GridCart.DataBind();
                TextTotal.Text = GetTotal();
            }
            else
                throw new Exception("Invalid Event Id.");
        }

        private string GetTotal()
        {
            double tot = _dt.Rows.Cast<DataRow>().Sum(dr => Convert.ToDouble(dr["Amount"]));
            //hard coded en-us as we are using only $ for currency.
            return tot.ToString("C2", CultureInfo.CreateSpecificCulture("en-US"));
        }

        protected string GetUrl(string id)
        {
            return string.Format("{0}?eventID={1}", SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingRegistration).Url, HttpUtility.UrlEncode(id));
        }
    }
}