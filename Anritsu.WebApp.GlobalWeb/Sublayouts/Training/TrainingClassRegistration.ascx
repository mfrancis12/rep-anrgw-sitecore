﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingClassRegistration.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.TrainingClassRegistration" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<div class="container accordianclr">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.PageTitle) %></h1>
            <p><%=Editable(x=>x.Content) %></p>
            <div class="location">
                <div class="location-title">
                    <asp:HyperLink runat="server" ID="LinkCourse"></asp:HyperLink>
                </div>
                <div class="location-description">
                    <div class="location-description-block">
                        <div class="location-row">
                            <div class="left">
                                <p><%=Translate.TextByDomain("Glossary", "Date") %></p>
                            </div>
                            <div class="right">
                                <p>
                                    <asp:Literal runat="server" ID="LabelDate" />
                                </p>
                            </div>
                        </div>
                        <div class="location-row">
                            <div class="left">
                                <p><%=Translate.TextByDomain("Glossary", "Location") %></p>
                            </div>
                            <div class="right">
                                <p>
                                    <asp:Literal runat="server" ID="LabelLocation" />
                                </p>
                            </div>
                        </div>
                        <div class="location-row">
                            <div class="left">
                                <p><%=Translate.TextByDomain("Training", "SeatsAvailable") %></p>
                            </div>
                            <div class="right">
                                <p>
                                    <asp:Literal runat="server" ID="LabelAvailableSeats" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-container">
                <h2><%=Translate.TextByDomain("Training", "ClassRegisterAttendee") %> </h2>
                <div>
                    <div style="float: left; width: 50%"><strong><%=Translate.TextByDomain("CommonFormStrings", "RequiredField") %> </strong></div>
                    <div style="float: right;">
                        <asp:PlaceHolder runat="server" ID="PlaceholderRelatedLinks">
                                <asp:HyperLink runat="server" ID="LinkLogOn"><%=Translate.TextByDomain("Home", "Login") %></asp:HyperLink>
                                &nbsp;/&nbsp;
                                <asp:HyperLink runat="server" ID="LinkRegister"><%=Translate.TextByDomain("MyAnritsu", "Register") %></asp:HyperLink>
                                &nbsp;<%=Translate.TextByDomain("Training", "AutoFill") %>
                        </asp:PlaceHolder>
                    </div>
                </div>
                <div class="bd" style="clear: both;">
                    <div class="group input-text required">
                        <label><%=Translate.TextByDomain("CommonFormStrings", "FirstName") %> *</label>
                        <p class="error-message"><%=Translate.TextByDomain("ErrorMessage", "ErrMsgBlankFirstName") %></p>
                        <asp:TextBox runat="server" ID="TextFirstName" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="group input-text required">
                        <label><%=Translate.TextByDomain("CommonFormStrings", "Lastname") %> *</label>
                        <p class="error-message"><%=Translate.TextByDomain("ErrorMessage", "ErrMsgBlankLastName") %></p>
                        <asp:TextBox runat="server" ID="TextLastName" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="group input-text">
                        <label><%=Translate.TextByDomain("CommonFormStrings", "Phone") %></label>
                        <asp:TextBox runat="server" ID="TextPhone" MaxLength="15"></asp:TextBox>
                    </div>
                    <div class="group input-text required">
                        <label><%=Translate.TextByDomain("CommonFormStrings", "Email") %> *</label>
                        <p class="error-message"><%=Translate.TextByDomain("ErrorMessage", "ErrMsgCannotBeEmpty") %></p>
                        <p class="custom-error-message"><%=Translate.TextByDomain("ErrorMessage", "ErrMsgInvalidEmail") %></p>
                        <asp:TextBox runat="server" ID="TextEmail" MaxLength="50" CssClass="email"></asp:TextBox>
                    </div>
                    <div class="group input-text">
                        <label><%=Translate.TextByDomain("CommonFormStrings", "ReferredBy") %></label>
                        <asp:TextBox runat="server" ID="TextReferredBy" MaxLength="200"></asp:TextBox>
                    </div>
                    <div class="group">
                        <asp:Button runat="server" ID="AddAttendee" Text='Add' CssClass="button form-submit" OnClick="AddAttendee_OnClick" />
                    </div>
                </div>
                <asp:PlaceHolder runat="server" ID="PlaceholderAttendees" Visible="False">
                    <h2><%=Translate.TextByDomain("Training", "Attendees") %></h2>
                    <div class="bd">
                        <div class="group">
                            <div class="detail-table">
                                <asp:GridView ID="GridAttendees" runat="server" AutoGenerateColumns="false"
                                    Width="100%" EmptyDataText="" OnRowDataBound="GridAttendees_OnRowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="LinkDelete_Click" CssClass="colorBlue">
                                                    <%=Translate.TextByDomain("Glossary", "Delete") %>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate><%=Translate.TextByDomain("CommonFormStrings", "FirstName") %></HeaderTemplate>
                                            <ItemTemplate><%#Eval("FirstName")%></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate><%=Translate.TextByDomain("CommonFormStrings", "Lastname") %></HeaderTemplate>
                                            <ItemTemplate><%#Eval("LastName")%></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate><%=Translate.TextByDomain("CommonFormStrings", "Phone") %></HeaderTemplate>
                                            <ItemTemplate><%#Eval("Phone")%></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate><%=Translate.TextByDomain("CommonFormStrings", "Email") %></HeaderTemplate>
                                            <ItemTemplate><%#Eval("Email")%></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="group input-text">
                            <label><%=Translate.TextByDomain("Training", "Amount") %></label>
                            <asp:TextBox runat="server" ID="TextTotal" MaxLength="10" ReadOnly="True"></asp:TextBox>
                        </div>
                        <div class="group">
                            <asp:Button runat="server" ID="AddToCart" Text="Add To Cart" CssClass="button addCartbtn" OnClick="AddToCart_OnClick" />
                            <asp:Button runat="server" ID="Cancel" Text="Cancel" CssClass="button" OnClick="Cancel_OnClick" />
                        </div>
                    </div>
                </asp:PlaceHolder>
            </div>
        </div>
        <div class="content-aside">
            <sc:placeholder runat="server" key="RightRail" />
        </div>
    </div>
</div>
