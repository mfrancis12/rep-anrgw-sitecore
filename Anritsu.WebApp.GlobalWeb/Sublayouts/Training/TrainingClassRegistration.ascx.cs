﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Configuration;
using Sitecore.Globalization;
using BO = Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{

    public partial class TrainingClassRegistration : GlassUserControl<IStaticText>
    {
        private int _cultureGroupId;
        private List<BO.ShopCartAttendees> _attendees = new List<BO.ShopCartAttendees>();
        private double _cost;
        private int _eventId;
        private int _seats;

        private bool IsFromOrderSummaryPage
        {
            get
            {
                var obj = ViewState["isFromOrderSummaryPage"];
                if (obj is bool)
                    return (bool)obj;
                return false;
            }
            set { ViewState["isFromOrderSummaryPage"] = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            // check if secure connection used
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                // redirect visitor to SSL connection
                Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString[QueryStringVariables.EventId] == null ||
                   !int.TryParse(Request.QueryString[QueryStringVariables.EventId].HtmlDecode(), out _eventId))
            {
                throw new HttpException(404, "File not found.");
            }
            var dtAttendees = new DataTable();
            
            _cultureGroupId = BusinessLayer.BLL.Common.GetCultureGroupIdForCultureCode(Sitecore.Context.Language.Name);
            var dt = BusinessLayer.BLL.Training.GetClassEventDetailsById(_cultureGroupId, _eventId);
            if (dt.Rows.Count > 0)
            {
                if (Sitecore.Data.ID.IsID(dt.Rows[0]["ClassId"].ToString()))
                {
                    var courseItem = SitecoreContext.GetItem<IModelBase>(dt.Rows[0]["ClassId"].ToString());
                    if (courseItem != null)
                    {
                        LinkCourse.NavigateUrl =courseItem.Url;
                    }
                }
                LinkCourse.Text = dt.Rows[0]["ClassName"].ToString();
               
                var dateFrom = DateTime.Parse(dt.Rows[0]["DateFrom"].ToString());
                var dateTo = DateTime.Parse(dt.Rows[0]["DateTo"].ToString());
                LabelDate.Text = string.Format("{0} - {1}",dateFrom.ToFormattedDate(), dateTo.ToFormattedDate());
                //str.Append(dateTo.ToString(Constants.DateFormat));
                LabelLocation.Text = dt.Rows[0]["Location"].ToString();
                LabelAvailableSeats.Text = dt.Rows[0]["AvailableSeats"].ToString();
                _seats = (int)dt.Rows[0]["AvailableSeats"];
                _cost = Convert.ToDouble(dt.Rows[0]["price"]);

                if (!IsPostBack)
                {
                    AddAttendee.Text = Translate.TextByDomain("Glossary", "AddButton");
                    AddToCart.Text = Translate.TextByDomain("Training", "AddtoCart");
                    Cancel.Text = Translate.TextByDomain("CommonFormStrings", "Cancel");
                    if (Request.UrlReferrer != null)
                    {
                        if (
                            Request.UrlReferrer.AbsolutePath.IndexOf("training-cart",
                                StringComparison.Ordinal) > 0)
                        {
                            AddToCart.Text = Translate.TextByDomain("Training", "Update");
                        }
                        else if (
                            Request.UrlReferrer.AbsolutePath.IndexOf("order-summary",
                                StringComparison.Ordinal) > 0)
                            IsFromOrderSummaryPage = true;
                    }

                    if (LoggedInUser != null)
                    {
                        TextFirstName.Text = LoggedInUser.FirstName;
                        TextLastName.Text = LoggedInUser.LastName;
                        var phoneNumberNExt = LoggedInUser.PhoneNumber.Split('|');
                        TextPhone.Text = phoneNumberNExt.GetValue(0).ToString();
                        TextEmail.Text = LoggedInUser.EmailAddress;
                        PlaceholderRelatedLinks.Visible = false;
                    }

                    var cartKey = Session[SessionVariables.TrainingCartKey] as string;
                    if (!string.IsNullOrEmpty(cartKey))
                    {
                        dtAttendees = BusinessLayer.BLL.Training.GetClassAttendeesInCart(cartKey, _eventId);
                        var cartDt = BusinessLayer.BLL.Training.GetCart(cartKey);

                        if (cartDt != null && cartDt.Rows.Count > 0)
                            TextReferredBy.Text = cartDt.Rows[0]["ReferredBy"] as string;
                    }

                    _attendees = (from DataRow dr in dtAttendees.Rows
                                 select new BO.ShopCartAttendees
                                 {
                                     AttendeeId = int.Parse(dr["AttendeeId"].ToString()),
                                     FirstName = dr["FirstName"].ToString(),
                                     LastName = dr["LastName"].ToString(),
                                     Phone = dr["PhoneNumber"].ToString(),
                                     Email = dr["EmailID"].ToString()
                                 }).ToList();

                    GridAttendees.DataSource = _attendees;
                    GridAttendees.DataBind();
                    LabelAvailableSeats.Text = (_seats - _attendees.Count).ToString(CultureInfo.InvariantCulture);
                    TextTotal.Text = (_cost * _attendees.Count).ToString("C2", CultureInfo.CreateSpecificCulture("en-US"));
                    if (_seats == _attendees.Count)
                        AddAttendee.Enabled = false;
                    Session[SessionVariables.TrainingAttendees] = _attendees;

                    LinkLogOn.NavigateUrl = string.Format("/{0}/Sign-In.aspx?ReturnURL={1}"
                , BasePage.GetCurrentLanguage(), HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));

                    LinkRegister.NavigateUrl = string.Format("{0}/account/new-user-registration?wreply={1}&lang={2}&mkt={2}&sptkn={3}", Settings.GetSetting("AnrSso.GWIdp.SSOHost"), HttpUtility.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri), BasePage.GetCurrentLanguage(), Settings.GetSetting("AnrSso.GWSp.SpTokenID"));
                }
                if (Session[SessionVariables.TrainingAttendees] != null)
                {
                    _attendees = (List<BO.ShopCartAttendees>)Session[SessionVariables.TrainingAttendees];
                }
            }
            else
            {
                throw new HttpException(404, "File not found.");
            }
        }

        protected void AddToCart_OnClick(object sender, EventArgs e)
        {
            var userId = 0;
            var deleted = new HashSet<int>();
            if (Session[SessionVariables.AttendeesDeleted] != null)
                deleted = (HashSet<int>)Session[SessionVariables.AttendeesDeleted];

            if (LoggedInUser != null)
            {
                userId = LoggedInUser.UserId;
            }
            foreach (int i in deleted)
            {
                BusinessLayer.BLL.Training.DeleteShopCartAttendee(i);
            }

            var cartKey = Session[SessionVariables.TrainingCartKey] as string;

            if (string.IsNullOrEmpty(cartKey))
            {
                cartKey = Guid.NewGuid().ToString();
                Session[SessionVariables.TrainingCartKey] = cartKey;
            }

            foreach (var attendee in _attendees.Where(attendee => attendee.AttendeeId == 0))
            {
                attendee.UserId = userId;
                attendee.EventId = _eventId;
                attendee.CartKey = cartKey;
                BusinessLayer.BLL.Training.InsertClassAttendeesToShopCart(attendee);
            }

            // save the referredby value
            BusinessLayer.BLL.Training.UpdateCartDetails(userId, cartKey, TextReferredBy.Text);
            Response.Redirect(SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingCart).Url);
        }

        protected void AddAttendee_OnClick(object sender, EventArgs e)
        {
            var attendee = new BO.ShopCartAttendees
            {
                AttendeeId = 0,
                FirstName = HttpUtility.HtmlEncode(TextFirstName.Text),
                LastName = HttpUtility.HtmlEncode(TextLastName.Text),
                Phone = HttpUtility.HtmlEncode(TextPhone.Text),
                Email = HttpUtility.HtmlEncode(TextEmail.Text)
            };
            _attendees.Add(attendee);
            Session[SessionVariables.TrainingAttendees] = _attendees;
            GridAttendees.DataSource = _attendees;
            GridAttendees.DataBind();
            
            LabelAvailableSeats.Text = (_seats - _attendees.Count).ToString(CultureInfo.InvariantCulture);
            //hard coded culture en-us as we are using only $ for currency
            TextTotal.Text = (_cost * _attendees.Count).ToString("C2", CultureInfo.CreateSpecificCulture("en-US"));
            if (_seats == _attendees.Count)
                AddAttendee.Enabled = false;

            PlaceholderAttendees.Visible = true;
            ClearForm();
        }

        protected void GridAttendees_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
                PlaceholderAttendees.Visible = GridAttendees.Rows.Count > 0;
        }
        
        protected void LinkDelete_Click(object sender, EventArgs e)
        {
            var lnkDelete = (LinkButton)sender;
            int dataItemIndex;

            if (!int.TryParse(lnkDelete.CommandArgument, out dataItemIndex)) return;
            var deleted = new HashSet<int>();

            BO.ShopCartAttendees attendeeToDelete = null;
            if (dataItemIndex < _attendees.Count)
                attendeeToDelete = _attendees[dataItemIndex];

            if (attendeeToDelete != null)
            {
                deleted.Add(attendeeToDelete.AttendeeId);
                _attendees.Remove(attendeeToDelete);
            }

            Session[SessionVariables.AttendeesDeleted] = deleted;
            Session[SessionVariables.TrainingAttendees] = _attendees;
            GridAttendees.DataSource = _attendees;
            GridAttendees.DataBind();
            AddAttendee.Enabled = true;
            PlaceholderAttendees.Visible = _attendees.Any();
            LabelAvailableSeats.Text = (_seats - _attendees.Count).ToString();
            //hard coded culture en-us as we are using only $ for currency
            TextTotal.Text = (_cost * _attendees.Count).ToString("C2", CultureInfo.CreateSpecificCulture("en-US"));
        }

        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            Session[SessionVariables.TrainingAttendees] = null;

            Response.Redirect(IsFromOrderSummaryPage
                ? SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingCart).Url
                : HttpContext.Current.Request.Url.AbsoluteUri);
        }

        private static GWSsoUserType LoggedInUser
        {
            get
            {
                var usr = HttpContext.Current.Session[SessionVariables.UserLoggedIn]
                    as GWSsoUserType;

                if (usr == null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    usr = RefreshUserSession();
                }
                return usr;
            }
        }

        public static GWSsoUserType RefreshUserSession()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;

            string redirectLoginUrl = string.Format("/{0}/Sign-In.aspx?ReturnURL={1}"
                , BasePage.GetCurrentLanguage(), HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
            HttpContext.Current.Response.Redirect(redirectLoginUrl);

            return null;
        }

        private void ClearForm()
        {
            TextFirstName.Text = string.Empty;
            TextLastName.Text = string.Empty;
            TextPhone.Text = string.Empty;
            TextEmail.Text = string.Empty;
        }

    }
}