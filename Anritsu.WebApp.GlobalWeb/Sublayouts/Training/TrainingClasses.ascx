﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingClasses.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.TrainingClasses" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<asp:Repeater ID="RepeaterClasses" runat="server" OnItemDataBound="RepeaterClasses_OnItemDataBound">
    <ItemTemplate>
        <div class="location">
            <div class="location-title"><%#GetScheduleDate(Eval("DateFrom").ToString(),Eval("DateTo").ToString()) %></div>
            <div class="location-description">
                <div class="location-description-block">
                    <div class="location-row">
                        <div class="left">
                            <p><%=Translate.TextByDomain("Glossary", "Location") %></p>
                        </div>
                        <div class="right">
                            <p>
                                <%#Eval("Location") %>
                            </p>
                        </div>
                    </div>
                    <div class="location-row">
                        <div class="left">
                            <p><%=Translate.TextByDomain("Training", "SeatsAvailable") %></p>
                        </div>
                        <div class="right">
                            <p><%#Eval("AvailableSeats") %></p>
                        </div>
                    </div>
                </div>
                <hr class="hr-grey">
                <div class="location-description-block">
                    <div class="location-row">
                        <div class="left">
                            <p><%=Translate.TextByDomain("Training", "PricePerStudent") %></p>
                        </div>
                        <div class="right">
                            <p class="money">$<%# DataBinder.Eval(Container.DataItem, "Price","{0:0.00}") %></p>
                            <p><asp:Hyperlink runat="server" ID="LinkRegistration" CssClass="button"><%=Translate.TextByDomain("Glossary", "RegisterNow") %></asp:Hyperlink></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>
