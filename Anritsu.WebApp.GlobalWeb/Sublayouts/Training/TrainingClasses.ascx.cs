﻿using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Constants;
using Glass.Mapper.Sc.Web.Ui;
using System;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using BLL = Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{
    public partial class TrainingClasses : GlassUserControl<IStaticText>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            int cultureGroupId = BLL.Common.GetCultureGroupIdForCultureCode(Sitecore.Context.Language.Name);
            RepeaterClasses.DataSource = BLL.Training.GetClassScheduleDetails(cultureGroupId, Model.Id.ToString("B").ToUpper(CultureInfo.InvariantCulture)); 
            RepeaterClasses.DataBind();
        }

        protected string GetScheduleDate(string fromDate, string toDate)
        {
            var dateFrom = DateTime.Parse(fromDate);
            var dateTo = DateTime.Parse(toDate);
            var str = string.Format("{0} - {1}", dateFrom.ToFormattedDate(), dateTo.ToFormattedDate());
            return str;
        }

        protected void RepeaterClasses_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var currentRow = (DataRowView)e.Item.DataItem;
                if (currentRow["AvailableSeats"] != null)
                {
                    ((HyperLink)e.Item.FindControl("LinkRegistration")).Visible = (int)currentRow["AvailableSeats"] > 0;
                    if (string.IsNullOrEmpty(currentRow["DistributorId"].ToString()))
                    {
                        ((HyperLink)e.Item.FindControl("LinkRegistration")).NavigateUrl = string.Format("{0}?eventID={1}", SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingRegistration).Url, HttpUtility.UrlEncode(currentRow["EventID"].ToString()));
                    }
                    else
                    {
                        ((HyperLink)e.Item.FindControl("LinkRegistration")).NavigateUrl = string.Format("{0}?eventID={1}", SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingDistributor).Url, HttpUtility.UrlEncode(currentRow["EventID"].ToString()));
                    }
                }
            }
        }
    }
}