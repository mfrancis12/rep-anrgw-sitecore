﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingCourseList.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.TrainingCourseList" %>
<div class="container">
    <div class="landing-tech">
        <asp:Repeater ID="RepeaterCourses" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.Training.ITrainingCourse">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <h2><%#Item.CourseName%></h2>
                        <p><%#Editable(Item,x=>x.CourseShortDescription) %></p>
                        <p>
                            <a href="<%#Item.Url %>"><%#Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ReadMore")%>
                                <i class="icon icon-arrow-blue"></i>
                            </a>
                        </p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
