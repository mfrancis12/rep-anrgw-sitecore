﻿using System;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages.Training;
using Glass.Mapper.Sc.Web.Ui;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{
    public partial class TrainingCourseList : GlassUserControl<ITrainingCategory>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            RepeaterCourses.DataSource = Model.GetChildren<ITrainingCourse>();
            RepeaterCourses.DataBind();
        }
    }
}