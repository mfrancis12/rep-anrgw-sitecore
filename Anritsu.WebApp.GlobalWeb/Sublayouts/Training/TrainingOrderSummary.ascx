﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingOrderSummary.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.TrainingOrderSummary" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<div class="container accordianclr">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.PageTitle) %></h1>
            <p><%=Editable(x=>x.Content) %></p>
            <asp:Repeater runat="server" ID="RepeaterAttendees" OnItemDataBound="RepeaterAttendees_OnItemDataBound">
                <ItemTemplate>
                    <div class="location">
                        <div class="location-title"><%=Translate.TextByDomain("Training", "Attendees") %></div>
                        <div class="location-description">
                            <div class="location-description-block">
                                <div class="location-row">
                                    <div class="left">
                                        <p> <%# DataBinder.Eval(Container.DataItem, "FirstName") %> &nbsp; <%# DataBinder.Eval(Container.DataItem, "LastName") %>
                                   &nbsp;&nbsp;
                                    <asp:HyperLink ID="lnkEdit" runat="server"><%=Translate.TextByDomain("Glossary", "Edit") %></asp:HyperLink></p>
                                    </div>
                                    <div class="right">
                                        <p>
                                        </p>
                                    </div>
                                </div>
                            <hr class="hr-grey">
                                <div class="location-row">
                                    <div class="left">
                                        <p><%=Translate.TextByDomain("CommonFormStrings", "Email") %></p>
                                    </div>
                                    <div class="right">
                                        <p><%# DataBinder.Eval(Container.DataItem, "EmailId") %></p>
                                    </div>
                                </div>
                                <div class="location-row">
                                    <div class="left">
                                        <p><%=Translate.TextByDomain("Training", "Course") %></p>
                                    </div>
                                    <div class="right">
                                        <p><asp:Label ID="lblCourseName" runat="server" /></p>
                                    </div>
                                </div>
                                <div class="location-row">
                                    <div class="left">
                                        <p><%=Translate.TextByDomain("Glossary", "Date") %></p>
                                    </div>
                                    <div class="right">
                                        <p><asp:Label ID="lblCourseDate" runat="server" /></p>
                                    </div>
                                </div>
                                <div class="location-row">
                                    <div class="left">
                                        <p><%=Translate.TextByDomain("CommonFormStrings", "Phone") %></p>
                                    </div>
                                    <div class="right">
                                        <p><%# DataBinder.Eval(Container.DataItem, "PhoneNumber") %></p>
                                    </div>
                                </div>
                                <div class="location-row">
                                    <div class="left">
                                        <p><%=Translate.TextByDomain("Training", "PartNumber") %></p>
                                    </div>
                                    <div class="right">
                                        <p><asp:Label ID="lblPartNumber" runat="server" /></p>
                                    </div>
                                </div>
                                
                                <div class="location-row">
                                    <div class="left">
                                        <p><%=Translate.TextByDomain("Glossary", "Location") %></p>
                                    </div>
                                    <div class="right">
                                        <p><asp:Label ID="lblLocation" runat="server" /></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="form-container">
                <div class="group input-text">
                    <label><%=Translate.TextByDomain("Training", "Amount") %></label>
                    <asp:TextBox runat="server" ID="TextTotal" MaxLength="10" ReadOnly="True"></asp:TextBox>
                </div>
                <div class="group">
                    <asp:Button runat="server" ID="Checkout" Text="CheckOut" CssClass="button" OnClick="Checkout_OnClick" />
                </div>
                <div class="group">
                    <asp:Label runat="server" ID="LabelMessage"></asp:Label>
                 </div>
            </div>
        </div>
        <div class="content-aside">
        </div>
    </div>
</div>
