﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Anritsu.Library.CheckoutSDK;
using Anritsu.Library.CheckoutSDK.ARCKRefPaymentManagementAPI;
using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Utilities;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Globalization;
using BO = Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{
    public partial class TrainingOrderSummary : GlassUserControl<IStaticText>
    {
        private int _cultureGroupId;
        protected void Page_Init(object sender, EventArgs e)
        {
            //DisableDownloadCache();
            var cartKey = Session[SessionVariables.TrainingCartKey] as string;
            if (String.IsNullOrEmpty(cartKey) || Session[SessionVariables.CartItems] == null)
            {
                Response.Redirect(SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingCart).Url);
                Response.End();
            }
            var cartKeyGuid = new Guid(cartKey);
            if (cartKeyGuid == Guid.Empty)
            {
                Response.Redirect(SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingCart).Url);
                Response.End();
            }
            var tacs = Session[SessionVariables.TrainingBackPageTrack] as TrainingAfterCheckoutSession;
            if (tacs != null && tacs.TrainingTransactionKey == cartKeyGuid && tacs.IsProcessed)
            {
                Session[SessionVariables.TrainingBackPageTrack] = null;
                Session[SessionVariables.TrainingCartKey] = null;
                Response.Redirect(SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingCart).Url);
                Response.End();
            }

            //if (!Common.isTeamsite)
            //{
            //    RequireSSL = true;
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var cartKey = Session[SessionVariables.TrainingCartKey] as string;
            if (String.IsNullOrEmpty(cartKey))
            {
                return;
            }

            _cultureGroupId = BusinessLayer.BLL.Common.GetCultureGroupIdForCultureCode(Sitecore.Context.Language.Name);
            var cartItems = BusinessLayer.BLL.Training.GetItemsInCart(cartKey, _cultureGroupId);
            var eventIDsInCart = new List<int>(1); // expected only 1 event in cart
            var cartAttendees = new DataTable();

            foreach (
                var tempEventId in
                    cartItems.Rows.Cast<DataRow>()
                        .Select(cartRow => (int)cartRow["eventID"])
                        .Where(tempEventId => !eventIDsInCart.Contains(tempEventId)))
            {
                eventIDsInCart.Add(tempEventId);
            }

            foreach (var eventId in eventIDsInCart)
            {
                var temp = BusinessLayer.BLL.Training.GetClassAttendeesInCart(cartKey, eventId);
                temp.Columns.Add("EventID", typeof(int));
                for (var i = 0; i < temp.Rows.Count; i++)
                {
                    temp.Rows[i]["EventID"] = eventId;
                }
                cartAttendees.Merge(temp);
            }

            // Attendee related information
            RepeaterAttendees.DataSource = cartAttendees;
            RepeaterAttendees.DataBind();

            TextTotal.Text = GetTotal(ref cartItems);
            Checkout.Text = Translate.TextByDomain("Glossary", "Checkout");

            if (Request.QueryString[QueryStringVariables.Mode] != null &&
                Request.QueryString[QueryStringVariables.Mode] == "checkout")
                Checkout_OnClick(this, null);
        }

        private string GetTotal(ref DataTable cartItems)
        {
            var tot = cartItems.Rows.Cast<DataRow>().Sum(dr => Convert.ToDouble(dr["Amount"]));
            //hard coded en-us as we are using only $ for currency.
            return tot.ToString("C2", CultureInfo.CreateSpecificCulture("en-US"));
        }

        protected void Checkout_OnClick(object sender, EventArgs e)
        {
            var cartKey = Session[SessionVariables.TrainingCartKey] as string;
            if (String.IsNullOrEmpty(cartKey) || Session[SessionVariables.CartItems] == null)
            {
                Response.Redirect(SitecoreContext.GetItem<IModelBase>(ItemIds.TrainingCart).Url);
                Response.End();
            }


            if (LoggedInUser == null)
            {
                var returnUrl = string.Format("{0}?{1}=checkout",
                    Request.Url,
                    QueryStringVariables.Mode);

                Response.Redirect(string.Format("/Sign-In.aspx?ReturnUrl={0}",
                    HttpUtility.UrlEncode(returnUrl)));
            }
            else
            {
                var logedInUser = LoggedInUser;
                BusinessLayer.BLL.Training.UpdateCartDetails(logedInUser.UserId, cartKey);
                var tb = (DataTable)Session[SessionVariables.CartItems];
                if (tb == null || tb.Rows.Count < 1)
                    return;

                var eventIDsInCart = new List<int>(1);
                var items = new List<CartItemType>();
                foreach (DataRow row in tb.Rows)
                {
                    var it = new CartItemType
                    {
                        ClientReference = row["eventID"].ToString(),
                        ShortDescription = row["ClassName"].ToString(),
                        PricePerItem = Single.Parse(row["Price"].ToString()),
                        Quantity = int.Parse(row["Attendees"].ToString())
                    };
                    items.Add(it);

                    if (eventIDsInCart.Contains((int)row["eventID"])) continue;
                    eventIDsInCart.Add((int)row["eventID"]);

                    var availableSeatsDr = BusinessLayer.BLL.Training.GetClassEventAvailableSeats(_cultureGroupId,
                        (int)row["eventID"]);
                    if (availableSeatsDr == null) continue;
                    var availableSeats = (int)availableSeatsDr["AvailableSeats"];
                    if (availableSeats >= it.Quantity) continue;
                    LabelMessage.Text =
                        string.Format(
                            "We are sorry but this class no longer has the availability that you require.  There are {0} seat(s) left for this particular event: {1}",
                            availableSeats, it.ShortDescription);
                    return;
                }


                try
                {
                    //string returnURL = string.Format("/{0}/Services-Support/Training-Education/ConfirmationMail.aspx", GetCurrentCulture());
                    var returnUrl = string.Format("https://{0}/{1}", Request.Url.Host, SitecoreContext.GetItem<IModelBase>(ItemIds.ReviewOrder).Url);
                    ; //string.Format("/{0}/test-measurement/support/training-and-education/review-order",
                    //BasePage.GetCurrentLanguage());
                    var checkoutClient = new CheckoutClient();
                    var newCart = new CheckoutCartType
                    {
                        GWUserId = logedInUser.UserId,
                        ItemType = ItemTypeCodeType.TRAINING,
                        ReturnURL = returnUrl,
                        ExpiredUtcDate = DateTime.Now.AddDays(1),
                        ItemCollection = items.ToArray(),
                        IsProcessed = false,
                        TaxRatePolicyKey = new Guid("EDFEAB0B-FA2E-430B-AE06-8B0CDB18270B"),
                        PreShSpecified1 = false,
                        ClientTranReference = Session[SessionVariables.TrainingCartKey].ToString()
                    };
                    var resp = checkoutClient.MakeAddNewCartCall(newCart, returnUrl);

                    if (resp.CheckoutCart == null)
                        throw new ArgumentException("Invalid cart token");
                    Response.Redirect(resp.RedirectURL);

                }
                catch (ArgumentException ex)
                {
                    LabelMessage.Text = string.Format("{0}", ex.Message);
                }

            }
        }

        protected void RepeaterAttendees_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var lblCourseName = (Label)e.Item.FindControl("lblCourseName");
            var lblPartNumber = (Label)e.Item.FindControl("lblPartNumber");
            var lblCourseDate = (Label)e.Item.FindControl("lblCourseDate");
            var lblLocation = (Label)e.Item.FindControl("lblLocation");
            var lnkEdit = (HyperLink)e.Item.FindControl("lnkEdit");

            var drv = (DataRowView)e.Item.DataItem;
            var eventId = (int)drv["eventID"];
            lnkEdit.NavigateUrl = string.Format(ConfigurationManager.AppSettings["TrainingRegistrationEditPage"],
                BasePage.GetCurrentLanguage(), eventId);

            var eventDetails = BusinessLayer.BLL.Training.GetClassEventDetailsById(_cultureGroupId, eventId);
            if (eventDetails != null && eventDetails.Rows.Count == 1)
            {
                lblCourseName.Text = eventDetails.Rows[0]["ClassName"] as string;
                lblPartNumber.Text = eventDetails.Rows[0]["PartNumber"] as string;
                lblCourseDate.Text = ((DateTime)eventDetails.Rows[0]["DateFrom"]).ToFormattedDate() + @" - " +
                                     ((DateTime)eventDetails.Rows[0]["DateTo"]).ToFormattedDate();
                lblLocation.Text = eventDetails.Rows[0]["Location"] as string;
            }
            else
                throw new Exception("Cannot locate event details.");
        }

        private static GWSsoUserType LoggedInUser
        {
            get
            {
                var usr = HttpContext.Current.Session[SessionVariables.UserLoggedIn]
                    as GWSsoUserType;

                if (usr == null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    usr = RefreshUserSession();
                }
                return usr;
            }
        }
        public static GWSsoUserType RefreshUserSession()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;

            string redirectLoginUrl = string.Format("/Sign-In.aspx?ReturnURL={0}", HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
            HttpContext.Current.Response.Redirect(redirectLoginUrl);

            return null;
        }
    }
}