﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingTypes.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.Training.TrainingTypes" %>
<div class="full-container container">
    <div class="products-heading">
        <div class="products-title">
            <h1><%=Editable(x=>x.PageTitle) %></h1>
        </div>
        <div class="products-description"><%=Editable(x=>x.Content) %></div>
    </div>
</div>
<div class="full-container">
    <div class="products-tech">
        <asp:Repeater runat="server" ID="RepeaterTrainingTypes" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.Training.ITrainingType">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <h3 class="title"><%#Editable(Item, x=>x.Title) %></h3>
                        <div class="content">
                            <div class="text">
                                <div class="img">
                                    <%#RenderImage(Item,x=>x.Image,new {@class="img100"} ) %>
                                </div>
                                <div>
                                    <p><%#Editable(Item, x=>x.Description)%></p>
                                </div>
                                </div>
                            <div class="device">
                                <asp:Repeater ID="RepeaterLinks" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.INavigation" DataSource="<%#GetLinks(Item) %>">
                                    <ItemTemplate>
                                        <a href="<%#Item.Url %>"><%#Item.MenuTitle %></a>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
