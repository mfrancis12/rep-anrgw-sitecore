﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Pages.Training;
using Glass.Mapper.Sc.Web.Ui;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{
    public partial class TrainingTypes : GlassUserControl<IStaticText>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            RepeaterTrainingTypes.DataSource = Model.GetChildren<ITrainingType>();
            RepeaterTrainingTypes.DataBind();
        }

        protected IEnumerable<INavigation> GetLinks(ITrainingType item)
        {
            var links = item.Links.Where(x => x.TemplateId.ToString().Equals(TemplateIds.TrainingCategory) || x.TemplateId.ToString().Equals(TemplateIds.TrainingCourse));
            return links;
        }
    }
}