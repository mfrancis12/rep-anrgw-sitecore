﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwoColumn50X50.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.TwoColumn50X50" %>
<div class="container">
    <div class="site-guide">
        <div class="row">
            <div class="item col50">
                <sc:placeholder id="DoubleColumn1" key="DoubleColumn1" runat="server" />
            </div>
            <div class="item col50">
                <sc:placeholder id="DoubleColumn2" key="DoubleColumn2" runat="server" />
            </div>
        </div>
    </div>
</div>
