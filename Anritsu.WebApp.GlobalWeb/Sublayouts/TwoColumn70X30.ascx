﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwoColumn70X30.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.TwoColumn70X30" %>
<div class="container nogap-mobile">
    <div class="site-guide">
        <div class="row">
            <div class="item col2">
                <sc:placeholder id="TwoColumn1" key="TwoColumn1" runat="server" />
            </div>

            <div class="item col1">
                <sc:placeholder id="TwoColumn2" key="TwoColumn2" runat="server" />
            </div>

        </div>
    </div>
</div>
