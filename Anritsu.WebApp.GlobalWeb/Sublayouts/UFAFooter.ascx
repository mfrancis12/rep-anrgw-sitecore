﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UFAFooter.ascx.cs" 
    Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.UFAFooter" %>

 <script type="text/javascript">
        $(document).ready(function () {
            var maxHt = 0;
            $(".divUniversalForm div").each(function () {
                if (maxHt < $(this).height()) {
                    maxHt = $(this).height();
                }
            });

            $(".divUniversalForm").css({ "height": maxHt + 50, "overflow-y": "auto" });
			$(".divUniversalForm > div:first-child").css({top: "85px"});
            $(".contentsarea").css("min-height", maxHt + 60);
        });
 </script>