﻿using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.Library.SSOSDK.SSOApiClient;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class VideoDetail : GlassUserControl<IVideo>
    {
       
        #region Global Variables
        Database db = Sitecore.Context.Database;
        string indexName = "anritsu_brightcovevideotemplates";
        //public Item videoItem;
        #endregion

        private void Page_Load(object sender, EventArgs e)
        {
            if (Model.LoginRequired == true)
            {
                var userLoggedIn = HttpContext.Current.Session[Constants.SSOUtilities.SessionVariables.UserLoggedIn]
              as GWSsoUserType;
                if (userLoggedIn == null)
                {
                    RefreshUserSession();
                }

                
                if (Context.User.Identity.IsAuthenticated && userLoggedIn != null && Sitecore.Context.PageMode.IsNormal)
                {
                    LogVideoAccessInfo(userLoggedIn);
                }
            }

            RelatedVideos.DataSource = Model.RelatedVideos;
            RelatedVideos.DataBind();

            BindRelatedLinks();
            BrightcoveMediaControl.DataSource = !string.IsNullOrEmpty(Model.Video) ? VideoItem(Model.Video).Id.ToString() : string.Empty;

            if (GsaHelpers.IsGsaCrawler())
                AddGsaMetaInfo();
        }
        public static void RefreshUserSession()
        {
            var redirectLoginUrl = string.Format("/Sign-In.aspx?ReturnURL={0}", HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
            HttpContext.Current.Response.Redirect(redirectLoginUrl);
        }

        private void LogVideoAccessInfo(GWSsoUserType userLoggedIn)
        {
            //log webinar request with user info
            var videoAccessInfo = GetVideoAccessLogInfo(userLoggedIn);
            BllWebinar.VideoAccess_AddLog(videoAccessInfo);
        }

        private VideoAccessLog GetVideoAccessLogInfo(GWSsoUserType userLoggedIn)
        {
            var videoAccessLog = new VideoAccessLog
            {
                
                VideoTitle = Model.Name,
                CultureGroupId = BasePage.GetCultureGroupId(Sitecore.Context.Language.Name),
                UserID = userLoggedIn.UserId,
                ClientIP = Request.UserHostAddress,
                ViewDateOnUTC = DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss")
            };

            return videoAccessLog;
        }
        private void AddGsaMetaInfo()
        {
            try
            {
                var ltMetaTags = (PlaceHolder)Parent.FindControl("GsaMetaPlaceHolder");
                if (ltMetaTags == null) return;
                var meta = new HtmlMeta
                {
                    Name = "videoduration",
                    Content = !string.IsNullOrEmpty(VideoItem(Model.Video).Duration) ? GetDuration(VideoItem(Model.Video).Duration) : ""
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "metareleasedate",
                    Content = !string.IsNullOrEmpty(VideoItem(Model.Video).LastModifiedDate) ? Convert.ToDateTime(VideoItem(Model.Video).LastModifiedDate).ToLocalTime().Date.ToFormattedDate() : ""
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message + " " + ex.StackTrace, "GsaMetaInfoAddition");
            }
        }


        private void BindRelatedLinks()
        {
            Item currentItem = SitecoreContext.GetCurrentItem<Item>();
            if (currentItem["RelatedLinks"] != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(currentItem, "RelatedLinks");
                RelatedLinks.DataSource = relatedGeneralLinks.LinkItems;
                RelatedLinks.DataBind();
            }
        }

        public string GetDuration(string videoLength)
        {
            string duration = string.Empty;
            if (!string.IsNullOrEmpty(videoLength))
            {
                TimeSpan time = TimeSpan.FromMilliseconds(Convert.ToInt32(videoLength));
                duration = time.ToString(@"hh\:mm\:ss");
            }
            return duration;
        }

        private string BindBrightcoveMedia()
        {
            var videoId = string.Empty;
            var selectedVideo = SitecoreContext.GetCurrentItem<Item>().Fields["Video"].Value;
            Item mediaItem = db.GetItem(selectedVideo, LanguageManager.GetLanguage("en"));
            if (mediaItem != null)
            {
                videoId = mediaItem.ID.ToString();
            }
            return videoId;
        }
        public IBrightcoveVideo VideoItem(string videoId)
        {
            if (videoId != null)
            {
                var contextService = new SitecoreContext();
                return contextService.GetItem<IBrightcoveVideo>(videoId, LanguageManager.GetLanguage("en"));
            }
            return null;
        }

        protected void RelatedLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("relatedLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }
    }
}