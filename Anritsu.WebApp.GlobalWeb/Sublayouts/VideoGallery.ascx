﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoGallery.ascx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.VideoGallery" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<asp:ScriptManager ID="videoScriptManager" runat="server"></asp:ScriptManager>
<div class="full-container video-gallery-title nogap">
    <div class="container nogap">
        <div class="hd-left">
            <h1><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "videogallery") %></h1>
        </div>
        <div class="hd-right"></div>
    </div>
</div>

<div class="full-container video-gallery-body">
    <div class="container nogap">
        <div class="video-filter-left">
            <div class="result-filter">
                <div class="hd"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "categoryfilter") %></div>
                <ul class="bd globalfacet">
                    <li class="level1 on">
                        <div class="level1-item">
                            <span><input type="checkbox" class="dispNone"/><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "products") %></span>
                            <div class="icon icon-arrow-up-grey"></div>
                        </div>
                        <ul class="level2_ul" id="checkbox-accordion">
                            <asp:Repeater ID="rptCategory" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                <ItemTemplate>
                                    <li class="level2">
                                        <a href="javascript:void(0)">
                                            <asp:CheckBox ID="chkCategory" runat="server" /><asp:HiddenField ID="hiddenCategory" runat="server" Value="<%#Item.Value%>" />
                                            <span class="level2hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span></a>
                                        <span class="icon icon-plus-grey iconStyles"></span>
                                        <ul class="paddingLeft level3_ul">
                                            <asp:Repeater ID="rptSubCategory" runat="server" DataSource='<%# GetSubcategoryFacets(Item) %>' ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                                <ItemTemplate>
                                                    <li class="level3">
                                                        <asp:CheckBox ID="chkSubCategory" runat="server" /><asp:HiddenField ID="hiddenSubCategory" runat="server" Value="<%#Item.Value%>" />
                                                        <span class="level2hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span>
                                                        <span class="icon icon-plus-grey iconStyles"></span>
                                                        <ul class="level4_ul paddingLeft">
                                                            <asp:Repeater ID="rptFamily" runat="server" DataSource='<%# GetFamilyFacets(Item) %>' ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                                                <ItemTemplate>
                                                                    <li class="level4">
                                                                        <asp:CheckBox ID="chkFamily" runat="server" /><asp:HiddenField ID="hiddenfamily" runat="server" Value="<%#Item.Value%>" />
                                                                        <span class="level3hd"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</span>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </li>
                </ul>
            </div>
           
            <div class="clearall align-right">
                <asp:LinkButton ID="clearAll" CssClass="clear-all" runat="server" ClientIDMode="Static" OnClick="ClearAll_OnClick"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "clearall") %></asp:LinkButton>
            </div>

        </div>
        <div class="latest-video-right">
            <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "latestvideos") %></h3>
            <asp:UpdatePanel ID="VideoUpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:Button ID="SubmitVideoGalleryFacets" runat="server" class="searchResBtn" OnClick="SubmitVideoGalleryFacets_Click" Style="display: none;" ClientIDMode="Static" />
                    <asp:ListView ID="videos" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.SearchItem">
                        <ItemTemplate>
                            <div class="item">
                                <div class="left">
                                    <div class="img">
                                        <a href="<%#Item.Url%>">
                                            <%--<%#RenderImage(Item,x=>x.Thumbnail,isEditable:true) %>--%>
                                            <img src="<%#Item.VideoItem.ThumbnailURL%>" alt="">
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="related">
                                        <p class="duration">
                                            <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "duration") %>: <%#GetDuration(Editable(Item,x=>x.VideoItem.Duration))%><br>
                                            <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "releasedate") %>: <%#!string.IsNullOrEmpty(Editable(Item,x=>x.VideoItem.LastModifiedDate))?Editable(Item,x=>Convert.ToDateTime(x.VideoItem.LastModifiedDate).ToLocalTime().ToFormattedDate()):"" %>
                                        </p>
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="content">
                                        <h3><%#Editable(Item,x=>x.Name) %></h3>
                                        <p><%#Editable(Item,x=>x.ShortDescription) %></p>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                    <div class="pagination">
                        <span class="page">
                            <span class="info"><%= GetPaginationName() %></span>
                            <asp:PlaceHolder ID="placeHolderPager" runat="server"></asp:PlaceHolder>
                        </span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
</div>
