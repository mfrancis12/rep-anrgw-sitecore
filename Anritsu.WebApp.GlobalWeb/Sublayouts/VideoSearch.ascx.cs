﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class VideoSearch : UserControl
    {
        private IProviderSearchContext _videocontext;
        protected IProviderSearchContext VideoSearchContext
        {
            get
            {
                return _videocontext ?? (_videocontext = ContentSearchManager.GetIndex(StaticVariables.VideoGalleryIndex).CreateSearchContext());
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            var videoList = new List<SearchItem>();

            var query = VideoSearchContext.GetQueryable<SearchItem>().Filter(i => i.Language == Sitecore.Context.Language.Name);

            foreach (var item in query)
            {
                sitecoreService.Map(item);
                videoList.Add(item);
            }

            videoList = videoList.Where(x => x != null).ToList();
            foreach (var item in videoList)
            {
                Response.Write(string.Format("<a href='{0}' style='{2}'>{1}</a>", item.Url, item.Name, "font-weight:bold") + "<br/>");
            }
        }
    }
}