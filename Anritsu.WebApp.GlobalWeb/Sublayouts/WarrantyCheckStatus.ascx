﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WarrantyCheckStatus.ascx.cs"
    Inherits="Anritsu.WebApp.GlobalWeb.SubLayouts.WarrantyCheckStatus" %>

<div class="warranty-check-status">
    <h1>
        <%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_PageTitleSmall") %>
    </h1>
    
    <div class="container">
        <div class="your-info">
            <div class="form-container">
                <!-- Main Body to search the Certificates -->
                <div id="divMainBody" runat="server">
                    <p>
                        <%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_Text1") %>
                    </p>
                    <%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_Text3") %>
                    <div class="bd">
                        <div class="group input-text required">
                            <label for="txtMN">
                                <%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_Model") %>
                            </label>
                            <p class="error-message">
                                <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "modelRequiredMsg") %>
                            </p>
                            <asp:TextBox ID="txtMN" runat="server"></asp:TextBox>
                        </div>
                        <div class="group input-text required">
                            <label for="txtSN">
                                <%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "wrtyckpagereskey_seriallabel") %>
                            </label>
                            <p class="error-message">
                                <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "serialRequiredMsg") %>
                            </p>
                            <asp:TextBox ID="txtSN" runat="server"></asp:TextBox>
                        </div>
                        <div class="group">
                            <asp:Button ID="bttWarrantySearch" runat="server" OnClick="bttWarrantySearch_Click"
                                CssClass="button form-submit" />
                        </div>
                    </div>

                    <asp:Panel ID="pnlWarrantyResults" runat="server" Visible="false">
                        <div id="divWarrantyResults" class="WRTYCK_dynamic_roundedbox">
                            <div class="WRTYCK_dynamic_roundedboxtitle">
                                <h2 class="clrGreen">
                                    <%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_Title1") %>
                                </h2>
                            </div>
                            <div class="WRTYCK_dynamic_roundedboxbody">
                                <p class="WRTYCK_ResultTitle">
                                    <asp:Literal ID="ltrWarrantyResultTitle1" runat="server"></asp:Literal>
                                </p>
                                <div>
                                    <p class="label">
                                        <strong>
                                            <%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_Serial") %> :
                                        </strong>
                                        <asp:Literal ID="ltrSN" runat="server"></asp:Literal>
                                    </p>
                                </div>
                                <!-- warranties -->
                                <div class="roundedboxplain-wrapper">
                                    <div class="WRTYCK_roundedboxplain">
                                        <div class="body">
                                            <div>
                                                <asp:DataList ID="dlWarrantyList" runat="server" CssClass="WRTYCK_WARRTYLIST"
                                                    OnItemDataBound="dlWarrantyList_ItemDataBound">
                                                    <ItemStyle VerticalAlign="Middle" />
                                                    <HeaderTemplate>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <p class="label">
                                                            <asp:Panel ID="pnlWarrantyType" runat="server">
                                                                <%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_WarrantyTypeLabel") %> - 
                                                    <strong><%# DataBinder.Eval(Container.DataItem, "WarrantyType.WarrantyDesc")%></strong><br />
                                                            </asp:Panel>
                                                            <strong><%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_JPShippingDateLabel") %> :</strong>
                                                            <%# FormatDate(DataBinder.Eval(Container.DataItem, "JPShippingDate"))%><br />
                                                            <br />
                                                            <strong><%=Sitecore.Globalization.Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_WarrantyExpDateLabel") %> :</strong>
                                                            <%# FormatDate(DataBinder.Eval(Container.DataItem, "WarrantyExpDate"))%><br />
                                                            <br />
                                                        </p>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                    </FooterTemplate>
                                                </asp:DataList>
                                                <p class="redlabel">
                                                    <asp:Literal ID="ltrNotFoundMessage" runat="server" Visible="false"></asp:Literal>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- warranties end -->
                            </div>
                        </div>
                    </asp:Panel>
                    <div style="margin-right: 100px;">
                        <asp:Panel ID="pnlLinks" runat="server" Visible="false">
                            <table cellpadding="10" cellspacing="20" border="0" style="margin-top: 10px; margin-right: 100px;" align="right">
                                <tr>
                                    <td>
                                        <ul class="WRTYCK_UrlBullet">
                                            <li style="list-style: none">
                                                <asp:LinkButton ID="lbttRequestRMA" runat="server" OnClick="lbttRequestRMA_Click" CssClass="anchorColor"></asp:LinkButton>
                                            </li>
                                            <li style="list-style: none">
                                                <asp:HyperLink ID="hlCheckAnother" runat="server" NavigateUrl="Warranty-Check-Status.aspx" CssClass="anchorColor"></asp:HyperLink>
                                            </li>
                                            <li style="list-style: none">
                                                <asp:HyperLink ID="hlRequestServiceQuote" runat="server" CssClass="anchorColor"></asp:HyperLink>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


