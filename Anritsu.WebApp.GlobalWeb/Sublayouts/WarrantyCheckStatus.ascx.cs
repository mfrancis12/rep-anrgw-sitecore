﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Sitecore.Globalization;
using ERP = Anritsu.WebApp.SitecoreUtilities;
using SessionVariables = Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SessionVariables;
using Utils = Anritsu.WebApp.SitecoreUtilities.Utilities;
using Anritsu.WebApp.GlobalWeb.ErpSdk.ErpWS;
using Anritsu.WebApp.GlobalWeb.ErpSdk;
using Sitecore.Configuration;
using Anritsu.WebApp.SitecoreUtilities.Extensions;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts
{
    public partial class WarrantyCheckStatus : UserControl
    {
        private GetWarrantyInfoCall_RespType _warrResp;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Sitecore.Context.Language.ToString() == "en-US")
            {
                Response.Redirect(Settings.GetSetting("RMA_WrtyCheck"));
                //Response.Redirect("/rrcs/warranty-check-status.aspx");
            }
            bttWarrantySearch.Text = Translate.TextByDomain("Glossary", "Search");
            lbttRequestRMA.Text = Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_RequestRMALinkText");
            hlCheckAnother.Text = Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_ChkAnotherWtyLinkText");
            hlRequestServiceQuote.Text = Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_ReqServiceQuoteLinkText");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (!string.IsNullOrEmpty(Request[QueryStringVariables.ModelId]))
                txtMN.Text = Utils.WebUtilities.ReadRequest(QueryStringVariables.ModelId, false);

            if (!string.IsNullOrEmpty(Request[QueryStringVariables.SerialNumber]))
                txtSN.Text = Utils.WebUtilities.ReadRequest(QueryStringVariables.SerialNumber, false);
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "btt"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "btt")]
        protected void bttWarrantySearch_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            SearchWarrantyInfo(txtMN.Text.Trim(), txtSN.Text.Trim());
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "hlRequestServiceQuote"), SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper"), SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        private void SearchWarrantyInfo(string modelNumber, string serialNumber)
        {
            ResetFields();

            var erpClient = new GwErpClient();
            _warrResp = erpClient.CheckWarrantyInfo(ErpSource
                , modelNumber
                , serialNumber
                , BasePage.GetCurrentCultureInfo().Name.ToLower());

            ltrWarrantyResultTitle1.Text = modelNumber.Trim().ToUpper();
            ltrSN.Text = serialNumber.Trim();
            pnlWarrantyResults.Visible = true;

            if (_warrResp.CallStatus != CallStatus_ApiType.SUCCESS)
            {
                ShowMessage(_warrResp.ResponseMessage);
                return;
            }

            if (_warrResp.SerialNumberInfo == null) //not found
            {
                ShowMessage(
                    _warrResp.IsModelOnlyFound
                        ? Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_WrongSerialCorrectModel")
                        : Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_WrongModel")
                    //GetGlobalResourceObject("RMA_ResType"
                    //    , "WrtyCkPageResKey_WrongSerialCorrectModel").ToString()
                    );
                return;
            }

            #region " show warranty info "

            if (!string.IsNullOrEmpty(_warrResp.SerialNumberInfo.Description))
            {
                ltrWarrantyResultTitle1.Text = string.Format("{0} - {1}"
                    , modelNumber.Trim().ToUpper(), _warrResp.SerialNumberInfo.Description);
            }

            if (_warrResp.WarrantyCollection == null || _warrResp.WarrantyCollection.Length < 1)
            {
                ShowMessage(Translate.TextByDomain("RMA_ResType", "WrtyCkPageResKey_NoWarrantyFoundText")
                    //GetGlobalResourceObject("RMA_ResType"
                    //, "WrtyCkPageResKey_NoWarrantyFoundText").ToString()
                );
            }
            else
            {
                dlWarrantyList.DataSource = _warrResp.WarrantyCollection;
                dlWarrantyList.DataBind();
                dlWarrantyList.Visible = true;
            }

            #endregion

            #region " links "

            var hlRequestServiceQuote = (HyperLink)pnlLinks.FindControl("hlRequestServiceQuote");

              hlRequestServiceQuote.NavigateUrl = string.Format("Request-Service-Quote?{0}={1}&{2}={3}"
                , QueryStringVariables.ModelId
                , modelNumber
                , QueryStringVariables.SerialNumber
                , serialNumber);

            pnlLinks.Visible = (BasePage.GetCurrentCultureInfo().Name.ToLower() == "en-us"); //RMA only for en-US

            #endregion
        }

        private void ResetFields()
        {
            pnlWarrantyResults.Visible = false;
            pnlLinks.Visible = false;
            ltrSN.Text = string.Empty;
            ltrNotFoundMessage.Text = string.Empty;
            ltrNotFoundMessage.Visible = false;
            dlWarrantyList.Visible = false;
        }

        private void ShowMessage(string msg)
        {
            ltrNotFoundMessage.Text = msg;
            ltrNotFoundMessage.Visible = true;
            dlWarrantyList.Visible = false;
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "datetime")]
        protected string FormatDate(object datetime)
        {
            try
            {
                if (datetime == null || string.IsNullOrEmpty(datetime.ToString()))
                    return string.Empty;
                var dt = Convert.ToDateTime(datetime.ToString());
                return dt.ToFormattedDate();
            }
            catch
            {
                return string.Empty;
            }
        }

        private ErpSource_ApiType ErpSource
        {
            get
            {
                try
                {
                    return (ErpSource_ApiType)Enum.Parse(typeof(ErpSource_ApiType)
                        , Settings.GetSetting("RMA_ERPSource_" + Sitecore.Context.Language.ToString().Substring(3, 2)));
                    //return (ErpSource_ApiType)Enum.Parse(typeof(ErpSource_ApiType)
                    //   , "JP");
                }
                catch
                {
                    throw new HttpException(404, "Page not found.");
                }
            }
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "lbtt"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "lbtt"), SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "RMA"), SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper"), SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        protected void lbttRequestRMA_Click(object sender, EventArgs e)
        {
            var mn = txtMN.Text.Trim();
            var sn = txtSN.Text.Trim();

            var repairPriceUrl =
                string.Format(
                    "/{0}/support/repair-and-calibration/repair-calibration-pricing?{1}={2}&{3}={4}"
                    , BasePage.GetCurrentCulture()
                    , QueryStringVariables.ModelId
                    , mn.ToUpper()
                    , QueryStringVariables.SerialNumber
                    , sn.Trim());

            var requestRmaUrl = string.Format("/{0}/support/RMA/Request-RMA.aspx?{1}={2}&{3}={4}"
                , BasePage.GetCurrentCulture()
                , QueryStringVariables.ModelId
                , mn.ToUpper()
                , QueryStringVariables.SerialNumber
                , sn.ToUpper());

            if (_warrResp == null && (!string.IsNullOrEmpty(mn) && !string.IsNullOrEmpty(sn)))
            {
                SearchWarrantyInfo(mn, sn);
            }

            if (_warrResp == null
                || _warrResp.SerialNumberInfo == null
                || _warrResp.WarrantyCollection == null
                || _warrResp.WarrantyCollection.Length < 1
                || _warrResp.CallStatus != CallStatus_ApiType.SUCCESS)
            {
                Response.Redirect(repairPriceUrl);
            }
            else
            {
                var compareDate = DateTime.Now;
                if (Session[SessionVariables.Language].ToString().ToLower() == "ja-jp")
                {
                    var eom = DateTime.Parse(string.Format("1/{0}/{1}", DateTime.Now.Month, DateTime.Now.Year));
                    compareDate = eom.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                }
                var underWarrantyOrContract = false;
                foreach (var warr in _warrResp.WarrantyCollection)
                {
                    DateTime warrExpDate;
                    if (!DateTime.TryParse(warr.WarrantyExpDate, out warrExpDate)) continue;
                    if (warrExpDate < compareDate) continue;
                    underWarrantyOrContract = true;
                    break;
                }

                Response.Redirect(underWarrantyOrContract ? requestRmaUrl : repairPriceUrl);
            }
        }

        [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "dl"), SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        protected void dlWarrantyList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var pnlWarrantyType = e.Item.FindControl("pnlWarrantyType") as Panel;
            if (pnlWarrantyType != null)
                pnlWarrantyType.Visible = BasePage.GetCurrentCulture().ConvertNullToEmptyString().ToLower() != "ja-jp"; // ConvertNullToEmptyString is an ambigious call
        }
    }
}