﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.BusinessLayer;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;

namespace Anritsu.WebApp.GlobalWeb.SubLayouts.Training
{
    public partial class TrainingAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            int res;
           TrainingAdminBO trainingAdminBusinessObject = new TrainingAdminBO();
           // trainingAdminBusinessObject.ClassId = ClassId.Text;
            trainingAdminBusinessObject.ClassDescription = ClassDescription.Text;
            trainingAdminBusinessObject.ClassName = ClassName.Text;
            trainingAdminBusinessObject.PageURL = PageURL.Text;
            trainingAdminBusinessObject.CreatedOnUTC = CreatedOnUTC.Text;
            trainingAdminBusinessObject.ModifiedOnUTC = ModifiedOnUTC.Text;
            trainingAdminBusinessObject.ContentTitle = ContentTitle.Text;
            trainingAdminBusinessObject.EventId = EventId.Text;
            trainingAdminBusinessObject.PartNumber = PartNumber.Text;
            trainingAdminBusinessObject.CultureGroupId = CultureGroupId.Text;
            trainingAdminBusinessObject.ClassId = ClassId.Text;
            trainingAdminBusinessObject.DateFrom = DateFrom.Text;
            trainingAdminBusinessObject.DateTo = DateTo.Text;
            trainingAdminBusinessObject.Location = Location.Text;
            trainingAdminBusinessObject.SeatsAllocated = SeatsAllocated.Text;
            trainingAdminBusinessObject.ExpiryDate = ExpiryDate.Text;
            res = TrainingAdminBL.Register(trainingAdminBusinessObject);
            if (res != 0)
                LabelSuccessMessage.Text = "Your records inserted successfully";
            else
                LabelFailureMessage.Text = "Records not inserted";
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
        private void ResetForm()                       //Clearing all text fields.
        {
            ClassName.Text = ClassDescription.Text = PageURL.Text = CreatedOnUTC.Text = ModifiedOnUTC.Text = ContentTitle.Text = EventId.Text = CultureGroupId.Text = ClassId.Text= PartNumber.Text = DateFrom.Text = DateTo.Text = Location.Text= SeatsAllocated.Text = ExpiryDate.Text =  "";
            IsActive.Checked = false;
            
           
        }
    }
}