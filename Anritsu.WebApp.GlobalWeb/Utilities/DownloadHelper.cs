﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.ContentSearch;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.Utilities
{
    public static class DownloadHelper
    {
        public static DownloadSearchItem GetDownloadItem(string downloadName)
        {
            try
            {
                if (downloadName == "Downloads")
                    return null;
                //
                //var downloadquery = string.Format("fast:/sitecore/content/Global/downloads//*[@@templateid='{0}']", TemplateIds.Downloads);

                Item downloadRoot = Sitecore.Context.Database.GetItem("/sitecore/content/Global/downloads");

                if (downloadRoot == null)
                    return null;

                ContentSearchContract contract = new ContentSearchContract();
                contract.TemplateRestrictions.Add(TemplateIds.Downloads);
                contract.RootRestrictions.Add(downloadRoot.ID);

                var sitecorecontextName = SitecoreContext.GetContextFromSite();
                var sitecorecontext = SitecoreContext.GetFromHttpContext(sitecorecontextName);

                var results = new GeneralRepository().SearchDownloadsByContract(contract)
                    .Where(x => x.Document.Name.Equals(downloadName));

                DownloadSearchItem downloadSearchItem = null;

                if (results.Any())
                {
                    downloadSearchItem = results.Where(x => x.Document.GetItem() != null)
                        .Select(x => sitecorecontext.GetItem<DownloadSearchItem>(x.Document.ItemId.ToGuid())).FirstOrDefault();
                }

                return downloadSearchItem;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, typeof(DownloadHelper));
            }
            return null;
        }
        public static FrequentlyAskedQuestionSearchItem GetFrequentlyAskedQuestionItem(string frequentlyAskedQuestionName)
        {
            try
            {
                //var faqquery = string.Format("fast:/sitecore/content/Global/faqs//*[@@templateid='{0}']", TemplateIds.FrequentlyAskedQuestion);

                Item faRoot = Sitecore.Context.Database.GetItem("/sitecore/content/Global/faqs");

                if (faRoot == null)
                    return null;

                ContentSearchContract contract = new ContentSearchContract();
                contract.TemplateRestrictions.Add(TemplateIds.FrequentlyAskedQuestion);
                contract.RootRestrictions.Add(faRoot.ID);

                var sitecorecontextName = SitecoreContext.GetContextFromSite();
                var sitecorecontext = SitecoreContext.GetFromHttpContext(sitecorecontextName);

                var results = new GeneralRepository().SearchFAQsByContract(contract)
                    .Where(x => x.Document.Name.Equals(frequentlyAskedQuestionName));

                FrequentlyAskedQuestionSearchItem faqItem = null;

                if (results.Any())
                {
                    faqItem = results.Where(x => x.Document.GetItem() != null)
                        .Select(x => sitecorecontext.GetItem<FrequentlyAskedQuestionSearchItem>(x.Document.ItemId.ToGuid())).FirstOrDefault();
                }

                return faqItem;

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, typeof(DownloadHelper));
            }
            return null;
        }
    }
}