﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System.Collections;
using Sitecore.Data.Fields;
using Sitecore.Links;
using System.Text;
using Anritsu.WebApp.GlobalWeb.Constants;

namespace Anritsu.WebApp.GlobalWeb.Utilities
{
    public class Navigation
    {

        private string lang = "";
        Item groupHeaderItem = null;
        public StringBuilder BuildHorizontalMenuSource()
        {
            //failed to get html from cache so we are going to build it up...
            StringBuilder builder = new StringBuilder();
           
            Item groupHeadersParent = Sitecore.Context.Database.GetItem(ItemIds.GroupHeaders);
            if (groupHeadersParent != null & HasVisibleChildren(groupHeadersParent))
            {

                //Add hidden elements which helps js function to show respective group header quick links
                if (Sitecore.Context.Database.GetItem(ItemIds.CorporateHeader) != null)
                    builder.Append($"<input type=\"hidden\" id=\"hdnCorporateHeader\" " +
                        $"value=\"{Sitecore.Context.Database.GetItem(ItemIds.CorporateHeader).Name.ToLowerInvariant()}\"/>");

                Layouts.BasePage basePg = new Layouts.BasePage();
                groupHeaderItem = basePg.GetGroupHeaderItem();

                builder.Append($"<input type=\"hidden\" id=\"hdnGroupHeaders\" " +
                      $"value=\"['{groupHeaderItem.Name}']\" />");

            }

            string horizontalmenupath = null;
            Item horizontalmenuitem = null;
            lang = Sitecore.Context.Language.Name;
           

            //if (Sitecore.Context.Language.Name.ToLower().StartsWith("en-"))
            //{
            //   // horizontalmenupath = Sitecore.Context.Database.GetItem(groupHeaderItem.ID).Fields["HorizontalMenu"].Value;
            //   // horizontalmenuitem = Sitecore.Context.Database.GetItem(horizontalmenupath);
            //    lang = "en-US";
            //}
            //else
            //{
            //    lang = Sitecore.Context.Language.Name.ToString();
            //}
           


            if (Sitecore.Context.Language.Name.ToLower() == "ja-jp")
            {
                horizontalmenupath = Sitecore.Context.Database.GetItem(groupHeaderItem.ID, Language.Parse(lang)).Fields["HorizontalMenu"].Value;
                horizontalmenuitem = Sitecore.Context.Database.GetItem(horizontalmenupath, Language.Parse(lang));
                builder.Append(BuildHorizontalMenuForJapanRegion(horizontalmenuitem));
                return builder;
            }
            else
            {

                try
                {

                    horizontalmenupath = Sitecore.Context.Database.GetItem(groupHeaderItem.ID, Language.Parse(lang)).Fields["HorizontalMenu"].Value;
                    if (horizontalmenupath == "")
                    {
                        //  lang = "en-US";
                        horizontalmenupath = Sitecore.Context.Database.GetItem(groupHeaderItem.ID, Language.Parse("en-US")).Fields["HorizontalMenu"].Value;
                        horizontalmenuitem = Sitecore.Context.Database.GetItem(horizontalmenupath, Language.Parse("en-US"));
                    }
                    else
                    {
                        horizontalmenuitem = Sitecore.Context.Database.GetItem(horizontalmenupath, Language.Parse(lang));
                        if(horizontalmenuitem == null)
                        {
                            throw new Exception("Target horzontal menu item is null");
                        }
                    }
                }
                catch
                {
                    //  lang = "en-US";
                    horizontalmenupath = Sitecore.Context.Database.GetItem(groupHeaderItem.ID, Language.Parse("en-US")).Fields["HorizontalMenu"].Value;
                    horizontalmenuitem = Sitecore.Context.Database.GetItem(horizontalmenupath, Language.Parse("en-US"));
                }
                if (horizontalmenuitem != null)
                {
                    builder.Append("<!--googleoff: all-->");
                    builder.Append("<nav class='global-nav'>");
                    builder.Append("<ul class='global-menu'>");

                    for (int topmenucount = 1; topmenucount <= 5; topmenucount++)
                    {
                        if (topmenucount == 5)
                        {
                            CheckboxField chkIsVisible = horizontalmenuitem.Fields["IsVisible"];
                            if (!chkIsVisible.Checked)
                            {
                                builder.Append("</ul></nav>");
                                return builder;
                            }
                        }
                        string menutitle = GetTopMenuTitleValue(horizontalmenuitem, "Menu" + topmenucount.ToString() + " Title");
                      
                        if (menutitle != "")
                        {
                            string menuLink = GetTopMenuLinkValue(horizontalmenuitem, "Menu" + topmenucount.ToString() + " Link", false);
                            //Read the mapped item from groupeddroplink
                            var topnavigationmegamenuitem = GetTopMenuGroupedDropLinkValues(horizontalmenuitem, "Menu" + topmenucount.ToString() + " Megamenu");

                            if (menuLink == "" && topnavigationmegamenuitem == null)
                            {
                                menuLink = GetTopMenuLinkValue(horizontalmenuitem, "Menu" + topmenucount.ToString() + " Link", true);
                            }
                           
                            //UPPER CASE FOR ALL ENGLISH REGIONS
                            menutitle = TitleToUpper(menutitle);
                            builder.Append("<li class='global-menu__item'>");
                            if (menuLink != "" )
                            {
                                builder.Append("<button onclick= window.open('" + menuLink + "','_self')>" + menutitle + "</button>");
                            }
                            else
                            {
                                builder.Append("<button>" + menutitle + "</button>");
                                builder.Append(MapMegamenu(horizontalmenuitem, "Menu" + topmenucount.ToString() + " Megamenu"));
                            }
                            builder.Append("</li>");

                        }

                    }

                    builder.Append("</ul></nav>");
                }
                return builder;
            }
        }


        private StringBuilder BuildHorizontalMenuForJapanRegion(Item horizontalmenuitem)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<!--googleoff: all-->");

            builder.Append("<nav class='global-nav'>");
            builder.Append("<ul class='global-menu'>");
            for (int topmenucount = 1; topmenucount <= 5; topmenucount++)
            {

                string menutitle = GetFieldValue(horizontalmenuitem, "Menu" + topmenucount.ToString() + " Title");
                string menuLink = GetLink(horizontalmenuitem, "Menu" + topmenucount.ToString() + " Link");
                if (menutitle != "")
                {
                     builder.Append("<li class='global-menu__item'>");
                    if (menuLink != "")
                    {
                        builder.Append("<button onclick= window.open('" + menuLink + "','_self')>" + menutitle + "</button>");
                    }
                    else
                    {
                        builder.Append("<button>" + menutitle + "</button>");
                        Sitecore.Data.Fields.HtmlField field = horizontalmenuitem.Fields["Megamenu" + topmenucount.ToString()];
                      
                        if (field != null)
                        {
                            string html = Sitecore.Web.UI.WebControls.FieldRenderer.Render(horizontalmenuitem, "Megamenu" + topmenucount.ToString());

                            builder.Append(html);
                        }
                        //builder.Append(GetFieldValue(horizontalmenuitem, "Megamenu" + topmenucount.ToString()));
                    }
                    builder.Append("</li>");

                }
            }
            builder.Append("</ul></nav>");
            return builder;     
         }
        private StringBuilder BuildMegaMenuForTopNavigation(Item megamenuItem, int itemscount, string megamenutype, string megamenuFieldName)
        {
            StringBuilder builder = new StringBuilder();
            string megamenuTitle = TitleToUpper(GetFieldValue(megamenuItem, "MegamenuTitle", megamenuFieldName));
           
                //close button
                builder.Append("<div class='megamenu'>");
                builder.Append("<button class='megamenu__close'>");
                builder.Append("<span class='btn--close'></span>");
                builder.Append("</button>");

                //image title
                builder.Append("<div class='megamenu-inner'>");
                if (megamenuTitle != "")
                {
                    builder.Append("<p class='megamenu__title'>" + megamenuTitle + " </p>");
                }

                //container
                builder.Append("<ul class='megamenu__container'>");
                //container item

                if (megamenutype == "MEGAMENUIMAGES")
                {

                    builder.Append(BuildMegaMenuImageItems(megamenuItem, itemscount, megamenuFieldName));

                }
                else if (megamenutype == "MEGAMENULINKS")
                {
                    builder.Append(BuildMegaMenuLinkItems(megamenuItem, itemscount, megamenuFieldName));
                }
                else if (megamenutype == "MEGAMENUIMAGELINKS")
                {

                    builder.Append(BuildMegaMenuImageLinksItems(megamenuItem, itemscount, megamenuFieldName));

                }

                builder.Append("</ul>");
                builder.Append(BuildrecommendedImages(megamenuItem, 4, megamenuFieldName));
                builder.Append("</div>");
                builder.Append("</div>");
            
            return builder;
        }
        private StringBuilder MapMegamenu(Item topnavigationitem, string megamenuFieldName)
        {
            string megamenuimages = "{1F502DCD-664D-4C82-B2B4-07A31AED23CE}";
            string leftsidemegamenu = "{5E085D21-9796-4FF2-B971-546B6960B546}";
            string megamenulinks = "{FEDBCCBE-BB19-41B0-943F-6013E5A5D324}";
            string megamenuimagelinks = "{1DF3F2C3-BBAA-41BD-BFFC-3ACF021FE3D7}";
            StringBuilder builder = new StringBuilder();
            try
            {
                //Read the mapped item from groupeddroplink
                var topnavigationmegamenuitem = ReadGroupedDropLinkValues(topnavigationitem, megamenuFieldName);

                if (topnavigationmegamenuitem != null)
                {
                    if (topnavigationmegamenuitem.TemplateID.ToString() == megamenuimages)
                    {
                        builder.Append(BuildMegaMenuForTopNavigation(topnavigationmegamenuitem, 8, "MEGAMENUIMAGES", megamenuFieldName));
                    }
                    else if (topnavigationmegamenuitem.TemplateID.ToString() == leftsidemegamenu)
                    {
                        builder.Append(BuildLeftNavigations(topnavigationmegamenuitem, megamenuFieldName));
                    }
                    else if (topnavigationmegamenuitem.TemplateID.ToString() == megamenulinks)
                    {
                        builder.Append(BuildMegaMenuForTopNavigation(topnavigationmegamenuitem, 8, "MEGAMENULINKS", megamenuFieldName));
                    }
                    else if (topnavigationmegamenuitem.TemplateID.ToString() == megamenuimagelinks)
                    {
                        builder.Append(BuildMegaMenuForTopNavigation(topnavigationmegamenuitem, 8, "MEGAMENUIMAGELINKS", megamenuFieldName));
                    }
                }
            }
            catch
            {
                builder.Append("");
            }

            return builder;
        }
        #region Left Navigation methods
        private StringBuilder BuildLeftNavigationItem(Item megamenuItem, string megamenuFieldName="")
        {
            // string megamenuimagesfortopnavigation = "{1F502DCD-664D-4C82-B2B4-07A31AED23CE}";
            //string megamenuimagesforleftnavigationtemplateid = "{9C7676DB-A5E5-4BE8-86FF-435279BFC327}";
            //string megamenulinksforleftnavigationtemplateid = "{7889FEC2-DB64-489F-A382-632E5311C7EB}";
            //string megamenuimagelinksforleftnavigationtemplateid = "{DEFACABC-EA97-49B9-8BEC-0086446A7161}";
            string megamenuimages = "{1F502DCD-664D-4C82-B2B4-07A31AED23CE}";
            string megamenulinks = "{FEDBCCBE-BB19-41B0-943F-6013E5A5D324}";
            string megamenuimagelinks = "{1DF3F2C3-BBAA-41BD-BFFC-3ACF021FE3D7}";
            StringBuilder builder = new StringBuilder();

            builder.Append("<li class='megamenu-sub__item'>");
            builder.Append("<button>");
            builder.Append("<img src = '" + GetImage(megamenuItem, "Icon", megamenuFieldName) + "' alt='' class='megamenu-sub__item__icon' >");
            builder.Append("<span class='megamenu-sub__item__label'>" + TitleToUpper(GetFieldValue(megamenuItem, "Title", megamenuFieldName)) + "</span>");
            builder.Append("</button>");
            var topmenumegamenuItem = ReadGroupedDropLinkValues(megamenuItem, "Megamenu Selection", megamenuFieldName);
            if (topmenumegamenuItem != null)
            {

                string megamenuTitle = TitleToUpper(GetFieldValue(topmenumegamenuItem, "MegamenuTitle", megamenuFieldName));
                if (megamenuTitle != "")
                {
                    //close button
                    builder.Append("<div class='megamenu-inner--sub'>");
                    builder.Append("<button class='megamenu__close'>");
                    builder.Append("<span class='btn--close'></span>");
                    builder.Append("</button>");
                    builder.Append("<p class='megamenu__title'>" + megamenuTitle + " </p>");
                    //container
                    builder.Append("<ul class='megamenu__container'>");

                    if (topmenumegamenuItem.TemplateID.ToString() == megamenuimages)
                    {
                        // builder.Append(BuildMegaMenuForLeftNavigation(topmenumegamenuItem, 6, "MEGAMENUIMAGES"));
                        builder.Append(BuildMegaMenuImageItems(topmenumegamenuItem, 6, megamenuFieldName));
                    }
                    else if (topmenumegamenuItem.TemplateID.ToString() == megamenulinks)
                    {
                        //builder.Append(BuildMegaMenuForLeftNavigation(topmenumegamenuItem, 6, "MEGAMENULINKS"));
                        builder.Append(BuildMegaMenuLinkItems(topmenumegamenuItem, 6, megamenuFieldName));
                    }
                    else if (topmenumegamenuItem.TemplateID.ToString() == megamenuimagelinks)
                    {
                        // builder.Append(BuildMegaMenuForLeftNavigation(topmenumegamenuItem, 6, "MEGAMENUIMAGELINKS"));
                        builder.Append(BuildMegaMenuImageLinksItems(topmenumegamenuItem, 6, megamenuFieldName));
                    }
                    builder.Append(BuildrecommendedImages(megamenuItem, 3, megamenuFieldName));
                    builder.Append("</ul>");
                    builder.Append("</div>");
                }

            }
            builder.Append("</li>");
            return builder;

        }

        private StringBuilder BuildLeftNavigations(Item topnavigationmegamenuitem, string megamenuFieldName="")
        {
            StringBuilder builder = new StringBuilder();
            ArrayList lstselecteditems = ReadMultilistValues(topnavigationmegamenuitem, "Left Navigation Links Order");
            if (lstselecteditems != null && lstselecteditems.Count > 0)
            {
                builder.Append("<div class='megamenu'>");
                builder.Append("<ul class='megamenu-sub'>");
                for (int i = 0; i <= lstselecteditems.Count - 1; i++)
                {

                    builder.Append(BuildLeftNavigationItem((Sitecore.Data.Items.Item)lstselecteditems[i], megamenuFieldName));
                }
                builder.Append("</ul>");
                builder.Append("</div>");
                builder.Append("</li>");
            }
            return builder;
        }

        //private StringBuilder BuildMegaMenuForLeftNavigation(Item megamenuItem, int itemscount, string megamenutype)
        //{
        //    StringBuilder builder = new StringBuilder();
        //    string megamenuTitle = TitleToUpper(GetFieldValue(megamenuItem, "MegamenuTitle"));
        //    if (megamenuTitle != "")
        //    {
        //        //close button
        //        builder.Append("<div class='megamenu-inner--sub'>");
        //        builder.Append("<button class='megamenu__close'>");
        //        builder.Append("<span class='btn--close'></span>");
        //        builder.Append("</button>");
        //        builder.Append("<p class='megamenu__title'>" + megamenuTitle + " </p>");
        //        //container
        //        builder.Append("<ul class='megamenu__container'>");

        //        if (megamenutype == "MEGAMENUIMAGES")
        //        {
        //            //container items
        //            builder.Append(BuildMegaMenuImageItems(megamenuItem, itemscount));

        //        }
        //        else if (megamenutype == "MEGAMENULINKS")
        //        {
        //            builder.Append(BuildMegaMenuLinkItems(megamenuItem, itemscount));
        //        }
        //        else if (megamenutype == "MEGAMENUIMAGELINKS")
        //        {

        //            builder.Append(BuildMegaMenuImageLinksItems(megamenuItem, itemscount));

        //        }
        //        builder.Append(BuildrecommendedImages(megamenuItem, 3));
        //        builder.Append("</ul>");
        //        builder.Append("</div>");
        //    }
        //    return builder;
        //}
        #endregion
        #region Build Megamenu Items
        private StringBuilder BuildMegaMenuImageItems(Item megamenuItem, int itemscount, string megamenuFieldName="")
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 1; i <= itemscount; i++)
            {

                string imagesrc = GetImage(megamenuItem, "image" + i.ToString(), megamenuFieldName);
                string imagelink = GetLink(megamenuItem, "link" + i.ToString(), megamenuFieldName);
                string imagetitle = GetFieldValue(megamenuItem, "title" + i.ToString(), megamenuFieldName);
                if (imagesrc != string.Empty && imagelink != string.Empty && imagetitle != string.Empty)
                {

                    builder.Append("<li class='megamenu__container__item'>");
                    builder.Append("<a href = '" + imagelink + "' >");
                    builder.Append("<img src='" + imagesrc + "'   alt='' class='mega-item__bnr'> </a>");
                    builder.Append("<p class='mega-item__label'>");
                    builder.Append("<a href ='" + imagelink + "'>" + imagetitle + "</a>");
                    builder.Append("</p>");
                    builder.Append("<dl class='mega -item__links' style='display: none;'>");
                    builder.Append("<dt class='mega-item__links__head'></dt>");
                    builder.Append("<dd class='mega-item__links__wrapper'>");
                    builder.Append("<ul class='mega-item__links__wrapper__list'></ul>");
                    builder.Append("</dd>");
                    builder.Append("</li>");
                }
            }
            return builder;

        }
        private StringBuilder BuildMegaMenuImageLinksItems(Item megamenuItem, int itemscount,string megamenuFieldName="")
        {
            StringBuilder builder = new StringBuilder();
            int listcount = 0;
            for (int count = 1; count <= itemscount; count++)
            {
                string imagelink = GetLink(megamenuItem, "Image" + count.ToString()+"Link", megamenuFieldName);
                string imagesrc = GetImage(megamenuItem, "image" + count.ToString(), megamenuFieldName);
                string imagetitle = GetFieldValue(megamenuItem, "ImageTitle"+ count.ToString(), megamenuFieldName,true);
                if (imagetitle != "HIDETHEITEM")
                {
                    if (imagesrc != "")
                    {


                        builder.Append("<li class='megamenu__container__item'>");
                        builder.Append("<a href = '" + imagelink + "' >");
                        builder.Append("<img src='" + imagesrc + "'   alt='' class='mega-item__bnr'> </a>");
                        //builder.Append("<p class='mega-item__label'> style='display: none;'");
                        //builder.Append("<a href ='" + imagelink + "'>" + imagetitle + "</a>");
                        //builder.Append("</p>");
                        builder.Append("<dl class='mega -item__links' >");
                        builder.Append("<dt class='mega-item__links__head'></dt>");
                        builder.Append("<dd class='mega-item__links__wrapper'>");
                        builder.Append("<ul class='mega-item__links__wrapper__list'>");
                        for (int j = count + listcount; j <= count * 4; j++)
                        {
                            if (GetFieldValue(megamenuItem, "LinkTitle" + j.ToString(), megamenuFieldName) != "")
                                builder.Append("<li><a href = '" + GetLink(megamenuItem, "Link" + j.ToString(), megamenuFieldName) + "' >" + GetFieldValue(megamenuItem, "LinkTitle" + j.ToString(), megamenuFieldName) + "</a></li>");

                        }
                       
                        builder.Append("</ul>");
                        builder.Append("</dd>");
                        builder.Append("</dl>");
                        builder.Append("</li>");
                    }
                }
                listcount = listcount + 3;
            }
            return builder;

        }
        private StringBuilder BuildMegaMenuLinkItems(Item megamenuItem, int itemscount,string megamenuFieldName="")
        {
            StringBuilder builder = new StringBuilder();
            int listcount = 0;
            for (int count = 1; count <= itemscount; count++)
            {
                string categorytitle = GetFieldValue(megamenuItem, "CategoryTitle" + count.ToString(), megamenuFieldName);
                string categoryLink = GetLink(megamenuItem, "CategoryLink" + count.ToString(), megamenuFieldName);

                builder.Append("<li class='megamenu__container__item'>");
                builder.Append("<img src ='' alt='' class='mega-item__bnr' style='display: none;'>");
                builder.Append("<p class='mega-item__label' style='display: none;'></p>");
                builder.Append("<dl class='mega-item__links'>");
                if (categorytitle != string.Empty)
                {
                    builder.Append("<dt class='mega-item__links__head'>");
                    if (categoryLink != "")
                    {

                        builder.Append("<a href = '" + categoryLink + "' ><span>" + categorytitle + " </span></a>");
                    }
                    else
                    {
                        builder.Append("<span>" + categorytitle + "</span>");
                    }
                    builder.Append("</dt>");
                }
                builder.Append("<dd class='mega-item__links__wrapper'>");
                builder.Append("<ul class='mega-item__links__wrapper__list'>");
                for (int j = count + listcount; j <= count * 5; j++)
                {
                    if (GetFieldValue(megamenuItem, "Title" + j.ToString(), megamenuFieldName) != "")
                        builder.Append("<li><a href = '" + GetLink(megamenuItem, "Link" + j.ToString(), megamenuFieldName) + "' >" + GetFieldValue(megamenuItem, "Title" + j.ToString(), megamenuFieldName) + "</a></li>");

                }
                listcount = listcount + 4;
                builder.Append("</ul>");
                builder.Append("</dd>");
                builder.Append("</dl>");
                builder.Append("</li>");

            }

            return builder;

        }

        private StringBuilder BuildrecommendedImages(Item megamenuItem, int itemscount,string megamenuFieldName="")
        {
            StringBuilder builder = new StringBuilder();
            var recomendeditem = ReadGroupedDropLinkValues(megamenuItem, "Related Items", megamenuFieldName);

            if (recomendeditem != null)
            {


                builder.Append("<div class='megamenu__recommend'>");
                builder.Append("<p class='megamenu__recommend__title'>" + TitleToUpper(GetFieldValue(recomendeditem, "Megamenu Title", megamenuFieldName)) + " </p>");
                builder.Append("<ul class='megamenu__recommend__list'>");

                for (int i = 1; i <= itemscount; i++)
                {
                    string title = GetFieldValue(recomendeditem, "Title" + i.ToString(), megamenuFieldName);
                    if (title != "")
                    {
                        builder.Append("<li class='megamenu__recommend__list__item'>");
                        builder.Append("<a href = '" + GetLink(recomendeditem, "Link" + i.ToString(), megamenuFieldName) + "' >");
                        builder.Append("<img class='megamenu__recommend__list__item__bnr'");
                        builder.Append("src='" + GetImage(recomendeditem, "image" + i.ToString(), megamenuFieldName) + "'");
                        builder.Append("alt=''>" + title + "</a>");
                        builder.Append("</li>");
                    }
                }

                builder.Append("</ul>");
                builder.Append("</div>");
            }
            return builder;
        }
        #endregion
        #region Common Methods

        private bool IsVisible(Item item)
        {
            return item.Versions.Count > 0;
        }
        private bool HasVisibleChildren(Item item)
        {
            foreach (Item child in item.Children)
            {
                if (IsVisible(child))
                {
                    return true;
                }
            }
            return false;
        }

        private string TitleToUpper(string menutitle)
        {
         
           menutitle = menutitle.ToUpper();
           return menutitle;
        }
        private ArrayList ReadMultilistValues(Item myItem, string fieldname)
        {
            ArrayList lstitems = new ArrayList();
            //Read the Multifield List
            Sitecore.Data.Fields.MultilistField multiselectField = myItem.Fields[fieldname];

            

            //Iterate through each item
            if (multiselectField.Items != null && multiselectField.Items.Length > 0)
            {
                for (int i = 0; i < multiselectField.Items.Length; i++)
                {
                    Sitecore.Data.ID id = new Sitecore.Data.ID(multiselectField.Items[i]);
                    lstitems.Add(Sitecore.Context.Database.GetItem(id, Language.Parse(myItem.Language.ToString())));
                }
            }
            return lstitems;
        }
        private string GetLink(Item item, string fieldname,string megamenuFieldName="")
        {
           

            string selectedItemsValue = string.Empty;
            try
            {
                var lnk = (LinkField)item.Fields[fieldname];
                if (lnk.IsInternal && lnk.TargetItem != null)
                {

                    selectedItemsValue = LinkManager.GetItemUrl(lnk.TargetItem);

                    if (!string.IsNullOrWhiteSpace(lnk.QueryString))
                    {
                        selectedItemsValue += "?" + lnk.QueryString;
                    }

                }
                else if (lnk.LinkType.ToLower().Equals("anchor"))
                {
                    selectedItemsValue = !string.IsNullOrEmpty(lnk.Anchor) ? "#" + lnk.Anchor : string.Empty;
                }
                else
                {
                    selectedItemsValue = lnk.Url;
                    if (!string.IsNullOrWhiteSpace(lnk.QueryString))
                    {
                        selectedItemsValue += "?" + lnk.QueryString;
                    }

                }
            }
            catch
            {
                selectedItemsValue = string.Empty;
            }

            if(selectedItemsValue =="" && megamenuFieldName !="" && Sitecore.Context.Language.ToString().ToLower() != "ja-jp" && Sitecore.Context.Language.ToString().ToLower() != "en-us")
            {
                Item basedlangItem = GetUSItemValue(item, megamenuFieldName);
                selectedItemsValue = GetLink(basedlangItem, fieldname);
            }
            return selectedItemsValue;
        }
        private string GetTopMenuLinkValue(Item item, string fieldname,bool langfallback=true)
        {


            string selectedItemsValue = string.Empty;
            try
            {
                var lnk = (LinkField)item.Fields[fieldname];
                if (lnk.IsInternal && lnk.TargetItem != null)
                {

                    selectedItemsValue = LinkManager.GetItemUrl(lnk.TargetItem);

                    if (!string.IsNullOrWhiteSpace(lnk.QueryString))
                    {
                        selectedItemsValue += "?" + lnk.QueryString;
                    }

                }
                else if (lnk.LinkType.ToLower().Equals("anchor"))
                {
                    selectedItemsValue = !string.IsNullOrEmpty(lnk.Anchor) ? "#" + lnk.Anchor : string.Empty;
                }
                else
                {
                    selectedItemsValue = lnk.Url;
                    if (!string.IsNullOrWhiteSpace(lnk.QueryString))
                    {
                        selectedItemsValue += "?" + lnk.QueryString;
                    }

                }

                if (Sitecore.Context.Language.ToString().ToLower() != "ja-jp")
                {
                    if (selectedItemsValue == "" && langfallback)
                    {
                        Item USregionitem = GetTopMenuItem();
                        if (USregionitem == null) return string.Empty;
                        if (USregionitem.Fields[fieldname] == null) return string.Empty;

                        lnk = (LinkField)USregionitem.Fields[fieldname];
                        if (lnk.IsInternal && lnk.TargetItem != null)
                        {

                            selectedItemsValue = LinkManager.GetItemUrl(lnk.TargetItem);

                            if (!string.IsNullOrWhiteSpace(lnk.QueryString))
                            {
                                selectedItemsValue += "?" + lnk.QueryString;
                            }

                        }
                    }
                }
            }
            catch
            {
                selectedItemsValue = string.Empty;
            }

           return selectedItemsValue;
        }



        private string GetImage(Item item, string fieldName,string megamenuFieldName="")
        {
            string quicklinkimage = string.Empty;
            try
            {
                if (item != null && item[fieldName] != null)
                {
                    var targetDb = Sitecore.Context.Database;
                    var itemLanguage = Sitecore.Context.Language;
                    ImageField imgField = ((ImageField)item.Fields[fieldName]);
                    if (targetDb.GetItem(imgField.MediaID, itemLanguage) != null)
                    {
                        MediaItem mediaItem = targetDb.GetItem(imgField.MediaID, itemLanguage);
                        quicklinkimage = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItem);
                    }

                }
            }
            catch
            {
                quicklinkimage = string.Empty;
            }

            if (quicklinkimage == "" && megamenuFieldName != "" && Sitecore.Context.Language.ToString().ToLower() != "ja-jp" && Sitecore.Context.Language.ToString().ToLower() != "en-us")
            {
                Item basedlangItem = GetUSItemValue(item, megamenuFieldName);
                quicklinkimage = GetImage(basedlangItem, fieldName);
            }
            return quicklinkimage;
        }

        private Item GetUSItemValue(Item item,string megamenuFieldName)
        {
            Item USAItemvalue=null;
            try
            {
                string templatename = item.TemplateName;
                Item ustopmenuitem = GetTopMenuItem();
                //var refitem = ReadGroupedDropLinkValues(ustopmenuitem, megamenuFieldName);
                Sitecore.Data.Fields.ReferenceField referenceField = ustopmenuitem.Fields[megamenuFieldName];
                if(referenceField.TargetItem == null)
                {
                    return null;
                }
                var refitem = Sitecore.Context.Database.GetItem(referenceField.TargetID, Language.Parse(ustopmenuitem.Language.ToString()));
                if (refitem.Template.Name.ToLower() == item.Template.Name.ToLower())
                {
                    USAItemvalue = refitem;
                }
                else if (refitem.Fields["Related Items"] != null)
                {
                    referenceField = refitem.Fields["Related Items"];
                    refitem = Sitecore.Context.Database.GetItem(referenceField.TargetID, Language.Parse(ustopmenuitem.Language.ToString()));
                    if (refitem.Template.Name.ToLower() == item.Template.Name.ToLower())
                    {
                        USAItemvalue = refitem;
                    }
                }
                else if (refitem.Fields["Left Navigation Links Order"] != null)
                {

                    ArrayList lstselecteditems = ReadMultilistValues(refitem, "Left Navigation Links Order");

                    refitem = (Sitecore.Data.Items.Item)lstselecteditems[0];
                    if (refitem.Template.Name.ToLower() == item.Template.Name.ToLower())
                    {
                        USAItemvalue = refitem;
                    }
                }
            }
            catch
            {
                USAItemvalue = null;
            }
             return USAItemvalue;
        }
        private Item GetTopMenuItem()
        {
            Item horizontalmenuitem = null;
            string horizontalmenupath = Sitecore.Context.Database.GetItem(groupHeaderItem.ID, Language.Parse("en-US")).Fields["HorizontalMenu"].Value;
            horizontalmenuitem = Sitecore.Context.Database.GetItem(horizontalmenupath, Language.Parse("en-US"));
            return horizontalmenuitem;
        }

        private string GetTopMenuTitleValue(Item item, string fieldName)
        {

            string fieldvalue = string.Empty;
            try
            {
                if (item == null) return string.Empty;
                if (item.Fields[fieldName] == null) return string.Empty;
                fieldvalue = item.Fields[fieldName].Value;
                if (Sitecore.Context.Language.ToString().ToLower() != "ja-jp")
                {
                    if (fieldvalue == "")
                    {
                        Item USregionitem = GetTopMenuItem();
                        if (USregionitem == null) return string.Empty;
                        if (USregionitem.Fields[fieldName] == null) return string.Empty;
                        fieldvalue = USregionitem.Fields[fieldName].Value;
                    }
                }
            }
            catch
            {
                fieldvalue = string.Empty;
            }

           return fieldvalue;
        }
        private string GetFieldValue(Item item, string fieldName, string megamenuFieldName="", bool optionalfield=false)
        {

           string fieldvalue = string.Empty;
            try
            {
                if (item == null) return string.Empty;
                if (item.Fields[fieldName] == null) return string.Empty;
                fieldvalue = item.Fields[fieldName].Value;
               
            }
            catch
            {
                fieldvalue = string.Empty;
            }

            if (fieldvalue == "" && megamenuFieldName != "" && Sitecore.Context.Language.ToString().ToLower() != "ja-jp" && Sitecore.Context.Language.ToString().ToLower() != "en-us")
            {
                Item basedlangItem = GetUSItemValue(item, megamenuFieldName);
                fieldvalue = GetFieldValue(basedlangItem, fieldName);
            }
            if(fieldvalue == "HIDETHEITEM" && optionalfield==false)
            {
                fieldvalue = string.Empty;
            }
            return fieldvalue;
        }

        private Item GetTopMenuGroupedDropLinkValues(Item topmenu, string fieldname)
        {
            Item item = null;
            try
            {
                Sitecore.Data.Fields.ReferenceField referenceField = topmenu.Fields[fieldname];
                item = Sitecore.Context.Database.GetItem(referenceField.TargetID, Language.Parse(topmenu.Language.ToString()));
               
            }
            catch
            {
                item = null;
            }
            return item;
        }

         private Item ReadGroupedDropLinkValues(Item topmenu, string fieldname,string megamenuFieldName="")
        {
            Item item = null;
            try
            {
                Sitecore.Data.Fields.ReferenceField referenceField = topmenu.Fields[fieldname];
                item = Sitecore.Context.Database.GetItem(referenceField.TargetID, Language.Parse(topmenu.Language.ToString()));
                if (Sitecore.Context.Language.ToString().ToLower() != "ja-jp" && Sitecore.Context.Language.ToString().ToLower() != "en-us")
                {
                    if (item == null && megamenuFieldName!="")
                    {

                        Item USregionitem = GetUSItemValue(topmenu, megamenuFieldName);// GetTopMenuItem();
                        referenceField = USregionitem.Fields[fieldname];
                        item = Sitecore.Context.Database.GetItem(referenceField.TargetID, Language.Parse(USregionitem.Language.Name.ToString()));
                    }
                    else if (item == null && megamenuFieldName == "")
                    {
                        Item USregionitem = GetTopMenuItem();
                        referenceField = USregionitem.Fields[fieldname];
                        item = Sitecore.Context.Database.GetItem(referenceField.TargetID, Language.Parse(USregionitem.Language.Name.ToString()));

                    }
                }
            }
            catch
            {
                item = null;
            }
            return item;
        }
        #endregion
    }
}