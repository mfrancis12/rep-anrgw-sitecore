﻿using System;
using System.Collections.Specialized;
using System.Web;
using Anritsu.WebApp.GlobalWeb.Constants;
using Glass.Mapper.Sc;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System.Text;
using System.Linq;

namespace Anritsu.WebApp.GlobalWeb.Utilities
{
    public  class QuickLinks
    {
       /// <summary>
       /// Read Link Field from Item
       /// </summary>
       /// <param name="item"></param>
       /// <returns></returns>
        private string GetTarget(BaseItem item)
        {
            if (item == null) return string.Empty;
            var lnk = (LinkField)item.Fields["Link"];
            return lnk != null ? lnk.Target : string.Empty;
        }
        /// <summary>
        /// Check the item version count
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool IsVisible(Item item)
        {
            return item.Versions.Count > 0;
        }
        /// <summary>
        /// Check if the item has children
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool HasVisibleChildren(Item item)
        {
            foreach (Item child in item.Children)
            {
                if (IsVisible(child))
                {
                    return true;
                }
            }
            return false;
        }
       
        public void BuildQLinks(StringBuilder builder ,bool _enableQuickLinks)
        {
            #region QuickLinks
            if (_enableQuickLinks)
            {
                Item groupHeadersParent = Sitecore.Context.Database.GetItem(ItemIds.GroupHeaders);
                if (groupHeadersParent != null & HasVisibleChildren(groupHeadersParent))
                {
                    builder.Append("<!-- Quick links -->");

                    //Add hidden elements which helps js function to show respective group header quick links
                    if (Sitecore.Context.Database.GetItem(ItemIds.CorporateHeader) != null)
                        builder.Append($"<input type=\"hidden\" id=\"hdnCorporateHeader\" " +
                            $"value=\"{Sitecore.Context.Database.GetItem(ItemIds.CorporateHeader).Name.ToLowerInvariant()}\"/>");

                    var groupHeaders = groupHeadersParent.GetChildren().Where(x => x.Versions.Count > 0 &&
                                            x.TemplateID.ToString().Equals(TemplateIds.GroupHeader));
                    builder.Append($"<input type=\"hidden\" id=\"hdnGroupHeaders\" " +
                        $"value=\"['{groupHeaders.Select(i => i.Name.ToLowerInvariant()).Aggregate((i, j) => i + "','" + j)}']\" />");

                    //Build All Group Headers Quick Links
                    foreach (Item groupHeaderItem in groupHeaders)
                    {
                        BuildQuickLinks(groupHeaderItem, builder);
                    }

                    builder.Append("<!-- Quick links -->");
                }
            }
            #endregion QuickLinks
           
        }

        /// <summary>
        /// Build Quick Links of each group header based on QuickLinks field selection in sitecore
        /// </summary>
        /// <param name="groupHeaderItem"></param>
        /// <param name="quickLinkBuilder"></param>
        private void BuildQuickLinks(Item groupHeaderItem, StringBuilder quickLinkBuilder)
        {
            try
            {
                if (groupHeaderItem == null || groupHeaderItem.Fields["QuickLinks"] == null)
                    return;

                var quickLinks = string.IsNullOrWhiteSpace(groupHeaderItem.Fields["QuickLinks"].Value) ?
                    null : groupHeaderItem.Fields["QuickLinks"].Value.Split('|');

                if (quickLinks == null)
                    return;

                quickLinkBuilder.Append($"<div class=\"quick-links-wrapper\" data-id=\"{groupHeaderItem.Name.ToLowerInvariant()}\">");
                try
                {
                    foreach (string itemId in quickLinks)
                    {
                        var quickLinkItem = Sitecore.Context.Database.GetItem(itemId);
                        if (quickLinkItem == null)
                            continue;

                        if (HasVisibleChildren(quickLinkItem))
                        {
                            if (!HasValidTitle(quickLinkItem) || !HasValidIcon(quickLinkItem))
                                continue;

                            quickLinkBuilder.Append("<!-- Business pages -->");
                            quickLinkBuilder.Append("<div class=\"menu-options business-pages\"><ul>");

                            quickLinkBuilder.Append("<li class=\"mega-menu-link\">");
                            quickLinkBuilder.Append("<div class=\"quick-links\">");
                            quickLinkBuilder.Append("<div class=\"close-mega-menu\"><span></span></div>");
                            quickLinkBuilder.Append($"<span class=\"link\"><img src=\"{GetIcon(quickLinkItem)}\"/></span>");
                            quickLinkBuilder.Append($"<span class=\"label-title\"><label>{GetTitle(quickLinkItem)}</label><img src=\"{GetMenuIcon(true)}\"/></span>");
                            quickLinkBuilder.Append("</div>");

                            if (!string.IsNullOrWhiteSpace(GetTitle(quickLinkItem)))
                                BuildMegaMenu(quickLinkItem, quickLinkBuilder, GetTitle(quickLinkItem));

                            quickLinkBuilder.Append("</li>");
                            quickLinkBuilder.Append("</ul></div>");
                        }
                        else
                        {
                            if (!HasValidTitle(quickLinkItem) || !HasValidLink(quickLinkItem) || !HasValidIcon(quickLinkItem))
                                continue;

                            quickLinkBuilder.Append("<!-- Landing pages -->");
                            quickLinkBuilder.Append("<div class=\"menu-options landing-pages\"><ul>");

                            quickLinkBuilder.Append($"<li class=\"mega-menu-link\">");
                            quickLinkBuilder.Append("<div class=\"quick-links\">");
                            quickLinkBuilder.Append($"<span class=\"link\"><img src=\"{GetIcon(quickLinkItem)}\"/></span>");
                            quickLinkBuilder.Append($"<span class=\"label-title\" data-url=\"{GetLink(quickLinkItem)}\" data-target=\"{GetTarget(quickLinkItem)}\"><label>{GetTitle(quickLinkItem)}</label><img src=\"{GetMenuIcon(false)}\"/></span>");
                            quickLinkBuilder.Append("</div>");
                            quickLinkBuilder.Append("</li>");

                            quickLinkBuilder.Append("</ul></div>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.StackTrace, ex, GetType());
                }
                finally { quickLinkBuilder.Append("</div>"); }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }


        /// <summary>
        /// Get Link information from
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private string GetLink(Item item)
        {
            string selectedItemsValue = string.Empty;
            var lnk = (LinkField)item.Fields["Link"];
            if (lnk.IsInternal && lnk.TargetItem != null)
            {
                //if (Parent.Controls.Count >= 1)
                //{
                //    selectedItemsValue = _useDomainUrl
                //               ? "//" + Context.Request.Url.Host + LinkManager.GetItemUrl(lnk.TargetItem)
                //               : LinkManager.GetItemUrl(lnk.TargetItem);
                //}
                //else
                //{
                    selectedItemsValue = LinkManager.GetItemUrl(lnk.TargetItem);
                //}
                if (!string.IsNullOrWhiteSpace(lnk.QueryString))
                {
                    selectedItemsValue += "?" + lnk.QueryString;
                }

            }
            else if (lnk.LinkType.ToLower().Equals("anchor"))
            {
                selectedItemsValue = !string.IsNullOrEmpty(lnk.Anchor) ? "#" + lnk.Anchor : string.Empty;
            }
            else
            {
                selectedItemsValue = lnk.Url;
                if (!string.IsNullOrWhiteSpace(lnk.QueryString))
                {
                    selectedItemsValue += "?" + lnk.QueryString;
                }

            }
            return selectedItemsValue;
        }



        /// <summary>
        /// Build MegaMenu of respective Quick Link
        /// </summary>
        /// <param name="quickLinkItem"></param>
        /// <param name="quickLinkBuilder"></param>
        /// <param name="megaMenuTitle"></param>
        public StringBuilder BuildMegaMenu(Item quickLinkItem, StringBuilder quickLinkBuilder, string megaMenuTitle)
        {
            
            try
            {
               // quickLinkBuilder.Append(GetProductCategoryAndFamilies(Sitecore.Context.Database.GetItem(ItemIds.TestMeasurement)));
                quickLinkBuilder.Append("<div class=\"mega-menu-wrapper_tobe_delete\">");
                quickLinkBuilder.Append($"<div class=\"mega-menu-container_tobe_delete\" data-id=\"{megaMenuTitle}\">");
                if (string.IsNullOrWhiteSpace(GetLink(quickLinkItem)))
                    quickLinkBuilder.Append($"<div class=\"mega-menu-title_tobe_delete\">{megaMenuTitle}</div>");
                else
                    quickLinkBuilder.Append($"<div class=\"mega-menu-title_tobe_delete\"><a href=\"{GetLink(quickLinkItem)}\" target=\"{GetTarget(quickLinkItem)}\">{megaMenuTitle}</a></div>");
                quickLinkBuilder.Append("<div class=\"mega-menu-nav\">");

                try
                {
                    foreach (Item columnPositionItem in
                            Sitecore.Context.Database.GetItem(ItemIds.QuickLinksColumnPositions).GetChildren()
                            .Where(x => x.TemplateID.ToString().Equals(TemplateIds.MegaMenuColumn)).Take(3))
                    {
                        var items = quickLinkItem.GetChildren().Where(x => x.Versions.Count > 0 &&
                                        x.TemplateID.ToString().Equals(TemplateIds.CategoryLink)).Where(x => x.Fields["Column Position"] != null &&
                                        x.Fields["Column Position"].Value.Equals(columnPositionItem.ID.ToString()));

                        quickLinkBuilder.Append("<div class=\"mega-menu-3columns\">");
                        try
                        {
                            foreach (Sitecore.Data.Items.Item categoryItem in SortItems(items))
                            {
                                #region BuildCategories

                                // If there is no category title, ignore the category item and continue with further category items
                                if (!HasValidTitle(categoryItem))
                                    continue;

                                if (string.IsNullOrWhiteSpace(GetLink(categoryItem)))
                                    quickLinkBuilder.Append($"<div class=\"column-title\">{GetTitle(categoryItem)}</div>");
                                else
                                    quickLinkBuilder.Append($"<div class=\"column-title\"><a href=\"{GetLink(categoryItem)}\" target=\"{GetTarget(categoryItem)}\">{GetTitle(categoryItem)}</a></div>");

                                quickLinkBuilder.Append("<ul>");

                                try
                                {
                                    foreach (Sitecore.Data.Items.Item subCategoryItem in SortItems(categoryItem.GetChildren()
                                        .Where(x => x.TemplateID.ToString().Equals(TemplateIds.SubCategoryLink))))
                                    {
                                        #region BuildSubCategories

                                        // If there is no subcategory title or link, ignore the subcategory item and continue 
                                        // with further subcategory items
                                        if (!HasValidTitle(subCategoryItem) || !HasValidLink(subCategoryItem))
                                            continue;

                                        quickLinkBuilder.Append($"<li><a href=\"{GetLink(subCategoryItem)}\" target=\"{GetTarget(subCategoryItem)}\">{GetTitle(subCategoryItem)}</a></li>");

                                        #endregion
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error(ex.StackTrace, ex, GetType());
                                }
                                finally { quickLinkBuilder.Append("</ul>"); }

                                #endregion
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex.StackTrace, ex, GetType());
                        }
                        finally
                        {
                            quickLinkBuilder.Append("</div>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.StackTrace, ex, GetType());
                }
                finally
                {

                    quickLinkBuilder.Append("</div>");
                    quickLinkBuilder.Append("</div>");
                    quickLinkBuilder.Append("</div>");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
            return quickLinkBuilder;
        }

        public string GetTitle(Item item)
        {
            if (item == null) return string.Empty;
            if (item.Fields["Title"] == null) return string.Empty;
            return item.Fields["Title"].Value;
        }

        /// <summary>
        /// Get the Sort Order information for Category/Sub Category
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private int GetSortOrder(Item item)
        {
            int sortOrder = 0;
            if (item == null || item.Fields["Sort Order"] == null) return 0;
            Int32.TryParse(item.Fields["Sort Order"].Value, out sortOrder);
            return sortOrder;
        }

        /// <summary>
        /// This method will provide sorted items
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private System.Collections.ArrayList SortItems(System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> items)
        {

            System.Collections.Hashtable sort = new System.Collections.Hashtable();
            foreach (Item item in items)
            {
                if (GetSortOrder(item) != 0)
                {
                    if (sort.ContainsKey(GetSortOrder(item)) == false)
                    {
                        sort.Add(GetSortOrder(item), item.ID);
                    }
                }
            }

            var orderedKeys = sort.Keys.Cast<int>().OrderBy(c => c); // supposing you're using string keys
            var allKvp = from x in orderedKeys select new { key = x, value = sort[x] };
            System.Collections.ArrayList arrlst = new System.Collections.ArrayList();
            foreach (var entry in allKvp)
            {
                var contextService = new SitecoreContext();
                arrlst.Add(contextService.GetItem<Item>(entry.value.ToString()));
            }

            return arrlst;
        }

        /// <summary>
        /// This method will provide image information for quicklink right icons
        /// </summary>
        /// <param name="isMegaMenu"></param>
        /// <returns></returns>
        private string GetMenuIcon(bool isMegaMenu)
        {
            string quickLinkImage = string.Empty;
            var contextService = new SitecoreContext();
            var item = contextService.GetItem<Item>(ItemIds.QuickLinksCommonIcons);
            if (item == null) return string.Empty;
            return GetQuickLinkImage(item, isMegaMenu ? "Mega Menu Icon" : "Quick Link Menu Icon");
        }

        /// <summary>
        /// This method will provide image information based on item and fieldName
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private string GetQuickLinkImage(Item item, string fieldName)
        {
            string quicklinkimage = string.Empty;
            if (item != null && item[fieldName] != null)
            {
                var targetDb = Sitecore.Context.Database;
                var itemLanguage = Sitecore.Context.Language;
                ImageField imgField = ((ImageField)item.Fields[fieldName]);
                if (targetDb.GetItem(imgField.MediaID, itemLanguage) != null)
                {
                    MediaItem mediaItem = targetDb.GetItem(imgField.MediaID, itemLanguage);
                    quicklinkimage = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItem);
                }
            }
            return quicklinkimage;
        }

        private string GetIcon(Item item)
        {
            return GetQuickLinkImage(item, "Icon");
        }

        private bool HasValidTitle(Item item)
        {
            return !string.IsNullOrWhiteSpace(GetTitle(item));
        }

        private bool HasValidLink(Item item)
        {
            return !string.IsNullOrWhiteSpace(GetLink(item));
        }

        private bool HasValidIcon(Item item)
        {
            return !string.IsNullOrWhiteSpace(GetIcon(item));
        }
    }
}
