﻿using System;
using System.Data;
using Anritsu.Library.CheckoutSDK.ARCKRefPaymentManagementAPI;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;

namespace Anritsu.WebApp.GlobalWeb.Utilities
{
    public class TrainingAfterCheckoutSession
    {
        public Guid TrainingTransactionKey { get; set; }
        public Guid CheckoutTransactionKey { get; set; }
        public String LastPageName { get; set; }
        public TraningRegistration TraningRegInfo { get; set; }
        public CheckoutTransactionType CKTran { get; set; }
        public DataSet CartData { get; set; }
        public Boolean IsProcessed { get; set; }
    }
}