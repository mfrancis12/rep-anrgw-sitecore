﻿﻿using Anritsu.WebApp.GlobalWeb.Constants;
using Sitecore.Data.Items;
using Sitecore.Web.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Sitecore;
using System.Web;
using System.Linq;
using Sitecore.Data.Fields;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Anritsu.WebApp.GlobalWeb.SiteSearch;

namespace Anritsu.WebApp.GlobalWeb.WebControls
{
    public class Breadcrumb : WebControl
    {
        readonly Sitecore.Data.Database _contextDb = Sitecore.Context.Database;

        protected override void DoRender(HtmlTextWriter output)
        {
            try
            {
                if (SiteSearchHelper.IsSearchCrawler(HttpContext.Current.Request))
                    return;

                var breadCrumbItems = new List<Item>();
                
                breadCrumbItems = BindParentItem(Sitecore.Context.Item, breadCrumbItems);
                breadCrumbItems.Reverse();

                //BElow changes are to show only last 3 breadcrumb itesm
                breadCrumbItems = breadCrumbItems.Where(item => item.Fields["ShowInBreadcrumb"] != null
                                        && item["ShowInBreadcrumb"] == "1" && item.TemplateID.ToString() != TemplateIds.ProductRegion).ToList();
                //breadCrumbItems.RemoveAll(item => item.Fields["ShowInBreadcrumb"] == null || item["ShowInBreadcrumb"] == "0");
                if (breadCrumbItems.Count > 4)
                {
                    int remove = breadCrumbItems.Count - 4;
                    breadCrumbItems.RemoveRange(0, remove);
                }
                
                output.Write("<!--googleoff: all-->");
                output.Write("<ul class='breadcrumb'>");
                if(breadCrumbItems.Count == 4)
                {
                    string temp = string.Format(@"<li><a {1} {0} title=""{2}"">...</a></li>", 
                                        GetActiveClass(breadCrumbItems[0]), GetUrl(breadCrumbItems[0]),
                                        GetName(breadCrumbItems[0]));
                    output.Write(temp);
                    breadCrumbItems.RemoveAt(0);
                }
                var str = BindBreadCrumb(breadCrumbItems);
                output.Write(str);
                output.Write("</ul>");
                output.Write("<!--googleon: all-->");
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.StackTrace, ex, GetType());
            }
        }

        private List<Item> BindParentItem(Item itm, List<Item> itemList)
        {
            var breadCrumbItem = itm;
            var home = _contextDb.GetItem(ItemIds.Home);
            if (itm == null || !itm.Axes.IsDescendantOf(home)) return itemList;

            if (itm.TemplateID.ToString().Equals(TemplateIds.HomeTemplate))
            {
                itemList.Add(breadCrumbItem);
            }
            else
            {
                itemList.Add(breadCrumbItem);
                BindParentItem(itm.Parent, itemList);
            }
            return itemList;
        }

        private string BindBreadCrumb(List<Item> breadCrumbList)
        {
            var breadCrumb = new StringBuilder();
            var bcList = new List<string>();
            
            foreach (var breadCrumbMarkUp in from child in breadCrumbList
                                             where child.Fields["MenuTitle"] != null && (CheckboxField)child.Fields["ShowInBreadcrumb"] != null
                                             let field = (CheckboxField)child.Fields["ShowInBreadcrumb"]
                                             let isChecked = field.Checked
                                             where isChecked && child.TemplateID.ToString() != TemplateIds.ProductRegion
                                             select string.Format(@"<li><a {1} {0} title=""{3}"">{2}</a></li>",
                                                 GetActiveClass(child), GetUrl(child),
                                                 GetName(child), GetName(child)))
            {
                breadCrumb.Append(breadCrumbMarkUp);
                bcList.Add(breadCrumbMarkUp);
            }

            var i = 0;
            var parsedString = breadCrumb.ToString();
            if (Sitecore.Context.Database.GetItem(ItemIds.Breadcrumb) == null) return parsedString;

            var item = Sitecore.Context.Database.GetItem(ItemIds.Breadcrumb);
            if (string.IsNullOrEmpty(item.Fields["Value"].Value) || !(item.Fields["Value"].Value).IsNumeric()) return parsedString;
            var limit = System.Convert.ToInt32(item.Fields["Value"].Value);
            while (StringUtil.RemoveTags(parsedString).Length > limit)
            {
                if (bcList.Count <= i)
                {
                    break;
                }
                string str = bcList[i + 1];
                string newStr = str.Replace(">" + StringUtil.RemoveTags(str) + "<", ">...<");
                parsedString = parsedString.Replace(bcList[i + 1], newStr);
                i++;
            }
            return parsedString;
        }


        private string GetUrl(Item currentItem)
        {
            if (currentItem.TemplateID.ToString() == TemplateIds.RedirectLink)
            {
                LinkField link = currentItem.Fields["RedirectLink"];

                return link.TargetItem != null
                    ? "href=" + Sitecore.Links.LinkManager.GetItemUrl(link.TargetItem) + "#goto_products"
                    : "";
            }
            return Sitecore.Context.Item.Equals(currentItem) ? null : "href=" + Sitecore.Links.LinkManager.GetItemUrl(currentItem);
        }

        private string GetActiveClass(Item currentItem)
        {
            return Sitecore.Context.Item.Equals(currentItem) ? "class=active" : null;
        }

        public string GetName(BaseItem currentItem)
        {
            return string.IsNullOrEmpty(currentItem["MenuTitle"]) ? HttpUtility.HtmlDecode(StringUtil.RemoveTags(currentItem["PageTitle"])) : HttpUtility.HtmlDecode(StringUtil.RemoveTags(currentItem["MenuTitle"]));
        }
    }
}