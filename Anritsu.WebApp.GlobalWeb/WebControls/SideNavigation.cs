﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using Anritsu.WebApp.GlobalWeb.Constants;
using Glass.Mapper.Sc;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Shell.Applications.ContentManager.ReturnFieldEditorValues;
using Sitecore.Web;
using Sitecore.Web.UI;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using System.Text;
using Anritsu.WebApp.SitecoreUtilities.CustomCaches;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data.Managers;
using Sitecore.Configuration;
using System.Collections;


namespace Anritsu.WebApp.GlobalWeb.WebControls
{
    public class SideNavigation : WebControl
    {
      
        private string SideNavigationCacheKey = "side_navigation_cache_key";
        private string lang = "";
        protected override string GetCachingID()
        {
            if (SiteSearchHelper.IsSearchCrawler(HttpContext.Current.Request))
                return GetType().FullName + "_Search";
            if (HttpContext.Current.Request.Headers["X-PJAX"] != null)
                return GetType().FullName + "_Pjax";

            return GetType().FullName;
        }

        private bool _useDomainUrl
        {
            get
            {
                try
                {
                    if (Parameters == null || !Parameters.Contains("UseDomainUrl")) return false;
                    var webControlParameters =
                        WebUtil.ParseUrlParameters(Parameters);
                    return webControlParameters["UseDomainUrl"].Equals("1");
                }
                catch (Exception)
                {

                    return false;
                }
            }
        }

        private bool _enableQuickLinks
        {
            get
            {
                try
                {
                    if (Parameters == null || !Parameters.Contains("EnableQuickLinks")) return true;
                    var webControlParameters =
                        WebUtil.ParseUrlParameters(Parameters);
                    return webControlParameters["EnableQuickLinks"].Equals("1");
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        protected override void DoRender(HtmlTextWriter output)
        {
            if (HttpContext.Current.Request.Headers["X-PJAX"] != null || SiteSearchHelper.IsSearchCrawler(HttpContext.Current.Request))
                return;

            try
            {
              
                //failed to get html from cache so we are going to build it up...
                StringBuilder builder = new StringBuilder();
                Anritsu.WebApp.GlobalWeb.Utilities.Navigation navigation = new Anritsu.WebApp.GlobalWeb.Utilities.Navigation();
                builder = navigation.BuildHorizontalMenuSource();
                
                //write builder.
                output.Write(builder.ToString());

            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }
      
        #region SideMenus and QuickLink methods
        private StringBuilder BuildSideMenuSource()
        {
            //failed to get html from cache so we are going to build it up...
            StringBuilder builder = new StringBuilder();

            var contextService = new SitecoreContext();
            var item = contextService.GetItem<Item>(ItemIds.SideMenuHome);
            if (item != null)
            {
                //output.Write("<!--googleoff: all-->");
                builder.Append("<!--googleoff: all-->");
                //output.Write("<div class='aside-menu-trigger'>");
                builder.Append("<div class='aside-menu-trigger'>");
                //output.Write("<div class='icon icon-burguer'></div>");
                builder.Append("<div class='icon icon-burguer'>");
                if (_enableQuickLinks)
                {

                    builder.Append("<span class='hamburger-menu'></span>");
                }
                else
                {
                    builder.Append("</div>");
                }
                var menuTxtItem = Sitecore.Context.Database.GetItem(ItemIds.MenuText);
                if (menuTxtItem != null && menuTxtItem.Versions.Count > 0)
                {
                    if (_enableQuickLinks)
                    {
                        builder.Append("<span class='menu-title'> ");
                    }
                    else
                    {
                        builder.Append("<span> ");
                    }
                    builder.Append(menuTxtItem.Fields["Title"] + "</span>");
                }
                if (_enableQuickLinks)
                {
                    builder.Append("</div>");
                }
                Anritsu.WebApp.GlobalWeb.Utilities.QuickLinks quickLinks = new Anritsu.WebApp.GlobalWeb.Utilities.QuickLinks();
                quickLinks.BuildQLinks(builder, _enableQuickLinks);

                builder.Append("</div>");
                //output.Write("<div class='pusher'><div class='aside-menu'>");
                builder.Append("<div class='pusher'><div class='aside-menu'>");
                BuildMenuItems(item, builder, 1);
                //output.Write("</div></div>");
                builder.Append("</div></div>");
                //output.Write("<!--googleon: all-->");
                builder.Append("<!--googleon: all-->");

            }

            return builder;
           
        }

        private void BuildMenuItems(Item menuItem, StringBuilder builder, int level)
        {
            if (HasVisibleChildren(menuItem))
            {
                var backClass = level == 1 ? "backhome" : "back";
                //output.WriteLine("<div data-tag=\"{0}\" class=\"item\">", GetLink(menuItem));
                builder.AppendFormat("<div data-tag=\"{0}\" class=\"item\">", GetLink(menuItem));
                //output.WriteLine("<div class='menu-title'>");
                  if (level > 1)
                {
                    //output.Write("<div class='menu-subtitle'>" + GetLinkText(menuItem) + "</div>");
                    builder.Append("<div class='menu-subtitle'>" + GetLinkText(menuItem) + "</div>");
                }

                builder.Append("<div class='menu-title'>");
                if (menuItem.ID.ToString() == ItemIds.SideMenuHome)
                {
                    //output.WriteLine(
                    //string.Format("<span href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</span>", GetLink(menuItem),
                    //       GetLinkText(menuItem)), backClass);

                    builder.AppendFormat(string.Format("<span href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</span>", GetLink(menuItem), GetLinkText(menuItem)), backClass);
                }
                else
                {
                    if (!HasVisibleChildren(menuItem))
                    {
                        //     output.WriteLine(
                        //string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</a>", GetLink(menuItem), ((menuItem.ID.ToString().Equals(temIds.NewSideMenuHome)) ? menuItem["Title"] : Translate.TextByDomain("GlobalDictionary", "backhome")))
                        //, backClass);

                        builder.AppendFormat(
                  string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</a>", GetLink(menuItem), ((menuItem.ID.ToString().Equals(ItemIds.SideMenuHome)) ? menuItem["Title"] : Translate.TextByDomain("GlobalDictionary", "backhome")))
                  , backClass);
                    }
                    else
                    {
                        if (level == 2)
                        {
                            var contextService = new SitecoreContext();
                            Item homeItem = contextService.GetItem<Item>(ItemIds.SideMenuHome);
                            //    output.WriteLine(
                            //string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem.Parent) + " target=\"{1}\">{2}</a>", GetLink(menuItem.Parent), GetTarget(menuItem.Parent), homeItem["Title"]), backClass);
                            builder.AppendFormat(string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem.Parent) + " target=\"{1}\">{2}</a>", GetLink(menuItem.Parent), GetTarget(menuItem.Parent), homeItem["Title"]), backClass);
                        }
                        else
                        {
                            //    output.WriteLine(
                            //string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem.Parent) + " target=\"{1}\">{2}</a>", GetLink(menuItem.Parent), GetTarget(menuItem.Parent),
                            //    ((menuItem.ID.ToString().Equals(temIds.NewSideMenuHome)) ? menuItem["Title"] : Translate.TextByDomain("GlobalDictionary", "backtomenu"))), backClass);

                            builder.AppendFormat(
                        string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem.Parent) + " target=\"{1}\">{2}</a>", GetLink(menuItem.Parent), GetTarget(menuItem.Parent),
                            ((menuItem.ID.ToString().Equals(ItemIds.SideMenuHome)) ? menuItem["Title"] : Translate.TextByDomain("GlobalDictionary", "backtomenu"))), backClass);
                        }
                    }
                }

                //output.WriteLine("</div>");
                builder.AppendLine("</div>");
              
                //output.WriteLine("<div class=\"menu-item\">");
                builder.AppendLine("<div class=\"menu-item\">");
                foreach (Item item in menuItem.Children)
                {
                    if (IsVisible(item))
                    {
                        if (HasVisibleChildren(item))
                        {
                            if (EnablePjax(item))
                            {
                                //output.Write("<a href=\"{0}\" class=\"haschild\" target=\"{1}\">{2}</a>",
                                //   GetLink(item), GetTarget(item), GetLinkText(item));
                                builder.AppendFormat("<a href=\"{0}\" class=\"haschild\" target=\"{1}\">{2}</a>",
                                 GetLink(item), GetTarget(item), GetLinkText(item));

                            }
                            else
                            {
                                //output.Write("<a href=\"{0}\" " + GetPjaxAttribute(item) + " class=\"haschild\" target=\"{1}\">{2}</a>",
                                //    GetLink(item), GetTarget(item), GetLinkText(item));
                                builder.AppendFormat("<a href=\"{0}\" " + GetPjaxAttribute(item) + " class=\"haschild\" target=\"{1}\">{2}</a>",
                                    GetLink(item), GetTarget(item), GetLinkText(item));
                            }
                        }
                        else
                        {
                            if (EnablePjax(item))
                            {
                                //output.Write("<a href=\"{0}\" target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
                                builder.AppendFormat("<a href=\"{0}\" target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
                            }
                            else
                            {
                                //output.Write("<a href=\"{0}\" " + GetPjaxAttribute(item) + "  target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
                                builder.AppendFormat("<a href=\"{0}\" " + GetPjaxAttribute(item) + "  target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
                            }
                        }
                    }
                }
                //output.WriteLine("</div>");
                builder.AppendLine("</div>");
                //output.WriteLine("</div>");
                builder.AppendLine("</div>");
                foreach (Item item in menuItem.Children)
                {
                    if (IsVisible(item))
                    {
                        BuildMenuItems(item, builder, level + 1);
                    }
                }
            }
        }

        private string GetPjaxAttribute(BaseItem item)
        {
            if (item == null) return "data-pjax=\"\"";
            var lnk = (LinkField)item.Fields["Link"];
            return lnk == null ? "data-pjax=\"\"" : (lnk.Target == "_blank" || lnk.Target.ToUpperInvariant() == "NEW BROWSER" ? "" : "data-pjax=\"\"");
        }

        public string GetLinkText(BaseItem item)
        {
            return item["Title"];
        }

        private bool IsVisible(Item item)
        {
            return item.Versions.Count > 0;
        }

        private bool HasVisibleChildren(Item item)
        {
            foreach (Item child in item.Children)
            {
                if (IsVisible(child))
                {
                    return true;
                }
            }
            return false;
        }

        public string GetLink(Item item)
        {
            string selectedItemsValue = string.Empty;
            var lnk = (LinkField)item.Fields["Link"];
            if (lnk.IsInternal && lnk.TargetItem != null)
            {
                if (Parent.Controls.Count >= 1)
                {
                    selectedItemsValue = _useDomainUrl
                               ? "//" + Context.Request.Url.Host + LinkManager.GetItemUrl(lnk.TargetItem)
                               : LinkManager.GetItemUrl(lnk.TargetItem);
                }
                else
                {
                    selectedItemsValue = LinkManager.GetItemUrl(lnk.TargetItem);
                }
                if(!string.IsNullOrWhiteSpace(lnk.QueryString))
                {
                    selectedItemsValue += "?" + lnk.QueryString;
                }
              
            }
            else if (lnk.LinkType.ToLower().Equals("anchor"))
            {
                selectedItemsValue = !string.IsNullOrEmpty(lnk.Anchor) ? "#" + lnk.Anchor : string.Empty;
            }
            else
            {
                selectedItemsValue = lnk.Url;
                if (!string.IsNullOrWhiteSpace(lnk.QueryString))
                {
                    selectedItemsValue += "?" + lnk.QueryString;
                }
              
            }
            return selectedItemsValue;
        }

        private bool EnablePjax(Item item)
        {
            bool downloadItem = false;

            var lnk = (LinkField)item.Fields["Link"];

            if (lnk != null && lnk.TargetItem != null)
            {
                if (lnk.TargetID.ToString().Equals(ItemIds.DownloadItem) ||
                    lnk.TargetID.ToString().Equals(ItemIds.VideoGallery) ||
                    lnk.TargetID.ToString().Equals(ItemIds.HomeNewsPage) ||
                    lnk.TargetID.ToString().Equals(ItemIds.HomeEventsPage) ||
                    lnk.TargetID.ToString().Equals(ItemIds.IRNewsPage) ||
                    lnk.TargetID.ToString().Equals(ItemIds.SearchDiscontinuedModels) ||
                    lnk.TargetID.ToString().Equals(ItemIds.OpticalDevice) ||
                    lnk.TargetID.ToString().Equals(ItemIds.InfivisHome) ||
                    lnk.TargetID.ToString().Equals(ItemIds.UltraFastElectronDevices))
                {
                    downloadItem = true;
                }
                if (lnk.TargetItem.TemplateID.ToString().Equals(TemplateIds.Forms))
                {
                    downloadItem = true;
                }
            }
            return downloadItem;
        }

        public string GetTarget(BaseItem item)
        {
            if (item == null) return string.Empty;
            var lnk = (LinkField)item.Fields["Link"];
            return lnk != null ? lnk.Target : string.Empty;
        }


        #endregion



    }
}