﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;

using Anritsu.WebApp.GlobalWeb.Constants;
using Glass.Mapper.Sc;

using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Web.UI;
using System.IO;
using Sitecore.Web;
using System.Globalization;



namespace Anritsu.WebApp.GlobalWeb.WebControls
{
    public class SiteMap : WebControl
    {
        int count = 0;
        protected override void DoRender(HtmlTextWriter output)
        {
            Item item = Sitecore.Context.Item;
            if (item != null)
            {

                //BindItems(item, output);
                output.Write("<div class='container'>");
                //output.Write("<div class='Grid4'>");
                output.Write("<div class='Section'>");

                FetchNameValueList(item, output);

                output.Write("</div></div>");
            }




        }
        private void FetchNameValueList(Item item, HtmlTextWriter output)
        {


            string _urlParamsToParse = item["SiteMapLinks"];
            NameValueCollection nameValueCollection = Sitecore.Web.WebUtil.ParseUrlParameters(_urlParamsToParse);
            //level=Convert.ToInt32(key)
            foreach (string key in nameValueCollection.Keys)
            {
                string finalKey = HttpUtility.UrlDecode(key);
                var value = nameValueCollection[key];
                count = 0;
                if (Sitecore.Context.Database.GetItem(finalKey) != null)
                {
                    BindItems(Sitecore.Context.Database.GetItem(finalKey), Convert.ToInt32(value), output);
                }

            }
        }
        public void BindItems(Item item, int level, TextWriter output)
        {

            if (IsVisible(item))
            {

                output.Write("<h2><span class='down-arrow'></span><span>");
                output.Write("<a class='mainHeading' href=\"{0}\" target=\"{2}\">{1}</a>", GetLink(item), GetLinkText(item),GetTargetType(item));
                output.Write("<br>   ");
                output.Write("</span></h2>");
                count++;

                SecondLevel(item, level, output, count);
            }
        }
        public void SecondLevel(Item item, int level, TextWriter output, int localCount)
        {

            //count++;
            foreach (Item child in item.Children)
            {
                if (item!= null && IsVisible(item))
                {
                    int temp = localCount;
                    if (temp <= localCount)
                    {
                        output.Write("<a href=\"{0}\" target=\"{2}\">{1}</a>",
                         GetLink(child), GetLinkText(child),GetTargetType(child));
                        output.Write("<br>   ");
                        temp++;
                        if (temp <= level)
                        {
                            BindLastLevel(child, level, output, temp);
                        }
                    }
                }
            }
        }


        public void BindLastLevel(Item item, int level, TextWriter output, int localCount)
        {
            output.Write("<ul class='second-level-list'>");
            foreach (Item child in item.Children)
            {
                if (item != null && IsVisible(item))
                {
                    int temp = localCount;
                    if (temp <= level)
                    {
                        output.Write("<li><a class='lastLevel' href=\"{0}\" target=\"{2}\">{1}</a></li>",
                           GetLink(child), GetLinkText(child),GetTargetType(child));
                        // output.Write("<br>");
                        temp++;
                        if (temp <= level)
                        {
                            BindLastLevel(child, level, output, temp);
                        }
                    }
                }


            }
            output.Write("</ul>");
        }


        public string GetLinkText(BaseItem item)
        {
            return item["Title"];
        }
        public string GetLink(BaseItem item)
        {
            string selectedItemsValue = string.Empty;
            var lnk = (LinkField)item.Fields["Link"];
            //if (lnk.TargetItem != null)
            //    return LinkManager.GetItemUrl(lnk.TargetItem);
            //else
            //    return string.Empty;
            if (lnk.IsInternal && lnk.TargetItem != null)
            {
                return LinkManager.GetItemUrl(lnk.TargetItem);
            }
            else if (lnk.LinkType.ToLower(CultureInfo.InvariantCulture).Equals("external"))
            {
                selectedItemsValue = lnk.Url;
            }
            else
            {
                selectedItemsValue = "";
            }
            return selectedItemsValue;
        }
        private bool IsVisible(Item item)
        {
            return item.Versions.Count > 0;
        }
        public string GetTargetType(BaseItem item)
        {
             var lnk = (LinkField)item.Fields["Link"];
             return lnk.Target;
        }
        //private bool HasVisibleChildren(Item item)
        //{
        //    foreach (Item child in item.Children)
        //    {
        //        if (IsVisible(child))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}
    }
}