echo =========== Starting Post Build: %DATE% %TIME% ===========
set ProjectDir="%~1"

set TargetDir=%~dp0..\..\..\Sitecore\Website
for %%A in ("%~dp0") do (set ShortFileName=%%~sA)
%~dp0..\..\ChirpyConsole\Chirpy.exe "%ProjectDir:"=%App_Config\Include\Custom"
xcopy /d/s/y/i/EXCLUDE:%ShortFileName%ExcludeConfigs.txt "%ProjectDir:"=%App_Config" "%TargetDir%\App_Config"
xcopy /d/s/y/i "%ProjectDir:"=%bin" "%TargetDir%\bin"
xcopy /d/s/y/i/EXCLUDE:%ShortFileName%exclude.txt "%ProjectDir:"=%*" "%TargetDir%\GlobalWeb"
xcopy /d/s/y/i "%ProjectDir:"=%SitecoreCustom" "%TargetDir%\Sitecore"
echo ============ Ending Post Build: %DATE% %TIME% ============