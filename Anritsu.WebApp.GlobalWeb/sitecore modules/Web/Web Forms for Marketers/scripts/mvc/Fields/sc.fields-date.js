﻿if (typeof ($scw) === "undefined") {
    window.$scw = jQuery.noConflict(true);
}


$scw.widget("wffm.datebox", {
    options: {
        day: null,
        month: null,
        year: null
    },

    _create: function () {

        var self = this;
        var id = $scw(this.element).attr("id");
        var fieldId = id.slice(0, -5);

        var options = this.options;

        options.day = this.element.parent().children("#" + fieldId + "Day");
        options.month = this.element.parent().children("#" + fieldId + "Month");
        options.year = this.element.parent().children("#" + fieldId + "Year");

        options.day.bind("change", function (e) { self.updateDateField(e, this, id); });

        options.month.bind("change", function (e) { self.updateDateField(e, this, id); });

        options.year.bind("change", function (e) { self.updateDateField(e, this, id); });
    },

    updateDateField: function (e, element, targetHiddenElementId) {
        
        if ($scw(this.options.month).find('option').length > 0 && $scw(this.options.year).find('option').length > 0) {
            var days = this.getDays(this.options.month.val(), this.options.year.val());

            while ($scw(this.options.day).find('option').length > days) {
                var options = $scw(this.options.day).find('option');
                options[options.length -1].remove();
            }

            while ($scw(this.options.day).find('option').length < days) {
                this.options.day.append($scw('<option/>', {
                    value: $scw(this.options.day).children().length + 1,
                    text: $scw(this.options.day).children().length + 1
                }));
            }
        }

        this.updateIsoDate(targetHiddenElementId);
    },

    getDays: function (month, year) {
        return new Date(year, month, 0).getDate();
    },

    updateIsoDate: function (targetHiddenElementId) {
        var year = this.options.year.val();
        var month = this.options.month.val();
        var day = this.options.day.val();

        if (month.length == 1) {
            month = "0" + month;
        }
        if (day.length == 1) {
            day = "0" + day;
        }

        $scw("#" + targetHiddenElementId).val(year + month + day + "T000000");
    },

});


