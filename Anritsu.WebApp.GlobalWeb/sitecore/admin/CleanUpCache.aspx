﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="Sitecore.Analytics.Lookups" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title></title>
</head>
    <script runat="server">
        protected void CleanCache(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtbx1.Text))
            {
                Sitecore.Sites.SiteContext site = Sitecore.Configuration.Factory.GetSite(txtbx1.Text);
                var htmlCache = Sitecore.Caching.CacheManager.GetHtmlCache(site);

                if (htmlCache != null)
                { htmlCache.Clear(); }
                lblSize.Text = htmlCache != null ? htmlCache.InnerCache.Size.ToString() : "no such cache exists!";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtbx1.Text))
            {
                Sitecore.Sites.SiteContext site = Sitecore.Configuration.Factory.GetSite(txtbx1.Text);
                var htmlCache = Sitecore.Caching.CacheManager.GetHtmlCache(site);

                lblSize.Text = htmlCache != null ? htmlCache.InnerCache.Size.ToString() : "no such cache exists!";
            }
        }
    </script>

<body>
    <form runat="server">
        <asp:TextBox runat="server" Text="GlobalWeb" ID="txtbx1" /> <br />
        Html Cache size: <asp:Label runat="server" ID="lblSize" /><br />
        <asp:Button ID="btn" runat="server" OnClick="CleanCache"  Text="Clean this cache"/>
    </form>
</body>
</html>