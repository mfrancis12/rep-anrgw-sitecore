﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditLinks.aspx.cs" Inherits="Anritsu.WebApp.GlobalWeb.SitecoreCustom.Shell.Client.YourApps.EditLinks" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="/GlobalWeb/Resources/css/bootstrap.min.css" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="/GlobalWeb/Resources/js/bootstrap.min.js"></script>
    <style>
        .GridButton {
            background: none repeat scroll 0 0 gray;
            border-radius: 5px;
            color: #fff;
            font: 8pt tahoma;
            height: auto;
            margin: 10px;
            min-width: 75px;
            padding: 6px 30px;
            white-space: nowrap;
            width: 100px;
        }

            .GridButton:hover, .GridButton:focus {
                text-decoration: none;
                color: #ffffff;
            }
    </style>
    <title>Edit Links</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <asp:GridView ID="GridViewEditLinks" runat="server" AutoGenerateColumns="False" Width="100%">
                <Columns>
                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Name">
                        <ItemTemplate>
                            <asp:Image runat="server" ID="LinkImage" ImageUrl='<%# Eval("Icon") %>' Width="20px" />
                            <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                            <asp:HiddenField runat="server" ID="ProductId" Value='<%# Eval("Id") %>' />
                        </ItemTemplate>
                        <ItemStyle Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="40px" HeaderText="Language">
                        <ItemTemplate>
                            <asp:Label ID="lblLanguage" runat="server" Text='<%# Eval("Language") %>'>></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="40px" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="330px" HeaderText="Path">
                        <ItemTemplate>
                            <asp:Label ID="lblProductPath" runat="server" Text='<%# Eval("Path") %>'>></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="330px" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="30px" HeaderText="Version">
                        <ItemTemplate>
                            <asp:Label ID="lblItemVersion" runat="server" Text='<%# Eval("Version") %>'>></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="30px" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="50px" ItemStyle-Height="35px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRemove" runat="server" Text="Delete" CommandArgument='<%# Eval("Id") %>'
                                OnClick="LinkRemove_Click" CssClass="GridButton"></asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle Width="50px" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div style="text-align: center; margin-top: 10px" id="addButton" runat="server">
                <a href="#SelectProductModal" data-toggle="modal" class="GridButton">Add</a>
            </div>
            <div style="text-align: center; margin-top: 10px" runat="server">
                <asp:Label ID="LabelReferenceError" runat="server" Visible="false"></asp:Label>
            </div>
            <div id="SelectProductModal" runat="server" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="True">&times;</button>
                            <div style="text-align: center;">
                                <h4>Please select the product</h4>
                            </div>
                        </div>
                        <div style="text-align: center;">
                            <telerik:RadListBox runat="server" ID="RadListBoxSource" Height="200px" Width="300px"
                                AllowTransfer="true" TransferToID="RadListBoxDestination" EmptyMessage="No Products are available for the selected Language">
                            </telerik:RadListBox>
                            <telerik:RadListBox runat="server" ID="RadListBoxDestination" Height="200px" Width="270px" ButtonSettings-AreaWidth="35px">
                            </telerik:RadListBox>
                            <div style="text-align: center; margin-top: 5px">
                                <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
