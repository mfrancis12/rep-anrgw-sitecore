﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Links;
using System;
using System.Collections.ObjectModel;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.SecurityModel;
using Anritsu.WebApp.GlobalWeb.Constants;
using System.Globalization;
using Sitecore.Shell.Applications.Layouts.PageDesigner.Commands;
using Telerik.Web.UI;

namespace Anritsu.WebApp.GlobalWeb.SitecoreCustom.Shell.Client.YourApps
{
    public partial class EditLinks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            BindAssociatedItems();
            BindProducts();
        }

        private void BindProducts()
        {
            Sitecore.Context.SetActiveSite("shell");
            Database master = Sitecore.Context.ContentDatabase;
            Language language = LanguageManager.GetLanguage(Request.QueryString["la"]);
            Item refItem = master.GetItem(Request.QueryString["id"], language);

            var itemReferrers = (Globals.LinkDatabase.GetReferrers(refItem)
                .Where(x => x.SourceItemLanguage.Equals(language))
                .Where(itemLink => itemLink != null)
                .Select(itemLink => master.GetItem(itemLink.SourceItemID, language))).ToList();

            if (itemReferrers.Count >= 3)
            {
                addButton.Visible = false;
                return;
            }

            var leftListBoxDataSource = new List<Item>();

            foreach (Item item in master.GetItem(ItemIds.GlobalProduct, language)
                .Children.Where(item => item.Language.Equals(language)))
            {
                var resultItem = itemReferrers.FirstOrDefault(x => x.ID.Equals(item.ID));
                if (resultItem == null)
                {
                    leftListBoxDataSource.Add(item);
                }
            }

            RadListBoxSource.DataSource = leftListBoxDataSource;
            RadListBoxSource.DataTextField = "Name";
            RadListBoxSource.DataValueField = "Id";
            RadListBoxSource.DataBind();

            RadListBoxDestination.DataSource = itemReferrers;
            RadListBoxDestination.DataTextField = "Name";
            RadListBoxDestination.DataValueField = "Id";
            RadListBoxDestination.DataBind();
        }

        private void BindAssociatedItems()
        {
            Sitecore.Context.SetActiveSite("shell");
            Database master = Sitecore.Context.ContentDatabase;

            // the Item that is referred to
            Item refItem = master.GetItem(Request.QueryString["id"], LanguageManager.GetLanguage(Request.QueryString["la"]));
            Language language = LanguageManager.GetLanguage(Request.QueryString["la"]);

            Debug.Assert(GridViewEditLinks != null, "GridViewEditLinks != null");

            var referenceItems = GetLinkedItems(master, language, refItem);
            GridViewEditLinks.DataSource = referenceItems;
            GridViewEditLinks.DataBind();

            addButton.Visible = referenceItems.Count < 3;
        }

        protected Collection<LinksAssociated> GetLinkedItems(Database database, Language language, Item refItem)
        {
            // getting all linked Items that refer to the “refItem” Item
            ItemLink[] links = Globals.LinkDatabase.GetReferrers(refItem).Where(x => x.SourceItemLanguage.Equals(language)).ToArray();
            var result = new Collection<LinksAssociated>();

            foreach (var item in links.Where(link => link.SourceDatabaseName == database.Name)
                .Select(link => database.Items[link.SourceItemID, language])
                .Where(item => item != null))
            {
                var associatedLink = new LinksAssociated
                {
                    Id = item.ID.ToString(),
                    Icon = "/temp/IconCache/" + item.Appearance.Icon,
                    Language = item.Language.ToString(),
                    Path = item.Paths.FullPath,
                    Name = item.Name,
                    Version = item.Version.ToString()
                };
                result.Add(associatedLink);
            }
            return result;
        }

        protected void LinkRemove_Click(object sender, EventArgs e)
        {
            Database master = Factory.GetDatabase("master");
            var linkDelete = (LinkButton)sender;

            var product = master.GetItem(linkDelete.CommandArgument, LanguageManager.GetLanguage(Request.QueryString["la"]));

            using (new Sitecore.Security.Accounts.UserSwitcher(Sitecore.Context.User))
            {
                if (product.Access.CanWrite())
                {
                    product.Editing.BeginEdit();

                    MultilistField linksMultiList = product.Fields["Links"];
                    if (linksMultiList != null)
                    {
                        if (!string.IsNullOrEmpty(linksMultiList.Value))
                        {
                            foreach (var id in linksMultiList.TargetIDs)
                            {
                                if (id.ToString().Equals(Request.QueryString["id"]))
                                {
                                    linksMultiList.Remove(Request.QueryString["id"]);
                                }
                            }
                        }
                    }
                    product.Editing.EndEdit();
                }
                else
                {
                    LabelReferenceError.Visible = true;
                    LabelReferenceError.Text = Sitecore.Context.User.Name + "Doesn't have the edit permission for the following item: </br>" + product.Name;
                    return;
                }
            }

            BindAssociatedItems();
            BindProducts();
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            Database master = Factory.GetDatabase("master");
            Language language = LanguageManager.GetLanguage(Request.QueryString["la"]);
            Item refItem = master.GetItem(Request.QueryString["id"], language);
            string _referenceItems = string.Empty;

            foreach (RadListBoxItem item in RadListBoxDestination.Items)
            {
                using (new Sitecore.Security.Accounts.UserSwitcher(Sitecore.Context.User))
                {
                    Item selectedItem = master.GetItem(item.Value, language);

                    if (selectedItem != null && selectedItem.Access.CanWrite())
                    {
                        MapLinks(selectedItem, refItem, false);
                    }
                    else
                    {
                        _referenceItems += "<li>" + selectedItem.Name + "</li>";
                    }
                }
            }
            foreach (RadListBoxItem item in RadListBoxSource.Items)
            {
                using (new Sitecore.Security.Accounts.UserSwitcher(Sitecore.Context.User))
                {
                    Item selectedItem = master.GetItem(item.Value, language);

                    if (selectedItem != null && selectedItem.Access.CanWrite())
                    {
                        RemoveLinks(selectedItem, refItem, false);
                    }
                    else
                    {
                        _referenceItems += "<li>" + selectedItem.Name + "</li>";
                    }
                }
            }

            BindAssociatedItems();
            BindProducts();

            if (!string.IsNullOrEmpty(_referenceItems))
            {
                LabelReferenceError.Visible = true;
                LabelReferenceError.Text = "<div><b>" + Sitecore.Context.User.LocalName + "</b>" + ", Doesn't have the edit permission for the following items: " + _referenceItems + "</div>";
            }
        }

        ///  <summary>
        ///  Process Link Field
        ///  </summary>
        /// <param name="currentItem"></param>
        /// <param name="linkItem"></param>
        /// <param name="isLinkWithMedia"></param>
        private static void MapLinks(Item currentItem, Item linkItem, bool isLinkWithMedia)
        {
            using (new Sitecore.Security.Accounts.UserSwitcher(Sitecore.Context.User))
            {
                currentItem.Editing.BeginEdit();
                if (isLinkWithMedia)
                {
                    //Mapping newly created teaser to current item selected
                    //MultilistField teasersMultiList = currentItem.Fields["Teasers"];
                    //if (teasersMultiList != null)
                    //{

                    //    if (!string.IsNullOrEmpty(teasersMultiList.Value))
                    //    {
                    //        teasersMultiList.Value = teasersMultiList.Value + "|";
                    //    }
                    //    if (teasersMultiList.Value.Contains(linkItem.ID.ToString()))
                    //    {
                    //        return;
                    //    }
                    //    teasersMultiList.Value = teasersMultiList.Value + System.Convert.ToString(linkItem.ID, CultureInfo.InvariantCulture);

                    //}
                }
                else
                {
                    //Mapping newly created link to current item selected
                    MultilistField linksMultiList = currentItem.Fields["Links"];
                    if (linksMultiList != null)
                    {
                        if (!string.IsNullOrEmpty(linksMultiList.Value))
                        {
                            linksMultiList.Value = linksMultiList.Value + "|";
                        }

                        if (linksMultiList.Value.Contains(linkItem.ID.ToString()))
                        {
                            return;
                        }
                        linksMultiList.Value = linksMultiList.Value + System.Convert.ToString(linkItem.ID, CultureInfo.InvariantCulture);
                    }
                }
                currentItem.Editing.EndEdit();
            }
        }

        private static void RemoveLinks(Item currentItem, Item linkItem, bool isLinkWithMedia)
        {
            using (new SecurityDisabler())
            {
                currentItem.Editing.BeginEdit();
                if (isLinkWithMedia)
                {
                    //Mapping newly created teaser to current item selected
                    //MultilistField teasersMultiList = currentItem.Fields["Teasers"];
                    //if (teasersMultiList != null)
                    //{
                    //    if (!string.IsNullOrEmpty(teasersMultiList.Value) && teasersMultiList.Value.Contains(linkItem.ID.ToString()))
                    //    {
                    //        teasersMultiList.Remove(linkItem.ID.ToString());
                    //    }
                    //}
                }
                else
                {
                    //Mapping newly created link to current item selected
                    MultilistField linksMultiList = currentItem.Fields["Links"];
                    if (linksMultiList != null)
                    {
                        if (!string.IsNullOrEmpty(linksMultiList.Value) && linksMultiList.Value.Contains(linkItem.ID.ToString()))
                        {
                            linksMultiList.Remove(linkItem.ID.ToString());
                        }
                    }
                }

                currentItem.Editing.EndEdit();
            }
        }
    }

    public class LinksAssociated
    {
        public string Id { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public string Path { get; set; }
        public string Version { get; set; }
    }
}