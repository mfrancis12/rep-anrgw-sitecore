﻿using System.Web;

namespace Anritsu.WebApp.GlobalWeb.SitecoreCustom.Shell.Client.YourApps.Properties
{
    /// <summary>
    /// Handler to get item details
    /// </summary>
    public class GetItemDetails : IHttpHandler
    {
        /// <summary>
        /// IsReusable Property
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Handler Process Request
        /// </summary>
        ///<param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            var itemPath = string.Empty;
            if (context.Request.Params["ID"] != null)
            {
                var db = Sitecore.Configuration.Factory.GetDatabase("master");

                var contextItem = context.Request.Params["ID"];
                var selectedItem = db.GetItem(contextItem);
                itemPath = selectedItem.Paths.ContentPath;
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write(itemPath);
        }
    }
}