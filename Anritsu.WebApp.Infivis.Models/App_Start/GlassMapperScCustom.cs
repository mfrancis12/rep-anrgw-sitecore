using Castle.Windsor;
using Glass.Mapper.Configuration;
using Glass.Mapper.Sc.CastleWindsor;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Configuration.Fluent;
using Anritsu.WebApp.Infivis.Models.Components;
using Glass.Mapper.Sc.Configuration;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Castle.MicroKernel.Registration;
using Glass.Mapper.Pipelines.ObjectConstruction;
using Glass.Mapper;
using Anritsu.WebApp.GlobalWeb.Models.FieldMappers;


namespace Anritsu.WebApp.Infivis.Models.App_Start
{
    public static class GlassMapperScCustom
    {
        public static void CastleConfig(IWindsorContainer container)
        {
            var config = new Config();

            container.Register(
                 Component.For<IObjectConstructionTask>().ImplementedBy<FallbackCheckTask>().LifestyleTransient()
                //Component.For<IObjectConstructionTask>().ImplementedBy<SearchProxyWrapperTask>().LifestyleTransient(),
            );
            container.Register(Component.For<AbstractDataMapper>().ImplementedBy<LinkListDataHandler>().LifestyleTransient());
            container.Install(new SitecoreInstaller(config));
        }

        public static IConfigurationLoader[] GlassLoaders()
        {
            return null;
        }
        public static void PostLoad()
        {
            //Remove the comments to activate CodeFist
            /* CODE FIRST START
            var dbs = Sitecore.Configuration.Factory.GetDatabases();
            foreach (var db in dbs)
            {
                var provider = db.GetDataProviders().FirstOrDefault(x => x is GlassDataProvider) as GlassDataProvider;
                if (provider != null)
                {
                    using (new SecurityDisabler())
                    {
                        provider.Initialise(db);
                    }
                }
            }
             * CODE FIRST END
             */
        }
    }
}
