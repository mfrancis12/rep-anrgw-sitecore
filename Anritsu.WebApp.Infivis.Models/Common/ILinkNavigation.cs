﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Common
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Infivis/Common/Link Navigation
    /// Template ID : {F5BA69FB-7599-4DAE-8818-274905E44BF7}
    /// </summary>
    [SitecoreType(TemplateId = "{F5BA69FB-7599-4DAE-8818-274905E44BF7}")]
    public partial interface ILinkNavigation : IModelBase
    {

        /// <summary>
        /// Gets the url of the navigation item 
        /// </summary>
        [SitecoreField("Navigation Url")]
        Link NavigationUrl { get; set; }

        /// <summary>
        /// Gets the status of this item
        /// </summary>
        [SitecoreField("Is Disabled")]
        bool IsDisabled  { get; set; }
    }
}
