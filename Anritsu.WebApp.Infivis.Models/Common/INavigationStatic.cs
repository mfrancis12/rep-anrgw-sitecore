﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Common
{
     [SitecoreType(TemplateId = "{782D0E54-3A77-460E-843C-019494824532}")]
    public partial interface INavigationStatic : IModelBase,INavigation
    {
        [SitecoreField]
        IEnumerable<IModelBase> SideNavigationItems { get; set; }
    }
}
