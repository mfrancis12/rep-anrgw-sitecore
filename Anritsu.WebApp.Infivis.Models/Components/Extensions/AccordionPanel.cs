﻿namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class AccordionPanel
    {
        public static string GetPanelClass(this IAccordionPanel panel)
        {
            return panel.OpenByDefault ? "open" : string.Empty;
        }

        public static string GetPanelIconClass(this IAccordionPanel panel)
        {
            return panel.OpenByDefault ? "icon-minus-grey" : "icon-plus-grey";
        }
    }
}
