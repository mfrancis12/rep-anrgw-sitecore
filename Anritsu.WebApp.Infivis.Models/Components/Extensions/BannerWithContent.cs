﻿namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class BannerWithContent
    {
        public static string GetTextColor(this IBannerWithContent banner)
        {
            return banner.TextColor == null ? string.Empty : banner.TextColor.Key;
        }
    }
}
