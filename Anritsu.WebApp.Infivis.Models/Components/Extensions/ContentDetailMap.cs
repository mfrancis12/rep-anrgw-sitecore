﻿using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.Infivis.Models.Components.Extensions
{
     public static class ContentDetailMap
    {
         public static string GetGlobalImagePath(this IAccordionStatic globalMap)
         {
             if (globalMap.Content != null && globalMap.Content.BannerImage != null && !string.IsNullOrEmpty(globalMap.Content.BannerImage.Src))
             {
                 return globalMap.Content.BannerImage.Src;
             }
             else
             {
                 return string.Empty;
             }
         }
    }
}
