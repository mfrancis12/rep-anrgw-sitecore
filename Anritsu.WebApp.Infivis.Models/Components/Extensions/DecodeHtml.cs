﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class DecodeHtml
    {
        public static string GetHtmlDecodedString(this string inputText)
        {
            return HttpUtility.HtmlDecode(inputText);
        }
    }
}
