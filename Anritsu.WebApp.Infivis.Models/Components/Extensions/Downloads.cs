﻿using System;
using System.Configuration;
using System.Web;
using Anritsu.WebApp.GlobalWeb.Models.App_Start;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc;
using Sitecore.Configuration;
using System.Globalization;

namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class Downloads
    {
        public static string GetReplacementItemUrl(this DownloadSearchItem download)
        {
            if (download != null)
            {
                var replacementLink = download.ReplacementDownloadPath;

                if (replacementLink == null)
                    return string.Empty;

                var context = new SitecoreContext();
                var replacementDownloadId = new Guid(replacementLink.TargetId.ToString());
                var replacementDownloadItem = context.GetItem<DownloadSearchItem>(replacementDownloadId.ToString());
                return replacementDownloadItem == null ? string.Empty : GetItemUrl(replacementDownloadItem);
            }
            return string.Empty;
        }

        public static string GetItemUrl(this DownloadSearchItem download)
        {
            if (download == null)
                return string.Empty;

            // Adding this for WWW-2017 fix, this will return software detail page url instead of myanritsu url for software downloads
            if (download.Subcategory != null && download.Subcategory.Key.Equals("drivers-software-downloads"))
            {
                var position = download.Url.LastIndexOf("/", StringComparison.Ordinal) + 1;
                return
                    string.Format(
                        "/" + Sitecore.Context.Language.Name + "/test-measurement/support/downloads/software/{0}",
                        download.Url.Substring(position, download.Url.Length - position));
            }

            var downloadFilePath = string.Empty;
            if (download.RedirectToMyAnritsu)
            {
                downloadFilePath = Settings.GetSetting("RedirectToMyAnritsu");
            }
            else if (download.Subcategory != null)
            {
                var position = download.Url.LastIndexOf("/", StringComparison.Ordinal) + 1;
                var categoryItem = Sitecore.Context.Database.GetItem(download.Subcategory.Id.ToString("B").ToUpper(CultureInfo.InvariantCulture)).Parent;

                return
                    string.Format(
                        "/" + Sitecore.Context.Language.Name + "/test-measurement/support/downloads/{0}/{1}",
                      categoryItem != null ?
                                    FallbackCheckTask.GetSitecoreFallbackItem(categoryItem, categoryItem.Fields["Key"]).Fields["Key"].Value : "download-category",
                        download.Url.Substring(position, download.Url.Length - position));
            }

            return downloadFilePath;

            //if (download.Subcategory != null && download.Subcategory.Key.Equals("drivers-software-downloads"))
            //{
            //    var position = download.Url.LastIndexOf("/", StringComparison.Ordinal) + 1;
            //    return
            //        string.Format(
            //            "/" + Sitecore.Context.Language.Name + "/test-measurement/support/downloads/software/{0}",
            //            download.Url.Substring(position, download.Url.Length - position));
            //}


            //string downloadFilePath;
            //var bucketPath = IsExternalUrl(download.FilePath)
            //    ? string.Empty : string.Format("http://{0}", ConfigurationManager.AppSettings["DownloadDistributionList"]);

            //if (download.RedirectToMyAnritsu)
            //{
            //    downloadFilePath = Settings.GetSetting("RedirectToMyAnritsu");
            //}
            //else if (download.LogOnRequired)
            //{
            //    if (HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.Session["User"] != null)
            //    {
            //        downloadFilePath = string.IsNullOrEmpty(bucketPath) ? download.FilePath : bucketPath + Sitecore.StringUtil.EnsurePrefix('/', download.FilePath);
            //    }
            //    else
            //    {
            //        downloadFilePath = string.Format("/Sign-In.aspx?ReturnURL={0}", string.IsNullOrEmpty(bucketPath)
            //            ? download.FilePath : bucketPath + Sitecore.StringUtil.EnsurePrefix('/', download.FilePath));
            //    }
            //}
            //else
            //    downloadFilePath = string.IsNullOrEmpty(bucketPath) ? download.FilePath : bucketPath + Sitecore.StringUtil.EnsurePrefix('/', download.FilePath);

            //return downloadFilePath;
        }

        public static string GetReplacementDownloadId(this DownloadSearchItem download)
        {
            if (download == null || download.ReplacementDownloadPath == null) return string.Empty;
            if (download.IsNewVersionAvailable &&
                !string.IsNullOrEmpty(Convert.ToString(download.ReplacementDownloadPath.TargetId)))
            {
                return download.ReplacementDownloadPath.TargetId.ToString();
            }
            return string.Empty;
        }

        public static bool IsExternalUrl(string source)
        {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
        }

        public static string GetDownloadCategory(this DownloadSearchItem download)
        {
            if (download == null || download.Subcategory == null) return string.Empty;
            return download.Subcategory.Value;
        }
    }
}
