﻿using System;

namespace Anritsu.WebApp.GlobalWeb.Models.Components.Extensions
{
    public static class Events
    {
        public static string GetCommaPrefixStartTime(this IEvents field)
        {            
              return  string.IsNullOrEmpty(field.StartTime)?string.Empty: " ," + field.StartTime;
        }

        public static bool IsExternalUrl(this IEvents source)
        {
            if (source.ExternalLandingLink == null || string.IsNullOrEmpty(source.ExternalLandingLink.Url)) return false;

            Uri uriResult;
            return Uri.TryCreate(source.ExternalLandingLink.Url, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }
    }
}
