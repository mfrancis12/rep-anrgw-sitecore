﻿using Anritsu.WebApp.Infivis.Models.Global;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.Infivis.Models.Components.Extensions
{
    public static class ProductGlobal
    {
        public static string GetEcoLink(this IProductGlobalStatic product)
        {
            if (product != null && product.EcoLink != null && !string.IsNullOrEmpty(product.EcoLink.Url))
            {
                return product.EcoLink.Url;
            }
            return "javascript:void(0);";
        }

        public static string GetEcoLinkTarget(this IProductGlobalStatic product)
        {
            if (product != null && product.EcoLink != null && !string.IsNullOrEmpty(product.EcoLink.Url))
            {
                return product.EcoLink.Target;
            }
            return string.Empty;
        }
    }
}
