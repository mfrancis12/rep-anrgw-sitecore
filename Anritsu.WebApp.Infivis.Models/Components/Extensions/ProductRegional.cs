﻿using Anritsu.WebApp.Infivis.Models.Pages;

namespace Anritsu.WebApp.Infivis.Models.Components.Extensions
{
  public static class ProductRegional
  {
    public static string GetProductStatus(this IProductDetailStatic product)
    {
      if (product != null && !product.SelectProduct.IsDiscontinued)
      {
        if (product.SelectProduct.Status != null && product.SelectProduct.Status.Image != null && !string.IsNullOrEmpty(product.SelectProduct.Status.Image.Src))
        {
          return product.SelectProduct.Status.Image.Src;
        }
      }
      return string.Empty;
    }
    public static string GetProductStatus(this IProductDetailStaticC1 product)
    {
      if (product != null && !product.SelectProduct.IsDiscontinued)
      {
        if (product.SelectProduct.Status != null && product.SelectProduct.Status.Image != null && !string.IsNullOrEmpty(product.SelectProduct.Status.Image.Src))
        {
          return product.SelectProduct.Status.Image.Src;
        }
      }
      return string.Empty;
    }

  }
}
