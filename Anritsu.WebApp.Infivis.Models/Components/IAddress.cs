﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/Address
  /// Template ID : {337A7927-78F9-4882-A22E-024ED6078A34}
  /// </summary>
  [SitecoreType(TemplateId = "{337A7927-78F9-4882-A22E-024ED6078A34}")]
  public partial interface IAddress : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. Description: This will be displayed as company name.. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the Rich Text Details. 
    /// </summary>
	[SitecoreField]
    string Details { get; set; }

    /// <summary>
    /// Gets the Multilist SelectCountry. Description: This is used for selecting country the address corresponds to. Datasource: Datasource={CBE79E28-2912-4127-BE67-D469825EA027}
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> SelectCountry { get; set; }

  }
}
