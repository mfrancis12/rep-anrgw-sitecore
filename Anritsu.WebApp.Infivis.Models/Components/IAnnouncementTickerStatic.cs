﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {3D0BE06D-1A9D-4330-8341-DACA0E9092A1}
    /// </summary>
    [SitecoreType(TemplateId = "{3D0BE06D-1A9D-4330-8341-DACA0E9092A1}")]
  public partial interface IAnnouncementTickerStatic : IModelBase 
  {

        /// <summary>
        /// Gets the Multilist Promos. Datasource: /sitecore/content/GlobalWeb/components/Infivis/toolbar
        /// </summary>
        [SitecoreField]
        IEnumerable<IAnnouncement> Announcements { get; set; }

    }
}
