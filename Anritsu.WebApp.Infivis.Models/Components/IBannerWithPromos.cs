﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/BannerWithPromos
  /// Template ID : {55EB8659-8FB9-4989-BC67-088A1EDBE977}
  /// </summary>
  [SitecoreType(TemplateId = "{55EB8659-8FB9-4989-BC67-088A1EDBE977}")]
  public partial interface IBannerWithPromos : IModelBase 
  {  

  }
}
