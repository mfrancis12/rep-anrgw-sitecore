﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/CorporateAddress
  /// Template ID : {ED9A97A7-85A8-4C29-8EF0-F71B73A22784}
  /// </summary>
  [SitecoreType(TemplateId = "{ED9A97A7-85A8-4C29-8EF0-F71B73A22784}")]
  public partial interface ICorporateAddress : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. Description: This will be displayed as company name.. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the General Link TitleLink. 
    /// </summary>
	[SitecoreField]
    Link TitleLink { get; set; }

    /// <summary>
    /// Gets the Multi-Line Text ShortDescription. 
    /// </summary>
	[SitecoreField]
    string ShortDescription { get; set; }

    /// <summary>
    /// Gets the Rich Text Details. Description: This will be used to describe the company.. 
    /// </summary>
	[SitecoreField]
    string Details { get; set; }

  }
}
