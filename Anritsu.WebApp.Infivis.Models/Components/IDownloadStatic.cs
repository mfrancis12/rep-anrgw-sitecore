﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {B6B7BED0-6FE0-4F2D-9F46-6C3224B9AE1A}
    /// </summary>
    [SitecoreType(TemplateId = "{B6B7BED0-6FE0-4F2D-9F46-6C3224B9AE1A}")]
  public partial interface IDownloadStatic : IModelBase 
  {
        [SitecoreField]
        string Name { get; set; }

        [SitecoreField]
        Glass.Mapper.Sc.Fields.Link DownloadLink { get; set; }

        [SitecoreField]
        DateTime ReleaseDate { get; set; }
        

    }
}
