﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {128C112F-2BEC-4493-A9B8-D01BEFA597DC}
    /// </summary>
    [SitecoreType(TemplateId = "{128C112F-2BEC-4493-A9B8-D01BEFA597DC}")]
  public partial interface IDownloadTypeStatic : IModelBase 
  {
        [SitecoreField]
        string Name { get; set; }


    }
}
