﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {1C2DCE10-07AF-410B-9D6D-4D031043E73A}
    /// </summary>
    [SitecoreType(TemplateId = "{1C2DCE10-07AF-410B-9D6D-4D031043E73A}")]
  public partial interface IDropListStatic : IModelBase 
  {

        /// <summary>
        /// Gets the Multilist Promos. Datasource: /sitecore/content/GlobalWeb/components/Infivis/toolbar
        /// </summary>
        [SitecoreField]
        IEnumerable<IDropListItem> DropListItems { get; set; }

    }
}
