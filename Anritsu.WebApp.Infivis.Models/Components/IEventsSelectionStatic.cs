﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Template ID : {13D24DCA-263D-48BF-A992-7800D9E8FAE0}
    /// </summary>
    [SitecoreType(TemplateId = "{13D24DCA-263D-48BF-A992-7800D9E8FAE0}")]
  public partial interface IEventsSelectionStatic : IModelBase 
  {

        /// <summary>
        /// </summary>
        [SitecoreField]
        IEnumerable<IEvents> EventItems { get; set; }

    }
}
