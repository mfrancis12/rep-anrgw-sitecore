﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Global;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/IconLinkWithTitle
    /// Template ID : {CFCE4401-8991-4FB0-A2EA-48A94AFE535E}
    /// </summary>
    [SitecoreType(TemplateId = "{CFCE4401-8991-4FB0-A2EA-48A94AFE535E}")]
  public partial interface IIconLinkWithTitleStatic : IIconLinkWithTitle// IModelBase 
    {  

        [SitecoreField]
        Image IconImg { get; set; }

  }
}
