﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;


namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/ImageLinkWithDescription
    /// Template ID : {8820C2AF-5662-492E-82CC-ECD51BB01EAC}
    /// </summary>
    [SitecoreType(TemplateId = "{8820C2AF-5662-492E-82CC-ECD51BB01EAC}")]
  public partial interface IImageLinkWithDescriptionStatic : IModelBase, IPromo 
  {  

    
    /// <summary>
    /// Gets the Droplink TextAlign. Datasource: /sitecore/content/GlobalWeb/components/TextAlign
    /// </summary>
	[SitecoreField]
    Guid TextAlign { get; set; }

    /// <summary>
    /// Gets the Droplink ButtonAlign. Datasource: /sitecore/content/GlobalWeb/components/ButtonAlign
    /// </summary>
	[SitecoreField]
    Guid ButtonAlign { get; set; }

    /// <summary>
    /// Gets the Multi-Line Text Description. 
    /// </summary>
	[SitecoreField]
    string Description { get; set; }

        //   /// <summary>
        //   /// Gets the Droplink TextColor. Datasource: /sitecore/content/GlobalWeb/components/TextColor
        //   /// </summary>
        //[SitecoreField]
        //   Guid TextColor { get; set; }

        /// <summary>
        /// Gets the Droplink TextColor. Datasource: /sitecore/content/GlobalWeb/components/TextColor
        /// </summary>
        [SitecoreField]
        IDropListItem TextColor { get; set; }

    }
}
