﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {5B47EDEB-642F-4F49-8FAC-6022614B03F3}
    /// </summary>
    [SitecoreType(TemplateId = "{5B47EDEB-642F-4F49-8FAC-6022614B03F3}")]
  public partial interface ILibraryStatic : IModelBase 
  {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        string Description { get; set; }

        /// <summary>
        /// Gets the Multilist Promos. Datasource: /sitecore/content/GlobalWeb/components/Infivis/toolbar
        /// </summary>
        [SitecoreField]
        IEnumerable<IDownloadTypeStatic> Downloads { get; set; }


    }
}
