﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {C1C16062-8B51-4EFB-A5DF-95ECBFA8A8E3}
    /// </summary>
    [SitecoreType(TemplateId = "{C1C16062-8B51-4EFB-A5DF-95ECBFA8A8E3}")]
  public partial interface ILinkManagerPromotionsStatic : IModelBase 
  {

        /// <summary>
        /// Gets the Multilist Promos. Datasource: /sitecore/content/GlobalWeb/components/Infivis/toolbar
        /// </summary>
        [SitecoreField]
        string LinkPromotions { get; set; }

    }
}
