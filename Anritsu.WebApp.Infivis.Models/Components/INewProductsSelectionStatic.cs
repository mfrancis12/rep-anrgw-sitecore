﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Template ID : {CACCF03A-6D7C-40BF-89E0-15F9F8D0879C}
    /// </summary>
    [SitecoreType(TemplateId = "{CACCF03A-6D7C-40BF-89E0-15F9F8D0879C}")]
  public partial interface INewProductsSelectionStatic : IModelBase 
  {

        /// <summary>
        /// </summary>
        [SitecoreField]
        IEnumerable<Anritsu.WebApp.Infivis.Models.Pages.IProductDetailStatic> NewProducts { get; set; }

    }
}
