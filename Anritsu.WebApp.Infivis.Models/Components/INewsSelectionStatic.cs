﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Template ID : {1858B45E-9659-43F5-B61A-F0FCBF2AFFFD}
    /// </summary>
    [SitecoreType(TemplateId = "{1858B45E-9659-43F5-B61A-F0FCBF2AFFFD}")]
  public partial interface INewsSelectionStatic : IModelBase 
  {

        /// <summary>
        /// </summary>
        [SitecoreField]
        IEnumerable<INewsRelease> NewsItems { get; set; }

    }
}
