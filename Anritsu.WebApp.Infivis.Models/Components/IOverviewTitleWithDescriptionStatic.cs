﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {82FAA05C-BB76-4CDC-9989-4498752D4F43}
    /// </summary>
    [SitecoreType(TemplateId = "{82FAA05C-BB76-4CDC-9989-4498752D4F43}")]
  public partial interface IOverviewTitleWithDescriptionStatic : ITitleWithDescription 
  {

        [SitecoreField]
        string FeaturesTitle { get; set; }

        [SitecoreField]
        string FeaturesDescription { get; set; }

        [SitecoreField]
        string MainDescriptionTitle { get; set; }

        [SitecoreField]
        string MainDescription { get; set; }



    }
}
