﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {12BF13B6-31FC-4612-AEEB-A8986457D6B8}
    /// </summary>
    [SitecoreType(TemplateId = "{12BF13B6-31FC-4612-AEEB-A8986457D6B8}")]
  public partial interface IProductDetailIcon : IModelBase 
  {

        /// <summary>
        /// Gets the Multilist Promos. Datasource: /sitecore/content/GlobalWeb/components/Infivis/toolbar
        /// </summary>
        [SitecoreField]
        Image IconImage { get; set; }

        [SitecoreField]
        Link IconLink { get; set; }

    }
}
