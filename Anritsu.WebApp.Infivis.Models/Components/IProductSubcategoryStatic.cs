﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace Anritsu.WebApp.Infivis.Models.Components
{
  /// <summary>
  /// Template ID : {1858B45E-9659-43F5-B61A-F0FCBF2AFFFD}
  /// </summary>
  [SitecoreType(TemplateId = "{50828032-8D60-4383-B920-F341C8AAE2FF}")]
  public interface IProductSubcategoryStatic : GlobalWeb.Models.IModelBase
  {

    /// <summary>
    /// Gets the Multi-Line Text Description. 
    /// </summary>
    [SitecoreField]
    string ProductSubcategoryDescription { get; set; }

    [SitecoreField]
    string ProductSubcategoryLinks { get; set; }

    /// <summary>
    /// Gets the Image Image. 
    /// </summary>
    [SitecoreField]
    Image ProductSubcategoryImage { get; set; }

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
    [SitecoreField]
    string ProductSubcategoryTitle { get; set; }

  }
}
