﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/RelatedCategories
  /// Template ID : {7C2094D2-ED72-4539-BE0D-EAAA44E012F3}
  /// </summary>
  [SitecoreType(TemplateId = "{7C2094D2-ED72-4539-BE0D-EAAA44E012F3}")]
  public partial interface IRelatedCategories : IModelBase 
  {  

    /// <summary>
    /// Gets the Treelist RelatedProducts. 
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> RelatedProducts { get; set; }

    /// <summary>
    /// Gets the Treelist RelatedProductFamilies. 
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> RelatedProductFamilies { get; set; }

    /// <summary>
    /// Gets the Treelist RelatedTechnologies. 
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> RelatedTechnologies { get; set; }

    /// <summary>
    /// Gets the Treelist RelatedIndustries. 
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> RelatedIndustries { get; set; }

    /// <summary>
    /// Gets the Treelist RelatedForums. 
    /// </summary>
	[SitecoreField]
    IEnumerable<IModelBase> RelatedForums { get; set; }

  }
}
