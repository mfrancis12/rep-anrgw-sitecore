﻿using Anritsu.WebApp.Infivis.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    [SitecoreType(TemplateId = "{24F1B869-C9E7-48EB-80F0-173412158016}")]
    public partial interface IStaticPageForFreeform : IModelBase, IBaseTemplate
    {
      
    }
}
