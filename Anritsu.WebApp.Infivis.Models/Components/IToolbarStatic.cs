﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {DDC5C412-AEC7-4FE4-8499-3196BE197E88}
    /// </summary>
    [SitecoreType(TemplateId = "{DDC5C412-AEC7-4FE4-8499-3196BE197E88}")]
  public partial interface IToolbarStatic : IModelBase 
  {

        /// <summary>
        /// Gets the Multilist Promos. Datasource: /sitecore/content/GlobalWeb/components/Infivis/toolbar
        /// </summary>
        [SitecoreField]
        IEnumerable<IIconLinkWithTitleStatic> Toolbar { get; set; }

        [SitecoreField]
        bool ShowToolbar { get; set; }

        [SitecoreField]
        bool ShowToolbarShareIcons { get; set; }

    }
}
