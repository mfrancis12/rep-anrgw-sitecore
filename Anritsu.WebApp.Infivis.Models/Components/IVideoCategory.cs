﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Components
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/TitleWithDescription
    /// Template ID : {10FFF13E-97CE-4266-9CCE-3E5E85CB0B90}
    /// </summary>
    [SitecoreType(TemplateId = "{10FFF13E-97CE-4266-9CCE-3E5E85CB0B90}")]
  public partial interface IVideoCategory : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string CategoryTitle { get; set; }

  }
}
