﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Components
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Components/YearFolder
  /// Template ID : {3CAF38BE-A8DA-4073-884E-1CDB1F4A5F0E}
  /// </summary>
  [SitecoreType(TemplateId = "{3CAF38BE-A8DA-4073-884E-1CDB1F4A5F0E}")]
  public partial interface IYearFolder : IModelBase 
  {  

  }
}
