﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace Anritsu.WebApp.GlobalWeb.Models.Global
{
  /// <summary>
  /// This is a auto generated file, do not make any changes to this file. Create a partial interface to modify this file.
  /// Code representation of item based on Sitecore template /sitecore/templates/Global/IRNews
  /// Template ID : {6CC09862-D66B-4C17-AB76-2F425126A66E}
  /// </summary>
  [SitecoreType]
  public partial interface IIRNews : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Title. 
    /// </summary>
	[SitecoreField]
    string Title { get; set; }

    /// <summary>
    /// Gets the General Link Link. 
    /// </summary>
	[SitecoreField]
    Guid Link { get; set; }

    /// <summary>
    /// Gets the Date Date. 
    /// </summary>
	[SitecoreField]
    DateTime Date { get; set; }

  }
}
