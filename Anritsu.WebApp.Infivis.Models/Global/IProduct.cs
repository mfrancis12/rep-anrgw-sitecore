﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace Anritsu.WebApp.GlobalWeb.Models.Global
{
  /// <summary>
  /// This is a auto generated file, do not make any changes to this file. Create a partial interface to modify this file.
  /// Code representation of item based on Sitecore template /sitecore/templates/Global/Product
  /// Template ID : {4AE61E33-0AB8-4C38-9414-4CB0726DC1B7}
  /// </summary>
  [SitecoreType]
  public partial interface IProduct : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text Product Name. 
    /// </summary>
	[SitecoreField]
    string Product_Name { get; set; }

    /// <summary>
    /// Gets the Image Image. 
    /// </summary>
	[SitecoreField]
    Image Image { get; set; }

    /// <summary>
    /// Gets the Rich Text Features. 
    /// </summary>
	[SitecoreField]
    string Features { get; set; }

    /// <summary>
    /// Gets the Rich Text Description. 
    /// </summary>
	[SitecoreField]
    string Description { get; set; }

    /// <summary>
    /// Gets the Multilist Teasers. Datasource: /sitecore/content/GlobalWeb/components/teasers
    /// </summary>
	[SitecoreField]
    IEnumerable<Guid> Teasers { get; set; }

    /// <summary>
    /// Gets the Multilist Links. Datasource: /sitecore/content/GlobalWeb/components/links
    /// </summary>
	[SitecoreField]
    IEnumerable<Guid> Links { get; set; }

  }
}
