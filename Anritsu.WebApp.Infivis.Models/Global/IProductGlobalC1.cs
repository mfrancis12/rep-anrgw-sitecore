﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace Anritsu.WebApp.Infivis.Models.Global
{
  /// <summary>
  /// Code representation of item based on Sitecore template /sitecore/templates/Global/ProductGlobal
  /// Template ID : {94757055-E1B4-4B0D-B0E7-7BDBC26861D8}
  /// </summary>
  [SitecoreType(TemplateId = "{94757055-E1B4-4B0D-B0E7-7BDBC26861D8}")]
  public partial interface IProductGlobalC1 : IModelBase 
  {  

    /// <summary>
    /// Gets the Single-Line Text ProductName. 
    /// </summary>
	[SitecoreField]
    string ProductName { get; set; }

    /// <summary>
    /// Gets the Single-Line Text ModelNumber. 
    /// </summary>
	[SitecoreField]
    string ModelNumber { get; set; }

    /// <summary>
    /// Gets the Image Thumbnail. 
    /// </summary>
	[SitecoreField]
    Image Thumbnail { get; set; }

    /// <summary>
    /// Gets the Image Image. 
    /// </summary>
	[SitecoreField]
    Image Image { get; set; }

    /// <summary>
    /// Gets the Multilist AdditionalImages. Datasource: /sitecore/media library/globalweb/images/products
    /// </summary>
	[SitecoreField]
    IEnumerable<Image> AdditionalImages { get; set; }

    /// <summary>
    /// Gets the Multi-Line Text ShortDescription. 
    /// </summary>
    [SitecoreField]
    string ShortDescription { get; set; }

    /// <summary>
    /// Gets the Single-Line Text ProductDescriptor. 
    /// </summary>
    [SitecoreField]
    string ProductDescriptor { get; set; }

    /// <summary>
    /// Gets the Date ReleaseDate. 
    /// </summary>
    [SitecoreField]
    DateTime ReleaseDate { get; set; }

    /// <summary>
    /// Gets the Checkbox IsDiscontinued. 
    /// </summary>
    [SitecoreField]
    bool IsDiscontinued { get; set; }

    /// <summary>
    /// Gets the Droplink Status. Datasource: /sitecore/content/GlobalWeb/components/Status
    /// </summary>
    [SitecoreField]
    IImageWithTitle Status { get; set; }

    /// <remarks>
    /// There is no specific template for the field type Name Lookup Value List Ex. This is a fallback field type template. 
    /// </remarks>
    /// <summary>
    /// Gets the Name Lookup Value List Ex Specifications. Datasource: /sitecore/content/GlobalWeb/components/Specifications
    /// </summary>
    [SitecoreField]
    NameValueCollection Specifications { get; set; }

    /// <summary>
    /// Gets the Checkbox ShowEcoIcon. 
    /// </summary>
    [SitecoreField]
    bool ShowEcoIcon { get; set; }

    /// <summary>
    /// Gets the Checkbox HideInRequestProductQuote. 
    /// </summary>
    [SitecoreField]
    bool HideInRequestProductQuote { get; set; }

    /// <summary>
    /// Gets the Treelist ProductFamily. Datasource: Datasource=/sitecore/content/GlobalWeb/home&IncludeTemplatesForSelection=ProductFamily
    /// </summary>
    [SitecoreField]
    IEnumerable<IModelBase> ProductFamily { get; set; }

    /// <summary>
    /// Gets the General Link ReplacementProduct. 
    /// </summary>
    [SitecoreField]
    IEnumerable<IProductDetailStaticC1> ReplacementProducts { get; set; }

    /// <summary>
    /// Gets the Multilist RelatedTechnologies. 
    /// </summary>
    [SitecoreField]
    IEnumerable<IAccordionplusgridStatic> RelatedTechnologies { get; set; }

    /// <summary>
    /// Gets the Checkbox ShowRequestQuote. 
    /// </summary>
    [SitecoreField]
    bool ShowRequestQuote { get; set; }

    /// <summary>
    /// Gets the Checkbox ShowRequestDemo. 
    /// </summary>
    [SitecoreField]
    bool ShowRequestDemo { get; set; }

    /// <summary>
    /// Gets the Checkbox ShowDownloads. 
    /// </summary>
    [SitecoreField]
    bool ShowDownloads { get; set; }

    /// <summary>
    /// Gets the Checkbox ShowFaQs. 
    /// </summary>
    [SitecoreField]
    bool ShowFaQs { get; set; }

    /// <summary>
    /// Gets the Checkbox ShowAdditionalButton. 
    /// </summary>
    [SitecoreField]
    bool ShowAdditionalHeroButton { get; set; }

    // <summary>
    /// Gets the General Link Link. 
    /// </summary>
    [SitecoreField]
    Link AdditionalHeroButtonLink { get; set; }

    /// <summary>
    /// Gets the Image EcoIcon. 
    /// </summary>
    [SitecoreField]
    Image EcoIcon { get; set; }

    // <summary>
    /// Gets the General Link EcoLink. 
    /// </summary>
    [SitecoreField]
    Link EcoLink { get; set; }

  }
}
