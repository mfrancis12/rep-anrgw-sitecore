// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

//Auto generated file
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "namespace", Target = "Anritsu.WebApp.Infivis.Models.App_Start")]
//Auto genrated file and represents Sitecore folder structure
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Global", Scope = "namespace", Target = "Anritsu.WebApp.Infivis.Models.Global")]
//Auto genrated file
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "args", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.App_Start.GlassMapperSc.#Process(Sitecore.Pipelines.PipelineArgs)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.MediaFramework.Brightcove.IBrightcoveVideo.#LinkUrl")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.MediaFramework.Brightcove.IBrightcoveVideo.#VideoStillURL")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "URL", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.MediaFramework.Brightcove.IBrightcoveVideo.#VideoStillURL")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.MediaFramework.Brightcove.IBrightcoveMediaElement.#ID")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.MediaFramework.Brightcove.IBrightcoveMediaElement.#ThumbnailURL")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "URL", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.MediaFramework.Brightcove.IBrightcoveMediaElement.#ThumbnailURL")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "type", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Metatag", Scope = "type", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Set")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "template", Scope = "type", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Metatags", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Set")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Metatags", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Metatags")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Predefined_keywords")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Metatags", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Predefined_keywords")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "keywords", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Predefined_keywords")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Other_keywords")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Metatags", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Other_keywords")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "keywords", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Other_keywords")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Description")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Metatags")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Metatags", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IMetatag_template.#Metatags_Description")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Pages.IProductRegional.#Specifications")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.IModelBase.#Url")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Qs", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IProductGlobalStatic.#ShowFaQs")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Fa", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IProductGlobalStatic.#ShowFaQs")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Fa", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IProductGlobalStatic.#ShowFaQs")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Global.IProductGlobalStatic.#Specifications")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Faq", Scope = "type", Target = "Anritsu.WebApp.Infivis.Models.Pages.IFaq")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Faq", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Pages.IFaq.#FaqType")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "We", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Pages.IContactSupport.#WhatCanWeHelpYouWith")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Scope = "member", Target = "Anritsu.WebApp.Infivis.Models.Components.Extensions.Downloads.#GetItemUrl(Anritsu.WebApp.Infivis.Models.Pages.DownloadSearchItem)")]
