﻿using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glass.Mapper.Sc.Fields;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    public class DownloadSearchItem: SearchResultItem,IDownloadsAI
    {

        public Guid Id { get; set; }

        public Sitecore.Data.ID TemplateId { get; set; }

        public string TemplateName { get; set; }

        public IModelBase ParentItem { get; set; }

        public IEnumerable<IModelBase> ChildItems { get; set; }

        public string Url { get; set; }

        public string MenuTitle { get; set; }

        public bool ShowInMainNavigation { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Version { get; set; }

        public bool IsNewVersionAvailable { get; set; }

        public Link ReplacementDownloadPath { get; set; }

        public DateTime ReleaseDate { get; set; }

        public IDropListItem Subcategory { get; set; }

        public IEnumerable<IProductDetailStaticC1> RelatedProducts { get; set; }

        public string OptionalLinks { get; set; }

        public string FilePath { get; set; }

        public string FileType { get; set; }

        public string Size { get; set; }

        public bool LogOnRequired { get; set; }

        public bool RedirectToMyAnritsu { get; set; }

        [IndexField("ProductModelNumber")]
        public IEnumerable<string> ProductModelNumbers { get; set; }

        [IndexField("DownloadCategory")]
        public string DownloadCategory { get; set; }

        [IndexField("DownloadCategoryId")]
        public string DownloadCategoryId { get; set; }

        [IndexField("itemmetakeywords")]
        public string Keyword { get; set; }

        [IndexField("DownloadReleaseDate")]
        public string DownloadReleaseDate { get; set; }

        public IEnumerable<IModelBase> SideNavigationItems { get; set; }
    }
}
