﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Components;

namespace Anritsu.WebApp.Infivis.Models.Pages.Extensions
{
    public static class ProductFamily
    {
        public static string GetTextColor(this IProductFamilyStaticC1 family)
        {
            return family.TextColor == null ? string.Empty : family.TextColor.Key;
        }
        public static string GetTextColor(this IProductFamilyStaticC2 family)
        {
            return family.TextColor == null ? string.Empty : family.TextColor.Key;
        }
    }
}
