﻿using System;
using System.Collections.Generic;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    public class FrequentlyAskedQuestionSearchItem:SearchResultItem,IFaq
    {
        public Guid Id { get; set; }

        public ID TemplateId { get; set; }

        public string TemplateName { get; set; }

        public IModelBase ParentItem { get; set; }

        public IEnumerable<IModelBase> ChildItems { get; set; }

        public string Url { get; set; }

        public string MenuTitle { get; set; }

        public bool ShowInMainNavigation { get; set; }

        public IDropListItem FaqType { get; set; }

        public bool IsActive { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public IEnumerable<IProductDetailStaticC1> RelatedProducts { get; set; }

        [IndexField("ProductModelNumber")]
        public IEnumerable<string> ProductModelNumbers { get; set; }

        [IndexField("RelatedFamilies")]
        public IEnumerable<string> RelatedFamilies { get; set; }

        [IndexField("RelatedSubcategories")]
        public IEnumerable<string> RelatedSubcategories { get; set; }

        [IndexField("RelatedCategories")]
        public IEnumerable<string> RelatedCategories { get; set; }

        [IndexField("itemmetakeywords")]
        public string Keyword { get; set; }

        public IEnumerable<IModelBase> SideNavigationItems { get; set; }

    }
}
