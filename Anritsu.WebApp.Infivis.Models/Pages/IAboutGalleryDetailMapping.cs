﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/AboutGalleryDetailMapping
    /// Template ID : {29DBAC41-E4EF-45BA-A3C3-A2072307275F}
    /// </summary>
    [SitecoreType(TemplateId = "{29DBAC41-E4EF-45BA-A3C3-A2072307275F}")]
    public partial interface IAboutGalleryDetailMapping:IModelBase, INavigation
    {
        /// <summary>
        /// Gets the DropTree Content. Datasource: {A0056C28-8049-4708-B0C1-06F362FA2925}
        /// </summary>
        [SitecoreField]
        IAboutGalleryDetail Content { get; set; }
    }
}
