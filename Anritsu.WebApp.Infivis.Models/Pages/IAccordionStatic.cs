﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template 	/sitecore/templates/Anritsu-WWW/Pages/AboutAnritsuContentDetailMap
    /// Template ID : {43FB30F9-06EA-4A56-B324-987CC155C904}
    /// </summary>
    [SitecoreType(TemplateId = "{43FB30F9-06EA-4A56-B324-987CC155C904}")]
    public partial interface IAccordionStatic : IModelBase
    {
        /// <summary>
        /// Gets the DropTree Content. Datasource: {A0056C28-8049-4708-B0C1-06F362FA2925}
        /// </summary>
        [SitecoreField]
        IStaticPageWithOptionalLinksInfivis Content { get; set; }
    }
}
