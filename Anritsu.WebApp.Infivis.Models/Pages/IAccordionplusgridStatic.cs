﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
     [SitecoreType(TemplateId = "{977AC54C-803F-46EF-A14F-68DEDCE6B16A}")]
    public partial interface IAccordionplusgridStatic : IModelBase,INavigationStatic
    {
         [SitecoreField]
         Image BannerImage { get; set; }

         [SitecoreField]
         string Title { get; set; }

         [SitecoreField]
         string LongDescription { get; set; }

         [SitecoreField]
         string TechnologyName { get; set; }

         [SitecoreField]
         IEnumerable<IProductDetailStaticC1> RelatedProducts { get; set; }

         [SitecoreField]
         Image Image { get; set; }

         [SitecoreField]
         string OptionalLinks { get; set; }

         [SitecoreField]
         string ShortDescription { get; set; }
    }
}
