﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Infivis/Pages/ProductCategory
    /// Template ID : {48CD0D76-523D-4FEC-8E17-F6D826A5BD43}
    /// </summary>
    [SitecoreType(TemplateId = "{48CD0D76-523D-4FEC-8E17-F6D826A5BD43}")]
  public partial interface ICategoryTopStatic : IModelBase, INavigationStatic ,ILink ,IBaseTemplate, INewsSelectionStatic, INewProductsSelectionStatic, IEventsSelectionStatic, IAnnouncementTickerStatic, ILinkManagerPromotionsStatic
    {  

    /// <summary>
    /// Gets the Treelist HeroCarousel. Datasource: Datasource={A9EEAA97-816C-4B51-B4AF-613B37664FCE}
    /// </summary>
  	[SitecoreField]
    IEnumerable<ICarousel> HeroCarousel { get; set; }

    [SitecoreField]
    IEnumerable<IProductSubcategoryStatic> ProductSubcategoryList { get; set; }

        [SitecoreField]
        IEnumerable<ILink> LinkButtons { get; set; }

    }
}
