﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;

namespace Anritsu.WebApp.GlobalWeb.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template 	/sitecore/templates/Anritsu-WWW/Pages/AboutAnritsuContentDetailMap
    /// Template ID : {0D5465BB-E4F0-45F3-AB8A-DB6A50B1A623}
    /// </summary>
    [SitecoreType(TemplateId = "{0D5465BB-E4F0-45F3-AB8A-DB6A50B1A623}")]
    public partial interface IContentDetailMap : IModelBase
    {
        /// <summary>
        /// Gets the DropTree Content. Datasource: {A0056C28-8049-4708-B0C1-06F362FA2925}
        /// </summary>
        [SitecoreField]
        IStaticPageWithOptionalLinks Content { get; set; }
    }
}
