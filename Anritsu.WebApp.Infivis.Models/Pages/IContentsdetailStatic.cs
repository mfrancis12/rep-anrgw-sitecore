﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Video
    /// Template ID : {2FF616E8-3D47-4AF2-83D9-C5E4DB0197B0}
    /// </summary>
    [SitecoreType(TemplateId = "{2FF616E8-3D47-4AF2-83D9-C5E4DB0197B0}")]
  public partial interface IContentsdetailStatic : IModelBase,INavigationStatic 
  {  
	[SitecoreField]
    IEnumerable<IModelBase> Content { get; set; }

  }
}
