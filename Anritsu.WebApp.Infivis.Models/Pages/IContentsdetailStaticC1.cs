﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Video
    /// Template ID : {B4293A76-AE43-4971-999C-057F0FD64514}
    /// </summary>
    [SitecoreType(TemplateId = "{B4293A76-AE43-4971-999C-057F0FD64514}")]
  public partial interface IContentsdetailStaticC1 : IModelBase,INavigationStatic 
  {  
	[SitecoreField]
    IEnumerable<IModelBase> Content { get; set; }

  }
}
