﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.Infivis.Models.Common;


namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/infivis/Pages/Home
    /// Template ID : {1F65FF2E-FA32-4304-A8AA-5A0D7C24092E}
    /// </summary>
    [SitecoreType(TemplateId = "{1F65FF2E-FA32-4304-A8AA-5A0D7C24092E}")]
    public partial interface ICorporateHomeStatic : IModelBase, INavigationStatic, IBaseTemplate, IToolbarStatic, IAnnouncementTickerStatic, INewsSelectionStatic, IEventsSelectionStatic
    {

        /// <summary>
        /// Gets the Multilist HeroCarousel. Datasource: /sitecore/content/GlobalWeb/components/Carousel
        /// </summary>
        [SitecoreField]
        IEnumerable<IImageLinkWithDescriptionStatic> HeroCarousel { get; set; }


        /// <summary>
        /// Gets the Multilist Promos. Datasource: /sitecore/content/GlobalWeb/components/promotions
        /// </summary>
        [SitecoreField]
        IEnumerable<IPromo> Highlights { get; set; }
    }
}
