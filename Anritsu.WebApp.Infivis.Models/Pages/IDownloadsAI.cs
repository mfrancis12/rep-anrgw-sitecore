﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.Infivis.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Downloads
    /// Template ID : {0A13323A-10F2-404A-9AA2-7E0BD4D0EF78}
    /// </summary>
    /// [SitecoreType(TemplateId = "{0A13323A-10F2-404A-9AA2-7E0BD4D0EF78}")]
    public partial interface IDownloadsAI : IModelBase, INavigationStatic
    {
        /// <summary>
        /// Gets the Single-Line Text Title. 
        /// </summary>
        [SitecoreField]
        string Title { get; set; }

        /// <summary>
        /// Gets the Multi-Line Text Description. 
        /// </summary>
        [SitecoreField]
        string Description { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Title. 
        /// </summary>
        [SitecoreField]
        string Version { get; set; }

        /// <summary>
        /// Gets the Checkbox IsNewVersionAvailable. 
        /// </summary>
        [SitecoreField]
        bool IsNewVersionAvailable { get; set; }

        /// <remarks>
        /// There is no specific template for the field type Browse.Used for selecting the S3 files
        /// </remarks>
        /// <summary>
        /// Gets the Browse ReplacementDowloadPath. 
        /// </summary>
        [SitecoreField]
        Link ReplacementDownloadPath { get; set; }

        /// <summary>
        /// Gets the DateTime ReleaseDate. 
        /// </summary>
        [SitecoreField]
        DateTime ReleaseDate { get; set; }

        /// <summary>
        /// Gets the Droplink Subcategory. Datasource={A7C21138-D509-440C-A5BB-A07B7E35AC9D}
        /// </summary>
        [SitecoreField]
        IDropListItem Subcategory { get; set; }

        /// <summary>
        /// Gets the Treelist RelatedProducts. Datasource={110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}&IncludeTemplatesForSelection=ProductRegional&includetemplatesfordisplay=ProductCategory,RedirectLink,ProductRegional
        /// </summary>
        [SitecoreField]
        IEnumerable<IProductDetailStaticC1> RelatedProducts { get; set; }

        /// <remarks>
        /// There is no specific template for the field type Field Suite General Links. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the Field Suite General Links OptionalLinks. 
        /// </summary>
        [SitecoreField]
        string OptionalLinks { get; set; } 

            /// <remarks>
        /// There is no specific template for the field type Browse.Used for selecting the S3 files
        /// </remarks>
        /// <summary>
        /// Gets the Browse FilePath. 
        /// </summary>
        [SitecoreField]
        string FilePath { get; set; } 

        /// <summary>
        /// Gets the Single-Line Text FileType. 
        /// </summary>
        [SitecoreField]
        string FileType { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Size. 
        /// </summary>
        [SitecoreField]
        string Size { get; set; }

        /// <summary>
        /// Gets the Checkbox LoginRequired. 
        /// </summary>
        [SitecoreField]
        bool LogOnRequired { get; set; }

         /// <summary>
        /// Gets the Checkbox LoginRequired. 
        /// </summary>
        [SitecoreField]
        bool RedirectToMyAnritsu { get; set; }
    }
}
