﻿using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Faq
    /// Template ID : {B7BDD7DC-0624-4526-83AB-4702B625E71E}
    /// </summary>
    [SitecoreType(TemplateId = "{B7BDD7DC-0624-4526-83AB-4702B625E71E}")]
    public partial interface IFaq : IModelBase, INavigationStatic
    {
        /// <remarks>
        /// There is no specific template for the field type Droplink. This is a fallback field type template. 
        /// </remarks>
        /// <summary>
        /// Gets the Droplink FaqType. Datasource: Datasource={227FC20D-8BBE-4AE5-8C7E-63884F9D534F}
        /// </summary>
        [SitecoreField]
        IDropListItem FaqType { get; set; }

        /// <summary>
        /// Gets the Checkbox Active. 
        /// </summary>
        [SitecoreField]
        bool IsActive { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Question. 
        /// </summary>
        [SitecoreField]
        string Question { get; set; }

        /// <summary>
        /// Gets the Single-Line Text Answer. 
        /// </summary>
        [SitecoreField]
        string Answer { get; set; }

        /// <summary>
        /// Gets the Multilist RelatedProducts. Datasource: Datasource={110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}&IncludeTemplatesForSelection=ProductRegional&includetemplatesfordisplay=ProductCategory,RedirectLink,ProductRegional
        /// </summary>
        [SitecoreField]
        IEnumerable<IProductDetailStaticC1> RelatedProducts { get; set; }

    }
}
