﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.Infivis.Models.Common;


namespace Anritsu.WebApp.Infivis.Models.Pages
{
    [SitecoreType(TemplateId = "{3A176CC8-B9D7-412B-8B40-F6D5FDD49CC0}")]
    public partial interface IIndustryAI : IModelBase,INavigationStatic
    {
        [SitecoreField]
        Image BannerImage { get; set; }
       

        [SitecoreField]
        string BannerTitle { get; set; }


        [SitecoreField]
        string BannerDescription { get; set; }


        [SitecoreField]
        string IndustryName { get; set; }


        [SitecoreField]
        IEnumerable<IProductDetailStaticC1> RelatedProducts { get; set; }


        [SitecoreField]
        Image Image { get; set; }


        [SitecoreField]
        string OptionalLinks { get; set; }

    }
}
