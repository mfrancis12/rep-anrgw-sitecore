﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Common;


namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ProductRegional
    /// Template ID : {A649E727-507F-48D3-A6F2-187931D2528C}
    /// </summary>
    [SitecoreType(TemplateId = "{A649E727-507F-48D3-A6F2-187931D2528C}")]
    public partial interface IProductDetailStatic : IModelBase, INavigationStatic, IToolbarStatic, IDropListStatic
    {  

    /// <summary>
    /// Gets the Droplink SelectProduct. Datasource: /sitecore/content/Global/Products
    /// </summary>
	[SitecoreField]
        Anritsu.WebApp.Infivis.Models.Global.IProductGlobalStatic SelectProduct { get; set; }

    /// <summary>
    /// Gets the Multilist RelatedProducts. Datasource: /sitecore/content/GlobalWeb/home/products
    /// </summary>
    [SitecoreField]
    IEnumerable<IProductDetailStatic> RelatedProducts { get; set; }

    /// <summary>
    /// Gets the Treelist Videos. Datasource: Datasource=/sitecore/media library/Media Framework/Accounts/AnritsuBrightcoveAccount/Media Content&includetemplatesforselection=BrightcoveVideo
    /// </summary>
    [SitecoreField]
    IEnumerable<IVideodetailStatic> Videos { get; set; }

    /// <summary>
    /// Gets the Checkbox WhatSNew. 
    /// </summary>
    [SitecoreField]
    bool WhatSNew { get; set; }

        [SitecoreField]
        IEnumerable<IProductDetailIcon> Icons { get; set; }

        //[SitecoreField]
        //IEnumerable<IIconLinkWithTitle> Icons { get; set; }

        /// <summary>
        /// Gets the Treelist Brightcove Japan Videos.
        /// </summary>
        [SitecoreField]
        IEnumerable<IBrightcoveJapanVideo> JapanVideoPortals { get; set; }
    }
}
