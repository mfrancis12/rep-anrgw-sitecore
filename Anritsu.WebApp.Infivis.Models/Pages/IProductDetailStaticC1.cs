﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ProductRegional
    /// Template ID : {FCC3D75C-09A4-4A3B-B9A8-04697DD096CC}
    /// </summary>
    [SitecoreType(TemplateId = "{FCC3D75C-09A4-4A3B-B9A8-04697DD096CC}")]
    public partial interface IProductDetailStaticC1 : IProductDetailStatic, IStaticText
  {  



  }
}
