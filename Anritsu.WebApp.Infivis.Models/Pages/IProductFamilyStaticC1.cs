﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ProductFamily
    /// Template ID : {3C47A664-F04F-4294-8416-931B5023597A}
    /// </summary>
    [SitecoreType(TemplateId = "{3C47A664-F04F-4294-8416-931B5023597A}")]
  public partial interface IProductFamilyStaticC1 : IModelBase, INavigationStatic ,IBaseTemplate, IStaticText, IToolbarStatic 
  {  

    /// <summary>
    /// Gets the Image Image. 
    /// </summary>
	[SitecoreField]
    Image Image { get; set; }

    /// <summary>
    /// Gets the Rich Text Description. 
    /// </summary>
	[SitecoreField]
    string Description { get; set; }

    /// <summary>
    /// Gets the Multilist Specifications. Datasource: /sitecore/content/GlobalWeb/components/Specifications
    /// </summary>
	[SitecoreField]
    IEnumerable<IDropListItem> Specifications { get; set; }

    [SitecoreField]
    IEnumerable<IProductDetailStatic> Products { get; set; }

    [SitecoreField]
    bool ShowReleaseDate { get; set; }

    /// <summary>
    /// Gets the Droplink TextColor. Datasource: /sitecore/content/GlobalWeb/components/TextColor
    /// </summary>
    [SitecoreField]
    IDropListItem TextColor { get; set; }

    [SitecoreField]
    IEnumerable<IPromo> ShortcutBanner { get; set; }


  }
}

