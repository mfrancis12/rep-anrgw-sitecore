﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.Infivis.Models.Common;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/ProductFamily
    /// Template ID : {60486DA0-749C-4063-9F1A-584E334EEE1B}
    /// </summary>
    [SitecoreType(TemplateId = "{60486DA0-749C-4063-9F1A-584E334EEE1B}")]
  public partial interface IProductFamilyStaticC2 : IProductFamilyStaticC1 
  {  
        /// <summary>
        /// Gets the Treelist Videos. Datasource: Datasource=/sitecore/media library/Media Framework/Accounts/AnritsuBrightcoveAccount/Media Content&includetemplatesforselection=BrightcoveVideo
        /// </summary>
        [SitecoreField]
        IEnumerable<IBrightcoveVideo> Videos { get; set; }

    }
}
