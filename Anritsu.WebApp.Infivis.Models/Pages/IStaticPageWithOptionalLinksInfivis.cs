﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.WebApp.Infivis.Models.Common;


namespace Anritsu.WebApp.Infivis.Models.Pages
{
    [SitecoreType(TemplateId = "{F714D5F9-99FD-4591-87E1-D3CAB8D15D8D}")]
    public partial interface IStaticPageWithOptionalLinksInfivis :IModelBase,INavigationStatic
    {
        [SitecoreField]
        string Title { get; set; }

        [SitecoreField]
        Image BannerImage { get; set; }

        [SitecoreField]
        string Description { get; set; }

        [SitecoreField]
        Image Image { get; set; }

        [SitecoreField]
        string Links { get; set; }

        [SitecoreField]
        IEnumerable<IProductDetailStaticC1> RelatedProducts { get; set; }
    }
}
