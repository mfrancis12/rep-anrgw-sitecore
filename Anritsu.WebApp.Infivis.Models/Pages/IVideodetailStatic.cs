﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/Video
    /// Template ID : {21FA5AFE-8B06-4E46-A4CB-C917C2B79DFD}
    /// </summary>
    [SitecoreType(TemplateId = "{21FA5AFE-8B06-4E46-A4CB-C917C2B79DFD}")]
  public partial interface IVideodetailStatic : IModelBase, INavigationStatic
    {  

    /// <summary>
    /// Gets the Single-Line Text Name. 
    /// </summary>
	[SitecoreField]
    string Name { get; set; }

    /// <summary>
    /// Gets the Multi-Line Text ShortDescription. 
    /// </summary>
	[SitecoreField]
    string ShortDescription { get; set; }

    /// <summary>
    /// Gets the Rich Text LongDescription. 
    /// </summary>
	[SitecoreField]
    string LongDescription { get; set; }

    /// <remarks>
    /// There is no specific template for the field type Droptree. This is a fallback field type template. 
    /// </remarks>
    /// <summary>
    /// Gets the Droptree Video. Datasource: Datasource=/sitecore/media library/Media Framework/Accounts/AnritsuBrightcoveAccount/Media Content&includetemplatesforselection=BrightcoveVideo
    /// </summary>
	[SitecoreField]
    string Video { get; set; }

    /// <remarks>
    /// There is no specific template for the field type Field Suite General Links. This is a fallback field type template. 
    /// </remarks>
    /// <summary>
    /// Gets the Field Suite General Links RelatedLinks. 
    /// </summary>
	[SitecoreField]
    string RelatedLinks { get; set; }

    /// <summary>
    /// Gets the Treelist RelatedVideos. Datasource: Datasource=/sitecore/media library/Media Framework/Accounts/AnritsuBrightcoveAccount/Media Content&includetemplatesforselection=BrightcoveVideo
    /// </summary>
	[SitecoreField]
    IEnumerable<IVideodetailStatic> RelatedVideos { get; set; }

    [SitecoreField]
    Image Thumbnail { get; set; }

        [SitecoreField]
        IEnumerable<IVideoCategory> VideoCategory { get; set; }
    }
}
