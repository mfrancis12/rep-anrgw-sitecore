﻿using System;
using System.Collections;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models.Common;


namespace Anritsu.WebApp.Infivis.Models.Pages
{
    /// <summary>
    /// Code representation of item based on Sitecore template /sitecore/templates/Anritsu-WWW/Pages/StaticPage
    /// Template ID : {9A15E9D6-7D3D-4BBD-9FC6-2C0EA7347882}
    /// </summary>
    [SitecoreType(TemplateId = "{9A15E9D6-7D3D-4BBD-9FC6-2C0EA7347882}")]
  public partial interface IVideogalleryStatic : IModelBase, INavigationStatic ,IBaseTemplate 
  {  

  }
}
