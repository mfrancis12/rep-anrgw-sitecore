﻿using Anritsu.WebApp.GlobalWeb.Models.Global;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
    public class ProductSearchItemC1 : SearchResultItem,IProductDetailStaticC1
    {
        public Guid Id { get; set; }

        public string PageTitle { get; set; }
        public string MetaTitle { get; set; }

        public Guid Metatags_Set { get; set; }

        public IEnumerable<IModelBase> Metatags_Metatags { get; set; }

        public string Metatags_Description { get; set; }
        public IEnumerable<IModelBase> Metatags_Predefined_keywords { get; set; }

        public string Metatags_Other_keywords { get; set; }

        public IStaticPageForFreeform ContentMap { get; set; }

        public Sitecore.Data.ID TemplateId { get; set; }


        public string TemplateName { get; set; }


        public IModelBase ParentItem { get; set; }


        public IEnumerable<IModelBase> ChildItems { get; set; }


        public string Url { get; set; }


        public Anritsu.WebApp.Infivis.Models.Global.IProductGlobalC1 SelectProduct { get; set; }


        public IEnumerable<IProductDetailStaticC1> RelatedProducts { get; set; }


        public IEnumerable<IBrightcoveVideo> Videos { get; set; }

        public IEnumerable<IIconLinkWithTitle> Toolbar { get; set; }


        //public bool WhatSNew { get; set; }


        public string MenuTitle { get; set; }


        public bool ShowInMainNavigation { get; set; }

        [IndexField("ModelNumber")]
        public string ModelNumber { get; set; }

        [IndexField("IsDiscontinued")]
        public bool IsDiscontinued { get; set; }

        [IndexField("WhatSNew")]
        public bool WhatSNew { get; set; }

        public IEnumerable<IModelBase> SideNavigationItems { get; set; }

    }
}
