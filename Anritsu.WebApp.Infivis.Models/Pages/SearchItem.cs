﻿using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models.Components;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.Infivis.Models.Pages
{
   
    public class SearchItem : SearchResultItem, IVideodetailStatic
    {

        public Guid Id { get; set; }

        public IEnumerable<IModelBase> SideNavigationItems { get; set; }

        public string MenuTitle { get; set; }

        public bool ShowInMainNavigation { get; set; }

        public override Sitecore.Data.ID TemplateId { get; set; }


        public override string TemplateName { get; set; }


        public IModelBase ParentItem { get; set; }


        public IEnumerable<IModelBase> ChildItems { get; set; }


        public override string Url { get; set; }

        public override string Name { get; set; }

        public string ShortDescription { get; set; }

        public string LongDescription { get; set; }

        
        public string Video { get; set; }

        public string RelatedLinks { get; set; }

       
        public IEnumerable<IVideodetailStatic> RelatedVideos { get; set; }

        public Image Thumbnail { get; set; }


        public IBrightcoveVideo VideoItem
        {
            get
            {
                if (Video != null)
                {
                    var contextService = new SitecoreContext();
                    return contextService.GetItem<IBrightcoveVideo>(Video, LanguageManager.GetLanguage("en"));
                }
                return null;
            }

        }

        public IEnumerable<IVideoCategory> VideoCategory { get; set; }


        [IndexField("VideoRootCategories")]
        public IEnumerable<string> VideoRootCategories { get; set; }

        [IndexField("VideoCategories")]
        public IEnumerable<string> VideoCategories { get; set; }

        [IndexField("VideoParentCategories")]
        public IEnumerable<string> VideoParentCategories { get; set; }



    }
}

