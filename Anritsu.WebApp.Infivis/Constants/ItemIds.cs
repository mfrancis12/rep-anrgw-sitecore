﻿namespace Anritsu.WebApp.Infivis.Constants
{
    public static class ItemIds
    {
        public const string Home = "{FA28063B-F46B-46F0-8C16-CE9EE4C8295B}";
        public const string Product = "{5F14D911-472E-46F9-A7F2-EF3EC8D35735}";
        public const string SharedContent = "{255222F4-E523-4995-B2AE-721795F234C7}";
        public const string IRNews = "{F9EA21B7-5CA5-4C77-B104-48E704F698EC}";
        public const string Contact = "{039B83B9-C1BE-468D-8659-4299A442CE30}";
        public const string WorkflowRejectEmailAction = "{BCBA63C3-9389-4B80-874F-92BAAB3AEB11}";
        public const string ProductId = "{414498B2-EDD8-4156-AF5B-2AF129DFC51C}";
        public const string GlobalProduct = "{A7737E86-7DAA-43E5-A864-B5415F2E6D44}";
        public const string HeaderLinks = "{db7e5ef3-0a02-4535-90ef-148db4db7594}";
        public const string ISOCountries = "{CBE79E28-2912-4127-BE67-D469825EA027}";
        public const string ConfirmYourCountry = "{F51AD35B-ADB4-4971-A0F7-95E6AD2D6CD9}";
        public const string ProductDetailTabFilters = "{72016F63-933E-4270-BCD4-24297D4B37FE}";
        public const string VideoGalleryFilters = "{93232A4D-1325-4B42-82C0-081C604866E3}";
        public const string FooterLinks = "{F5D07951-75B5-4C85-8B36-F76D277AC9EB}";
        public const string CategorySlider = "{846b7d8f-3e0a-46e7-88f3-626962f761a6}";
        public const string ProductCategoryButtons = "{99484265-9A02-48F9-9364-724C6C6C5DAB}";
        public const string SustainabilityButtons = "{F04AD36C-1A10-4017-ACD1-566B6A489A04}";
        public const string SustainabilitySlider = "{E308F26E-A316-4A70-BA26-3838FDC20913}";
        public const string ToolBarFolder = "{CB6DD87D-9C42-468E-977B-86082E51EEB7}";
        public const string HelpIcons = "{2581A154-6B09-4477-91EB-2C55BBF17A0C}";
        public const string CorporateInformation = "{7750876A-309F-4D61-9A20-DEDF7062DBC2}";
        public const string ShareIcons = "{FED5EFA9-BE42-40B5-A0EE-BA2F4747CD56}";
        public const string ToolBar = "{CB6DD87D-9C42-468E-977B-86082E51EEB7}";
        public const string HeaderImageLogo = "{BA7059F9-0F80-46C4-8AFA-5513644E18FF}";
        public const string HeaderImageWithoutTagline = "{FAAF27E5-4A6A-4583-A74A-22D816340D13}";
        public const string NewProducts = "{1CD4BB35-29F5-4433-A15E-22DD0AE5ECEB}";
        public const string EcoProductDetails = "{BC3E366B-A1BE-4F0D-81D5-2BF1B502A530}";
        public const string ComponentAndAccessoriesDiscontinuedModel = "{B9A0D2A6-E5DC-4DA9-BBFC-6021FFC3A0D0}";
        public const string InvestorRelations = "{0ABF5F3D-C7A9-42A7-A966-6BF75685AD8B}";
        
        
        public const string Products = "{FB12A877-D524-4471-B848-D069DC6A2B3E}";
        public const string VideoDetails = "{D84F9FE7-E826-48A4-8267-ECAA0D37F7B8}";
        public const string VideoGallery = "{7ED83465-DD88-4FB1-AAB4-84E6ABCD1C27}";
        public const string SideMenuHome = "{F30528F8-C34D-4BC9-AABE-F121FA8D7F88}";        
        public const string BuyCategories = "{F98BF245-D32F-4C77-A3D6-C4A66B5C624E}";
        public const string BuyToolbar = "{B4B4A9CF-F16B-4458-B20A-5D0DCE7BF462}";
        public const string ProfessionalServices = "{D5475BA7-5649-4399-8074-9B7C4ED158AC}";
        public const string NewsReleaseFilters = "{939DAC42-CE59-43EB-8CC4-ABB159AD61D7}";

        public const string TestMeasurement = "{CCE3218B-E4E1-476F-A541-14E925B51F9E}";
        public const string ComponentAndAccessories = "{4319C2F4-197C-4D8F-8456-D62AF61FEB42}";
        public const string AnritsuDevices = "{9ECE2C53-448B-4A9A-92E4-9C252E2D965A}";


        public const string HomeNewsPage = "{F88E5772-62D0-4AB0-9592-D9D722B21E52}";
        public const string TMNewsPage = "{D25B7A44-5A77-4725-8510-3C665CF01D29}";
        public const string HomeEventsPage = "{50FC205B-57B3-4697-8B2E-ED2E5739CCF4}";
        public const string TMEventsPage = "{D4F0EA71-1BAD-48C5-9FBA-22402D18D711}";
        public const string IRNewsPage = "{FBCF9289-3A93-4159-AEF9-001ABAB84477}";
        public const string IRNewsRssFeed = "{92A6B6EB-06F7-4764-BBA2-4A65725A704E}";


        public const string TradeshowsHome = "{7621241F-3A06-4F05-99E5-36321259DDF5}";
        public const string CorporateEventHome = "{68181094-1CA0-4955-ACF4-775100121CDC}";
        public const string TradeshowsTM = "{7F57EB6B-7BBE-4D34-8285-535AAAAF6613}";
        public const string WebinarsTM = "{1E982F16-4FA3-453F-BEA1-8DFCB10B744D}";
        public const string TMeventsTM = "{7AC1946F-2BFC-448B-B820-3B6EC2667780}";

        public const string HomeNewsRelease = "{8768A8C7-CF26-442A-9716-88B37CF8D3A5}";
        public const string HomeAnnouncement = "{2B50224A-8A99-4FE4-BA22-7E86ED3CA6C0}";
        public const string TMNewsRelease = "{2BB421D7-7D80-47B5-A18A-DB387B629BD7}";
        public const string TMAnnouncement = "{DDD9B9D4-11B2-4716-9D71-8172E0D21D1E}";

        public const string HomeNewsRSSFeeds = "{7FB136E5-38A4-403F-91CE-1EA05F3BBA57}";
        public const string TMNewsRSSFeeds = "{92BEEAAE-DC07-425B-9B03-0CE7E4EF195F}";
        public const string New = " {1CD4BB35-29F5-4433-A15E-22DD0AE5ECEB}";
        public const string Breadcrumb = "{90338009-6BD9-4064-99C5-6A4DC7DB3227}";
       

        public const string Regions = "{A397A5D9-EB32-43ED-8863-2364C741EB76}";
        public const string ChangeRegion = "{7E8C86F0-BD90-48DD-9A79-AAE6AEDABA03}";
        public const string GlobalRegions = "{FA78F31F-255D-4BF0-AE2C-9E199279D90C}";
        public const string AnritsuWorldwideFolder = "{F1ECB015-F399-4DE4-86F3-8AF27ABAE1DE}";
        public const string ContactUsCategoriesFolder = "{90450865-E109-41DC-A186-242E8321D238}";
        public const string CorporateContact = "{D44B63DE-6137-4499-AB39-074F35C501DC}";
        public const string UnitedStates = "{94C28598-E4E4-410B-9475-D9684ABE044D}";

        public const string TechmacContactUs = "{FB7B918B-4990-40F6-8BE5-4F553A38F875}";
        public const string TohokuAnritsuContactUs = "{788E05A3-93D0-4830-9292-174FB1AA76EA}";
        public const string DownloadItem = "{D4C0680D-D510-497C-8079-02D8324D7984}";
        public const string RequestProductQuote = "{F728C4FB-BA01-4008-AB93-5EC07B0B19DA}";
        public const string GetQuote = "{1E9552C8-9342-4E05-B5E6-E46DDF99E2DF}";
        
        public const string RequestAQuoteAnritsuDevices = "{6A6C69F4-FDEE-4F12-8689-77A1EC3CD0F1}";
        public const string RequestAQuoteTestMeasurement = "{13A852A6-E3ED-42A1-8C32-E8665DF868F8}";
        public const string RequestADemo = "{AD458F4C-8036-4188-8CFC-8DA5977CD12F}";
        public const string RequestServiceQuote = "{F9942BE4-5565-4BD6-911B-3A371C8A8EAB}";

        public const string DownloadManuals = "{968F1F44-5072-441F-966C-B17C85DD8BE9}";
        public const string DownloadSoftware = "{D134AA78-9A59-4D66-821E-02F34457748D}";
        public const string DownloadBrochures = "{1EC177F1-3884-4710-88F8-A91B3189F2E7}";
        
        public const string TrainingIndex = "{329C1E6A-C7B3-49C8-AA1F-0466295BCE43}";
        public const string TrainingRegistration = "{06AF0DB7-3480-43D3-ABD8-28F1DF7DA98A}";
        public const string TrainingCart = "{AFFCC7C0-3FDE-4593-B6C9-C9F441AEA596}";
        public const string TrainingOrderSummary = "{B899B511-8849-4D58-B163-2C9C895A82B7}";
        public const string TrainingDistributor = "{D61F2C89-73A9-4C3D-86A7-BDE5AA8F690D}";
        public const string ReviewOrder = "{23A3495E-1465-4000-B60B-54C1FBA4C188}";

        public const string SiteMap = "{AFA87AF1-BFD5-49B2-90C6-57E8F29E26E7}";
        public const string TabsList = "{A2F8CF03-CD5B-44EA-A51A-C6DEF2663BCE}";
        public const string Search = "{8D1B174B-0A7A-45E7-9535-72C474E73D3C}";

        public const string FaqWildcardItem = "{C2AFE25C-12E9-493E-9633-60ED013A2820}";
        public const string SoftwareWildcardItem = "{DF5CDB82-20AA-46B2-ADDC-61EA2DA7DCE5}";
        public const string Employment = "{405CD9F6-CBF7-40B7-805D-646787633026}";
        public const string AboutAnritsu = "{AE794A8F-758A-4F3D-97D0-041B6AF0F8ED}";
        public const string BusinessServices = "{26DA5DEE-29CC-46A5-8A3D-87B41429FAB7}";
        public const string Support = "{A54E15CC-74CE-4E84-A62C-925B9ADC6B99}";
        public const string Buy = "{8006A20D-A648-4382-93BE-8DF6F54151A2}";
        public const string Technologies = "{EA648742-86E8-4D27-8526-A6F6AC5472D1}";
        public const string Industries = "{79098D2F-AF3F-428A-9FB3-609FCB21C925}";
        public const string AnritsuWorldwideItem = "{E9A68189-30A3-4134-86A0-8897A326CA6B}";
        public const string BreadcrumbSubLayout = "{4B5C805E-BFD1-497D-98C8-7FEEFEE112A8}";
      
        //Search Filter Item IDs
        public const string FilterItemProducts = "{A1B9B43D-F508-4548-80E1-881890BB40CA}";
        public const string FilterItemDownloads = "{049CB6C2-F139-4EC0-8E13-6DE526BD1401}";
        public const string FilterItemBusinessServices = "{802BEFA5-855E-4FBB-9562-F18F196C1910}";
        public const string FilterItemSupport = "{D8CB9673-A71E-491F-BE58-C1D77C2B8309}";
        public const string FilterItemAboutAnritsu = "{E15B255D-F7AD-401B-9222-151A14500C2A}";
        public const string FilterItemContactUs = "{066902AE-F2A3-430A-9153-D0B6E61D612D}";
        public const string FilterItemFaqs = "{5B1402B0-A0AB-4F98-9A7A-0427931FA7B7}";
        public const string FilterItemEvents = "{1F97EF00-0BAE-4736-8C05-6CDB0E5E127B}";
        public const string FilterItemNews = "{BEA3DD8B-5314-4448-BA90-95477F3138BE}";
        public const string FilterItemBuy = "{A273BB36-0665-4EC0-84E6-8FCAA187E0A2}";
        public const string FilterItemTechnologies = "{342E2B4C-BDEF-4E8E-A547-80046D3523AA}";
        public const string FilterItemIndustries = "{AAA6D17A-891D-4D40-B0D1-A18E40C09653}";
        public const string FilterItemVideos = "{946CAA19-BA26-463A-9143-8C11B3BA9315}";
        public const string FilterItemEmployment = "{9F06A513-6A82-46BE-A9AA-12E7F15546C0}";
        public const string SitecoreNoIndex = "{4F34E73A-CD25-4F7A-AA49-C02227646866}";
        public const string DownloadSoftwareCategory = "{0E3C8E23-CA9F-488A-B819-9E60FEF3816D}";
        public const string FilterItemDiscontinuedProducts = "{5419DBE8-293B-420A-9361-605F2DDB1CCA}";
        //search filter item id's end
        public const string Register = "{00D5A14C-DCE2-4C6F-BBF8-E554D3482B76}";

        //Toolbar CTA item id's
        public const string RequestAQuote = "{D1E47FAD-BCEA-4602-95D0-4256A03E0076}";
        public const string GetSupport = "{D736FF69-A7FA-4C25-A926-72E0C37E0FA5}";
        public const string BuyCta = "{FE0C1873-C235-40E3-BFAF-19B6C36BC36F}";
        public const string EmailShareItem = "{7ABBEC3E-9A6A-4AB2-956F-02F1401F4B91}";

        public const string MenuText = "{65FF3A64-A428-4B65-A262-EDDB255FABB2}";
        public const string SearchFilterFolder = "{5F38D725-4DF7-4522-A668-8DF8F56BD873}";
        public const string Description = "{AF6B3875-D9FA-4E41-AB38-327243AE0E6A}";

        public const string InfiviSNSFooter = "{630838C0-036B-45EA-B778-855486F9E9D1}";
    } 

    public static class FieldIds
    {
        public const string SelectProduct = "{92CC0A07-4718-42CD-83D8-6C03C6D2DF7B}";
    }

    public static class CookieVariables
    {
        public const string IsCountryConfirmed = "IsCC";
        public const string UserCountryCode = "UCC";
        public const string IPAddressCountryCode = "ICC";
        public const string ConfirmedCountryCode = "CCC";
        public const string IsIPDetected = "IID";
        public const string UserSelectedCountry = "USC";
    }

    public static class SessionVariables
    {
        public const string IsCountryConfirmed = "IsCC";
        public const string UserCountryCode = "UCC";
        public const string IPAddressCountryCode = "ICC";
        public const string TrainingCartKey = "TrainingCartKey";
        public const string TrainingAttendees = "TraningAttendees";
        public const string TrainingBackPageTrack = "TrainingBackPageTrack";
        public const string AttendeesDeleted = "AttendeesDeleted";
        public const string CartItems = "CartItems";
        public const string UserLoggedIn = "User";
    }

    public static class StaticVariables
    {
        public const string DriversSoftwareDownloads = "drivers-software-downloads";
        public const string DownloadsIndex = "anritsu_downloads";
        public const string DocumentLibrary = "document-library";
        public const string FrequentQuestionsIndex = "anritsu_faqs";
        public const string VideoGalleryIndex = "anritsu_infivis_brightcovevideotemplates";
        public const string TrainingCheckoutAdmin = "TRAINING-CHECKOUT-ADMIN-EMAIL";
        public const string TrainingCheckoutUser = "TRAINING-CHECKOUT-USER-EMAIL";
    }
}