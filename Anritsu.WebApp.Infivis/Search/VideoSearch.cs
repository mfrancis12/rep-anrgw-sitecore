﻿using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.Infivis.Search
{
    public class VideoCategoryItemEqualityComparer : IEqualityComparer<Item>
    {
        // Items are equal if their names and Item numbers are equal.
        public bool Equals(Item x, Item y)
        {
            //Check whether the Items' ID's are equal.
            return x.ID == y.ID;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(Item obj)
        {
            //Get hash code for the ID 
            return obj.ID.GetHashCode();
        }
    }

    public class VideoCategoryBase
    {
        protected Item GetRoot(Item item)
        {
            if (item.TemplateID.ToString() == TemplateIds.VideoCategory)
                return item.Parent.Parent;

            return null;
        }

        protected Item GetCategory(Item item)
        {
            if (item.TemplateID.ToString() == TemplateIds.VideoCategory)
                return item;

            return null;
        }

        protected Item GetParentCategory(Item item)
        {
            if (item.TemplateID.ToString() == TemplateIds.VideoCategory)
                return item.Parent;

            return null;
        }

        protected List<Item> GetVideoCategories(Item item)
        {
            if (string.IsNullOrWhiteSpace(item["Video Category"])) return new List<Item>();
            string[] list = item["Video Category"].Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            return list.Select(i => DB.Items[new Sitecore.Data.ID(i)]).ToList();
        }

        protected Sitecore.Data.Database DB
        {
            get
            {
                return Sitecore.Configuration.Factory.GetDatabase("web");
            }
        }
    }

    public class VideoRootCategory : VideoCategoryBase,IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (item.TemplateID.ToString() != TemplateIds.Video)
                return null;

            if (string.IsNullOrEmpty(item["Video"]))
                return null;

            return GetRootCategories(item);
        }


        private IEnumerable<ID> GetRootCategories(Item videoItem)
        {
            List<Item> rootCategories = new List<Item>();
            using (new Sitecore.Globalization.LanguageSwitcher(videoItem.Language))
            {
                rootCategories.AddRange((from category in GetVideoCategories(videoItem)
                                           let rootItem = GetRoot(category)
                                           where rootItem != null
                                           select rootItem).ToList().Distinct(new VideoCategoryItemEqualityComparer()).ToList<Item>());

                return rootCategories.Select(i => i.ID);
            }
        }       

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    public class VideoCategory : VideoCategoryBase,IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (item.TemplateID.ToString() != TemplateIds.Video)
                return null;

            if (string.IsNullOrEmpty(item["Video"]))
                return null;

            return GetCategories(item);
        }

        private IEnumerable<ID> GetCategories(Item videoItem)
        {
            List<Item> categories = new List<Item>();
            using (new Sitecore.Globalization.LanguageSwitcher(videoItem.Language))
            {
                categories.AddRange((from category in GetVideoCategories(videoItem)
                                         let categoryItem = GetCategory(category)
                                         where categoryItem != null
                                         select categoryItem).ToList().Distinct(new VideoCategoryItemEqualityComparer()).ToList<Item>());

                return categories.Select(i => i.ID);
            }
        }


        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }

    public class VideoParentCategory : VideoCategoryBase,IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
                return null;

            if (item.TemplateID.ToString() != TemplateIds.Video)
                return null;

            if (string.IsNullOrEmpty(item["Video"]))
                return null;

            return GetSubCategories(item);
        }

        private IEnumerable<ID> GetSubCategories(Item videoItem)
        {
            List<Item> subCategories = new List<Item>();
            using (new Sitecore.Globalization.LanguageSwitcher(videoItem.Language))
            {
                subCategories.AddRange((from category in GetVideoCategories(videoItem)
                                     let categoryItem = GetParentCategory(category)
                                     where categoryItem != null
                                     select categoryItem).ToList().Distinct(new VideoCategoryItemEqualityComparer()).ToList<Item>());

                return subCategories.Select(i => i.ID);
            }
        }

        public string FieldName
        {
            get;
            set;
        }

        public string ReturnType
        {
            get;
            set;
        }
    }
}