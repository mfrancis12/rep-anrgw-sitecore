﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutAnritsuGalleryDetailStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.AboutAnritsuGalleryDetailStatic" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="full-container video-container nogap">
    <sc:Sublayout runat="server" Id="BrightcoveMediaControl" Path="/layouts/MediaFramework/Sublayouts/EmbedMediaPlayer.ascx"></sc:Sublayout>
</div>
<div class="full-container video-info nogap">
    <div class="container nogap">
        <div class="video-basic">
            <p>
                <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "duration") %>: <%=!string.IsNullOrEmpty(VideoItem(Model.Content.Video).Length)? GetDuration(VideoItem(Model.Content.Video).Length) : "" %><br/>
                <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","releasedate") %>: <%= !string.IsNullOrEmpty(VideoItem(Model.Content.Video).PublishedDate)?Convert.ToDateTime(VideoItem(Model.Content.Video).PublishedDate).ToFormattedDate():"" %><br />
                <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","views") %>: <%=VideoItem(Model.Content.Video).PlaysTotal %>
            </p>
        </div>
        <div class="video-gallery">
            <div class="video-link">
                <div class="title">
                    <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","RelatedLinks") %></span>
                    <div class="icon icon-arrow-down"></div>
                </div>
                <div class="sub-links">
                    <asp:Repeater ID="RelatedLinks" runat="server" OnItemDataBound="RelatedLinks_ItemDataBound">
                        <ItemTemplate>
                            <a id="relatedLink" runat="server"></a>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="video-left-intro">
        <div class="left-content">
            <h1>
                <%=Editable(Model,x=>x.Content.Name)%>
            </h1>
            <%=Editable(Model, x=>x.Content.LongDescription) %>
        </div>
    </div>
    <div class="video-right-related">
        <div class="hd">
            <p><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","RelatedVideos") %></p>
        </div>
        <asp:Repeater ID="RelatedVideos" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Pages.IVideodetailStatic">
            <ItemTemplate>
                <div class="item">
                    <div class="img">
                        <a href="<%# Item.Url %>">
                            <img alt="" src="<%# VideoItem(Item.Video).ThumbnailURL %>">
                        </a>
                    </div>
                    <div class="video-related">
                        <p class="intro">
                            <a href="<%# Item.Url %>">
                                <%# Editable(Item,x=>x.Name) %>
                            </a>
                        </p>
                        <p class="duration">
                            <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","duration") %>:<%#!string.IsNullOrEmpty(VideoItem(Item.Video).Length)? GetDuration(VideoItem(Item.Video).Length) : "" %> <br />
                            <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","releasedate") %>: <%# !string.IsNullOrEmpty(VideoItem(Item.Video).PublishedDate)?Convert.ToDateTime(VideoItem(Item.Video).PublishedDate).ToFormattedDate():"" %>
                        </p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>

        <div class="more-video">
            <a href="<%= SitecoreContext.GetItem<Anritsu.WebApp.GlobalWeb.Models.IModelBase>(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.VideoGallery).Url %>">
                <div class="icon icon-video"></div>
                &nbsp;
                  &nbsp;
                  <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","videogallery") %></a>
        </div>
    </div>
</div>
