﻿using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using Sitecore.Data;
using Sitecore.Configuration;
using Sitecore.Search;
using Sitecore.Data.Managers;
using System.Collections.ObjectModel;
using System.Linq;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using System.Web.UI.HtmlControls;
using Sitecore.SharedSource.FieldSuite.Types;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models;
using Glass.Mapper.Sc;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class AboutAnritsuGalleryDetailStatic : GlassUserControl<IAboutGalleryDetailMapping>
    {
        #region Global Variables
        Database master = Factory.GetDatabase("master");
        string indexName = "anritsu_brightcovevideotemplates";
        //public Item videoItem;
        #endregion

        private void Page_Load(object sender, EventArgs e)
        {
            RelatedVideos.DataSource = Model.Content.RelatedVideos;
            RelatedVideos.DataBind();

            BindRelatedLinks();
            BrightcoveMediaControl.DataSource = !string.IsNullOrEmpty(Model.Content.Video) ? VideoItem(Model.Content.Video).Id.ToString() : string.Empty;
        }

        private void BindRelatedLinks()
        {
            Item currentItem = SitecoreContext.GetCurrentItem<Item>();

            string itemId = currentItem["Content"];

            Item globalItem = Sitecore.Context.Database.GetItem(itemId);


            //Item currentItem = SitecoreContext.GetCurrentItem<Item>();
            //if (currentItem["RelatedLinks"] != null)
            //{
            //    GeneralLinks relatedGeneralLinks = new GeneralLinks(currentItem, "RelatedLinks");
            //    RelatedLinks.DataSource = relatedGeneralLinks.LinkItems;
            //    RelatedLinks.DataBind();
            //}
            if (Model.Content != null && Model.Content.RelatedLinks != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(SitecoreContext.GetItem<Item>(Model.Content.Id), "RelatedLinks");
                RelatedLinks.DataSource = relatedGeneralLinks.LinkItems;
                RelatedLinks.DataBind();
            }
        }

        public string GetDuration(string videoLength)
        {
            string duration = string.Empty;
            if (!string.IsNullOrEmpty(videoLength))
            {
                TimeSpan time = TimeSpan.FromMilliseconds(Convert.ToInt32(videoLength));
                duration = time.ToString(@"hh\:mm\:ss");
            }
            return duration;
        }

        private string BindBrightcoveMedia()
        {
            var videoId = string.Empty;
            var selectedVideo = SitecoreContext.GetCurrentItem<Item>().Fields["Video"].Value;
            Item mediaItem = master.GetItem(selectedVideo, LanguageManager.GetLanguage("en"));
            if (mediaItem != null)
            {
                videoId = mediaItem.ID.ToString();
            }
            return videoId;
        }
        public IBrightcoveVideo VideoItem(string videoId)
        {
            if (videoId != null)
            {
                var contextService = new SitecoreContext();
                return contextService.GetItem<IBrightcoveVideo>(videoId, LanguageManager.GetLanguage("en"));
            }
            return null;
        }

        protected void RelatedLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("relatedLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }
    }
}