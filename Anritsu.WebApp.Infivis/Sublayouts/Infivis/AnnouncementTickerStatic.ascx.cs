﻿using System;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using System.Collections.Generic;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Sitecore.Data.Items;
using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.SitecoreUtilities.Helpers;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Anritsu.WebApp.GlobalWeb.ContentSearch;
using Sitecore.Data;
using Anritsu.WebApp.Infivis.Models.Components;
using System.Web;
using System.Data;
using Anritsu.Library.GWCMSDK;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
  /// <summary>
  /// Annoucement Ticker
  /// </summary>
    public partial class AnnouncementTickerStatic : GlassUserControl<IAnnouncementTickerStatic>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
            GetTickerFromCMS();
        }



        private void GetTickerFromCMS()
        {
            try
            {
                var url = HttpContext.Current.Request.Url.AbsolutePath;

                DataTable dt = null;
                dt = GWCMServiceManager.GetTickers(url);


                if (dt != null && dt.Rows.Count > 0)
                {
                    ticker.Visible = true;
                    pimticker.DataSource = dt;
                    pimticker.DataBind();

                }
                else
                {
                    ticker.Style.Add("display", "none");
                }


            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Ticker Error" + ex.Message, this);
                ticker.Style.Add("display", "none");
            }


        }


    }
}