﻿using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class CarouselStatic : GlassUserControl<ICorporateHomeStatic>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            RepeaterCarousel.DataSource = Model.HeroCarousel;
            RepeaterCarousel.DataBind();

        }

    }
}