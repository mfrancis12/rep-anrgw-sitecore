﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeRegion.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ChangeRegion" %>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<div class="region">
<div class="container region-container">
    <div class="region-form">
        <div class="region-heading">
            <h1><%= Editable(x=>x.PageTitle) %></h1>
            <div class="form-container">
                <div class="bd">
                    <div style="border: 0" class="group input-select select-region">
                        <asp:DropDownList ID="ddlSelectRegion" runat="server" OnSelectedIndexChanged="ddlSelectRegion_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel" runat="server">
            <ContentTemplate>
                <div class="country">
                    <asp:Repeater ID="SelectCountries" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Global.IISOCountry" >
                        <ItemTemplate>
                            <div class="country-item">                             
                               <a href='<%# GetUrl(Item.Id.ToString()) %>' data-country-code="<%# Item.CountryCode %>"><%# Item.CountryName %></a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger  ControlID="ddlSelectRegion"/>
            </Triggers>
        </asp:UpdatePanel>
        
    </div>
</div>
</div>
