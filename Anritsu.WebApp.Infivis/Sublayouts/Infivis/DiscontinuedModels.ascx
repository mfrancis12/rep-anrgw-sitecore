﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DiscontinuedModels.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.DiscontinuedModels" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:ScriptManager ID="DiscontinuedScriptManager" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="DiscontinuedUpdatePanel" runat="server" >
                <ContentTemplate>
<div class="container">
      <div class="content-detail-table-form">
         <div class="content-detail-table">
            <%=Editable(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IStaticPageWithContentRegional>(), x=>x.Content)%>
             <hr />
             <p class="tip"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "model-number-search-tip") %></p>
            <asp:TextBox ID="SearchBox" runat="server" CssClass="group" />
            <asp:Button ID="Search" runat="server"  CssClass="search-button" OnClick="Search_Click"></asp:Button>
            <h2><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "SearchResults") %></h2>
            <table border="0">
                <tr class="table-head">
                    <th align="left"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ModelNumber") %></th>
                    <th align="left"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Description") %></th>
                </tr>

                <asp:ListView ID="DiscontinuedModelsList" runat="server" OnPagePropertiesChanged="DiscontinuedModelsList_PagePropertiesChanged" ItemType="Anritsu.WebApp.Infivis.Models.Pages.IProductDetailStaticC1">
                    <ItemTemplate>
                        <tr>
                            <td class="col1"><a href="<%#Item.Url %>"><span><%#Editable(Item,x=>x.SelectProduct.ModelNumber) %></span></a></td>
                            <td class="col2"><a href="<%#Item.Url %>"><span><%#Editable(Item,x=>x.SelectProduct.ProductDescriptor) %></span></a></td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
           <div class="seprate-line" style="padding-top:20px;height:1px;"></div>
            <div class="pagination">
                <span class="page">
                    <asp:DataPager ID="DiscontinuedModelsDataPager" runat="server" PagedControlID="DiscontinuedModelsList" PageSize="20">
                        <Fields>
                            <asp:TemplatePagerField>
                                <PagerTemplate>
                                    <span class="info">
                                        <%# Container.TotalRowCount > 0 ? (Container.StartRowIndex + 1) : 0 %>-<%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%>/ <%# Container.TotalRowCount %><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "results")%>                                                                 
                                    </span>
                                </PagerTemplate>
                            </asp:TemplatePagerField>
                            <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active floatLft" NextPageText=">" PreviousPageText="<" />
                        </Fields>
                    </asp:DataPager>
                </span>
            </div>
        </div>
        <div class="sidebar">
            <p class="desktop"><%= RenderImage(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IStaticPageWithContentRegional>(),x=>x.Image,new {@class="img100"},isEditable:true) %></p>
            <p class="mobile"><%= RenderImage(SitecoreContext.GetCurrentItem<Anritsu.WebApp.GlobalWeb.Models.Pages.IStaticPageWithContentRegional>(),x=>x.Image,new {@class="img100"},isEditable:true) %></p>
                <asp:Repeater ID="OptionalLinks" runat="server" OnItemDataBound="OptionalLinks_ItemDataBound" OnPreRender="OptionalLinks_PreRender">
                    <HeaderTemplate> <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3></HeaderTemplate>
                    <ItemTemplate>
                        <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                    </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
</ContentTemplate></asp:UpdatePanel>
