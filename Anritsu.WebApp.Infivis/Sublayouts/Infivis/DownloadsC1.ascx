﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DownloadsC1.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.DownloadsC1" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components.Extensions" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<asp:ScriptManager ID="downloadScriptManager" runat="server"></asp:ScriptManager>
<div class="container">
    <h1 class="download-center-head"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "downloadcenter") %></h1>
    <div class="search-model">
        <span><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "searchbymodel") %></span>
        <asp:TextBox ID="txtProductNumber" runat="server"></asp:TextBox>
        <asp:Button ID="SearchButton" runat="server" Text="Search" CssClass="button" OnClick="SearchButton_Click" />
    </div>
    <div class="container download-center-container">
        <div class="tab-interface hero-tab download-center-tabs-container">
            <ul>
                <li>
                    <a class="download-center-tabs" href="#downloadLibrary">
                        <h2><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "documentlibrary") %></h2>
                    </a>
                </li>
                <li>
                    <a class="active download-center-tabs" href="#downloadSoftware">
                        <h2><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "drivers") %></h2>
                    </a>
                </li>
                <li class="<%= Sitecore.Context.Language.Name.ToLower().Equals("ja-jp") ? "hiddentxt" : string.Empty %>">
                    <a href="#faq" class="download-center-tabs">
                        <h2><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "faq") %></h2>
                    </a>
                </li>
            </ul>
            <asp:UpdatePanel ID="DownloadLibraryUpdatePanel" runat="server">
                <ContentTemplate>
                    <div class="section content-list" id="downloadLibrary">
                        <h3 class="list-type"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "bydocumenttype") %></h3>
                        <ul>
                            <asp:Repeater ID="DownloadLibraryTypes" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue" OnItemCommand="DownloadLibraryTypes_OnItemCommand">
                                <ItemTemplate>
                                    <li>
                                        <asp:LinkButton ID="LinkButtonDownloadLibraryCategoryFilter" runat="server" CommandName="Click" CommandArgument="<%# Item.Value %>"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</asp:LinkButton>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                        <div>
                            <asp:ListView ID="DownloadLibraryList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.DownloadSearchItem">
                                <ItemTemplate>
                                    <div class="download-item">
                                        <h4><b>
                                            <asp:HyperLink runat="server" id="LibraryFileDownloadLink" Target="_blank" NavigateUrl="<%# Item.GetItemUrl()%>" onclick="<%# GaTrackingScript(Item.GetItemUrl()) %>">
                                                <%# Editable(Item,x=>x.Title) %>
                                            </asp:HyperLink>
                                        </b></h4>
                                        <asp:HyperLink runat="server" id="LibraryFileNewVersionDownloadLink" Target="_blank" CssClass="new-version" Visible="<%# Item.IsNewVersionAvailable %>" NavigateUrl="<%# Item.GetReplacementItemUrl()%>"  onclick="<%# GaTrackingScript(Item.GetReplacementItemUrl()) %>">
                                            <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "newerversionavailable") %>
                                        </asp:HyperLink>
                                        <p><%# Editable(Item,x=>x.Description) %></p>
                                        <div class="details">
                                            <span><%# Editable(Item,x=>x.Subcategory.Value) %></span>
                                            <span class="pdf-icon"><%# Editable(Item,x=>x.FileType) %> <%# Item.Size %></span>
                                            <span>Version: <%# Editable(Item,x=>x.Version) %></span>
                                            <span><%# Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToFormattedDate()) %></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                    <div class="download-item-wrapper"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "norecordsfound") %></div>
                                </EmptyDataTemplate>
                            </asp:ListView>
                        </div>
                        <div class="pagination">
                            <span class="page">
                                <span class="info"><%= GetDownloadLibraryPaginationName() %></span>
                                <asp:PlaceHolder ID="placeHolderDownloadLibraryPager" runat="server"></asp:PlaceHolder>
                            </span>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="DownloadSoftwareUpdatePanel" runat="server">
                <ContentTemplate>
                    <div class="section active content-list" id="downloadSoftware">
                        <h3 class="list-type"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "bydocumenttype") %></h3>
                        <ul>
                            <asp:Repeater ID="DownloadSoftwareTypes" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue" OnItemCommand="DownloadSoftwareTypes_OnItemCommand">
                                <ItemTemplate>
                                    <li>
                                        <asp:LinkButton ID="LinkButtonDownloadSoftwareCategoryFilter" runat="server" CommandName="Click" CommandArgument="<%# Item.Value %>"><%#Item.DisplayName%> (<%#Item.DocumentCount %>)</asp:LinkButton>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                        <div>
                            <asp:ListView ID="DownloadSoftwareList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.DownloadSearchItem">
                                <ItemTemplate>
                                    <div class="download-item">
                                        <h4><b><a href="<%# Item.GetItemUrl() %>"><%# Editable(Item,x=>x.Title) %></a></b></h4>
                                        <a href="<%# Item.GetReplacementItemUrl() %>" class="new-version"><%# Item.IsNewVersionAvailable ? Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "newerversionavailable") : string.Empty %></a>
                                        <p><%# Editable(Item,x=>x.Description.GetHtmlDecodedString()) %></p>
                                        <div class="details">
                                            <span><%# Editable(Item,x=>x.GetDownloadCategory()) %></span>
                                            <span><%# Editable(Item,x=>x.FileType) %> <%# Item.Size %></span>
                                            <span>Version: <%# Editable(Item,x=>x.Version) %></span>
                                            <span><%# Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToFormattedDate()) %></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                    <div class="download-item-wrapper"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "norecordsfound") %></div>
                                </EmptyDataTemplate>
                            </asp:ListView>
                        </div>
                        <div class="pagination">
                            <span class="page">
                                <span class="info"><%= GetDownloadSoftwarePaginationName() %></span>
                                <asp:PlaceHolder ID="placeHolderDownloadSoftwarePager" runat="server"></asp:PlaceHolder>
                            </span>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="FrequentQuestionUpdatePanel" runat="server">
                <ContentTemplate>
                    <div class="<%= Sitecore.Context.Language.Name.ToLower().Equals("ja-jp") ? "hiddentxt" : string.Empty %>">
                        <div class="section content-list" id="faq">
                            <h3 class="list-type"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "byproductcategory") %></h3>
                            <ul id="accordion">
                                <asp:Repeater ID="DownloadFAQCategories" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                    <ItemTemplate>
                                        <li>
                                            <span class="accordion-list link"></span>
                                            <asp:LinkButton ID="LinkButtonDownloadFrequentQuestionCategoryFilter" runat="server" CssClass="" OnClick="LinkButtonDownloadFrequentQuestionCategoryFilter_OnClick" CommandArgument="<%# Item.Value %>"><%#Item.DisplayName%></asp:LinkButton>
                                            <%--<a href="#" class="type-item"></a>--%>
                                            <ul class="sub-slider">
                                                <asp:Repeater ID="DownloadFAQSubcategories" runat="server" DataSource='<%# GetHierarchicalFacets(Item) %>' ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                                    <ItemTemplate>
                                                        <li>
                                                            <span class="accordion-list link"></span>
                                                            <asp:LinkButton ID="LinkButtonDownloadFrequentQuestionSubcategoryFilter" runat="server" CssClass="" OnClick="LinkButtonDownloadFrequentQuestionSubcategoryFilter_OnClick" CommandArgument="<%# Item.Value %>"><%#Item.DisplayName%></asp:LinkButton>
                                                            <%--<a href="#" class="type-item"></a>--%>
                                                            <ul class="sub-slider">
                                                                <asp:Repeater ID="DownloadFAQFamlies" runat="server" DataSource='<%# GetHierarchicalFacets(Item) %>' ItemType="Anritsu.WebApp.GlobalWeb.Search.FacetValue">
                                                                    <ItemTemplate>
                                                                        <li>
                                                                            <asp:LinkButton ID="LinkButtonDownloadFrequentQuestionFamilyFilter" runat="server" CssClass="link" OnClick="LinkButtonDownloadFrequentQuestionFamilyFilter_OnClick" CommandArgument="<%# Item.Value %>"><%#Item.DisplayName%></asp:LinkButton>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                            <div>
                                <asp:ListView ID="DownloadFrequentQuestionList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Pages.FrequentlyAskedQuestionSearchItem">
                                    <ItemTemplate>
                                        <div class="download-item">
                                            <h4><b><a href="<%# GetFrequentlyAskedQuestionUrl(Item.Path) %>"><%# Item.Question %></a></b></h4>
                                            <div class="paddingTop">
                                                <span><%# Item.Answer %></span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <div class="download-item-wrapper"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "norecordsfound") %></div>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                            </div>
                            <div class="pagination">
                                <span class="page">
                                    <span class="info"><%= GetFaqPaginationName() %></span>
                                    <asp:PlaceHolder ID="placeHolderFaqPager" runat="server"></asp:PlaceHolder>
                                </span>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
