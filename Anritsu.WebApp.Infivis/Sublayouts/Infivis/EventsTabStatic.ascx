﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventsTabStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.EventsTabStatic" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components" %>
<%@ Import Namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>

<div id="section3" class="section">
    <asp:Repeater ID="EventsList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IEvents">
        <ItemTemplate>
            <h3><span class="date"><%#Editable(Item,x=>x.StartDate,x=>x.StartDate.ToLocalTime().ToFormattedDate())+ (Item.EndDate.Equals(DateTime.MinValue)?"":" - "+ Editable(Item,x=>x.EndDate,x=>x.EndDate.ToLocalTime().ToFormattedDate()))%></span>
                <a href="<%# Item.IsExternalUrl()? Item.ExternalLandingLink.Url: Item.Url %>" target="<%# Item.IsExternalUrl()?Item.ExternalLandingLink.Target:string.Empty %>"><%# Editable(Item, x=>x.Title.Truncate(100,"...")) %></a></h3>
        </ItemTemplate>
    </asp:Repeater>
    <div id="divItems" runat="server"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "noupcomingevents") %></div>
    <p class="align-right"><a href="<%=GetMoreLink()%>" class="more"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "More") %> <i class="icon icon-arrow-blue"></i></a></p>
</div>

