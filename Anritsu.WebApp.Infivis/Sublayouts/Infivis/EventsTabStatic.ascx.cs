﻿﻿using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.GlobalWeb.ContentSearch;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Anritsu.WebApp.SitecoreUtilities.Helpers;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;



namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class EventsTabStatic : GlassUserControl<IEventsSelectionStatic>
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            try
            {
                BindEvents();
            }
            catch (Exception ex)
            {
                Response.Write("EventsTab Says<br/>" + ex.Message);
                Log.Error(ex.StackTrace, ex, GetType());
            }

        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private void BindEvents()
        {
            //pass contract to content search method SearchByContract()
            var eventResults = Model.EventItems
                //convert content search result items to IEvents glass item
                .Select(x => SitecoreContext.GetItem<IEvents>(x.Id))
                //order by start date 
                .OrderBy(x => x.StartDate)
                //list it
                .ToList();

            //eventList
            EventsList.DataSource = eventResults.Where(x => x != null).OrderBy(x => x.EndDate).ThenBy(x => x.StartDate).Distinct().Take(5);
            EventsList.DataBind();
            divItems.Visible = EventsList.Items.Count == 0;
        }

        public string GetMoreLink()
        {
            string url = string.Empty;
            //if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.Home)
            {
                var item = SitecoreContext.GetItem<IModelBase>(ItemIds.HomeEventsPage);
                if (item != null) return item.Url;
            }
            //else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.TestMeasurement)
            //{
            //    var item = SitecoreContext.GetItem<IModelBase>(ItemIds.TMEventsPage);
            //    if (item != null) return item.Url;
            //}
            return url;
        }
    }
}