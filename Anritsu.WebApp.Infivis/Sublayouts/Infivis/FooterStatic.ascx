﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.FooterStatic" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%--<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components.Extensions" %>--%>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>

<!--googleoff: all-->
<div class="container">
    <div class="min-container">
        <div class="site-links">
            <asp:Repeater runat="server" ID="FatFooter" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                <ItemTemplate>
                    <div class="col">
                        <div class="title">

                            <%# Editable(Item, x => x.Value) %>
                        </div>
                        <asp:Repeater runat="server" ID="ProductLinks" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.ILink" DataSource="<%#BindData(Item) %>">
                            <ItemTemplate>

                                <div class="link">

                                    <%# RenderLink(Item, x=>x.Link, isEditable: true, contents:Editable(Item,x=>x.Title) + "<div class='" + GetClass(Item) + "'></div>")%>

                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="social-links">
             <asp:Repeater runat="server" ID="SNSFooter" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IImageTitleWithLink" >
<ItemTemplate>
    <div class="social-icon"><a href="<%#Item.Link.Url%>" target="_blank" >
    <img src="<%#Item.Image.Src%>"/>
</a></div>
</ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="site-info">
            <div class="left">
                <asp:Repeater runat="server" ID="FooterLinks" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.ILink">
                    <ItemTemplate>

                        <%# RenderLink(Item,x=>x.Link, isEditable: true)%>
                    </ItemTemplate>
                </asp:Repeater>


                 <div id='teconsent' consent="null" style="font-family:Segoe UI,Helvetica Neue,Helvetica,Arial, sans-serif; font-size:14px; font-weight:bold; background:transparent; color:#FFF">
                    <sc:Text runat="server" Field="Script" ID="cookiePreferences" DataSource="/sitecore/content/GlobalWeb/components/Scripts/cookie-preferences"></sc:Text>
                </div>
            </div>

            <div class="copy-right">
                <div class="icon icon-logo-small"></div>
                <br>
                <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Copyright")%> <%=DateTime.Now.ToString("yyyy") %>
                <br>
                <%--<%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Subsidary") %>--%>
                <%--<br>--%>
                <%if (SitecoreContext.GetCurrentItem<Sitecore.Data.Items.Item>()!=null 
                    && SitecoreContext.GetCurrentItem<Sitecore.Data.Items.Item>().Language.Name.ToUpperInvariant().Equals("ZH-CN") 
                    && !string.IsNullOrEmpty(SitecoreContext.GetItem<Sitecore.Data.Items.Item>(Anritsu.WebApp.GlobalWeb.Constants.ItemIds.ICPNumber)["Phrase"]))
                  { %>
                <%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "chinese-only-contact-group-company-information") %>
                <% }  %>
            </div>
        </div>
    </div>
</div>
<sc:sublayout id="Toolbar" runat="server" path="/GlobalWeb/Sublayouts/ToolBar.ascx"></sc:sublayout>

<!--googleon: all-->

