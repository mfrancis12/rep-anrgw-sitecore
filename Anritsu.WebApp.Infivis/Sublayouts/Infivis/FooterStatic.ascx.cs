﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.Infivis.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.SiteSearch;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class FooterStatic : GlassUserControl<IModelBase>
    {
        private string _dataSource;

        public string DataSource
        {

            get
            {
                if (_dataSource == null)
                {
                    var parent = Parent as Sublayout;
                    if (parent != null)
                        _dataSource = (parent.DataSource);
                }
                return _dataSource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                FooterLinks.DataSource = SitecoreContext.GetItem<IModelBase>(ItemIds.FooterLinks).GetChildren<ILink>();
                FooterLinks.DataBind();

                List<IDropListItem> fatfooterTitle = new List<IDropListItem>();

                Sitecore.Data.Items.Item rootItem = SitecoreContext.Database.GetItem(new Sitecore.Data.ID(DataSource));

                List<Sitecore.Data.Items.Item> items = rootItem.Axes.GetDescendants().Where(x => x.TemplateID.ToString().Equals("{F547E7B1-B7B9-41AD-8958-4756BD7A5633}")).ToList();
                fatfooterTitle.AddRange(items.Select(i => i.GlassCast<IDropListItem>()));

                FatFooter.DataSource = fatfooterTitle;
                FatFooter.DataBind();

                try
                {
                    SNSFooter.DataSource = SitecoreContext.GetItem<IModelBase>(ItemIds.InfiviSNSFooter).GetChildren<IImageTitleWithLink>();
                    SNSFooter.DataBind();
                }
                catch(Exception footerex)
                {
                    Sitecore.Diagnostics.Log.Error(footerex.Message, footerex);
                }

               
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message,ex);
                Visible = false;
            }

        }
        public IEnumerable<ILink> BindData(IModelBase item)
        {
            var links = item.GetChildren<ILink>().Take(7);
            return links;
        }

        protected string GetClass(ILink item)
        {
            if (item.Link != null && !string.IsNullOrEmpty(item.Link.Target))
            {
                if (item.Link.Target.ToUpperInvariant().Equals("NEW BROWSER"))
                {
                    return "icon icon-outbound";
                }
            }
            return string.Empty;
        }


    }

}
