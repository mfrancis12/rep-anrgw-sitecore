﻿using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.GlobalWeb.ContentSearch;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Anritsu.WebApp.SitecoreUtilities.Helpers;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class IRNews : GlassUserControl<IBaseTemplate>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            try
            {
                BindIRNews();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, ex);
                Visible = false;
            }
        }
        public void BindIRNews()
        {
            //var query = string.Format("fast:{0}//*[@@templateid='{1}']", StringExtensions.EscapePath(SitecoreContext.GetItem<Item>(Model.Id).Paths.FullPath), TemplateIds.IRNews);
            //var irnewsList = SitecoreContext.Query<Item>(query).Select(x => SitecoreContext.GetItem<IIRNews>(x.ID.Guid)).ToList();
            //var irnews = irnewsList.Where(x => x != null && !x.ReleaseDate.Equals(DateTime.MinValue)).OrderByDescending(x => x.ReleaseDate).Take(5).ToList();


            //3chillies AW: NEW CODE 2016.05.19  
            NewsSearchContract newsContract = new NewsSearchContract();
            newsContract.TemplateRestrictions.Add(TemplateIds.IRNews);

            //get ordered results from repo/index select 5
            var newsResults = new NewsRepository().SearchNewsByContract(newsContract, 5, 0, true)
                .Where(x => x.Document.GetItem() != null)
                .Select(x => SitecoreContext.GetItem<IIRNews>(x.Document.GetItem().ID.ToGuid()));

            IRNewsList.DataSource = newsResults;
            IRNewsList.DataBind();
        }
        public string GetMoreLink()
        {
            return SitecoreContext.GetItem<Item>(ItemIds.IRNewsPage) != null ? SitecoreContext.GetItem<IModelBase>(ItemIds.IRNewsPage).Url : "";
        }
    }
}