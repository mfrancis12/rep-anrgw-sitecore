﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LinkManagerPromotionsStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.LinkManagerPromotionsStatic" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Pages.Extensions" %>


<div class="container">
   <div class="hero-menu">
       <sc:Text ID="PromotionData" runat="server" Field="LinkPromotions" />
    </div>
</div>