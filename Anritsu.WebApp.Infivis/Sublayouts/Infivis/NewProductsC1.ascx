﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewProductsC1.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.NewProductsC1" %>
<div id="section2" class="section <%=Sitecore.Context.Item.TemplateID.ToString().Equals(Anritsu.WebApp.Infivis.Constants.TemplateIds.ProductCategory)?"active":""%>">
    <div class="new-products-slider slick">
        <asp:Repeater ID="ProductList" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Pages.IProductDetailStaticC1">
            <ItemTemplate>
                <div class="slider">
                    <h2 class="image"><a href="<%#Item.Url %>"><%#RenderImage(Item,x=>x.SelectProduct.Thumbnail) %></h2>
                    <h2 class="title"><a href="<%#Item.Url %>"><%#Item.SelectProduct.ProductName%></a></h2>
                    <div class="sub-title"><a href="<%#Item.Url %>"><%#Item.SelectProduct.ModelNumber%></a></div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
