﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Anritsu.WebApp.Infivis.Constants;
using Sitecore;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc;
using System.Collections.Generic;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class NewProductsC1 : GlassUserControl<IModelBase>
    {
        string indexName = "anritsu_products";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;

            var sitecoreService = new SitecoreService(Sitecore.Context.Database.Name);
            //var regionalProducts = new List<ProductSearchItemC1>();
            //using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
            //{
            //    var query = context.GetQueryable<ProductSearchItemC1>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(i => i.Language == Sitecore.Context.Language.Name && !i.IsDiscontinued && i.WhatSNew);
            //    foreach (var item in query)
            //    {
            //        sitecoreService.Map(item);
            //        regionalProducts.Add(item);
            //    }
            //    regionalProducts = regionalProducts.Where(x => x != null && x.SelectProduct!=null &&SitecoreContext.GetItem<Item>(x.Path).Axes.IsDescendantOf(SitecoreContext.GetItem<Item>(Model.Id.ToString()))).ToList();
            //    ProductList.DataSource = regionalProducts;
            //    ProductList.DataBind();
            //}
        }

    }
}