﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewProductsStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.NewProductsStatic" %>
<div id="section2" class="section <%=Sitecore.Context.Item.TemplateID.ToString().Equals(Anritsu.WebApp.Infivis.Constants.TemplateIds.ProductCategory)?"active":""%>">
    <div class="new-products-slider slick">                    
        <asp:Repeater ID="ProductList" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Pages.IProductDetailStatic">
            <ItemTemplate>
                <div class="slider">
                    <h2 class="image"><a href="<%#Item.Url %>"><%#RenderImage(Item,x=>x.SelectProduct.Thumbnail) %></h2>
                    <h2 class="title"><a href="<%#Item.Url %>"><%#Item.SelectProduct.ProductName%></a></h2>
                    <div class="sub-title"><a href="<%#Item.Url %>"><%#Item.SelectProduct.ModelNumber%></a></div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
