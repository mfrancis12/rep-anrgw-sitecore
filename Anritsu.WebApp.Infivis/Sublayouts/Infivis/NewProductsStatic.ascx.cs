﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Anritsu.WebApp.Infivis.Constants;
using Sitecore;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper.Sc;
using System.Collections.Generic;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Anritsu.WebApp.Infivis.Models.Components;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class NewProductsStatic : GlassUserControl<INewProductsSelectionStatic>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Model == null)
                return;

            //ProductList.DataSource = Model.NewProducts;

            List<IProductDetailStatic> list = SitecoreContext.GetCurrentItem<INewProductsSelectionStatic>().NewProducts.ToList();
            ProductList.DataSource = SitecoreContext.GetCurrentItem<INewProductsSelectionStatic>()
              .NewProducts.Where(x=>x.SelectProduct!=null)
              .OrderByDescending(i =>i.SelectProduct.ReleaseDate);

            ProductList.DataBind();
        }

    }
}