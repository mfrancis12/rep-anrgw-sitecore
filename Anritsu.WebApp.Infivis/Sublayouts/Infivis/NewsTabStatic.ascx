﻿<%@ control language="C#" autoeventwireup="true" codebehind="NewsTabStatic.ascx.cs" inherits="Anritsu.WebApp.Infivis.SubLayouts.NewsTabStatic" %>
<%@ import namespace="Anritsu.WebApp.Infivis.Models.Pages" %>
<%@ import namespace="Anritsu.WebApp.GlobalWeb.Models.Pages" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div id="section1" class="section <%=Sitecore.Context.Item.ID.ToString().Equals(Anritsu.WebApp.Infivis.Constants.ItemIds.Home)?"active":""%>">
    <asp:Repeater id="NewsReleases" runat="server" ItemType="INewsRelease" >
        <ItemTemplate>           
            <h3><span class="date"><%#Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToLocalTime().ToFormattedDate())%></span><a href="<%# Item.Url%>"><%# Editable(Item, x=>x.Title.Truncate(100,"...")) %> </a></h3>   
        </ItemTemplate>
    </asp:Repeater>
    <p class="align-right"><a href="<%=GetMoreLink %>" class="more"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "More") %> <i class="icon icon-arrow-blue"></i></a></p>
 
</div>
