﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Anritsu.WebApp.SitecoreUtilities.Helpers;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Anritsu.WebApp.GlobalWeb.ContentSearch.Contracts;
using Sitecore.Data;
using Anritsu.WebApp.GlobalWeb.ContentSearch;
using Anritsu.WebApp.Infivis.Models.Components;


namespace Anritsu.WebApp.Infivis.SubLayouts
{

    public partial class NewsTabStatic : GlassUserControl<INewsSelectionStatic>
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            try
            {
                BindNews();
            }
            catch (Exception ex)
            {
                Response.Write("NewsTab Says<br/>" + ex.Message);
                Log.Error(ex.StackTrace, GetType());
            }
        }

        public void BindNews()
        {
            //get ordered results from repo/index select 5
            var newsResults = Model.NewsItems
                .Select(x => SitecoreContext.GetItem<INewsRelease>(x.Id))
                .ToList();


            NewsReleases.DataSource = newsResults.OrderByDescending(x => x.ReleaseDate).Distinct().Take(5);
            NewsReleases.DataBind();
        }
        public string GetMoreLink
        {
            get
            {
                string url = string.Empty;
                try
                {
                    //if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.Home && SitecoreContext.GetItem<IModelBase>(ItemIds.HomeNewsPage) != null)
                    {
                        url = SitecoreContext.GetItem<IModelBase>(ItemIds.HomeNewsPage).Url;
                    }
                    //else if (SitecoreContext.GetCurrentItem<Item>().ID.ToString() == ItemIds.TestMeasurement && SitecoreContext.GetItem<IModelBase>(ItemIds.TMNewsPage) != null)
                    //{
                    //    url = SitecoreContext.GetItem<IModelBase>(ItemIds.TMNewsPage).Url;
                    //}
                }
                catch (Exception ex)
                {
                    Log.Error(ex.StackTrace, GetType());
                }
                return url;
            }
        }
    }
}