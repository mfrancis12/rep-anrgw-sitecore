﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductCategoryCarouselStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductCategoryCarouselStatic" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<div class="full-container nogap">
    <div class="page-heading">
        <div class="inner">
            <div class="heading-text">
                <h1 class="title"><%=Editable(x=> x.PageTitle) %></h1>
            </div>


            <div class="heading-button">
                <asp:Repeater ID="button" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.ILink">
                    <ItemTemplate>
                       <%# @RenderLink(Item, x => x.Link, new System.Collections.Specialized.NameValueCollection { { "target", Item.Link.Target },{"class","button"}}, true, contents: Item.Title)%>
                    </ItemTemplate>
                </asp:Repeater>
            </div>


        </div>
    </div>
</div>

<div class="full-container nogap">
    <div class="category-slider slick">
        <asp:Repeater ID="sliders" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICarousel">
            <ItemTemplate>
                <div class="slider">
                    <div class="product-image">
                        <%#RenderImage(Item,x=>x.Image,isEditable:true) %>
                    </div>
                    <div class="text">
                        <div class="inner">
                            <div class="name <%#Editable(Item,x=>x.GetTextColor()) %>"><%#Editable(Item, x=>x.Title) %></div>
                            <div class="sub-title <%#Editable(Item,x=>x.GetTextColor()) %>"><%#Editable(Item, x=>x.Subtitle) %></div>
                            <div class="description <%#Editable(Item,x=>x.GetTextColor()) %>">
                                <%#Editable(Item, x=>x.Description) %>
                            </div>
                            <div class="link">
                                <%# RenderLink(Item, x=>x.Link,new { @class ="button"}, true,Editable(Item, x=>x.Link.Text))%>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="category-slider-tab">
        <asp:Repeater ID="categories" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ICarousel">
            <ItemTemplate>
                <div class="item <%#Container.ItemIndex==0?"active":"" %>">
                    <a href="javascript:void(0)"><%#Editable(Item, x=>x.PanelName) %></a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>


