﻿using System;
using System.Linq;
using System.Collections.Generic;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Glass.Mapper.Sc;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class ProductCategoryCarouselStatic : GlassUserControl<ICategoryTopStatic>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model.HeroCarousel.Any())
            {
                //SitecoreContext.GetItem<IModelBase>(ItemIds.ProductCategoryButtons).GetChildren<ILink>();
                button.DataSource = Model.LinkButtons;
                button.DataBind();


                sliders.DataSource = Model.HeroCarousel.Take(5);
                sliders.DataBind();
                categories.DataSource = Model.HeroCarousel;
                categories.DataBind();
            }
            else
            {
                Visible = false;
            }

        }
    }
}