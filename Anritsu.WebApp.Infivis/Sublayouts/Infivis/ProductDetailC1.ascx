﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetailC1.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductDetailC1" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components.Extensions" %>
<% if (Model.SelectProduct != null)
   { %>
<div class="product-detail">
    <div class="container">
        <div class="row">
            <div class="right">
                <div class="img">
                    <%= GlassHtml.RenderImage(Model,x=>x.SelectProduct.Image,isEditable:true) %>
                </div>
            </div>
            <div class="left">
                <div class="inner">
                    <% if (Model.SelectProduct.ShowEcoIcon)
                       { %>
                    <div class="eco">
                        <a href="<%=Editable(Model,x=>x.SelectProduct.GetEcoLink()) %>" target="<%=Editable(Model, x=>x.SelectProduct.GetEcoLinkTarget()) %>">
                            <img src="<%=Editable(Model,x=>x.SelectProduct.EcoIcon.Src) %>" />
                        </a>
                    </div>
                    <% } %>
                    <h1 class="cate"><%=Editable(x=> x.SelectProduct.ProductName)%>
                    </h1>
                    <div class="name"><%=Editable(x=> x.SelectProduct.ModelNumber)%></div>


                    <div class="tip <%= GetStyleClass("IsDiscontinued",Model.Id.ToString()) %>">
                        <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "DiscontinuedText") %>
                        <% if (Model.SelectProduct.ReplacementProducts.Any())
                           { %>
                        <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "FindNew") %>
                        <% foreach (var product in Model.SelectProduct.ReplacementProducts)
                           {%>
                        <%if (product.SelectProduct != null)
                          { %>
                        <a href="<%=product.Url %>"><%=product.SelectProduct.ModelNumber %></a>
                        <%=Model.SelectProduct.ReplacementProducts.Last<IModelBase>().Id.Equals(product.Id)?"":"," %>
                        <%} %>
                        <% } %>
                        <% } %>
                    </div>

                    <div class="action">
                        <div class="mrow">
                            <asp:HyperLink ID="HyperlinkShowRequestQuote" runat="server" CssClass="button"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "requestquote") %></asp:HyperLink>
                            <asp:HyperLink ID="HyperlinkShowRequestDemo" runat="server" CssClass="button"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "requestdemo") %></asp:HyperLink>
                        </div>
                        <div class="mrow">
                            <asp:HyperLink ID="HyperlinkShowDownloads" runat="server" CssClass="button"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "downloads") %></asp:HyperLink>
                            <asp:HyperLink ID="HyperlinkShowFaqs" runat="server" CssClass="button"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "faqs") %></asp:HyperLink>
                        </div>
                        <% if (Model.SelectProduct.ShowAdditionalHeroButton)
                           { %>
                        <div class="mrow">
                            <%=RenderLink(x=>x.SelectProduct.AdditionalHeroButtonLink, new { @class = "button"}, isEditable: true, contents:Editable(x=>x.SelectProduct.AdditionalHeroButtonLink.Text))%>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>

            <div class="status">
                <%if (Model.SelectProduct.IsDiscontinued == false)
                  {%>
                <%if (Model.SelectProduct.Status != null)
                  {%>
                <%=GlassHtml.RenderImage(Model,x=>x.SelectProduct.Status.Image,isEditable:true) %>
                <%}%>
                <%}%>
            </div>
        </div>
    </div>
</div>
<% } %>

