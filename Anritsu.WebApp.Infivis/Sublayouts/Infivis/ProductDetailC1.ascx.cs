﻿using System;
using System.Web.UI.HtmlControls;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Web.UI.WebControls;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.SiteSearch;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class ProductDetailC1 : GlassUserControl<IProductDetailStaticC1>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            HyperlinkShowRequestQuote.NavigateUrl = GetUrl("ShowRequestQuote");
            HyperlinkShowRequestQuote.Visible = Model.SelectProduct.ShowRequestQuote;
            HyperlinkShowRequestQuote.CssClass = "button";
            HyperlinkShowRequestDemo.NavigateUrl = GetUrl("ShowRequestDemo");
            HyperlinkShowRequestDemo.Visible = Model.SelectProduct.ShowRequestDemo;
            HyperlinkShowRequestDemo.CssClass = "button";
            HyperlinkShowDownloads.NavigateUrl = GetUrl("ShowDownloads");
            HyperlinkShowDownloads.Visible = Model.SelectProduct.ShowDownloads;
            HyperlinkShowDownloads.CssClass = "button";
            HyperlinkShowFaqs.NavigateUrl = GetUrl("ShowFaqs");
            HyperlinkShowFaqs.Visible = Model.SelectProduct.ShowFaQs;
            HyperlinkShowFaqs.CssClass = "button";

            if (GsaHelpers.IsGsaCrawler())
                AddGsaMetaInfo();
        }

        private void AddGsaMetaInfo()
        {
            try
            {
                var ltMetaTags = (PlaceHolder) Parent.FindControl("GsaMetaPlaceHolder");
                if (ltMetaTags == null) return;

                var meta = new HtmlMeta
                {
                    Name = "isobsolete",
                    Content = Convert.ToString(!Model.SelectProduct.IsDiscontinued)
                };
                ltMetaTags.Controls.Add(meta);

                var area = SiteSearchArea.GetGsaArea(Sitecore.Context.Item);
                var sortOrder = SiteSearchArea.GetSearchFilterSortOrder(area.Trim());
                meta = new HtmlMeta
                {
                    Name = "sortorder",
                    Content =
                        Model.SelectProduct.IsDiscontinued
                            ? Convert.ToString(sortOrder + 1)
                            : Convert.ToString(sortOrder)
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);

                //Adding product release date with 'last-modified' meta tag for GSA sorting purpose
                meta = new HtmlMeta
                {
                    Name = "last-modified",
                    Content = Model.SelectProduct.IsDiscontinued
                        ? "1990-01-01"
                        : Model.SelectProduct.ReleaseDate != null
                            ? Model.SelectProduct.ReleaseDate.ToString("yyyy-MM-dd")
                            : ""
                };
                ltMetaTags.Controls.Add(meta);

                if (Model.SelectProduct.IsDiscontinued)
                {
                    meta = new HtmlMeta
                    {
                        Name = "category",
                        Content = area.Substring(0, 2) + GsaMetaStringSeperators.AreaCategorySeparator +
                                  Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary",
                                      "DiscontinuedProductText")
                    };
                    ltMetaTags.Controls.Add(meta);
                    meta = new HtmlMeta
                    {
                        Name = "googlebot",
                        Content = "noarchive"
                    };
                    ltMetaTags.Controls.Add(meta);
                    meta = new HtmlMeta
                    {
                        Name = "robots",
                        Content = "NOARCHIVE"
                    };
                    ltMetaTags.Controls.Add(meta);
                }
                else
                {
                    var categories = SiteSearchItemCategory.GetGsaItemCategory(Sitecore.Context.Item);
                    foreach (var category in categories)
                    {
                        meta = new HtmlMeta
                        {
                            Name = "category",
                            Content =
                                !string.IsNullOrWhiteSpace(category)
                                    ? area.Substring(0, 2) + GsaMetaStringSeperators.AreaCategorySeparator + category
                                    : string.Empty
                        };
                        if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                    }

                    var subcategories = SiteSearchItemSubcategory.GetGsaItemSubcategory(Sitecore.Context.Item);
                    foreach (var subcategory in subcategories)
                    {
                        meta = new HtmlMeta
                        {
                            Name = "subcategory",
                            Content =
                                !string.IsNullOrWhiteSpace(subcategory)
                                    ? area.Substring(0, 2) + GsaMetaStringSeperators.AreaCategorySeparator + subcategory
                                    : string.Empty
                        };
                        if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                    }
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message + " " + ex.StackTrace, "GsaMetaInfoAddition");
            }
        }

        public string GetStyleClass(string fieldName, string itemId)
        {
            switch (fieldName)
            {
                case "IsDiscontinued":
                {
                    return Model.SelectProduct.IsDiscontinued ? string.Empty : "hiddentxt";
                }
                case "ShowRequestQuote":
                {
                    return Model.SelectProduct.ShowRequestQuote ? string.Empty : "hiddentxt";
                }
                case "ShowRequestDemo":
                {
                    return Model.SelectProduct.ShowRequestDemo ? string.Empty : "hiddentxt";
                }
                case "ShowDownloads":
                {
                    return Model.SelectProduct.ShowDownloads ? string.Empty : "hiddentxt";
                }
                case "ShowFaqs":
                {
                    return Model.SelectProduct.ShowFaQs ? string.Empty : "hiddentxt";
                }
            }
            return string.Empty;
        }

        public string GetUrl(string fieldName)
        {
            var sublayout = Parent as Sublayout;
            if (sublayout == null) return null;

            var requestQuoteCtaItemId = sublayout.Attributes["RequestQuoteCTA"];
            var requestDemoCtaItemId = sublayout.Attributes["RequestDemoCTA"];

            switch (fieldName)
            {
                case "ShowDownloads":
                {
                    return SitecoreContext.GetItem<IModelBase>(ItemIds.DownloadItem) == null
                        ? string.Empty
                        : SitecoreContext.GetItem<IModelBase>(ItemIds.DownloadItem).Url + "?model=" +
                          Model.SelectProduct.ModelNumber;
                }
                case "ShowFaqs":
                {
                    return SitecoreContext.GetItem<IModelBase>(ItemIds.DownloadItem) == null
                        ? string.Empty
                        : SitecoreContext.GetItem<IModelBase>(ItemIds.DownloadItem).Url + "?tab=3&model=" +
                          Model.SelectProduct.ModelNumber;
                }
                case "ShowRequestQuote":
                {
                    if (!string.IsNullOrEmpty(requestQuoteCtaItemId))
                    {
                        var quoteItem = SitecoreContext.GetItem<ILink>(requestQuoteCtaItemId);
                        if (quoteItem != null)
                        {
                            return quoteItem.Link != null
                                ? string.Format("{0}?model={1}&cta=QUOTE", quoteItem.Link.Url,
                                    Model.SelectProduct.ModelNumber)
                                : string.Empty;
                        }
                    }
                    return string.Empty;
                }
                case "ShowRequestDemo":
                {
                    if (!string.IsNullOrEmpty(requestDemoCtaItemId))
                    {
                        var demoItem = SitecoreContext.GetItem<ILink>(requestDemoCtaItemId);
                        if (demoItem != null)
                        {
                            return demoItem.Link != null
                                ? string.Format("{0}?model={1}&cta=DEMO", demoItem.Link.Url,
                                    Model.SelectProduct.ModelNumber)
                                : string.Empty;
                        }
                    }
                    return string.Empty;
                }
            }
            return string.Empty;
        }
    }
}