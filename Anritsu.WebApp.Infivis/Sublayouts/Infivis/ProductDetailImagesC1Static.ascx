﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetailImagesC1Static.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductDetailImagesC1Static" %>


 <% if(Model.SelectProduct.AdditionalImages.Count()!=0)
       { %>
<div class="container">
    <div class="slider-title"><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "Images").ToUpper() %></div>
    <div class="product-image-slider slick">
        <asp:Repeater ID="AdditionalImages" runat="server" ItemType="Glass.Mapper.Sc.Fields.Image">
            <ItemTemplate>
                <div class="slider">
                    <a href="<%#Item.Src %>">
                        <%#RenderImage(Item,x=>x, new { Width = 240, Height = 240}) %>
                    </a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>

 <% } %>
