﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class ProductDetailImagesStatic : GlassUserControl<IProductDetailStatic>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model != null && Model.SelectProduct != null && Model.SelectProduct.AdditionalImages.Any())
            {
                AdditionalImages.DataSource = Model.SelectProduct.AdditionalImages;
                AdditionalImages.DataBind();
            }
            else
            {
                Visible = false;
            }
        }
    }
}