﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetailStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductDetailStatic" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components.Extensions" %>
<% if (Model.SelectProduct != null)
   { %>
<div class="product-detail">
    <div class="container"  style="margin-bottom:0px;">
        <div class="row">
            <div class="right">
                <div class="img">
                    <%= GlassHtml.RenderImage(Model,x=>x.SelectProduct.Image,isEditable:true) %>
                </div>


               
                    <%--<table style="float:right">
                        <tr>
                            <td class="img" style="width:50px;height:50px;" >
                                <%= GlassHtml.RenderImage(Model,x=>x.SelectProduct.Image,isEditable:true) %>
                            </td>
                            <td class="img" style="width:50px;height:50px;">
                                <%= GlassHtml.RenderImage(Model,x=>x.SelectProduct.Image,isEditable:true) %>
                            </td>
                            <td class="img" style="width:50px;height:50px;">
                                <%= GlassHtml.RenderImage(Model,x=>x.SelectProduct.Image,isEditable:true) %>
                            </td>
                            <td class="img" style="width:50px;height:50px;">
                                <%= GlassHtml.RenderImage(Model,x=>x.SelectProduct.Image,isEditable:true) %>
                            </td>
                            <td class="img" style="width:50px;height:50px;">
                                <%= GlassHtml.RenderImage(Model,x=>x.SelectProduct.Image,isEditable:true) %>
                            </td>
                        </tr>
                    </table>--%>

                <table style="float:right">
                    <tr>
    <asp:Repeater ID="repeaterToolBar" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Components.IProductDetailIcon">
        <ItemTemplate>
            <%--<td class="img" style="max-width:50px;max-height:50px;">--%>
            <td class="img">
                <div style="max-width:50px;max-height:50px;display:block;">
                    <a href="<%#GetLink(Item)%>"><%#GetImage(Item)%></a>
                </div>
            </td>
        </ItemTemplate>
    </asp:Repeater>
                        </tr>
                    </table>
               

            </div>
                

            <div class="left">
                <div class="inner">
                    <% if (Model.SelectProduct.ShowEcoIcon)
                       { %>
                    <div class="eco">
                        <a href="<%=Editable(Model,x=>x.SelectProduct.GetEcoLink()) %>" target="<%=Editable(Model, x=>x.SelectProduct.GetEcoLinkTarget()) %>">
                            <img src="<%=Editable(Model,x=>x.SelectProduct.EcoIcon.Src) %>" />
                        </a>
                    </div>
                    <% } %>
                    <h1 class="cate"><%=Editable(x=> x.SelectProduct.ProductName)%>
                    </h1>
                    <div class="name"><%=Editable(x=> x.SelectProduct.ModelNumber)%></div>


                    <div class="tip <%= GetStyleClass("IsDiscontinued",Model.Id.ToString()) %>">
                        <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "DiscontinuedText") %>
                        <% if (Model.SelectProduct.ReplacementProducts.Any())
                           { %>
                        <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "FindNew") %>
                        <% foreach (var product in Model.SelectProduct.ReplacementProducts)
                           {%>
                        <%if (product.SelectProduct != null)
                          { %>
                        <a href="<%=product.Url %>"><%=product.SelectProduct.ModelNumber %></a>
                        <%=Model.SelectProduct.ReplacementProducts.Last<IModelBase>().Id.Equals(product.Id)?"":"," %>
                        <%} %>
                        <% } %>
                        <% } %>
                    </div>
                      <div class="description">
                        <%=Editable(x=> x.SelectProduct.BannerShortDescription)%>
                    </div>
                    <div class="action">
                        <div class="mrow">
                            <asp:HyperLink ID="HyperlinkShowRequestQuote" runat="server" CssClass="button"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "requestquote") %></asp:HyperLink> </div>
							 <div class="mrow">
                            <asp:HyperLink ID="HyperlinkShowRequestDemo" runat="server" CssClass="button"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "requestdemo") %></asp:HyperLink>
                        </div>
                        <div class="mrow">
                            <asp:HyperLink ID="HyperlinkShowDownloads" runat="server" CssClass="button"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "downloads") %></asp:HyperLink>
							 </div> <div class="mrow">
                            <asp:HyperLink ID="HyperlinkShowFaqs" runat="server" CssClass="button"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "faqs") %></asp:HyperLink>
                        </div>
                        <% if (Model.SelectProduct.ShowInquiry)
                           { %>
                        <div class="mrow">
                            <%=RenderLink(x=>x.SelectProduct.InquiryLink, new { @class = "button"}, isEditable: true, contents:Editable(x=>x.SelectProduct.InquiryLink.Text))%>
                        </div>
                        <% } %>

                        <% if (Model.SelectProduct.ShowAdditionalHeroButton)
                           { %>
                        <div class="mrow">
                            <%=RenderLink(x=>x.SelectProduct.AdditionalHeroButtonLink, new { @class = "button"}, isEditable: true, contents:Editable(x=>x.SelectProduct.AdditionalHeroButtonLink.Text))%>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>

            <div class="status">
                <%if (Model.SelectProduct.IsDiscontinued == false)
                  {%>
                <%if (Model.SelectProduct.Status != null)
                  {%>
                <%=GlassHtml.RenderImage(Model,x=>x.SelectProduct.Status.Image,isEditable:true) %>
                <%}%>
                <%}%>
            </div>
        </div>
        
    </div>
</div>
<% } %>

<sc:sublayout id="Ticker" runat="server" path="/GlobalWeb/Sublayouts/AnnouncementTicker.ascx"></sc:sublayout>
<div style="margin-bottom:35px;">
</div>
