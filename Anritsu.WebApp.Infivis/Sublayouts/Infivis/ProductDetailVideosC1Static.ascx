﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetailVideosC1Static.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductDetailVideosC1Static" %>
<%@ Import Namespace="Sitecore.Globalization" %>

<% if (Model.Videos.Count() != 0)
   { %>
<div class="container">

    <div class="slider-title"><%=Translate.TextByDomain("GlobalDictionary","Videos").ToUpper() %></div>
    <div class="product-video-slider slick">
        <asp:Repeater ID="RepeaterVideos" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Pages.IVideodetailStatic">
            <ItemTemplate>
                <div class="slider productItemHeight">
                    <a href="<%#Item.Url%>" class="videoImg">
                        <img src="<%#  GetVideoThumbnailUrl(Item.Video)%>" alt="<%# GetVideoAlt(Item.Video) %>" />
                    </a>
                    <p class="description">
                       <span class="video-name"><b><%# Editable(Item,x=>x.Name)%></b></span><br/>
                        <%# Editable(Item,x=>x.ShortDescription)%>
                    </p>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
<% } %>