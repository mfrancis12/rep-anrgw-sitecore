﻿using System;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Managers;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class ProductDetailVideosC1Static : GlassUserControl<IProductDetailStaticC1>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            RepeaterVideos.DataSource = Model.Videos.TryParseToGlass<IVideodetailStatic>();
            RepeaterVideos.DataBind();
        }

        protected string GetVideoThumbnailUrl(string videoId)
        {
            var thumnbailPath = string.Empty;
            var brightcoveVideoItem = SitecoreContext.GetItem<IBrightcoveVideo>(videoId, LanguageManager.GetLanguage("en"));

            if (!string.IsNullOrEmpty(videoId) && brightcoveVideoItem != null && !string.IsNullOrEmpty(brightcoveVideoItem.ThumbnailURL))
            {
                thumnbailPath = brightcoveVideoItem.ThumbnailURL;
            }
            return thumnbailPath;
        }

        protected string GetVideoAlt(string videoId)
        {
            var videoAltText = string.Empty;
            var brightcoveVideoItem = SitecoreContext.GetItem<IBrightcoveVideo>(videoId, LanguageManager.GetLanguage("en"));

            if (!string.IsNullOrEmpty(videoId) && brightcoveVideoItem != null && !string.IsNullOrEmpty(brightcoveVideoItem.Name))
            {
                videoAltText = brightcoveVideoItem.Name;
            }
            return videoAltText;
        }
    }
}