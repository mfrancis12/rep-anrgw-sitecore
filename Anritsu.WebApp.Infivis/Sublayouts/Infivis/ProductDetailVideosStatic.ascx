﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetailVideosStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductDetailVideosStatic" %>
<%@ Import Namespace="Sitecore.Globalization" %>

<% if ((Model.Videos != null && Model.Videos.Count() != 0) ||
           (RepeaterJPVideos.DataSource != null && ((List<BrightcoveJapanVideoThumbnailItem>)RepeaterJPVideos.DataSource).Count != 0))
    { %>
<div class="container">
    <div class="slider-title"><%=Translate.TextByDomain("GlobalDictionary","Videos").ToUpper() %></div>
    <div class="product-video-slider slick">
        <asp:Repeater ID="RepeaterVideos" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Pages.IVideodetailStatic">
            <ItemTemplate>
                <div class="slider productItemHeight">
                    <a href="<%#Item.Url%>" class="videoImg">
                        <img src="<%#  GetVideoThumbnailUrl(Item.Video)%>" alt="<%# GetVideoAlt(Item.Video) %>" />
                    </a>
                    <p class="description">
                        <span class="video-name"><b><%# Editable(Item,x=>x.Name)%></b></span><br />
                        <%# Editable(Item,x=>x.ShortDescription)%>
                    </p>
                </div>
            </ItemTemplate>
        </asp:Repeater>

        <asp:Repeater ID="RepeaterJPVideos" runat="server" ItemType="Anritsu.WebApp.Infivis.SubLayouts.ProductDetailVideosStatic.BrightcoveJapanVideoThumbnailItem">
            <ItemTemplate>
                <div class="slider productItemHeight">
                    <a href="<%#Item.VideoPortalUri%>" class="videoImg">
                        <img src="<%#  Item.Thumbnail %>" alt="<%# Item.Title %>" />
                    </a>
                    <p class="description">
                        <span class="video-name"><b><%# Item.Title %></b></span><br />
                        <%# Item.Description %>
                    </p>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

</div>
<% } %>