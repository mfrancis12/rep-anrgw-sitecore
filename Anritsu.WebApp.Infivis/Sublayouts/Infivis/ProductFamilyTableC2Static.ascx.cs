﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models;

using Glass.Mapper.Sc.Web.Ui;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Components;

namespace Anritsu.WebApp.Infivis.SubLayouts
{

    public partial class ProductFamilyTableC2Static : GlassUserControl<IProductFamilyStaticC2>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Sort.DataSource = SitecoreContext.GetItem<IModelBase>(ItemIds.ProductDetailTabFilters).GetChildren<IDropListItem>();
                Sort.DataTextField = "Value";
                Sort.DataValueField = "Key";
                Sort.DataBind();
            }

            Headers.DataSource = Model.Specifications.Take(6);
            Headers.DataBind();

        }

        protected IEnumerable<IProductDetailStaticC1> Products
        {
            get
            {
                var productsRegional = Model.Products.Where(x => x.SelectProduct != null && !x.SelectProduct.IsDiscontinued);
                //if (Sort.SelectedValue == "Select")
                //{
                //    return productsRegional;
                //}     
                //else
                //{
                //    return Sort.SelectedValue == "ReleaseDate"
                //       ? productsRegional.OrderByDescending(x => x.SelectProduct.ReleaseDate)
                //       : productsRegional.OrderBy(x => x.SelectProduct.ModelNumber);

                //}
                return (Sort.SelectedValue == "Select") ? productsRegional : productsRegional.OrderBy(x => x.SelectProduct.ModelNumber);
            }
        }

    }
}