﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductFamilyTableStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductFamilyTableStatic" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<%@ Import Namespace="System.Globalization" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel" runat="server">
    <ContentTemplate>
        <div class="container">
            <div class="products-table">
                <table>
                    <thead>
                        <tr>
                            <th>
                                <div class="border"></div>
                            </th>                            
                            <th>
                                <div class="border"><%= Translate.TextByDomain("GlobalDictionary", "ModelNumberInfivis") %></div>
                            </th>
                            <asp:Repeater runat="server" ID="Headers" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">
                                <ItemTemplate>
                                    <th>
                                        <div class="border">
                                            <%#Item.Value %>
                                        </div>
                                    </th>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </thead>
                    <tbody>
                        <% foreach (var product in Products)
                           {%>
                        <tr>
                            <td>
                                <div class="status">
								<%if(product.SelectProduct.Status!=null)
								{%>
                                    <%=RenderImage(product,x=>x.SelectProduct.Status.Image) %>
									<%}%>
                                </div>
                                <a href="<%=product.Url %>">
                                    <%=RenderImage(product,x=>x.SelectProduct.Thumbnail, new { Width = 160, Height = 118},isEditable:true) %>
                                </a>
                                <% if(Model.ShowReleaseDate) 
                                    {%>
                                <span class="number">
                                    <%=Editable(product,x=>x.SelectProduct.ReleaseDate,x=>x.SelectProduct.ReleaseDate.ToFormattedDate())%>
                                </span>
                                <%  }%>
                            </td>
                            <td class="border">
                                <a href="<%=product.Url %>">
                                     <b><%=product.SelectProduct.ModelNumber%><br /><%=product.SelectProduct.ProductName%></b>
                                </a>
                            </td>
                            <% foreach (var key in Model.Specifications)
                               {%>
                            <td class="border"><b><%=product.SelectProduct.Specifications[key.Id.ToString("B")]??"-" %></b>
                            </td>
                            <%} %>
                        </tr>

                        <% } %>
                    </tbody>
                </table>
            </div>
        </div>
    </ContentTemplate>
    <%--<Triggers>
        <asp:AsyncPostBackTrigger ControlID="Sort" />
    </Triggers>--%>
</asp:UpdatePanel>
