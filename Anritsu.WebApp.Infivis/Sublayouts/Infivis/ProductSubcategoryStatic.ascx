﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSubcategoryStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductSubcategoryStatic" %>
<%@ Import Namespace="Sitecore.Globalization" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Pages" %>

<div class="full-container">
    <div class="products-info" id="ProductCategory">
        <div class="inner">
            <div>
                <% if (Model.Link!=null) %>
                <% { %>
                <% using (BeginRenderLink(x => x.Link, new { @class= "action" }, isEditable: true))
                   { %>
                <%=Editable(x=>x.Title) %>
                <% } %>
                <% } %>
            </div>
            <h2 class="title">
                <a id="goto_products" class="go-to-products">
                <%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "products") %>
                    </a></h2>
        </div>
    </div>
    <div class="products-tech">                                                
        <asp:Repeater ID="repeaterProductSubcategory" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Components.IProductSubcategoryStatic">
            <ItemTemplate>
                <div class="item">
                    <div class="inner">
                        <h3 class="title"><%#Editable(Item, x=>x.ProductSubcategoryTitle) %></h3>
                        <div class="content">
                            <div class="text">
                                <div class="img">
                                    
                                    <%#Editable(Item,x=>x.ProductSubcategoryImage,new {@class="img100"} ) %>
                                        
                                </div>
                                <p class="more">
                                    <%#Editable(Item, x=>x.ProductSubcategoryDescription)%>
                                </p>
                            </div>
                            <div class="device">
                                    <%# Editable(Item,x=>x.ProductSubcategoryLinks) %> 
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
