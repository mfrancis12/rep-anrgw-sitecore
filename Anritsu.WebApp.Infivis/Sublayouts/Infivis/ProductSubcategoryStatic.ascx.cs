﻿using System;
using System.Collections.Generic;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models;
using Glass.Mapper.Sc.Web.Ui;
using System.Linq;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
  public partial class ProductSubcategoryStatic : GlassUserControl<ICategoryTopStatic>
  {
    private void Page_Load(object sender, EventArgs e)
    {
      if (Model == null)
        return;

      repeaterProductSubcategory.DataSource = Model.ProductSubcategoryList;
      repeaterProductSubcategory.DataBind();

    }

    public IEnumerable<IProductFamilyStaticC2> GetProductFamilies(IModelBase item)
    {
      var links = item.GetChildren<IProductFamilyStaticC2>();
      return links;
    }
    public IEnumerable<IStaticPageWithContentRegional> GetDiscontunedModel(IModelBase item)
    {
      return item.GetChildren<IStaticPageWithContentRegional>();
    }
  }
}