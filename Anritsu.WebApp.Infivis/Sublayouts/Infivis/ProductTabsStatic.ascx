﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductTabsStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ProductTabsStatic" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components.Extensions" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ import namespace="Anritsu.WebApp.SitecoreUtilities.Extensions" %>

<div class="container">
    <div class="infivis tab-interface-2 <%= GetTabClass %>">
        <ul class="product-detail-tab-ul">
            <li><%=GetOverViewTab()%></li>
            <asp:Repeater ID="ProductTabsList" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
                <ItemTemplate>
                    <li><a href="#<%#Item.Id.ToString()%>" <%# string.IsNullOrEmpty(GetOverViewTab()) && Container.ItemIndex == 0 ? "class=\"active\"" : "" %>><span><%#Editable(Item,x=>x.Title)%></span></a></li>
                </ItemTemplate>
            </asp:Repeater>
            <%if (string.IsNullOrEmpty(ShowHideLibrary) ) { %>
            <li class="<%=ShowHideLibrary %>"><a href="#Library"><span><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "library") %></span></a></li>
            <% } %>
        </ul>
        <asp:Repeater ID="OverviewItems" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Components.IOverviewTitleWithDescriptionStatic">
            <ItemTemplate>
                <div id="<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?"active":"" %>">
                    <div class="section-title"><span><%#Editable(Item,x=>x.Title)%></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
                    <div class="section-content detailSection">
                        <div class="tab-interface inner-tab">
                            <ul>
                                <li><a href="#section1-item1"  class="active"><%#GetFeaturesTitle(Item)%> </a></li>
                                <li><a href="#section1-item2"><%#GetDescriptionTitle(Item)%> </a></li>

                                <% if (HasTechnologies)
                                   {%>
                                <li><a href="#section1-item-tech"><%#GetTechnologyTab() %></a></li>
                                <% } %>
                            </ul>
                            <div class="select-mobile">
                                <select>
                                    <option value="0"><%#GetFeaturesTitle(Item)%></option>
                                    <option value="1"><%#GetDescriptionTitle(Item)%></option>
                                    <%if (HasTechnologies)
                                      { %>
                                    <option value="2"><%#GetTechnologyTab() %></option>
                                    <% } %>
                                </select>
                            </div>
                            <div id="section1-item1" class="section active">
                                <%#GetFeatureDescription(Item)%>
                            </div>
                            <div id="section1-item2" class="section">
                                <%#GetMainDescription(Item)%>
                            </div>

                            <%if (HasTechnologies)
                              { %>
                            <div id="section1-item-tech" class="section">
                                <ul>
                                    <% foreach (var tech in Model.SelectProduct.RelatedTechnologies.ToList<Anritsu.WebApp.GlobalWeb.Models.Pages.ITechnology>())
                                       {%>
                                    <li><a href="<%= tech.Url%>"><%= tech.TechnologyName %></a></li>
                                    <% } %>
                                </ul>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Repeater ID="ProductTabsDescription" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
            <ItemTemplate>
                <div id="<%#Item.Id.ToString()%>" class="section <%#Container.ItemIndex==0?GetCssClass:""%>">
                    <div class="section-title"><span><%#Editable(Item,x=>x.Title) %></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
                    <div class="section-content">
                        <div class="<%# Sitecore.Context.Database.GetItem(Item.Id.ToString()).HasChildren? "width70 floatLft":"width100" %>">
                            <%#Editable(Item,x=>x.Description) %>
                        </div>
                        <div class="<%# HideRelatedLinks(Item) %>">
                            <div class="link-title"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "RelatedLinks") %></div>
                            <asp:Repeater ID="RelatedLinks" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Common.ILink" DataSource="<%#GetRelatedLinks(Item) %>">
                                <ItemTemplate>
                                    <div class="links">
                                        <%# RenderLink(Item, x=>x.Link, isEditable: true, contents:Editable(Item,x=>x.Title) +"&nbsp;"+ "<i class='icon icon-arrow-blue'></i>")%>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>

            </ItemTemplate>
        </asp:Repeater>

        <div id="Library" class="section <%=ShowHideLibrary %>">
            <div class="section-title"><span><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "library") %></span><i class="icon icon-plus-grey"></i><i class="icon icon-minus-grey"></i></div>
            <div class="section-content detailSection">
                <div class="tab-interface inner-tab product-downloads products-table">
                    <ul>
                        <asp:Repeater ID="productDownloads" runat="server" ItemType="IDownloadTypeStatic">
                            <ItemTemplate> 
                                <li>
                                    <a class="<%# Container.ItemIndex==0? "active" : ""  %>" href="#<%# Item.Name.Replace(" ","_") %>"><%# Editable(Item,x=>x.Name) %> </a>
                                    <table class="section detail-table active" id="<%#  Item.Name.Replace(" ","_") %>" style="display: table;">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div><%# Editable(Item,x=>x.Name) %></div>
                                                </th>
                                                <th>
                                                    <div><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "releasedate") %></div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <asp:Repeater ID="productDownloadLibrary" runat="server" ItemType="IDownloadStatic" DataSource="<%# GetProductLibraryContent(Item) %>">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="border">
                                                        <b>
                                                            <asp:HyperLink runat="server" ID="LibraryFileDownloadLink" Target="_blank" NavigateUrl="<%# Item.DownloadLink.Url%>" >
                                                                <%# Editable(Item,x=>x.Name) %>
                                                            </asp:HyperLink>
                                                        </b>
                                                    </td>
                                                    <td><b><%# Editable(Item,x=>x.ReleaseDate,x=>x.ReleaseDate.ToFormattedDate()) %></b></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </table>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="select-mobile">
                        <select id="product-download-category">
                            <asp:Repeater ID="ProductsDownloadsMobile" runat="server" ItemType="IDownloadTypeStatic"><%--ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IDropListItem">--%>
                                <ItemTemplate>
                                    <%--<option value="<%#Editable(Item,x=>x.Key)%>"><%#Editable(Item,x=>x.Value) %></option>--%>
                                    <option value="<%#Editable(Item,x=>x.Name)%>"><%#Editable(Item,x=>x.Name) %></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
