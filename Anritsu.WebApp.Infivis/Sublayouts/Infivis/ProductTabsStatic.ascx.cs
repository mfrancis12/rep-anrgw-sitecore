﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI.WebControls;
using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using SessionVariables = Anritsu.WebApp.GlobalWeb.Constants.SSOUtilities.SessionVariables;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class ProductTabsStatic : GlassUserControl<IProductDetailStatic>
    {
        public bool HasCount;
        public string ShowHideLibrary;

        //private IProviderSearchContext _downloadcontext;
        //protected IProviderSearchContext DownloadSearchContext
        //{
        //    get
        //    {
        //        return _downloadcontext ?? (_downloadcontext = ContentSearchManager.GetIndex(StaticVariables.DownloadsIndex).CreateSearchContext());
        //    }
        //}

        private List<Anritsu.WebApp.GlobalWeb.Models.IModelBase> _downloadList;

        protected List<Anritsu.WebApp.GlobalWeb.Models.IModelBase> DownloadList
        {
            get
            {
                return new List<IModelBase>();
            }
            private set
            {
                if (value == null) throw new ArgumentNullException("value");
                _downloadList = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion)|| Model.TemplateId.ToString().Equals(TemplateIds.ProductRegionC1))
                {
                    ProductTabsList.DataSource = Model.SelectProduct.GetChildren<ITitleWithDescription>().Where(x => x.GetChildren<ITitleWithDescription>().Count() == 0).Take(5);
                    ProductTabsList.DataBind();

                }
                else
                {
                    ProductTabsList.DataSource = Model.GetChildren<ITitleWithDescription>().Where(x => x.GetChildren<ITitleWithDescription>().Count() == 0).Take(5);
                    ProductTabsList.DataBind();
                }

                List<ITitleWithDescription> itemWithChildList = new List<ITitleWithDescription>();
                List<ITitleWithDescription> itemWithOutChildList = new List<ITitleWithDescription>();
                IEnumerable<ITitleWithDescription> tabItems;
                if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion) || Model.TemplateId.ToString().Equals(TemplateIds.ProductRegionC1))
                {
                    tabItems = Model.SelectProduct.GetChildren<ITitleWithDescription>();
                    itemWithChildList.Add(Model.SelectProduct.GetChildren<IOverviewTitleWithDescriptionStatic>().FirstOrDefault());
                }
                else
                {
                    tabItems = Model.GetChildren<ITitleWithDescription>();
                    itemWithChildList.Add(Model.GetChildren<IOverviewTitleWithDescriptionStatic>().FirstOrDefault());
                }

                //itemWithChildList.Add(tabItems.FirstOrDefault<IOverviewTitleWithDescriptionStatic>());
                foreach (ITitleWithDescription item in tabItems)
                {
                        itemWithOutChildList.Add(item);
                }

                OverviewItems.DataSource = itemWithChildList;
                OverviewItems.DataBind();
                ProductTabsDescription.DataSource = itemWithOutChildList;
                ProductTabsDescription.DataBind();
                //DownloadList.DataSource = BindDownloads();
                //DownloadList.DataBind();          

                //Bind library content
                productDownloads.DataSource = GetProductDownloadsCategories();
                productDownloads.DataBind();
                ProductsDownloadsMobile.DataSource = GetProductDownloadsCategories();
                ProductsDownloadsMobile.DataBind();
                if (productDownloads.Items.Count <= 0)
                {
                    ShowHideLibrary = "hiddentxt";
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, ex, GetType());
            }
        }
        public Item LibraryItem
        {
            get
            {
                if (Model.GetChildren<ILibrary>().SingleOrDefault() != null)
                {
                    var singleOrDefault = Model.GetChildren<ILibrary>().SingleOrDefault();
                    if (singleOrDefault != null)
                        return Sitecore.Context.Database.GetItem(singleOrDefault.Id.ToString());
                }
                return null;
            }
        }
        public bool HasTechnologies
        {
            get
            {
                if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion) || Model.TemplateId.ToString().Equals(TemplateIds.ProductRegionC1))
                {

                    return (Model.SelectProduct.RelatedTechnologies.Count() > 0);
                }
                else
                    return false;
            }
        }
        public IEnumerable<Item> GetOverviewContent(IModelBase item)
        {
            Item overViewItem = SitecoreContext.GetItem<Item>(item.Id.ToString());
            bool isdescriptionRequried = true;
            if (overViewItem != null)
            {
                var listItems = overViewItem.Children.ToList();
                foreach (var currentItem in listItems.Where(currentItem => currentItem.TemplateID.ToString().Equals(TemplateIds.ProductDescriptionTab)))
                {
                    isdescriptionRequried = false;
                }
                if (isdescriptionRequried)
                    listItems.Insert(0, SitecoreContext.GetItem<Item>(ItemIds.Description));

                return listItems;
            }
            return null;
        }
        public Collection<ILink> GetRelatedLinks(IModelBase item)
        {
            Collection<ILink> listItems = new Collection<ILink>();
            foreach (var childItem in item.GetChildren<ILink>())
            {
                listItems.Add(childItem);
            }

            return listItems;
        }

        public string GetDescriptionTab()
        {
            if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
            {

                return Translate.TextByDomain("GlobalDictionary", "Description");
            }
            else
            {
                return "";
            }
        }

        public string GetTechnologyTab()
        {
            return Translate.TextByDomain("Products", "Technologies");
        }

        public string GetOverViewTab()
        {
            string strTab = string.Empty;
            ITitleWithDescription overViewTabItem;
            if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion) || Model.TemplateId.ToString().Equals(TemplateIds.ProductRegionC1))
                overViewTabItem = (Model.SelectProduct.GetChildren<IOverviewTitleWithDescriptionStatic>().FirstOrDefault());
            else
                overViewTabItem = (Model.GetChildren<IOverviewTitleWithDescriptionStatic>().FirstOrDefault());
            if (overViewTabItem != null)
                strTab = "<a class=\"active\" href=\"#" + overViewTabItem.Id + "\"><span>" + Translate.TextByDomain("GlobalDictionary", "overview") + "</span></a>";

            return strTab;
        }

        public string GetDescriptionTabContent(IModelBase item)
        {
            if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion) || Model.TemplateId.ToString().Equals(TemplateIds.ProductRegionC1))
            {

                var tabItem = item.GetChildren<IProductDescriptionTab>().FirstOrDefault();
                string description = (tabItem != null) ? tabItem.Description : "";
                return Model.SelectProduct.ShortDescription + description;
            }
            else
            {
                return "";
            }
        }

        public string GetCssClass
        {
            get { return !HasCount ? "active" : ""; }
        }
        public string GetTabClass
        {
            get { return (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion) || Model.TemplateId.ToString().Equals(TemplateIds.ProductRegionC1)) ? "product-detail-tab" : "content-tab"; }
        }
        public string HideRelatedLinks(IModelBase itm)
        {
            return Sitecore.Context.Database.GetItem(itm.Id.ToString()).HasChildren ? "related-link" : "hiddentxt";
        }

        public IEnumerable<IDownloadStatic> GetProductLibraryContent(IDownloadTypeStatic downloadCategory)
        {
            return downloadCategory.GetChildren<IDownloadStatic>().Where(x=>x.DownloadLink!=null);
        }

        protected IEnumerable<IDownloadTypeStatic> GetProductDownloadsCategories()
        {
            ILibraryStatic libraryItem = Model.SelectProduct.GetChildren<ILibraryStatic>().FirstOrDefault();
            if(libraryItem!=null)
            {
                if (libraryItem.Downloads.Count() != 0) return libraryItem.Downloads;
            }
            return null;
        }


        public static Item[] SitecoreDefaultSort(List<Item> itemList)
        {
            // Sort by sortorder, if sortorder value is empty, assume 100. 

            itemList.Sort(
                (a, b) =>
                (
                    (a.Fields["__sortorder"].Value == "" ? "100" : a.Fields["__sortorder"].Value)
                     ==
                    (b.Fields["__sortorder"].Value == "" ? "100" : b.Fields["__sortorder"].Value)
                )
                ?
                (String.CompareOrdinal(a.Name, b.Name))
                :
                (int.Parse(a.Fields["__sortorder"].Value == "" ? "100" : a.Fields["__sortorder"].Value).CompareTo(int.Parse(b.Fields["__sortorder"].Value == "" ? "100" : b.Fields["__sortorder"].Value)))
                );
            return itemList.ToArray();
        }

        private static Func<Item, int> GetSortOrderValue(List<Item> i)
        {
            return x => string.IsNullOrEmpty(i[0][FieldIDs.Sortorder]) ? 0 : int.Parse(i[0][FieldIDs.Sortorder]);
        }

        //protected string GetTitle(object item)
        //{
        //    Item tabItem = (Item)item;

        //    return tabItem.TemplateID.ToString().Equals(TemplateIds.TitleWithDescription) ? tabItem["Title"] :
        //        Translate.TextByDomain("GlobalDictionary", "Description");
        //}

        protected string GetFeaturesTitle(object item)
        {
            IOverviewTitleWithDescriptionStatic tabItem = (IOverviewTitleWithDescriptionStatic)item;

            return tabItem.FeaturesTitle;
        }

        protected string GetDescriptionTitle(object item)
        {
            IOverviewTitleWithDescriptionStatic tabItem = (IOverviewTitleWithDescriptionStatic)item;

            return tabItem.MainDescriptionTitle;
        }

        protected string GetFeatureDescription(object item)
        {
            var tabItem = (IOverviewTitleWithDescriptionStatic)item;
            var description = tabItem.FeaturesDescription;
            return description;
        }
        protected string GetMainDescription(object item)
        {
            var tabItem = (IOverviewTitleWithDescriptionStatic)item;
            var description = tabItem.MainDescription;
            return description;
        }

        protected string GetDescription(object item)
        {
            var tabItem = (Item)item;
            var description = string.Empty;

            if (Model.TemplateId.ToString().Equals(TemplateIds.ProductRegion))
            {
                if (tabItem.TemplateID.ToString().Equals(TemplateIds.ProductDescriptionTab))
                {
                    return Model.SelectProduct.ShortDescription + SitecoreContext.GetItem<IProductDescriptionTab>(tabItem.ID.ToString()).Description;
                }
                if (tabItem.ID.ToString().Equals(ItemIds.Description))
                {
                    return Model.SelectProduct.ShortDescription;
                }
                if (tabItem.TemplateID.ToString().Equals(TemplateIds.TitleWithDescription))
                {
                    return SitecoreContext.GetItem<ITitleWithDescription>(tabItem.ID.ToString()) != null ? SitecoreContext.GetItem<ITitleWithDescription>(tabItem.ID.ToString()).Description : string.Empty;
                }
            }
            return string.Empty;
        }

        protected string GaTrackingScript(string itemUrl)
        {
            return string.Format(ConfigurationManager.AppSettings["GaTrackingScript"]
                , System.Convert.ToString(itemUrl.Substring(itemUrl.IndexOf("www.anritsu.com/") + 1)));
        }
    }

    public class DownloadComparer : IEqualityComparer<IDropListItem>
    {
        public bool Equals(IDropListItem x, IDropListItem y)
        {
            return x.Key == y.Key;
        }

        public int GetHashCode(IDropListItem obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}