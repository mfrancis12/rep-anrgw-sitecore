﻿using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;


namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class PromosStatic : GlassUserControl<ICorporateHomeStatic>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            promos.DataSource = Model.Highlights.TryParseToGlass<IPromo>();
            promos.DataBind();
        }

        protected string GetTruncatedString(string originalString, int charCount)
        {
            return GetWords(originalString, charCount);
        }

        private string GetWords(string s, int charCount)
        {
            List<string> list = new List<string>();
            MatchCollection collection = Regex.Matches(s, @"[\S]+");
            System.Collections.IEnumerator enumerator = collection.GetEnumerator();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; enumerator.MoveNext(); i++)
            {
                string value = enumerator.Current.ToString();// as string;
                sb.AppendFormat("{0} ", value);
                if (sb.Length > charCount)
                {
                    list.Add("...");
                    break;
                }
                list.Add(value);
            }

            return FormString(list.ToArray());
        }

        private string FormString(string[] words)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in words)
            {
                sb.Append(s);
                sb.Append(" ");
            }
            return sb.ToString();
        }

    }
}