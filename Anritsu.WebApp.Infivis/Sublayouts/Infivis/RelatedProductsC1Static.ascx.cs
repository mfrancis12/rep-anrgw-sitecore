﻿using System;
using System.Linq;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class RelatedProductsC1Static : GlassUserControl<IProductDetailStaticC1>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Model.RelatedProducts.Any())
            {
                RelatedProductsList.DataSource = Model.RelatedProducts.Where(x=>x.SelectProduct!=null);//.Where(x => !x.SelectProduct.IsDiscontinued);
                RelatedProductsList.DataBind();
            }
            else
            {
                Visible = false;
            }
        }
    }
}