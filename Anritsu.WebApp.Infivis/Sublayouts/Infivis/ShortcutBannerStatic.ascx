﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShortcutBannerStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ShortcutBannerStatic" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="container">
   <div class="hero-menu">
        <asp:Repeater ID="promos" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.IPromo">
            <ItemTemplate>                
                <div class="item"><a href="<%#Editable(Item, x=>x.Link.Url) %>"><img src="<%#Editable(Item, x=>x.Image.Src) %>" alt="<%#Editable(Item, x=>x.Image.Alt) %>"></a>
                <h2 class="link"><a href="<%#Editable(Item, x=>x.Link.Url) %>"><%#Editable(Item, x=>GetTruncatedString(x.Title,47)) %><i class="icon icon-arrow-grey"></i></a></h2>
              </div>             
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
