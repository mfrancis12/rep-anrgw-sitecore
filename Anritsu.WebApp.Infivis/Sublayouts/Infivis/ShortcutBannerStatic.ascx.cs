﻿using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models;
using System.Text;
using System.Text.RegularExpressions;
using Anritsu.WebApp.Infivis.Models.Pages;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class ShortcutBannerStatic : GlassUserControl<IProductFamilyStaticC1>
    {
        private void Page_Load(object sender, EventArgs e)
        {
      if (Model == null)
        return;
      promos.DataSource = Model.ShortcutBanner.TryParseToGlass<IPromo>();
      promos.DataBind();
    }

    protected string GetTruncatedString(string originalString, int charCount)
        {
            return GetWords(originalString, charCount);
        }

        private string GetWords(string s, int charCount)
        {
            List<string> list = new List<string>();
            MatchCollection collection = Regex.Matches(s, @"[\S]+");
            System.Collections.IEnumerator enumerator = collection.GetEnumerator();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; enumerator.MoveNext(); i++)
            {
                string value = enumerator.Current.ToString();// as string;
                sb.AppendFormat("{0} ", value);
                if (sb.Length > charCount)
                {
                    list.Add("...");
                    break;
                }
                list.Add(value);
            }

            return FormString(list.ToArray());
        }

        private string FormString(string[] words)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in words)
            {
                sb.Append(s);
                sb.Append(" ");
            }
            return sb.ToString();
        }

    }


}