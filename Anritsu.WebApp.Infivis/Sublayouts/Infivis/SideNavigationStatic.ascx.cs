﻿using System;
using System.Linq;
using Amazon.SimpleDB.Model;
using Anritsu.WebApp.GlobalWeb.Constants;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Common;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Models;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using System.Web;
using Sitecore.Data.Fields;

using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using Glass.Mapper.Sc;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Shell.Applications.ContentManager.ReturnFieldEditorValues;
using Sitecore.Web;
using Sitecore.Web.UI;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using System.Text;
using Anritsu.WebApp.SitecoreUtilities.CustomCaches;


namespace Anritsu.WebApp.Infivis.SubLayouts
{
  public class NavigationItem
  {
    public Sitecore.Data.Items.Item thisItem { get; set; }
    public Sitecore.Data.Items.Item ChildItem { get; set; }
  }

  public partial class SideNavigationStatic : GlassUserControl<INavigationStatic>
  {
    //private string SideNavigationCacheKey = "side_navigation_cache_key";

    //private bool _useDomainUrl
    //{
    //    get
    //    {
    //        try
    //        {
    //            return true; 
    //        }
    //        catch (Exception)
    //        {

    //            return false;
    //        }
    //    }
    //}


    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      BuildControl();
    }

    private void BuildControl()
    {
      StringBuilder builder = new StringBuilder();
      try
      {
        var list = Model.SideNavigationItems.ToList();
        //if (list.Count() != 0)
        {
          //output.Write("<!--googleoff: all-->");
          builder.Append("<!--googleoff: all-->");
          //output.Write("<div class='aside-menu-trigger'>");
          builder.Append("<div class='aside-menu-trigger'>");
          //output.Write("<div class='icon icon-burguer'></div>");
          builder.Append("<div class='icon icon-burguer'></div>");
          //output.Write("</div>");
          builder.Append("</div>");
          //output.Write("<div class='pusher'><div class='aside-menu'>");
          builder.Append("<div class='pusher'><div class='aside-menu'>");


        if (list.Count() != 0)
        {
            BuildMenuItems(list.Select(navItem => Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(navItem.Id))).ToList(), builder, 1);
        }
        else
        {
            BuildMenuItems(builder);
        }


          //output.Write("</div></div>");
          builder.Append("</div></div>");
          //output.Write("<!--googleon: all-->");
          builder.Append("<!--googleon: all-->");
        }


      }
      catch (Exception ex)
      {
        Log.Error(ex.StackTrace, ex, GetType());
      }


      NavigationCode.Text = builder.ToString();
    }


    private void BuildMenuItems(StringBuilder builder)
    {
        Sitecore.Data.Items.Item menuItem = Sitecore.Context.Item;
        var backClass = "backhome";
        builder.AppendFormat("<div data-tag=\"{0}\" class=\"item\">", GetLink(menuItem));
        builder.Append("<div class='menu-title'>");
        {
            {
                builder.AppendFormat(
            string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</a>", GetLink(menuItem.Parent), (GetTitle(menuItem.Parent)))
            , backClass);
            }
        }

        builder.AppendLine("</div>");
        {
            builder.Append("<div class='menu-subtitle'>" + GetLinkText(menuItem) + "</div>");
        }
        builder.AppendLine("</div>");
    }




    private void BuildMenuItems(List<Sitecore.Data.Items.Item> menuItems, StringBuilder builder, int level)
    {
      {
        Sitecore.Data.Items.Item menuItem = Sitecore.Context.Item;
        var backClass = level == 1 ? "backhome" : "back";
        builder.AppendFormat("<div data-tag=\"{0}\" class=\"item\">", GetLink(menuItem));
        builder.Append("<div class='menu-title'>");
        {
          {
            builder.AppendFormat(
      string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</a>", GetLink(menuItem.Parent), (GetTitle(menuItem.Parent)))
      , backClass);
          }
        }

        builder.AppendLine("</div>");
        {
          builder.Append("<div class='menu-subtitle'>" + GetLinkText(menuItem) + "</div>");
        }

        builder.AppendLine("<div class=\"menu-item\">");
        foreach (Sitecore.Data.Items.Item item in menuItems)
        {
          if (IsVisible(item))
          {
            {
              if (EnablePjax(item))
              {
                builder.AppendFormat("<a href=\"{0}\" target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
              }
              else
              {
                builder.AppendFormat("<a href=\"{0}\" " + GetPjaxAttribute(item) + "  target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
              }
            }
          }
        }

        builder.AppendLine("</div>");
        builder.AppendLine("</div>");
      }
    }

        //private void BuildMenuItems(List<Sitecore.Data.Items.Item> menuItems, StringBuilder builder, int level)
        //{
        //    //if (HasVisibleChildren(menuItem))
        //    {
        //        Sitecore.Data.Items.Item menuItem = Sitecore.Context.Item;
        //        var backClass = level == 1 ? "backhome" : "back";
        //        builder.AppendFormat("<div data-tag=\"{0}\" class=\"item\">", GetLink(menuItem));
        //        builder.Append("<div class='menu-title'>");
        //        if (menuItem.ID.ToString() == ItemIds.SideMenuHome)
        //        {
        //            builder.AppendFormat(string.Format("<span href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</span>", GetLink(menuItem), GetLinkText(menuItem)), backClass);
        //        }
        //        else
        //        {
        //            menuItem = menuItem.Parent;
        //            //if (!HasVisibleChildren(menuItem))
        //            {
        //                builder.AppendFormat(
        //          string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</a>", GetLink(menuItem), (GetTitle(menuItem)))
        //          , backClass);
        //            }
        //        }

        //        builder.AppendLine("</div>");
        //        //if (level > 1)
        //        {
        //            builder.Append("<div class='menu-subtitle'>" + GetLinkText(menuItem) + "</div>");
        //        }

        //        builder.AppendLine("<div class=\"menu-item\">");
        //        foreach (Sitecore.Data.Items.Item item in menuItems)
        //        {
        //            if (IsVisible(item))
        //            {
        //                {
        //                    if (EnablePjax(item))
        //                    {
        //                        builder.AppendFormat("<a href=\"{0}\" target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
        //                    }
        //                    else
        //                    {
        //                        builder.AppendFormat("<a href=\"{0}\" " + GetPjaxAttribute(item) + "  target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
        //                    }
        //                }
        //            }
        //        }

        //        builder.AppendLine("</div>");
        //        builder.AppendLine("</div>");
        //    }
        //}

        private string GetPjaxAttribute(BaseItem item)
    {
      return ""; //"data-pjax=\"\"";
    }

    public string GetLinkText(BaseItem item)
    {
      return GetTitle((Sitecore.Data.Items.Item)item);
    }

    private bool IsVisible(Sitecore.Data.Items.Item item)
    {
      return item.Versions.Count > 0;
    }

    private bool HasVisibleChildren(Sitecore.Data.Items.Item item)
    {
      foreach (Sitecore.Data.Items.Item child in item.Children)
      {
        if (IsVisible(child))
        {
          return true;
        }
      }
      return false;
    }

    public string GetLink(Sitecore.Data.Items.Item item)
    {
      return LinkManager.GetItemUrl(item);
    }

    public string GetTitle(Sitecore.Data.Items.Item item)
    {
      if (!string.IsNullOrWhiteSpace(item["Title"])) return item["Title"];
      if (!string.IsNullOrWhiteSpace(item["MenuTitle"])) return item["MenuTitle"];
      return item.Name;
    }

    private bool EnablePjax(Sitecore.Data.Items.Item item)
    {
      bool downloadItem = false;

      if (item.ID.ToString().Equals(ItemIds.DownloadItem) ||
          item.ID.ToString().Equals(ItemIds.VideoGallery))
      {
        downloadItem = true;
      }
      if (item.TemplateID.ToString().Equals(TemplateIds.Forms))
      {
        downloadItem = true;
      }
      return downloadItem;
    }

    public string GetTarget(BaseItem item)
    {
      return "";// LinkManager.GetItemUrl((Sitecore.Data.Items.Item)item);
    }
  }
}
