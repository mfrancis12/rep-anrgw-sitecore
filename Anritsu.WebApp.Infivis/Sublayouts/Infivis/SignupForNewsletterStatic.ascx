﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SignupForNewsletterStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.SignupForNewsletterStatic" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="full-container nogap">
    <div class="signup-promo">
        <%=Editable(x=>x.Title)%>
        <%=RenderLink(x => x.Link, new { @class = "button" }, isEditable: true, contents: Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "SignUpInfivis"))%>
        <%--<a href="#" class="button">sign up</a>--%>
    </div>
</div>
