﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaticFreeForm.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.StaticFreeForm" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models" %>
<div class="container">
<h1><%= Editable(x=>x.PageTitle) %></h1>
<%= Model.Content %>
</div>