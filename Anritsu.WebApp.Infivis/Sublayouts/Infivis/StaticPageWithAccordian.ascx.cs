﻿using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models;
using Anritsu.WebApp.Infivis.Models.Components;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using System;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Glass.Mapper.Sc;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
  public partial class StaticPageWithAccordian : BaseGlassUserControl<IAccordionStatic>
  {
    public int itemCount = 0;

    private void Page_Load(object sender, EventArgs e)
    {
      if (Model == null)
      {
        SetErrorState();
      }
      IEnumerable<ITitleWithDescription> accrodinaItems = null;
      if (Model.Content == null)
        accrodinaItems = Model.GetChildren<ITitleWithDescription>();
      else
        accrodinaItems = Model.Content.GetChildren<ITitleWithDescription>();

      itemCount = accrodinaItems.Count();

      tabsDescription.DataSource = accrodinaItems;
      tabsDescription.DataBind();
      BindOptionalLinks();
      contentAsideDiv.Visible = HasOptionalLinkContent();
    }

    protected bool HasOptionalLinkContent()
    {
      return Model.Content.Links.Any() || !string.IsNullOrEmpty(Model.Content.Image.Src);
    }

    public string GetClass(int index)
    {

      if (index == itemCount - 1)
      {
        return "open";
      }
      else
      {
        return string.Empty;
      }
    }

    public string GetIconClass(int index)
    {
      if (index == itemCount - 1)
      {
        return "icon icon-minus-grey";
      }
      else
      {
        return "icon icon-plus-grey";
      }
    }

    public void BindOptionalLinks()
    {
      if (Model.Content.Links != null && Model.Content.Links.Any())
      {
        Item currentItem = SitecoreContext.GetCurrentItem<Item>();
        ReferenceField referenceField = currentItem.Fields["Content"];
        if (referenceField != null && referenceField.TargetItem != null)
        {
          GeneralLinks relatedGeneralLinks = new GeneralLinks(referenceField.TargetItem, "Links");
          if (relatedGeneralLinks.LinkItems.Any())
          {
            optionalLinks.DataSource = relatedGeneralLinks.LinkItems;
            optionalLinks.DataBind();
          }
        }
      }
    }

    protected void optionalLinks_ItemDataBound1(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
      GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
      HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
      if (linkItem != null && relatedAnchor != null)
      {
        relatedAnchor.InnerText = linkItem.LinkText;
        relatedAnchor.HRef = linkItem.Url;
        relatedAnchor.Target = linkItem.Target;
      }
    }

    protected void Products_PagePropertiesChanged(object sender, EventArgs e)
    {
      Products.DataBind();
    }

    protected void OptionalLinks_OnPreRender(object sender, EventArgs e)
    {
      if (optionalLinks.Items.Count == 0)
      {
        optionalLinks.Visible = false;
      }
    }
  }
}