﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaticPageWithOptionalLinks.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.StaticPageWithOptionalLinks" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>


<div class="container accordianclr">
    <div class="content-form accordion-page">
        <div class="content-detail">
            <h1><%=Editable(x=>x.Title)%></h1>
            <% if (!string.IsNullOrEmpty(Model.BannerImage.Src))
               { %>
            <p>
                <img src="<%=Model.BannerImage.Src%>" class="img100">
            </p>
            <% } %>
            <%=Editable(x=>x.Description) %>
            <div class="accordion">

                <asp:Repeater ID="tabsDescription" runat="server" ItemType="Anritsu.WebApp.GlobalWeb.Models.Components.ITitleWithDescription">
                    <ItemTemplate>
                       <div class="accordion-item">
                            <div class="accordion-title">
                                <span><%#Editable(Item,x=>x.Title) %></span>
                                <div class="accordion-icon">
                                    <div class="icon icon-plus-grey"></div>
                                </div>
                            </div>
                            <div class="accordion-content">
                                <%#Editable(Item,x=>x.Description) %>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

            <% if (Model.RelatedProducts.Count()!=0)
               { %>
             <h2 class="margin-top50"><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "products") %></h2>
            <div class="products-intro">
                <asp:ListView ID="Products" runat="server" ItemType="Anritsu.WebApp.Infivis.Models.Pages.IProductDetailStaticC1" OnPagePropertiesChanged="Products_PagePropertiesChanged">
                    <ItemTemplate>
                        <div class="products-item">
                              <a href="<%#Item.Url%>">
                                <img src="<%#Editable(Item, x=>x.SelectProduct.Thumbnail.Src) %>"></a>
                            <h3><%#Editable(Item, x=>x.SelectProduct.ModelNumber) %></h3>
                            <p><%#Editable(Item, x=>x.SelectProduct.ProductDescriptor) %></p>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <div class="seprate-line"></div>
            <div class="pagination">
                <span class="page">
                    <asp:DataPager ID="ProductsDataPager" runat="server" PagedControlID="Products" PageSize="6">
                        <Fields>
                            <asp:NumericPagerField ButtonType="Link" ButtonCount="5" CurrentPageLabelCssClass="active" NextPageText=">" PreviousPageText="<" />
                        </Fields>
                    </asp:DataPager>
                </span>
            </div>
             <% } %>      

        </div>
               
        <div class="content-aside">
        <% if (Model.Image != null && Model.Image.Src!=null)
        { %>
            <p class="mobile">
                <img src="<%= Model.Image.Src%>">
            </p>
             <p class="desktop">
                <img src="<%= Model.Image.Src%>">
            </p>
        <% } %>
            <% if (Model.Links.Count()!=0)
               { %>
            <h3><%= Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "optionallinks") %></h3>
            <asp:Repeater ID="optionalLinks" runat="server" OnItemDataBound="optionalLinks_ItemDataBound1">
                <ItemTemplate>
                    <p><a id="OptionalLink" runat="server"></a><i class="icon icon-arrow-blue"></i></p>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <% } %>
    </div>
</div>
