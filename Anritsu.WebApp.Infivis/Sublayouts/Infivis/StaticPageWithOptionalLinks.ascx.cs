﻿namespace Anritsu.WebApp.Infivis.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Anritsu.WebApp.Infivis.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Items;
    using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
    using Sitecore.SharedSource.FieldSuite.Types;
    using System;
    using System.Web.UI.HtmlControls;
    using System.Linq;

    public partial class StaticPageWithOptionalLinks : GlassUserControl<IStaticPageWithOptionalLinksInfivis>
    {
        public int itemCount = 0;
        private void Page_Load(object sender, EventArgs e)
        {
            itemCount = Model.GetChildren<ITitleWithDescription>().Count();

            tabsDescription.DataSource = Model.GetChildren<ITitleWithDescription>();
            tabsDescription.DataBind();
            BindOptionalLinks();

           
            var products = Model.RelatedProducts.Where(x => x.SelectProduct.IsDiscontinued == false);
            Products.DataSource = products.ToArray();
            Products.DataBind();
        }

        

        public void BindOptionalLinks()
        {
            IStaticPageWithOptionalLinksInfivis currentItem = SitecoreContext.GetItem<IStaticPageWithOptionalLinksInfivis>(SitecoreContext.GetCurrentItem<Item>().ID.ToString());
            if (currentItem.Links != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(SitecoreContext.Database.GetItem(currentItem.Id.ToString()), "Links");
                optionalLinks.DataSource = relatedGeneralLinks.LinkItems;
                optionalLinks.DataBind();
            }
        }

        protected void optionalLinks_ItemDataBound1(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }

        protected void Products_PagePropertiesChanged(object sender, EventArgs e)
        {
            Products.DataBind();
        }
    }
}
