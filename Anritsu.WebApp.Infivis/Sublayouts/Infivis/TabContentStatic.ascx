﻿<%@ control language="C#" autoeventwireup="true" codebehind="TabContentStatic.ascx.cs" inherits="Anritsu.WebApp.Infivis.SubLayouts.TabContentStatic" %>
<%@ import namespace="Anritsu.WebApp.Infivis.Models.Pages" %>
<div class="container marginTop">
    <div class="infivis tab-interface hero-tab marginTop20">
        <ul class="hero-tab-ul">   
            <li><a href="#section1" class="<%=Sitecore.Context.Item.ID.ToString().Equals(Anritsu.WebApp.Infivis.Constants.ItemIds.Home)?"active":""%>"> <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","NewsRelease") %></span></a></li>
            <li><a href="#section2" class="<%=Sitecore.Context.Item.TemplateID.ToString().Equals(Anritsu.WebApp.Infivis.Constants.TemplateIds.ProductCategory)?"active":""%>"> <span><%=GetTabTitle %></span></a></li>
            <li><a href="#section3"> <span><%=Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary","Events") %></span></a></li>                   
        </ul>
         <sc:placeholder runat="server" key="TabContent" />
    </div>
</div>


<%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>--%>


<%--<script type="text/javascript">
    window.onload = function () {
        var width = Math.floor(100 / $(".hero-tab ul li").size());
        if (width == 50) width = width - 1;
        $(".hero-tab ul li").css('width', width + "%");
    }
</script>--%>
