﻿using Anritsu.WebApp.GlobalWeb.Models.Common;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data.Items;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.WebApp.Infivis.Models;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class TabContentStatic : GlassUserControl<IVideogalleryStatic>
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        public string GetTabTitle
        {
            get
            {
                return Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "NewProducts");
            }
        }
    }
}