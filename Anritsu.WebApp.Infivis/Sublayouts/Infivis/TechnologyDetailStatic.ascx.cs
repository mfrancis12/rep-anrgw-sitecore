﻿namespace Anritsu.WebApp.Infivis.SubLayouts
{
  using Anritsu.WebApp.GlobalWeb.Models;
  using Anritsu.WebApp.GlobalWeb.Models.Components;
  using Anritsu.WebApp.GlobalWeb.Models.Pages;
  using Anritsu.WebApp.Infivis.Models.Pages;
  using Glass.Mapper.Sc.Web.Ui;
  using Sitecore.Data.Items;
  using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
  using Sitecore.SharedSource.FieldSuite.Types;
  using System;
  using System.Linq;
  using System.Web.UI.HtmlControls;

  public partial class TechnologyDetailStatic : GlassUserControl<IAccordionplusgridStatic>
  {
    public int itemCount = 0;
    public void Page_Load(object sender, EventArgs e)
    {
      if (Model == null)
        return;

      var products = Model.RelatedProducts.Where(x => x.SelectProduct != null && x.SelectProduct.IsDiscontinued == false);
      //var products = Model.RelatedProducts.Where(x => x.SelectProduct.IsDiscontinued == false);
      productList.DataSource = products;
      productList.DataBind();

      itemCount = Model.GetChildren<ITitleWithDescription>().Count();

      tabsDescription.DataSource = Model.GetChildren<ITitleWithDescription>();
      tabsDescription.DataBind();
      BindOptionalLinks();

      contentAsideDiv.Visible = HasOptionalLinkContent();
    }

    protected bool HasOptionalLinkContent()
    {
      return Model.OptionalLinks.Any() || !string.IsNullOrEmpty(Model.Image.Src);
    }

    public void BindOptionalLinks()
    {
      if (Model.OptionalLinks != null)
      {
        GeneralLinks relatedGeneralLinks = new GeneralLinks(SitecoreContext.GetItem<Item>(Model.Id), "OptionalLinks");
        optionalLinks.DataSource = relatedGeneralLinks.LinkItems;
        optionalLinks.DataBind();
      }
    }



    protected void OptionalLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
      GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
      if (linkItem != null)
      {
        HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("OptionalLink");
        relatedAnchor.InnerText = linkItem.LinkText;
        relatedAnchor.HRef = linkItem.Url;
        relatedAnchor.Target = linkItem.Target;
      }
    }

    protected void OptionalLinks_PreRender(object sender, EventArgs e)
    {
      if (optionalLinks.Items.Count == 0)
      {
        optionalLinks.Visible = false;
      }
    }
  }
}