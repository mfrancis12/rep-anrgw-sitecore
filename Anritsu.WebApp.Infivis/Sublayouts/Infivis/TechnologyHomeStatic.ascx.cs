﻿namespace Anritsu.WebApp.Infivis.SubLayouts
{
    using Anritsu.WebApp.GlobalWeb.Constants;
    using Anritsu.WebApp.GlobalWeb.Models;
    using Anritsu.WebApp.GlobalWeb.Models.Components;
    using Anritsu.WebApp.GlobalWeb.Models.Pages;
    using Anritsu.WebApp.Infivis.Models.Pages;
    using Glass.Mapper.Sc.Web.Ui;
    using Sitecore.Data.Items;
    using System;

    public partial class TechnologyHomeStatic : GlassUserControl<IStaticPageWithBannerAndThumbnail>
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (Model == null)
                return;
            contentBlock.DataSource = Model.GetChildren<IAccordionplusgridStatic>();            
            contentBlock.DataBind();
        }
    }
}