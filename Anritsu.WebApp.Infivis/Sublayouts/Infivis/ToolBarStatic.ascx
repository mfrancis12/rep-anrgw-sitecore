﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ToolBarStatic.ascx.cs" Inherits="Anritsu.WebApp.Infivis.SubLayouts.ToolBarStatic" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Models.Components" %>
<%@ Import Namespace="Anritsu.WebApp.GlobalWeb.Models.Components" %>
<%@ Import Namespace="Anritsu.WebApp.Infivis.Constants" %>

<asp:PlaceHolder ID="WrapperPH" runat="server">
<div id="fulltoolbar" class="aside-helper" runat="server">

    <asp:Repeater ID="repeaterToolBar" runat="server">
        <ItemTemplate>
            <div class="link">
                <div class="item-hover">
                    <a href="<%#GetCTALink(Eval("ToolbarType").ToString(),Eval("ToolbarLink").ToString()) %>"> <%# Eval("ToolbarTitle").ToString() %></a>
                </div>
                  <div class="item">
                    <div class="icon <%# Eval("ToolbarCssClassName").ToString() %>"><%#GetImage(Eval("ToolbarIconPath").ToString())%></div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
        <div id="fullsharedtoolbar" runat="server">
       <div class="link last">
    <div class="item-hover">
            
                <a  href="<%=GetShareLink("LINKEDIN")%>" target="_blank"><i class="icon icon-share-linkin"></i></a>
            
                <a href="<%=GetShareLink("TWITTER")%>" target="_blank"><i class="icon icon-share-twitter"></i></a>
            
                <a href="<%=GetShareLink("FACEBOOK")%>" target="_blank"><i class="icon icon-share-facebook"></i></a>
            
                <a href="<%=GetShareLink("EMAIL")%>" target="_blank"><i class="icon icon-share-email"></i></a>
            
        </div>
        <div class="item">
            <div class="icon icon-helper4"></div>
        </div>
    </div>
            </div>
	</div>

  <div id="sharedtoolbar" class="aside-helper" runat="server">
     <div class="link last">
 <div class="item-hover">
            
                <a  href="<%=GetShareLink("LINKEDIN")%>" target="_blank"><i class="icon icon-share-linkin"></i></a>
            
                <a href="<%=GetShareLink("TWITTER")%>" target="_blank"><i class="icon icon-share-twitter"></i></a>
            
                <a href="<%=GetShareLink("FACEBOOK")%>" target="_blank"><i class="icon icon-share-facebook"></i></a>
            
                <a href="<%=GetShareLink("EMAIL")%>" target="_blank"><i class="icon icon-share-email"></i></a>
            
        </div>
      <div class="item">
            <div class="icon icon-helper4"></div>
        </div>
   </div>
  	</div>


</asp:PlaceHolder>