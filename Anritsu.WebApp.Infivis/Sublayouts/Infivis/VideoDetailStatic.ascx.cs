﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Anritsu.WebApp.GlobalWeb.Models.Pages;
using Anritsu.WebApp.Infivis.Models.Pages;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Ui;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;

using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Anritsu.WebApp.GlobalWeb.Constants;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class VideoDetailStatic : GlassUserControl<IVideodetailStatic>
    {
        #region Global Variables
        Database db = Sitecore.Context.Database;
        string indexName = "anritsu_infivis_brightcovevideotemplates";
        //public Item videoItem;
        #endregion

        private void Page_Load(object sender, EventArgs e)
        {
            RelatedVideos.DataSource = Model.RelatedVideos;
            RelatedVideos.DataBind();

            BindRelatedLinks();
            BrightcoveMediaControl.DataSource = !string.IsNullOrEmpty(Model.Video) ? VideoItem(Model.Video).Id.ToString() : string.Empty;

            if (GsaHelpers.IsGsaCrawler())
                AddGsaMetaInfo();
        }

        private void AddGsaMetaInfo()
        {
            try
            {
                var ltMetaTags = (PlaceHolder)Parent.FindControl("GsaMetaPlaceHolder");
                if (ltMetaTags == null) return;
                var meta = new HtmlMeta
                {
                    Name = "videoduration",
                    Content = !string.IsNullOrEmpty(VideoItem(Model.Video).Length) ? GetDuration(VideoItem(Model.Video).Length) : ""
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
                meta = new HtmlMeta
                {
                    Name = "metareleasedate",
                    Content = !string.IsNullOrEmpty(VideoItem(Model.Video).LastModifiedDate) ? Convert.ToDateTime(VideoItem(Model.Video).LastModifiedDate).ToLocalTime().Date.ToFormattedDate() : ""
                };
                if (!string.IsNullOrWhiteSpace(meta.Content)) ltMetaTags.Controls.Add(meta);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message + " " + ex.StackTrace, "GsaMetaInfoAddition");
            }
        }


        private void BindRelatedLinks()
        {
            Item currentItem = SitecoreContext.GetCurrentItem<Item>();
            if (currentItem["RelatedLinks"] != null)
            {
                GeneralLinks relatedGeneralLinks = new GeneralLinks(currentItem, "RelatedLinks");
                RelatedLinks.DataSource = relatedGeneralLinks.LinkItems;
                RelatedLinks.DataBind();
            }
        }

        public string GetDuration(string videoLength)
        {
            string duration = string.Empty;
            if (!string.IsNullOrEmpty(videoLength))
            {
                TimeSpan time = TimeSpan.FromMilliseconds(Convert.ToInt32(videoLength));
                duration = time.ToString(@"hh\:mm\:ss");
            }
            return duration;
        }

        private string BindBrightcoveMedia()
        {
            var videoId = string.Empty;
            var selectedVideo = SitecoreContext.GetCurrentItem<Item>().Fields["Video"].Value;
            Item mediaItem = db.GetItem(selectedVideo, LanguageManager.GetLanguage("en"));
            if (mediaItem != null)
            {
                videoId = mediaItem.ID.ToString();
            }
            return videoId;
        }
        public IBrightcoveVideo VideoItem(string videoId)
        {
            if (videoId != null)
            {
                var contextService = new SitecoreContext();
                return contextService.GetItem<IBrightcoveVideo>(videoId, LanguageManager.GetLanguage("en"));
            }
            return null;
        }

        protected void RelatedLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            GeneralLinkItem linkItem = (GeneralLinkItem)e.Item.DataItem;
            HtmlAnchor relatedAnchor = (HtmlAnchor)e.Item.FindControl("relatedLink");
            relatedAnchor.InnerText = linkItem.LinkText;
            relatedAnchor.HRef = linkItem.Url;
            relatedAnchor.Target = linkItem.Target;
        }
    }
}