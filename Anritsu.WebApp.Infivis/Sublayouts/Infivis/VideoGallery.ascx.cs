﻿using Sitecore.ContentSearch;
using System;
using System.Collections.Generic;
using Sitecore.ContentSearch.Linq;
using Glass.Mapper.Sc;
using Anritsu.WebApp.GlobalWeb.Models;
using Anritsu.WebApp.Infivis.Constants;
using Anritsu.WebApp.GlobalWeb.Models.Components;
using Glass.Mapper.Sc.Web.Ui;
using System.Linq;
using System.Web.UI.WebControls;
using Anritsu.WebApp.Infivis.Models.Pages;
using Anritsu.WebApp.GlobalWeb.Search;
using Sitecore.Diagnostics;
using Sitecore.ContentSearch.Linq.Utilities;
using System.Linq.Expressions;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using Sitecore.Data.Items;
using Sitecore.Data;
using FacetValue = Sitecore.ContentSearch.Linq.FacetValue;
using SearchFacetValue = Anritsu.WebApp.GlobalWeb.Search.FacetValue;
using Anritsu.WebApp.GlobalWeb.Models.MediaFramework.Brightcove;
using Sitecore.Data.Managers;

namespace Anritsu.WebApp.Infivis.SubLayouts
{
    public partial class VideoGallery : GlassUserControl<IModelBase>
    {
        #region Global Variables

        private IProviderSearchContext _videocontext;
        protected IProviderSearchContext VideoSearchContext
        {
            get
            {
                return this._videocontext ?? (this._videocontext = ContentSearchManager.GetIndex(StaticVariables.VideoGalleryIndex).CreateSearchContext());
            }
        }

        private List<SearchFacetValue> _CategoryFacets;
        public List<SearchFacetValue> CategoryFacets
        {
            get
            {
                if (_CategoryFacets == null)
                {
                    List<SearchFacetValue> facets = new List<SearchFacetValue>();
                    return facets;
                }
                return (List<SearchFacetValue>)_CategoryFacets;
            }
            set { _CategoryFacets = value; }
        }

        private List<SearchFacetValue> _ParentCategoryFacets;
        public List<SearchFacetValue> ParentCategoryFacets
        {
            get
            {
                if (_ParentCategoryFacets == null)
                {
                    List<SearchFacetValue> facets = new List<SearchFacetValue>();
                    return facets;
                }
                return (List<SearchFacetValue>)_ParentCategoryFacets;
            }
            set { _ParentCategoryFacets = value; }
        }

        private List<string> _SelectedParentCategoryFacets;
        public List<string> SelectedParentCategoriesFacets
        {
            get
            {
                if (_SelectedParentCategoryFacets == null)
                {
                    List<string> facets = new List<string>();
                    return facets;
                }
                return (List<string>)_SelectedParentCategoryFacets;
            }
            set { _SelectedParentCategoryFacets = value; }
        }

        private List<string> _SelectedCategoryFacets;
        public List<string> SelectedCategoriesFacets
        {
            get
            {
                if (_SelectedCategoryFacets == null)
                {
                    List<string> facets = new List<string>();
                    return facets;
                }
                return (List<string>)_SelectedCategoryFacets;
            }
            set { _SelectedCategoryFacets = value; }
        }

        private List<string> _SelectedRootCategoryFacets;
        public List<string> SelectedRootCategoriesFacets
        {
            get
            {
                if (_SelectedRootCategoryFacets == null)
                {
                    List<string> facets = new List<string>();
                    return facets;
                }
                return (List<string>)_SelectedRootCategoryFacets;
            }
            set { _SelectedRootCategoryFacets = value; }
        }

        public int TotalRecords
        {
            get
            {
                object o = ViewState["TotalRecords"];
                if (o == null)
                    return 0;
                return (int)o;
            }
            set { ViewState["TotalRecords"] = value; }
        }

        public int PageIndex
        {
            get
            {
                object o = ViewState["PageIndex"];
                if (o == null)
                    return 1;
                return (int)o;
            }
            set { ViewState["PageIndex"] = value; }
        }

        public int RecordsPerPage
        {
            get
            {
                object o = ViewState["RecordsPerPage"];
                if (o == null)
                    return 20;
                return (int)o;
            }
            set { ViewState["RecordsPerPage"] = value; }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PageIndex = 1;
                BindVideos();
            }

            if (TotalRecords > 0)
            { CreatePagingControl(); }

        }

        private void BindVideos(bool facet = true)
        {
            var sitecoreService = new SitecoreService(Sitecore.Context.Database);
            List<Anritsu.WebApp.Infivis.Models.Pages.SearchItem> videoList = new List<Anritsu.WebApp.Infivis.Models.Pages.SearchItem>();

            //var query = VideoSearchContext.GetQueryable<Anritsu.WebApp.Infivis.Models.Pages.SearchItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(i => i.Language == Sitecore.Context.Language.Name && i.Parent == Sitecore.Context.Item.ID);
            var query = VideoSearchContext.GetQueryable<Anritsu.WebApp.Infivis.Models.Pages.SearchItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo)).Filter(i => i.Language == Sitecore.Context.Language.Name);// && i.Parent == Sitecore.Context.Item.ID);

            var predicate = GetSiteSearchRefinementPredicate();
            query = query.Where(predicate);

            var searchResults = query.GetResults();
            TotalRecords = searchResults.TotalSearchResults;

            //if (TotalRecords > 0)
            //{
            //    CreatePagingControl();
            //}

            foreach (var item in searchResults.Select(x => x.Document))
            {
                sitecoreService.Map(item);
                videoList.Add(item);
            }

            //********** This should not be needed
            videoList = UpdateVideoItems(videoList);
            //************************************

            videoList = videoList.Where(x => x != null && x.VideoItem != null).ToList();

            TotalRecords = videoList.Count;
            if (TotalRecords > 0)
            {
                CreatePagingControl();
            }


            videoList = videoList.OrderByDescending(x => Convert.ToDateTime(x.VideoItem.LastModifiedDate)).ToList();
            videoList = videoList.Skip(RecordsPerPage * (PageIndex - 1)).Take(RecordsPerPage).ToList();


            videos.DataSource = videoList;
            videos.DataBind();

            if (facet)
            {
                var rootcategoryFacets = CollectFacets(query.FacetPivotOn(x => x.FacetOn(y => y.VideoRootCategories)).GetFacets().Categories).FirstOrDefault();
                ParentCategoryFacets = CollectFacets(query.FacetPivotOn(x => x.FacetOn(y => y.VideoParentCategories)).GetFacets().Categories).FirstOrDefault().FacetValues.ToList();
                CategoryFacets = CollectFacets(query.FacetPivotOn(x => x.FacetOn(y => y.VideoCategories)).GetFacets().Categories).FirstOrDefault().FacetValues.ToList();

                if (rootcategoryFacets != null)
                {
                    int sortOrder = 0;
                    rptCategory.DataSource = rootcategoryFacets.FacetValues.OrderBy(i => (int.TryParse(Sitecore.Context.Database.Items[new Sitecore.Data.ID(i.Value)]["__Sortorder"], out sortOrder)?sortOrder:0)).ToList();

                    //rptCategory.DataSource = rootcategoryFacets.FacetValues.OrderBy(i => int.Parse(Sitecore.Context.Database.Items[new Sitecore.Data.ID(i.Value)]["__Sortorder"])).ToList();
                    rptCategory.DataBind();
                }
            }
        }

        private List<Anritsu.WebApp.Infivis.Models.Pages.SearchItem> UpdateVideoItems(List<Anritsu.WebApp.Infivis.Models.Pages.SearchItem> results)
        {
            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].Video == null)
                {
                    Sitecore.Data.ID itemid = results[i].ItemId;
                    Item item = Sitecore.Context.Database.Items[itemid];
                    if(!string.IsNullOrWhiteSpace(item["Video"]))   results[i].Video = Sitecore.Data.ID.Parse(item["Video"]).ToGuid().ToString();
                    results[i].Name = item["Name"];
                    results[i].ShortDescription = item["ShortDescription"];
                    results[i].Url = Sitecore.Links.LinkManager.GetItemUrl(item);
                }
            }

            return results;
        }

        /// <summary>
        /// Builds a predicate for the selected refinements in the request
        /// using the 'OR' operator to join refinements within a refinement group
        /// and the 'AND' operator to join refinement groups
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        protected Expression<Func<SearchItem, bool>> GetSiteSearchRefinementPredicate()
        {
            var predicate = PredicateBuilder.True<Anritsu.WebApp.Infivis.Models.Pages.SearchItem>();
            foreach (string fItem in SelectedRootCategoriesFacets)
            {
                predicate = predicate.Or(item => item.VideoRootCategories.Contains(fItem));
            }

            foreach (string fItem in SelectedParentCategoriesFacets)
            {
                predicate = predicate.Or(item => item.VideoParentCategories.Contains(fItem));
            }

            foreach (string fItem in SelectedCategoriesFacets)
            {
                predicate = predicate.Or(item => item.VideoCategories.Contains(fItem));
            }

            //predicate = predicate.And(Item => Item.Video!=null);

            return predicate;
        }

        protected void ClearAll_OnClick(object sender, EventArgs e)
        {
            SelectedRootCategoriesFacets = null;
            PageIndex = 1;
            BindVideos(false);
        }

        public string GetDuration(string length)
        {
            string duration = string.Empty;
            if (!string.IsNullOrEmpty(length))
            {
                TimeSpan time = TimeSpan.FromMilliseconds(Convert.ToInt32(length));
                duration = time.ToString(@"hh\:mm\:ss");
            }
            return duration;
        }

        #region Facets

        private IEnumerable<IFacet> CollectFacets(IEnumerable<FacetCategory> facetCategoryList)
        {

            var list = new List<Facet>();
            try
            {
                list.AddRange(from resultFacetCategory in facetCategoryList
                              let neestedFacetNames = resultFacetCategory.Name.Split(',')
                              select new Facet()
                              {
                                  DisplayName = neestedFacetNames[0],
                                  FieldName = neestedFacetNames[0],
                                  FacetValues = CollectFacetValues(resultFacetCategory.Values, neestedFacetNames, 0, new List<string>()),
                              });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }
            return list;
        }

        private IEnumerable<IFacet> CollectNestedFacets(List<FacetValue> facetValueList, string[] neestedFacetNames, int level, List<string> parentPathList)
        {
            var list = new List<Facet>();
            try
            {
                var parentPath = string.Join("/", parentPathList.ToArray());

                list.AddRange(facetValueList.Where(d => d.Name.StartsWith(parentPath, StringComparison.OrdinalIgnoreCase))
                    .Select(d => d.Name)
                    .Select(s => s.Substring(parentPath.Length))
                    .Where(s => s.Length > 0)
                    .Select(s => s.Substring(0, (s.IndexOf('/') < 0) ? s.Length : s.IndexOf('/')))
                    .Distinct().Select(resultFacetCategory => new Facet()
                    {
                        DisplayName = neestedFacetNames[level], // todo: use name resolver
                        FieldName = neestedFacetNames[level],
                        FacetValues = CollectFacetValues(facetValueList, neestedFacetNames, level, parentPathList)
                    }));
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }

            return list;
        }

        private IEnumerable<SearchFacetValue> CollectFacetValues(List<FacetValue> facetValueList, string[] neestedFacetNames, int level, List<string> parentPathList)
        {
            var list = new List<Anritsu.WebApp.GlobalWeb.Search.FacetValue>();
            try
            {
                var parentPath = string.Join("/", parentPathList.ToArray());

                if (level >= neestedFacetNames.Length)
                    return list;

                foreach (var resultFacetValue in
                    facetValueList
                        .Where(d => d.Name.StartsWith(parentPath, StringComparison.OrdinalIgnoreCase))
                        .Select(d => d.Name.Split('/'))
                        .Where(l => l.Count() > parentPathList.Count())
                        .Select(l => l.Skip(parentPathList.Count()).First())
                        .Distinct())
                {
                    var nextLevelPathList = new List<string>(parentPathList);
                    nextLevelPathList.Add(resultFacetValue);
                    var nextLevelPath = string.Join("/", nextLevelPathList.ToArray());

                    var sum = facetValueList.Where(d => d.Name.StartsWith(nextLevelPath, StringComparison.OrdinalIgnoreCase)).Select(d => d.AggregateCount).Sum();

                    list.Add(new Anritsu.WebApp.GlobalWeb.Search.FacetValue()
                    {
                        DisplayName = ResolveFacetValueName(resultFacetValue),
                        Value = resultFacetValue,
                        ParentId = ResolveFacetParent(resultFacetValue),
                        DocumentCount = sum,
                        NestedFacets = CollectNestedFacets(facetValueList, neestedFacetNames, level + 1, nextLevelPathList)
                    });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, GetType());
            }

            return list;
        }

        public string ResolveFacetValueName(string name)
        {
            Guid id;
            if (!Guid.TryParse(name, out id)) { return Sitecore.Context.Culture.TextInfo.ToTitleCase(name); }
            var item = Sitecore.Context.Database.GetItem(new ID(id));
            return Sitecore.Context.Culture.TextInfo.ToTitleCase(item != null ? GetFacetName(item) : name);
        }

        private string GetFacetName(Item item)
        {
            var fallbackItem = SiteSearchHelper.GetSitecoreFallbackItem(item, item.Fields["Category Title"]);

            if (!string.IsNullOrEmpty(fallbackItem["Category Title"]))
            {
                return fallbackItem["Category Title"];
            }
            if (!string.IsNullOrEmpty(fallbackItem["PageTitle"]))
            {
                return fallbackItem["PageTitle"];
            }
            if (fallbackItem.Fields["Title"] != null && !string.IsNullOrEmpty(fallbackItem["Title"]))
            {
                return fallbackItem["Title"];
            }
            return Sitecore.Context.Culture.TextInfo.ToTitleCase(item.Name);
        }

        public string ResolveFacetParent(string id)
        {
            string parentid = string.Empty;

            if (!string.IsNullOrEmpty(id))
            {
                Guid itemId = new Guid(id);
                Item item = Sitecore.Context.Database.GetItem(new ID(itemId));
                if (item != null && item.ParentID.ToString() != null)
                {
                    parentid = item.ParentID.ToGuid().ToString();
                }
            }

            return parentid;
        }

        #endregion

        public List<SearchFacetValue> GetParentCategoryFacets(SearchFacetValue facetValue)
        {
            Guid itemId = new Guid(facetValue.Value);
            return ParentCategoryFacets.Where(x => x.ParentId.Equals(itemId.ToString())).ToList();
        }

        public List<SearchFacetValue> GetCategoryFacets(SearchFacetValue facetValue)
        {
            Guid itemId = new Guid(facetValue.Value);
            return CategoryFacets.Where(x => x.ParentId.Equals(itemId.ToString())).ToList();
        }

        private void GetSelectedFacets()
        {
            List<string> selectedCategoryFacets = new List<string>();
            List<string> selectedSubcategoryFacets = new List<string>();
            List<string> selectedFamilyFacets = new List<string>();

            foreach (RepeaterItem item in rptCategory.Items)
            {
                Repeater subCategoryRepeater = (Repeater)item.FindControl("rptSubCategory");
                CheckBox category = (CheckBox)item.FindControl("chkCategory");
                if (category.Checked)
                {
                    string id = ((HiddenField)item.FindControl("hiddenCategory")).Value;
                    if (!string.IsNullOrEmpty(id))
                    {
                        selectedCategoryFacets.Add(id);
                    }
                }

                foreach (RepeaterItem subCategoryItem in subCategoryRepeater.Items)
                {
                    CheckBox subCategory = (CheckBox)subCategoryItem.FindControl("chkSubCategory");
                    if (subCategory.Checked)
                    {
                        string id = ((HiddenField)subCategoryItem.FindControl("hiddenSubCategory")).Value;
                        if (!string.IsNullOrEmpty(id))
                        {
                            selectedSubcategoryFacets.Add(id);
                        }
                    }

                    Repeater familyRepeater = (Repeater)subCategoryItem.FindControl("rptFamily");
                    foreach (RepeaterItem familyItem in familyRepeater.Items)
                    {
                        CheckBox family = (CheckBox)familyItem.FindControl("chkFamily");
                        if (family.Checked)
                        {
                            string id = ((HiddenField)familyItem.FindControl("hiddenfamily")).Value;
                            if (!string.IsNullOrEmpty(id))
                            {
                                selectedFamilyFacets.Add(id);
                            }
                        }
                    }
                }
            }

            SelectedRootCategoriesFacets  = selectedCategoryFacets;
            SelectedParentCategoriesFacets  = selectedSubcategoryFacets;
            SelectedCategoriesFacets = selectedFamilyFacets;
        }

        private void Page_Unload(object sender, EventArgs e)
        {
            this.VideoSearchContext.Dispose();
        }

        protected void SubmitVideoGalleryFacets_Click(object sender, EventArgs e)
        {
            GetSelectedFacets();
            PageIndex = 1;
            BindVideos(false);
        }

        private void CreatePagingControl()
        {
            placeHolderPager.Controls.Clear();

            if (this.PageIndex > 1)
            {
                LinkButton button = new LinkButton();
                button.ID = "lnkPrevPage";
                button.Text = "<";
                button.Command += new CommandEventHandler(Pagination_Click);
                button.CommandName = "previous";
                placeHolderPager.Controls.Add(button);
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderPager.Controls.Add(spacer);
            }

            int Pages = this.TotalRecords / this.RecordsPerPage;
            int PageLenth = 9;
            PageLenth = (PageLenth > Pages) ? Pages : PageLenth;
            int j = 0;

            if (this.PageIndex / 10 >= 1)
            {
                PageLenth = this.PageIndex + 1;
                j = PageLenth - 10;
            }

            if (PageLenth >= Pages)
                PageLenth = Pages;

            if (this.TotalRecords.Equals(this.RecordsPerPage))
                PageLenth = 0;

            if ((this.TotalRecords % this.RecordsPerPage) == 0)
                PageLenth = PageLenth - 1;

            for (int i = j; i < PageLenth + 1; i++)
            {
                if (this.PageIndex.Equals(i + 1))
                {
                    Label lnk = new Label();
                    lnk.Text = (i + 1).ToString();
                    lnk.CssClass = "active floatLft";
                    placeHolderPager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderPager.Controls.Add(spacer);
                }
                else
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnkPage" + (i + 1).ToString();
                    lnk.Text = (i + 1).ToString();
                    lnk.Command += new CommandEventHandler(Pagination_Click);
                    lnk.CommandArgument = (i + 1).ToString();
                    placeHolderPager.Controls.Add(lnk);
                    Literal spacer = new Literal();
                    spacer.Text = "&nbsp";
                    placeHolderPager.Controls.Add(spacer);
                }
            }

            if (this.PageIndex * this.RecordsPerPage < this.TotalRecords)
            {
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";
                placeHolderPager.Controls.Add(spacer);
                LinkButton lnk = new LinkButton();
                lnk.ID = "lnkNextPage";
                lnk.Text = ">";
                lnk.Command += new CommandEventHandler(Pagination_Click);
                lnk.CommandName = "next";
                placeHolderPager.Controls.Add(lnk);
            }
        }

        protected void Pagination_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName.Equals("previous"))
            {
                PageIndex = PageIndex - 1;
            }
            else if (e.CommandName.Equals("next"))
            {
                PageIndex = PageIndex + 1;
            }
            else
            {
                PageIndex = Convert.ToInt32(e.CommandArgument.ToString());
            }

            GetSelectedFacets();
            BindVideos(false);
        }

        protected string GetPaginationName()
        {
            return (TotalRecords > 0 ? (((PageIndex - 1) * RecordsPerPage) + 1) : 0) + " - " + ((PageIndex * RecordsPerPage) > TotalRecords ? TotalRecords : (PageIndex * RecordsPerPage)) + " " + Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "ofabout") + " " + TotalRecords + " " + Sitecore.Globalization.Translate.TextByDomain("GlobalDictionary", "results");
        }
    }
}
