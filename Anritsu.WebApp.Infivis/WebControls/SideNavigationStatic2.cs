﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Anritsu.WebApp.GlobalWeb.Constants;
using Glass.Mapper.Sc;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Shell.Applications.ContentManager.ReturnFieldEditorValues;
using Sitecore.Web;
using Sitecore.Web.UI;
using Anritsu.WebApp.GlobalWeb.SiteSearch;
using System.Text;
using Anritsu.WebApp.SitecoreUtilities.CustomCaches;
using Glass.Mapper.Sc.Web.Ui;
using Anritsu.WebApp.Infivis.Models.Common;
using System.Linq;

namespace Anritsu.WebApp.Infivis.WebControls
{
    public class SideNavigationStatic2 : WebControl
    {
        private string INFIVIS_HOME = "{FA28063B-F46B-46F0-8C16-CE9EE4C8295B}";
        private string WEB_FORMS_STATIC = "{39B9E2E3-4AF7-4D1A-BEE8-14865B3FD8E5}";
        string LINK_NAV_TEMPLATE_ID = "{F5BA69FB-7599-4DAE-8818-274905E44BF7}";
        private string SideNavigationCacheKey = "side_navigation_cache_key2";

        protected override string GetCachingID()
        {
            if (SiteSearchHelper.IsSearchCrawler(HttpContext.Current.Request))
                return GetType().FullName + "_Search";
            if (HttpContext.Current.Request.Headers["X-PJAX"] != null)
                return GetType().FullName + "_Pjax";

            return GetType().FullName;
        }

        private bool _useDomainUrl
        {
            get
            {
                try
                {
                    if (Parameters == null || !Parameters.Contains("UseDomainUrl")) return false;
                    var webControlParameters =
                        WebUtil.ParseUrlParameters(Parameters);
                    return webControlParameters["UseDomainUrl"].Equals("1");
                }
                catch (Exception)
                {

                    return false;
                }
            }
        }
        private bool _enableQuickLinks
        {
            get
            {
                try
                {
                    if (Parameters == null || !Parameters.Contains("EnableQuickLinks")) return true;
                    var webControlParameters =
                        WebUtil.ParseUrlParameters(Parameters);
                    return webControlParameters["EnableQuickLinks"].Equals("1");
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        protected override void DoRender(HtmlTextWriter output)
        {
            if (HttpContext.Current.Request.Headers["X-PJAX"] != null || SiteSearchHelper.IsSearchCrawler(HttpContext.Current.Request))
                return;

            try
           
            {
             
                //failed to get html from cache so we are going to build it up...
                Anritsu.WebApp.GlobalWeb.Utilities.Navigation navigation = new Anritsu.WebApp.GlobalWeb.Utilities.Navigation();
                StringBuilder builder = navigation.BuildHorizontalMenuSource();
                //write builder.
                output.Write(builder.ToString());
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, ex, GetType());
            }
        }

        private void BuildMenuItems(Item menuItem, StringBuilder builder, int level)
        {
            List<Item> navItems = GetSideNavigationItems(menuItem);
            
            if (HasVisibleChildren(navItems))
            {
                var backClass = level == 1 ? "backhome" : "back";
                builder.AppendFormat("<div data-tag=\"{0}\" class=\"item\">", GetLink(menuItem));
                if (level > 1)
                {
                    builder.Append("<div class='menu-subtitle'>" + GetLinkText(menuItem) + "</div>");
                }
                builder.Append("<div class='menu-title'>");

                if (menuItem.ID.ToString() == INFIVIS_HOME)
                {
                    builder.AppendFormat(string.Format("<span href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</span>", GetLink(menuItem), GetLinkText(menuItem)), backClass);
                }
                else
                {
                    if (!HasVisibleChildren(navItems))
                    {
                        builder.AppendFormat(string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem) + ">{1}</a>",
                          GetLink(menuItem),
                          ((menuItem.ID.ToString().Equals(INFIVIS_HOME)) ? menuItem["Title"] : Translate.TextByDomain("GlobalDictionary", "backhome"))),
                          backClass);
                    }
                    else
                    {
                        if (level == 2)
                        {
                            var contextService = new SitecoreContext();
                            Item homeItem = contextService.GetItem<Item>(ItemIds.SideMenuHome);
                            builder.AppendFormat(string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem.Parent) + " target=\"{1}\">{2}</a>",
                              GetLink(menuItem.Parent), GetTarget(menuItem.Parent),
                              homeItem["Title"]),
                              backClass);
                        }
                        else
                        {
                            builder.AppendFormat(string.Format("<a href='{0}' " + GetPjaxAttribute(menuItem.Parent) + " target=\"{1}\">{2}</a>",
                              GetLink(menuItem.Parent),
                              GetTarget(menuItem.Parent),
                            ((menuItem.ID.ToString().Equals(ItemIds.SideMenuHome)) ? menuItem["Title"] : Translate.TextByDomain("GlobalDictionary", "backtomenu"))),
                            backClass);
                        }
                    }
                }

                builder.AppendLine("</div>");
               
                 builder.AppendLine("<div class=\"menu-item\">");
                foreach (Item item in navItems)
                {
                    
                    if (item != null && string.Compare(item.TemplateID.ToString(), LINK_NAV_TEMPLATE_ID, true) == 0)
                    {
                        var NavLinkModel = item.GlassCast<ILinkNavigation>();
                        if (NavLinkModel != null && !NavLinkModel.IsDisabled && NavLinkModel.NavigationUrl != null
                                    && !string.IsNullOrEmpty(NavLinkModel.NavigationUrl.Url))
                        {
                           
                            builder.AppendFormat("<a href=\"{0}\" target=\"{1}\" class=\"link\">{2}</a>",
                                NavLinkModel.NavigationUrl.Url, NavLinkModel.NavigationUrl.Target, NavLinkModel.NavigationUrl.Text);
                        }
                    }
                   else if (IsVisible(item))
                    {
                        List<Item> navItems2 = GetSideNavigationItems(item);

                        if (HasVisibleChildren(navItems2))
                        {
                            if (EnablePjax(item))
                            {
                                builder.AppendFormat("<a href=\"{0}\" class=\"haschild\" target=\"{1}\">{2}</a>",
                                 GetLink(item), GetTarget(item), GetLinkText(item));

                            }
                            else
                            {
                                builder.AppendFormat("<a href=\"{0}\" " + GetPjaxAttribute(item) + " class=\"haschild\" target=\"{1}\">{2}</a>",
                                    GetLink(item), GetTarget(item), GetLinkText(item));
                            }
                        }
                        else
                        {


                            if (EnablePjax(item))
                            {
                                builder.AppendFormat("<a href=\"{0}\" target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
                            }
                            else
                            {
                               builder.AppendFormat("<a href=\"{0}\" " + GetPjaxAttribute(item) + "  target=\"{1}\" class=\"link\">{2}</a>", GetLink(item), GetTarget(item), GetLinkText(item));
                            }

                        }
                    }
                }
                builder.AppendLine("</div>");
                builder.AppendLine("</div>");
                if (!sideNavRendered.ContainsKey(menuItem.ID.ToString()))
                {
                    sideNavRendered.Add(menuItem.ID.ToString(), menuItem.ID.ToString());
                }
                foreach (Item item in navItems)
                {
                    if (IsVisible(item) && !sideNavRendered.ContainsKey(item.ID.ToString()))
                    {
                        BuildMenuItems(item, builder, level + 1);
                    }
                }
            }
        }
        private Dictionary<string, string> sideNavRendered = new Dictionary<string, string>();

        private Dictionary<string, List<Item>> sideNavs = new Dictionary<string, List<Item>>();

        private List<Item> GetSideNavigationItems(Item item)
        {

            string navKey = item.ID.ToString();
            if (!sideNavs.ContainsKey(navKey))
            {
                List<Item> sideNavigationItemList = new List<Item>();

                foreach (var navItemId in Sitecore.StringUtil.Split(item["SideNavigationItems"], '|', true))
                {
                    if (!string.IsNullOrEmpty(navItemId) && Sitecore.Data.ID.IsID(navItemId))
                    {
                        Item item2 = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(navItemId));
                        if (item2 != null)
                        {
                            sideNavigationItemList.Add(item2);
                        }
                    }
                }
                sideNavs.Add(navKey, sideNavigationItemList);
            }

            return sideNavs[navKey];
        }

        private string GetPjaxAttribute(BaseItem item)
        {
            return "data-pjax=\"\"";
            //if (item == null) return "data-pjax=\"\"";
            //var lnk = (LinkField)item.Fields["Link"];
            //return item == null ? "data-pjax=\"\"" : (lnk.Target == "_blank" || lnk.Target.ToUpperInvariant() == "NEW BROWSER" ? "" : "data-pjax=\"\"");
        }

        public string GetLinkText(Item item)
        {
            if (!string.IsNullOrWhiteSpace(item["MenuTitle"])) return item["MenuTitle"];
            if (!string.IsNullOrWhiteSpace(item["Title"])) return item["Title"];
            return item.Name;
        }

        private bool IsVisible(Item item)
        {
            if (item != null)
                return item.Versions.Count > 0;
            return false;
        }

        private bool HasVisibleChildren(List<Item> navItems)
        {
            foreach (Item child in navItems)
            {
                if (IsVisible(child))
                {
                    return true;
                }
            }
            return false;
        }

        public string GetLink(Item item)
        {
            string itemUrl = LinkManager.GetItemUrl(item);

            string selectedItemsValue = string.Empty;
            //var lnk = (LinkField)item.Fields["Link"];
            //if (lnk.IsInternal && lnk.TargetItem != null)
            //{
            //  if (Parent.Controls.Count >= 1)
            //  {
            //for (int i = 0; i < Parent.Controls.Count; i++)
            //{
            //    var webControl = Parent.Controls[i] as WebControl;
            //    if (webControl != null && webControl.Parameters.Contains("UseDomainUrl"))
            //    {
            //        WebControl sideNavigationWebControl = (WebControl)Parent.Controls[i];
            //        NameValueCollection webControlParameters =
            //            WebUtil.ParseUrlParameters(sideNavigationWebControl.Parameters);
            //        selectedItemsValue = webControlParameters["UseDomainUrl"].Equals("1")
            //            ? "//" + Context.Request.Url.Host + LinkManager.GetItemUrl(lnk.TargetItem)
            //            : LinkManager.GetItemUrl(lnk.TargetItem);
            //    }
            //}
            selectedItemsValue = _useDomainUrl
                       ? "//" + Context.Request.Url.Host + LinkManager.GetItemUrl(item)
                       : LinkManager.GetItemUrl(item);
            //  }
            //  else
            //  {
            //    selectedItemsValue = LinkManager.GetItemUrl(item);
            //  }
            //}
            //else if (lnk.LinkType.ToLower().Equals("anchor"))
            //{
            //  selectedItemsValue = !string.IsNullOrEmpty(lnk.Anchor) ? "#" + lnk.Anchor : string.Empty;
            //}
            //else
            //{
            //  selectedItemsValue = lnk.Url;
            //}
            return selectedItemsValue;
        }

        private bool EnablePjax(Item item)
        {
            bool downloadItem = false;

            //var lnk = (LinkField)item.Fields["Link"];

            if (item != null)
            {
                if (item.ID.ToString().Equals(ItemIds.DownloadItem) ||
                    item.ID.ToString().Equals(ItemIds.VideoGallery))
                {
                    downloadItem = true;
                }
                if (item.TemplateID.ToString().Equals(TemplateIds.Forms) || item.TemplateID.ToString().Equals(WEB_FORMS_STATIC))
                {
                    downloadItem = true;
                }
            }
            return downloadItem;
        }

        public string GetTarget(BaseItem item)
        {
            return "";
            //if (item == null) return string.Empty;
            //var lnk = (LinkField)item.Fields["Link"];
            //return lnk != null ? lnk.Target : string.Empty;
        }
    }
}