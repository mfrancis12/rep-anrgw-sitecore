﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Links;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web.UI.Sheer;

namespace Anritsu.WebApp.SitecoreUtilities.BrowseUrl
{
    public class GetSelectedItemUrl : Command
    {
        private Database _db;
        public Database Master
        {
            get { return _db ?? (_db = Factory.GetDatabase("master")); }
            set { _db = value; }
        }

        public override void Execute(CommandContext context)
        {
            if (context == null || context.Items.Length != 1) return;
            var item = context.Items[0];

            //// Change the context site to website as the context site will be Shell.
            //// Revert back context site to shell once the context item url is obtained.
            
            var shellSite = Sitecore.Context.GetSiteName();
            Sitecore.Context.SetActiveSite("GlobalWeb");
            var options = LinkManager.GetDefaultUrlOptions();
            options.LanguageEmbedding = LanguageEmbedding.Never;
            var url = LinkManager.GetItemUrl(item, options);
            var domain = HttpContext.Current.Request.Url.GetComponents(UriComponents.Scheme | UriComponents.Host, UriFormat.Unescaped);
            Sitecore.Context.SetActiveSite(shellSite);

            SheerResponse.Alert(domain + Sitecore.StringUtil.EnsurePrefix('/', item.Language.Name) + Sitecore.StringUtil.EnsurePrefix('/', url));
        }

        public override CommandState QueryState(CommandContext context)
        {
            var item = context.Items[0];
            var homeItem = Master.GetItem("{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}");
            return item.Axes.IsDescendantOf(homeItem) ? CommandState.Enabled : CommandState.Disabled;
        }
    }
}
