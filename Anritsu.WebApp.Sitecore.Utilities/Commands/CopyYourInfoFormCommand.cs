﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.SitecoreUtilities.Commands
{
    public class CopyYourInfoFormCommand : Sitecore.Shell.Framework.Commands.Command
    {
        public override void Execute(Sitecore.Shell.Framework.Commands.CommandContext context)
        {
            if (context.Items.Length == 0)
                return;

            Sitecore.Data.Items.Item destinationItem = context.Items[0];
            if (destinationItem == null)
                return;

            //retrieve the base YourInfo item
            Sitecore.Data.Items.Item myPaymentForm = destinationItem.Database.GetItem("{65C7457A-97C6-41C2-A34C-ABCF1C04D1A2}");
            if (myPaymentForm == null)
                return; // instead of just exiting here, you may want to log an error first

            //copy the YourInfo item to the location in the content tree from which the command template was triggered
            Sitecore.Data.Items.Item copyOfMyPaymentForm = myPaymentForm.CopyTo(destinationItem, "Your Info");

            //perform any necessary post-processing of your newly copied item
        }
    }
}