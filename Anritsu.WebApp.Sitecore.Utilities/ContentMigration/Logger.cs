﻿using System;
using System.IO;
using System.Text;

namespace Anritsu.WebApp.SitecoreUtilities.ContentMigration
{
    public class Logger : IDisposable
    {
        public Logger(string filePath)
        {
            if (File.Exists(filePath))
            {
                _fileStream = File.Open(filePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            }
            else
            {
                _fileStream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
            }
            _streamWriter = new StreamWriter(_fileStream, Encoding.UTF8);
        }

        ~Logger()
        {
            Dispose(false);
        }

        private readonly FileStream _fileStream;
        private readonly StreamWriter _streamWriter;
        private bool _disposed;

        public void Log(string formatString, params object[] args)
        {
            if (_disposed) throw new ObjectDisposedException("Logger");
            _streamWriter.WriteLine(formatString, args);
            _streamWriter.Flush();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (_streamWriter != null)
                {
                    _streamWriter.Close();
                    if (disposing) _streamWriter.Dispose();
                }
                if (_fileStream != null)
                {
                    _fileStream.Close();
                    if (disposing) _fileStream.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}