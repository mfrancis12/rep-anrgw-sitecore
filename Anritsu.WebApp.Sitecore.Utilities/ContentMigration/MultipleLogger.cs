﻿using System;
using System.Collections.Generic;

namespace Anritsu.WebApp.SitecoreUtilities.ContentMigration
{
    public class MultipleLogger : IDisposable
    {
        public MultipleLogger() { }

        ~MultipleLogger() { Dispose(false); }

        public void Add(string name, string filePath)
        {
            if (_disposed) throw new ObjectDisposedException("MultiLogger");
            _loggers.Add(name, new Logger(filePath));
        }

        private readonly Dictionary<string, Logger> _loggers = new Dictionary<string, Logger>();
        private bool _disposed;

        public Logger this[string name]
        {
            get
            {
                if (_disposed) throw new ObjectDisposedException("MultiLogger");
                return _loggers[name];
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                foreach (var key in _loggers.Keys) _loggers[key].Dispose();
                _loggers.Clear();
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}