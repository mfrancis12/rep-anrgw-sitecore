﻿using Anritsu.WebApp.SitecoreUtilities.ContentSearch.ResultItems;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.ContentSearch
{
    public class EmailTemplateRepository
    {
        private string IndexName = "anritsu_emailtemplates";

        /// <summary>
        /// 
        /// </summary>
        protected IProviderSearchContext ContentSearchContext
        {
            get { return ContentSearchManager.GetIndex(IndexName).CreateSearchContext(); }
        }

        /// <summary>
        /// returns list of email templates from index based on passed emailTemplateKey
        /// </summary>
        /// <param name="emailTemplateKey"></param>
        /// <returns></returns>
        public IEnumerable<Item> GetEmailTemplates(string emailTemplateKey, ID templateId)
        {
            return ContentSearchContext
                      .GetQueryable<EmailTemplateResultItem>()
                      .Where(et => et.TemplateKey.Equals(emailTemplateKey) && et.TemplateId == templateId && et.Language == Sitecore.Context.Language.Name)
                      .GetResults()
                      .Where(x => x.Document.GetItem() != null)
                      .Select(x => x.Document.GetItem());
        }
    }
}