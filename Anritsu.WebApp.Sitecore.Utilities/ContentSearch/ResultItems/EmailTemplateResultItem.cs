﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.WebApp.SitecoreUtilities.ContentSearch.ResultItems;

namespace Anritsu.WebApp.SitecoreUtilities.ContentSearch.ResultItems
{
    public class EmailTemplateResultItem : SearchResultItem
    {
        [IndexField("templatekey_computed")]
        public virtual string TemplateKey { get; set; }
    }
}