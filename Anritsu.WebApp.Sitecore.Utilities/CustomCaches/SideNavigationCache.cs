﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Caching;
using Sitecore;
using System.IO;
using Sitecore.ContentSearch.Linq;

namespace Anritsu.WebApp.SitecoreUtilities.CustomCaches
{
    public class PublishCacheDependency : System.Web.Caching.CacheDependency
    {
        public PublishCacheDependency()
        {
            Sitecore.Events.Event.Subscribe("publish:end", this.PublishCacheDependency_OnPublishEnd);
            Sitecore.Events.Event.Subscribe("publish:end:remote", this.PublishCacheDependency_OnPublishEnd);
        }

        public void PublishCacheDependency_OnPublishEnd(object sender, System.EventArgs eventArgs)
        {
            this.NotifyDependencyChanged(sender, eventArgs);
        }

        protected override void DependencyDispose()
        {
            Sitecore.Events.Event.Unsubscribe("publish:end", this.PublishCacheDependency_OnPublishEnd);
            Sitecore.Events.Event.Unsubscribe("publish:end:remote", this.PublishCacheDependency_OnPublishEnd);
        }
    }

    public class SideNavigationCacheManager
    {
        public string GetSideNavigationHtml(string key)
        {
            var cache = System.Web.HttpRuntime.Cache;
            var content = cache[key] as string;
            return (!string.IsNullOrEmpty(content)) ? content : string.Empty;
        }
    }
}
