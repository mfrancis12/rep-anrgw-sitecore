﻿using Sitecore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.SitecoreUtilities.CustomFields
{
    public class BrightcoveVideoId : Sitecore.Buckets.FieldTypes.IDataSource
    {
        public Sitecore.Data.Items.Item[] ListQuery(Sitecore.Data.Items.Item item)
        {
            var mediaContentFolder = Sitecore.Configuration.Factory.GetDatabase("master").GetItem("{7436E92E-6060-4B00-A2A2-7F435DD0CF40}");//Change Id to the one in qa and dev


            var query = string.Format("fast:{0}//*[@@templateid='{1}']",
                     StringUtil.EscapeApostrophe(mediaContentFolder.Paths.FullPath), "{6A5C6835-6E11-4602-A11D-B626E9255397}");

            var videoList = Sitecore.Configuration.Factory.GetDatabase("master").SelectItems(query).ToList();

            string[] ids;

            //foreach(var videoItem in videoList)
            //{
            //    //ids.Add(videoItem["ID"]);

            //    //ids[videoItem] = videoItem["ID"];
            //}
            var sortedList = videoList.OrderBy(x => x.Fields["Id"].Value).ToArray();
            // var productsGlobalReferrers = videoList.Select(x => Globals.LinkDatabase.GetReferrers(x, new ID("{92CC0A07-4718-42CD-83D8-6C03C6D2DF7B}")).Where(y => y.SourceItemLanguage.Equals(x.Language))).ToList();


            return sortedList;
        }
    }
}