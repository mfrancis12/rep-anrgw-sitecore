﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.SitecoreUtilities.CustomFields
{
    public class FieldDropLink : Sitecore.Shell.Applications.ContentEditor.LookupEx
    {
       
        protected override string GetItemHeader(Sitecore.Data.Items.Item item)
        {
            if (string.IsNullOrEmpty(this.FieldName)
              || item[this.FieldName].StartsWith("@",StringComparison.OrdinalIgnoreCase)) // don't impact default usage
            {
                return base.GetItemHeader(item);
            }

            if (string.IsNullOrEmpty(item[this.FieldName]))
            {
                return item.DisplayName;
            }

            return item[this.FieldName];
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (String.IsNullOrEmpty(this.Source)
              || !this.Source.Contains("="))
            {
                return;
            }

            NameValueCollection args = Sitecore.Web.WebUtil.ParseUrlParameters(
              this.Source);

            if (!String.IsNullOrEmpty(args["field"]))
            {
                this.FieldName = args["field"];
            }

            this.Source = args["source"] == null ? String.Empty : args["source"];
        }
    }
}
