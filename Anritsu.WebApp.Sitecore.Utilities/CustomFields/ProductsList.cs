﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Anritsu.WebApp.SitecoreUtilities.CustomFields
{
    public class ProductsList : Sitecore.Buckets.FieldTypes.IDataSource
    {
        public Sitecore.Data.Items.Item[] ListQuery(Sitecore.Data.Items.Item item)
        {
            var itemGlobalProduct = Sitecore.Configuration.Factory.GetDatabase("master").GetItem("{A7737E86-7DAA-43E5-A864-B5415F2E6D44}");
            var query = string.Format("{0}//*[contains(@ProductFamily,'{1}') and @IsDiscontinued!='1']",
                StringUtil.EscapeApostrophe(itemGlobalProduct.Paths.FullPath), item.ID);
            var productsGlobal = Sitecore.Configuration.Factory.GetDatabase("master").SelectItems(query);

            var productsGlobalReferrers = productsGlobal.Select(x => Globals.LinkDatabase.GetReferrers(x, new ID("{92CC0A07-4718-42CD-83D8-6C03C6D2DF7B}")).Where(y => y.SourceItemLanguage.Equals((x.Language)) || y.SourceItemLanguage.Name.Equals("en"))).ToList();

            if (productsGlobalReferrers.Any())
            {
               
                return productsGlobalReferrers.Aggregate((x, y) => y.Union(x)).Select(z => z.GetSourceItem()).ToArray();;
            }
            return null;
        }
    }
}