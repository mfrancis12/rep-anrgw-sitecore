﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Anritsu.WebApp.SitecoreUtilities.DataAccess
{
    public static class Calibration
    {
        public static DataTable GetCertificatePricesForPart(string modelNumber)
        {
            using (SqlConnection conn = CommonLinks.GetOpenedConnection(ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString))
            {
                SqlCommand cmd = CommonLinks.MakeSpCommand(conn, "uSP_Repair_Calibration_Certificate_Price_byPartNumber_Get");
                cmd.Parameters.AddWithValue("@PartNumber", modelNumber);
                DataTable tb = CommonLinks.FillInDataTable(cmd);
                return tb;
            }
        }
        public static DataTable GetProductNotesForPart(string modelNumber)
        {
            using (SqlConnection conn = CommonLinks.GetOpenedConnection(ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString))
            {
                SqlCommand cmd = CommonLinks.MakeSpCommand(conn, "uSP_Repair_Calibration_Product_Notes_byPartNumber_Get");
                cmd.Parameters.AddWithValue("@PartNumber", modelNumber);
                DataTable tb = CommonLinks.FillInDataTable(cmd);
                return tb;
            }
        }


    }
}
