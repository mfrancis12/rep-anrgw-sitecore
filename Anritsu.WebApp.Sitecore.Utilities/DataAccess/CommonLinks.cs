﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.SitecoreUtilities.DataAccess
{
    public static class CommonLinks
    {
        public static int GetCultureGroupIdForCultureCode(string cultureCode)
        {
            using (SqlConnection conn = GetOpenedConnection(ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString))
            {
                SqlCommand cmd =MakeSpCommand(conn, "uSP_AR_Resources_GetCultureGroupID");
                cmd.Parameters.AddWithValue("@CultureCode", cultureCode);
                int cultureGroupId;
                try
                {
                    cultureGroupId = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch
                {
                    cultureGroupId = 1;
                }                
                return cultureGroupId;
            }
        }

        public static string GetCurrentLanguage()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null &&
                HttpContext.Current.Session["Language"] != null)
                return HttpContext.Current.Session["Language"].ToString();
            else
            {
                string lang =Sitecore.Context.Language.Name;
                if (HttpContext.Current != null)
                    HttpContext.Current.Session["Language"] = lang;
                return lang;
            }
        }

        public static int GetCurrentCultureGroupId()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null &&
                HttpContext.Current.Session["CultureGroupId"] != null)
                return int.Parse(HttpContext.Current.Session["CultureGroupId"].ToString());
            else
            {
                string lang = GetCurrentLanguage();
                int groupId = GetCultureGroupIdForCultureCode(lang);
                if (HttpContext.Current != null)
                    HttpContext.Current.Session["CultureGroupId"] = groupId;
                return groupId;
            }
        }

        private static int GetSelectedCultureGroupID(string cultureCode)
        {
            if (!string.IsNullOrEmpty(cultureCode))
            {
                ListItem item = GetCultureGroups().FindByText(cultureCode);
                return !string.IsNullOrEmpty(item.Value) ? Convert.ToInt32(item.Value) : 0;
            }
            return 0;
        }

        private static ListItemCollection GetCultureGroups()
        {
            DataTable dtCultureGroup;
            using (SqlConnection conn = GetOpenedConnection(ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString))
            {
                SqlCommand cmd = MakeSpCommand(conn, "uSP_AR_AppResources_CultureGroupMaster_GetAll");
                dtCultureGroup = FillInDataTable(cmd);

                dtCultureGroup.PrimaryKey = new[] { dtCultureGroup.Columns[0] };
            }

            ListItemCollection cultureGroupList = new ListItemCollection();

            foreach (DataRow dr in dtCultureGroup.Rows)
            {
                ListItem item = new ListItem(dr["DefaultCultureCode"].ToString(), dr["CultureGroupID"].ToString());
                cultureGroupList.Add(item);
            }
            return cultureGroupList;
        }

        public static SqlConnection GetOpenedConnection(string connStr)
        {
            try
            {
                var conn = new SqlConnection(connStr);
                conn.Open();
                return conn;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public static SqlCommand MakeSpCommand(SqlConnection sqlConn, string sSpName)
        {
            var cmd = new SqlCommand(sSpName, sqlConn)
            {
                CommandText = sSpName,
                CommandType = CommandType.StoredProcedure
            };
            return cmd;
        }

        public static DataSet FillInDataSet(SqlCommand sqCmd)
        {
            var da = new SqlDataAdapter(sqCmd);
            var ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public static DataTable FillInDataTable(SqlCommand sqCmd)
        {
            DataSet ds = FillInDataSet(sqCmd);
            if (ds != null && ds.Tables.Count > 0)
                return ds.Tables[0];
            return null;

        }

        public static DataTable SelectLeftLinks(int cultureGroupId, string absolutePath, int siteId, DateTime today)
        {
            using (SqlConnection conn = GetOpenedConnection(ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString))
            {
                SqlCommand cmd = MakeSpCommand(conn, "uSP_SelectLinks");
                cmd.Parameters.AddWithValue("@CultureGroupID", cultureGroupId);
                cmd.Parameters.AddWithValue("@DisplayPageURL", absolutePath);
                cmd.Parameters.AddWithValue("@SiteId", siteId);
                cmd.Parameters.AddWithValue("@Date", today);
                DataTable tb = FillInDataTable(cmd);               
                return tb;
            }
        }
    }
}
