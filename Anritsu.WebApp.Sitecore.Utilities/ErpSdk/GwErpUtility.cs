﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Anritsu.WebApp.SitecoreUtilities.ErpWS;

namespace Anritsu.WebApp.SitecoreUtilities.ErpSdk
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Erp"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Gw"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Gw")]
    public static class GwErpUtility
    {
        private static List<WarrantyCoverage_ApiType> _warrantyCoverageCollection;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
        public static List<WarrantyCoverage_ApiType> WarrantyCoverageCollection
        {
            get
            {
                if (_warrantyCoverageCollection != null) return _warrantyCoverageCollection;
                var erpClient = new GwErpClient();
                var resp =
                    erpClient.WarrantyCoverage_GetAll();
                if (resp == null
                    || resp.CallStatus != CallStatus_ApiType.SUCCESS
                    || resp.WarrantyCoverageCollection == null)
                {
                    throw new ArgumentException("Unable to get warranty coverage collection from the server.");
                }
                _warrantyCoverageCollection = resp.WarrantyCoverageCollection.ToList();
                return _warrantyCoverageCollection;
            }
            set { _warrantyCoverageCollection = value; }
        }

        public static bool IsUnderWarrantyOrContract(CultureInfo userCultureInfo
            , WarrantyInfo_ApiType[] warrantyCollection)
        {
            var compareDate = DateTime.Now;
            if (userCultureInfo == null || userCultureInfo.Name.ToLower(CultureInfo.InvariantCulture) != "ja-jp")
                return IsUnderWarrantyOrContract(compareDate, warrantyCollection);
            var eom = DateTime.Parse(string.Format("1/{0}/{1}", DateTime.Now.Month, DateTime.Now.Year));
            compareDate = eom.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            return IsUnderWarrantyOrContract(compareDate, warrantyCollection);
        }

        public static bool IsUnderWarrantyOrContract(DateTime compareDate, WarrantyInfo_ApiType[] warrantyCollection)
        {
            if (warrantyCollection == null || warrantyCollection.Length < 1)
                return false;

            var underWarrantyOrContract = false;
            foreach (var warr in warrantyCollection)
            {
                DateTime warrExpDate;
                if (!DateTime.TryParse(warr.WarrantyExpDate, out warrExpDate)) continue;
                if (warrExpDate < compareDate) continue;
                underWarrantyOrContract = true;
                break;
            }
            return underWarrantyOrContract;
        }

        public static bool IsUnderCoverage(int warrantyTypeId, string svcPricingTypeCode)
        {
            if (string.IsNullOrEmpty(svcPricingTypeCode) || warrantyTypeId < 1)
                return false;

            var col = WarrantyCoverageCollection;
            var query = from wc in col
                where wc.WarrantyTypeId == warrantyTypeId
                      && wc.SvcPricingTypeCode.Trim().ToLower(CultureInfo.InvariantCulture) == svcPricingTypeCode.Trim().ToLower(CultureInfo.InvariantCulture)
                select wc;
            return query.Any();
        }
    }
}
