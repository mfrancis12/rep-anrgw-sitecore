﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Caching;
using Sitecore.Data;
using Sitecore.Pipelines;

namespace Anritsu.WebApp.SitecoreUtilities.Events
{
    public class ClearCache
    {
        private readonly ArrayList _databases = new ArrayList();

        public ArrayList Databases
        {
            get
            {
                return _databases;
            }
        }

        public void ClearItemCache(object sender, EventArgs args)
        {
            Sitecore.Diagnostics.Log.Info("Started cache clearing", GetType());
            foreach (string database in _databases)
            {
                Database db = Sitecore.Configuration.Factory.GetDatabase(database);
                CacheManager.GetItemCache(db).Clear();
                CacheManager.GetDataCache(db).Clear();
                Sitecore.Diagnostics.Log.Info(string.Format("Cache cleared for {0} database", database), GetType());
            }
        }
    }
}
