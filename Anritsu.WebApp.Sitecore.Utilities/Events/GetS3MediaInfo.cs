﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;
using Sitecore.Events;
using Sitecore.SecurityModel;
using Convert = System.Convert;

namespace Anritsu.WebApp.SitecoreUtilities.Events
{
    public class GetS3MediaInfo
    {
        public string BasePath { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        public void OnItemSaved(object sender, EventArgs args)
        {
            if (Context.Job != null) return;

            if (args == null) return;

            var currentItem = Event.ExtractParameter(args, 0) as Item;

            if (currentItem == null) return;

            if (currentItem.TemplateID.ToString().Equals("{0A13323A-10F2-404A-9AA2-7E0BD4D0EF78}") && !string.IsNullOrWhiteSpace(currentItem["FilePath"]))
            {
                var lstS3Objects = GetS3Objects(currentItem["FilePath"], "/");
                if ((lstS3Objects != null && lstS3Objects.Count>0) &&
                    ((currentItem["LogOnRequired"] != null && !currentItem["LogOnRequired"].Equals("1")) || lstS3Objects.Count == 1))
                {
                    using (new SecurityDisabler())
                    {
                        currentItem.Editing.BeginEdit();
                        var objs3Objects = lstS3Objects[0];
                        currentItem["Size"] = CalculateContentLength(objs3Objects.Size.ToString());
                        currentItem["FileType"] = objs3Objects.Key.Substring(objs3Objects.Key.LastIndexOf(".", StringComparison.Ordinal)+1);
                        currentItem.Editing.EndEdit();
                    }
                }
            }
        }

        public List<S3Object> GetS3Objects(String prefix, String delimiter)
        {
            List<S3Object> resources = new List<S3Object>();
            AmazonS3Client s3Client = GetS3Client();

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            request.Delimiter = delimiter;
            request.Prefix = prefix;
            request.MaxKeys = 10000;
            do
            {
                ListObjectsResponse response = s3Client.ListObjects(request);
                resources.AddRange(response.S3Objects.Where(res => res.Size > 0));
                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);
            return resources;
        }

        private AmazonS3Client GetS3Client()
        {
            AmazonS3Client s3Client;
            RegionEndpoint regionEndpoint = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["AWSEndPoint"]);
            s3Client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSAccessKey"], ConfigurationManager.AppSettings["AWSSecretKey"], regionEndpoint);
            return s3Client;
        }

        public class S3Objects
        {
            public List<string> CommonPrefixes { get; set; }
            public List<S3Object> Content { get; set; }
        }

        public class AnritsuUtilityS3
        {
            private String _AWSS3_AccessKey;
            private String _AWSS3_SecretKey;

            private String AWSS3_AccessKey
            {
                get
                {
                    return _AWSS3_AccessKey;
                }
            }

            private String AWSS3_SecretKey
            {
                get
                {
                    return _AWSS3_SecretKey;
                }
            }

            public AnritsuUtilityS3()
            {
                InitConfig();
            }

            private void InitConfig()
            {
                _AWSS3_AccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
                _AWSS3_SecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
            }
        }

        private string CalculateContentLength(string value)
        {
            if (!string.IsNullOrEmpty(value) && Convert.ToInt32(value) >= 0)
            {
                int size = Convert.ToInt32(value);
                string[] sizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

                if (size == 0)
                {
                    return "0.0 bytes";
                }

                var mag = (int)Math.Log(size, 1024);
                var adjustedSize = (decimal)size / (1L << (mag * 10));

                return string.Format("{0:n1} {1}", adjustedSize, sizeSuffixes[mag]);
            }
            return string.Empty;
        }
    }
}
