﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using Sitecore.Events;
using Sitecore.Web.UI.Sheer;
using System.Web.Configuration;
using System.Configuration;
using System.Collections.Specialized;

namespace Anritsu.WebApp.SitecoreUtilities.Events
{
    public class LimitNumberOfItems
    {
        public string ItemCount
        {
            get;
            set;

        }
        public string Item 
        {
            get;
            set;
        }
        public void OnItemCreated(object sender, EventArgs args)
        {

            //string itemcount=System.Configuration.ConfigurationManager.AppSettings["itemCount"];
            //int count = Convert.ToInt32(itemcount);
            //NameValueCollection section = (NameValueCollection)ConfigurationManager.GetSection("event");
            //int count = Convert.ToInt32(section["itemCount"]);
           
           // string userPassword = section["userPassword"];
            //Sitecore.Data.Database contextDB = Sitecore.Context.Database;
            //Item item = Sitecore.Context.Item;
            //if (item.Children.Count >= 2)

            //    // Delete the item, warn user
            //    SheerResponse.Alert(String.Format("Sorry, you cannot add more than 2 items"));
            //item.Children[2].Delete();

            var createdArgs = Event.ExtractParameter(args, 0) as ItemCreatedEventArgs;

            Sitecore.Diagnostics.Assert.IsNotNull(createdArgs, "args");
            if (createdArgs != null)
            {
                Sitecore.Diagnostics.Assert.IsNotNull(createdArgs.Item, "item");
                if (createdArgs.Item != null)
                {
                    var item = createdArgs.Item;
                    System.Configuration.Configuration rootWebConfig1 = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
                    // NOTE: you may want to do additional tests here to ensure that the item
                    // descends from /sitecore/content/home
                    if (item.Parent.ID.ToString().Equals(Item))
                      //  int num=Sitecore.Events.Event["itemCount1"];
                        if (item.Parent != null && item.Parent.Children.Count() > int.Parse(ItemCount))
                        {
                            // Delete the item, warn user
                            SheerResponse.Alert(String.Format("Sorry, you cannot add more than 2 items"));
                            item.Delete();
                        }
                }
            }
        }
    }
}
