﻿using System;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.Extensions
{
    public static class CreateCookie
    {
        public static void SetCustomCookie(this HttpResponse response, string cookieName, string cookieValue)
        {
            if (response != null)
            {
                HttpCookie cookie;
                // set default cookie

                if ((cookie = HttpContext.Current.Request.Cookies.Get(cookieName)) == null)
                {
                    cookie = new HttpCookie(cookieName);
                }

                cookie.Value = cookieValue;

                cookie.Expires = DateTime.MinValue;

                response.Cookies.Add(cookie);
            }
        }

        public static void SetCustomCookie(this HttpResponse response, string cookieName, string cookieValue, DateTime dateTime)
        {
            if (response != null)
            {
                HttpCookie cookie;
                // set default cookie 

                if ((cookie = HttpContext.Current.Request.Cookies.Get(cookieName)) == null)
                {
                    cookie = new HttpCookie(cookieName);
                }

                cookie.Value = cookieValue;

                cookie.Expires = dateTime;

                response.Cookies.Add(cookie);
            }
        }

    }
}
