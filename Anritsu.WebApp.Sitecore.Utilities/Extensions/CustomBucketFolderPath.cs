﻿using Sitecore;
using Sitecore.Buckets.Rules.Bucketing;
using Sitecore.Buckets.Util;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Rules;
using System;

namespace Anritsu.WebApp.SitecoreUtilities.Extensions
{
    public class CustomBucketFolderPath : IDynamicBucketFolderPath
    {
        public string GetFolderPath(Database database, string name, ID templateId, ID newItemId, ID parentItemId,
            DateTime creationDateOfNewItem)
        {
            Assert.ArgumentNotNull(database, "database");
            Assert.ArgumentNotNull(newItemId, "itemId");
            Assert.ArgumentNotNull(parentItemId, "parentItemId");

            Item parentItem = database.GetItem(parentItemId);

            if (parentItem != null && templateId.ToString().Equals(Sitecore.Configuration.Settings.GetSetting("BucketTemplateTest")))
            {
                string resolvedPath;
                if (name.Length == 1)
                {
                    resolvedPath = name;
                }
                else
                {
                    resolvedPath = name.Substring(0, 1) + "/" + name.Substring(0, 2);
                }
                return resolvedPath;
            }
            else
            {
                Assert.ArgumentNotNull(database, "database");
                BucketingRuleContext bucketingRuleContext = new BucketingRuleContext(database, parentItemId, newItemId, name, templateId, creationDateOfNewItem)
                {
                    NewItemId = newItemId,
                    CreationDate = creationDateOfNewItem
                };
                Item item = database.GetItem(Sitecore.Buckets.Util.Constants.SettingsItemId);
                Assert.IsNotNull(item, "Setting Item");
                RuleList<BucketingRuleContext> rules = RuleFactory.GetRules<BucketingRuleContext>(new Item[]
			{
				item
			}, Sitecore.Buckets.Util.Constants.BucketRulesFieldId);
                try
                {
                    if (rules != null)
                    {
                        rules.Run(bucketingRuleContext);
                    }
                }
                catch (Exception owner)
                {
                    Log.Error(string.Format("Bucket Folder Path Resolver: Cannot resolve bucket path for item {0}. Parent = {1}", newItemId, parentItemId), owner);
                }
                string text = bucketingRuleContext.ResolvedPath;
                if (string.IsNullOrEmpty(text))
                {
                    text = creationDateOfNewItem.ToString(BucketConfigurationSettings.BucketFolderPath, Context.Culture);
                }
                return text;
            }
            return null;
        }
    }
}
