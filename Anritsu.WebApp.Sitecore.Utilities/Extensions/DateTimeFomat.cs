﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Anritsu.WebApp.SitecoreUtilities.Extensions
{
    public static class DateTimeFomat
    {
        public static string ToFormattedDate(this DateTime unformattedDate)
        {
            CultureInfo culture = new CultureInfo(Sitecore.Context.Language.Name);
            try
            {
                Item item = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(Constants.ItemIDs.DateTimeSeparator), Sitecore.Context.Language);
                if (item == null || item.Fields["Value"] == null || string.IsNullOrEmpty(item.Fields["Value"].Value))
                    return unformattedDate.ToShortDateString();
                string formatter = string.Empty;
                if (culture != null && !string.IsNullOrEmpty(culture.DateTimeFormat.DateSeparator))
                    formatter = culture.DateTimeFormat.ShortDatePattern.Replace(culture.DateTimeFormat.DateSeparator, item.Fields["Value"].Value);

                return !string.IsNullOrEmpty(formatter) ? unformattedDate.ToString(formatter) :
                    unformattedDate.ToShortDateString();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message + " " + ex.StackTrace, "DateTimeFormat");
            }
            return unformattedDate.ToShortDateString();
        }
    }
}
