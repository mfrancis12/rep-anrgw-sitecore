﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.SitecoreUtilities.Extensions
{
    public class ItemEqualityComparer : IEqualityComparer<Item>
    {
        // Items are equal if their names and Item numbers are equal.
        public bool Equals(Item x, Item y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the Items' ID's are equal.
            return x.ID == y.ID;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(Item obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Get hash code for the ID 
            return obj.ID.GetHashCode();
        }
    }

}
