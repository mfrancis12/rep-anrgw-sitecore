﻿using System.Collections.Generic;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;

namespace Anritsu.WebApp.SitecoreUtilities.Extensions
{
    public static class ItemExtension
    {
        public static List<Item> GetLinkedItems(this Item refItem, Database database, Language language, string templateId)
        {
            // getting all linked Items that refer to the “refItem” Item
            ItemLink[] links = Globals.LinkDatabase.GetReferrers(refItem);
            if (links == null)
            {
                return null;
            }

            List<Item> result = new List<Item>();

            foreach (ItemLink link in links)
            {
                // checking the database name of the linked Item
                if (link.SourceDatabaseName == database.Name && link.SourceItemLanguage.Equals(language))
                {
                    Item item = database.Items[link.SourceItemID, language];
                    // adding the Item to an array if the Item is not null
                    if (item != null && item.TemplateID.ToString().Equals(templateId))
                    {
                        result.Add(item);
                    }
                }
            }

            return result;
        }
    }
}
