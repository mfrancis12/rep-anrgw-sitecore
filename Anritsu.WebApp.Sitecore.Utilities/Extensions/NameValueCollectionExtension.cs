﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.Extensions
{
    public static class NameValueCollectionExtension
    {
        public static string GetKeyByValue(this NameValueCollection collection, string value)
        {
            return collection.AllKeys.FirstOrDefault(key => collection.Get(key).Equals(value.ToUpperInvariant()));
        }

        public static string GetKeyByValue(this NameValueCollection collection, string value,bool urlDecode)
        {
            if (urlDecode)
            {
                return HttpUtility.UrlDecode(collection.GetKeyByValue(value));
            }
            return collection.GetKeyByValue(value);
        }
    }
}
