﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.Extensions
{
    public  static class StringExtensions
    {
        public static string EscapePath(string path)
        {
            return Regex.Replace(path, @"([^/]+)", "#$1#").Replace("#*#", "*");
        }

        //public static string ConvertNullToEmptyString(this string input)
        //{
        //    if (string.IsNullOrEmpty(input))
        //    {
        //        return string.Empty;
        //    }
        //    return input;
        //}

        public static bool IsNumeric(this string input)
        {
            return Regex.IsMatch(input, @"^\d+$");
        }

        public static string HtmlEncode(this string inputText)
        {
            return HttpUtility.HtmlEncode(inputText);
        }
        public static string HtmlDecode(this string inputText)
        {
            return HttpUtility.HtmlDecode(inputText);
        }
    }
}
