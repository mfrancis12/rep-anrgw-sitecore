﻿using System;
using System.Globalization;
using System.ServiceModel.Syndication;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.Syndication;


namespace Anritsu.WebApp.SitecoreUtilities.Extensions
{
    public class SyndicationItemExtension : PublicFeed
    {
        protected override SyndicationItem RenderItem(Item item)
        {
            SyndicationItem syndicationItem = base.RenderItem(item);
            Item currentItem = Sitecore.Context.Database.GetItem(syndicationItem.Id);
            if (currentItem != null && currentItem.Fields["Link"] != null)
            {
                SyndicationLink syndicationLink = GetSyndicationLink(currentItem);
                if (syndicationLink != null && syndicationLink.Uri != null)
                {
                    syndicationItem.Links.Clear();
                    syndicationItem.AddPermalink(syndicationLink.Uri);
                }
            }

            return syndicationItem;
        }

        private SyndicationLink GetSyndicationLink(Item item)
        {
            SyndicationLink syndicationLink = new SyndicationLink();
            var domain =
                HttpContext.Current.Request.Url.GetComponents(
                    UriComponents.Scheme | UriComponents.Host, UriFormat.Unescaped);
            try
            {
                Sitecore.Data.Fields.LinkField linkField = item.Fields["Link"];
                switch (linkField.LinkType.ToLower(CultureInfo.InvariantCulture))
                {
                    case "internal":
                        if (linkField.TargetItem != null)
                        {
                            var url = LinkManager.GetItemUrl(linkField.TargetItem);
                            syndicationLink.Uri =
                                new Uri(domain + Sitecore.StringUtil.EnsurePrefix('/', url));
                        }
                        break;

                    case "external":
                        syndicationLink.Uri = new Uri(linkField.Url);
                        break;

                    case "media":
                        if (linkField.TargetItem != null)
                            syndicationLink.Uri = new Uri(domain + MediaManager.GetMediaUrl(linkField.TargetItem));
                        break;

                    default:
                        syndicationLink.Uri = new Uri(linkField.Url);
                        break;
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception.StackTrace, this);
            }
            return syndicationLink;
        }
    }
}
