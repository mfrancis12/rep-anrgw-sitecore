﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.S3;
using Amazon.S3.Model;

namespace Anritsu.WebApp.SitecoreUtilities.Helpers.AmazonHelper
{
    public static class S3Utility
    {
        private static readonly Amazon.RegionEndpoint EndPoint = Amazon.RegionEndpoint.GetBySystemName(System.Configuration.ConfigurationManager.AppSettings["AWSEndPoint"]);
        public static void UploadFile(string bucketName, string fileName, string filePath)
        {

            using (IAmazonS3 s3Client = new AmazonS3Client(EndPoint))
            {
                // List to store upload part responses.
                List<UploadPartResponse> uploadResponses = new List<UploadPartResponse>();

                // 1. Initialize.
                InitiateMultipartUploadRequest initiateRequest = new InitiateMultipartUploadRequest
                {
                    BucketName = bucketName,
                    Key = fileName,
                    CannedACL = S3CannedACL.PublicRead
                };

                InitiateMultipartUploadResponse initResponse =
                    s3Client.InitiateMultipartUpload(initiateRequest);

                // 2. Upload Parts.
                long contentLength = new FileInfo(filePath).Length;
                long partSize = 5 * (long)Math.Pow(2, 20); // 5 MB

                try
                {
                    long filePosition = 0;
                    for (int i = 1; filePosition < contentLength; i++)
                    {
                        UploadPartRequest uploadRequest = new UploadPartRequest
                        {
                            BucketName = bucketName,
                            Key = fileName,
                            UploadId = initResponse.UploadId,
                            PartNumber = i,
                            PartSize = partSize,
                            FilePosition = filePosition,
                            FilePath = filePath
                        };

                        // Upload part and add response to our list.
                        uploadResponses.Add(s3Client.UploadPart(uploadRequest));

                        filePosition += partSize;
                    }

                    // Step 3: complete.
                    CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest
                    {
                        BucketName = bucketName,
                        Key = fileName,
                        UploadId = initResponse.UploadId,
                        //PartETags = new List<PartETag>(uploadResponses)

                    };
                    completeRequest.AddPartETags(uploadResponses);

                    s3Client.CompleteMultipartUpload(completeRequest);

                }
                catch (Exception exception)
                {
                    Console.WriteLine("Exception occurred: {0}", exception.Message);
                    AbortMultipartUploadRequest abortMpuRequest = new AbortMultipartUploadRequest
                    {
                        BucketName = bucketName,
                        Key = fileName,
                        UploadId = initResponse.UploadId
                    };
                    s3Client.AbortMultipartUpload(abortMpuRequest);
                }
            }
        }

        public static void UploadFile(string bucketName, string fileName, Stream fileStream)
        {
            if (fileStream != null)
            {
                using (IAmazonS3 s3Client = new AmazonS3Client(EndPoint))
                {
                    // List to store upload part responses.
                    List<UploadPartResponse> uploadResponses = new List<UploadPartResponse>();

                    // 1. Initialize.
                    InitiateMultipartUploadRequest initiateRequest = new InitiateMultipartUploadRequest
                    {
                        BucketName = bucketName,
                        Key = fileName,
                        CannedACL = S3CannedACL.PublicRead
                    };

                    InitiateMultipartUploadResponse initResponse =
                        s3Client.InitiateMultipartUpload(initiateRequest);

                    // 2. Upload Parts.
                    long contentLength = fileStream.Length;
                    long partSize = 5 * (long)Math.Pow(2, 20); // 5 MB

                    try
                    {
                        long filePosition = 0;
                        for (int i = 1; filePosition < contentLength; i++)
                        {
                            UploadPartRequest uploadRequest = new UploadPartRequest
                            {
                                BucketName = bucketName,
                                Key = fileName,
                                UploadId = initResponse.UploadId,
                                PartNumber = i,
                                PartSize = partSize,
                                FilePosition = filePosition,
                                InputStream = fileStream
                            };

                            // Upload part and add response to our list.
                            uploadResponses.Add(s3Client.UploadPart(uploadRequest));

                            filePosition += partSize;
                        }

                        // Step 3: complete.
                        CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest
                        {
                            BucketName = bucketName,
                            Key = fileName,
                            UploadId = initResponse.UploadId,
                            //PartETags = new List<PartETag>(uploadResponses)

                        };
                        completeRequest.AddPartETags(uploadResponses);

                        s3Client.CompleteMultipartUpload(completeRequest);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine("Exception occurred: {0}", exception.Message);
                        AbortMultipartUploadRequest abortMpuRequest = new AbortMultipartUploadRequest
                        {
                            BucketName = bucketName,
                            Key = fileName,
                            UploadId = initResponse.UploadId
                        };
                        s3Client.AbortMultipartUpload(abortMpuRequest);
                    }
                }
            }
        }

        public static void DeleteFile(string bucketName, string filePath)
        {
            try
            {
                using (IAmazonS3 s3Client = new AmazonS3Client(EndPoint))
                {
                    DeleteObjectRequest deleteRequest = new DeleteObjectRequest
                    {
                        BucketName = bucketName,
                        Key = filePath
                    };
                    s3Client.DeleteObject(deleteRequest);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception occurred: {0}", exception.Message);
            }

        }
    }
}
