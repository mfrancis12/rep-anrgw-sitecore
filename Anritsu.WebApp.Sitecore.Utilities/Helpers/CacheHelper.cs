﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace Anritsu.WebApp.SitecoreUtilities.Helpers
{
    public static class CacheHelper
    {
        public static void AddCache<T>(string cacheKey, T value) where T : class
        {
             AddCache<T>(cacheKey, value, 60);
        }
        public static void AddCache<T>(string cacheKey, T value, int minutes) where T : class
        {
            HttpContext.Current.Cache.Insert(cacheKey, value, null, DateTime.Now.AddMinutes(minutes),
                    Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, null);
        }

        public static T GetCache<T>(string cacheKey) where T : class
        {
            return HttpContext.Current.Cache[cacheKey] != null
                ? HttpContext.Current.Cache[cacheKey] as T
                : null;
        }

        public static string GetCache(string cacheKey)
        {
            return GetCache<string>(cacheKey);
        }
    }
}
