﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Anritsu.WebApp.SitecoreUtilities.EloquaWS;
using RestSharp;
using Sitecore.Diagnostics;
using Newtonsoft.Json;
//using Newtonsoft.Json;

namespace Anritsu.WebApp.SitecoreUtilities.Helpers.EloquaHelper
{

    #region Classes

    public class Item
    {
        public string DisplayName { get; set; }
        public string Value { get; set; }
    }

    public class OptionList
    {
        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<Item> Elements { get; set; }
    }

    public class Condition
    {
        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public string Type { get; set; }

        public int? Maximum { get; set; }
        public int? Minimum { get; set; }
    }

    public class Validation
    {
        public Condition Condition { get; set; }
        public string Message { get; set; }
    }

    public class Field
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public string Type { get; set; }
        public string Name { get; set; }
        public string DisplayType { get; set; }
        public string HtmlName { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<Validation> Validations { get; set; }
        public string OptionListId { get; set; }
    }

    public class Element
    {
        public string Name { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<Field> Fields { get; set; }
        public string DisplayType { get; set; }
        public string HtmlName { get; set; }
        public string OptionListId { get; set; }
        public string DefaultValue { get; set; }

        [SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "Checkbox"),
         SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Precheck"),
         SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public string X_E10_PrecheckCheckbox { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<Validation> Validations { get; set; }
    }

    public class Form
    {
        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public string Type { get; set; }

        public string Id { get; set; }
        public string Name { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<Element> Elements { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<ProcessingStep> ProcessingSteps { get; set; }
    }

    public class PageUrl
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming",
            "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public string Type { get; set; }

        public string ConstantValue { get; set; }
        public string ValueType { get; set; }
    }

    public class ProcessingStep
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming",
            "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public string Type { get; set; }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Execute { get; set; }
        public PageUrl PageUrl { get; set; }
    }

    public class StructuredHtmlContent
    {
        public string CssHeader { get; set; }
        public string HtmlBody { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly",
            MessageId = "java")]
        public string javaScriptHeader { get; set; }
    }

    public class StructuredLandingPage
    {
        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public string Type { get; set; }

        public StructuredHtmlContent HtmlContent { get; set; }
    }

    #endregion


    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Eloqua")]
    public class EloquaInstance : IDisposable
    {
        private EloquaServiceClient serviceProxy;

        private DateTime _dttLastEloquaApiCall;

        public EloquaInstance(string instanceName, string userId, string userPassword)
        {
            serviceProxy = new EloquaServiceClient();
            serviceProxy.ClientCredentials.UserName.UserName = instanceName + "\\" + userId;
            serviceProxy.ClientCredentials.UserName.Password = userPassword;

            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            _dttLastEloquaApiCall = DateTime.Now.ToUniversalTime().Subtract(TimeSpan.FromMilliseconds(1000));
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Eloqua")]
        public bool PostToEloqua(DynamicEntityFields fields)
        {
            var status = false;
            var entityType = new EntityType
            {
                ID = int.Parse(fields["FormID"]),
                Name = fields["formName"],
                Type = fields["formType"]
            };

            fields.Remove("formName");
            fields.Remove("formType");


            var dynamicEntities = new DynamicEntity[1];
            dynamicEntities[0] = new DynamicEntity
            {
                EntityType = entityType,
                FieldValueCollection = fields
            };
            var result = serviceProxy.Create(dynamicEntities);
            foreach (var t in result)
            {
                // Successfull requests return a positive integer value for ID
                if (t.ID != -1)
                {
                    status = true;
                    break;
                    //success
                }
                // Extract the Error Message and Error Code for each failed Create request
                foreach (var createError in t.Errors)
                {
                    Log.Error(string.Format("Code: {0}", createError.ErrorCode), "Eloqua");
                    Log.Error(string.Format("Message: {0}", createError.Message), "Eloqua");
                }
            }

            return status;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                serviceProxy.Close();
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Api"),
     SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Eloqua")]
    public class EloquaRestApi
    {
        private readonly RestClient _client;

        public EloquaRestApi(string url, string companyName, string userName, string password)
        {
            _client = new RestClient(url)
            {
                Authenticator = new HttpBasicAuthenticator(companyName + "\\" + userName, password)
            };
        }

        public Collection<Item> GetOptionListById(string optionListId)
        {
            var request = new RestRequest(Method.GET)
            {
                Resource = string.Format("/assets/optionList/{0}?depth=complete", optionListId)
            };
            var response = _client.Execute(request);
            var dataForm = JsonConvert.DeserializeObject<OptionList>(response.Content);
            return dataForm.Elements;
        }

        public Form GetFormFieldsById(int? formId)
        {
            var request = new RestRequest(Method.GET)
            {
                Resource = string.Format("/assets/form/{0}?depth=complete&extensions=e10", formId)
            };

            var response = _client.Execute(request);
            var dataForm = JsonConvert.DeserializeObject<Form>(response.Content);
            return dataForm;

            //var response = _client.Execute<Form>(request);
            //return response.Data;
        }

        public StructuredLandingPage GetStructuredLandingPage(int? id)
        {
            var request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = "/assets/landingPage/" + id
            };

            var response = _client.Execute<StructuredLandingPage>(request);
            return response.Data;
        }
    }

    public static class CryptUtility
    {
        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static string Decrypt(string cipherText, string encryptionKey)
        {
            var cipherBytes = Convert.FromBase64String(cipherText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(encryptionKey,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
}
