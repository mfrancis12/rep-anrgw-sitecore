﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = Anritsu.WebApp.SitecoreUtilities.WebForms.EmailHelper.Models;

namespace Anritsu.WebApp.SitecoreUtilities.Helpers
{
   public static class SearchFilterHelper
   {      
       public static List<string> GetFilters
       {
           get
           {
               List<string> searchFilters = new List<string>();
               searchFilters = CacheHelper.GetCache<List<string>>("searchfilters-" + Sitecore.Context.Language.Name);
               if (searchFilters != null) return searchFilters;
               Item searchItem = Sitecore.Context.Database.GetItem(Models.Constants.SearchFilterFolder);
               searchFilters = searchItem.GetChildren().Select(x => x.Fields["Key"].ToString()).ToList();
               CacheHelper.AddCache("searchfilters-" + Sitecore.Context.Language.Name, searchFilters);
               return searchFilters;
           }
       }

   }
}
