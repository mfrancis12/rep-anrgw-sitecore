﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Events.Hooks;
using Sitecore.Resources.Media;

namespace Anritsu.WebApp.SitecoreUtilities.Hooks
{
    public class MediaProvider : Sitecore.Resources.Media.MediaProvider, IHook
    {
        public void Initialize()
        {
            MediaManager.Provider = this;
        }

        public override string GetMediaUrl(MediaItem item, MediaUrlOptions options)
        {
            string mediaUrl = base.GetMediaUrl(item, options);
            return GetMediaUrl(mediaUrl, item);
        }

        /// <summary>
        /// Determines if we should be pulling from the CDN or not
        /// </summary>
        /// <param name="mediaUrl"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public string GetMediaUrl(string mediaUrl, MediaItem item)
        {
            //verify the domain was set in the config
            if (string.IsNullOrEmpty(OriginPrefix))
            {
                return mediaUrl;
            }

            //make sure this is one of the allowed sites defined in the config
            if (!AllowedSites.Contains(Sitecore.Context.GetSiteName()))
            {
                return mediaUrl;
            }

            //if the url is already expanded than ignore
            //(this could happen with emails)
            if (mediaUrl != null && mediaUrl.Contains("http"))
            {
                return mediaUrl;
            }
            
            //clean the url
            mediaUrl = mediaUrl.Replace("/~/media/", "/");

            //this happens while indexing unless the proper site is set
            mediaUrl = mediaUrl.Replace("/sitecore/shell/", "/");

            //reference the file in the cdn by the actual extension
            if (item != null) mediaUrl = mediaUrl.Replace(".ashx", "." + item.Extension);

            mediaUrl = string.Format("{0}{1}", OriginPrefix, mediaUrl.ToLowerInvariant());

            if (HttpContext.Current != null && HttpContext.Current.Request.IsSecureConnection)
            {
                //if we are on a secure connection, make sure we are making an https url over
                //to the cdn
                mediaUrl = mediaUrl.Replace("http://", "https://");
            }

            return mediaUrl;
        }

        /// <summary>
        /// Property defined in the config
        /// </summary>
        public string OriginPrefix { get; set; }

        /// <summary>
        /// Property defined in the config
        /// </summary>
        public string Sites { get; set; }

        /// <summary>
        /// Sites that are allows to use the CDN Media Provider
        /// </summary>
        public IEnumerable<string> AllowedSites
        {
            get
            {
                if (string.IsNullOrEmpty(Sites))
                {
                    return Enumerable.Empty<string>();
                }

                return Sites.Split('|').Where(x => !string.IsNullOrEmpty(x));
            }
        }
    }
}
