﻿using System.IO;
using Sitecore.Data.Items;
using Sitecore.Publishing.Pipelines.PublishItem;

namespace Anritsu.WebApp.SitecoreUtilities.Pipelines
{
    public class CDNProcessor
    {
        private readonly string bucketName = System.Configuration.ConfigurationManager.AppSettings["AWSBucketName"];

        /// <summary>
        ///  Process media files appropriately based on provider
        /// </summary>
        /// <param name="mediaItem"></param>
        /// <param name="publishContext"></param>
        /// <returns></returns>
        public bool Process(MediaItem mediaItem, PublishItemContext publishContext)
        {
            bool result = true;
            if (publishContext != null)
            {
                // determine what to do by publishing action
                if (publishContext.Action != Sitecore.Publishing.PublishAction.DeleteTargetItem)
                {
                    if (publishContext.Action == Sitecore.Publishing.PublishAction.None)
                        result = Add(mediaItem);

                    if ((publishContext.Action == Sitecore.Publishing.PublishAction.PublishSharedFields) || (publishContext.Action == Sitecore.Publishing.PublishAction.PublishVersion))
                        result = Replace(mediaItem);
                }
                else
                {
                    result = Delete(mediaItem);
                }
            }
            return result;
        }

        protected bool Add(MediaItem mediaItem)
        {
            if (mediaItem != null)
            {

                string filePath = GetFullFilePath(mediaItem);
                if (!string.IsNullOrEmpty(filePath) && Sitecore.IO.FileUtil.FileExists(filePath))
                {
                    Helpers.AmazonHelper.S3Utility.UploadFile(bucketName, GetCdnFilePath(mediaItem), filePath);
                    return true;
                }
                if (!mediaItem.HasMediaStream("blob")) return false;
                Helpers.AmazonHelper.S3Utility.UploadFile(bucketName, GetCdnFilePath(mediaItem), mediaItem.GetMediaStream());
                return true;
            }
            return false;
        }

        protected bool Replace(MediaItem mediaItem)
        {
            bool result;
            var fullFile = GetFullFilePath(mediaItem);

            // determine if media item is not in repository - Fix code
            if (!File.Exists(fullFile))
            {
                result = Add(mediaItem);
            }
            else
            {
                // update file to repository
                result = Delete(mediaItem);
                if (result)
                    result = Add(mediaItem);
            }

            return result;
        }

        protected bool Delete(MediaItem mediaItem)
        {
            string filePath = GetCdnFilePath(mediaItem);
            if (string.IsNullOrEmpty(filePath))
                return false;
            Helpers.AmazonHelper.S3Utility.DeleteFile(bucketName, filePath);
            return true;
        }

        private static string GetFullFilePath(MediaItem mediaItem)
        {
            if (mediaItem.InnerItem["file path"].Length > 0)
            {
                return Sitecore.IO.FileUtil.MapDataFilePath(mediaItem.InnerItem["file path"]);
            }
            return "";
        }

        private static string GetCdnFilePath(MediaItem mediaItem)
        {
            return string.Format("{0}.{1}", mediaItem.MediaPath.TrimStart('/'), mediaItem.Extension).ToLowerInvariant();
        }
    }
}
