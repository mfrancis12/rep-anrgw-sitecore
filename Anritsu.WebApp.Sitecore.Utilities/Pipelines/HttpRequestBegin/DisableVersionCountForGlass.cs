﻿using Glass.Mapper.Sc;
using Sitecore;
using Sitecore.Pipelines.HttpRequest;

namespace Anritsu.WebApp.SitecoreUtilities.Pipelines.HttpRequestBegin
{
    public class DisableVersionCountForGlass : HttpRequestProcessor
    {
        public override void Process(HttpRequestArgs args)
        {
            Context.Items["Disable"] = new VersionCountDisabler();
        }
    }
}
