﻿using System;
using System.Web;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Web;

namespace Anritsu.WebApp.SitecoreUtilities.Pipelines.HttpRequestBegin
{
    public class ExecuteRequest : Sitecore.Pipelines.HttpRequest.ExecuteRequest
    {
        protected override void RedirectOnItemNotFound(string url)
        {
            var context = HttpContext.Current;
            var customItemNotFoundUrl = Settings.GetSetting("ItemNotFoundUrl");
            try
            {
                // Request the NotFound page
                var domain = context.Request.Url.GetComponents(UriComponents.Scheme | UriComponents.Host, UriFormat.Unescaped);
                string content = string.Empty;
                var siteStartPath = Context.Site.StartPath;
                var pageNotFoundItemPath = siteStartPath + customItemNotFoundUrl;
                var pageNotFoundItem = Context.Database.GetItem(pageNotFoundItemPath);
                if (pageNotFoundItem != null)
                {
                    if (pageNotFoundItem.Versions.Count > 0)
                    {
                        content =
                            WebUtil.ExecuteWebPage(string.Format("{0}/{1}{2}?AppendStatus=FALSE", domain,
                                Context.Language.Name, customItemNotFoundUrl));
                    }
                    else
                    {
                        content = WebUtil.ExecuteWebPage(string.Format("{0}/en-us{1}?AppendStatus=FALSE", domain, customItemNotFoundUrl));
                    }
                }
                // Send the NotFound page content to the client with a 404 status code
                context.Response.TrySkipIisCustomErrors = true;
                context.Response.StatusCode = 404;
                context.Response.Write(content);
            }
            catch (Exception)
            {
                // If our plan fails for any reason, fall back to the base method
                base.RedirectOnItemNotFound(url);
            }

            // Must be outside the try/catch, cause Response.End() throws an exception
            context.ApplicationInstance.CompleteRequest();
        }
    }
}
