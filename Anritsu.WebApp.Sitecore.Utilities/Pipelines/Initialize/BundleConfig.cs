﻿

namespace Anritsu.WebApp.SitecoreUtilities.Pipelines.Initialize
{
    using System.Web.Optimization;
    using Sitecore;
    using Sitecore.Pipelines;

    public class BundleConfig
    {
        [UsedImplicitly]
        public virtual void Process(PipelineArgs args)
        {
            RegisterBundles(BundleTable.Bundles);
        }
        public void RegisterBundles(BundleCollection bundles)
        {
            System.Web.Optimization.BundleTable.Bundles.Add(new System.Web.Optimization.ScriptBundle("~/bundle/js")
          .Include("~/GlobalWeb/Resources/js/jquery.min.js",
          "~/GlobalWeb/Resources/js/jquery.transit.js",
          "~/GlobalWeb/Resources/js/select.js",
          "~/GlobalWeb/Resources/js/slick.min.js",
          "~/GlobalWeb/Resources/js/jquery.autocomplete.min.js",
          "~/GlobalWeb/Resources/js/jquery.pjax.js",
          "~/GlobalWeb/Resources/magnific-popup/jquery.magnific-popup.min.js",
          "~/GlobalWeb/Resources/js/app.js",
          "~/GlobalWeb/Resources/js/page.js",
          "~/GlobalWeb/Resources/js/fixes.js",
          "~/GlobalWeb/Resources/js/jquery.cookie.js",
          "~/GlobalWeb/Resources/js/form-validation.js"));

            System.Web.Optimization.BundleTable.Bundles.Add(new System.Web.Optimization.StyleBundle("~/bundle/css")
                 .Include("~/GlobalWeb/Resources/magnific-popup/magnific-popup.css",
                 "~/GlobalWeb/Resources/css/style.css",
                 "~/GlobalWeb/Resources/img/icon/icons.css",
                 "~/GlobalWeb/Resources/css/fixes.css"));
        }
    }

}
