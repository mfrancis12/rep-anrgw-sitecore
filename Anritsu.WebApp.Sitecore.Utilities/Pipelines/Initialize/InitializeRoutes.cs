﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Pipelines;
using System.Web.Routing;
using System.Web.Http;

namespace Anritsu.WebApp.SitecoreUtilities.Pipelines.Initialize
{
    public class InitializeRoutes
    {
        public void Process(PipelineArgs args)
        {
            // Register route for Widgets Web API service
            
             RouteTable.Routes.MapHttpRoute(
                "ResourceStringAPI",
                "api/{controller}/{action}",
                 new { action = "Get", controller = "ResourceString", id = RouteParameter.Optional }
            );
            
            RouteTable.Routes.MapHttpRoute(
                "DefaultAPI",
                "api/{controller}/{id}",
                 new { action = "Get", controller = "ResourceString", id = RouteParameter.Optional }
            );

           

            //RouteTable.Routes.MapHttpRoute("ResourceStringAPI",
            //"api/get",
            //new {action = "Get", controller = "ResourceString"});
        }
    }
}