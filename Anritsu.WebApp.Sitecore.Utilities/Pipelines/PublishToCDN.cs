﻿using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Publishing.Pipelines.PublishItem;
using System.Collections.Generic;
using System.Linq;

namespace Anritsu.WebApp.SitecoreUtilities.Pipelines
{
    public class PublishToCDN : PublishItemProcessor
    {
        public List<string> ExcludeNodes
        {
            get;
            set;
        }
        public PublishToCDN()
		{
            this.ExcludeNodes = new List<string>();
		}
        public override void Process(PublishItemContext context)
        {
            if (context != null)
            {
                // get Sitecore Item for publishing context
                // when Item is null(Deleted) get item from target item
                var contextItem = context.PublishHelper.GetSourceItem(context.ItemId) ??
                                  context.PublishHelper.GetTargetItem(context.ItemId);

                if (contextItem != null)
                {
                    List<Item> ascedants = contextItem.Axes.GetAncestors().ToList();
                    if (ascedants.Any(x => ExcludeNodes.Contains(x.ID.ToString())))
                        return;                    
                    //check if media item
                    if ((contextItem.Paths.IsMediaItem) && (contextItem.TemplateID.ToString() != TemplateIDs.MediaFolder.ToString()))
                    {
                        MediaItem mediaItem = contextItem;
                        CDNProcessor processor = new CDNProcessor();

                        //execute process
                        var cdnSuccess = processor.Process(mediaItem, context);
                        if (!cdnSuccess)
                        {
                            string errorMessage = string.Format("CDN Processing failed for {0}-{1}.", contextItem.ID, contextItem.Name);
                            Log.Error(errorMessage, this);

                            context.Job.Status.Failed = true;
                            context.Job.Status.Messages.Add(errorMessage);
                        }

                    }
                }

            }
        }

       
        public void AddFilterItems(string node)
        {
            Assert.ArgumentNotNullOrEmpty(node, "node");
            if (!this.ExcludeNodes.Contains(node))
            {
                this.ExcludeNodes.Add(node);
            }
        }
    }
}
