﻿using System;
using System.Collections.Specialized;
using System.Web;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Web;

namespace Anritsu.WebApp.SitecoreUtilities.Providers
{

    public class SwitchingLinkProvider : LinkProvider
    {
        private const String FallbackAttributeKey = "fallback";
        private const String LinkproviderAttributeKey = "linkProvider";
        private string Fallback { get; set; }

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            Fallback = config[FallbackAttributeKey];
            Assert.IsNotNullOrEmpty(Fallback, "fallback");
        }

        private LinkProvider SiteLinkProvider
        {
            get
            {
                var siteLinkProvider = (Context.Site != null)
                                        ? Context.Site.Properties[LinkproviderAttributeKey] : String.Empty;

                if (String.IsNullOrEmpty(siteLinkProvider))
                    siteLinkProvider = Fallback;

                return LinkManager.Providers[siteLinkProvider]
                    ?? LinkManager.Providers[Fallback];
            }
        }


        #region LinkProvider Options

        public override bool AddAspxExtension
        {
            get
            {
                return SiteLinkProvider.AddAspxExtension;
            }
        }

        public override bool AlwaysIncludeServerUrl
        {
            get
            {
                return SiteLinkProvider.AlwaysIncludeServerUrl;
            }
        }

        public override bool EncodeNames
        {
            get
            {
                return SiteLinkProvider.EncodeNames;
            }
        }

        public override LanguageEmbedding LanguageEmbedding
        {
            get
            {
                return SiteLinkProvider.LanguageEmbedding;
            }
        }

        public override LanguageLocation LanguageLocation
        {
            get
            {
                return SiteLinkProvider.LanguageLocation;
            }
        }

        public override bool LowercaseUrls
        {
            get
            {
                return SiteLinkProvider.LowercaseUrls;
            }
        }

        public override bool ShortenUrls
        {
            get
            {
                return SiteLinkProvider.ShortenUrls;
            }
        }

        public override bool UseDisplayName
        {
            get
            {
                return SiteLinkProvider.UseDisplayName;
            }
        }

        #endregion

        #region LinkProvider Methods

        public override string ExpandDynamicLinks(string text, bool resolveSites)
        {
            return SiteLinkProvider.ExpandDynamicLinks(text, resolveSites);
        }

        public override UrlOptions GetDefaultUrlOptions()
        {
            return SiteLinkProvider.GetDefaultUrlOptions();
        }

        public override string GetItemUrl(Item item, UrlOptions options)
        {
            return SiteLinkProvider.GetItemUrl(item, options);
        }

        public override string GetDynamicUrl(Item item, LinkUrlOptions options)
        {
            return SiteLinkProvider.GetDynamicUrl(item, options);
        }

        public override bool IsDynamicLink(string linkText)
        {
            return SiteLinkProvider.IsDynamicLink(linkText);
        }

        public override DynamicLink ParseDynamicLink(string linkText)
        {
            return SiteLinkProvider.ParseDynamicLink(linkText);
        }

        public override RequestUrl ParseRequestUrl(HttpRequest request)
        {
            return SiteLinkProvider.ParseRequestUrl(request);
        }

        #endregion

    }
}
