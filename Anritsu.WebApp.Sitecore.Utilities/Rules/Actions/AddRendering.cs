﻿using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Layouts;
using Sitecore.Rules.Actions;
using Sitecore.Rules.ConditionalRenderings;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.Rules.Actions
{
    public class AddRenderingAction<T> : RuleAction<T> where T : ConditionalRenderingsRuleContext
    {
        private string renderingItem;
        private string _placeholder;
        private string dataSource;
        private string sublayoutItem;
        public string RenderingItem
        {
            get
            {
                return this.renderingItem ?? string.Empty;
            }
            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.renderingItem = value;
            }
        }
        public string Placeholder
        {
            get
            {
                return this._placeholder ?? string.Empty;
            }
            set
            {
                Assert.ArgumentNotNull(value, "value");
                this._placeholder = value;
            }
        }
        public string DataSource
        {
            get
            {
                return this.dataSource ?? string.Empty;
            }
            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.dataSource = value;
            }
        }
        public string SublayoutItem
        {
            get
            {
                return this.sublayoutItem ?? string.Empty;
            }
            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.sublayoutItem = value;
            }
        }
        public override void Apply(T ruleContext)
        {
            Assert.ArgumentNotNull(ruleContext, "ruleContext");
            if (ruleContext.Reference.RenderingID.ToShortID().ToID().ToString().Equals(this.SublayoutItem))
            {
                RenderingReference renderingReference = new RenderingReference(Context.Database.GetItem(this.RenderingItem));
                renderingReference.Settings.Placeholder = this.Placeholder;
                renderingReference.Settings.DataSource = this.DataSource;
                ruleContext.References.Add(renderingReference);                
            }
        }
    }
}
