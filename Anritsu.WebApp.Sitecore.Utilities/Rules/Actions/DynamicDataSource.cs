﻿namespace Anritsu.WebApp.SitecoreUtilities.Rules.Actions
{
    using Sitecore;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Rules;
    using Sitecore.Rules.Actions;
    using Sitecore.Rules.ConditionalRenderings;

    // TODO: Created Sitecore Item "/sitecore/system/Settings/Rules/Common/Actions/DynamicDataSource" when creating DynamicDataSource class. Fix Title field.

    public class DynamicDataSource<T> : RuleAction<T> where T : ConditionalRenderingsRuleContext
    {
        /// </summary>
        private string dataSource;
        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>
        /// The data source.
        /// </value>
        public string DataSource
        {
            get
            {
                return this.dataSource ?? string.Empty;
            }
            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.dataSource = value;
            }
        }
        public ID ItemId1
        {
            get;
            set;
        }
        public ID ItemId2
        {
            get;
            set;
        }
        public ID ItemId3
        {
            get;
            set;
        }
        public ID ItemId4
        {
            get;
            set;
        }
        public ID SNSFooterID1
        {
            get;
            set;
        }
        public ID SNSFooterID2
        {
            get;
            set;
        }
        public ID SNSFooterID3
        {
            get;
            set;
        }
        public ID SNSFooterID4
        {
            get;
            set;
        }
        public ID SNSFooterID5
        {
            get;
            set;
        }
        public ID SNSFooterID6
        {
            get;
            set;
        }
        public ID SNSFooterID7
        {
            get;
            set;
        }
        public ID SNSFooterID8
        {
            get;
            set;
        }
        public override void Apply(T ruleContext)
        {

            Assert.ArgumentNotNull(ruleContext, "ruleContext");
           
            Item item1 = Sitecore.Context.Database.GetItem(ItemId1);
            Item item2 = Sitecore.Context.Database.GetItem(ItemId2);
            Item item3 = Sitecore.Context.Database.GetItem(ItemId3);
            Item item4 = Sitecore.Context.Database.GetItem(ItemId4);
            Item sns1 = null;
            Item sns2 = null;
            Item sns3 = null;
            Item sns4 = null;
            Item sns5 = null;
            Item sns6 = null;
            Item sns7 = null;
            Item sns8 = null;
            if (!Sitecore.Data.ID.IsNullOrEmpty(SNSFooterID1))
            {
                sns1 = Sitecore.Context.Database.GetItem(SNSFooterID1); 
            }
            if (!Sitecore.Data.ID.IsNullOrEmpty(SNSFooterID2))
            {
                sns2 = Sitecore.Context.Database.GetItem(SNSFooterID2);
            }
            if (!Sitecore.Data.ID.IsNullOrEmpty(SNSFooterID3))
            {
                sns3 = Sitecore.Context.Database.GetItem(SNSFooterID3);
            }
            if (!Sitecore.Data.ID.IsNullOrEmpty(SNSFooterID4))
            {
                sns4 = Sitecore.Context.Database.GetItem(SNSFooterID4);
            }
          
            if (!Sitecore.Data.ID.IsNullOrEmpty(SNSFooterID5))
            {
                sns5 = Sitecore.Context.Database.GetItem(SNSFooterID5);
            }
            if (!Sitecore.Data.ID.IsNullOrEmpty(SNSFooterID6))
            {
                sns6 = Sitecore.Context.Database.GetItem(SNSFooterID6);
            }
            if (!Sitecore.Data.ID.IsNullOrEmpty(SNSFooterID7))
            {
                sns7 = Sitecore.Context.Database.GetItem(SNSFooterID7);
            }
            if (!Sitecore.Data.ID.IsNullOrEmpty(SNSFooterID8))
            {
                sns8 = Sitecore.Context.Database.GetItem(SNSFooterID8);
            }

            Item item = ruleContext.Item;
            if (item != null)
            {
              
                    if (ruleContext.Reference.RenderingID.ToShortID().ToID().ToString().Equals("{166804C2-8069-4969-A3A6-37D26D8765A1}"))
                    {
                    string snspath = "";
                    
                    if(sns1 != null)
                    {
                        if(snspath=="")
                        snspath = sns1.Paths.FullPath;
                        else
                            snspath = snspath + "|" + sns1.Paths.FullPath;
                    }
                    
                    if (sns2 != null)
                    {
                        if (snspath == "")
                            snspath = sns2.Paths.FullPath;
                        else
                            snspath = snspath + "|" + sns2.Paths.FullPath;
                    }
                    if (sns3 != null)
                    {
                        if (snspath == "")
                            snspath = sns3.Paths.FullPath;
                        else
                            snspath = snspath + "|" + sns3.Paths.FullPath;
                    }
                    if (sns4 != null)
                    {
                        if (snspath == "")
                            snspath = sns4.Paths.FullPath;
                        else
                            snspath = snspath + "|" + sns4.Paths.FullPath;
                    }
                    if (sns5 != null)
                    {
                        if (snspath == "")
                            snspath = sns5.Paths.FullPath;
                        else
                            snspath = snspath + "|" + sns5.Paths.FullPath;
                    }
                    if (sns6 != null)
                    {
                        if (snspath == "")
                            snspath = sns6.Paths.FullPath;
                        else
                            snspath = snspath + "|" + sns6.Paths.FullPath;
                    }
                    if (sns7 != null)
                    {
                        if (snspath == "")
                            snspath = sns7.Paths.FullPath;
                        else
                            snspath = snspath + "|" + sns7.Paths.FullPath;
                    }
                    if (sns8 != null)
                    {
                        if (snspath == "")
                            snspath = sns8.Paths.FullPath;
                        else
                            snspath = snspath + "|" + sns8.Paths.FullPath;
                    }
                    if (snspath=="")
                        ruleContext.Reference.Settings.DataSource =  item1.Paths.FullPath + "|" + item2.Paths.FullPath + "|" + item3.Paths.FullPath + "|" + item4.Paths.FullPath;
                    else
                        ruleContext.Reference.Settings.DataSource = item1.Paths.FullPath + "|" + item2.Paths.FullPath + "|" + item3.Paths.FullPath + "|" + item4.Paths.FullPath + "_"+snspath ;
                }
               
                    //ruleContext.Abort();
            }
        }
    }


}