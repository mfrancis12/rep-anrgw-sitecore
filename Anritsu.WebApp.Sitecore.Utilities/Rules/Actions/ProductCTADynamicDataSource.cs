﻿using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Rules.Actions;
using Sitecore.Rules.ConditionalRenderings;
using Sitecore.Web.UI.WebControls;

namespace Anritsu.WebApp.SitecoreUtilities.Rules.Actions
{
    public class ProductCTADynamicDataSource<T> : RuleAction<T> where T : ConditionalRenderingsRuleContext
    {
        public ID ItemId1
        {
            get;
            set;
        }
        public ID ItemId2
        {
            get;
            set;
        }

        public override void Apply(T ruleContext)
        {
            Assert.ArgumentNotNull(ruleContext, "ruleContext");

            var item = ruleContext.Item;
            if (item == null) return;
            if (!ruleContext.Reference.RenderingID.ToString().Equals("{B0813CFF-C57D-4432-BF82-E08572C678B8}")) return;

            var productSublayout = ruleContext.Reference.GetControl() as Sublayout;
            if (productSublayout == null) return;

            productSublayout.Attributes["RequestQuoteCTA"] = ItemId1.ToString();
            productSublayout.Attributes["RequestDemoCTA"] = ItemId2.ToString();
        }
    }
}
