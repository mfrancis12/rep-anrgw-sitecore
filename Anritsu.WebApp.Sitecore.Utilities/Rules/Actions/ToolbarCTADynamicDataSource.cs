﻿using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Rules.Actions;
using Sitecore.Rules.ConditionalRenderings;
using Sitecore.Web.UI.WebControls;

namespace Anritsu.WebApp.SitecoreUtilities.Rules.Actions
{
    public class ToolbarCTADynamicDataSource<T> : RuleAction<T> where T : ConditionalRenderingsRuleContext
    {
        public ID ItemId1
        {
            get;
            set;
        }
        public ID ItemId2
        {
            get;
            set;
        }

        public ID ItemId3
        {
            get; 
            set;
        }

        public override void Apply(T ruleContext)
        {
            Assert.ArgumentNotNull(ruleContext, "ruleContext");

            var item = ruleContext.Item;
            if (item == null) return;
            if (!ruleContext.Reference.RenderingID.ToString().Equals("{1BB2F8B4-8F49-4C65-B75F-8C73DE4D154C}")) return;

            var toolbarSublayout = ruleContext.Reference.GetControl() as Sublayout;
            if (toolbarSublayout == null) return;

            toolbarSublayout.Attributes["RequestQuoteCTA"] = ItemId1.ToString();
            toolbarSublayout.Attributes["SupportCTA"] = ItemId2.ToString();
            toolbarSublayout.Attributes["BuyCTA"] = ItemId3.ToString();
        }
    }
}
