﻿namespace Anritsu.WebApp.SitecoreUtilities.Rules.Rules
{
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Rules;
    using Sitecore.Rules.Conditions;
   
    // TODO: Created Sitecore Item "/sitecore/system/Settings/Rules/Common/Conditions/GetAncestor" when creating GetAncestor class. Fix Text field.

    public class GetAncestor<T> : WhenCondition<T> where T : RuleContext
    {
        /// <summary>
		/// Gets or sets the item id.
		/// </summary>
		/// <value>The item id.</value>
		public ID ItemId
		{
			get;
			set;
		}
		/// <summary>Initializes a new instance of the <see cref="T:Sitecore.Rules.Conditions.PathConditions.AncestorOrSelfCondition`1" /> class. 
		/// Initializes a new instance of the WhenDescendsFrom class.</summary>
		public GetAncestor()
		{
			this.ItemId = ID.Null;
		}
		/// <summary>Executes the specified rule context.</summary>
		/// <param name="ruleContext">The rule context.</param>
		/// <returns><c>True</c>, if the condition succeeds, otherwise <c>false</c>.</returns>
		protected override bool Execute(T ruleContext)
		{
			Assert.ArgumentNotNull(ruleContext, "ruleContext");
			Item item = ruleContext.Item;
			if (item == null)
			{
				return false;
			}
			Item selectedItem = item.Database.GetItem(this.ItemId);
            if (selectedItem!=null)
		    {
		        return (selectedItem != null && selectedItem.Paths.IsAncestorOf(item) || selectedItem.ID.Equals(item.ID));
		    }
            return false;
		}
	}
}
