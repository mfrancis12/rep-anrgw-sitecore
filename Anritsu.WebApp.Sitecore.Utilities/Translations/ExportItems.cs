﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System.Collections.Generic;

namespace Anritsu.WebApp.SitecoreUtilities.Translations
{
    /// <summary>
    /// Invoke the class when export string command is clicked in sitecore.
    /// </summary>
    public static class ExportItems
    {
        private static Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

        public static Collection<string> GetLanguageCodes
        {
            get
            {
                var lstlaguageCodes = new Collection<string>();
                var languageRoot = master.GetItem("{64C4F646-A3FA-4205-B98E-4DE2C609B60F}");
                if (languageRoot == null) return lstlaguageCodes;
                foreach (string name in languageRoot.GetChildren().Select(x => x.Name).ToList())
                {
                    lstlaguageCodes.Add(name);
                }

                return lstlaguageCodes;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId = "System.Xml.XmlNode")]
        public static XmlDocument GetItemsAsXml(Collection<Item> items, Collection<string> languageCodes, string sourceLangCode)
        {
            XmlDocument doc = new XmlDocument();

            //// XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            //// doc.AppendChild(dec);// Create the root element

            //// XmlElement root = doc.CreateElement("item");
            XmlElement root = doc.CreateElement("sitecore");

            //// Sitecore.Collections.ChildList childList = Context.Database.GetItem(rootItemPath).Children;
            //// Sitecore.Collections.ChildList childList = master.GetItem(rootItemPath).Children;
            //// Sitecore.Collections.ChildList childList = rootItem.Children;
            foreach (Item itm in items)
            {
                root = BuildXmlNodesForChildren(itm, root, doc, languageCodes, sourceLangCode);
            }           
            doc.AppendChild(root);
            return doc;
        }

        /// <summary>
        /// Recursive method to iterate through all child items and build
        /// XML tree.
        /// </summary>
        /// <param name="parentItem">Parent item</param>
        /// <param name="root">Root node in xml</param>
        /// <param name="doc"></param>
        /// <param name="languageCodes"></param>
        /// <param name="sourceLangCode"></param>
        /// <returns>xml element</returns>
        private static XmlElement BuildXmlNodesForChildren(Item parentItem, XmlElement root, XmlDocument doc, Collection<string> languageCodes, string sourceLangCode)
        {
            XmlElement parentNode;
            parentItem.Fields.ReadAll();
            foreach (Field fld in parentItem.Fields)
            {
                //// Item sourceLangItem = Sitecore.Context.Database.GetItem(parentItem.ID, Sitecore.Data.Managers.LanguageManager.GetLanguage(sourceLangCode));
                Item sourceLangItem = master.GetItem(parentItem.ID, Sitecore.Data.Managers.LanguageManager.GetLanguage(sourceLangCode));
                sourceLangItem.Fields.ReadAll();


                if (!string.IsNullOrEmpty(sourceLangItem.Fields[fld.Name].Value) && !fld.Shared && (fld.Type == "memo" || fld.Type == "Single-Line Text" || fld.Type == "Multi-Line Text" || fld.Type == "Rich Text"))
                {
                    if (!fld.Name.Contains("__") || fld.Name.Equals("__Display name"))
                    {
                        parentNode = doc.CreateElement("phrase");
                        parentNode = SetItemAttributes(parentNode, parentItem, fld, doc, languageCodes, sourceLangCode);
                        root.AppendChild(parentNode);
                    }
                }
            }
            //// Iterate though children
            Sitecore.Collections.ChildList childList = parentItem.Children;
            foreach (Item child in childList)
            {
                parentNode = doc.CreateElement("phrase");
                BuildXmlNodesForChildren(child, parentNode, doc, languageCodes, sourceLangCode);
            }

            return root;
        }

        /// <summary>
        /// Set any item information you want as attributes of the xmlElement.
        /// It will set all fields as attributes:
        /// </summary>
        /// <param name="xmlElement">xml element</param>
        /// <param name="item">item</param>
        /// <param name="fld"></param>
        /// <param name="doc"></param>
        /// <param name="languageCodes"></param>
        /// <returns>xml element</returns>
        private static XmlElement SetItemAttributes(XmlElement xmlElement, Item item, Field fld, XmlDocument doc, Collection<string> languageCodes, string sourceLangCode)
        {
            if(fld.Type=="")
            //// foreach (Field fld in item.Fields)
            //// {
            xmlElement.SetAttribute("path", item.Paths.FullPath);
            xmlElement.SetAttribute("key", item.Name);
            xmlElement.SetAttribute("itemid", item.ID.Guid.ToString());
            xmlElement.SetAttribute("fieldid", fld.Name);
            xmlElement.SetAttribute("updated", item.Fields["__UpDated"].ToString());
            XmlElement fldValue;
            foreach (string languageCode in languageCodes)
            {
                Sitecore.Globalization.Language language = Sitecore.Data.Managers.LanguageManager.GetLanguage(languageCode);
                Item laguageItem = master.GetItem(item.ID, language);

                //// if (string.IsNullOrEmpty(laguageItem.Fields[fld.Name].Value) || languageCode.Equals(sourceLangCode))
                //// {
                fldValue = GetFieldValues(laguageItem.Fields[fld.Name].Value, doc, languageCode, sourceLangCode);
                xmlElement.AppendChild(fldValue);
                //// }
            }

            //// }

            return xmlElement;
        }

        private static XmlElement GetFieldValues(string fldValue, XmlDocument doc, string languageCode, string sourceLangCode)
        {
            XmlElement laguageTag;
            laguageTag = doc.CreateElement(languageCode);
            if (sourceLangCode == languageCode)
            {
                laguageTag.InnerText = fldValue;
            }
            else
            {
                laguageTag.InnerText = "";
            }
            return laguageTag;
        }
    }
}
