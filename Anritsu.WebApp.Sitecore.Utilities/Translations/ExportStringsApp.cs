﻿using Sitecore.Data.Items;
using Sitecore.Shell.Framework.Commands;

namespace Anritsu.WebApp.SitecoreUtilities.Translations
{
    public class ExportStringsApp : Command
    {
        public override void Execute(CommandContext context)
        {
            if (context!=null && context.Items.Length == 1)
            {
                Item item = context.Items[0];
                var parameters = new System.Collections.Specialized.NameValueCollection();
                parameters["id"] = item.ID.ToString();
                Sitecore.Context.ClientPage.Start(this, "Run", parameters);
            }
        }

        protected static void Run(Sitecore.Web.UI.Sheer.ClientPipelineArgs args)
        {
            if (args != null)
            {
                var url = new Sitecore.Text.UrlString("/GlobalWeb/CustomApps/Translations/SelectLanguages.aspx");
                url.Append("id", args.Parameters["id"]);
                Sitecore.Context.ClientPage.ClientResponse.ShowModalDialog(url.ToString());
            }
        }
    }
}
