﻿using Sitecore.Data.Items;
using Sitecore.Data.Managers;

namespace Anritsu.WebApp.SitecoreUtilities.Translations
{
    public static class ItemDetails
    {
        /// <summary>
        /// Get language code from the selected region.
        /// </summary>
        /// <returns></returns>
        public static string GetLanguageCode
        {
            get
            {
                var contextLanguageId = LanguageManager.GetLanguageItemId(Sitecore.Context.Language, Sitecore.Context.Database);
                Item contextLanguage = Sitecore.Context.Database.GetItem(contextLanguageId);
                string iso = contextLanguage["Regional Iso Code"];
                if (string.IsNullOrEmpty(iso))
                {
                    iso = contextLanguage["Iso"];
                }
                else
                {
                    iso = iso.Substring(0, 2);
                }
                return iso;
            }

        }
    }
}
