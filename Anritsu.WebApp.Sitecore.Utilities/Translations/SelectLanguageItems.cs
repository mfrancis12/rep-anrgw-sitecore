﻿using System.Collections.ObjectModel;
using Sitecore.Data.Items;

namespace Anritsu.WebApp.SitecoreUtilities.Translations
{
    public class SelectLanguageItems
    {
        public string FileName { get; set; }
        public string SourceLanguage { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<string> TargetLanguages { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<Item> SelectedItems { get; set; }
    }    
}
