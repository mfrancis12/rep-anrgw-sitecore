﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Anritsu.WebApp.SitecoreUtilities.Utilities
{
    public static class SecurityUtilities
    {
        /// <summary>
        /// Use this class to encrypt strings before you save in client side such as cookie values.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Encrypter")]
        public class ClientCacheEncrypter
        {
            private const string Enckey = "4038C253-85C4-436A-8775-C413DDBBE05C";//don't change this
            public static string EncryptString(string input)
            {
                var enc = new AnritsuEncrypter(Enckey);
                return enc.EncryptData(input);
            }
            public static string DecryptString(string encryptedValue)
            {
                var enc = new AnritsuEncrypter(Enckey);
                return enc.DecryptData(encryptedValue);
            }
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Encrypter")]
        public class AnritsuEncrypter
        {
            private const int MinKeyLength = 9;
            static private readonly byte[] Key = new Byte[8];
            static private readonly byte[] Iv = new Byte[8];
            static private string _keystring = string.Empty;//enter key //"E2BE0601-0AFE-4306-8FE3-FDC60CB3FD6C";

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
            public AnritsuEncrypter(string sKey)
            {
                _keystring = sKey;
            }
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "int")]
            public void SetEncryptionKeyByIntId(int intId)
            {
                string segment = "_" + intId;
                string key = segment;
                while (key.Length < MinKeyLength)
                {
                    key += segment;
                }
                _keystring = key;
            }
            /// <summary>
            /// Function to encrypt data
            /// string Length cannot exceed 92160 bytes.
            /// </summary>
            /// <param name="sStringToEncrypt"></param>
            /// <returns></returns>
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
            public string EncryptData(string sStringToEncrypt)
            {
                string mResultString;//Return Result
                //1. string Length cannot exceed 92160 bytes.
                //Otherwise, buffer will overflow.
                //See point 3 for reasons
                if (sStringToEncrypt.Length > 92160)
                {
                    mResultString = "Error. Data string too large. Keep within 90Kb.";
                    return mResultString;
                }
                //2. Generate the Keys
                if (!InitKey())
                {
                    mResultString = "Error. Fail to generate key for encryption";
                    return mResultString;
                }
                //3. Prepare the string
                //The first 5 character of the string is formatted to store
                //the actual length of the data.
                //This is the simplest way to remember to original length of the data,
                //without resorting to complicated computations.
                sStringToEncrypt = string.Format("{0,5:00000}" + sStringToEncrypt,
                         sStringToEncrypt.Length);
                //4. Encrypt the Data
                var mDataBytes = new byte[sStringToEncrypt.Length];
                var mAsciiEnc = new ASCIIEncoding();
                //UTF8Encoding aEnc = new UTF8Encoding();
                mAsciiEnc.GetBytes(sStringToEncrypt, 0, sStringToEncrypt.Length, mDataBytes, 0);
                var mDesProvider = new DESCryptoServiceProvider();
                ICryptoTransform mDesEncrypt = mDesProvider.CreateEncryptor(Key, Iv);
                //5. Perpare the streams
                var mInputStream = new MemoryStream(mDataBytes);
                var mTransformStream = new CryptoStream(mInputStream,
                    mDesEncrypt, CryptoStreamMode.Read);
                var mOutputStream = new MemoryStream();
                //6. Start performing the encryption
                int mBytesRead;
                var mOutputBytes = new byte[1024];
                do
                {
                    mBytesRead = mTransformStream.Read(mOutputBytes, 0, 1024);
                    if (mBytesRead != 0)
                        mOutputStream.Write(mOutputBytes, 0, mBytesRead);
                }
                while (mBytesRead > 0);
                //7. Returns the encrypted result after it is base64 encoded
                //In this case, the actual result is converted to base64 so that
                //it can be transported over the HTTP protocol without deformation.
                if (mOutputStream.Length == 0)
                    mResultString = "";
                else
                    mResultString = Convert.ToBase64String(mOutputStream.GetBuffer(),
                        0, (int)mOutputStream.Length);
                return mResultString;
            }
            /// <summary>
            /// Function to decrypt data
            /// </summary>
            /// <param name="stringToEncrypt"></param>
            /// <returns></returns>
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string")]
            public string DecryptData(string stringToEncrypt)
            {
                string mResultString;
                //1. Generate the Key used for decrypting
                if (!InitKey())
                {
                    mResultString = "Error. Fail to generate key for decryption.";
                    return mResultString;
                }
                //2. Initialize the service provider
                var mDesProvider = new DESCryptoServiceProvider();
                ICryptoTransform mDesDecrypt = mDesProvider.CreateDecryptor(Key, Iv);
                //3. Prepare the streams
                var mOutputStream = new MemoryStream();
                var mTransformStream = new CryptoStream(mOutputStream, mDesDecrypt,
                    CryptoStreamMode.Write);
                //4. Remember to revert the base64 encoding into a byte array
                //to restore the original encrypted data stream
                byte[] mPlainBytes;
                try
                {
                    mPlainBytes = Convert.FromBase64CharArray(stringToEncrypt.ToCharArray(),
                        0, stringToEncrypt.Length);
                }
                catch (Exception)
                {
                    mResultString = "Error. Input Data is not base64 encoded.";
                    return mResultString;
                }
                long mRead = 0;
                long mTotal = stringToEncrypt.Length;
                try
                {
                    //5. Perform the actual decryption
                    while (mTotal >= mRead)
                    {
                        mTransformStream.Write(mPlainBytes, 0, mPlainBytes.Length);
                        //descsp.BlockSize=64
                        mRead = mOutputStream.Length + Convert.ToUInt32(
                            ((mPlainBytes.Length / mDesProvider.BlockSize) *
                            mDesProvider.BlockSize));
                    }
                    var mAsciiEnc = new ASCIIEncoding();
                    mResultString = mAsciiEnc.GetString(mOutputStream.GetBuffer(),
                        0, (int)mOutputStream.Length);
                    //6. Trim the string to return only the meaningful data
                    //Remember that in the encrypt function, the first 5 character
                    //holds the length of the actual data. This is the simplest way
                    //to remember to original length of the data, without resorting
                    //to complicated computations.
                    string mStringLength = mResultString.Substring(0, 5);
                    int mLen = Convert.ToInt32(mStringLength);
                    mResultString = mResultString.Substring(5, mLen);
                    //strResult = strResult.Remove(0,5);
                    return mResultString;
                }
                catch
                {
                    mResultString = "Error. Decryption Failed. Possibly due to incorrect Key"
                        + " or corrputed data";
                    return mResultString;
                }
            }
            /// <summary>
            /// Private function to generate the keys into member variables
            /// </summary>
            /// <returns></returns>
            static private bool InitKey()
            {
                try
                {
                    // Convert Key to byte array
                    var mKeyBytes = new byte[_keystring.Length];
                    var mAsciiEnc = new ASCIIEncoding();
                    mAsciiEnc.GetBytes(_keystring, 0, _keystring.Length, mKeyBytes, 0);
                    //Hash the key using SHA1
                    var mShaProvider = new SHA1CryptoServiceProvider();
                    byte[] mHashBytes = mShaProvider.ComputeHash(mKeyBytes);
                    int i;
                    // use the low 64-bits for the key value
                    for (i = 0; i < 8; i++)
                        Key[i] = mHashBytes[i];
                    for (i = 8; i < 16; i++)
                        Iv[i - 8] = mHashBytes[i];
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}
