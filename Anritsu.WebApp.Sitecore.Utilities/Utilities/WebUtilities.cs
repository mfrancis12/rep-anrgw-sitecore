﻿using System;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.Utilities
{
    public static class WebUtilities
    {
        public static T ReadRequest<T>(string requestName, bool isQueryStringOnly) where T : IConvertible
        {
            var reqValue = string.Empty;
            var context = HttpContext.Current;

            if (isQueryStringOnly)
            {
                if (!string.IsNullOrEmpty(context.Request.QueryString[requestName]))
                    reqValue = context.Request.QueryString[requestName];
            }
            else
            {
                if (!string.IsNullOrEmpty(context.Request[requestName]))
                    reqValue = context.Request[requestName];
            }

            if (reqValue.Trim() != string.Empty)
                return (T)Convert.ChangeType(reqValue.Trim(), typeof(T));
            return default(T);
        }

        public static string ReadRequest(string requestName, bool isQueryStringOnly)
        {
            return ReadRequest<string>(requestName, isQueryStringOnly);
        }
    }
}
