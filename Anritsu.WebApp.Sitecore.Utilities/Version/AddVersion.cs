﻿using Sitecore.Shell.Framework.Commands;

namespace Anritsu.WebApp.SitecoreUtilities.Version
{
    public class AddVersion : Command
    {
        public override void Execute(CommandContext context)
        {
            if (context != null && context.Items.Length == 1)
            {

                Sitecore.Data.Items.Item item = context.Items[0];

                var parameters = new System.Collections.Specialized.NameValueCollection();

                parameters["id"] = item.ID.ToString();

                Sitecore.Context.ClientPage.Start(this, "Run", parameters);

            }
        }

        protected static void Run(Sitecore.Web.UI.Sheer.ClientPipelineArgs args)
        {
            if (args != null)
            {
                var url = new Sitecore.Text.UrlString("/GlobalWeb/CustomApps/Version/ApplyVersion.aspx");
                url.Append("id", args.Parameters["id"]);
                Sitecore.Context.ClientPage.ClientResponse.ShowModalDialog(url.ToString());
            }
        }
    }
}
