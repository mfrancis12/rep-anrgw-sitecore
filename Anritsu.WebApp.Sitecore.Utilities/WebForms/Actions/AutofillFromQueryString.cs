﻿using Anritsu.Library.SSOSDK.SSOApiClient;
using Sitecore.Diagnostics;
using Sitecore.Form.Core.Utility;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Forms.Core.Rules;
using Sitecore.Rules.Actions;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.Actions
{
    public class AutoFillFromQueryString<T> : RuleAction<T> where T : ConditionalRuleContext
    {
        /// <summary>
        /// Gets or sets the QueryString Key.
        /// </summary>
        /// <value>QueryString Key.</value>
        public string QueryStringKey { get; set; }

        public override void Apply(T ruleContext)
        {
            Assert.ArgumentNotNull(ruleContext, "ruleContext");
            if (!string.IsNullOrEmpty(this.QueryStringKey))
            {
                object value = this.GetValue;
                if (value != null)
                {
                    if (ruleContext.Control != null)
                    {
                        this.SetValue(ruleContext.Control, value);
                        return;
                    }
                    if (ruleContext.Model != null)
                    {
                        ReflectionUtils.SetProperty(ruleContext.Model, "Value", value, false);
                    }
                }
            }
        }

        public object GetValue
        {
            get
            {
                if (HttpContext.Current.Request.QueryString[QueryStringKey] != null)
                {
                    return HttpContext.Current.Request.QueryString[QueryStringKey];
                }
                
                return HttpContext.Current.Request.UrlReferrer;
            }
        }

        protected void SetValue(System.Web.UI.Control control, object value)
        {
            this.SetValue(control, value, "DefaultValue");
        }

        protected void SetValue(System.Web.UI.Control control, object value, string property)
        {
            if (control is IResult)
            {
                ((IResult)control).DefaultValue = value.ToString();
                return;
            }
            ReflectionUtils.SetProperty(control, property, value, false);
        }
    }
}
