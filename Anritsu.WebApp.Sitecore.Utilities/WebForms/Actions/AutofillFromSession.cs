﻿using Anritsu.Library.SSOSDK.SSOApiClient;
using Sitecore.Diagnostics;
using Sitecore.Form.Core.Utility;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Forms.Core.Rules;
using Sitecore.Rules.Actions;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.Actions
{
    public class AutoFillFromSession<T> : RuleAction<T> where T : ConditionalRuleContext
    {
        /// <summary>
        /// Gets or sets the Session Key.
        /// </summary>
        /// <value>Session Key.</value>
        public string SessionKey { get; set; }

        public override void Apply(T ruleContext)
        {
            Assert.ArgumentNotNull(ruleContext, "ruleContext");
            if (!string.IsNullOrEmpty(this.SessionKey))
            {
                object value = this.GetValue;
                if (value != null)
                {
                    if (ruleContext.Control != null)
                    {
                        this.SetValue(ruleContext.Control, value);
                        return;
                    }
                    if (ruleContext.Model != null)
                    {
                        ReflectionUtils.SetProperty(ruleContext.Model, "Value", value, false);
                    }
                }
            }
        }

        public object GetValue
        {
            get
            {
                if (HttpContext.Current.Session["User"] != null)
                {
                    GWSsoUserType ssoWsUser = (GWSsoUserType)HttpContext.Current.Session["User"];
                    return ssoWsUser.GetType().GetProperties().Single(x => x.Name.ToUpperInvariant() == SessionKey.ToUpperInvariant()).GetValue(ssoWsUser, null);
                }
                return string.Empty;
            }
        }

        protected void SetValue(System.Web.UI.Control control, object value)
        {
            this.SetValue(control, value, "DefaultValue");
        }

        protected void SetValue(System.Web.UI.Control control, object value, string property)
        {
            if (control is IResult)
            {
                if (control is DropList)
                {
                    var dropList = (DropList) control;
                    dropList.SelectedValue = dropList.Items.FindByText(value.ToString()).Value;
                    return;
                }
                ((IResult)control).DefaultValue = value.ToString();
                return;
            }
            ReflectionUtils.SetProperty(control, property, value, false);
        }
    }
}

