﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using Anritsu.WebApp.SitecoreUtilities.WebForms.EmailHelper.Models;
using Anritsu.Library.SSOSDK.SSOApiClient;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomFields
{
    public class CountryDropList : Sitecore.Form.Web.UI.Controls.DropList
    {
        protected override void InitItems(Sitecore.Form.Web.UI.Controls.ListItemCollection items)
        {
            this.KeepHiddenValue = false;

            items = new Sitecore.Form.Web.UI.Controls.ListItemCollection();
            var dictionaryCountries = Countries;
            var culture = Culture;
            foreach (var country in dictionaryCountries)
            {
                items.Add(new ListItem() { Value = country.Region, Text = country.CountryName, Selected = IsSelected(country.Region, country.CountryName, culture) });
            }
            base.InitItems(items);
        }

        private bool IsSelected(string region, string countryName, string culture)
        {
            var res = false;
            GWSsoUserType ssoWsUser = (GWSsoUserType)HttpContext.Current.Session["User"];
            if (ssoWsUser != null)
            {
                var selectedCountry = ssoWsUser.GetType().GetProperties().Single(x => x.Name.Equals("CountryName", StringComparison.OrdinalIgnoreCase)).GetValue(ssoWsUser, null);
                if (selectedCountry != null)
                {
                    res = selectedCountry.ToString().Equals(countryName, StringComparison.OrdinalIgnoreCase) || selectedCountry.ToString().Equals(region, StringComparison.OrdinalIgnoreCase);
                }
                else if (!string.IsNullOrEmpty(culture))
                {
                    res = string.Equals(region, Culture.Split('-')[1], StringComparison.OrdinalIgnoreCase);
                }
            }
            else if (!string.IsNullOrEmpty(culture))
            {
                res = string.Equals(region, Culture.Split('-')[1], StringComparison.OrdinalIgnoreCase);
            }
            
            return res;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        private IEnumerable<Country> Countries
        {
            get
            {
                var countries = new List<Country>();
                var db = Sitecore.Configuration.Factory.GetDatabase("web");
                try
                {
                    var currentCulture = Culture;

                    var countriesFolder = db.GetItem("{CBE79E28-2912-4127-BE67-D469825EA027}", Sitecore.Globalization.Language.Parse(currentCulture));
                    if (countriesFolder != null)
                    {
                        countriesFolder.GetChildren().ToList().ForEach(delegate(Item p)
                        {
                            if (p.Fields["CountryName"] != null && p.Fields["CountryCode"] != null)
                            {
                                var country = new Country
                                {
                                    CountryName = Convert.ToString(p.Fields["CountryName"]),
                                    Region = Convert.ToString(p.Fields["CountryCode"]),
                                    ContextRegion = (Convert.ToString(p.Fields["CountryCode"]).ToUpperInvariant() == currentCulture.ToUpperInvariant()) ? System.Convert.ToString(p.Fields["CountryCode"]) : "",
                                };
                                countries.Add(country);
                            }
                        });
                    }
                }
                catch (Exception exception)
                {
                    Sitecore.Diagnostics.Log.Error(exception.Message, this);
                }
                return countries;
            }
        }

        private string Culture
        {
            get
            {
                string culture = string.Empty;
                try
                {
                    if (Sitecore.Context.PageMode.IsNormal)
                    {
                        var url = HttpContext.Current.Request.Url.AbsolutePath;
                        {
                            var regexMatch = Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

                            if (!string.IsNullOrEmpty(regexMatch.Value))
                            {
                                culture = regexMatch.Value.Replace("/", string.Empty);
                            }
                        }
                    }
                    else culture = Sitecore.Context.Culture.Name;

                }
                catch (Exception exception) { Sitecore.Diagnostics.Log.Error(exception.Message, this); culture = Sitecore.Context.Culture.Name; }
                return culture;
            }
        }

    }
}
