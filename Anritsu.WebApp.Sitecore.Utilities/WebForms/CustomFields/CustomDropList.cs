﻿using Anritsu.Library.SSOSDK.SSOApiClient;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomFields
{
    public class CustomDropList : Sitecore.Form.Web.UI.Controls.DropList
    {
        [VisualCategory("DataSource")]
        [VisualProperty("DataSource:", 10)]
        public string DataSource { get; set; }
        [VisualCategory("DataSource"), VisualProperty("Culture to Use:", 15)]
        public string CultureToUse { get; set; }

        protected override void InitItems(Sitecore.Form.Web.UI.Controls.ListItemCollection items)
        {
            this.KeepHiddenValue = false;

            items = new Sitecore.Form.Web.UI.Controls.ListItemCollection();

            var dictionaryItem = SourceItems;

            var list = dictionaryItem.ToList();
            var option = Translate.TextByLanguage("Select", Language);

            items.Add(new ListItem() { Value = option, Text = "Select", Selected = IsSelected("Select", option) });

            foreach (var item in list.Where(item => !string.IsNullOrEmpty(item.Key) && !string.IsNullOrEmpty(item.Value)).OrderBy(item => item.Key))
            {
                items.Add(new ListItem() { Value = item.Value, Text = item.Key, Selected = IsSelected(item.Key, item.Value) });
            }
            base.InitItems(items);
        }

        private bool IsSelected(string stateCode, string stateName)
        {
            var res = false;
            GWSsoUserType ssoWsUser = (GWSsoUserType)HttpContext.Current.Session["User"];
            if (ssoWsUser != null)
            {
                var selectedState = ssoWsUser.GetType().GetProperties().Single(x => x.Name.Equals("State", StringComparison.OrdinalIgnoreCase)).GetValue(ssoWsUser, null);
                if (selectedState != null)
                {
                    res = selectedState.ToString().Equals(stateCode, StringComparison.OrdinalIgnoreCase) || selectedState.ToString().Equals(stateName, StringComparison.OrdinalIgnoreCase);
                }
            }
            return res;
        }

        private bool HasLanguageVersion(Item item, string languageName)
        {
            var language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                var languageSpecificItem = database.GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        private SafeDictionary<string, string> SourceItems
        {
            get
            {
                var dictionaryItems = new SafeDictionary<string, string>();
                Item item = null;
                if (!string.IsNullOrEmpty(DataSource))
                    item = database.GetItem(DataSource, Language);

                if (item != null)
                {
                    item.GetChildren().ToList().ForEach(delegate(Item p)
                    {
                        if (HasLanguageVersion(p, Language.Name))
                        {
                            if (!dictionaryItems.Keys.Contains(p["Key"]))
                                dictionaryItems.Add(p["Key"], p["Value"]);
                        }
                    });
                }

                return dictionaryItems;
            }
        }

        private Language Language
        {
            get
            {
                var language = Language.Parse(Culture);
                if (!string.IsNullOrEmpty(CultureToUse))
                {
                    try
                    {
                        language = Language.Parse(CultureToUse);
                    }
                    catch (Exception exception)
                    {
                        Sitecore.Diagnostics.Log.Error(exception.Message, this);
                    }
                }
                return language;
            }
        }

        private string Culture
        {
            get
            {
                string culture = string.Empty;
                try
                {
                    if (Sitecore.Context.PageMode.IsNormal)
                    {
                        var url = HttpContext.Current.Request.Url.AbsolutePath;
                        {
                            var regexMatch = Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

                            if (!string.IsNullOrEmpty(regexMatch.Value))
                            {
                                culture = regexMatch.Value.Replace("/", string.Empty);
                            }
                        }
                    }
                }
                catch (Exception exception) { Sitecore.Diagnostics.Log.Error(exception.Message, this); culture = Sitecore.Context.Culture.Name; }
                return culture;
            }
        }

        private Database database
        {
            get
            {
                return Sitecore.Configuration.Factory.GetDatabase("web");
            }
        }
    }
}
