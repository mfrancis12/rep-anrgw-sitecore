﻿using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomFields
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public class CustomLabel : HtmlParagraph
    {
        private static readonly string baseCssClassName = "scfIntroLabel";
        [VisualCategory("Label Text"), VisualFieldType(typeof(Sitecore.Form.Core.Visual.TextAreaField)), VisualProperty("Label Text:", 10)]
        public string LabelText
        {
            get;
            set;
        }

        public CustomLabel()  : base()
        {
            this.Class = CustomLabel.baseCssClassName;
        }
       
        protected override void OnInit(EventArgs e)
        {
            this.Text = LabelText;
        }
    }
}
