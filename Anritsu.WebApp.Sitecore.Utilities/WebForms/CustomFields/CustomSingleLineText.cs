﻿using System;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Visual;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Form.Web.UI.Controls;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomFields
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public class CustomSingleLineText : RegexInputControl
    {
        private static readonly string baseCssClassName = "scfSingleLineTextBorder";
        [VisualCategory("Validation"), VisualProperty("Maximum Length:", 2000), DefaultValue(256)]
        public int MaxLength
        {
            get
            {
                return this.textbox.MaxLength;
            }
            set
            {
                this.textbox.MaxLength = value;
            }
        }
        [VisualCategory("Validation"), VisualProperty("Minimum Length:", 1000), DefaultValue(0)]
        public int MinLength
        {
            get;
            set;
        }
        [VisualFieldType(typeof(CssClassField)), VisualProperty("Css Class:", 600), DefaultValue("scfSingleLineTextBorder")]
        public new string CssClass
        {
            get
            {
                return base.CssClass;
            }
            set
            {
                base.CssClass = value;
            }
        }

        public CustomSingleLineText(HtmlTextWriterTag tag)
            : base(tag)
        {
            this.MaxLength = 256;
            this.MinLength = 0;
            this.CssClass = CustomSingleLineText.baseCssClassName;
        }
        public CustomSingleLineText()
            : this(HtmlTextWriterTag.Div)
        {
        }
        protected override void OnInit(EventArgs e)
        {
            this.textbox.CssClass = "scfSingleLineTextBox";
            this.help.CssClass = "scfSingleLineTextUsefulInfo";
            this.generalPanel.CssClass = "scfSingleLineGeneralPanel";
            this.title.CssClass = "scfSingleLineTextLabel";
            this.textbox.TextMode = TextBoxMode.SingleLine;
            this.Controls.AddAt(0, this.generalPanel);
            this.Controls.AddAt(0, this.title);
            this.generalPanel.Controls.AddAt(0, this.help);
            this.generalPanel.Controls.AddAt(0, this.textbox);
        }
    }
}