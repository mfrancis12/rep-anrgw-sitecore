﻿using Anritsu.WebApp.SitecoreUtilities.Helpers;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomFields
{
    public class ProductsDropList : Sitecore.Form.Web.UI.Controls.DropList
    {
        private const string ProductsTemplateId = "{94757055-E1B4-4B0D-B0E7-7BDBC26861D8}";
        protected override void InitItems(Sitecore.Form.Web.UI.Controls.ListItemCollection items)
        {
            this.KeepHiddenValue = false;

            items = new Sitecore.Form.Web.UI.Controls.ListItemCollection();
            var cacheItems = CacheHelper.GetCache<IOrderedEnumerable<Item>>("ProductsDropList-" + Sitecore.Context.Language.Name);
            if (cacheItems == null)
            {
                var products =
                    Database.SelectItems("fast:/sitecore/content/global/products//*[@@templateid='" + ProductsTemplateId +
                                         "' and @isDiscontinued!='1' and @HideInRequestProductQuote!='1']");
                var contextLanguage = Language.Parse(Culture);
                var regionalProducts = new List<Item>();
                products.ToList().ForEach(delegate(Item p)
                {
                    if (ItemManager.GetVersions(p, contextLanguage).Count > 0)
                    {
                        regionalProducts.Add(p);
                    }
                });

                var orderedList = from element in regionalProducts
                                  orderby element.Name ascending
                                  select element;
                var item = new ListItem { Text = "Select", Value = "Select" };
                items.Add(item);
                foreach (var listItem in orderedList.Select(product => new ListItem
                {
                    Text = ProductName(product),
                    Value = product.Name
                }))
                {
                    items.Add(listItem);
                }
                CacheHelper.AddCache("ProductsDropList-" + Sitecore.Context.Language.Name, orderedList);
            }
            else
            {
                var item = new ListItem { Text = "Select", Value = "Select" };
                items.Add(item);
                foreach (var listItem in cacheItems.Select(product => new ListItem
                {
                    Text = ProductName(product),
                    Value = product.Name
                }))
                {
                    items.Add(listItem);
                }
            }
            base.InitItems(items);
        }

        private string ProductName(Item product)
        {
            var desc = product.Fields["ProductName"];
            if (desc != null && desc.Value != product.Name)
            {
                return product.Name.ToUpperInvariant() + " - " + desc;
            }
            return product.Name.ToUpperInvariant();
        }

        private string Culture
        {
            get
            {
                var culture = string.Empty;
                try
                {
                    if (Sitecore.Context.PageMode.IsNormal)
                    {
                        var url = HttpContext.Current.Request.Url.AbsolutePath;
                        {
                            var regexMatch = Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

                            if (!string.IsNullOrEmpty(regexMatch.Value))
                            {
                                culture = regexMatch.Value.Replace("/", string.Empty);
                            }
                        }
                    }
                }
                catch (Exception exception) { Sitecore.Diagnostics.Log.Error(exception.Message, this); culture = Sitecore.Context.Culture.Name; }
                return culture;
            }
        }

        private Database Database
        {
            get
            {
                return Sitecore.Configuration.Factory.GetDatabase("web");
            }
        }
    }
}
