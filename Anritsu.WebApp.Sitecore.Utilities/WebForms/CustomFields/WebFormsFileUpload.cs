﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Core.Media;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.UI.Adapters;
using Sitecore.Form.Web.UI.Controls;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomFields
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1708:IdentifiersShouldDifferByMoreThanCase"), Adapter(typeof(FileUploadAdapter))]
    public class WebFormsFileUpload : ValidateControl, IHasTitle
    {
        private static readonly string baseCssClassName = "scfFileUploadBorder";
        protected Panel generalPanel = new Panel();
        protected Sitecore.Form.Web.UI.Controls.Label title = new Sitecore.Form.Web.UI.Controls.Label();
        protected FileUpload upload = new FileUpload();
        private string uploadDir = "/sitecore/media library";

        public override string ID
        {
            get
            {
                return upload.ID;
            }
            set
            {
                title.ID = value + "text";
                upload.ID = value;
                base.ID = value + "scope";
            }
        }

        [VisualCategory("Validation"), VisualFieldType(typeof(SelectPredefinedValidatorField)), VisualProperty("Validation:", 790), DefaultValue("")]
        public string PredefinedValidator
        {
            get
            {
                return classAtributes["predefinedValidator"];
            }
            set
            {
                classAtributes["predefinedValidator"] = value;
            }
        }

        [Localize, SitecoreDefaultValue("{070FCA14-1E9A-45D7-8611-EA650F20FE77}", "{2D63E327-79CC-479D-950E-DA4E1F7A7C82}"), VisualCategory("Validation"), VisualFieldType(typeof(TextAreaField)), VisualProperty("Error Message:", 810)]
        public string PredefinedValidatorTextMessage
        {
            get;
            set;
        }

        [VisualCategory("Validation"), VisualFieldType(typeof(RegexField)), VisualProperty("Regular Expression:", 800), DefaultValue("")]
        public string RegexPattern
        {
            get
            {
                return classAtributes["regex"];
            }
            set
            {
                classAtributes["regex"] = value;
            }
        }

        [VisualCategory("Validation"), VisualProperty("File Size Limit (In MB):", 900), DefaultValue("1")]
        public int FileSizeLimit
        {
            get;
            set;
        }

        [VisualCategory("Upload"), VisualFieldType(typeof(SelectDirectoryField)), VisualProperty("Upload To:", 0), DefaultValue("/sitecore/media library")]
        public string UploadTo
        {
            get
            {
                return uploadDir;
            }
            set
            {
                uploadDir = value;
            }
        }

        public override ControlResult Result
        {
            get
            {
                if (upload.HasFile)
                {
                    return new ControlResult(ControlName, new PostedFile(upload.FileBytes, upload.FileName, UploadTo), "medialink");
                }
                return new ControlResult(ControlName, null, string.Empty);
            }
            set { Result = value; }
        }

        public string Title
        {
            get
            {
                return title.Text;
            }
            set
            {
                title.Text = value;
            }
        }

        [VisualFieldType(typeof(CssClassField)), VisualProperty("Css Class:", 600), DefaultValue("scfFileUploadBorder")]
        public new string CssClass
        {
            get
            {
                return base.CssClass;
            }
            set
            {
                base.CssClass = value;
            }
        }

        protected override Control ValidatorContainer
        {
            get
            {
                return this;
            }
        }

        protected override Control InnerValidatorContainer
        {
            get
            {
                return generalPanel;
            }
        }

        public WebFormsFileUpload(HtmlTextWriterTag tag)
            : base(tag)
        {
            CssClass = WebFormsFileUpload.baseCssClassName;
        }

        public WebFormsFileUpload()
            : this(HtmlTextWriterTag.Div)
        {
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            DoRender(writer);
        }

        protected virtual void DoRender(HtmlTextWriter writer)
        {
            base.RenderControl(writer);
        }

        protected override void OnInit(EventArgs e)
        {
            upload.CssClass = "scfFileUpload";
            help.CssClass = "scfFileUploadUsefulInfo";
            title.CssClass = "scfFileUploadLabel";
            title.AssociatedControlID = upload.ID;
            generalPanel.CssClass = "scfFileUploadGeneralPanel";
            Controls.AddAt(0, generalPanel);
            Controls.AddAt(0, title);
            generalPanel.Controls.AddAt(0, help);
            generalPanel.Controls.AddAt(0, upload);
        }
    }
}
