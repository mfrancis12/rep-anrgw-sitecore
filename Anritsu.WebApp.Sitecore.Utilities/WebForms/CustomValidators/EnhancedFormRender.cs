﻿using System.Web.UI;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomValidators
{
    public class EnhancedFormRender : Sitecore.Form.Core.Renderings.FormRender
    {
        protected override void DoRender(HtmlTextWriter output)
        {
            base.DoRender(output);

            if (System.Web.HttpContext.Current.Items["IncludeExtension"] == null)
            {
                output.Write("<script type=\"text/javascript\" src=\"/sitecore modules/Web/Web Forms for Marketers/scripts/wffmExtension.js\"></script>");
            }
            System.Web.HttpContext.Current.Items["IncludeExtension"] = true;
        }
    }
}