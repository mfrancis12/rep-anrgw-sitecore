﻿using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Utility;
using Sitecore.Form.Core.Validators;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using System;
using System.ComponentModel;
using System.Linq;


namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomValidators
{
    /// <summary>
    /// Enhanced Required Validator which compares contents in current field are same as in related field.
    /// </summary>
    public class EnhancedRequiredValidator : FormCustomValidator
    {
        public EnhancedRequiredValidator()
            : base()
        {
            this.ValidateEmptyText = true;
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.WebControls.CustomValidator.ServerValidate" /> event for the <see cref="T:System.Web.UI.WebControls.CustomValidator" /> textBoxControl.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>
        /// true if the value specified by the <paramref name="value" /> parameter passes validation; otherwise, false.
        /// </returns>
        protected override bool OnServerValidate(string value)
        {
            var result = true;

            var controlToValidate = this.FindControl(this.ControlToValidate);
            if (controlToValidate != null)
            {
                var textBoxControl = controlToValidate as System.Web.UI.IAttributeAccessor;
                if (textBoxControl != null)
                {
                    // Get Related Control's Client ID
                    var relatedClientId = textBoxControl.GetAttribute("RelatedValidationItemClientId");
                    var validValues = textBoxControl.GetAttribute("ValidValues");
                    var initialValue = textBoxControl.GetAttribute("InitialValue");
                    initialValue = initialValue == null ? "" : initialValue;

                    if (!string.IsNullOrWhiteSpace(validValues) && !string.IsNullOrWhiteSpace(relatedClientId))
                    {
                        // Obtain an instance of the Form
                        var wffmForm = WebUtil.GetParent<Sitecore.Form.Core.Renderings.FormRender>(this);
                        if (wffmForm != null)
                        {
                            // Obtain an control instance of the corresponding ClientID
                            // and Cast to Base Control for this situation
                            var requestType = WebUtil.FindFirstOrDefault(wffmForm, c => c.ClientID == relatedClientId) as BaseControl;
                            if (requestType != null)
                            {
                                var selectedValue = Convert.ToString(requestType.Result.Value);
                                if (CheckWithValidValuesIfValidationRequired(selectedValue, validValues))
                                {
                                    return ValidateValue(value, initialValue);
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        private bool ValidateValue(string value, string initialValue)
        {
            // Validate value provided by user
            // This piece of code is taken from System.Web.UI.WebControls.RequiredFieldValidator
            return !value.Trim().Equals(initialValue.Trim());
        }

        private bool CheckWithValidValuesIfValidationRequired(string selectedValue, string validValues)
        {
            // Split the valid values
            var arrayOfValidValues = validValues.Split(new[] { '|' }, System.StringSplitOptions.RemoveEmptyEntries);

            // Comparing with valid values if matched then this Textbox is mandatory
            var isRequired = arrayOfValidValues.Any(o => o.Trim().Equals(selectedValue, System.StringComparison.OrdinalIgnoreCase));
            return isRequired;
        }
    }
}
