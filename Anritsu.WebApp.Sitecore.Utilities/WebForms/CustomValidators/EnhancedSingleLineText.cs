﻿using Sitecore.Data;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using System;
using System.ComponentModel;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomValidators
{
    public class EnhancedSingleLineText : SingleLineText
    {
        /// <summary>
        /// Gets or sets the related validation item.
        /// </summary>
        /// <value>
        /// The related validation item.
        /// </value>
        [VisualProperty("Related Validation Item:", 600), VisualFieldType(typeof(EditField)), DefaultValue("")]
        public string RelatedValidationItem
        {
            get
            {
                return this.textbox.Attributes["RelatedValidationItem"];
            }
            set
            {
                this.textbox.Attributes["RelatedValidationItem"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the valid values of Related Validation Item for this field to be mandatory.
        /// </summary>
        /// <value>
        /// The related validation item.
        /// </value>
        [VisualProperty("Valid Values:", 700), VisualFieldType(typeof(EditField)), DefaultValue("")]
        public string ValidValues
        {
            get
            {
                return this.textbox.Attributes["ValidValues"];
            }
            set
            {
                this.textbox.Attributes["ValidValues"] = value;
            }
        }

        /// <summary>Gets or sets the initial value of the associated input textBoxControl.</summary>
        /// <returns>A string that specifies the initial value of the associated input textBoxControl. The default is <see cref="F:System.String.Empty" />.</returns>
        [VisualProperty("Initial Value:", 800), VisualFieldType(typeof(EditField)), DefaultValue("")]
        public string InitialValue
        {
            get
            {
                return this.textbox.Attributes["InitialValue"];
            }
            set
            {
                this.textbox.Attributes["InitialValue"] = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Guid relatedItemGuid;
            if (Guid.TryParse(RelatedValidationItem, out relatedItemGuid))
            {
                var id = new ID(relatedItemGuid).ToShortID().ToString();
                var relatedControl = this.FindControl("field_" + id);
                if (relatedControl != null)
                {
                    textbox.Attributes.Add("RelatedValidationItemClientId", relatedControl.ClientID);
                }
            }
        }
    }
}
