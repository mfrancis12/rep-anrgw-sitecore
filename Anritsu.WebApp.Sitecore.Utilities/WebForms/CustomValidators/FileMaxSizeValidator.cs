﻿using System;
using System.Web.UI.WebControls;
using Sitecore.Form.Core.Validators;
using System.IO;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.CustomValidators
{
    class FileMaxSizeValidator : FormCustomValidator
    {
        public int FileSizeLimit
        {
            get;
            set;
        }

        public FileMaxSizeValidator()
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            base.ErrorMessage = string.Format(base.ErrorMessage, FileSizeLimit.ToString(), FileSizeLimit.ToString());
            Text = string.Format(Text, FileSizeLimit.ToString(), FileSizeLimit.ToString());
            base.OnLoad(e);
        }

        protected override bool OnServerValidate(string value)
        {
            FileUpload upload = FindControl(base.ControlToValidate) as FileUpload;
            if (upload == null || FileSizeLimit <= 0) return true;

            var validSize = upload.FileBytes.Length <= (FileSizeLimit * 1024 * 1024);
            if (validSize)
            {
                SaveBytesToFile(upload.FileName, upload.FileBytes);
            }
            return validSize;

        }

        private void SaveBytesToFile(string filename, byte[] bytesToWrite)
        {
            try
            {
                if (!string.IsNullOrEmpty(filename) && bytesToWrite != null)
                {
                    string filePath = string.Empty;
                    string tempDirectory = HttpContext.Current.Server.MapPath("/temp");
                    if (!Directory.Exists(tempDirectory))
                        Directory.CreateDirectory(tempDirectory);

                    filePath = tempDirectory + "\\" + filename;
                    Sitecore.Diagnostics.Log.Debug("File is being saved to " + filePath, this);
                    using (FileStream file = System.IO.File.Create(filePath))
                    {
                        file.Write(bytesToWrite, 0, bytesToWrite.Length);
                        file.Flush();
                    }
                    Sitecore.Diagnostics.Log.Info("File has been saved to " + filePath, this);
                    HttpContext.Current.Session["WffmUploadFilePath"] = filePath;
                }
            }
            catch (Exception exception)
            {
                Sitecore.Diagnostics.Log.Error(exception.Source + " " + exception.StackTrace, this);
            }
        }
    }
}