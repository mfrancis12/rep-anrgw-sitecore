﻿using Sitecore.Configuration;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.DbMappers
{
    public class DbMapper : DialogForm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Params")]
        public string Params
        {
            get
            {
                return (System.Web.HttpContext.Current.Session[Sitecore.Web.WebUtil.GetQueryString("params")] as string);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Sitecore.Context.ClientPage.IsEvent)
            {
                var db = Factory.GetDatabase(Sitecore.Web.WebUtil.GetQueryString("db"));
                var formId = Sitecore.Web.WebUtil.GetQueryString("id");
                var form = new FormItem(db.GetItem(formId));
                var parameters = ParametersUtil.XmlToNameValueCollection(Params);
                string mappedTable = string.Empty;

                if (!string.IsNullOrEmpty(parameters["MappedTable"]))
                    mappedTable = parameters["MappedTable"];


                List<string> tables = new List<string>();
                tables = Tables;
                List<string> columns = new List<string>();
                columns = GetColumns("AR_MediaSignUp");
                List<string> procedures = new List<string>();
                procedures = Procedures;
                List<string> procedureParams = new List<string>();
                procedureParams = GetProcedureParams(mappedTable);

                procedures.ForEach(delegate(string p)
                {
                    AvailableTables.Controls.Add(new ListItem()
                    {
                        Header = p,
                        Value = p,
                        ID = Control.GetUniqueID("efn"),
                        Selected = p == mappedTable
                    });
                });

                AvailableTables.Controls.AddAt(0, new ListItem() { Header = "--Select Procedure--", Value = "--Select Procedure--" });
                GridPanel.Columns = 2;

                if (form != null)
                {
                    // Iterate WFM form fields
                    foreach (var field in form.Fields)
                    {

                        Literal ltrFormField = new Literal();
                        ltrFormField.Text = field.Name.Replace("*", "").Replace(":", "") + " : ";
                        ltrFormField.CssClass = "scComboboxLabel";
                        ltrFormField.Attributes.Add("width", "200px");
                        ltrFormField.ID = Control.GetUniqueID("ff_");
                        GridPanel.Controls.Add(ltrFormField);

                        if (isInputField(field))
                        {
                            Listbox columnMapper = new Listbox();
                            columnMapper.ID = field.ID.ToString();

                            procedureParams.ForEach(delegate(string p)
                            {
                                columnMapper.Controls.Add(new ListItem()
                                {
                                    Header = p,
                                    Value = p,
                                    ID = Control.GetUniqueID("efn"),
                                    Selected = p == GetFieldValue(field.ID.ToString())
                                });
                            });

                            columnMapper.Controls.AddAt(0, new ListItem() { Header = "--Select Parameter--", Value = "--Select Parameter--" });
                            GridPanel.Controls.Add(columnMapper);
                        }
                        else
                        {
                            Literal ltrEmptyField = new Literal();
                            ltrEmptyField.Text = string.Empty;
                            ltrEmptyField.ID = Control.GetUniqueID("ff_");
                            GridPanel.Controls.Add(ltrEmptyField);
                        }
                    }
                }
            }
        }

        private bool isInputField(FieldItem field)
        {
            var db = Factory.GetDatabase(Sitecore.Web.WebUtil.GetQueryString("db"));
            bool isInputField = true;
            Item filedItem = db.GetItem(field.ID);
            if (filedItem != null)
            {
                ReferenceField refField = filedItem.Fields["Field Link"];
                if (refField != null)
                {
                    Item item = refField.TargetItem;
                    if (item != null)
                    {
                        string val = item.Fields["Class"].Value;
                        string[] arr = val.Split('.');
                        if (arr.Length > 0)
                            if (arr[arr.Length - 1].ToUpperInvariant().Equals("CustomLabel".ToUpperInvariant()))
                                isInputField = false;
                    }
                }
            }
            return isInputField;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> GetColumns(string tableName)
        {
            List<string> listColumns = new List<string>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString))
            {
                string[] restrictions = new string[4] { null, null, tableName, null };
                conn.Open();
                var columnList = conn.GetSchema("Columns", restrictions).AsEnumerable().Select(s => s.Field<String>("Column_Name")).ToList();
                if (columnList != null)
                    columnList.ForEach(delegate(string p) { listColumns.Add(p); });
            }
            return listColumns;
        }

        protected string GetFieldValue(string id)
        {
            string result = string.Empty;
            var parameters = ParametersUtil.XmlToNameValueCollection(Params);
            var mappedFields = new string[0];
            if (!string.IsNullOrEmpty(parameters["MappedFields"]))
                mappedFields = parameters["MappedFields"].Split(',');

            foreach (string key in mappedFields)
            {
                var fieldKey = new string[0];
                fieldKey = key.Split(':');
                if (fieldKey.Length > 0)
                {
                    if (fieldKey.Contains(id))
                    {
                        result = fieldKey[1];
                        break;
                    }
                }
            }
            return result;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<string> Tables
        {
            get
            {
                string conStrng = ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(conStrng))
                {
                    connection.Open();
                    DataTable schema = connection.GetSchema("Tables");
                    List<string> TableNames = new List<string>();
                    foreach (DataRow row in schema.Rows)
                    {
                        TableNames.Add(row[2].ToString());
                    }
                    return TableNames;
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected List<string> Procedures
        {
            get
            {
                string conStrng = ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(conStrng))
                {
                    connection.Open();
                    DataTable schema = connection.GetSchema("Procedures");
                    List<string> TableNames = new List<string>();
                    foreach (DataRow row in schema.Rows)
                    {
                        TableNames.Add(row[2].ToString());
                    }
                    return TableNames;
                }
            }
        }

        private List<string> GetProcedureParams(string procedureName)
        {
            List<string> spParams = new List<string>();
            if (!string.IsNullOrEmpty(procedureName))
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString);
                SqlCommand cmd = new SqlCommand(procedureName, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlCommandBuilder.DeriveParameters(cmd);
                foreach (SqlParameter p in cmd.Parameters)
                {
                    spParams.Add(p.ParameterName.Replace("@", ""));
                }
            }
            return spParams;
        }

        protected override void OnOK(object sender, EventArgs args)
        {
            NameValueCollection nameValueCollection = new NameValueCollection();
            var selectedFields = new List<string>();
            foreach (var control in GridPanel.Controls.OfType<Sitecore.Web.UI.HtmlControls.Listbox>())
            {
                selectedFields.Add(control.ID + ":" + control.Value);
            }
            // Get XML fragment of settings to store
            SheerResponse.SetDialogValue(
            ParametersUtil.NameValueCollectionToXml(new NameValueCollection(){
                            {"MappedTable", AvailableTables.SelectedItem.Value},
                            {"MappedFields",  string.Join(",", selectedFields)}}));
            base.OnOK(sender, args);
        }

        public void OnItemsModeChanged()
        {
            string selectedProc = this.AvailableTables.Value;
            if (!string.IsNullOrEmpty(selectedProc))
            {
                List<string> procedureParams = new List<string>();
                procedureParams = GetProcedureParams(selectedProc);
                List<string> ids = new List<string>();
                ClearControls();

                foreach (var control in GridPanel.Controls.OfType<Listbox>())
                {
                    procedureParams.ForEach(delegate(string p)
                    {
                        control.Controls.Add(new ListItem()
                        {
                            Header = p,
                            Value = p,
                            ID = Control.GetUniqueID("efn"),
                        });
                    });
                    control.Controls.AddAt(0, new ListItem() { Header = "--Select Parameter--", Value = "--Select Parameter--" });
                }
            }
        }

        private void ClearControls()
        {
            Listbox[] controls = GridPanel.Controls.OfType<Listbox>().ToArray();
            foreach (Listbox lbControl in controls)
            {
                lbControl.Controls.Clear();
                SheerResponse.Refresh(lbControl);
            }
        }

        private void RefreshControls()
        {
            Listbox[] controls = GridPanel.Controls.OfType<Listbox>().ToArray();
            foreach (Listbox lbControl in controls)
            {
                SheerResponse.Refresh(lbControl);
            }
        }

        public GridPanel GridPanel { get; set; }
        public Listbox AvailableTables { get; set; }
    }
}
