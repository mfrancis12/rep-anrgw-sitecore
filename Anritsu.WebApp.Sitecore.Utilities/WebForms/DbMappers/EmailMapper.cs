﻿using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.DbMappers
{
    public class EmailMapper : DialogForm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Params")]
        public string Params
        {
            get
            {
                return (System.Web.HttpContext.Current.Session[Sitecore.Web.WebUtil.GetQueryString("params")] as string);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Sitecore.Context.ClientPage.IsEvent)
            {
                var db = Factory.GetDatabase(Sitecore.Web.WebUtil.GetQueryString("db"));
                var formId = Sitecore.Web.WebUtil.GetQueryString("id");
                var form = new FormItem(db.GetItem(formId));
                var parameters = ParametersUtil.XmlToNameValueCollection(Params);
                string mappedAdminTemplate = string.Empty;
                string mappedUserTemplate = string.Empty;
                string mappedDistributionListKey = string.Empty;

                if (!string.IsNullOrEmpty(parameters["AdminEmailTemplateId"]))
                    mappedAdminTemplate = parameters["AdminEmailTemplateId"];

                if (!string.IsNullOrEmpty(parameters["UserEmailTemplateId"]))
                    mappedUserTemplate = parameters["UserEmailTemplateId"];

                if (!string.IsNullOrEmpty(parameters["DistributionListKey"]))
                    mappedDistributionListKey = parameters["DistributionListKey"];

                Dictionary<string, string> emailTeplates = new Dictionary<string, string>();
                emailTeplates = EmailTemplates;

                foreach (var tpl in emailTeplates)
                {
                    AdminEmailTemplates.Controls.Add(new ListItem()
                    {
                        Header = tpl.Value,
                        Value = tpl.Key,
                        ID = Control.GetUniqueID("efn"),
                        Selected = tpl.Key == mappedAdminTemplate
                    });
                }

                AdminEmailTemplates.Controls.AddAt(0, new ListItem() { Header = "--Select Admin Email Template--", Value = "Select" });

                foreach (var tpl in emailTeplates)
                {
                    UserEmailTemplates.Controls.Add(new ListItem()
                    {
                        Header = tpl.Value,
                        Value = tpl.Key,
                        ID = Control.GetUniqueID("efn"),
                        Selected = tpl.Key == mappedUserTemplate
                    });
                }

                UserEmailTemplates.Controls.AddAt(0, new ListItem() { Header = "--Select User Email Template--", Value = "Select" });

                Dictionary<string, string> distributionList = new Dictionary<string, string>();
                distributionList = DistributedListNames;
                foreach (var listItem in distributionList)
                {
                    DistributionList.Controls.Add(new ListItem()
                    {
                        Header = listItem.Value,
                        Value = listItem.Key,
                        ID = Control.GetUniqueID("efn"),
                        Selected = listItem.Key == mappedDistributionListKey
                    });
                }

                DistributionList.Controls.AddAt(0, new ListItem() { Header = "--Select Distribution List--", Value = "Select" });
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected Dictionary<string, string> EmailTemplates
        {
            get
            {
                Database db = Sitecore.Configuration.Factory.GetDatabase("web");
                Item templateFolder = db.GetItem("{E25A3558-B427-4CDD-9960-0C60D6B819D8}");
                Dictionary<string, string> templates = new Dictionary<string, string>();

                if (templateFolder != null)
                {
                    templateFolder.GetChildren().ToList().ForEach(delegate(Item p)
                    {
                        if (p.Fields["BodyTemplate"] != null)
                        {
                            templates.Add(p.ID.ToString(), p.Name);
                        }
                    });
                }
                return templates;
            }
        }


        //private List<string> GetTemplateTokens(string selectedTemplate)
        //{
        //    List<string> tokens = new List<string>();
        //    Database db = Sitecore.Configuration.Factory.GetDatabase("web");
        //    Item templateFolder = db.GetItem("{E25A3558-B427-4CDD-9960-0C60D6B819D8}");
        //    if (!string.IsNullOrEmpty(selectedTemplate))
        //    {
        //        List<string> strs = new List<string>();
        //        Item tpl = templateFolder.GetChildren().Where(x => x.Name.Equals(selectedTemplate)).FirstOrDefault();
        //        if (tpl != null)
        //        {
        //            strs = ExtractFromString(tpl["BodyTemplate"], "[[", "]]");
        //            foreach (string p in strs)
        //            {
        //                tokens.Add(p);
        //            }
        //        }
        //    }
        //    return tokens;
        //}



        //private List<string> ExtractFromString(string text, string startString, string endString)
        //{
        //    List<string> matched = new List<string>();
        //    int indexStart = 0, indexEnd = 0;
        //    bool exit = false;
        //    while (!exit)
        //    {
        //        indexStart = text.IndexOf(startString, StringComparison.OrdinalIgnoreCase);
        //        indexEnd = text.IndexOf(endString, StringComparison.OrdinalIgnoreCase);
        //        if (indexStart != -1 && indexEnd != -1)
        //        {
        //            matched.Add(text.Substring(indexStart + startString.Length,
        //                indexEnd - indexStart - startString.Length));
        //            text = text.Substring(indexEnd + endString.Length);
        //        }
        //        else
        //            exit = true;
        //    }
        //    return matched;
        //}


        protected override void OnOK(object sender, EventArgs args)
        {
            NameValueCollection nameValueCollection = new NameValueCollection();
            // Get XML fragment of settings to store
            SheerResponse.SetDialogValue(
            ParametersUtil.NameValueCollectionToXml(new NameValueCollection(){
                            {"AdminEmailTemplateId", AdminEmailTemplates.SelectedItem.Value},
                            {"UserEmailTemplateId",  UserEmailTemplates.SelectedItem.Value},
                            {"DistributionListKey",  DistributionList.SelectedItem.Value}
            }));
            base.OnOK(sender, args);
        }

        protected Dictionary<string, string> DistributedListNames
        {
            get
            {
                Dictionary<string, string> distributionList = new Dictionary<string, string>();
                string conStrng = ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(conStrng))
                {
                    connection.Open();
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString);
                    string query = "SELECT [DistributionListKey],[DistributionName] FROM GW_EmailDistributionList WHERE IsActive='TRUE' ORDER BY DistributionName";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandType = CommandType.Text;
                    conn.Open();

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.SelectCommand.CommandTimeout = 120;
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        DataTable tb = ds.Tables[0];
                        if (tb.Rows.Count > 0)
                        {
                            foreach (DataRow dr in tb.Rows)
                            {
                                distributionList.Add(dr[0].ToString(), dr[1].ToString());
                            }
                        }
                    }

                    return distributionList;
                }
            }
        }

        public Listbox AdminEmailTemplates { get; set; }
        public Listbox UserEmailTemplates { get; set; }
        public Listbox DistributionList { get; set; }
    }
}
