﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Anritsu.Library.EmailSDK;
using Anritsu.Library.EmailSDK.EmailWebRef;
using Anritsu.WebApp.SitecoreUtilities.WebForms.EmailHelper.Models;
using Anritsu.WebApp.SitecoreUtilities.WebForms.SaveActions;
using Sitecore;
using Sitecore.Diagnostics;
using Convert = System.Convert;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.EmailHelper
{
    public static class EmailManager
    {
        private const string UserEmailTemplateKey = "USER-EmailTemplateKey";
        private const string AdminEmailTemplateKey = "ADMIN-EmailTemplateKey";
        //this key will be used to trace the error from service in logs
        private const string ServiceResponse = "Response from Service : ";

        public static bool SendMail(Dictionary<string, string> formFields)
        {
            var replaceKeys = GetReplaceKeyValues(formFields).ToArray();

            var results = true;

            if (formFields.ContainsKey(UserEmailTemplateKey))
            {
                results = SendTemplatedUserEmail(formFields[UserEmailTemplateKey], formFields["Email_Address"], replaceKeys);
            }

            if (!formFields.ContainsKey(AdminEmailTemplateKey)) return results;

            var adminResults = SendTemplatedDistributedEmail(formFields[AdminEmailTemplateKey], formFields["DistributionListKey"], replaceKeys);

            return results && adminResults;
        }

        public static bool SendTemplatedUserEmail(string emailTemplateKey, string email, ReplaceKeyValueType[] replaceKeys)
        {
            var myReq = new SendTemplatedEmailCallRequest
            {
                EmailTemplateKey = emailTemplateKey,
                ToEmailAddresses = new[] { email },
                CultureGroupId = GetCulture(Context.Language.Name),
                BodyReplacementKeyValues = replaceKeys,
                SubjectReplacementKeyValues = replaceKeys
            };

            var myResp = EmailServiceManager.SendTemplatedEmail(myReq);
            if (myResp.StatusCode != 0)
                Error.LogError(ServiceResponse + myResp.ResponseMessage);
            return myResp.StatusCode == 0;
        }

        public static bool SendTemplatedDistributedEmail(string emailTemplateKey, string distributionListKey, ReplaceKeyValueType[] replaceKeys)
        {
            List<ReplaceKeyValueType> updatereplaceKeys=new List<ReplaceKeyValueType>();

            foreach (ReplaceKeyValueType value in replaceKeys)
            {
                ReplaceKeyValueType newValue = new ReplaceKeyValueType();
                newValue.TemplateKey = value.TemplateKey;
                newValue.ReplacementValue = HttpUtility.HtmlDecode(value.ReplacementValue);
                updatereplaceKeys.Add(newValue);
            }

            var myRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = emailTemplateKey,
                DistributionListKey = distributionListKey,
                CultureGroupId = GetCulture(Context.Language.Name),
                BodyReplacementKeyValues = updatereplaceKeys.ToArray(),
                SubjectReplacementKeyValues = updatereplaceKeys.ToArray()
            };
            var myResp = EmailServiceManager.SendDistributedTemplatedEmail(myRequest);
            if (myResp.StatusCode != 0)
                Error.LogError(ServiceResponse + myResp.ResponseMessage);
            return myResp.StatusCode == 0;

        }

        /// <summary>
        /// Body Replacement Values -- all keys and values
        /// </summary>
        /// <param name="formFields"></param>
        /// <returns></returns>
        private static List<ReplaceKeyValueType> GetReplaceKeyValues(IReadOnlyDictionary<string, string> formFields)
        {
            var replaceKeys = new List<ReplaceKeyValueType>();

            replaceKeys.AddRange(
                formFields.Select(
                    field =>
                        new ReplaceKeyValueType
                        {
                            TemplateKey = "[[" + field.Key.Replace(" ", "").ToUpper(CultureInfo.InvariantCulture) + "]]",
                            ReplacementValue = field.Value.HtmlEncodeDecode()
                        }));

            //replaceKeys.Add(new ReplaceKeyValueType
            //{
            //    TemplateKey = "[[USEREMAIL]]",
            //    ReplacementValue = formFields["Email"] ?? formFields["Email_Address"]
            //});

            replaceKeys.Add(new ReplaceKeyValueType { TemplateKey = "[[IPADDRESS]]", ReplacementValue = GetIPAddress });

            //EMAILREQUESTS
            //if (!formFields.ContainsKey("MediaRequest")) return replaceKeys;

            //var emailRequests = formFields["MediaRequest"].Split(',');
            //if (emailRequests.Length <= 0) return replaceKeys;

            //var emailRequestsHtml = new StringBuilder();
            //emailRequestsHtml.Append("<ul>");
            //emailRequests.ToList()
            //    .ForEach(delegate(string p) { emailRequestsHtml.Append("<li>" + p + "</li>"); });
            //emailRequestsHtml.Append("</ul>");

            //replaceKeys.Add(new ReplaceKeyValueType
            //{
            //    TemplateKey = "[[EMAILREQUESTS]]",
            //    ReplacementValue = emailRequestsHtml.ToString()
            //});

            var browser = HttpContext.Current.Request.Browser;
            replaceKeys.Add(new ReplaceKeyValueType { TemplateKey = "[[BROWSER]]", ReplacementValue = browser.Type.HtmlEncodeDecode() });
            replaceKeys.Add(new ReplaceKeyValueType { TemplateKey = "[[VERSION]]", ReplacementValue = browser.Version.HtmlEncodeDecode() });

            return replaceKeys;
        }

        public static bool NewsletterSignupConfirmationSendMail(EmailActionsModel objParameter)
        {
            var myRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.NewsletterList,
                DistributionListKey =
                    "NewsLetter_Signup_Request_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region Body Replacement Values -- all keys and values

            var bodyKeys = new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() + " " + objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FIRSTNAME]]", ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[LASTNAME]]", ReplacementValue = objParameter.LastName.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[COMPANY]]", ReplacementValue = objParameter.Company.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRY]]",
                ReplacementValue = objParameter.Country
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[USEREMAIL]]", ReplacementValue = objParameter.Email };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[IPADDRESS]]",
                ReplacementValue = GetIPAddress
                //(Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"])

            };
            bodyKeys.Add(keyValue);

            //EMAILREQUESTS
            var emailRequests = objParameter.MediaRequest.Split(',');
            if (emailRequests.Length > 0)
            {
                StringBuilder emailRequestsHtml = new StringBuilder();
                emailRequestsHtml.Append("<ul>");
                emailRequests.ToList().ForEach(delegate(string p) { emailRequestsHtml.Append("<li>" + p + "</li>"); });
                emailRequestsHtml.Append("</ul>");

                keyValue = new ReplaceKeyValueType { TemplateKey = "[[EMAILREQUESTS]]", ReplacementValue = emailRequestsHtml.ToString() };
                bodyKeys.Add(keyValue);
            }
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REFERENCENUMBER]]",
                ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            var subjectKeys = new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() + " " + objParameter.LastName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FIRSTNAME]]", ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[LASTNAME]]", ReplacementValue = objParameter.LastName.HtmlEncodeDecode() };
            subjectKeys.Add(keyValue);

            #endregion

            myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();
            myRequest.SubjectReplacementKeyValues = subjectKeys.ToArray();

            var myRes = EmailServiceManager.SendDistributedTemplatedEmail(myRequest);

            #region Mail to User

            if (myRes.StatusCode.Equals(0))
            {
                var myReq = new SendTemplatedEmailCallRequest
                {
                    EmailTemplateKey = EmailTemplates.NewsletterSignupConfirmation,
                    ToEmailAddresses = new[] { objParameter.Email },
                    CultureGroupId = GetCulture(Context.Language.Name)
                };

                var userMailBodyKeys = new List<ReplaceKeyValueType>();
                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[USERNAME]]",
                    ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() + " " + objParameter.LastName.HtmlEncodeDecode()
                };
                userMailBodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType { TemplateKey = "[[FIRSTNAME]]", ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() };
                userMailBodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType { TemplateKey = "[[LASTNAME]]", ReplacementValue = objParameter.LastName.HtmlEncodeDecode() };
                userMailBodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[REFERENCENUMBER]]",
                    ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
                };
                userMailBodyKeys.Add(keyValue);

                var userMailSubjectKeys = new List<ReplaceKeyValueType>();

                myReq.BodyReplacementKeyValues = userMailBodyKeys.ToArray();
                myReq.SubjectReplacementKeyValues = userMailSubjectKeys.ToArray();

                var myResp = EmailServiceManager.SendTemplatedEmail(myReq);
                if (myResp.StatusCode == 0)
                {
                    return true;
                }
                else
                {
                    Error.LogError(ServiceResponse + myRes.ResponseMessage);
                    return false;
                }
            }
            else
            {
                Error.LogError(ServiceResponse + myRes.ResponseMessage);
                return false;
            }

            #endregion
        }

        public static int ContactWebmasterSendMail(EmailActionsModel objParameter)
        {
            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.WebmasterDistributionList,
                DistributionListKey = "Contact_WebMaster_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region body replacement values

            var bodyKeys =
                new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType();
            keyValue.TemplateKey = "[[CATEGORY]]";
            keyValue.ReplacementValue = objParameter.CommentCategory.HtmlEncodeDecode();
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[URL]]", ReplacementValue = objParameter.ReferenceLink.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS]]",
                ReplacementValue = HttpUtility.HtmlEncode(objParameter.Comments)
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REPLYREQUESTED]]",
                ReplacementValue = objParameter.ReplyRequest
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[EMAIL]]", ReplacementValue = objParameter.Email };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[NAME]]", ReplacementValue = objParameter.Name.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber };
            bodyKeys.Add(keyValue);
            HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[BROWSER]]", ReplacementValue = browser.Type.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[VERSION]]", ReplacementValue = browser.Version.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            #endregion

            templatedReq.SubjectReplacementKeyValues = bodyKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
                throw new ArgumentException(emailCallResponse.ResponseMessage);
            }
            return emailCallResponse.StatusCode;
        }

        public static bool RequestPartsQuoteSendMail(RequestPartsQuoteModel objParameter)
        {
            bool response = false;
            var myRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.RequestPartQuoteDistributionList,
                DistributionListKey = "Request_Part_Quote_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region body replacement values -- all keys and values

            var bodyKeys = new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FIRSTNAME]]", ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[LASTNAME]]", ReplacementValue = objParameter.LastName.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[USERPHONE]]", ReplacementValue = objParameter.Phone.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[USEREMAIL]]", ReplacementValue = objParameter.Email.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SHIPPINGNAME]]",
                ReplacementValue = objParameter.Name.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SHIPPINGADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SHIPPINGADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SHIPPINGCITY]]",
                ReplacementValue = objParameter.City.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SHIPPINGSTATE]]",
                ReplacementValue = objParameter.State
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SHIPPINGPOSTAL]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SHIPPINGCOUNTRY]]",
                ReplacementValue = objParameter.Country
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PARTNUMBER_1]]",
                ReplacementValue = objParameter.Part1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[QUANTITY_1]]",
                ReplacementValue = objParameter.Quantity1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PROBLEM_1]]",
                ReplacementValue = objParameter.ProblemDescription1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_1]]",
                ReplacementValue = objParameter.ModelNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_1]]",
                ReplacementValue = objParameter.SerialNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FOREXCHANGE_1]]",
                ReplacementValue = objParameter.ForExchange1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FORRESALE_1]]",
                ReplacementValue = objParameter.ForResale1
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FOREXPORT_1]]",
                ReplacementValue = objParameter.ForExport1
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS_1]]",
                ReplacementValue = objParameter.Comment1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PARTNUMBER_2]]",
                ReplacementValue = objParameter.Part2
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[QUANTITY_2]]",
                ReplacementValue = objParameter.Quantity2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PROBLEM_2]]",
                ReplacementValue = objParameter.ProblemDescription2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_2]]",
                ReplacementValue = objParameter.ModelNumber2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_2]]",
                ReplacementValue = objParameter.SerialNumber2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FOREXCHANGE_2]]" };
            //if (rblForEx2.Items[0].Selected || rblForEx2.Items[1].Selected)
            //{
            //    keyValue.ReplacementValue = rblForEx2.SelectedItem.Text;
            //}
            //else
            //{
            keyValue.ReplacementValue = objParameter.ForExchange2.HtmlEncodeDecode();
            //}
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FORRESALE_2]]" };
            //if (rblForResale2.Items[0].Selected || rblForResale2.Items[1].Selected)
            //{
            //    keyValue.ReplacementValue = rblForResale2.SelectedItem.Text;
            //}
            //else
            //{
            keyValue.ReplacementValue = objParameter.ForResale2;
            //}
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FOREXPORT_2]]" };
            //if (rblForExp2.Items[0].Selected || rblForExp2.Items[1].Selected)
            //{
            //    keyValue.ReplacementValue = rblForExp2.SelectedItem.Text;
            //}
            //else
            //{
            keyValue.ReplacementValue = objParameter.ForExport2;
            //}
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS_2]]",
                ReplacementValue = objParameter.Comment2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PARTNUMBER_3]]",
                ReplacementValue = objParameter.Part3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[QUANTITY_3]]",
                ReplacementValue = objParameter.Quantity3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PROBLEM_3]]",
                ReplacementValue = objParameter.ProblemDescription3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_3]]",
                ReplacementValue = objParameter.ModelNumber3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_3]]",
                ReplacementValue = objParameter.SerialNumber3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FOREXCHANGE_3]]" };
            //if (rblForEx3.Items[0].Selected || rblForEx3.Items[1].Selected)
            //{
            //    keyValue.ReplacementValue = rblForEx3.SelectedItem.Text;
            //}
            //else
            //{
            keyValue.ReplacementValue = objParameter.ForExchange3.HtmlEncodeDecode();
            //}
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FORRESALE_3]]" };
            //if (rblForResale3.Items[0].Selected || rblForResale3.Items[1].Selected)
            //{
            //    keyValue.ReplacementValue = rblForResale3.SelectedItem.Text;
            //}
            //else
            //{
            keyValue.ReplacementValue = objParameter.ForResale3;
            //}
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FOREXPORT_3]]" };
            //if (rblForExp3.Items[0].Selected || rblForExp3.Items[1].Selected)
            //{
            //    keyValue.ReplacementValue = rblForExp3.SelectedItem.Text;
            //}
            //else
            //{
            keyValue.ReplacementValue = objParameter.ForExport3;
            //}
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS_3]]",
                ReplacementValue = objParameter.Comment3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERIP]]",
                ReplacementValue = GetIPAddress.HtmlEncodeDecode()
            };

            bodyKeys.Add(keyValue);


            var subjectKeys = new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber };
            subjectKeys.Add(keyValue);

            #endregion

            myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();
            myRequest.SubjectReplacementKeyValues = subjectKeys.ToArray();

            var myResponse = EmailServiceManager.SendDistributedTemplatedEmail(myRequest);

            if (myResponse.StatusCode.Equals(0))
            {

                var myReq = new SendTemplatedEmailCallRequest
                {
                    EmailTemplateKey = EmailTemplates.RequestPartQuoteToUser,
                    ToEmailAddresses = new[] { objParameter.Email },
                    CultureGroupId = GetCulture(Context.Language.Name)
                };


                var userMailBodyKeys = new List<ReplaceKeyValueType>();

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[FIRSTNAME]]",
                    ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
                };
                userMailBodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[LASTNAME]]",
                    ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
                };
                userMailBodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
                userMailBodyKeys.Add(keyValue);


                var userMailSubjectKeys = new List<ReplaceKeyValueType>();
                keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
                userMailSubjectKeys.Add(keyValue);

                myReq.BodyReplacementKeyValues = userMailBodyKeys.ToArray();
                myReq.SubjectReplacementKeyValues = userMailSubjectKeys.ToArray();

                var myResp = EmailServiceManager.SendTemplatedEmail(myReq);
                if (myResp.StatusCode.Equals(0))
                {
                    response = true;
                }
                else { Error.LogError(ServiceResponse + myResponse.ResponseMessage); }
            }
            else { Error.LogError(ServiceResponse + myResponse.ResponseMessage); }
            return response;
        }

        public static bool ProfessionServiceRequestAcknowledgeSendMail(EmailActionsModel objParameter)
        {
            var templatedReq = new SendTemplatedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.ProfessionalServiceTemplateKey,
                ToEmailAddresses = new[] { objParameter.Email },
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region subject replacement values

            var subjectKeys = new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SUBJECTPREFIX]]",
                ReplacementValue = objParameter.SubjectPrefix
            };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys = new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[JOBTITLE]]",
                ReplacementValue = objParameter.JobTitle.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = objParameter.Fax.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.City.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSTALCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYNAME]]",
                ReplacementValue = objParameter.Country.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[TOBECONTACTED]]",
                ReplacementValue = objParameter.ToBeContacted.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS]]",
                ReplacementValue = objParameter.Comments.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber };
            bodyKeys.Add(keyValue);

            #endregion

            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode == 0)
            {
                return true;
            }
            else
            {
                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
                throw new ArgumentException(emailCallResponse.ResponseMessage);
            }
        }

        public static bool ProfessionServiceRequestAcknowledgeSendAdminMail(EmailActionsModel objParameter)
        {
            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.ProfessionalServiceRequestDistributionList,
                DistributionListKey =
                    "Professional_Service_Request_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region subject replacement values

            var subjectKeys = new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SUBJECTPREFIX]]",
                ReplacementValue = objParameter.RequestType
            };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys = new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[JOBTITLE]]",
                ReplacementValue = objParameter.JobTitle.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = objParameter.Fax.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.City.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSTALCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYNAME]]",
                ReplacementValue = objParameter.Country.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[TOBECONTACTED]]",
                ReplacementValue = objParameter.ToBeContacted.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS]]",
                ReplacementValue = objParameter.Comments.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber };
            bodyKeys.Add(keyValue);

            #endregion

            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
                throw new ArgumentException(emailCallResponse.ResponseMessage);
            }
            return true;
        }

        public static bool RequestServiceQuoteSendMail(RequestServiceQuoteModel objParameter)
        {
            bool response = false;
            var myRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.RequestServiceQuoteDistributionList,
                DistributionListKey = "Request_Service_Quote_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region Body Replacement Values -- all keys and values

            var bodyKeys = new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERPHONE]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USEREMAIL]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = objParameter.Fax.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CITY]]",
                ReplacementValue = objParameter.City.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSTALCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRY]]",
                ReplacementValue = objParameter.Country
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_1]]",
                ReplacementValue = objParameter.ModelNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_1]]",
                ReplacementValue = objParameter.SerialNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REPAIRONLY_1]]",
                ReplacementValue = objParameter.RepairOnly1.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CALIBRATIONONLY_1]]",
                ReplacementValue = objParameter.CalibrationOnly1
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CALIBRATIONTESTDATA_1]]",
                ReplacementValue = objParameter.RepairAndCalibration1
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ONSITESERVICE_1]]",
                ReplacementValue = objParameter.OnsiteService1
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PROBLEM_1]]",
                ReplacementValue = objParameter.ProblemDescription1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_2]]",
                ReplacementValue = objParameter.ModelNumber2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_2]]",
                ReplacementValue = objParameter.SerialNumber2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REPAIRONLY_2]]",
                ReplacementValue = objParameter.RepairOnly2
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CALIBRATIONONLY_2]]",
                ReplacementValue = objParameter.CalibrationOnly2
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CALIBRATIONTESTDATA_2]]",
                ReplacementValue = objParameter.RepairAndCalibration2
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ONSITESERVICE_2]]",
                ReplacementValue = objParameter.OnsiteService2
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PROBLEM_2]]",
                ReplacementValue = objParameter.ProblemDescription2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_3]]",
                ReplacementValue = objParameter.ModelNumber3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_3]]",
                ReplacementValue = objParameter.SerialNumber3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REPAIRONLY_3]]",
                ReplacementValue = objParameter.RepairOnly3
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CALIBRATIONONLY_3]]",
                ReplacementValue = objParameter.CalibrationOnly3
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CALIBRATIONTESTDATA_3]]",
                ReplacementValue = objParameter.RepairAndCalibration3
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ONSITESERVICE_3]]",
                ReplacementValue = objParameter.OnsiteService3
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PROBLEM_3]]",
                ReplacementValue = objParameter.ProblemDescription3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERIP]]",
                ReplacementValue = GetIPAddress
            };
            bodyKeys.Add(keyValue);

            var subjectKeys = new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber };
            subjectKeys.Add(keyValue);

            #endregion

            myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();
            myRequest.SubjectReplacementKeyValues = subjectKeys.ToArray();

            var myRes = EmailServiceManager.SendDistributedTemplatedEmail(myRequest);

            #region Mail to User
            if (myRes.StatusCode.Equals(0))
            {
                var myReq = new SendTemplatedEmailCallRequest
                {
                    EmailTemplateKey = EmailTemplates.RequestServiceQuoteToUser,
                    ToEmailAddresses = new[] { objParameter.Email },
                    CultureGroupId = GetCulture(Context.Language.Name)
                };

                // Mail to Service Agent is Sent, now to user
                var userMailBodyKeys = new List<ReplaceKeyValueType>();

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[FIRSTNAME]]",
                    ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
                };
                userMailBodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[LASTNAME]]",
                    ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
                };
                userMailBodyKeys.Add(keyValue);


                keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber };
                userMailBodyKeys.Add(keyValue);


                var userMailSubjectKeys = new List<ReplaceKeyValueType>();
                keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber };
                userMailSubjectKeys.Add(keyValue);

                myReq.BodyReplacementKeyValues = userMailBodyKeys.ToArray();
                myReq.SubjectReplacementKeyValues = userMailSubjectKeys.ToArray();

                var myResp = EmailServiceManager.SendTemplatedEmail(myReq);
                if (myResp.StatusCode.Equals(0))
                {
                    response = true;
                }
                else
                {
                    Error.LogError(ServiceResponse + myRes.ResponseMessage);
                }
            }
            else
            {
                Error.LogError(ServiceResponse + myRes.ResponseMessage);
            }
            return response;
            #endregion
        }

        public static bool MasterUserGroupSignupSendMail(EmailActionsModel objParameter)
        {
            var response = false;
            var myRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.MasterUserGroupDistributionList,
                DistributionListKey = "Master_User_Group_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };


            ReplaceKeyValueType keyValue;

            #region body replacement values -- all keys and values

            var bodyKeys = new List<ReplaceKeyValueType>();

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[LASTNAME]]", ReplacementValue = objParameter.LastName.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[JOBTITLE]]", ReplacementValue = objParameter.JobTitle.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[COMPANY]]", ReplacementValue = objParameter.Company.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[PHONENUMBER]]", ReplacementValue = objParameter.Phone.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[ZIPCODE]]", ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.City.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[EMAILADDRESS]]", ReplacementValue = objParameter.Email };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType();
            keyValue.TemplateKey = "[[COUNTRY]]";
            keyValue.ReplacementValue = objParameter.Country;
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ANRITSUNEWS]]",
                ReplacementValue = objParameter.ReceiveFutureNews.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[NEWSLETTERHARDCOPY]]",
                ReplacementValue = objParameter.ReceiveHardcopy.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REFERENCENUMBER]]",
                ReplacementValue = objParameter.ReferenceNumber
            };
            bodyKeys.Add(keyValue);

            #endregion

            myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();
            var myResponse = EmailServiceManager.SendDistributedTemplatedEmail(myRequest);
            if (myResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + myResponse.ResponseMessage);
            }
            else
            {
                response = true;
            }
            return response;
        }

        public static bool RequestProductQuoteSendMail(RequestProductQuoteModel objParameter, string quotationFor)
        {
            var templatedReq = new SendTemplatedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.ProductQuoteTemplateKey,
                ToEmailAddresses = new[] { objParameter.Email },
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region subject replacement values

            var subjectKeys =
                new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys =
                new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber };
            bodyKeys.Add(keyValue);

            var quotationdetails = Quotations(objParameter, quotationFor);
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[QUOTATION]]", ReplacementValue = quotationdetails };
            bodyKeys.Add(keyValue);

            #endregion

            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode == 0)
            {
                return true;
            }
            Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
            return false;
        }

        public static bool RequestProductQuoteSendAdminMail(RequestProductQuoteModel objParameter, string quotationFor)
        {
            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.ProductQuoteRequestDistributionList,
                DistributionListKey = "Product_Quote_Request_DistributionList_" +
                                      GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region subject replacement values

            var subjectKeys =
                new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys =
                new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[DEPARTMENT]]",
                ReplacementValue = objParameter.Department.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);



            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.City.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSTALCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = objParameter.Fax.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYNAME]]",
                ReplacementValue = objParameter.Country.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYOFENDUSE]]",
                ReplacementValue = objParameter.CountryOfEndUse.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[INDUSTRYTYPE]]",
                ReplacementValue = objParameter.TypeOfIndustry.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[BUSINESSACTIVITY]]",
                ReplacementValue = string.Empty
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ANRITSUEQUIPMENT]]",
                ReplacementValue = string.Empty
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[RENTALOPTIONS]]",
                ReplacementValue = objParameter.RentalOptions.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[PRICEQUOTE]]" };
            keyValue.ReplacementValue = objParameter.PriceQuoteEquipment.HtmlEncodeDecode();
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[GSAPRICING]]" };
            keyValue.ReplacementValue = objParameter.Pricing.HtmlEncodeDecode();

            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[DODCONTRACT]]",
                ReplacementValue = objParameter.ContractNo.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[DODPROGRAM]]",
                ReplacementValue = objParameter.ProgramOrProject.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PURCHASE]]",
                ReplacementValue = objParameter.WhenLookingPurchase.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS]]",
                ReplacementValue = objParameter.Comments.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            var quotationdetails = Quotations(objParameter, quotationFor);
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[QUOTATION]]", ReplacementValue = quotationdetails };
            bodyKeys.Add(keyValue);

            #endregion

            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode != 0)
            {
                Log.SingleError(emailCallResponse.ResponseMessage, emailCallResponse);
                throw new ArgumentException(emailCallResponse.ResponseMessage);
            }
            return true;
        }

        public static bool RequestRetroOptionQuoteSendMails(RequestProductQuoteModel objParameter)
        {
            var myRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.RequestRetrofitOptionQuoteDistributionList,
                DistributionListKey =
                    "Request_Retrofit_Option_Quote_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region body replacement values -- all keys and values


            var bodyKeys = new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERPHONE]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USEREMAIL]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = objParameter.Fax.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CITY]]",
                ReplacementValue = objParameter.City.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ZIPCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRY]]",
                ReplacementValue = objParameter.Country.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYOFENDUSE]]",
                ReplacementValue = objParameter.CountryOfEndUse.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            //First Request Quote
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_1]]",
                ReplacementValue = objParameter.ModelNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_1]]",
                ReplacementValue = objParameter.SerialNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[OPTIONS_1]]",
                ReplacementValue = objParameter.Options1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS_1]]",
                ReplacementValue = objParameter.Comment1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            //Second Request Quote

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_2]]",
                ReplacementValue = objParameter.ModelNumber2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_2]]",
                ReplacementValue = objParameter.SerialNumber2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[OPTIONS_2]]",
                ReplacementValue = objParameter.Options2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS_2]]",
                ReplacementValue = objParameter.Comment2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            //Third Request Quote

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[MODELNUMBER_3]]",
                ReplacementValue = objParameter.ModelNumber3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER_3]]",
                ReplacementValue = objParameter.SerialNumber3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[OPTIONS_3]]",
                ReplacementValue = objParameter.Options3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS_3]]",
                ReplacementValue = objParameter.Comment3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[USERIP]]",
                ReplacementValue = GetIPAddress
            };
            bodyKeys.Add(keyValue);


            var subjectKeys = new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            subjectKeys.Add(keyValue);

            #endregion

            myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();
            myRequest.SubjectReplacementKeyValues = subjectKeys.ToArray();

            var myResponse = EmailServiceManager.SendDistributedTemplatedEmail(myRequest);

            if (myResponse.StatusCode.Equals(0))
            {
                var myReq = new SendTemplatedEmailCallRequest
                {
                    EmailTemplateKey = EmailTemplates.RequestRetrofitOptionQuoteToUser,
                    ToEmailAddresses = new[] { objParameter.Email },
                    CultureGroupId = GetCulture(Context.Language.Name)
                };

                var userMailBodyKeys = new List<ReplaceKeyValueType>();

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[FIRSTNAME]]",
                    ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
                };
                userMailBodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[LASTNAME]]",
                    ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
                };
                userMailBodyKeys.Add(keyValue);


                keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
                userMailBodyKeys.Add(keyValue);


                var userMailSubjectKeys = new List<ReplaceKeyValueType>();
                keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFNUM]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
                userMailSubjectKeys.Add(keyValue);

                myReq.BodyReplacementKeyValues = userMailBodyKeys.ToArray();
                myReq.SubjectReplacementKeyValues = userMailSubjectKeys.ToArray();

                var myResp = EmailServiceManager.SendTemplatedEmail(myReq);
                if (myResp.StatusCode != 0)
                {
                    Error.LogError(ServiceResponse + myResponse.ResponseMessage);
                }
                return myResp.StatusCode == 0;
            }
            Error.LogError(ServiceResponse + myResponse.ResponseMessage);
            return false;
            //throw new ArgumentException(myResponse.ResponseMessage);
        }

        public static bool InquiryFormSendUserMail(EmailActionsModel objParameter)
        {

            var myRequest = new SendTemplatedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.ResponseSupportEnquiry,
                ToEmailAddresses = new[] { objParameter.Email.HtmlEncodeDecode() },
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            var bodyKeys = new List<ReplaceKeyValueType>();

            var keyvalue = new ReplaceKeyValueType { TemplateKey = "[[FIRSTNAME]]", ReplacementValue = objParameter.FirstName.Trim().HtmlEncodeDecode() };
            bodyKeys.Add(keyvalue);
            keyvalue = new ReplaceKeyValueType { TemplateKey = "[[LASTNAME]]", ReplacementValue = objParameter.LastName.Trim().HtmlEncodeDecode() };
            bodyKeys.Add(keyvalue);
            keyvalue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REFNUM]]",
                ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyvalue);

            var subjectKeys = new List<ReplaceKeyValueType>();

            keyvalue = new ReplaceKeyValueType { TemplateKey = "[[REFNUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            subjectKeys.Add(keyvalue);

            myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();
            myRequest.SubjectReplacementKeyValues = subjectKeys.ToArray();
            var myResponse = EmailServiceManager.SendTemplatedEmail(myRequest);
            if (myResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + myResponse.ResponseMessage);
            }
            return myResponse.StatusCode == 0;
        }

        public static bool InquiryFormSendAdminMail(TrainingInquiryModel objParameter)
        {
            var myRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.SupportTrainingEnquiryDistributionList,
                DistributionListKey = "Support_Training_Enquiry_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region body replacement values -- all keys and values

            var bodyKeys = new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANY]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[JOBTITLE]]",
                ReplacementValue = objParameter.JobTitle.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = objParameter.Fax.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.City.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ZIPCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRY]]",
                ReplacementValue = objParameter.Country.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS]]",
                ReplacementValue = objParameter.Comments.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);
            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CLASS1]]",
                ReplacementValue = objParameter.Class1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CLASS2]]",
                ReplacementValue = objParameter.Class2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CLASS3]]",
                ReplacementValue = objParameter.Class3.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[DEPARTMENT]]",
                ReplacementValue = objParameter.Department.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            #endregion

            var subjectKeys = new List<ReplaceKeyValueType>();
            keyValue = new ReplaceKeyValueType();

            myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();
            myRequest.SubjectReplacementKeyValues = subjectKeys.ToArray();

            var myResponse = EmailServiceManager.SendDistributedTemplatedEmail(myRequest);
            if (myResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + myResponse.ResponseMessage);
            }
            return myResponse.StatusCode == 0;
        }

        public static bool RequestCustomerSupportSendMail(RequestCustomerSupportModel objParameter)
        {

            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.CustomerSupportDistributionList,
                DistributionListKey = "Request_Customer_Support_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region subject replacement values

            var subjectKeys = new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REFERENCENUMBER]]",
                ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys = new List<ReplaceKeyValueType>();

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SALUTATION]]",
                ReplacementValue = objParameter.Salutation.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSITION]]",
                ReplacementValue = objParameter.Position.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.City.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSTALCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = objParameter.Fax.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYNAME]]",
                ReplacementValue = objParameter.Country.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            /*keyValue = new ReplaceKeyValueType();
            keyValue.TemplateKey = "[[COMPANYTYPE]]";
            keyValue.ReplacementValue = (txtOtherType.Text.Length > 0 && ddlCompanyType.SelectedIndex <= 0 ? txtOtherType.Text.HtmlEncodeDecode() : ddlCompanyType.SelectedItem.Text);
            bodyKeys.Add(keyValue);
            */

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ASSISTANCE]]",
                ReplacementValue = objParameter.NeedAssistance.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PRODUCTTYPE]]",
                ReplacementValue = objParameter.ModelNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER]]",
                ReplacementValue = objParameter.SerialNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SOFTWAREVERSION]]",
                ReplacementValue = objParameter.SoftwareVersion.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PROBLEM]]",
                ReplacementValue = objParameter.DetailsOfProblemQuery.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SENDNEWS]]",
                ReplacementValue = objParameter.PleaseSendENews.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTEMAIL]]",
                ReplacementValue = objParameter.PreferenceEmail.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTPHONE]]",
                ReplacementValue = objParameter.PreferencePhone.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTPOST]]",
                ReplacementValue = objParameter.PreferencePost.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);


            #endregion

            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
            }
            return true;
        }

        public static bool RequestSurePrioritySupportSendMail(RequestSurePrioritySupportModel objParameter)
        {
            var response = false;
            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.SurePriorityDistributionList,
                DistributionListKey = "A_sure_Priority_Support_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            #region subject replacement values

            var subjectKeys = new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REFERENCENUMBER]]",
                ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys = new List<ReplaceKeyValueType>();

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SALUTATION]]",
                ReplacementValue = objParameter.Salutation.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSITION]]",
                ReplacementValue = objParameter.Position.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS1]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STREETADDRESS2]]",
                ReplacementValue = objParameter.Address2.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.City.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[STATE]]",
                ReplacementValue = objParameter.State.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSTALCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAXNUMBER]]",
                ReplacementValue = objParameter.Fax.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYNAME]]",
                ReplacementValue = objParameter.Country.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[QUOTATION]]",
                ReplacementValue = objParameter.CheckQuotation.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTME]]",
                ReplacementValue = objParameter.CheckContactMe.ToString()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REGISTRATIONCODE]]",
                ReplacementValue = objParameter.RegistrationCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PRODUCTTYPE]]",
                ReplacementValue = objParameter.ProductType.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SERIALNUMBER]]",
                ReplacementValue = objParameter.SerialNumber1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SOFTWAREVERSION]]",
                ReplacementValue = objParameter.SoftwareVersion.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYTYPE]]",
                ReplacementValue = string.Empty
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PROBLEM]]",
                ReplacementValue = objParameter.DetailsOfProblemQuery.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[SENDNEWS]]",
                ReplacementValue = objParameter.PleaseSendENews.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTEMAIL]]",
                ReplacementValue = objParameter.PreferenceEmail.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTPHONE]]",
                ReplacementValue = objParameter.PreferencePhone.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTPOST]]",
                ReplacementValue = objParameter.PreferencePost.ToString().HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            //var lblChkItems = new Label();
            //var lblChkInter = new Label();

            //keyValue = new ReplaceKeyValueType
            //{
            //    TemplateKey = "[[FIELDOPERATIONS]]",
            //    ReplacementValue = lblChkItems.Text
            //};
            //bodyKeys.Add(keyValue);

            //keyValue = new ReplaceKeyValueType
            //{
            //    TemplateKey = "[[TECHNOLOGYINTEREST]]",
            //    ReplacementValue = lblChkInter.Text
            //};
            //bodyKeys.Add(keyValue);


            #endregion

            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode.Equals(0))
            {
                response = true;
            }
            else
            {
                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
            }
            return response;
        }

        public static bool TalkToAnritsuSendUserMail(EmailActionsModel objParameter)
        {
            var templatedReq = new SendTemplatedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.TalkToAnritsuUser,
                ToEmailAddresses = new string[] { objParameter.Email.HtmlEncodeDecode() },
                CultureGroupId = GetCulture(Context.Language.Name)
            };
            var reqType = objParameter.RequestTypeText.HtmlEncodeDecode();

            var subjectKeys = new List<ReplaceKeyValueType>();

            var keyvalue = new ReplaceKeyValueType { TemplateKey = "[[SUBJECTPREFIX]]", ReplacementValue = reqType };
            subjectKeys.Add(keyvalue);

            keyvalue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            subjectKeys.Add(keyvalue);

            var bodyKeys = new List<ReplaceKeyValueType>();

            keyvalue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyvalue);

            keyvalue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyvalue);

            keyvalue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            bodyKeys.Add(keyvalue);

            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            var myResponse = EmailServiceManager.SendTemplatedEmail(templatedReq);
            if (myResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + myResponse.ResponseMessage);
            }
            return true;
        }

        public static bool TalkToAnritsuSendAdminMail(EmailActionsModel objParameter)
        {
            var response = false;
            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.TalkToAnritsuDistributionList
            };

            var selectedRequestValue = int.Parse(objParameter.RequestType);
            switch (selectedRequestValue)
            {
                case 1:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_ProdDemo_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
                case 2:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_PartsManual_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
                case 3:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_Comments_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
                case 5:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_Other_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
                case 9:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_Training_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
                case 10:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_SalesCall_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
                case 11:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_RequestTradeCompliance_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
                case 12:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_SubmitaTechnicalQuestion_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
                default:
                    templatedReq.DistributionListKey = "Talk_To_Anritsu_Other_DistributionList_" +
                                                       GetCurrentCulture.Substring(3, 2);
                    break;
            }





            // var selectedRequestValue = objParameter.RequestType;

            templatedReq.CultureGroupId = GetCulture(Context.Language.Name);

            //var reqType = GetPrefix(Convert.ToInt32(rdFaqs.SelectedValue));

            #region subject replacement values

            var subjectKeys = new List<ReplaceKeyValueType>();

            var keyValue = new ReplaceKeyValueType { TemplateKey = "[[FIRSTNAME]]", ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() };
            subjectKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType { TemplateKey = "[[LASTNAME]]", ReplacementValue = objParameter.LastName.HtmlEncodeDecode() };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[SUBJECTPREFIX]]", ReplacementValue = objParameter.RequestTypeText.HtmlEncodeDecode() };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys = new List<ReplaceKeyValueType>();

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[SUBJECTPREFIX]]", ReplacementValue = objParameter.RequestTypeText.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[REFERENCENUMBER]]", ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PRODUCTINTEREST]]",
                ReplacementValue = objParameter.ProductInterest.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS]]",
                ReplacementValue = objParameter.Comments.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMPANYNAME]]",
                ReplacementValue = objParameter.Company.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRYNAME]]",
                ReplacementValue = objParameter.Country
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[POSTALCODE]]",
                ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[TOBECONTACTED]]",
                ReplacementValue = objParameter.ToBeContacted.ToString()
            };

            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ADDRESS]]",
                ReplacementValue = objParameter.Address1.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            #endregion

            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
            }
            else response = true;
            return response;
        }

        public static int EmploymentFormSendMail(EmailActionsModel objParameter)
        {
            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.EmploymentTemplateKey,
                DistributionListKey = "Employment_Request_ToAdmin_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };
            Sitecore.Diagnostics.Log.Debug("Processing Employment Form Send eMail Method", templatedReq);
            #region body replacement values

            var bodyKeys =
                new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType();

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);


            keyValue = new ReplaceKeyValueType { TemplateKey = "[[ADDRESS1]]", ReplacementValue = objParameter.Address1.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[ADDRESS2]]", ReplacementValue = objParameter.Address2.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.City.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[STATE]]", ReplacementValue = objParameter.State.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRY]]",
                ReplacementValue = objParameter.Country.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[PHONENUMBER]]", ReplacementValue = objParameter.Phone.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[EMAIL]]", ReplacementValue = objParameter.Email.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[ZIPCODE]]", ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[NAME]]", ReplacementValue = objParameter.FirstName.HtmlEncodeDecode() + " " + objParameter.LastName.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
            keyValue = new ReplaceKeyValueType { TemplateKey = "[[BROWSER]]", ReplacementValue = browser.Type.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[VERSION]]", ReplacementValue = browser.Version.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[COMMENTS]]", ReplacementValue = objParameter.Comments.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[JOBTITLE]]", ReplacementValue = objParameter.JobTitle.HtmlEncodeDecode() };
            bodyKeys.Add(keyValue);

            #endregion

            var attachments = new List<FileUploadType>();
            var fullFilePath = objParameter.UploadFilePath;
            var fileExists = File.Exists(fullFilePath);
            if (fileExists)
            {
                try
                {
                    var file = new FileUploadType { FileName = Path.GetFileName(fullFilePath) };
                    byte[] fileData;
                    Sitecore.Diagnostics.Log.Debug("File is being attached to " + fullFilePath, templatedReq);
                    using (var fileStream = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read))
                    {
                        fileData = new byte[fileStream.Length];
                        fileStream.Read(fileData, 0, Convert.ToInt32(fileStream.Length));
                    }

                    file.FileBytes = fileData;
                    attachments.Add(file);
                    Sitecore.Diagnostics.Log.Debug("File has been attached to " + fullFilePath, templatedReq);
                }
                catch (Exception exception)
                {
                    Sitecore.Diagnostics.Log.Error("Unable to attach file : " + fullFilePath, templatedReq);
                }
            }

            if (attachments.Count > 0) { templatedReq.EmailAttachments = attachments.ToArray(); }
            templatedReq.SubjectReplacementKeyValues = bodyKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            //if (fileExists)
            //{
            //    System.IO.File.Delete(fullFilePath);
            //    Sitecore.Diagnostics.Log.Debug("File deleted :" + fullFilePath, templatedReq);
            //}
            //HttpContext.Current.Session["WffmUploadFilePath"] = null;
            //Sitecore.Diagnostics.Log.Debug("Email has been sent successfully - Response : " + emailCallResponse.StatusCode, templatedReq);

            if (emailCallResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
                throw new ArgumentException(emailCallResponse.ResponseMessage);
            }
            return emailCallResponse.StatusCode;
        }

        public static int SendApplicationFormMails(EmailActionsModel objParameter)
        {

            #region AdminMail

            var emailCallRequest = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.JobApplicationDistributionList,
                DistributionListKey = "New_JobApplication_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };

            var bodyKeys1 =
                new List<ReplaceKeyValueType>();
            ReplaceKeyValueType keyValue = null;

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAMEINLANG1]]",
                ReplacementValue = objParameter.Name.HtmlEncodeDecode()
            };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAMEINLANG1]]",
                ReplacementValue = objParameter.Surname.HtmlEncodeDecode()
            };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAMEINLANG2]]",
                ReplacementValue = objParameter.FirstName.HtmlEncodeDecode()
            };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAMEINLANG2]]",
                ReplacementValue = objParameter.LastName.HtmlEncodeDecode()
            };
            bodyKeys1.Add(keyValue);

            //keyValue = new ReplaceKeyValueType { TemplateKey = "[[SEX]]", ReplacementValue = jobObj.Sex };
            //bodyKeys1.Add(keyValue);

            //keyValue = new ReplaceKeyValueType { TemplateKey = "[[DATEOFBIRTH]]", ReplacementValue = jobObj.DateOfBirth };
            //bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[ADDRESS1]]", ReplacementValue = objParameter.Address1.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[ADDRESS2]]", ReplacementValue = objParameter.Address2.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CITY]]", ReplacementValue = objParameter.Localities.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[STATE]]", ReplacementValue = objParameter.State.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COUNTRY]]",
                ReplacementValue = objParameter.Country.HtmlEncodeDecode()
            };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[PHONENUMBER]]", ReplacementValue = objParameter.Phone.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[EMAIL]]", ReplacementValue = objParameter.Email.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[ZIPCODE]]", ReplacementValue = objParameter.PostalCode.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[UNIVERSITY]]", ReplacementValue = objParameter.SchoolName.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[FACULTY]]", ReplacementValue = objParameter.FacultyName.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[CELLPHONE]]", ReplacementValue = objParameter.MobileNumber.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[QUESTIONS]]", ReplacementValue = objParameter.Comments.HtmlEncodeDecode() };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType { TemplateKey = "[[SUBJECT]]", ReplacementValue = string.Empty };
            bodyKeys1.Add(keyValue);


            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[IPADDRESS]]",
                ReplacementValue = GetIPAddress
            };
            bodyKeys1.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[APPLICATIONNUMBER]]",
                ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
            };
            bodyKeys1.Add(keyValue);

            emailCallRequest.BodyReplacementKeyValues = bodyKeys1.ToArray();
            var emailCallRes = EmailServiceManager.SendDistributedTemplatedEmail(emailCallRequest);

            #endregion

            if (emailCallRes.StatusCode.Equals(0))
            {
                //user mail
                var templatedReq = new SendTemplatedEmailCallRequest
                {
                    EmailTemplateKey = EmailTemplates.JobApplicationAcknowledgement,
                    ToEmailAddresses = new[] { objParameter.Email },
                    CultureGroupId = GetCulture(Context.Language.Name)
                };

                #region body replacement values

                var bodyKeys =
                    new List<ReplaceKeyValueType>();
                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[NAME]]",
                    ReplacementValue = objParameter.Surname.HtmlEncodeDecode() + " " + objParameter.Name.HtmlEncodeDecode()
                };
                bodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[APPLICATIONNUMBER]]",
                    ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
                };
                bodyKeys.Add(keyValue);

                #endregion

                templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
                var emailCallResponse = EmailServiceManager.SendTemplatedEmail(templatedReq);
                if (emailCallResponse.StatusCode != 0)
                {
                    Error.LogError(ServiceResponse + emailCallRes.ResponseMessage);
                    throw new ArgumentException(emailCallResponse.ResponseMessage);
                }
                return emailCallResponse.StatusCode;
            }
            Error.LogError(ServiceResponse + emailCallRes.ResponseMessage);
            return emailCallRes.StatusCode;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Faq")]
        public static int FaqSurveyEmail(EmailActionsModel objParameter)
        {

            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = EmailTemplates.FAQSurveyDistributionList,
                DistributionListKey = "FAQ_Survey_DistributionList_" + GetCurrentCulture.Substring(3, 2),
                CultureGroupId = GetCulture(Context.Language.Name)
            };


            #region subject replacement values

            var subjectKeys = new List<ReplaceKeyValueType>();


            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAQID]]",
                ReplacementValue = Convert.ToString(objParameter.FaqId)
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[NAME]]",
                ReplacementValue = objParameter.Name.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REFERENCENUMBER]]",
                ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
            };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys = new List<ReplaceKeyValueType>();

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAQID]]",
                ReplacementValue = Convert.ToString(objParameter.FaqId)
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FAQANSWERED]]",
                ReplacementValue = objParameter.FaqAnswered.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[RATEOFQUALITY]]",
                ReplacementValue = objParameter.RateOfQuality.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FEEDBACK]]",
                ReplacementValue = HttpUtility.HtmlEncode(objParameter.Feedback)
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[NAME]]",
                ReplacementValue = objParameter.Name.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PHONENUMBER]]",
                ReplacementValue = objParameter.Phone.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAILADDRESS]]",
                ReplacementValue = objParameter.Email.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REFERENCENUMBER]]",
                ReplacementValue = objParameter.ReferenceNumber.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[QUESTION]]",
                ReplacementValue = HttpUtility.UrlDecode(objParameter.Question)
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ANSWER]]",
                ReplacementValue = HttpUtility.UrlDecode(objParameter.Answer)
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[PAGEURL]]",
                ReplacementValue = objParameter.PageUrl.HtmlEncodeDecode()
            };
            bodyKeys.Add(keyValue);

            #endregion

            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();


            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);

            if (emailCallResponse.StatusCode != 0)
            {
                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
                throw new ArgumentException(emailCallResponse.ResponseMessage);
            }
            return emailCallResponse.StatusCode;
        }

        public static string GetCurrentCulture
        {
            get
            {
                return Context.Language.Name;
            }
        }

        public static int GetCulture(string code)
        {
            int cultureCode = 1;
            switch (code)
            {

                case "en-US":
                    cultureCode = 1;
                    break;

                case "ja-JP":
                    cultureCode = 2;
                    break;
                case "en-GB":
                    cultureCode = 3;
                    break;

                case "en-AU":
                    cultureCode = 4;
                    break;

                case "zh-CN":
                    cultureCode = 6;
                    break;

                case "ko-KR":
                    cultureCode = 7;
                    break;

                case "zh-TW":
                    cultureCode = 10;
                    break;
                case "ru-RU":
                    cultureCode = 14;
                    break;
                case "en-IN":
                    cultureCode = 15;
                    break;

            }
            return cultureCode;
        }

        public static string GetIPAddress
        {
            get
            {
                var ipAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null ? HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] : HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                return ipAddress.ToString();
            }
        }

        public static string HtmlEncode(this string value)
        {
            return HttpUtility.HtmlEncode(value);
        }

        public static string HtmlEncodeDecode(this string value)
        {
            return HttpUtility.HtmlDecode(HttpUtility.HtmlEncode(value));
        }

        public static string Quotations(RequestProductQuoteModel model, string quotationFor)
        {
            DataTable _productsDt = new DataTable();
            _productsDt.Columns.Add("ProductModel");
            _productsDt.Columns.Add("Quantity");
            _productsDt.Columns.Add("FrequencyRange");
            _productsDt.Columns.Add("Options");
            _productsDt.Rows.Add(model.ModelNumber1, model.Quantity1, model.FrequencyRange1, model.Options1);
            _productsDt.Rows.Add(model.ModelNumber2, model.Quantity2, model.FrequencyRange2, model.Options2);
            _productsDt.Rows.Add(model.ModelNumber3, model.Quantity3, model.FrequencyRange3, model.Options3);
            var body = new StringBuilder();


            if (quotationFor.ToUpperInvariant().Equals("JA-JP") || quotationFor.ToUpperInvariant().Equals("ZH-TW"))
            {
                foreach (DataRow dr in _productsDt.Rows)
                {
                    body.Append(
                        string.Format(
                            "<tr><td><font face=\"VERDANA\" SIZE=\"1\" >{0}</font></td><td><font face=\"VERDANA\" SIZE=\"1\" >{1}</font></td><td><font face=\"VERDANA\" SIZE=\"1\" >{2}</font></td></tr>",
                            dr["ProductModel"], dr["FrequencyRange"], dr["Quantity"]));
                }
            }
            else
            {
                foreach (DataRow dr in _productsDt.Rows)
                {
                    body.Append(
                        string.Format(
                            "<tr><td><font face=\"VERDANA\" SIZE=\"1\" >{0}</font></td><td><font face=\"VERDANA\" SIZE=\"1\" >{1}</font></td><td><font face=\"VERDANA\" SIZE=\"1\" >{2}</font></td><td><font face=\"VERDANA\" SIZE=\"1\" >{3}</font></td></tr>",
                            dr["ProductModel"], dr["FrequencyRange"], dr["Options"], dr["Quantity"]));
                }
            }

            return body.ToString();
        }
    }
}
