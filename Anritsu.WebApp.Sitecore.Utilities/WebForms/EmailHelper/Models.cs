﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.EmailHelper.Models
{
    public class EmailActionsModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string ReferenceNumber { get; set; }

        //Request Service Quote properties
        public string ModelNumber1 { get; set; }
        public string SerialNumber1 { get; set; }
        public string ServiceType1 { get; set; }
        public string ProblemDescription1 { get; set; }
        public string ModelNumber2 { get; set; }
        public string SerialNumber2 { get; set; }
        public string ServiceType2 { get; set; }
        public string ProblemDescription2 { get; set; }
        public string ModelNumber3 { get; set; }
        public string SerialNumber3 { get; set; }
        public string ServiceType3 { get; set; }
        public string ProblemDescription3 { get; set; }
        public string Select { get; set; }

        //Professional Service
        public string RequestType { get; set; }
        public string RequestTypeText { get; set; }
        public string Comments { get; set; }
        public string SubjectPrefix { get; set; }
        public string ToBeContacted { get; set; }

        //Web-Master
        public string ReferenceLink { get; set; }
        public string CommentCategory { get; set; }
        public string ReplyRequest { get; set; }
        public string Name { get; set; }

        //Master user group signup
        public bool ReceiveFutureNews { get; set; }
        public bool ReceiveHardcopy { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }

        //Talk to anritsu
        public string ProductInterest { get; set; }

        //Application form
        public string Surname { get; set; }
        public string SchoolName { get; set; }
        public string FacultyName { get; set; }
        public string Localities { get; set; }
        public string MobileNumber { get; set; }

        public string UploadFilePath { get; set; }
        public string MediaRequest { get; set; }

        //FAQ
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Faq")]
        public string FaqAnswered { get; set; }
        public string RateOfQuality { get; set; }
        public string Feedback { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings")]
        public string PageUrl { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Faq")]
        public int FaqId { get; set; }
    }

    public class RequestPartsQuoteModel : EmailActionsModel
    {
        public string Part1 { get; set; }
        public string Part2 { get; set; }
        public string Part3 { get; set; }

        public string Quantity1 { get; set; }
        public string Quantity2 { get; set; }
        public string Quantity3 { get; set; }

        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }

        public string ForExchange1 { get; set; }
        public string ForExchange2 { get; set; }
        public string ForExchange3 { get; set; }

        public string ForResale1 { get; set; }
        public string ForResale2 { get; set; }
        public string ForResale3 { get; set; }

        public string ForExport1 { get; set; }
        public string ForExport2 { get; set; }
        public string ForExport3 { get; set; }

    }

    public class RequestServiceQuoteModel : EmailActionsModel
    {
        public string RepairOnly1 { get; set; }
        public string CalibrationOnly1 { get; set; }
        public string RepairAndCalibration1 { get; set; }
        public string OnsiteService1 { get; set; }

        public string RepairOnly2 { get; set; }
        public string CalibrationOnly2 { get; set; }
        public string RepairAndCalibration2 { get; set; }
        public string OnsiteService2 { get; set; }

        public string RepairOnly3 { get; set; }
        public string CalibrationOnly3 { get; set; }
        public string RepairAndCalibration3 { get; set; }
        public string OnsiteService3 { get; set; }
    }

    public class RequestProductQuoteModel : RequestPartsQuoteModel
    {
        public string CountryOfEndUse { get; set; }
        public string TypeOfIndustry { get; set; }

        public string FrequencyRange1 { get; set; }
        public string Options1 { get; set; }
        public string FrequencyRange2 { get; set; }
        public string Options2 { get; set; }
        public string FrequencyRange3 { get; set; }
        public string Options3 { get; set; }

        public string Pricing { get; set; }
        public string ContractNo { get; set; }
        public string ProgramOrProject { get; set; }
        public string PriceQuoteEquipment { get; set; }
        public string WhenLookingPurchase { get; set; }
        public string RentalOptions { get; set; }

    }

    public class RequestCustomerSupportModel : EmailActionsModel
    {
        public string Salutation { get; set; }
        public string Position { get; set; }
        public string NeedAssistance { get; set; }
        public string SoftwareVersion { get; set; }
        public string DetailsOfProblemQuery { get; set; }
        public string PleaseSendENews { get; set; }
        public string ContactPreference { get; set; }

        public string PreferenceEmail { get; set; }
        public string PreferencePhone { get; set; }
        public string PreferencePost { get; set; }
    }

    public class RequestSurePrioritySupportModel : RequestCustomerSupportModel
    {
        public string CheckQuotation { get; set; }
        public string CheckContactMe { get; set; }
        public string RegistrationCode { get; set; }
        public string ProductType { get; set; }
    }
    //Training
    public class TrainingInquiryModel:EmailActionsModel
    {
        public string Class1 { get; set; }
        public string Class2 { get; set; }
        public string Class3 { get; set; }
    }
    public class Country
    {
        public string CountryName { get; set; }
        public string Region { get; set; }
        public string ContextRegion { get; set; }
    }

    public static class Constants
    {
        public const string SearchFilterFolder = "{5F38D725-4DF7-4522-A668-8DF8F56BD873}";
    }

    public class UserInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string CountryId { get; set; }
        public string Department { get; set; }
    }
}
