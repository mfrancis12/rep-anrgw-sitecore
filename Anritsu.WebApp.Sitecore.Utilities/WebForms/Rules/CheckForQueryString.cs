﻿using Anritsu.Library.SSOSDK.SSOApiClient;
using Sitecore.Rules;
using Sitecore.Rules.Conditions;
using System;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.Rules
{
    public class CheckForQueryString<T> : WhenCondition<T> where T : RuleContext
    {
        /// <summary>
        /// Gets or sets the QueryString Key.
        /// </summary>
        /// <value>QueryString Key.</value>
        public string QueryStringKey { get; set; }

        /// <summary>Executes the specified rule context.</summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns><c>True</c>, if the condition succeeds, otherwise <c>false</c>.</returns>
        protected override bool Execute(T ruleContext)
        {
            try
            {
                //This condition can be used only if query string has to be checked
                //Since the url field in 'web-master' form has to filled all the times irrespective of the query string
                //return true always. and check for query string in action class
                //if (System.Web.HttpContext.Current.Request.QueryString[QueryStringKey] != null)
                //{
                return true;
                //}
                //return false;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Error : " + ex.Message, this);
                return false;
            }
        }
    }
}
