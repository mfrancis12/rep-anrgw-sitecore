﻿using Anritsu.Library.SSOSDK.SSOApiClient;
using Sitecore.Rules;
using Sitecore.Rules.Conditions;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Anritsu.WebApp.SitecoreUtilities.UserSession;
using Sitecore.Configuration;
using Sitecore.Shell.Framework.Commands;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.Rules
{
    public class CheckForSession<T> : WhenCondition<T> where T : RuleContext
    {
        /// <summary>
        /// Gets or sets the Session Key.
        /// </summary>
        /// <value>Session Key.</value>
        public string SessionKey { get; set; }

        /// <summary>Executes the specified rule context.</summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns><c>True</c>, if the condition succeeds, otherwise <c>false</c>.</returns>
        protected override bool Execute(T ruleContext)
        {
            try
            {
                //set session
                UserSessionRefresh.SetSession();
                if (System.Web.HttpContext.Current.Session["User"] != null)
                {
                    GWSsoUserType ssoWsUser = (GWSsoUserType)HttpContext.Current.Session["User"];
                    if (ssoWsUser.GetType().GetProperties().Single(x => x.Name.ToUpperInvariant() == SessionKey.ToUpperInvariant()) != null)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Error on session - " + ex.Message, this);
                return false;
            }
        }
    }
  
}

