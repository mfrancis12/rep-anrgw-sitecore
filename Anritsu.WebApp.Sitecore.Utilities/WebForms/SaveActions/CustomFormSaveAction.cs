﻿using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.SitecoreUtilities.WebForms.EmailHelper.Models;
using Sitecore;
using Sitecore.Data;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.SaveActions
{
    public class CustomFormSaveAction : ISaveAction
    {
        readonly Dictionary<string, string> userInfo = new Dictionary<string, string>();
        /// <param name="fields">The fields of the form.</param>
        /// <param name="data">The custom data.</param>
        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            var honeypotFlag = HttpContext.Current.Request.Params.AllKeys.ToList().FirstOrDefault(x => x.EndsWith("txtUserInteractionIdentifier", new StringComparison() { }));
            if (HttpContext.Current.Request.Params[honeypotFlag] == "green")
                throw new InvalidOperationException("Bots are not allowed");

            Database db = Sitecore.Configuration.Factory.GetDatabase("web");
            Collection<AdaptedControlResult> formFields = new Collection<AdaptedControlResult>();
            if (!fields.Any()) return;
            try
            {
                foreach (AdaptedControlResult item in fields.ToList())
                {
                    formFields.Add(item);
                }
                string userInformation = CollectUserInfo(formFields.ToList());
                string additionalInfo = CollectAdditionalInfo(formFields.ToList());
                string formName = db.GetItem(formid).Name;
                DynamicForm.SaveSitecoreDynamiceFormDetails(formName, formid.ToString(),
                    GetCultureIdByCultureCode(Context.Language.Name), userInformation, additionalInfo);
            }
            catch (Exception exception)
            {
                Sitecore.Diagnostics.Log.Error(exception.Message, this);
                throw new ArgumentException(exception.Message);
            }
        }

        private string CollectUserInfo(List<AdaptedControlResult> fields)
        {
            UserInfo uInfo = new UserInfo();
            var swTe = new StringWriter();
            using (var xtwUserInfo = new XmlTextWriter(swTe))
            {
                xtwUserInfo.WriteStartElement("UserInfo");
                uInfo.GetType().GetProperties().ToList().ForEach(delegate(PropertyInfo p)
                {
                    string fieldValue = GetFieldValue(fields, p.Name);
                    if (!userInfo.ContainsKey(p.Name.ToUpperInvariant())) userInfo.Add(p.Name.ToUpperInvariant(), fieldValue);
                    xtwUserInfo.WriteElementString(p.Name, fieldValue ?? string.Empty);
                });
                xtwUserInfo.WriteElementString("IPAddress",
                   (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]));

                xtwUserInfo.WriteEndElement();

                xtwUserInfo.Flush();
                xtwUserInfo.Close();
            }
            swTe.Flush();

            return swTe.ToString();
        }

        private string CollectAdditionalInfo(List<AdaptedControlResult> fields)
        {
            var swTe = new StringWriter();
            using (var xtwUserInfo = new XmlTextWriter(swTe))
            {
                xtwUserInfo.WriteStartElement("Items");
                foreach (AdaptedControlResult field in fields.Where(field => !userInfo.ContainsKey(field.FieldName.Replace(" ", "").Replace("_","").ToUpperInvariant())))
                {
                    xtwUserInfo.WriteStartElement("Item");
                    xtwUserInfo.WriteElementString("FieldName", field.FieldName);
                    xtwUserInfo.WriteElementString("FieldValue", HttpUtility.HtmlEncode(field.Value));
                    xtwUserInfo.WriteEndElement();
                }
                xtwUserInfo.WriteEndElement();
            }
            swTe.Flush();
            return swTe.ToString();
        }

        private string GetFieldValue(List<AdaptedControlResult> fields, string fieldName)
        {
            var fieldValue = string.Empty;
            var formField = fields.FirstOrDefault(x => x.FieldName.Replace(" ", "").Replace("_", "").Equals(fieldName, StringComparison.OrdinalIgnoreCase));
            if (formField != null)
            {
                var field = fields.FirstOrDefault(x => x.FieldID == formField.FieldID);
                if (field != null)
                    fieldValue = HttpUtility.HtmlEncode(field.Value);
            }
            return fieldValue;
        }

        /// <summary>
        /// Method to fetch CultureID based on culture code
        /// </summary>
        /// <param name="cultureCode"></param>
        /// <returns>CultureID</returns>
        public int GetCultureIdByCultureCode(string cultureCode)
        {
            int cultureId = 0;
            switch (cultureCode)
            {
                case "en-US":
                    cultureId = 1;
                    break;
                case "ja-JP":
                    cultureId = 2;
                    break;
                case "en-GB":
                    cultureId = 3;
                    break;
                case "en-AU":
                    cultureId = 4;
                    break;
                case "zh-CN":
                    cultureId = 6;
                    break;
                case "ko-KR":
                    cultureId = 7;
                    break;
                case "zh-TW":
                    cultureId = 10;
                    break;
                case "ru-RU":
                    cultureId = 14;
                    break;
                case "en-IN":
                    cultureId = 15;
                    break;
                case "en":
                    cultureId = 16;
                    break;
                default:
                    cultureId = 0;
                    break;
            }
            return cultureId;
        }
    }
}
