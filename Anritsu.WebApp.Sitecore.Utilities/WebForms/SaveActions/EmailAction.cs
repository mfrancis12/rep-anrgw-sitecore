﻿using Anritsu.WebApp.SitecoreUtilities.WebForms.EmailHelper;
using Anritsu.WebApp.SitecoreUtilities.WebForms.EmailHelper.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore;
using Convert = System.Convert;
using Anritsu.WebApp.SitecoreUtilities.ContentSearch;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.SaveActions
{
    public class EmailAction : ISaveAction
    {
        //select all /sitecore/content/globl/Mail Templates that end with -USER-EMAIL
        public string UserEmailTemplateId { get; set; }

        //select all /sitecore/content/globl/Mail Templates that end with -ADMIN-EMAIL
        public string AdminEmailTemplateId { get; set; }

        //free form text to end DistributionListKey?
        public string DistributionListKey { get; set; }

        /// <property>This is string which stores the filed mappings in : separeated forms</property>
        /// <info> These fields names will be set in save action in Sitecore edtior window</info>
        public string MappedFields { get; set; }
        /// <property>This is string which stores the table name into which the form info has to be stored.</property>
        /// <info> This table name will be set in save action in Sitecore edtior window</info>
        public string MappedTable { get; set; }

        /// <param name="fields">The fields of the form.</param>
        /// <param name="data">The custom data.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            try
            {
                var honeypotFlag = HttpContext.Current.Request.Params.AllKeys.ToList().FirstOrDefault(x => x.EndsWith("txtUserInteractionIdentifier", new StringComparison() { }));
                if (HttpContext.Current.Request.Params[honeypotFlag] == "green")
                    throw new InvalidOperationException("Bots are not allowed");
                //Environment.FailFast("Bots are not allowed", null);

                string emailTemplate = string.Empty;
                EmailActionsModel model = new EmailActionsModel();
                List<Item> lstItems = new List<Item>();

                List<AdaptedControlResult> lstResults = new List<AdaptedControlResult>();
                lstResults = fields.ToList();
                Collection<AdaptedControlResult> collResults = new Collection<AdaptedControlResult>();
                if (fields.Count() != 0)
                {
                    foreach (AdaptedControlResult item in lstResults)
                    {
                        collResults.Add(item);
                    }

                    switch (formid.ToString())
                    {
                        case "{EC2D1255-CEA3-4A32-A4BB-DFAB6EEC98DA}":
                            NewsletterSignupConfirmation(collResults);
                            break;
                        case "{928E743E-DF6C-4095-8D96-BC6281F286A1}":
                            ContactWebmasterRequest(collResults);
                            break;

                        case "{B320671E-A118-44A2-AC2C-98F6F4BE9DEE}":
                            RequestPartsQuote(collResults);
                            break;

                        case "{D37FBB29-05BD-4132-89F9-1DB7A509DD9B}":
                            ProfessionServiceRequestAcknowledge(collResults);
                            break;

                        case "{90E989DA-519F-47F6-8229-4BD282DF62AA}":
                            RequestServiceQuoteAdmin(collResults);
                            break;

                        case "{7FA538FA-C01E-419F-B44D-0EAF107C96E1}":
                            MasterUserGroupSignup(collResults);
                            break;

                        case "{7C55C187-4337-4EA9-9910-600CDC09DBF1}":
                            RequestProductQuote(collResults);
                            break;

                        case "{9C93074A-6D46-40FB-A459-E6BE9118DC50}":
                            RequestProductQuoteJapan(collResults);
                            break;

                        case "{05B05B7B-B22B-4473-A661-057BF3EDB18F}":
                            RequestRetroOptionQuote(collResults);
                            break;

                        case "{66AC389A-5779-42FF-A965-A1E988418F3C}":
                            InquiryForm(collResults);
                            break;

                        case "{5FF32ECE-F712-4372-90C8-1F51E4883867}":
                            RequestCustomerSupport(collResults);
                            break;

                        case "{66C4ED9E-4059-476C-9CDC-F9FC8CBAC52F}":
                            RequestSurePriority(collResults);
                            break;

                        case "{86C749BC-CD50-4406-B7FA-71AF49CD6CE4}":
                            TalkToAnritsu(collResults);
                            break;

                        case "{C6884934-37B4-4B2E-A3F2-0DE1B19AA70D}":
                            ApplicationForm(collResults);
                            break;

                        case "{AFE660AC-6253-4B3A-874E-7FE48DB1CAE6}":
                            EmploymentForm(collResults);
                            break;

                        case "{82868190-7662-45C8-A14F-C99D5E52DA5B}":
                            FaqSurveyEmail(collResults);
                            break;

                        default:
                            SendEmail(collResults);
                            break;
                    }
                }
            }
            catch (Exception exception)
            {
                Sitecore.Diagnostics.Log.Error(exception.Message + " - form ID:" + formid, this);
                throw new ArgumentException(exception.Message);
            }
        }

        #region Methods

        private string GetFieldValue(List<AdaptedControlResult> lstResults, string fieldName)
        {
            var fieldValue = "";
            var field = lstResults.FirstOrDefault(x => x.FieldName == fieldName);
            if (field != null)
                fieldValue = field.Value.HtmlEncodeDecode();

            return fieldValue;
        }

        private string GetFieldText(List<AdaptedControlResult> lstResults, string fieldName)
        {
            var fieldText = "";
            var field = lstResults.FirstOrDefault(x => x.FieldName == fieldName);
            if (field != null)
                fieldText = field.Parameters;

            return fieldText;
        }

        private string GetFieldValueByID(Collection<AdaptedControlResult> lstResults, string fieldId)
        {
            var fieldValue = "";
            var field = lstResults.FirstOrDefault(x => x.FieldID == fieldId);
            if (field != null)
                fieldValue = field.Value.HtmlEncodeDecode();

            return fieldValue; ;
        }

        private string GetFieldType(string fieldID)
        {
            string fieldType = string.Empty;
            ReferenceField refField = Sitecore.Context.Database.GetItem(fieldID).Fields["Field Link"];
            if (refField == null) return fieldType;
            Item item = refField.TargetItem;
            if (item == null) return fieldType;
            string val = item.Fields["Class"].Value;
            string[] arr = val.Split('.');
            fieldType = arr.Length > 0 ? arr[arr.Length - 1] : val;
            return fieldType;
        }

        public RequestServiceQuoteModel GetModelValues(Collection<AdaptedControlResult> listFields)
        {
            RequestServiceQuoteModel model = new RequestServiceQuoteModel
            {
                FirstName = GetFieldValue(listFields.ToList(), "First Name"),
                LastName = GetFieldValue(listFields.ToList(), "Last Name"),
                Company = GetFieldValue(listFields.ToList(), "Company"),
                Address1 = GetFieldValue(listFields.ToList(), "Address1"),
                Address2 = GetFieldValue(listFields.ToList(), "Address2"),
                City = GetFieldValue(listFields.ToList(), "City"),
                Phone = GetFieldValue(listFields.ToList(), "Phone"),
                PostalCode = GetFieldValue(listFields.ToList(), "Postal Code"),
                Country = GetFieldValue(listFields.ToList(), "Country")
            };
            //model.ReferenceNumber = "";




            model.State = GetState(listFields, model.Country);


            //model.State = GetFieldValue(listFields.ToList(), "State");

            model.Email = GetFieldValue(listFields.ToList(), "Email");

            model.Fax = GetFieldValue(listFields.ToList(), "Fax");

            model.ModelNumber1 = GetFieldValue(listFields.ToList(), "Model Number");
            model.SerialNumber1 = GetFieldValue(listFields.ToList(), "Serial Number");
            model.ServiceType1 = GetFieldValue(listFields.ToList(), "Service Type");
            model.ProblemDescription1 = GetFieldValue(listFields.ToList(), "Problem Description");

            model.ModelNumber2 = GetFieldValue(listFields.ToList(), "Model Number_2");
            model.SerialNumber2 = GetFieldValue(listFields.ToList(), "Serial Number_2");
            model.ServiceType2 = GetFieldValue(listFields.ToList(), "Service Type_2");
            model.ProblemDescription2 = GetFieldValue(listFields.ToList(), "Problem Description_2");

            model.ModelNumber3 = GetFieldValue(listFields.ToList(), "Model Number_3");
            model.SerialNumber3 = GetFieldValue(listFields.ToList(), "Serial Number_3");
            model.ServiceType3 = GetFieldValue(listFields.ToList(), "Service Type_3");
            model.ProblemDescription3 = GetFieldValue(listFields.ToList(), "Problem Description_3");


            return model;
        }

        private string GetState(Collection<AdaptedControlResult> listFields, string Country)
        {
            var state = string.Empty;
            var fields = listFields.Where(x => x.FieldName == "State");
            List<ListItem> countryAndState = (from ctrl in fields let fieldType = GetFieldType(ctrl.FieldID) let fieldVal = ctrl.Value where fieldType.ToUpperInvariant().Equals(Constants.CustomSingleLineText.ToUpperInvariant()) || fieldType.ToUpperInvariant().Equals(Constants.CustomDropList.ToUpperInvariant()) || fieldType.ToUpperInvariant().Equals(Constants.CustomDropListJapanStates.ToUpperInvariant()) select new ListItem { Text = fieldType, Value = fieldVal }).ToList();

            if (countryAndState.Count <= 0) return state;
            ListItem listItem;
            switch (Country)
            {
                case "US":
                    {
                        listItem = countryAndState.FirstOrDefault(x => x.Text.ToUpperInvariant().Equals(Constants.CustomDropList.ToUpperInvariant()));

                        break;
                    }
                case "JP":
                    {
                        listItem = countryAndState.FirstOrDefault(x => x.Text.ToUpperInvariant().Equals(Constants.CustomDropListJapanStates.ToUpperInvariant()));
                        break;
                    }
                default:
                    {
                        listItem = countryAndState.FirstOrDefault(x => x.Text.ToUpperInvariant().Equals(Constants.CustomSingleLineText.ToUpperInvariant()));
                        break;
                    }
            }
            return listItem == null ? state : listItem.Value;
        }

        public Collection<Item> GetItemsByTemplate(string emailTemplate)
        {
            List<Item> listItems = new List<Item>(); Item[] Items;
            Collection<Item> collItems = new Collection<Item>();

            //3chillies AW - old code
            Items = Sitecore.Context.Database.SelectItems("fast:/sitecore/content//*[@@templateid = '{B54368F7-B760-4E9F-ACCA-DC34D6A711C3}']");
            listItems = Items.Where(x => x.Fields["Templatekey"].Value == emailTemplate).ToList();

            //3chillies AW - new code
            //listItems = new EmailTemplateRepository().GetEmailTemplates(emailTemplate, new ID("{B54368F7-B760-4E9F-ACCA-DC34D6A711C3}")).ToList<Item>();

            foreach (var item in listItems)
            {
                collItems.Add(item);
            }
            return collItems;
        }

        public void SendEmail(Collection<AdaptedControlResult> listFields)
        {
            var fields = GetFields(listFields);

            fields.Add("DistributionListKey", DistributionListKey);

            if (UserEmailTemplateId != null)
            {
                if (!UserEmailTemplateId.Equals("Select", StringComparison.OrdinalIgnoreCase))
                {
                    var templateFields = GetTemplateFields(ID.Parse(UserEmailTemplateId));
                    foreach (var field in templateFields.Where(field => !fields.ContainsKey(field.Key)))
                    {
                        fields.Add(field.Key, field.Value.HtmlEncodeDecode());
                    }
                }
            }

            if (AdminEmailTemplateId != null)
            {
                if (!AdminEmailTemplateId.Equals("Select", StringComparison.OrdinalIgnoreCase))
                {
                    var templateFields = GetTemplateFields(ID.Parse(AdminEmailTemplateId));
                    foreach (var field in templateFields.Where(field => !fields.ContainsKey(field.Key)))
                    {
                        fields.Add(field.Key, field.Value.HtmlEncodeDecode());
                    }
                }
            }

            if (!EmailManager.SendMail(fields))
            {
                Sitecore.Diagnostics.Log.Error("Error while sending email:", this);
                throw new ArgumentException("Error while sending email");
            }
        }

        private Dictionary<string, string> GetTemplateFields(ID templateItemId)
        {
            var results = new Dictionary<string, string>();

            var emailTemplateItem = Context.Database.GetItem(templateItemId);
            if (emailTemplateItem == null) return results;

            var templateType = emailTemplateItem.Name.EndsWith("-USER-EMAIL", StringComparison.OrdinalIgnoreCase) ? "USER" : "ADMIN";
            results.Add(templateType + "-EmailTemplateKey", emailTemplateItem.Name);

            return results;
        }

        public static Dictionary<string, string> GetFields(IEnumerable<AdaptedControlResult> listFields)
        {
            var fieldsList = listFields as IList<AdaptedControlResult> ?? listFields.ToList();

            var results = fieldsList.ToDictionary(field => field.FieldName, field => field.Value);

            var mediaField = fieldsList.FirstOrDefault(x => x.FieldName == "Select");

            if (mediaField != null)
            {
                results.Add("MediaRequest", mediaField.Parameters);
            }

            if (HttpContext.Current.Session["ReferenceNumber"] != null)
            {
                results.Add("ReferenceNumber", HttpContext.Current.Session["ReferenceNumber"].ToString());
            }

            if (results.ContainsKey("First Name") && results.ContainsKey("Last Name"))
            {
                results.Add("Username", results["First Name"] + " " + results["Last Name"]);
            }

            return results;
        }

        #endregion

        #region Templates

        public void NewsletterSignupConfirmation(Collection<AdaptedControlResult> listFields)
        {
            EmailActionsModel model = new EmailActionsModel(); string emailTemplate = string.Empty; Item[] Items;
            List<Item> lstItems = new List<Item>(); Item resultItem;
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.Company = GetFieldValue(listFields.ToList(), "Company");
            var field = listFields.Where(x => x.FieldName == "Select").FirstOrDefault();
            if (field != null)
                model.MediaRequest = field.Parameters;


            //Items = Sitecore.Context.Database.SelectItems("fast:/sitecore/content//*[@@templateid = '{B54368F7-B760-4E9F-ACCA-DC34D6A711C3}']");
            lstItems = GetItemsByTemplate("NEWSLETTER-SIGNUP-CONFIRMATION").ToList();

            if (lstItems.Count > 0)
            {
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    String bodyTemplate = resultItem["BodyTemplate"].ToString();
                    if (HttpContext.Current.Session["ReferenceNumber"] != null)
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REFERENCENUMBER]]", HttpContext.Current.Session["ReferenceNumber"].ToString());  //changed templates key -PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE    
                        model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
                    }
                    string fromEmailAddress = resultItem["FromEmailAddress"].ToString();
                    string subject = resultItem["SubjectTemplate"].ToString();
                    string toEmailAddress = model.Email;
                    bool response = EmailManager.NewsletterSignupConfirmationSendMail(model);
                    if (response)
                    {
                        HttpContext.Current.Session.Remove("ReferenceNumber");
                    }
                }
            }
        }

        public void ContactWebmasterRequest(Collection<AdaptedControlResult> listFields)
        {
            EmailActionsModel model = new EmailActionsModel(); string emailTemplate = string.Empty;
            List<Item> lstItems = new List<Item>(); Item resultItem;
            model.Name = GetFieldValue(listFields.ToList(), "Name");
            model.Email = GetFieldValue(listFields.ToList(), "Email Address");
            model.ReplyRequest = GetFieldValue(listFields.ToList(), "Reply Requested");
            model.ReferenceLink = GetFieldValue(listFields.ToList(), "Url");
            model.CommentCategory = GetFieldValue(listFields.ToList(), "Select a category that best describes your comment");
            model.Phone = GetFieldValue(listFields.ToList(), "Day Time Phone Number");
            model.Comments = GetFieldValue(listFields.ToList(), "Comments Maximum 2000 characters");
            //Items = Sitecore.Context.Database.SelectItems("fast:/sitecore/content//*[@@templateid = '{B54368F7-B760-4E9F-ACCA-DC34D6A711C3}']");
            lstItems = GetItemsByTemplate("CONTACT-WEBMASTER-REQUEST").ToList();

            if (lstItems.Count > 0)
            {
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    string subject = resultItem["SubjectTemplate"].ToString();
                    String bodyTemplate = resultItem["BodyTemplate"].ToString();
                    if (HttpContext.Current.Session["ReferenceNumber"] != null)
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REFERENCENUMBER]]", HttpContext.Current.Session["ReferenceNumber"].ToString());  //changed templates key -PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE    
                        model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
                    }

                    if (model.ReplyRequest == "1")
                    {
                        model.ReplyRequest = "True";
                    }
                    else if (model.ReplyRequest == "0")
                    {
                        model.ReplyRequest = "False";
                    }
                    System.Web.HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
                    string fromEmailAddress = resultItem["FromEmailAddress"].ToString();
                    string toEmailAddress = model.Email;
                    //SendMail(subject, bodyTemplate, toEmailAddress, fromEmailAddress);
                    EmailManager.ContactWebmasterSendMail(model);
                    HttpContext.Current.Session.Remove("ReferenceNumber");
                }
            }
        }

        public void RequestPartsQuote(Collection<AdaptedControlResult> listFields)
        {
            RequestPartsQuoteModel model = new RequestPartsQuoteModel(); string emailTemplate = string.Empty; Item[] Items;
            List<Item> lstItems = new List<Item>(); Item resultItem;
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.Name = GetFieldValue(listFields.ToList(), "Name");
            model.Company = GetFieldValueByID(listFields, "{F2805EFA-D33D-4E47-B5EF-DE14ADAA229E}");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.PostalCode = GetFieldValue(listFields.ToList(), "ZipPostal Code");
            model.Country = GetFieldValueByID(listFields, "{64F3E2E8-09BA-4DD6-A478-04EC7452126C}");

            model.Part1 = GetFieldValueByID(listFields, "{7CDEC8D8-5122-4732-98AB-E1777126F44C}");
            model.Quantity1 = GetFieldValueByID(listFields, "{5782D0AE-48A3-488A-9289-2153B2DFF7F1}");
            model.ProblemDescription1 = GetFieldValueByID(listFields, "{259B02CF-47BA-465C-8673-90DD359564C3}");
            model.ModelNumber1 = GetFieldValueByID(listFields, "{E6296387-227B-43B6-AD05-5E755A93CB53}");
            model.SerialNumber1 = GetFieldValueByID(listFields, "{DAAD90DB-A8FC-47CB-B604-4900A352EE24}");
            model.Comment1 = GetFieldValueByID(listFields, "{FC5C689B-4873-448D-ADFB-ACF1971E7FEF}");
            model.ForExchange1 = GetFieldValueByID(listFields, "{D6BE9BB8-DC6E-4218-92EB-59649A545F5F}");
            model.ForResale1 = GetFieldValueByID(listFields, "{61C08431-FE24-4B89-8CA6-DA15838140D9}");
            model.ForExport1 = GetFieldValueByID(listFields, "{28F1F7BE-4610-4928-B4A4-636C999A8440}");

            model.Part2 = GetFieldValueByID(listFields, "{30643F34-15B8-4172-B764-F6831E9C93D9}");
            model.Quantity2 = GetFieldValueByID(listFields, "{F6A51BB5-DE9B-4B79-9E85-D1CCF42D7A6A}");
            model.ProblemDescription2 = GetFieldValueByID(listFields, "{4D2489E3-19C2-4C05-A4DD-1D7F56B405D6}");
            model.ModelNumber2 = GetFieldValueByID(listFields, "{D27739C1-E070-42AF-80B2-1FC1C207E64D}");
            model.SerialNumber2 = GetFieldValueByID(listFields, "{958D8C44-E123-45CC-948F-EBAF81D18115}");
            model.Comment2 = GetFieldValueByID(listFields, "{D4B6A757-F90C-4FB1-AD2C-F79B17930F73}");
            model.ForExchange2 = GetFieldValueByID(listFields, "{2A73279E-207E-401D-888F-419ABDA20CAD}");
            model.ForResale2 = GetFieldValueByID(listFields, "{16A34D73-4900-4E3A-B75E-335B81625290}");
            model.ForExport2 = GetFieldValueByID(listFields, "{83BBF0E6-C7D9-4DA8-A3EB-49AD60FD39A2}");

            model.Part3 = GetFieldValueByID(listFields, "{65EA5547-F040-416F-9C9F-B606F38B79F7}");
            model.Quantity3 = GetFieldValueByID(listFields, "{A092D2D5-97AB-48DE-99C4-FBB7EA48F8D6}");
            model.ProblemDescription3 = GetFieldValueByID(listFields, "{F1B3D2F5-7CA6-402A-A4B6-74EDBF579646}");
            model.ModelNumber3 = GetFieldValueByID(listFields, "{C9436CD5-6AF6-4786-8232-0F3DEE4B457E}");
            model.SerialNumber3 = GetFieldValueByID(listFields, "{3F6CFF81-E5BF-4A15-8455-3D8FA7566C5B}");
            model.Comment3 = GetFieldValueByID(listFields, "{9328ED90-7BCE-4C5A-BEB3-FB4113EC67AE}");
            model.ForExchange3 = GetFieldValueByID(listFields, "{03009AD9-90CF-43DA-AF50-6BCD6AF1B7AD}");
            model.ForResale3 = GetFieldValueByID(listFields, "{50D56634-CABA-47FB-9D6F-381D4A130279}");
            model.ForExport3 = GetFieldValueByID(listFields, "{10036ACB-2C9E-4F8B-84E3-86319743E69F}");

            if (model.Country == "US")
            {
                model.State = listFields.Where(x => x.FieldID == "{1EBF80AB-2E80-4832-ACF8-88AE5392B252}").FirstOrDefault().Value;
            }
            else if (model.Country == "JP")
            {
                model.State = listFields.Where(x => x.FieldID == "{FE488844-D1DF-4A3A-BBB9-BFFA53047BB0}").FirstOrDefault().Value;
            }
            else
            {
                model.State = listFields.Where(x => x.FieldID == "{135E5BA6-5591-452F-A380-BE9B24F5F9D6}").FirstOrDefault().Value;
            }
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            //Items = Sitecore.Context.Database.SelectItems("fast:/sitecore/content//*[@@templateid = '{B54368F7-B760-4E9F-ACCA-DC34D6A711C3}']");
            lstItems = GetItemsByTemplate("REQUEST-PART-QUOTE-TO-USER").ToList();

            if (lstItems.Count > 0)
            {
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    String bodyTemplate = resultItem["BodyTemplate"].ToString();
                    if (HttpContext.Current.Session["ReferenceNumber"] != null)
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REFNUM]]", HttpContext.Current.Session["ReferenceNumber"].ToString());  //changed templates key -PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE    
                        model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
                    }
                    string fromEmailAddress = resultItem["FromEmailAddress"].ToString();
                    string subject = resultItem["SubjectTemplate"].ToString();
                    string toEmailAddress = model.Email;
                    //SendMail(subject, bodyTemplate, toEmailAddress, fromEmailAddress);
                    bool response = EmailManager.RequestPartsQuoteSendMail(model);
                    if (response)
                    {
                        HttpContext.Current.Session.Remove("ReferenceNumber");
                    }
                }
            }
        }

        public void ProfessionServiceRequestAcknowledge(Collection<AdaptedControlResult> listFields)
        {
            EmailActionsModel model = new EmailActionsModel(); string emailTemplate = string.Empty; Item[] Items;
            List<Item> lstItems = new List<Item>(); Item resultItem;
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.JobTitle = GetFieldValue(listFields.ToList(), "Job Title");
            model.Company = GetFieldValue(listFields.ToList(), "Company");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.Fax = GetFieldValue(listFields.ToList(), "Fax");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal Code");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.ToBeContacted = GetFieldValue(listFields.ToList(), "Yes I would like to be contacted");
            model.Comments = GetFieldValue(listFields.ToList(), "Questions or Comments");
            if (model.Country == "US")
            {
                model.State = listFields.Where(x => x.FieldID == "{85AE69B1-7120-4FFA-B79E-D992A485F158}").FirstOrDefault().Value;
            }
            else if (model.Country == "JP")
            {
                model.State = listFields.Where(x => x.FieldID == "{648FD442-DE3B-4443-9429-7F21C9BFD6EB}").FirstOrDefault().Value;
            }
            else
            {
                model.State = listFields.Where(x => x.FieldID == "{F5E5B444-EAA0-4199-931F-B522B101F49D}").FirstOrDefault().Value;
            }
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.RequestType = GetFieldValue(listFields.ToList(), "Request Type");

            //Items = Sitecore.Context.Database.SelectItems("fast:/sitecore/content//*[@@templateid = '{B54368F7-B760-4E9F-ACCA-DC34D6A711C3}']");
            lstItems = GetItemsByTemplate("PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE").ToList();

            if (lstItems.Count > 0)
            {
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    string subject = resultItem["SubjectTemplate"].ToString();
                    String bodyTemplate = resultItem["BodyTemplate"].ToString();
                    if (HttpContext.Current.Session["ReferenceNumber"] != null)
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REFERENCENUMBER]]", HttpContext.Current.Session["ReferenceNumber"].ToString());  //changed templates key -PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE    
                        subject = subject.Replace("[[REFERENCENUMBER]]", HttpContext.Current.Session["ReferenceNumber"].ToString());
                        model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
                    }


                    switch (model.RequestType)
                    {
                        case "7":
                            //Request Fiber Optic Characterization;
                            subject = subject.Replace("[[SUBJECTPREFIX]]", "Request Fiber Optic Characterization");
                            model.SubjectPrefix = "Request Fiber Optic Characterization";
                            break;
                        case "8":
                            //Request Line Sweep Interpretation;
                            subject = subject.Replace("[[SUBJECTPREFIX]]", "Request Line Sweep Interpretation");
                            model.SubjectPrefix = "Request Line Sweep Interpretation";
                            break;
                        case "5":
                            subject = subject.Replace("[[SUBJECTPREFIX]]", "Others");
                            model.SubjectPrefix = "Others";
                            break;

                        default:
                            break;
                    }

                    string fromEmailAddress = resultItem["FromEmailAddress"].ToString();

                    string toEmailAddress = model.Email;
                    //SendMail(subject, bodyTemplate, toEmailAddress, fromEmailAddress);
                    bool response = EmailManager.ProfessionServiceRequestAcknowledgeSendAdminMail(model);
                    if (response)
                    {
                        var userMailResponse = EmailManager.ProfessionServiceRequestAcknowledgeSendMail(model);
                        if (userMailResponse)
                        {
                            HttpContext.Current.Session.Remove("ReferenceNumber");
                        }
                    }

                }
            }
        }

        public void RequestServiceQuoteAdmin(Collection<AdaptedControlResult> listFields)
        {
            RequestServiceQuoteModel model = new RequestServiceQuoteModel(); string emailTemplate = string.Empty; Item[] Items;
            List<Item> lstItems = new List<Item>(); Item resultItem;
            //Request Servie Quote

            model = GetModelValues(listFields);

            //Items = Sitecore.Context.Database.SelectItems("fast:/sitecore/content//*[@@templateid = '{B54368F7-B760-4E9F-ACCA-DC34D6A711C3}']");
            lstItems = GetItemsByTemplate("REQUEST-SERVICE-QUOTE").ToList();
            
            if (lstItems.Count > 0)
            {
                
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    string subject = resultItem["SubjectTemplate"].ToString();
                    String bodyTemplate = resultItem["BodyTemplate"].ToString();
                    if (HttpContext.Current.Session["ReferenceNumber"] != null)
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REFNUM]]", HttpContext.Current.Session["ReferenceNumber"].ToString());  //changed templates key -PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE    
                        subject = subject.Replace("[[REFNUM]]", HttpContext.Current.Session["ReferenceNumber"].ToString());
                        model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
                    }
                    
                    if (!string.IsNullOrEmpty(model.ServiceType1))
                    {
                        //bodyTemplate = bodyTemplate.Replace("[[SERVICETYPE_1]]", model.ServiceType1);
                        //resultItem["BodyTemplate"]                     

                        string text = HttpUtility.HtmlDecode(model.ServiceType1);
                        
                        if (text.StartsWith("<item>", StringComparison.OrdinalIgnoreCase))
                        {
                            text = text.Replace("<item>", string.Empty).Replace("</item>", ",");
                            text = text.Substring(0, text.Length - 1);
                        }
                        var str = text.Split(',');
                       
                        if (str.Any())
                        {
                            foreach (var item in str)
                            {
                                
                                if (item == "1")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[REPAIRONLY_1]]", "YES");
                                    model.RepairOnly1 = "YES";
                                }
                                else if (item == "2")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONONLY_1]]", "YES");
                                    model.CalibrationOnly1 = "YES";
                                }
                                else if (item == "3")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONTESTDATA_1]]", "YES");
                                    model.RepairAndCalibration1 = "YES";
                                }
                                else if (item == "4")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[ONSITESERVICE_1]]", "YES");
                                    model.OnsiteService1 = "YES";
                                }
                            }
                            if (bodyTemplate.Contains("[[REPAIRONLY_1]]"))
                            {
                                bodyTemplate = bodyTemplate.Replace("[[REPAIRONLY_1]]", "NO");
                                model.RepairOnly1 = "NO";
                            }
                            if (bodyTemplate.Contains("[[CALIBRATIONONLY_1]]"))
                            {
                                bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONONLY_1]]", "NO");
                                model.CalibrationOnly1 = "NO";
                            }
                            if (bodyTemplate.Contains("[[CALIBRATIONTESTDATA_1]]"))
                            {
                                bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONTESTDATA_1]]", "NO");
                                model.RepairAndCalibration1 = "NO";
                            }
                            if (bodyTemplate.Contains("[[ONSITESERVICE_1]]"))
                            {
                                bodyTemplate = bodyTemplate.Replace("[[ONSITESERVICE_1]]", "NO");
                                model.OnsiteService1 = "NO";
                            }
                        }

                        //text;

                    }
                    bodyTemplate = bodyTemplate.Replace("[[PROBLEM_1]]", model.ProblemDescription1);

                    bodyTemplate = bodyTemplate.Replace("[[MODELNUMBER_2]]", model.ModelNumber2);
                    bodyTemplate = bodyTemplate.Replace("[[SERIALNUMBER_2]]", model.SerialNumber2);

                    if (!String.IsNullOrEmpty(model.ServiceType2))
                    {
                        string text = HttpUtility.HtmlDecode(model.ServiceType2);
                        if (text.StartsWith("<item>", StringComparison.OrdinalIgnoreCase))
                        {
                            text = text.Replace("<item>", string.Empty).Replace("</item>", ",");
                            text = text.Substring(0, text.Length - 1);
                        }
                        string[] str = text.Split(',');
                        if (str.Any())
                        {
                            foreach (var item in str)
                            {
                                if (item == "1")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[REPAIRONLY_2]]", "YES");
                                    model.RepairOnly2 = "YES";
                                }
                                else if (item == "2")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONONLY_2]]", "YES");
                                    model.CalibrationOnly2 = "YES";
                                }
                                else if (item == "3")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONTESTDATA_2]]", "YES");
                                    model.RepairAndCalibration2 = "YES";
                                }
                                else if (item == "4")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[ONSITESERVICE_2]]", "YES");
                                    model.OnsiteService2 = "YES";
                                }
                            }
                        }
                    }
                    if (bodyTemplate.Contains("[[REPAIRONLY_2]]"))
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REPAIRONLY_2]]", "NO");
                        model.RepairOnly2 = "NO";
                    }
                    if (bodyTemplate.Contains("[[CALIBRATIONONLY_2]]"))
                    {
                        bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONONLY_2]]", "NO");
                        model.CalibrationOnly2 = "NO";
                    }
                    if (bodyTemplate.Contains("[[CALIBRATIONTESTDATA_2]]"))
                    {
                        bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONTESTDATA_2]]", "NO");
                        model.RepairAndCalibration2 = "NO";
                    }
                    if (bodyTemplate.Contains("[[ONSITESERVICE_2]]"))
                    {
                        bodyTemplate = bodyTemplate.Replace("[[ONSITESERVICE_2]]", "NO");
                        model.OnsiteService2 = "NO";
                    }


                    bodyTemplate = bodyTemplate.Replace("[[PROBLEM_2]]", model.ProblemDescription2);

                    bodyTemplate = bodyTemplate.Replace("[[MODELNUMBER_3]]", model.ModelNumber3);
                    bodyTemplate = bodyTemplate.Replace("[[SERIALNUMBER_3]]", model.SerialNumber3);

                    if (!String.IsNullOrEmpty(model.ServiceType3))
                    {
                        string serviceTypes = HttpUtility.HtmlDecode(model.ServiceType3);
                        if (serviceTypes.StartsWith("<item>", StringComparison.OrdinalIgnoreCase))
                        {
                            serviceTypes = serviceTypes.Replace("<item>", string.Empty).Replace("</item>", ",");
                            serviceTypes = serviceTypes.Substring(0, serviceTypes.Length - 1);
                        }
                        string[] types = serviceTypes.Split(',');
                        if (types.Any())
                        {
                            foreach (var item in types)
                            {
                                if (item == "1")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[REPAIRONLY_3]]", "YES");
                                    model.RepairOnly3 = "YES";
                                }
                                else if (item == "2")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONONLY_3]]", "YES");
                                    model.CalibrationOnly3 = "YES";
                                }
                                else if (item == "3")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONTESTDATA_3]]", "YES");
                                    model.RepairAndCalibration3 = "YES";
                                }
                                else if (item == "4")
                                {
                                    bodyTemplate = bodyTemplate.Replace("[[ONSITESERVICE_3]]", "YES");
                                    model.OnsiteService3 = "YES";
                                }
                            }
                        }
                    }
                    if (bodyTemplate.Contains("[[REPAIRONLY_3]]"))
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REPAIRONLY_3]]", "NO");
                        model.RepairOnly3 = "NO";
                    }
                    if (bodyTemplate.Contains("[[CALIBRATIONONLY_3]]"))
                    {
                        bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONONLY_3]]", "NO");
                        model.CalibrationOnly3 = "NO";
                    }
                    if (bodyTemplate.Contains("[[CALIBRATIONTESTDATA_3]]"))
                    {
                        bodyTemplate = bodyTemplate.Replace("[[CALIBRATIONTESTDATA_3]]", "NO");
                        model.RepairAndCalibration3 = "NO";
                    }
                    if (bodyTemplate.Contains("[[ONSITESERVICE_3]]"))
                    {
                        bodyTemplate = bodyTemplate.Replace("[[ONSITESERVICE_3]]", "NO");
                        model.OnsiteService3 = "NO";
                    }

                    bodyTemplate = bodyTemplate.Replace("[[PROBLEM_3]]", model.ProblemDescription3);

                    string fromEmailAddress = resultItem["FromEmailAddress"].ToString();

                    string toEmailAddress = model.Email;
                    bool response = EmailManager.RequestServiceQuoteSendMail(model);
                  
                    if (response)
                    {
                        HttpContext.Current.Session.Remove("ReferenceNumber");
                    }
                }
            }
           
        }

        public void MasterUserGroupSignup(Collection<AdaptedControlResult> listFields)
        {
            EmailActionsModel model = new EmailActionsModel();
            List<Item> lstItems = new List<Item>();
            Item resultItem;
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Company = GetFieldValue(listFields.ToList(), "Company");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.JobTitle = GetFieldValue(listFields.ToList(), "Job Title");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal Code");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.Country = GetFieldText(listFields.ToList(), "Country");

            if (model.Country == "United States")
            {
                var adaptedControlResult = listFields.FirstOrDefault(x => x.FieldID == "{46B28FF3-1A76-490A-A7AF-1ABD01917470}");
                if (adaptedControlResult != null) model.State = adaptedControlResult.Parameters;
            }
            else if (model.Country == "Japan")
            {
                var adaptedControlResult = listFields.FirstOrDefault(x => x.FieldID == "{0B0D3FD7-0F97-4534-A4BE-A57BBCE47A75}");
                if (adaptedControlResult != null) model.State = adaptedControlResult.Parameters;
            }
            else
            {
                var adaptedControlResult = listFields.FirstOrDefault(x => x.FieldID == "{EC9EE295-2B22-4B75-96E9-458A4FBAF48C}");
                if (adaptedControlResult != null) model.State = adaptedControlResult.Parameters;
            }

            model.ReceiveFutureNews = ((GetFieldValue(listFields.ToList(), "Yes Id like to receive future news and offers from Anritsu via email") == "1") ? true : false);
            model.ReceiveHardcopy = ((GetFieldValue(listFields.ToList(), "Yes Id also like to receive a hard copy of the newsletter in the mail") == "1") ? true : false);

            lstItems = GetItemsByTemplate("MASTER-USER-GROUP-ADMIN").ToList();

            if (lstItems.Count > 0)
            {
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    if (HttpContext.Current.Session["ReferenceNumber"] != null)
                    {
                        model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
                    }
                    var response = EmailManager.MasterUserGroupSignupSendMail(model);
                    if (response)
                    {
                        HttpContext.Current.Session.Remove("ReferenceNumber");
                    }
                }
            }
        }

        public void RequestProductQuote(Collection<AdaptedControlResult> listFields)
        {
            RequestProductQuoteModel model = new RequestProductQuoteModel(); string emailTemplate = string.Empty;
            List<Item> lstItems = new List<Item>(); Item resultItem;
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Company = GetFieldValue(listFields.ToList(), "Company Name");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal Code");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.Country = GetFieldText(listFields.ToList(), "Country");
            model.Fax = GetFieldValue(listFields.ToList(), "Fax number");
            model.CountryOfEndUse = GetFieldText(listFields.ToList(), "Country of End Use");
            model.TypeOfIndustry = GetFieldValue(listFields.ToList(), "Type of Industry");
            model.Department = GetFieldValue(listFields.ToList(), "Department");

            if (model.Country == "United States")
            {
                var field = listFields.Where(x => x.FieldID == "{45AED1FD-0255-455D-9010-E807942CD617}").FirstOrDefault();
                model.State = field != null ? field.Parameters : string.Empty;
            }
            else if (model.Country == "Japan")
            {
                var field = listFields.Where(x => x.FieldID == "{902CB02F-CB62-4FB9-B9D4-A53047EC9953}").FirstOrDefault();
                model.State = field != null ? field.Parameters : string.Empty;
            }
            else
            {
                var field = listFields.Where(x => x.FieldID == "{5DE7AE9C-0CC5-406E-AB11-8C9C092EA490}").FirstOrDefault();
                model.State = field != null ? field.Value : string.Empty;
            }

            model.ModelNumber1 = GetFieldValue(listFields.ToList(), "Select productmodel name");
            if (model.ModelNumber1 != "Select" && string.IsNullOrWhiteSpace(GetFieldValue(listFields.ToList(), "or enter productmodel name")) == false)
            {
                model.ModelNumber1 = model.ModelNumber1 + " / " + GetFieldValue(listFields.ToList(), "or enter productmodel name"); 
            }
            else if(string.IsNullOrWhiteSpace(GetFieldValue(listFields.ToList(), "or enter productmodel name"))== false)
            {
                model.ModelNumber1 =  GetFieldValue(listFields.ToList(), "or enter productmodel name");
            }
           
            model.FrequencyRange1 = GetFieldValue(listFields.ToList(), "Frequency Range min to max");
            model.Options1 = GetFieldValue(listFields.ToList(), "Options");
            model.Quantity1 = GetFieldValue(listFields.ToList(), "Approximate quantity");

            model.ModelNumber2 = GetFieldValue(listFields.ToList(), "Select productmodel name2");
           

            if (model.ModelNumber2 != "Select" && string.IsNullOrWhiteSpace(GetFieldValue(listFields.ToList(), "or enter productmodel name2"))==false)
            {
                model.ModelNumber2 = model.ModelNumber2 + " / " + GetFieldValue(listFields.ToList(), "or enter productmodel name2"); 
            }
            else if(string.IsNullOrWhiteSpace(GetFieldValue(listFields.ToList(), "or enter productmodel name2")) == false)
            {
                model.ModelNumber2 = GetFieldValue(listFields.ToList(), "or enter productmodel name2");
            }

            model.FrequencyRange2 = GetFieldValue(listFields.ToList(), "Frequency Range min to max2");
            model.Options2 = GetFieldValue(listFields.ToList(), "Options2");
            model.Quantity2 = GetFieldValue(listFields.ToList(), "Approximate quantity2");


            model.ModelNumber3 = GetFieldValue(listFields.ToList(), "Select productmodel name3");
           
            if (model.ModelNumber3 != "Select" && string.IsNullOrWhiteSpace(GetFieldValue(listFields.ToList(), "or enter productmodel name3")) ==false)
            {
                model.ModelNumber3 = model.ModelNumber3 + " / " + GetFieldValue(listFields.ToList(), "or enter productmodel name3");
            }
            else if(string.IsNullOrWhiteSpace(GetFieldValue(listFields.ToList(), "or enter productmodel name3")) == false)
            {
                model.ModelNumber3 = GetFieldValue(listFields.ToList(), "or enter productmodel name3");
            }
            model.FrequencyRange3 = GetFieldValue(listFields.ToList(), "Frequency Range min to max3");
            model.Options3 = GetFieldValue(listFields.ToList(), "Options3");
            model.Quantity3 = GetFieldValue(listFields.ToList(), "Approximate quantity3");

            model.Pricing = GetFieldValue(listFields.ToList(), "Do you require GSA pricing");
            model.ContractNo = GetFieldValue(listFields.ToList(), "GovernmentDoD Contract No");
            model.ProgramOrProject = GetFieldValue(listFields.ToList(), "GovernmentDoD project or program");
            model.PriceQuoteEquipment = GetFieldValue(listFields.ToList(), "Are you interested in a price quote for demo equipment");
            model.WhenLookingPurchase = GetFieldValue(listFields.ToList(), "When are you looking to purchase");
            if (model.WhenLookingPurchase == "Select") model.WhenLookingPurchase = string.Empty;
            model.Comments = GetFieldValue(listFields.ToList(), "Additional information or comments");
            model.RentalOptions = GetFieldValue(listFields.ToList(), "Are you interested in financing or rental options");

            lstItems = GetItemsByTemplate("PRODUCT-QUOTATION-REQUEST-ACKNOWLEDGE").ToList();
            if (lstItems.Count > 0)
            {
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    String bodyTemplate = resultItem["BodyTemplate"].ToString();
                    if (HttpContext.Current.Session["ReferenceNumber"] != null)
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REFERENCENUMBER]]", HttpContext.Current.Session["ReferenceNumber"].ToString());  //changed templates key -PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE    
                        model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
                    }
                    string fromEmailAddress = resultItem["FromEmailAddress"].ToString();
                    string subject = resultItem["SubjectTemplate"].ToString();
                    string toEmailAddress = model.Email;
                    //SendMail(subject, bodyTemplate, toEmailAddress, fromEmailAddress);                   
                    var response = EmailManager.RequestProductQuoteSendAdminMail(model, Context.Language.Name);
                    if (response)
                    {
                        var userMailresponse = EmailManager.RequestProductQuoteSendMail(model, Context.Language.Name);
                        if (userMailresponse)
                        {
                            HttpContext.Current.Session.Remove("ReferenceNumber");
                        }
                    }
                }
            }
        }

        public void RequestProductQuoteJapan(Collection<AdaptedControlResult> listFields)
        {
            RequestProductQuoteModel model = new RequestProductQuoteModel(); string emailTemplate = string.Empty;
            List<Item> lstItems = new List<Item>(); Item resultItem;
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Company = GetFieldValue(listFields.ToList(), "Company Name");
            model.Department = GetFieldValue(listFields.ToList(), "Department Name");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal Code");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.Fax = GetFieldValue(listFields.ToList(), "Fax number");

            if (model.Country == "US")
            {
                model.State = listFields.Where(x => x.FieldID == "{8BDF4C6C-4AEA-4DB7-8B12-DF61D748B3F3}").FirstOrDefault().Value;
            }
            else if (model.Country == "JP")
            {
                model.State = listFields.Where(x => x.FieldID == "{FDE50A7C-7586-4752-80D1-1B32535BDF34}").FirstOrDefault().Value;
            }
            else
            {
                model.State = listFields.Where(x => x.FieldID == "{EC9EE295-2B22-4B75-96E9-458A4FBAF48C}").FirstOrDefault().Value;
            }

            model.ModelNumber1 = GetFieldValue(listFields.ToList(), "Select a product1");
            model.ModelNumber1 = GetFieldValue(listFields.ToList(), model.ModelNumber1 == "Select" ? "Enter if not in product list1" : "Select a product1");

            model.Quantity1 = GetFieldValue(listFields.ToList(), "Quantity1");
            model.FrequencyRange1 = GetFieldValue(listFields.ToList(), "Specifications1");

            model.ModelNumber2 = GetFieldValue(listFields.ToList(), "Select a product2");
            model.ModelNumber2 = GetFieldValue(listFields.ToList(), model.ModelNumber2 == "Select" ? "Enter if not in product list2" : "Select a product2");

            model.Quantity2 = GetFieldValue(listFields.ToList(), "Quantity2");
            model.FrequencyRange2 = GetFieldValue(listFields.ToList(), "Specifications2");

            model.ModelNumber3 = GetFieldValue(listFields.ToList(), "Select a product3");
            model.ModelNumber3 = GetFieldValue(listFields.ToList(), model.ModelNumber3 == "Select" ? "Enter if not in product list3" : "Select a product3");

            model.Quantity3 = GetFieldValue(listFields.ToList(), "Quantity3");
            model.FrequencyRange3 = GetFieldValue(listFields.ToList(), "Specifications3");

            lstItems = GetItemsByTemplate("PRODUCT-QUOTATION-REQUEST-ACKNOWLEDGE").ToList();
            if (lstItems.Count > 0)
            {
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    String bodyTemplate = resultItem["BodyTemplate"].ToString();
                    if (HttpContext.Current.Session["ReferenceNumber"] != null)
                    {
                        bodyTemplate = bodyTemplate.Replace("[[REFERENCENUMBER]]", HttpContext.Current.Session["ReferenceNumber"].ToString());  //changed templates key -PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE    
                        model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
                    }
                    string fromEmailAddress = resultItem["FromEmailAddress"].ToString();
                    string subject = resultItem["SubjectTemplate"].ToString();
                    string toEmailAddress = model.Email;
                    //SendMail(subject, bodyTemplate, toEmailAddress, fromEmailAddress);                   
                    var response = EmailManager.RequestProductQuoteSendAdminMail(model, Context.Language.Name);
                    if (response)
                    {
                        var userMailresponse = EmailManager.RequestProductQuoteSendMail(model, Context.Language.Name);
                        if (userMailresponse)
                        {
                            HttpContext.Current.Session.Remove("ReferenceNumber");
                        }
                    }
                }
            }
        }

        public void RequestRetroOptionQuote(Collection<AdaptedControlResult> listFields)
        {
            RequestProductQuoteModel model = new RequestProductQuoteModel();
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Company = GetFieldValue(listFields.ToList(), "Company");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.JobTitle = GetFieldValue(listFields.ToList(), "Job Title");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Zip Code");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.State = GetFieldValue(listFields.ToList(), "State");
            model.CountryOfEndUse = GetFieldValue(listFields.ToList(), "Country of End Use");
            model.TypeOfIndustry = GetFieldValue(listFields.ToList(), "Type of Industry");
            model.Fax = GetFieldValue(listFields.ToList(), "Fax Number");
            if (model.Country == "US")
            {
                model.State = listFields.Where(x => x.FieldID == "{C581A999-1507-4E00-BA3E-9881EF5C4C85}").FirstOrDefault().Value;
            }
            else if (model.Country == "JP")
            {
                model.State = listFields.Where(x => x.FieldID == "{C532CB4B-EFC3-411F-86E6-96A57F6427B1}").FirstOrDefault().Value;
            }
            else
            {
                model.State = listFields.Where(x => x.FieldID == "{3F5B9A72-F662-4075-9277-201EE14DFC08}").FirstOrDefault().Value;
            }
            model.ModelNumber1 = GetFieldValue(listFields.ToList(), "Model Number");
            model.SerialNumber1 = GetFieldValue(listFields.ToList(), "Serial Number");
            model.Options1 = GetFieldValue(listFields.ToList(), "Options Requested");
            model.Comment1 = GetFieldValue(listFields.ToList(), "Comments To be printed on Quote");

            model.ModelNumber2 = GetFieldValue(listFields.ToList(), "Model Number2");
            model.SerialNumber2 = GetFieldValue(listFields.ToList(), "Serial Number2");
            model.Options2 = GetFieldValue(listFields.ToList(), "Options Requested2");
            model.Comment2 = GetFieldValue(listFields.ToList(), "Comments To be printed on Quote2");

            model.ModelNumber3 = GetFieldValue(listFields.ToList(), "Model Number3");
            model.SerialNumber3 = GetFieldValue(listFields.ToList(), "Serial Number3");
            model.Options3 = GetFieldValue(listFields.ToList(), "Options Requested3");
            model.Comment3 = GetFieldValue(listFields.ToList(), "Comments To be printed on Quote3");
            if (HttpContext.Current.Session["ReferenceNumber"] != null)
            {
                model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
            }

            var response = EmailManager.RequestRetroOptionQuoteSendMails(model);
            if (response)
            {
                HttpContext.Current.Session.Remove("ReferenceNumber");
            }
        }

        public void InquiryForm(Collection<AdaptedControlResult> listFields)
        {
            TrainingInquiryModel model = new TrainingInquiryModel();
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Company = GetFieldValue(listFields.ToList(), "Company");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.JobTitle = GetFieldValue(listFields.ToList(), "Job Title");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal Code");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.Comments = GetFieldValue(listFields.ToList(), "Comments");
            model.Department = GetFieldValue(listFields.ToList(), "Department");
            model.Fax = GetFieldValue(listFields.ToList(), "Fax Number");

            if (model.Country == "US")
            {
                model.State = listFields.Where(x => x.FieldID == "{F6962816-5C33-42BB-8797-E49E92BEE07B}").FirstOrDefault().Value;
            }
            else if (model.Country == "JP")
            {
                model.State = listFields.Where(x => x.FieldID == "{DAA9FC16-23B3-47B4-85FA-ED85CE49ECA9}").FirstOrDefault().Value;
            }
            else
            {
                model.State = listFields.Where(x => x.FieldID == "{0E564DBF-2750-44BE-9164-8A51F84D445F}").FirstOrDefault().Value;
            }
            if (HttpContext.Current.Session["ReferenceNumber"] != null)
            {
                model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
            }
            model.Class1= GetFieldValue(listFields.ToList(), "Class Name1");
            model.Class2 = GetFieldValue(listFields.ToList(), "Class Name2");
            model.Class3 = GetFieldValue(listFields.ToList(), "Class Name3");
            var response = EmailManager.InquiryFormSendAdminMail(model);
            if (response)
            {
                var adminResponse = InquiryFormUserMail(listFields);
            }
        }

        public bool InquiryFormUserMail(Collection<AdaptedControlResult> listFields)
        {
            EmailActionsModel model = new EmailActionsModel();
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            if (HttpContext.Current.Session["ReferenceNumber"] != null)
            {
                model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
            }

            var response = EmailManager.InquiryFormSendUserMail(model);
            if (response)
            {
                HttpContext.Current.Session.Remove("ReferenceNumber");
            }
            return response;
        }

        public void RequestCustomerSupport(Collection<AdaptedControlResult> listFields)
        {
            RequestCustomerSupportModel model = new RequestCustomerSupportModel();
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Position = GetFieldValue(listFields.ToList(), "Position");
            model.Company = GetFieldValue(listFields.ToList(), "Company");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Fax = GetFieldValue(listFields.ToList(), "Fax Number");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal Code");

            model.NeedAssistance = ((GetFieldValue(listFields.ToList(), "I need assistance") == "1") ? "True" : "False");
            model.ModelNumber1 = GetFieldValue(listFields.ToList(), "Model Number eg MG3700A");
            model.SoftwareVersion = GetFieldValue(listFields.ToList(), "Software Version");
            model.SerialNumber1 = GetFieldValue(listFields.ToList(), "Serial Number");
            model.DetailsOfProblemQuery = GetFieldValueByID(listFields, "{D5D17838-DC8E-4299-9D4E-A1704B587DB9}");
            model.PleaseSendENews = (GetFieldValue(listFields.ToList(), "Please send me eNews") == "1" ? "True" : "False");
            model.ContactPreference = GetFieldValue(listFields.ToList(), "Contact Preferences");

            if (model.Country == "US")
            {
                model.State = listFields.Where(x => x.FieldID == "{265DE005-CE19-4EFE-B8CE-37F696233AB5}").FirstOrDefault().Value;
            }
            else if (model.Country == "JP")
            {
                model.State = listFields.Where(x => x.FieldID == "{FF10FB77-DE85-4D06-A089-ED2592DF0BF1}").FirstOrDefault().Value;
            }
            else
            {
                model.State = listFields.Where(x => x.FieldID == "{CB89A2C8-2370-4F7F-9292-9160272627D0}").FirstOrDefault().Value;
            }
            if (HttpContext.Current.Session["ReferenceNumber"] != null)
            {
                model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
            }

            string text = HttpUtility.HtmlDecode(model.ContactPreference);
            if (text.StartsWith("<item>", StringComparison.OrdinalIgnoreCase))
            {
                text = text.Replace("<item>", string.Empty).Replace("</item>", ",");
                text = text.Substring(0, text.Length - 1);
            }
            string[] str = text.Split(',');
            if (str.Count() > 0)
            {
                model.PreferenceEmail = model.PreferencePhone = model.PreferencePost = "NO";
                foreach (var item in str)
                {
                    if (item == "1")
                    {
                        model.PreferenceEmail = "YES";
                    }
                    else if (item == "2")
                    {
                        model.PreferencePhone = "YES";
                    }
                    else if (item == "3")
                    {
                        model.PreferencePost = "YES";
                    }
                }
            }

            var response = EmailManager.RequestCustomerSupportSendMail(model);
            if (response)
            {
                HttpContext.Current.Session.Remove("ReferenceNumber");
            }
        }

        public void RequestSurePriority(Collection<AdaptedControlResult> listFields)
        {
            RequestSurePrioritySupportModel model = new RequestSurePrioritySupportModel();
            model.Salutation = GetFieldValue(listFields.ToList(), "Salutation");
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Position = GetFieldValue(listFields.ToList(), "Position");
            model.Company = GetFieldValue(listFields.ToList(), "Company");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Fax = GetFieldValue(listFields.ToList(), "Fax Number");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal Code");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            if (model.Country == "US")
            {
                model.State = listFields.Where(x => x.FieldID == "{6035B14D-7E7E-49F1-A012-01C5A9C9B1DF}").FirstOrDefault().Value;
            }
            else if (model.Country == "JP")
            {
                model.State = listFields.Where(x => x.FieldID == "{5A15DAE2-C8B0-4A05-8B32-6EBBE5703785}").FirstOrDefault().Value;
            }
            else
            {
                model.State = listFields.Where(x => x.FieldID == "{4B3DDDCE-B198-43EA-968C-430955A641DF}").FirstOrDefault().Value;
            }
            model.CheckQuotation = (GetFieldValueByID(listFields, "{F357D08F-7E82-4F51-BF34-97A8FBDE8B87}") == "1" ? "True" : "False");
            model.CheckContactMe = (GetFieldValueByID(listFields, "{182952F9-E828-4E19-BE3D-A3221D45C02A}") == "1" ? "True" : "False");
            model.RegistrationCode = GetFieldValueByID(listFields, "{A5DF52F9-6E16-41FA-9D8A-E42F9FA9205F}");
            model.SerialNumber1 = GetFieldValueByID(listFields, "{E06CAAC6-0516-49B0-AF68-49E21A863DEF}");
            model.ProductType = GetFieldValueByID(listFields, "{0B9157E0-8A57-407C-B01F-429A89AF4FCF}");
            model.SoftwareVersion = GetFieldValueByID(listFields, "{0896CED5-7553-46BD-A826-C6A878EE57CF}");
            model.DetailsOfProblemQuery = GetFieldValueByID(listFields, "{736E6FBD-47B6-40F1-B825-FFB86E2C59B0}");
            model.PleaseSendENews = (GetFieldValueByID(listFields, "{A3E434A5-49B9-48BF-8170-06FC67B93A3B}") == "1" ? "True" : "False");
            model.ContactPreference = GetFieldValueByID(listFields, "{0C9A2D26-6C13-477F-9134-8C68852C61A9}");
            if (HttpContext.Current.Session["ReferenceNumber"] != null)
            {
                model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
            }

            string text = HttpUtility.HtmlDecode(model.ContactPreference);
            if (text.StartsWith("<item>", StringComparison.OrdinalIgnoreCase))
            {
                text = text.Replace("<item>", string.Empty).Replace("</item>", ",");
                text = text.Substring(0, text.Length - 1);
            }
            string[] str = text.Split(',');
            if (str.Count() > 0)
            {
                model.PreferenceEmail = model.PreferencePhone = model.PreferencePost = "NO";
                foreach (var item in str)
                {
                    if (item == "1")
                    {
                        model.PreferenceEmail = "YES";
                    }
                    else if (item == "2")
                    {
                        model.PreferencePhone = "YES";
                    }
                    else if (item == "3")
                    {
                        model.PreferencePost = "YES";
                    }
                }
            }

            var response = EmailManager.RequestSurePrioritySupportSendMail(model);
            if (response)
            {
                HttpContext.Current.Session.Remove("ReferenceNumber");
            }
        }

        public void TalkToAnritsu(Collection<AdaptedControlResult> listFields)
        {
            EmailActionsModel model = new EmailActionsModel();
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone");
            model.Company = GetFieldValue(listFields.ToList(), "Company");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.PostalCode = GetFieldValue(listFields.ToList(), "ZipPostal Code");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address");

            model.RequestType = GetFieldValue(listFields.ToList(), "Request Type");
            switch (model.RequestType)
            {
                case "1":
                    model.RequestTypeText = "Request a Product Demonstration"; break;

                case "2":
                    model.RequestTypeText = "Request Parts/Manuals"; break;

                case "3":
                    model.RequestTypeText = "Comments/Suggestions for improving our Products or Service"; break;

                case "9":
                    model.RequestTypeText = "TR"; break;

                case "10":
                    model.RequestTypeText = "Request a Sales Call"; break;

                case "5":
                    model.RequestTypeText = "Other"; break;

                case "11":
                    model.RequestTypeText = "Request Trade Compliance Regulatory Information (Product Model and Serial Number Required)";
                    break;
                case "12":
                    model.RequestTypeText = "Submit a Technical Question";
                    break;

                default:
                    break;
            }
            string toBeContacted = GetFieldValue(listFields.ToList(), "Yes I would like to be contacted");
            model.ToBeContacted = toBeContacted.Equals("1", StringComparison.OrdinalIgnoreCase) ? "Yes" : "No";
            model.ProductInterest = GetFieldValue(listFields.ToList(), "Product Interest");
            model.Comments = GetFieldValue(listFields.ToList(), "Questions or Comments");
            if (HttpContext.Current.Session["ReferenceNumber"] != null)
            {
                model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
            }
            var adminMailResponse = EmailManager.TalkToAnritsuSendAdminMail(model);
            if (adminMailResponse)
            {
                var response = EmailManager.TalkToAnritsuSendUserMail(model);
                if (response)
                {
                    HttpContext.Current.Session.Remove("ReferenceNumber");
                }
            }
        }

        public void ApplicationForm(Collection<AdaptedControlResult> listFields)
        {
            EmailActionsModel model = new EmailActionsModel();
            model.Surname = GetFieldValue(listFields.ToList(), "Surname");
            model.Name = GetFieldValue(listFields.ToList(), "Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Phonetic Surname");
            model.FirstName = GetFieldValue(listFields.ToList(), "Phonetic Name");
            model.SchoolName = GetFieldValue(listFields.ToList(), "School name");
            model.FacultyName = GetFieldValue(listFields.ToList(), "Faculty Name");
            model.Department = GetFieldValue(listFields.ToList(), "Department Name");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal code");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.State = GetFieldValue(listFields.ToList(), "State");
            model.Localities = GetFieldValue(listFields.ToList(), "Localities");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Phone = GetFieldValue(listFields.ToList(), "Telephone number");
            model.MobileNumber = GetFieldValue(listFields.ToList(), "Mobile number");
            model.Email = GetFieldValue(listFields.ToList(), "Mail address");
            model.Comments = GetFieldValue(listFields.ToList(), "Inquiry matters");

            if (HttpContext.Current.Session["ReferenceNumber"] != null)
            {
                model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"].ToString();
            }
            var response = EmailManager.SendApplicationFormMails(model);
            if (response > 0)
            {
                HttpContext.Current.Session.Remove("ReferenceNumber");
            }
        }

        public void EmploymentForm(Collection<AdaptedControlResult> listFields)
        {
            EmailActionsModel model = new EmailActionsModel(); string emailTemplate = string.Empty;
            List<Item> lstItems = new List<Item>(); Item resultItem;
            model.FirstName = GetFieldValue(listFields.ToList(), "First Name");
            model.LastName = GetFieldValue(listFields.ToList(), "Last Name");
            model.Email = GetFieldValue(listFields.ToList(), "Email");
            model.Address1 = GetFieldValue(listFields.ToList(), "Address1");
            model.Address2 = GetFieldValue(listFields.ToList(), "Address2");
            model.Phone = GetFieldValue(listFields.ToList(), "Phone Number");
            model.City = GetFieldValue(listFields.ToList(), "City");
            model.State = GetFieldValue(listFields.ToList(), "State");
            model.Country = GetFieldValue(listFields.ToList(), "Country");
            model.PostalCode = GetFieldValue(listFields.ToList(), "Postal Code");
            model.JobTitle = GetFieldValue(listFields.ToList(), "Reference Cod and Job Title you are applying");
            model.Comments = GetFieldValue(listFields.ToList(), "Comment");


            if (HttpContext.Current.Session["WffmUploadFilePath"] != null)
            {
                model.UploadFilePath = Convert.ToString(HttpContext.Current.Session["WffmUploadFilePath"]);
            }

            var templateKey = string.Empty;

            if (Context.Language.Name.ToLower(CultureInfo.InvariantCulture).Equals("en-gb"))
                templateKey = "UK-EMPLOYMENT-TOADMIN";
            else if (Context.Language.Name.ToLower(CultureInfo.InvariantCulture).Equals("en-in"))
                templateKey = "IN-EMPLOYMENT-TOADMIN";
            else if (Context.Language.Name.ToLower(CultureInfo.InvariantCulture).Equals("en-us"))
                templateKey = "US-EMPLOYMENT-TOADMIN";

            lstItems = GetItemsByTemplate(templateKey).ToList();

            if (lstItems.Count > 0)
            {
                resultItem = lstItems.FirstOrDefault();
                if (resultItem != null)
                {
                    int response = EmailManager.EmploymentFormSendMail(model);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Faq")]
        public void FaqSurveyEmail(Collection<AdaptedControlResult> listFields)
        {
            var model = new EmailActionsModel
            {
                FaqAnswered = GetFieldValue(listFields.ToList(), "Does this FAQ answer your question"),
                RateOfQuality = GetFieldValue(listFields.ToList(), "How do you rate the quality of this content"),
                Feedback = GetFieldValue(listFields.ToList(), "Feedback"),
                Name = GetFieldValue(listFields.ToList(), "Name"),
                Phone = GetFieldValue(listFields.ToList(), "Phone"),
                Email = GetFieldValue(listFields.ToList(), "Email"),
                PageUrl = HttpContext.Current.Request.Url.AbsoluteUri
            };

            var faqid = HttpContext.Current.Request.Params.AllKeys.ToList()
                .FirstOrDefault(x => x.EndsWith("hdnFaqId", new StringComparison() { }));
            model.FaqId = Convert.ToInt32(HttpContext.Current.Request.Params[faqid]);

            var question = HttpContext.Current.Request.Params.AllKeys.ToList()
                .FirstOrDefault(x => x.EndsWith("hdnQuestion", new StringComparison() { }));
            model.Question = HttpContext.Current.Request.Params[question];

            var answer = HttpContext.Current.Request.Params.AllKeys.ToList()
                .FirstOrDefault(x => x.EndsWith("hdnAnswer", new StringComparison() { }));
            model.Answer = HttpContext.Current.Request.Params[answer];

            model.ReferenceNumber = HttpContext.Current.Session["ReferenceNumber"] != null
                ? ThankyouEnumeration.FAQSurvey + HttpContext.Current.Session["ReferenceNumber"].ToString()
                : ThankyouEnumeration.FAQSurvey;

            var response = EmailManager.FaqSurveyEmail(model);
            if (response > 0)
            {
                HttpContext.Current.Session.Remove("ReferenceNumber");
            }
        }
    }

        #endregion


    public static class EmailTemplates
    {
        public const string SurePriorityDistributionList = "A-SURE-PRIORITY-SUPPORT-DISTRIBUTIONLIST";
        public const string WebmasterDistributionList = "CONTACT-WEBMASTER-DISTRIBUTIONLIST";
        public const string CustomerSupportDistributionList = "CUSTOMER-SUPPORT-DISTRIBUTIONLIST";
        public const string MasterUserGroupDistributionList = "MASTER-USER-GROUP-DISTRIBUTIONLIST";
        public const string NewsletterList = "NEWSLETTER-SIGNUP-REQUEST-DISTRIBUTIONLIST";
        public const string NewsletterSignupConfirmation = "NEWSLETTER-SIGNUP-CONFIRMATION";
        public const string ProductQuoteRequestDistributionList = "PRODUCT-QUOTATION-REQUEST-DISTRIBUTIONLIST";
        public const string ProductQuoteTemplateKey = "PRODUCT-QUOTATION-REQUEST-ACKNOWLEDGE";
        public const string ProfessionalServiceRequestDistributionList = "PROFESSIONAL-SERVICE-REQUEST-DISTRIBUTIONLIST";
        public const string ProfessionalServiceTemplateKey = "PROFESSIONAL-SERVICE-REQUEST-ACKNOWLEDGE";
        public const string RequestPartQuoteDistributionList = "REQUEST-PART-QUOTE-DISTRIBUTIONLIST";
        public const string RequestRetrofitOptionQuoteDistributionList = "REQUEST-RETROFIT-OPTION-QUOTE-DISTRIBUTIONLIST";
        public const string RequestServiceQuoteDistributionList = "REQUEST-SERVICE-QUOTE-DISTRIBUTIONLIST";
        public const string RequestPartQuoteToUser = "REQUEST-PART-QUOTE-TO-USER";
        public const string RequestRetrofitOptionQuoteToUser = "REQUEST-RETROFIT-OPTION-QUOTE-TO-USER";
        public const string RequestServiceQuoteToUser = "REQUEST-SERVICE-QUOTE-TO-USER";
        public const string ResponseSupportEnquiry = "RESPONSE-TRAINING-ENQUIRY";
        public const string SupportTrainingEnquiryDistributionList = "SUPPORT-TRAINING-ENQUIRY-DISTRIBUTIONLIST";
        public const string TalkToAnritsuDistributionList = "TALK-TO-ANRITSU-DISTRIBUTIONLIST";
        public const string TalkToAnritsuUser = "TALK-TO-ANRITSU-USER-EMAIL";
        public const string JobApplicationDistributionList = "NEW-JOB-APPLICATION-DISTRIBUTIONLIST";
        public const string JobApplicationAcknowledgement = "JOB-APPLICATION-ACKNOWLEDGE";
        public const string EmploymentTemplateKey = "EMPLOYMENT-TOADMIN";
        public const string CalibrationCertificateRequestDistributionList =
                "CALIBRATION-CERTIFICATE-REQUEST-DISTRIBUTIONLIST";
        public const string CalibrationCertificateRequestToUser = "CALIBRATION-CERTIFICATE-REQUEST-TO-USER";
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "FAQ")]
        public const string FAQSurveyDistributionList = "TECHNICAL-FAQ-SURVEY-DISTRIBUTIONLIST";
        public const string WorkflowRejectUserEmail = "WORKFLOW-REJECT-USER-MAIL";
        public const string WorkflowApproveWebmasterEmail = "WORKFLOW-APPROVE-WEBMASTER-EMAIL";
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Thankyou")]
    public static class ThankyouEnumeration
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "FAQ")]
        public const string FAQSurvey = "FS";
    }
}
