﻿using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Submit;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Sitecore.Diagnostics;

namespace Anritsu.WebApp.SitecoreUtilities.WebForms.SaveActions
{
    public class SaveToWebAdmin : ISaveAction
    {
        /// <property>This is string which stores the filed mappings in : separeated forms</property>
        /// <info> These fields names will be set in save action in Sitecore edtior window</info>
        public string MappedFields { get; set; }
        /// <property>This is string which stores the table name into which the form info has to be stored.</property>
        /// <info> This table name will be set in save action in Sitecore edtior window</info>
        public string MappedTable { get; set; }

        /// <param name="fields">The fields of the form.</param>
        /// <param name="data">The custom data.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "faq")]
        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            try
            {
                var honeypotFlag = HttpContext.Current.Request.Params.AllKeys.ToList().FirstOrDefault(x => x.EndsWith("txtUserInteractionIdentifier", new StringComparison() { }));
                if (HttpContext.Current.Request.Params[honeypotFlag] == "green")
                    throw new InvalidOperationException("Bots are not allowed");
                    //Environment.FailFast("Bots are not allowed", null);

                string conStrng = ConfigurationManager.ConnectionStrings["DB_NGW_ConnectionString"].ConnectionString;
                SqlConnection sqlCon;

                if (!string.IsNullOrEmpty(MappedTable))
                {
                    using (sqlCon = new SqlConnection(conStrng))
                    {
                        sqlCon.Open();
                        var mappedFields = new string[0];
                        if (!string.IsNullOrEmpty(MappedFields))
                            mappedFields = MappedFields.Split(',');

                        SqlCommand cmd = new SqlCommand(MappedTable, sqlCon);
                        cmd.CommandType = CommandType.StoredProcedure;
                        List<ListItem> CountryAndState = new List<ListItem>();

                        foreach (string key in mappedFields)
                        {
                            var fieldInfo = new string[0];
                            fieldInfo = key.Split(':');
                            if (fieldInfo.Length > 0 && fieldInfo[1].ToLower(CultureInfo.InvariantCulture) != Constants.SelectParameter)
                            {
                                string fieldType = GetFieldType(fieldInfo[0]);
                                string fieldVal = GetFieldValue(fieldInfo[0], fieldType, fields);
                                if (fieldType.ToUpperInvariant().Equals(Constants.CountryDropList.ToUpperInvariant()) ||
                                    fieldType.ToUpperInvariant().Equals(Constants.CustomSingleLineText.ToUpperInvariant()) ||
                                    fieldType.ToUpperInvariant().Equals(Constants.CustomDropList.ToUpperInvariant()) ||
                                    fieldType.ToUpperInvariant().Equals(Constants.CustomDropListJapanStates.ToUpperInvariant()))
                                {
                                    CountryAndState.Add(new ListItem { Text = fieldType, Value = fieldVal });
                                }
                                else
                                    cmd.Parameters.AddWithValue(fieldInfo[1], fieldVal);
                            }
                        }
                        if (CountryAndState.Count > 0)
                        {
                            var firstOrDefault = CountryAndState.FirstOrDefault(x => x.Text.ToUpperInvariant().Equals(Constants.CountryDropList.ToUpperInvariant()));
                            if (firstOrDefault != null)
                            {
                                var country = firstOrDefault.Value;
                                cmd.Parameters.AddWithValue("@CountryId", country);
                                if (CountryAndState.Count != 1)
                                    cmd.Parameters.AddWithValue("@State", GetStateByCountry(CountryAndState, country));
                            }
                        }
                        var culture = GetCultureFromUrl();
                        cmd.Parameters.AddWithValue("@CultureGroupID", culture);

                        SqlParameter paramSubmitId = new SqlParameter
                        {
                            ParameterName = "@SubmitId",
                            SqlDbType = SqlDbType.Int,
                            Direction = ParameterDirection.Output
                        };
                        cmd.Parameters.Add(paramSubmitId);

                        //this parameter is only for faq survey feedback form
                        if (formid.ToString() == "{82868190-7662-45C8-A14F-C99D5E52DA5B}")
                        {
                            var faqid = HttpContext.Current.Request.Params.AllKeys.ToList()
                                .FirstOrDefault(x => x.EndsWith("hdnFaqId", new StringComparison() { }));

                            cmd.Parameters.AddWithValue("@FaqId", Convert.ToInt32(HttpContext.Current.Request.Params[faqid]));
                            cmd.Parameters.AddWithValue("@ClientIP", (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]));
                        }

                        if (formid.ToString().Equals("{AFE660AC-6253-4B3A-874E-7FE48DB1CAE6}"))
                        {
                            if (HttpContext.Current.Session["WffmUploadFilePath"] != null &&
                                !string.IsNullOrEmpty(HttpContext.Current.Session["WffmUploadFilePath"].ToString()))
                            {
                                var filePath = HttpContext.Current.Session["WffmUploadFilePath"].ToString();
                                var fileExists = File.Exists(filePath);
                                if (fileExists)
                                {
                                    var fileName = Path.GetFileNameWithoutExtension(filePath);
                                    var extension = Path.GetExtension(filePath);
                                    var newFileName = fileName + "-" + DateTime.Now.Ticks + extension;

                                    var folderPath = string.Empty;

                                    if (GetCultureFromUrl().Equals("en-GB"))
                                        folderPath = ConfigurationManager.AppSettings["UKEmploymentFolderPath"];
                                    else if (GetCultureFromUrl().Equals("en-IN"))
                                        folderPath = ConfigurationManager.AppSettings["INEmploymentFolderPath"];

                                    UploadFile(filePath, ConfigurationManager.AppSettings["AWSBucketName"], newFileName, true, folderPath);

                                    cmd.Parameters.RemoveAt("FilePath");
                                    cmd.Parameters.AddWithValue("@FilePath", ConfigurationManager.AppSettings["AWSBucketName"] + folderPath + "/" + newFileName);
                                    HttpContext.Current.Session["WffmUploadFilePath"] = null;
                                }
                            }
                        }

                        cmd.ExecuteNonQuery();
                        if (!string.IsNullOrEmpty(Convert.ToString(paramSubmitId.Value)))
                            HttpContext.Current.Session["ReferenceNumber"] = Convert.ToString(paramSubmitId.Value);
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message + " - form ID:" + formid, this);
                throw new ArgumentException(exception.Message);
            }
        }

        private string GetStateByCountry(List<ListItem> CountryAndState, string country)
        {
            string state = string.Empty;
            if (CountryAndState.Count > 1)
            {
                switch (country)
                {
                    case "US":
                        {
                            var ctrl = CountryAndState.Where(x => x.Text.ToUpperInvariant().Equals(Constants.CustomDropList.ToUpperInvariant())).FirstOrDefault();
                            state = ctrl != null ? ctrl.Value : string.Empty; break;
                        }
                    case "JP":
                        {
                            var ctrl = CountryAndState.Where(x => x.Text.ToUpperInvariant().Equals(Constants.CustomDropListJapanStates.ToUpperInvariant())).FirstOrDefault();
                            state = ctrl != null ? ctrl.Value : string.Empty; break;
                        }
                    default:
                        {
                            var ctrl = CountryAndState.Where(x => x.Text.ToUpperInvariant().Equals(Constants.CustomSingleLineText.ToUpperInvariant())).FirstOrDefault();
                            state = ctrl != null ? ctrl.Value : string.Empty; break;
                        }
                }
            }
            return state;
        }

        private string GetFieldValue(string fieldID, string fieldType, AdaptedResultList fields)
        {
            string val = string.Empty;
            var currentControl = fields.GetEntryByID(fieldID);
            if (currentControl != null)
            {
                var currentControlVal = currentControl.Value;
                ReferenceField refField = Sitecore.Context.Database.GetItem(fieldID).Fields[Constants.FieldLink];
                if (refField != null)
                {
                    Item item = refField.TargetItem;
                    if (item != null)
                    {
                        switch (fieldType.ToLower(CultureInfo.InvariantCulture))
                        {
                            //case "droplist":
                            //case "list":
                            case "checkboxlist":
                                {
                                    if (!string.IsNullOrEmpty(currentControlVal))
                                    {
                                        string text = HttpUtility.HtmlDecode(currentControlVal);
                                        if (text.StartsWith("<item>", StringComparison.OrdinalIgnoreCase))
                                        {
                                            text = text.Replace("<item>", string.Empty).Replace("</item>", ",");
                                            text = text.Substring(0, text.Length - 1);
                                        }

                                        val = text;
                                    }
                                    break;
                                }
                            default: return val =  HttpUtility.HtmlEncode(Convert.ToString(currentControlVal));
                        }
                    }
                }
            }
            return val;
        }

        private string GetFieldType(string fieldID)
        {
            string fieldType = string.Empty;
            ReferenceField refField = Sitecore.Context.Database.GetItem(fieldID).Fields["Field Link"];
            if (refField != null)
            {
                Item item = refField.TargetItem;
                if (item != null)
                {
                    string val = item.Fields["Class"].Value;
                    string[] arr = val.Split('.');
                    if (arr.Length > 0) fieldType = arr[arr.Length - 1];
                    else fieldType = val;
                }
            }
            return fieldType;
        }

        protected string GetColValue(string col, AdaptedResultList fields)
        {
            string val = string.Empty;
            var mappedFields = new string[0];
            if (!string.IsNullOrEmpty(MappedFields))
                mappedFields = MappedFields.Split(',');
            foreach (string key in mappedFields)
            {
                var fieldInfo = new string[0];
                fieldInfo = key.Split(':');
                if (fieldInfo.Length > 0)
                {
                    if (col == fieldInfo[1])
                    {
                        val = fields.GetEntryByID(fieldInfo[0]).Value;
                        break;
                    }
                }
            }
            return val;
        }

        private string GetCultureFromUrl()
        {
            var language = string.Empty;

            var url = HttpContext.Current.Request.Url.AbsolutePath;
            //url = url.Replace("www-", string.Empty).Replace("?", "/");
            var regexMatch = Regex.Match(url, "/[a-zA-Z][a-zA-Z]-[a-zA-Z][a-zA-Z]");

            if (!string.IsNullOrEmpty(regexMatch.Value))
            {
                language = regexMatch.Value.Replace("/", string.Empty);
            }
            return language;

        }

        private AmazonS3Client GetS3Client()
        {
            var regionEndpoint = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["AWSEndPoint"]);
            var s3Client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSAccessKey"], ConfigurationManager.AppSettings["AWSSecretKey"], regionEndpoint);
            return s3Client;
        }

        public void UploadFile(string filePath, string s3Bucket, string newFileName, bool deleteLocalFileOnSuccess,string folderPath)
        {
            //save in s3
            var s3PutRequest = new PutObjectRequest
            {
                FilePath = filePath,
                BucketName = s3Bucket + folderPath,
                CannedACL = S3CannedACL.PublicRead
            };

            //key - new file name
            if (!string.IsNullOrWhiteSpace(newFileName))
            {
                s3PutRequest.Key = newFileName;
            }

            try
            {
                var s3PutResponse = GetS3Client().PutObject(s3PutRequest);

                if (!deleteLocalFileOnSuccess) return;

                //Delete local file
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unable to delete the file at : " + filePath, ex, this.GetType());
            }
        }
    }

    public static class Constants
    {
        public const string SelectParameter = "--select parameter--";
        public const string CountryDropList = "CountryDropList";
        public const string CustomSingleLineText = "CustomSingleLineText";
        public const string CustomDropList = "CustomDropList";
        public const string CustomDropListJapanStates = "CustomDropListJapanStates";
        public const string FieldLink = "Field Link";
    }
}
