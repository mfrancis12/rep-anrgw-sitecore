﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Web.UI.Sheer;
using Sitecore.Workflows;
using Sitecore.Workflows.Simple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.WebApp.SitecoreUtilities.Workflows.Actions
{
    public class WorkflowEmailAction
    {
        Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");
        WorkflowPipelineArgs workflowArgs;
        public void Process(WorkflowPipelineArgs args)
        {
            Item currentItem = args.DataItem;
            ProcessorItem processorItem = args.ProcessorItem;
            Item innerItem = processorItem.InnerItem;

           // Sitecore.Context.ClientPage.ClientResponse.ShowModalDialog("/GlobalWeb/CustomApps/Workflow/SelectUsers.aspx?contentItemID=" + currentItem.ID.ToString() + "&innerItemID=" + innerItem.ID.ToString());
            
            //args.AbortPipeline();
            ClientPipelineArgs clientArgs = new ClientPipelineArgs();
            clientArgs.Parameters.Add("contentItemID", currentItem.ID.ToString());
            clientArgs.Parameters.Add("innerItemID", innerItem.ID.ToString());
            //clientArgs.Parameters.Add("stateID", currentItem.Fields["__Workflow state"].Value);
           // clientArgs.Parameters.Add("nextState", args.NextStateId.ToString());
            Sitecore.Context.ClientPage.Start(this, "DialogProcessor", clientArgs);
            workflowArgs = args;
            string nextStateId = string.Empty;
            IWorkflow workflow = masterDb.WorkflowProvider.GetWorkflow(currentItem);
            WorkflowState state = workflow.GetState(currentItem);
            List<WorkflowCommand> applicableWorkflowCommands = workflow.GetCommands(state.StateID).ToList();
            foreach (var applicableWorkflowCommand in applicableWorkflowCommands)
            {
                Item commandItem = masterDb.GetItem(applicableWorkflowCommand.CommandID);
                nextStateId = commandItem["Next state"];
            }
            if (!string.IsNullOrEmpty(nextStateId))
            {
                clientArgs.Parameters.Add("nextState", nextStateId);
            }
        }
        protected void DialogProcessor(ClientPipelineArgs clientArgs)
        {

            if (clientArgs.IsPostBack)
            {
                if (clientArgs.Result != "yes")
                {
                //    Item currentItem = masterDb.GetItem(clientArgs.Parameters["contentItemID"]);
                //    currentItem.Editing.BeginEdit();
                //    currentItem.Fields["__Workflow state"].Value = args.Parameters["stateID"];
                //    currentItem.Editing.EndEdit();
                //    return;
                    workflowArgs.AbortPipeline();

                }
                //SheerResponse.Eval("window.location.reload();");
                
                //if (args.Result == "yes")
                //{
                //    // 1. take item ID from args.Parameters["id"];
                //    // 2. get item by this ID
                //    // 3. move item to target workflow state

                //}
                //else
                //{
                //    workflowArgs.AbortPipeline();
                //    return;
                //}

            }
            else
            {
                //Sitecore.Context.ClientPage.ClientResponse.Confirm("Are you sure you want to edit this?");                
                Sitecore.Context.ClientPage.ClientResponse.ShowModalDialog("/GlobalWeb/CustomApps/Workflow/SelectUsers.aspx?contentItemID=" + clientArgs.Parameters["contentItemID"] + "&innerItemID=" + clientArgs.Parameters["innerItemID"] + "&nextState=" + clientArgs.Parameters["nextState"], true);
                clientArgs.WaitForPostBack();
               
            }

        }
            /*
        /// <summary>
        /// Runs the processor.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public void Process(WorkflowPipelineArgs args)
        {

            if (args != null)
            {
                ProcessorItem processorItem = args.ProcessorItem;
                Item contentItem = args.DataItem;
                if (processorItem == null)
                {
                    return;
                }
                Item innerItem = processorItem.InnerItem;
                string fullPath = innerItem.Paths.FullPath;
                string text = Sitecore.Context.User.Profile.Email;
                string[] users = { };
                if (state.ToString().Equals("{BCBA63C3-9389-4B80-874F-92BAAB3AEB11}"))
                {
                    if (!string.IsNullOrEmpty(contentItem.Fields["__Updated by"].ToString()))
                    {
                        users = new string[] { contentItem.Fields["__Updated by"].ToString() };
                    }
                    else if (!string.IsNullOrEmpty(contentItem.Fields["__Created by"].ToString()))
                    {
                        users = new string[] { contentItem.Fields["__Created by"].ToString() };
                    }
                }
                else
                {
                    users = System.Web.Security.Roles.GetUsersInRole("ANRGW\\PRODUCT_CA");
                }

                string email = string.Empty;
                if (users != null)
                {
                    foreach (var user in users)
                    {

                        var membershipUser = System.Web.Security.Membership.GetUser(user);
                        email = membershipUser.Email;
                        // use this email to send the message to that user
                        string text2 = email;// this.GetText(innerItem, "to", args);
                        string text3 = GetText(innerItem, "mail server", args);
                        string text4 = GetText(innerItem, "subject", args);
                        string text5 = GetText(innerItem, "message", args);
                        Error.Assert(text2.Length > 0, "The 'To' field is not specified in the mail action item: " + fullPath);
                        Error.Assert(text.Length > 0, "The 'From' field is not specified in the mail action item: " + fullPath);
                        Error.Assert(text4.Length > 0, "The 'Subject' field is not specified in the mail action item: " + fullPath);
                        Error.Assert(text3.Length > 0, "The 'Mail server' field is not specified in the mail action item: " + fullPath);
                        //System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage(text, text2);
                        using (MailMessage mailMessage = new System.Net.Mail.MailMessage(text, text2))
                        {
                            mailMessage.Subject = text4;
                            mailMessage.Body = text5;
                            //System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(text3);
                            using (SmtpClient smtpClient = new System.Net.Mail.SmtpClient(text3))
                            {
                                smtpClient.Send(mailMessage);
                            }
                        }
                    }
                }
            }
        }*/
        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <param name="commandItem">The command item.</param>
        /// <param name="field">The field.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private string GetText(Item commandItem, string field, WorkflowPipelineArgs args)
        {
            string text = commandItem[field];
            if (text.Length > 0)
            {
                return ReplaceVariables(text, args);
            }
            return string.Empty;
        }
        /// <summary>
        /// Replaces the variables.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private static string ReplaceVariables(string text, WorkflowPipelineArgs args)
        {
            text = text.Replace("$itemPath$", args.DataItem.Paths.FullPath);
            text = text.Replace("$itemLanguage$", args.DataItem.Language.ToString());
            text = text.Replace("$itemVersion$", args.DataItem.Version.ToString());
            text = text.Replace("$From$", Sitecore.Context.User.Name.ToString());
            return text;
        }
    }
}
