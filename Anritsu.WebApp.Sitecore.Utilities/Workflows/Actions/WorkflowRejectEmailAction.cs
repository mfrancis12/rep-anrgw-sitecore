﻿using System;
using System.Collections.Generic;
using Anritsu.Library.EmailSDK;
using Anritsu.Library.EmailSDK.EmailWebRef;
using Anritsu.WebApp.SitecoreUtilities.WebForms.SaveActions;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Workflows.Simple;

namespace Anritsu.WebApp.SitecoreUtilities.Workflows.Actions
{
    public class WorkflowRejectEmailAction
    {
        private const string ServiceResponse = "Response from Service : ";

        public void Process(WorkflowPipelineArgs args)
        {
            var currentItem = args.DataItem;
            var contentWorkflow = currentItem.Database.WorkflowProvider.GetWorkflow(currentItem);
            var contentHistory = contentWorkflow.GetHistory(currentItem);

            try
            {
                if (contentHistory.Length <= 0) return;

                //submitting user
                var lastUser = contentHistory[contentHistory.Length - 1].User;
                var submittingUser = Sitecore.Security.Accounts.User.FromName(lastUser, false);

                var myRequest = new SendTemplatedEmailCallRequest
                {
                    EmailTemplateKey = EmailTemplates.WorkflowRejectUserEmail,
                    ToEmailAddresses = new[] { submittingUser.Profile.Email },
                    CultureGroupId = GetCulture(currentItem.Language.Name)
                };

                var bodyKeys = new List<ReplaceKeyValueType>();

                var keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[USER]]",
                    ReplacementValue = Context.User.LocalName
                };
                bodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[ITEMID]]",
                    ReplacementValue = currentItem.ID.ToString()
                };
                bodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[ITEMPATH]]",
                    ReplacementValue = currentItem.Paths.Path
                };
                bodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[LANGUAGE]]",
                    ReplacementValue = currentItem.Language.Name
                };
                bodyKeys.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[VERSION]]",
                    ReplacementValue = currentItem.Version.ToString()
                };
                bodyKeys.Add(keyValue);

                myRequest.BodyReplacementKeyValues = bodyKeys.ToArray();
                var emailCallResponse = EmailServiceManager.SendTemplatedEmail(myRequest);

                if (emailCallResponse.StatusCode == 0) return;

                Error.LogError(ServiceResponse + emailCallResponse.ResponseMessage);
                throw new ArgumentException(emailCallResponse.ResponseMessage);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace, GetType());
            }
        }

        public static int GetCulture(string code)
        {
            var cultureCode = 1;
            switch (code)
            {

                case "en-US":
                    cultureCode = 1;
                    break;

                case "ja-JP":
                    cultureCode = 2;
                    break;
                case "en-GB":
                    cultureCode = 3;
                    break;

                case "en-AU":
                    cultureCode = 4;
                    break;

                case "zh-CN":
                    cultureCode = 6;
                    break;

                case "ko-KR":
                    cultureCode = 7;
                    break;

                case "zh-TW":
                    cultureCode = 10;
                    break;
                case "ru-RU":
                    cultureCode = 14;
                    break;
                case "en-IN":
                    cultureCode = 15;
                    break;

            }
            return cultureCode;
        }

        private string GetCultureCode(string languageName)
        {
            if (string.IsNullOrEmpty(languageName)) return string.Empty;

            return languageName.IndexOf('-') > 0 ? languageName.Substring(3, 2) : languageName;
        }
    }
}
