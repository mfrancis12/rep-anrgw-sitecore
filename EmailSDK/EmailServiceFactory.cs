﻿using System;
using System.Configuration;

namespace Anritsu.Library.EmailSDK
{
    public static class EmailServiceFactory
    {
        public static string _emailServiceToken = string.Empty;
        public static string _emailServiceEndpointUrl = string.Empty;

        public static string EmailServiceToken
        {
            get
            {
                if (string.IsNullOrEmpty(_emailServiceToken))
                {
                    _emailServiceToken = ConfigurationManager.AppSettings["EmailSDK_ServiceToken"];
                    //_emailServiceToken = "B814A12B-E153-4D86-BE46-596C38E7477B";
                    if (string.IsNullOrEmpty(_emailServiceToken))
                        throw new ArgumentException("Invalid Email service token.");
                }
                return _emailServiceToken;
            }
        }
        public static string EmailServiceEndpointURL
        {
            get
            {
                if (string.IsNullOrEmpty(_emailServiceEndpointUrl))
                {
                    _emailServiceEndpointUrl = ConfigurationManager.AppSettings["EmailSDK_ServiceEndpointURL"];
                    //_emailServiceEndpointUrl = "https://qa-webservices.anritsu.com/gwemailapi/emailservices.asmx";
                
                    if (string.IsNullOrEmpty(_emailServiceEndpointUrl))
                        throw new ArgumentException("Invalid Email service endpoint url.");
                }
                return _emailServiceEndpointUrl;
            }
        }

        static EmailServiceFactory()
        {
            _emailServiceToken = ConfigurationManager.AppSettings["EmailSDK_ServiceToken"];
           // _emailServiceToken = "B814A12B-E153-4D86-BE46-596C38E7477B";           

        }

        public static EmailWebRef.AnritsuEmailwebservice GetService()
        {
            var ws = new EmailWebRef.AnritsuEmailwebservice
            {
                Url = EmailServiceEndpointURL,
                GWApiHeaderTypeValue = new EmailWebRef.GWApiHeaderType {SToken = EmailServiceToken}
            };
            return ws;
        }
    }
}
