﻿using System;

namespace Anritsu.Library.EmailSDK
{
    public static class EmailServiceManager
    {
        public static EmailWebRef.BaseResponseType SendEmail(EmailWebRef.SendEmailCallRequest parm)
        {
            if (parm == null)
                throw new ArgumentException("Invalid request.");

            EmailWebRef.AnritsuEmailwebservice ws = EmailServiceFactory.GetService();
            return ws.SendEmailCall(parm);
        }

        public static EmailWebRef.BaseResponseType SendTemplatedEmail(EmailWebRef.SendTemplatedEmailCallRequest parm)
        {
            if (parm == null)
                throw new ArgumentException("Invalid request.");

            EmailWebRef.AnritsuEmailwebservice ws = EmailServiceFactory.GetService();
            return ws.SendTemplatedEmailCall(parm);
        }

        public static EmailWebRef.BaseResponseType SendDistributedEmail(EmailWebRef.SendDistributedEmailCallRequest parm)
        {
            if (parm == null)
                throw new ArgumentException("Invalid request.");

            EmailWebRef.AnritsuEmailwebservice ws = EmailServiceFactory.GetService();
            return ws.SendDistributedEmailCall(parm);
        }

        public static EmailWebRef.BaseResponseType SendDistributedTemplatedEmail(
            EmailWebRef.SendTemplatedDistributedEmailCallRequest parm)
        {
            if (parm == null)
                throw new ArgumentException("Invalid request.");

            EmailWebRef.AnritsuEmailwebservice ws = EmailServiceFactory.GetService();
            return ws.SendDistributedTemplatedEmailCall(parm);
        }
    }
}
