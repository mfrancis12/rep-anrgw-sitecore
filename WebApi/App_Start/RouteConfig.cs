﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
namespace WebApi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            RouteTable.Routes.MapHttpRoute(
                  "ResourceStringAPI",
                  "api/{controller}/{action}",
                   new { action = "Get", controller = "ResourceString", id = RouteParameter.Optional }
              );

            RouteTable.Routes.MapHttpRoute(
                "DefaultResourceString",
                "api/{controller}/{id}",
                 new { action = "Get", controller = "ResourceString", id = RouteParameter.Optional }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}