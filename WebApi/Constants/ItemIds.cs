﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Constants
{
    public class ItemIds
    {
        #region Error Messages
        public const string NotValidCulture = "Not a valid Culture Code";
        public const string NotValidTargetpath = "Not a valid target path to create items";
        public const string NoTemplateFound = "No such template found";
        public const string ItemAlreadyExists = "A page already exists with same name";
        public const string CreatedSuccessfully = "Page created successfully";
        public const string PublishedSuccessfully = "Page published successfully";
        public const string ItemNotFound = "Item Not Found";
        public const string ItemDeletedSuccessfully = "Item deleted successfully";
        public const string UpdatedSuccessfully = "Page updated successfully";
        #endregion
        #region Item IDs
        public const string SearchFilterFolder = "{5F38D725-4DF7-4522-A668-8DF8F56BD873}";
        #endregion
    }

    public class DownloadConstans
    {
        #region Downloads Constants
        public const string DetailsPagePath = "/test-measurement/support/downloads/software/";
        public const string DetailsPageKey = "drivers-software-downloads";
        public const string Succesfull = "Succesfull";
        public const string NoResultsFound = "No results fetched";
        public const string NotValidCulture = "Not a valid Culture group id";
        public const string NoProperInputs = "No Proper inputs";
        public const string NotValidCategory = "Not a valid category";
        public const string NotValidSubCategory = "Not a valid sub category";
        public const string NoSubCategories = "No sub categories fetched";
        public const string NotValidPageTitle = "Not valid page title";
        public const string NotValidTitle = "Not valid title";
        public const string ReleaseDate = "Please provide release date";
        public const string Category = "Please provide category";
        public const string SubCategories = "Please provide sub-category";
        public const string NotValidProduct = "Not a valid product";
        public const string ProvideItemId = "Please provide item id";
        public const string ProvideItemName = "Please provide item name";
        public const string NotValidId = "Not a valid item id";
        public const string NoLanguageVersion = "No language version";
        public const string NotValidModelNumber = "Not a valid model number";
        public const string NotValidDatabase = "Not a valid database";
        public const string ItemCreateSuccess = "Item Created Successfully";
        public const string ItemUpdateSuccess = "Item Updated Successfully";
        public const string FetchSuccessful = "Successfully fetched";
        public const string NullGuid = "00000000-0000-0000-0000-000000000000";
        public const string ProdHost = "www.anritsu.com";
        public const string ItemExist = "Item name already exist";
        public const string ParnetChildViolation = "Subcategory needs to be a child of Category Item";
        #endregion

        #region  DownloadsAdmin
        public const string DownloadsTemplate = "{0A13323A-10F2-404A-9AA2-7E0BD4D0EF78}";
        public const string SubCategoryTemplate = "{E7B62AFB-B727-4257-B6EE-67FA097E8417}";
        public const string CategoryTemplate = "{F547E7B1-B7B9-41AD-8958-4756BD7A5633}";
        public const string ProductsTemplate = "{3FA9B0BB-E7FB-4DA3-9835-8EF8ED8BD652}";
        #endregion
    }

    public static class Token
    {
        private static Guid _globalVerficationToken = new Guid("{d86c814d-ddb3-46d1-88eb-a4465d8c5918}");
        public static Guid GlobalVerficationToken
        {
            get { return _globalVerficationToken; }
        }
    }
}