﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Publishing;
using Sitecore.Resources.Media;
using Sitecore.SecurityModel;
using Sitecore.SharedSource.FieldSuite.Controls.GeneralLinks;
using Sitecore.SharedSource.FieldSuite.Types;
using WebApi.Constants;
using WebApi.Models;
using Convert = System.Convert;
using Anritsu.WebApp.GlobalWeb.BusinessLayer.BLL;
using Anritsu.WebApp.GlobalWeb.BusinessObjects.PropertyObjects;
using Sitecore.Jobs;
using Sitecore.Publishing.Pipelines.Publish;

namespace WebApi.Controllers
{
    public class DownloadsController : ApiController
    {
        #region Global Variables
        public const string indexName = "anritsu_downloads";
        public const string DriversSoftwareDownloads = "drivers-software-downloads";
        public Database masterDb
        {
            get { return Factory.GetDatabase("master"); }
        }
        public Database master
        {
            get { return Factory.GetDatabase("master"); }
        }
        public Database web
        {
            get { return Factory.GetDatabase("web"); }
        }
        //Database prod = (Sitecore.Configuration.Factory.GetDatabase("prod")!=null)?Sitecore.Configuration.Factory.GetDatabase("prod"):null;
        List<Database> ProdDatabases = Factory.GetDatabases().Where(x => x.Name == "production").ToList();
        int DefaultCultureId = 1;
        public const string fallbackLanguage = "en";

        #endregion

        #region Downloads
        // GET api/downloads/5
        public HttpResponseMessage GetDownloads(int cultureGroupId, string modelNumber, string targetDb, string includeRegistrationRequiredDwl = "true")
        {
            List<Downloads> lstDownloads = new List<Downloads>();
            try
            {
                int cultureGrpID = (cultureGroupId != 0) ? cultureGroupId : 1;
                List<Item> lstItems = new List<Item>();
                DataTable dataTable = new DataTable();

                string cultureCode = GetCultureCodeById(cultureGrpID);
                if (String.IsNullOrEmpty(cultureCode) || cultureCode== "invalid")
                {
                    return GetDownloadsResponse(lstDownloads, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);
                }
                if (String.IsNullOrEmpty(modelNumber))
                {
                    return GetDownloadsResponse(lstDownloads, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidModelNumber);
                }
                var currentDb = Context.Database;//ValidateTargetDb(targetDb);
                if (currentDb == null) return GetDownloadsResponse(lstDownloads, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidDatabase);
                Language itemLanguage = LanguageManager.GetLanguage(cultureCode, currentDb);
                using (var context = ContentSearchManager.GetIndex("anritsu_downloads").CreateSearchContext())
                {
                    var query = context.GetQueryable<SearchResultItem>().Where(i => i.Language == itemLanguage.Name);
                    foreach (SearchResultItem item in query)
                    {
                        if (currentDb.Items.GetItem(item.ItemId, itemLanguage) != null)
                        {
                            lstItems.Add(currentDb.Items.GetItem(item.ItemId, itemLanguage));
                        }
                    }
                }
                lstItems = lstItems.Distinct(new ItemEqualityComparer()).ToList();
                string[] modelnos = null;
               
                if (modelNumber.Contains(','))
                {
                    modelnos = modelNumber.Trim().Split(',');
                }
                if (modelnos != null)
                {
                    List<List<Downloads>> alldownloads = new List<List<Downloads>>();
                    foreach (var model in modelnos)
                    {
                        if (string.IsNullOrEmpty(model) == false)
                        {
                            List<Downloads> downloadperModelNumber = GetDownloadsForModelNumber(lstItems, model.ToString(), itemLanguage, includeRegistrationRequiredDwl);
                            if(downloadperModelNumber != null && downloadperModelNumber.Count() >0)
                            alldownloads.Add(downloadperModelNumber);
                        }
                    }
                    if (alldownloads.Count <= 0)
                    {
                        return GetDownloadsResponse(lstDownloads, (int)HttpStatusCode.NoContent,
                     DownloadConstans.NoResultsFound);
                    }
                    return GetALLDownloadsResponse(alldownloads, (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
                }



                if (includeRegistrationRequiredDwl.Equals("false", StringComparison.InvariantCultureIgnoreCase))
                    lstItems = lstItems.Where(item => !item["RedirectToMyAnritsu"].ToString().Equals("1", StringComparison.InvariantCultureIgnoreCase)).ToList();
                if (lstItems.Count <= 0)
                    return GetDownloadsResponse(lstDownloads, (int)HttpStatusCode.NoContent,
                        DownloadConstans.NoResultsFound);
                List<Item> outputList = new List<Item>();
                foreach (Item item in lstItems.Where(item => !String.IsNullOrEmpty(item["RelatedProducts"])))
                {
                    if (item["RelatedProducts"].Contains("|"))
                    {
                        string[] words = item["RelatedProducts"].Split('|');
                        outputList.AddRange(from id in words
                                            let regionalProductItem = currentDb.Items.GetItem(new ID(id), itemLanguage)
                                            where regionalProductItem != null && !String.IsNullOrEmpty(regionalProductItem.Name)
                                            && regionalProductItem.Name.Equals(modelNumber, StringComparison.InvariantCultureIgnoreCase)
                                            select item);
                    }
                    else
                    {
                        if (currentDb.GetItem(new ID(item["RelatedProducts"]), itemLanguage) != null)
                        {

                            if (currentDb.GetItem(new ID(item["RelatedProducts"]), itemLanguage).Name.ToLowerInvariant() == modelNumber.Trim().ToLowerInvariant())
                            {
                                outputList.Add(item);
                            }
                        }
                    }
                }
                if (outputList.Count <= 0)
                {
                    return GetDownloadsResponse(lstDownloads, (int)HttpStatusCode.NotFound, DownloadConstans.NoResultsFound);
                }
                outputList = outputList.Distinct(new ItemEqualityComparer()).ToList();
                foreach (Item item in outputList)
                {
                    Downloads model = new Downloads();
                    model.Title = item["Title"];
                    model.FilePath = item["FilePath"];
                    model.CategoryType = currentDb.GetItem(new ID(item["Subcategory"]), itemLanguage).Parent["Value"];

                    Item downloadDetailPage = currentDb.GetItem(new ID(item["Subcategory"]), itemLanguage);
                    if (downloadDetailPage != null)
                    {
                        if (downloadDetailPage["Key"] == DownloadConstans.DetailsPageKey)
                        {
                            model.DownloadPath = GetDownloadDetailePageUrl(item);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item["FilePath"]))
                            {
                                if (item["FilePath"].StartsWith("http") || item["FilePath"].StartsWith("https"))
                                {
                                    model.DownloadPath = item["FilePath"];
                                }
                                else
                                {
                                    model.DownloadPath = GetS3ObjectUrl(item["FilePath"]);
                                }
                            }

                        }
                    }
                    model.ModelNumber = modelNumber.Trim();
                    model.Version = item["Version"];
                    model.Size = item["Size"];
                    model.RedirectToMyAnritsu = item["RedirectToMyAnritsu"];
                    model.Created = DateUtil.IsoDateToDateTime(item["__Created"]).ToUniversalTime();
                    model.Updated = DateUtil.IsoDateToDateTime(item["__Updated"]).ToUniversalTime();
                    model.ReleaseDate = DateUtil.IsoDateToDateTime(item["ReleaseDate"]).ToUniversalTime();
                    lstDownloads.Add(model);
                }
                return GetDownloadsResponse(lstDownloads.OrderByDescending(dwl => dwl.ReleaseDate).ToList(), (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
            }
            catch (Exception ex)
            {
                return GetDownloadsResponse(lstDownloads, (int)HttpStatusCode.InternalServerError, string.Format("Error Message:{0} \n Stack trace: {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// Method for Downloads Search by Key
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetDownlodasByKey(DownloadSearchModel values)
        {
            //fix for special/html chars
            values.Title = HttpUtility.HtmlDecode(values.Title);
            var contextLang = GetCultureCodeById(values.CultureGroupId);
            using (new LanguageSwitcher(string.IsNullOrEmpty(contextLang) ? fallbackLanguage : contextLang))
            {
                //get the list of Items based on the filter values and include custom pagination
                int DefaultNoOfRows = 20, DefaultSkipRows = 0, TotalRows;
                var lstDownloads = new List<DownloadResults>();
                var consolidateDownloads = new List<Item>();
                var relatedProductDownloads = new List<Item>();

                //get the product item
                if (!string.IsNullOrEmpty(values.AssociatedProduct))
                {
                    var prodItems = master.SelectItems("fast:/sitecore/content/GlobalWeb/home//*[@@templateid = '" + DownloadConstans.ProductsTemplate + "']").ToList();
                    var targetProdItem =
                        prodItems.FirstOrDefault(prodItem => prodItem.Name.Equals(values.AssociatedProduct));
                    if (null != targetProdItem)
                    {
                        relatedProductDownloads = (from link in Globals.LinkDatabase.GetItemReferrers(targetProdItem, false)
                                                   let sourceItem = link.GetSourceItem()
                                                   where sourceItem != null
                                                   where sourceItem.TemplateID.ToString() == DownloadConstans.DownloadsTemplate
                                                   select sourceItem).ToList().Distinct(new ItemEqualityComparer()).ToList<Item>();
                    }
                }

                //chek if the subcategory id is not null or empty
                if (!string.IsNullOrEmpty(values.SubCategoryType))
                {
                    //get the childerns of the download item based on the sub category Id
                    var subCategoryItem = masterDb.GetItem(new ID(new Guid(values.SubCategoryType)));
                    var query = subCategoryItem.Axes.GetDescendants()
                    .ToList()
                    .Where(de => de.TemplateID.ToString().Equals(DownloadConstans.DownloadsTemplate));
                    if (query.Any())
                    {
                        consolidateDownloads.AddRange(query.ToArray());
                    }
                }
                else if (string.IsNullOrEmpty(values.CategoryType) && string.IsNullOrEmpty(values.SubCategoryType) &&
                         String.IsNullOrEmpty(values.AssociatedProduct))
                {
                    //get all the downloads
                    var allDownloads =
                        master.SelectItems("fast:/sitecore/content/global/downloads//*[@@templateid ='" +
                                           DownloadConstans.DownloadsTemplate + "']").ToList();
                    if (allDownloads.Any())
                    {
                        consolidateDownloads.AddRange(allDownloads.ToArray());
                    }
                }
                else
                    consolidateDownloads = relatedProductDownloads;

                if (relatedProductDownloads.Any())
                {
                    consolidateDownloads = (from dlItem in consolidateDownloads
                                            where relatedProductDownloads.Exists(relprod => relprod.ID.Equals(dlItem.ID))
                                            select dlItem).ToList();

                }

                if (!string.IsNullOrEmpty(values.ItemName))
                {
                    consolidateDownloads = consolidateDownloads.Where(x => x.Name.Contains(values.ItemName.ToString().Trim())).ToList();
                }

                if (!string.IsNullOrEmpty(values.Title))
                {
                    consolidateDownloads = consolidateDownloads.Where(x => x.Fields["Title"] != null
                                                   &&
                                                   x.Fields["Title"].ToString()
                                                       .ToLower()
                                                       .Contains(values.Title.ToLower()))
                        .ToList();
                }
                consolidateDownloads = consolidateDownloads.Distinct(new ItemEqualityComparer()).ToList();
                if (consolidateDownloads.Count <= 0)
                    return GetDowloadByKeyResponse(lstDownloads, (int)HttpStatusCode.BadRequest,
                        DownloadConstans.NoResultsFound, consolidateDownloads.Count);

                //var categoryName = masterDb.GetItem(new ID(new Guid(values.CategoryType)));

                lstDownloads.AddRange(from item in consolidateDownloads
                                      let subcategoryItem = item.Axes.GetAncestors().FirstOrDefault(x => x.TemplateID.ToString().Equals(DownloadConstans.SubCategoryTemplate))
                                      select new DownloadResults()
                                      {
                                          ItemName = item.Name,
                                          ItemId = item.ID.ToGuid(),
                                          Title = Convert.ToString(item.Fields["Title"]),
                                          SubCategoryType = (subcategoryItem != null) ? subcategoryItem.Name : string.Empty,
                                          CategoryType = subcategoryItem.Parent.Name,
                                          AvailableLanguages = GetLanagItem(item)
                                      });
                TotalRows = lstDownloads.Count;
                lstDownloads = lstDownloads.Skip(((Convert.ToInt32(values.SkipRows)) != 0) ? Convert.ToInt32((lstDownloads.Count() > values.SkipRows) ? values.SkipRows : 0) : DefaultSkipRows).Take((Convert.ToInt32(values.NoOfRecords) != 0) ? Convert.ToInt32(values.NoOfRecords) : DefaultNoOfRows).ToList();
                return GetDowloadByKeyResponse(lstDownloads, (int)HttpStatusCode.OK, DownloadConstans.Succesfull, TotalRows);
            }
        }

        /// <summary>
        /// Method to get downloads response codes and messages.
        /// </summary>
        /// <param name="downloads"></param>
        /// <param name="StatusCode"></param>
        /// <param name="StatusMessage"></param>
        /// <returns></returns>
        public HttpResponseMessage GetDownloadsResponse(List<Downloads> downloads, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (downloads.Count > 0)
            {
                var obj = new APIResponse()
                {
                    ResponseContent = serializer.Serialize(downloads),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new APIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            //return serializedResult;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        /// <summary>
        /// Method to get downloads response codes and messages.
        /// </summary>
        /// <param name="downloads"></param>
        /// <param name="StatusCode"></param>
        /// <param name="StatusMessage"></param>
        /// <returns></returns>
        public HttpResponseMessage GetALLDownloadsResponse(List<List<Downloads>> downloads, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            serializer.MaxJsonLength = Int32.MaxValue;
            if (downloads.Count > 0)
            {
                var obj = new APIResponse()
                {
                    ResponseContent = serializer.Serialize(downloads),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new APIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            //return serializedResult;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }


        public HttpResponseMessage GetDowloadByKeyResponse(List<DownloadResults> downloadItems, int StatusCode, string StatusMessage, int TotalCount)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (downloadItems.Count > 0)
            {
                var obj = new GetDownloadsByKeyAPIResponse()
                {
                    ResponseContent = serializer.Serialize(downloadItems),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage },
                    TotalRowCount = TotalCount
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new GetDownloadsByKeyAPIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage },
                    TotalRowCount = 0
                };
                serializedResult = serializer.Serialize(obj);
            }
            //return serializedResult;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        [HttpPost]
        public HttpResponseMessage CreateEditDownloadItem(DownloadItem values)
        {
            //fix for special/html chars
            DecodeHtml(ref values);
            var isItemCreateMode = (values.ItemId == Guid.Empty);

            var cultureCode = GetCultureCodeById(values.CultureGroupId);
            //culture validation
            if (string.IsNullOrEmpty(cultureCode) || cultureCode.Equals("invalid"))
                return CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);

            var itemLanguage = LanguageManager.GetLanguage(cultureCode, master);
            if (!isItemCreateMode)
            {
                var newLangItem = master.GetItem(new ID(values.ItemId), itemLanguage);
                if (newLangItem != null)
                {
                    using (new SecurityDisabler())
                    {
                        if (newLangItem.Versions.Count == 0)
                        {
                            newLangItem.Versions.AddVersion();
                            return GetDownloadItemById(new DownloadByIdModel() { Id = newLangItem.ID.ToString(), CultureGroupId = values.CultureGroupId });
                        }
                    }
                }
            }

            //Item validation
            var validationMsg = ValidateItem(values);
            if (!string.IsNullOrEmpty(validationMsg))
                return CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, validationMsg);


            Item categoryItem = null;
            Item subcategoryItem = null;
            var validationMsg1 = ValidateItemCategoryAndSubCategory(values, itemLanguage, out categoryItem, out subcategoryItem);
            if (!string.IsNullOrEmpty(validationMsg1))
                return CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, validationMsg1);


            try
            {
                //get template by category and subcat
                var template = master.GetTemplate(new ID(DownloadConstans.DownloadsTemplate));
                //if (template == null) return CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, "Not a valid template");
                var parentItem = master.GetItem(new DataUri(subcategoryItem.Paths.Path));
                Guid itemIdToReturn;
                //check downloads item exists with the same name
                if (DownloadItemExistWithSameName(values.ItemName, parentItem) && isItemCreateMode)
                    return CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, DownloadConstans.ItemExist);

                //get the workflow items
                var workflowItem = master.GetItem("{D7D3D8AB-69F5-4ECC-96DF-597F93D18A1A}");
                var workflowState = master.GetItem("{EAD4039A-12C5-411C-B474-14C53D366DEF}");
                using (new SecurityDisabler())
                {
                    var downloadItem = isItemCreateMode ? parentItem.Add(ItemUtil.ProposeValidItemName(values.ItemName.Trim()), template) :
                        master.GetItem(new ID(values.ItemId));

                    using (new LanguageSwitcher(itemLanguage))
                    {
                        #region create "en" default version
                        if (itemLanguage.Name != "en")
                        {
                            var enLanguage = LanguageManager.GetLanguage(fallbackLanguage, master);
                            if (enLanguage != null)
                            {
                                var enItem = master.GetItem(downloadItem.ID, enLanguage);
                                if (enItem.Versions.Count < 1)
                                {
                                    enItem.Editing.BeginEdit();
                                    enItem.Versions.AddVersion();
                                    if (workflowItem != null || workflowState != null)
                                    {
                                        //add workflow and flip it to final state
                                        enItem.Fields[FieldIDs.Workflow].Value = workflowItem.ID.ToString();
                                        enItem.Fields[FieldIDs.WorkflowState].Value = workflowState.ID.ToString();
                                    }
                                    enItem.Editing.EndEdit();
                                }
                            }
                        }
                        #endregion

                        var localizedItem = master.GetItem(downloadItem.ID, itemLanguage);

                        if (localizedItem.Versions.Count == 0)
                        {
                            //return CreateDowloadItemesponse((int)HttpStatusCode.OK, "Item Updated Successfully");
                            localizedItem.Editing.BeginEdit();
                            localizedItem.Versions.AddVersion();
                        }

                        #region Set Item Values

                        localizedItem.Editing.BeginEdit();
                        if (!string.IsNullOrEmpty(values.Title))
                            localizedItem.Fields["Title"].Value = values.Title;
                        if (!string.IsNullOrEmpty(values.MenuTitle))
                            localizedItem.Fields["MenuTitle"].Value = values.MenuTitle;
                        localizedItem.Fields["PageTitle"].Value = string.IsNullOrEmpty(values.PageTitle) ? "" : values.PageTitle;
                        if (!string.IsNullOrEmpty(values.Description))
                            localizedItem.Fields["Description"].Value = values.Description;

                        localizedItem.Fields["Version"].Value = string.IsNullOrEmpty(values.Version) ? "" : values.Version;
                        localizedItem.Fields["FileType"].Value = string.IsNullOrEmpty(values.FileType) ? "" : values.FileType;
                        localizedItem.Fields["Size"].Value = string.IsNullOrEmpty(values.Size) ? "" : values.Size;
                        localizedItem.Fields["Metatags-Other keywords"].Value = string.IsNullOrEmpty(values.MetaKeywords) ? "" : values.MetaKeywords;

                        CheckboxField newVersionCheckboxField = localizedItem.Fields["IsNewVersionAvailable"];
                        if (newVersionCheckboxField != null)
                            newVersionCheckboxField.Checked = values.IsNewVersionAvailable;

                        if (!string.IsNullOrEmpty(values.ReleaseDate.ToString()))
                        {
                            new DateField(new Field(localizedItem.Fields["ReleaseDate"].ID, localizedItem))
                            {
                                Value =
                                    DateUtil.ToIsoDate(Convert.ToDateTime(values.ReleaseDate))
                            };
                        }
                        ReferenceField referenceField = localizedItem.Fields["Subcategory"];
                        referenceField.Value = subcategoryItem.ID.ToString(); //Dropdown only      
                        MultilistField multiselectField = localizedItem.Fields["RelatedProducts"];
                        //return CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, IsRelatedProductsUpdated(multiselectField, values.RelatedProducts).ToString());

                        if (values.RelatedProducts != null && values.RelatedProducts.Count > 0 && IsRelatedProductsUpdated(localizedItem, values.RelatedProducts))
                        {
                            if (multiselectField.Items.Length > 0)
                            {
                                //remove all the related products.
                                foreach (var item in multiselectField.Items)
                                {
                                    multiselectField.Remove(item);
                                }
                            }
                            using (new LanguageSwitcher(LanguageManager.GetLanguage(fallbackLanguage, master)))
                            {
                                var items = master.SelectItems("fast:/sitecore/content/GlobalWeb/home//*[@@templateid = '" + DownloadConstans.ProductsTemplate + "']");

                                var lstItems = items.ToList();
                                var lstPro = (from item in values.RelatedProducts
                                              where lstItems.Where(x => x.Name.Equals(item.ProductName)).ToList().Count > 0
                                              select lstItems.FirstOrDefault(x => x.Name.Equals(item.ProductName))).ToList();
                                if (lstPro.Count > 0)
                                {
                                    foreach (var item in lstPro.Where(item => !multiselectField.Contains(item.ID.ToString())))
                                    {
                                        multiselectField.Add(item.ID.ToString());
                                    }
                                }
                            }
                        }
                        //get the general links 
                        var generalLinks = new GeneralLinks(localizedItem, "OptionalLinks");
                        if (values.OptionalLinks == null || values.OptionalLinks.Count == 0)
                        {
                            //remove all genral links
                            //generalLinks.LinkItems.Clear();
                            localizedItem["OptionalLinks"] = String.Empty;
                        }
                        else
                        {
                            var filteredLinks =
                                values.OptionalLinks.Where(
                                    op => op.Text != string.Empty && op.TargetUrl != String.Empty);
                            var optionalLinks = filteredLinks as OptionalLink[] ?? filteredLinks.ToArray();
                            if (optionalLinks.Any())
                            {
                                var linkBuilder = new StringBuilder();
                                linkBuilder.Append("<links>");
                                foreach (var item in optionalLinks)
                                {
                                    //GeneralLinkItem linkItem = new GeneralLinkItem() { Id = Guid.NewGuid().ToString(), LinkText = item.Text, Text = item.Text, Url = item.TargetUrl, LinkType = "external" };
                                    linkBuilder.Append(
                                        String.Format(
                                            "<link linkid=\"{0}\" text=\"{1}\" linktype=\"external\" url=\"{2}\" target=\"\" anchor=\"\" linktext=\"{3}\" />",
                                            Guid.NewGuid(), item.Text, item.TargetUrl, item.Text));
                                }
                                linkBuilder.Append("</links>");
                                localizedItem["OptionalLinks"] = linkBuilder.ToString();
                            }
                        }
                        if (!string.IsNullOrEmpty(values.MetaDescription))
                            localizedItem.Fields["MetaTitle"].Value = values.MetaDescription;

                        if (!string.IsNullOrEmpty(values.DownloadFilePath))
                            localizedItem.Fields["FilePath"].Value = values.DownloadFilePath;   //insert link 
                        //if (!string.IsNullOrEmpty(values.MetaKeywords))
                        //{
                        //    localizedItem.Fields["Metatags-Other keywords"].Value = values.MetaKeywords;
                        //}
                        if (values.ReplacementDownload != null)
                        {
                            LinkField newVersionDwl = localizedItem.Fields["ReplacementDownloadPath"];
                            if (String.IsNullOrEmpty(values.ReplacementDownload.DownloadId))
                                newVersionDwl.Clear();
                            else
                            {
                                newVersionDwl.TargetID = new ID(values.ReplacementDownload.DownloadId);
                                newVersionDwl.LinkType = "internal";
                            }
                        }


                        if (!string.IsNullOrEmpty(values.LoginRequired.ToString()))
                        {
                            CheckboxField loginRequired = localizedItem.Fields["LogOnRequired"];
                            loginRequired.Checked = values.LoginRequired;
                        }
                        CheckboxField prodRegistrationRequired = localizedItem.Fields["RedirectToMyAnritsu"];
                        prodRegistrationRequired.Checked = values.RedirectToMyAnritsu;
                        CheckboxField showInBreadCrumb = localizedItem.Fields["ShowInBreadcrumb"];
                        showInBreadCrumb.Checked = values.ShowInBreadcrumb;

                        itemIdToReturn = localizedItem.ID.ToGuid();

                        if (workflowItem != null || workflowState != null)
                        {
                            //add workflow and flip it to final state
                            localizedItem.Fields[FieldIDs.Workflow].Value = workflowItem.ID.ToString();
                            localizedItem.Fields[FieldIDs.WorkflowState].Value = workflowState.ID.ToString();
                        }
                        //item.Editing.EndEdit();

                        localizedItem.Editing.EndEdit();

                        #endregion

                        if (values.AvailableLanguages != null && values.AvailableLanguages.Count > 0 && GetCultureCodeById(values.CultureGroupId).Equals(fallbackLanguage))
                        {
                            //Create new version for the available languages if not exist else overwrite the Item with en values
                            values.ItemId = itemIdToReturn;
                            CreateOrSaveToMultipleLanguages(values);
                        }
                    }
                }
                return isItemCreateMode ? GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = itemIdToReturn },
                    (int)HttpStatusCode.OK, DownloadConstans.ItemCreateSuccess) :
                     CreateDowloadItemesponse((int)HttpStatusCode.OK, DownloadConstans.ItemUpdateSuccess);
            }
            catch (Exception ex)
            {
                return CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, ex.StackTrace + ex.InnerException.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetDownloadItemById(DownloadByIdModel values)
        {
            var dlItem = new GetDownloadByIDModel();
            var cultureCode = GetCultureCodeById(values.CultureGroupId);
            if (string.IsNullOrEmpty(cultureCode)) return GetDowloadItemByIdResponse(dlItem, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);
            var itemLanguage = LanguageManager.GetLanguage(cultureCode, masterDb);
            if (string.IsNullOrEmpty(values.Id)) return GetDowloadItemByIdResponse(dlItem, (int)HttpStatusCode.BadRequest, "Please provide item id ");
            if (masterDb.GetItem(values.Id.Trim(), itemLanguage) == null) return GetDowloadItemByIdResponse(dlItem, (int)HttpStatusCode.BadRequest, "Not a valid item id");
            var downloadItem = masterDb.GetItem(values.Id.Trim(), itemLanguage);
            if (downloadItem.Versions.Count.Equals(0)) return GetDowloadItemByIdResponse(dlItem, (int)HttpStatusCode.BadRequest, "No language version");

            dlItem.ItemName = downloadItem.Name;
            dlItem.Title = downloadItem["Title"];
            dlItem.Description = downloadItem["Description"];
            dlItem.DownloadFilePath = downloadItem["FilePath"];

            //set ReplacementDownloadPath
            if (downloadItem["ReplacementDownloadPath"] != null)
            {
                LinkField newVersionDwl = downloadItem.Fields["ReplacementDownloadPath"];
                if (newVersionDwl != null && !string.IsNullOrEmpty(Convert.ToString(newVersionDwl.TargetID)))
                {
                    if (newVersionDwl.TargetItem != null)
                    {
                        dlItem.ReplacementDownload = new ReplacementDownload()
                        {
                            DownloadName = newVersionDwl.TargetItem.Name,
                            DownloadId = newVersionDwl.TargetItem.ID.ToString()
                        };
                    }
                }
            }


            dlItem.ReleaseDate = DateUtil.IsoDateToDateTime(downloadItem["ReleaseDate"], DateTime.Now);
            var categoryItem = downloadItem.Axes.GetAncestors().FirstOrDefault(x => x.TemplateID.ToString().
                Equals(DownloadConstans.CategoryTemplate));
            var subCategoryItem = downloadItem.Axes.GetAncestors().FirstOrDefault(x => x.TemplateID.ToString()
                .Equals(DownloadConstans.SubCategoryTemplate));

            dlItem.CategoryType = categoryItem.ID.ToString();
            dlItem.SubCategoryType = subCategoryItem.ID.ToString();
            dlItem.MenuTitle = downloadItem["MenuTitle"];
            dlItem.PageTitle = downloadItem["PageTitle"];
            dlItem.Version = downloadItem["Version"];
            dlItem.ShowInBreadcrumb = (downloadItem["ShowInBreadcrumb"].Equals("1"));
            dlItem.FileType = downloadItem["FileType"];
            dlItem.Size = downloadItem["Size"];
            dlItem.IsNewVersionAvailable = (downloadItem["IsNewVersionAvailable"].Equals("1"));
            dlItem.AllowHardCopyRequest = (downloadItem["AllowHardCopyRequest"].Equals("1"));
            dlItem.LoginRequired = (downloadItem["LogOnRequired"].Equals("1"));
            dlItem.RedirectToMyAnritsu = (downloadItem["RedirectToMyAnritsu"].Equals("1"));
            dlItem.MetaDescription = downloadItem["MetaDescription"];
            dlItem.MetaKeywords = downloadItem["MetaKeywords"];
            dlItem.CultureGroupId = values.CultureGroupId;
            dlItem.ItemId = downloadItem.ID.ToGuid();
            dlItem.MetaKeywords = downloadItem["Metatags-Other keywords"];
            dlItem.AvailableLanguages = GetLanagItem(downloadItem);
            //check if subcategory is software download
            dlItem.DownloadUrl = subCategoryItem.Key.Equals(DriversSoftwareDownloads) ?
                GetDownloadDetailePageUrl(downloadItem) : GetS3ObjectUrl(dlItem.DownloadFilePath);

            MultilistField multiselectField = downloadItem.Fields["RelatedProducts"];
            dlItem.RelatedProducts = new List<Product>();
            if (multiselectField == null)
            { //TODO: handle case that field does not exist
            }
            else
            {
                var items = multiselectField.GetItems();
                if (items != null && items.Length > 0)
                {
                    foreach (var product in items.Select(t => new Product { ProductName = t.Name }))
                    {
                        dlItem.RelatedProducts.Add(product);
                    }
                }
            }
            var optionalGeneralLinks = new GeneralLinks(downloadItem, downloadItem.Fields["OptionalLinks"]);
            dlItem.OptionalLinks = new List<OptionalLink>();
            if (optionalGeneralLinks.LinkItems.Count <= 0)
                return GetDowloadItemByIdResponse(dlItem, (int)HttpStatusCode.OK, DownloadConstans.FetchSuccessful);
            foreach (var item in optionalGeneralLinks.LinkItems)
            {
                dlItem.OptionalLinks.Add(new OptionalLink() { TargetUrl = item.Url, Text = item.LinkText });
            }
            return GetDowloadItemByIdResponse(dlItem, (int)HttpStatusCode.OK, DownloadConstans.FetchSuccessful);
        }

        public HttpResponseMessage GetDowloadItemByIdResponse(GetDownloadByIDModel downloadItems, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (downloadItems.ItemId.ToString() != DownloadConstans.NullGuid)
            {
                var obj = new APIResponse()
                {
                    ResponseContent = serializer.Serialize(downloadItems),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new APIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            //return serializedResult;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        [HttpPost]
        public HttpResponseMessage DeleteDownloadItemById(PublishDownload publishParams)
        {
            try
            {
                if (publishParams.CultureGroupId != 0)
                {
                    //delete item by id and lang
                    //Language validation
                    var lang = GetCultureCodeById(publishParams.CultureGroupId);
                    if (lang.Equals("invalid")) return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = Guid.Empty }, (int)HttpStatusCode.BadRequest, "Invalid Language");
                    //get language
                    var itemLanguage = LanguageManager.GetLanguage(lang, masterDb);
                    //Item validation
                    if (string.IsNullOrEmpty(publishParams.ItemId)) return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = Guid.Empty }, (int)HttpStatusCode.BadRequest, "Invalid Item Id");
                    var localizedItem = masterDb.GetItem(new ID(publishParams.ItemId), itemLanguage);
                    if (localizedItem == null || localizedItem.Versions.Count <= 0)
                        return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = Guid.Empty },
                            (int)HttpStatusCode.BadRequest, "Version count is 0 for the item");
                    return DeleteDownload(publishParams, localizedItem, itemLanguage);
                }
                //delete item by id (all version will be deleted)
                var dlItem = masterDb.GetItem(new ID(publishParams.ItemId));
                return DeleteDownload(publishParams, dlItem, null);
            }
            catch (Exception e)
            {
                return DeleteDowloadItemByIdResponse((int)HttpStatusCode.BadRequest, e.StackTrace + e.InnerException.Message);
            }
        }

        public HttpResponseMessage DeleteDowloadItemByIdResponse(int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;

            var obj = new APIResponse()
            {
                ResponseContent = serializer.Serialize(string.Empty),
                ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
            };
            serializedResult = serializer.Serialize(obj);

            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        [HttpPost]
        public HttpResponseMessage PublishDownload(PublishDownload publishParams)
        {
            try
            {
                //Language validation
                var selectedLang = GetCultureCodeById(publishParams.CultureGroupId);
                if (selectedLang.Equals("invalid"))
                    return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = Guid.Empty },
                        (int)HttpStatusCode.BadRequest,
                        "Invalid Language: " + publishParams.CultureGroupId + publishParams.ItemId);
                //get language
                var itemLanguage = LanguageManager.GetLanguage(selectedLang, master);
                var enItemLanguage = LanguageManager.GetLanguage(fallbackLanguage, master);
                //mulitple languages
                var languages = new List<Language>();
                //Item validation
                if (string.IsNullOrEmpty(publishParams.ItemId))
                    return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = Guid.Empty },
                        (int)HttpStatusCode.BadRequest, "Invalid Item Id");

                var localizedItem = master.GetItem(new ID(publishParams.ItemId), itemLanguage);
                if (localizedItem == null || localizedItem.Versions.Count <= 0)
                    return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = Guid.Empty },
                        (int)HttpStatusCode.BadRequest, "Version count is 0 for the item");
                languages.Add(enItemLanguage);
                if (publishParams.Languages != null && publishParams.Languages.Count > 0)
                {
                    languages.AddRange(publishParams.Languages.Select(cultureId => GetCultureCodeById(Convert.ToInt32(cultureId))).Select(cultureCode => LanguageManager.GetLanguage(cultureCode, master)).Where(lang => lang != null));
                }
                else
                {
                    languages.Add(itemLanguage);
                }
                //get the target databases
                //web db is the default database
                var targetDBs = new List<Database>() { web };
                //add production dbs
                //if (parms.PublishToLive)
                //    targetDbs.AddRange(ProdDatabases.ToArray());
                if (publishParams.PublishToLive)
                {
                    targetDBs.AddRange(ProdDatabases.ToArray());
                }
                //publish to target database
                PublishItemWithMultipleLanguages(localizedItem, targetDBs.ToArray(), languages.ToArray());
                //send the response 
                return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = Guid.Empty },
               (int)HttpStatusCode.OK, string.Format("Item {0} published to Target DBs:{1} in Published Languages: {2}", localizedItem.ID, GetTargetDatabseAsString(targetDBs), GetPublishedLanguages(languages)));
            }
            catch (Exception ex)
            {
                return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = Guid.Empty },
                    (int)HttpStatusCode.InternalServerError,
                    ex.Message);
            }
        }

        private string GetPublishedLanguages(List<Language> languages)
        {
            if (languages == null || languages.Count < 1) return string.Empty;
            return languages.Select(l => l.Name).Aggregate((i, j) => i + "|" + j);
        }
        private string GetTargetDatabseAsString(List<Database> databases)
        {
            if (databases == null || databases.Count < 1) return string.Empty;
            return databases.Select(l => l.Name).Aggregate((i, j) => i + "|" + j);
        }

        public Handle PublishItemWithMultipleLanguages(Item item, Database[] targetsDatabases, Language[] languages, bool deep = true, bool compareRevisions = false)
        {
            using (new SecurityDisabler())
            {
                return PublishManager.PublishItem(item, targetsDatabases, languages,
                    false, compareRevisions, false);
            }
        }

        public void PublishItem(Item item, Publisher publisher, List<string> publishToLanguages = null)
        {
            using (new SecurityDisabler())
            {
                publisher.Options.RootItem = item;
                publisher.Options.RepublishAll = true;
                publisher.Options.Deep = true;
                publisher.PublishAsync();
                //publish selected languages 
                if (publishToLanguages == null || publishToLanguages.Count <= 0) return;
                foreach (var langItem in publishToLanguages.Select(cultureId => GetCultureCodeById(Convert.ToInt32(cultureId))).Select(cultureCode => LanguageManager.GetLanguage(cultureCode, master)).Select(itemLanguage => master.GetItem(item.ID, itemLanguage)).Where(newLangItem => newLangItem != null))
                {
                    //create new publisher
                    var publishernew = new Publisher(new PublishOptions(master, publisher.Options.TargetDatabase,
                        PublishMode.SingleItem, langItem.Language, DateTime.Now));
                    publishernew.Options.RootItem = langItem;
                    publishernew.Options.RepublishAll = true;
                    publishernew.Options.Deep = true;
                    publishernew.PublishAsync();
                }
            }
        }

        public HttpResponseMessage DeleteDownload(PublishDownload parms, Item dlitem, Language itemLang)
        {
            using (new SecurityDisabler())
            {
                //preapre languages to publish
                var langs = new List<Language>();
                if (itemLang != null)
                    langs.Add(itemLang);
                else
                    langs.AddRange(dlitem.Languages);
                //prepare target databases
                //add default Database
                var targetDbs = new List<Database>() { web };
                if (parms.PublishToLive)
                    targetDbs.AddRange(ProdDatabases.ToArray());
                //signle item delete
                if (itemLang != null)
                {
                    var localizedItem = dlitem.Database.GetItem(dlitem.ID, itemLang);
                    if (localizedItem != null)
                    {
                        localizedItem.Versions.RemoveAll(false);
                        PublishItemWithMultipleLanguages(localizedItem, targetDbs.ToArray(), langs.ToArray(), false, false);
                    }
                    return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = dlitem.ID.ToGuid() },
                   (int)HttpStatusCode.OK,
                   String.Format("Item Deleted from {0} and {1} Database", GetTargetDatabseAsString(targetDbs), GetPublishedLanguages(langs)));
                }

                using (new EditContext(dlitem))
                {
                    dlitem.Publishing.NeverPublish = true;
                }

                var handle = PublishItemWithMultipleLanguages(dlitem, targetDbs.ToArray(), langs.ToArray(), false, false);
                var status = PublishManager.GetStatus(handle).State;
                while (status != JobState.Finished)
                {
                    //check for 500 milliseconds
                    System.Threading.Thread.Sleep(500);
                    status = PublishManager.GetStatus(handle).State;
                }
                //delete after the job has completed
                dlitem.Delete();
                return GetDowloadItemByIdResponse(new GetDownloadByIDModel() { ItemId = dlitem.ID.ToGuid() },
                    (int)HttpStatusCode.OK,
                    String.Format("Item Deleted from {0} and {1} Database", GetTargetDatabseAsString(targetDbs), GetPublishedLanguages(langs)));
            }
        }
        public bool DeleteDownloadItem(string environment, Item dlitem, Language itemLang)
        {
            if (environment == Environment.master.ToString())
            {
                dlitem = itemLang != null ? master.GetItem(dlitem.ID, itemLang) : master.GetItem(dlitem.ID);
            }
            else if (environment == Environment.web.ToString())
            {
                dlitem = itemLang != null ? web.GetItem(dlitem.ID, itemLang) : web.GetItem(dlitem.ID);
            }
            else if (environment == Environment.production.ToString())
            {
                if (ProdDatabases.Count > 0)
                {
                    var prodDb = Factory.GetDatabase("production");
                    dlitem = itemLang != null ? prodDb.GetItem(dlitem.ID, itemLang) : prodDb.GetItem(dlitem.ID);
                }
            }
            if (dlitem == null) return false;
            if (itemLang != null)
            {
                dlitem.Versions.RemoveVersion();
            }
            else
                dlitem.Delete();
            return true;
        }

        /// <summary>
        /// Get Categories list by region
        /// </summary>
        /// <param name="cultureGroupId"></param>
        /// <returns></returns>
        public HttpResponseMessage GetCategories(int cultureGroupId)
        {
            List<DropDownListModel> lstCategories = new List<DropDownListModel>();
            List<Item> lstCats = new List<Item>();
            int cultureGrpID = (cultureGroupId != 0) ? cultureGroupId : DefaultCultureId;

            string cultureCode = GetCultureCodeById(cultureGrpID);
            if (string.IsNullOrEmpty(cultureCode)) return GetCategoriesResponse(lstCategories, (int)HttpStatusCode.BadRequest, DownloadConstans.NoProperInputs);
            Language itemLanguage = LanguageManager.GetLanguage(cultureCode, Context.Database);
            using (new LanguageSwitcher(itemLanguage))
            {
                var query = string.Format("fast:{0}//*[@@templateid='{1}']", StringExtensions.EscapePath("/sitecore/content/Global/downloads"), DownloadConstans.CategoryTemplate);
                var CategoryItems = masterDb.SelectItems(query);
                lstCats = CategoryItems.Where(x => x.Language.Equals(itemLanguage)).ToList();
            }
            if (lstCats.Count <= 0) return GetCategoriesResponse(lstCategories, (int)HttpStatusCode.BadRequest, DownloadConstans.NoProperInputs);
            foreach (var item in lstCats)
            {
                DropDownListModel model = new DropDownListModel();
                model.Name = (String.IsNullOrEmpty(item["Value"]) ? item.Name : item["Value"].Trim());
                model.ID = item.ID.ToString();
                lstCategories.Add(model);
            }
            if (lstCategories.Count <= 0)
            {
                return GetCategoriesResponse(lstCategories, (int)HttpStatusCode.NotFound, DownloadConstans.NoResultsFound);
            }
            return GetCategoriesResponse(lstCategories, (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
        }

        /// <summary>
        /// Get Sub Categories list by region
        /// </summary>
        /// <param name="cultureGroupId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public HttpResponseMessage GetSubCategories(int cultureGroupId, string categoryId)
        {
            List<DropDownListModel> lstSubCategories = new List<DropDownListModel>();
            if (string.IsNullOrEmpty(categoryId)) return GetCategoriesResponse(lstSubCategories, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCategory);
            int cultureGrpID = (cultureGroupId != 0) ? cultureGroupId : DefaultCultureId;
            string cultureCode = GetCultureCodeById(cultureGrpID);
            if (string.IsNullOrEmpty(cultureCode)) return GetCategoriesResponse(lstSubCategories, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);
            Language itemLanguage = LanguageManager.GetLanguage(cultureCode, Context.Database);

            ID catId = new ID(new Guid(categoryId.Trim()));
            Item CategoryItem = masterDb.GetItem(catId, itemLanguage);
            if (CategoryItem == null) return GetCategoriesResponse(lstSubCategories, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCategory);
            List<Item> lstChildCategories = CategoryItem.Children.ToList();
            foreach (var item in lstChildCategories)
            {
                DropDownListModel model = new DropDownListModel();
                model.Name = (String.IsNullOrEmpty(item["Value"]) ? item.Name : item["Value"].Trim());
                model.ID = item.ID.ToString();
                lstSubCategories.Add(model);
            }
            if (lstSubCategories.Count <= 0) return GetCategoriesResponse(lstSubCategories, (int)HttpStatusCode.BadRequest, DownloadConstans.NoSubCategories);
            return GetCategoriesResponse(lstSubCategories, (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
        }

        public HttpResponseMessage GetCategoriesResponse(List<DropDownListModel> categories, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (categories.Count > 0)
            {
                var obj = new APIResponse()
                {
                    ResponseContent = serializer.Serialize(categories),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new APIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            //return serializedResult;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        [HttpPost]
        public string LogDownloadRequest(DownloadLog values)
        {
            try
            {
                // return values.UserIpAddress;
                BllDownloads.Download_AddLog(values);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "SUCCESS";
        }

        #endregion

        #region Product/Model info
        /// <summary>
        /// This Method is used for search engine functionality
        /// </summary>
        /// <param name="cultureGroupId"></param>
        /// <param name="modelNumber"></param>
        /// <returns></returns>
        [HttpPost]
        public DataTable GetModelsByKey()
        {
            List<String> lstModelnumbers = new List<string>();
            DataTable dataTable = new DataTable();
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;

            Language itemLanguage = LanguageManager.GetLanguage(Context.Language.Name, web);

            var query = string.Format("fast:{0}//*[@@templateid='{1}']", StringExtensions.EscapePath("/sitecore/content/global/products"), "{94757055-E1B4-4B0D-B0E7-7BDBC26861D8}");
            var Items = web.SelectItems(query);
            var lstItems = Items.ToList();
            lstItems = lstItems.Distinct(new ItemEqualityComparer()).ToList();

            if (lstItems.Count <= 0)
            {
                return GetResponseStatusObject((int)HttpStatusCode.NoContent, DownloadConstans.NoResultsFound);
            }

            lstItems = lstItems.Where(x => x != null && x.Fields["ModelNumber"] != null && !string.IsNullOrEmpty(x["ModelNumber"])).ToList();
            lstItems = lstItems.Distinct(new ItemEqualityComparer()).ToList();
            lstItems = lstItems.Distinct().ToList();
            foreach (var item in lstItems)
            {
                lstModelnumbers.Add((item["ModelNumber"]).Replace("<br>", "/"));
            }
            if (lstModelnumbers.Count <= 0)
            {
                return GetResponseStatusObject((int)HttpStatusCode.NoContent, DownloadConstans.NoResultsFound);
            }
            dataTable.Columns.Add("ModelNumber", typeof(string));
            dataTable.Columns.Add("ResponseStatusCode", typeof(int));
            dataTable.Columns.Add("ResponseStatusMessage", typeof(string));
            foreach (var item in lstModelnumbers)
            {
                dataTable.Rows.Add(item, (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
            }
            return dataTable;
        }

        [HttpPost]
        public HttpResponseMessage GetProductsByModelNumber(ModelNumber values)
        {
            int cultureGrpID = (values.cultureGroupId != 0) ? values.cultureGroupId : 1;
            Item[] Items;
            string[] modelnos = null;
            List<ProdInfo> outputObject = new List<ProdInfo>();
            string cultureCode = GetCultureCodeById(cultureGrpID);
            var targetDb = Context.Database;//ValidateTargetDb(values.targetDb);
            if (targetDb == null) return GetProductsResponse(outputObject, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidDatabase);

            Language itemLanguage = LanguageManager.GetLanguage(cultureCode, targetDb);
            if (String.IsNullOrEmpty(cultureCode)) return GetProductsResponse(outputObject, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);
            if (String.IsNullOrEmpty(values.modelNumber))
            {
                return GetProductsResponse(outputObject, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidModelNumber);
            }
            else
            {
                if (values.modelNumber.Contains(','))
                {
                    modelnos = values.modelNumber.Trim().Split(',');
                }
            }
            Items = targetDb.SelectItems("fast:/sitecore/content//*[@@templateid = '" + DownloadConstans.ProductsTemplate + "']");
            var lstItems = Items.ToList();
            List<Item> lstProducts = new List<Item>();
            List<Item> lstFilteredProds = new List<Item>();
            // return GetProductsResponse(outputObject, (int)HttpStatusCode.NotFound, lstItems.Count.ToString());
            if (lstItems.Count <= 0)
            {
                return GetProductsResponse(outputObject, (int)HttpStatusCode.NotFound, DownloadConstans.NoResultsFound);
            }
            lstProducts.AddRange(from item in lstItems
                                 where (item["SelectProduct"] != null && targetDb.GetItem(item["SelectProduct"]) != null)
                                 select targetDb.GetItem(item["SelectProduct"], itemLanguage));
            //StringBuilder sb = new StringBuilder();
            //foreach (var VARIABLE in lstProducts)
            //{
            //    sb.Append(VARIABLE["ModelNumber"].ToString() + "@");
            //}
            //return GetProductsResponse(outputObject, (int)HttpStatusCode.NotFound, lstProducts.Count.ToString()+"!"+sb.ToString());
            if (lstProducts.Count <= 0)
            {
                return GetProductsResponse(outputObject, (int)HttpStatusCode.NotFound, DownloadConstans.NoResultsFound);
            }
           
            if (values.modelNumber == "ALLITEMS")
            {
                string ImgPath = string.Empty, sts = string.Empty, ThumnPath = string.Empty;

                foreach (Item item in lstProducts)
                {
                    ProdInfo model = new ProdInfo();
                    if (item["Image"] != null)
                    {
                        ImageField imgField = ((ImageField)item.Fields["Image"]);
                        if (targetDb.GetItem(imgField.MediaID, itemLanguage) != null)
                        {
                            MediaItem mediaItem = targetDb.GetItem(imgField.MediaID, itemLanguage);
                            ImgPath = MediaManager.GetMediaUrl(mediaItem);
                        }
                    }
                    if (item["Thumbnail"] != null)
                    {
                        ImageField imgField = ((ImageField)item.Fields["Thumbnail"]);
                        if (targetDb.GetItem(imgField.MediaID, itemLanguage) != null)
                        {
                            MediaItem mediaItem = targetDb.GetItem(imgField.MediaID, itemLanguage);
                            ThumnPath = MediaManager.GetMediaUrl(mediaItem);
                        }
                    }
                    string productUrl = lstItems.Where(x => x.Fields["SelectProduct"].ToString() == item.ID.ToString()).FirstOrDefault().Paths.FullPath;

                    string[] segments = productUrl.Split(new char[] { '/' }, 6);
                    productUrl = Request.RequestUri.Scheme + "://" + GetHost() + "/" + Context.Language.CultureInfo + "/" + segments[5];

                    model.GlobalProductItemName = item.Name;
                    model.PageURL = productUrl;
                    model.ModelNumber = item["ModelNumber"];
                    model.ProductName = item["ProductName"];
                    model.ProductImageURL = ImgPath;
                    model.ProductSmallImageURL = ThumnPath;
                    outputObject.Add(model);
                    ThumnPath = ImgPath = string.Empty;
                }
                return GetProductsResponse(outputObject, (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
            }

            if (modelnos != null)
            {
                foreach (var model in modelnos)
                {
                    foreach (Item item in lstProducts)
                    {
                        if (item["ModelNumber"] != null && item["ModelNumber"].ToLowerInvariant() == model.ToLowerInvariant())
                        {
                            lstFilteredProds.Add(item);
                        }
                        else if (item["ModelNumber"] != null && String.IsNullOrEmpty(item["ModelNumber"].ToLowerInvariant()))
                        {
                            Item productItem = targetDb.GetItem(item.ID, LanguageManager.GetLanguage(fallbackLanguage, targetDb));
                            if (productItem["ModelNumber"] != null && productItem["ModelNumber"].ToLowerInvariant() == model.ToLowerInvariant())
                                lstFilteredProds.Add(productItem);
                        }
                    }
                }
            }
            else
            {
                foreach (Item item in lstProducts)
                {
                    if (item["ModelNumber"] != null && item["ModelNumber"].ToLowerInvariant() == values.modelNumber.Trim().ToLowerInvariant())
                    {
                        lstFilteredProds.Add(item);
                    }
                    else if (item["ModelNumber"] != null && String.IsNullOrEmpty(item["ModelNumber"].ToLowerInvariant()))
                    {
                        Item productItem = targetDb.GetItem(item.ID, LanguageManager.GetLanguage(fallbackLanguage, targetDb));
                        if (productItem["ModelNumber"] != null && productItem["ModelNumber"].ToLowerInvariant() == values.modelNumber.Trim().ToLowerInvariant())
                            lstFilteredProds.Add(productItem);
                    }
                }

            }

            if (lstFilteredProds.Count <= 0)
            {
                return GetProductsResponse(outputObject, (int)HttpStatusCode.NotFound, DownloadConstans.NoResultsFound);
            }

            string ImagePath = string.Empty, status = string.Empty, ThumbnailPath = string.Empty;
            foreach (Item item in lstFilteredProds)
            {
                ProdInfo model = new ProdInfo();
                if (item["Image"] != null)
                {
                    ImageField imgField = ((ImageField)item.Fields["Image"]);
                    if (targetDb.GetItem(imgField.MediaID, itemLanguage) != null)
                    {
                        MediaItem mediaItem = targetDb.GetItem(imgField.MediaID, itemLanguage);
                        ImagePath = MediaManager.GetMediaUrl(mediaItem);
                    }
                }
                if (item["Thumbnail"] != null)
                {
                    ImageField imgField = ((ImageField)item.Fields["Thumbnail"]);
                    if (targetDb.GetItem(imgField.MediaID, itemLanguage) != null)
                    {
                        MediaItem mediaItem = targetDb.GetItem(imgField.MediaID, itemLanguage);
                        ThumbnailPath = MediaManager.GetMediaUrl(mediaItem);
                    }
                }
                string productUrl = lstItems.Where(x => x.Fields["SelectProduct"].ToString() == item.ID.ToString()).FirstOrDefault().Paths.FullPath;

                string[] segments = productUrl.Split(new char[] { '/' }, 6);
                productUrl = Request.RequestUri.Scheme + "://" + GetHost() + "/" + Context.Language.CultureInfo + "/" + segments[5];
                //Context.Request.Url.Host + Context.Request.Url.Segments[0] + Context.Request.Url.Segments[1] + segments[3];
                //Request.RequestUri.DnsSafeHost + "/" + Context.Language.CultureInfo + "/" + segments[5];
                model.GlobalProductItemName = item.Name;
                model.PageURL = productUrl;
                model.ModelNumber = item["ModelNumber"];
                model.ProductName = item["ProductName"];
                model.ProductImageURL = ImagePath;
                model.ProductSmallImageURL = ThumbnailPath;
                outputObject.Add(model);
                ThumbnailPath = ImagePath = string.Empty;
            }
            return GetProductsResponse(outputObject, (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
        }

        /// <summary>
        /// Method to get products response codes and messages.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="StatusCode"></param>
        /// <param name="StatusMessage"></param>
        /// <returns></returns>
        public HttpResponseMessage GetProductsResponse(List<ProdInfo> products, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (products.Count > 0)
            {
                var obj = new APIResponse()
                {
                    ResponseContent = serializer.Serialize(products),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new APIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        /// <summary>
        /// Method for Search Model Numbers functionality
        /// </summary>
        /// <param name="models"></param>
        /// <param name="StatusCode"></param>
        /// <param name="StatusMessage"></param>
        /// <returns></returns>
        public HttpResponseMessage GetModelsByKeyResponse(List<String> models, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (models.Count > 0)
            {
                var obj = new APIResponse()
                {
                    ResponseContent = serializer.Serialize(models),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new APIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            //return serializedResult;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        /// <summary>
        /// Method for Products Search by Key
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetProductsBySearchKey(ProductSearchModel values)
        {
            //get default culture id
            //var cultureGrpID = (values.cultureGroupId != 0) ? values.cultureGroupId : 1;
            var outputObject = new List<DropDownListModel>();
            //var cultureCode = GetCultureCodeById(cultureGrpID);

            //var itemLanguage = LanguageManager.GetLanguage(cultureCode, masterDb);
            //if (String.IsNullOrEmpty(cultureCode)) return GetProductsBySearchKeyesponse(outputObject, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);

            if (String.IsNullOrEmpty(values.ProductName)) return GetProductsBySearchKeyesponse(outputObject, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidProduct);

            //filter the producst from master db 
            var lstItems = masterDb.SelectItems("fast:/sitecore/content/GlobalWeb/home//*[@@templateid = '" + DownloadConstans.ProductsTemplate + "' and @@key = '%" + values.ProductName.ToLower() + "%']").ToList();
            if (lstItems.Count <= 0)
                return GetProductsBySearchKeyesponse(outputObject, (int)HttpStatusCode.NotFound, "No Items Found");
            outputObject.AddRange(lstItems.Select(item => new DropDownListModel() { ID = item.ID.ToString(), Name = item.Name }));
            return GetProductsBySearchKeyesponse(outputObject, (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
        }

        public HttpResponseMessage GetProductsBySearchKeyesponse(List<DropDownListModel> products, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (products.Count > 0)
            {
                var obj = new APIResponse()
                {
                    ResponseContent = serializer.Serialize(products),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new APIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }
        #endregion

        #region Helper methods
        private List<Downloads> GetDownloadsForModelNumber(List<Item> lstItems, string modelNumber,  Language itemLanguage, string includeRegistrationRequiredDwl = "true")
        {
           
            List<Downloads> lstDownloads = new List<Downloads>();
            if (includeRegistrationRequiredDwl.Equals("false", StringComparison.InvariantCultureIgnoreCase))
                lstItems = lstItems.Where(item => !item["RedirectToMyAnritsu"].ToString().Equals("1", StringComparison.InvariantCultureIgnoreCase)).ToList();
            if (lstItems.Count <= 0)
                return null;
            try
            {
                List<Item> outputList = new List<Item>();
                var currentDb = Context.Database;
                foreach (Item downloaditem in lstItems.Where(item => !String.IsNullOrEmpty(item["RelatedProducts"])))
                {
                    if (downloaditem["RelatedProducts"].Contains("|"))
                    {
                        string[] words = downloaditem["RelatedProducts"].Split('|');
                        outputList.AddRange(from id in words
                                            let regionalProductItem = currentDb.Items.GetItem(new ID(id), itemLanguage)
                                            where regionalProductItem != null && !String.IsNullOrEmpty(regionalProductItem.Name)
                                            && regionalProductItem.Name.Equals(modelNumber, StringComparison.InvariantCultureIgnoreCase)
                                            select downloaditem);
                    }
                    else
                    {
                        if (currentDb.GetItem(new ID(downloaditem["RelatedProducts"]), itemLanguage) != null)
                        {

                            if (currentDb.GetItem(new ID(downloaditem["RelatedProducts"]), itemLanguage).Name.ToLowerInvariant() == modelNumber.Trim().ToLowerInvariant())
                            {
                                outputList.Add(downloaditem);
                            }
                        }
                    }
                }

                    if (outputList.Count > 0)
                    {


                        outputList = outputList.Distinct(new ItemEqualityComparer()).ToList();
                        foreach (Item item in outputList)
                        {
                            Downloads model = new Downloads();
                            model.Title = item["Title"];
                            model.FilePath = item["FilePath"];
                            model.CategoryType = currentDb.GetItem(new ID(item["Subcategory"]), itemLanguage).Parent["Value"];

                            Item downloadDetailPage = currentDb.GetItem(new ID(item["Subcategory"]), itemLanguage);
                            if (downloadDetailPage != null)
                            {
                                if (downloadDetailPage["Key"] == DownloadConstans.DetailsPageKey)
                                {
                                    model.DownloadPath = GetDownloadDetailePageUrl(item);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(item["FilePath"]))
                                    {
                                        if (item["FilePath"].StartsWith("http") || item["FilePath"].StartsWith("https"))
                                        {
                                            model.DownloadPath = item["FilePath"];
                                        }
                                        else
                                        {
                                            model.DownloadPath = GetS3ObjectUrl(item["FilePath"]);
                                        }
                                    }

                                }
                            }
                            model.ModelNumber = modelNumber.Trim();
                            model.Version = item["Version"];
                            model.Size = item["Size"];
                            model.RedirectToMyAnritsu = item["RedirectToMyAnritsu"];
                            model.Created = DateUtil.IsoDateToDateTime(item["__Created"]).ToUniversalTime();
                            model.Updated = DateUtil.IsoDateToDateTime(item["__Updated"]).ToUniversalTime();
                            model.ReleaseDate = DateUtil.IsoDateToDateTime(item["ReleaseDate"]).ToUniversalTime();
                            lstDownloads.Add(model);
                        }
                    }
             }
            catch(Exception ex)
            {

            }
            return lstDownloads;
        }
        private bool IsRelatedProductsUpdated(Item localizedItem, List<Product> prodList)
        {
            if (!localizedItem.Fields["RelatedProducts"].ContainsStandardValue)
                return true;
            MultilistField multiselectField = localizedItem.Fields["RelatedProducts"];
            var pords = multiselectField.GetItems();
            if (localizedItem.Fields["RelatedProducts"] != null &&
                localizedItem.Fields["RelatedProducts"].ContainsStandardValue)
            {
                //get the fallback item
                var fallbakItem = master.GetItem(localizedItem.ID, LanguageManager.GetLanguage(fallbackLanguage, master));
                multiselectField = fallbakItem.Fields["RelatedProducts"];
                using (new LanguageSwitcher(LanguageManager.GetLanguage(fallbackLanguage, master)))
                {
                    pords = multiselectField.GetItems();
                }
            }
            return prodList.Count != pords.Count() ||
                prodList.Any(prod => !pords.Any(item => item.Name.Equals(prod.ProductName)));
        }

        private string ValidateItemCategoryAndSubCategory(DownloadItem values, Language itemLanguage, out Item categoryItem, out Item subcategoryItem)
        {
            subcategoryItem = null;
            categoryItem = master.GetItem(new ID(new Guid(values.CategoryType)), itemLanguage);
            if (categoryItem == null) return DownloadConstans.NotValidCategory;
            subcategoryItem = master.GetItem(new ID(new Guid(values.SubCategoryType)), itemLanguage);
            if (subcategoryItem == null) return DownloadConstans.NotValidSubCategory;
            return subcategoryItem.Parent.ID != categoryItem.ID ? DownloadConstans.ParnetChildViolation : null;
        }

        private String ValidateItem(DownloadItem values)
        {
            //validate item name
            if (string.IsNullOrEmpty(values.ItemName)) return DownloadConstans.Category;
            //validate category and subcategory
            if (string.IsNullOrEmpty(values.CategoryType)) return DownloadConstans.Category;
            return string.IsNullOrEmpty(values.SubCategoryType) ? DownloadConstans.SubCategories : null;
        }

        private void DecodeHtml(ref DownloadItem values)
        {
            values.Title = (values.Title != null)
               ? HttpUtility.HtmlDecode(values.Title.Replace("&apos", "&#39;"))
               : null;
            values.MenuTitle = (values.MenuTitle != null) ? HttpUtility.HtmlDecode(values.MenuTitle.Replace("&apos", "&#39;")) : null;
            values.PageTitle = (values.PageTitle != null) ? HttpUtility.HtmlDecode(values.PageTitle.Replace("&apos", "&#39;")) : null;
            values.Description = (values.Description != null) ? HttpUtility.HtmlDecode(values.Description.Replace("&apos", "&#39;")) : null;
            values.MetaKeywords = (values.MetaKeywords != null)
                ? HttpUtility.HtmlDecode(values.MetaKeywords.Replace("&apos", "&#39;"))
                : null;
        }

        private void CreateOrSaveToMultipleLanguages(DownloadItem values)
        {
            foreach (var cultureCode in values.AvailableLanguages.Select(cultureId => GetCultureCodeById(Convert.ToInt32(cultureId))))
            {
                //if (string.IsNullOrEmpty(cultureCode) || cultureCode.Equals("invalid"))
                //return;//CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);
                var itemLanguage = LanguageManager.GetLanguage(cultureCode, master);
                var newLangItem = master.GetItem(new ID(values.ItemId), itemLanguage);
                if (newLangItem == null) continue;
                using (new SecurityDisabler())
                {
                    if (newLangItem.Versions.Count != 0) continue;
                    newLangItem.Versions.AddVersion();
                    //return;
                    //GetDownloadItemById(new DownloadByIdModel() { Id = newLangItem.ID.ToString(), CultureGroupId = values.CultureGroupId });
                }
            }
        }

        private bool DownloadItemExistWithSameName(string itemName, Item parentItem)
        {
            return (parentItem.Children.Where(x => x.Name == itemName.Trim()).ToList().Count > 0);
        }

        public HttpResponseMessage CreateNewLanguageVersion(DownloadItem values, Item child, Language itemLanguage)
        {
            Item localizedItem = master.GetItem(child.ID, itemLanguage);
            Item SubcategoryItem = localizedItem.Axes.GetAncestors().Where(x => x.TemplateID.ToString().Equals(DownloadConstans.SubCategoryTemplate)).FirstOrDefault();
            if (SubcategoryItem != null)
            {
                //Sitecore.Data.Database[] targetDatabases = { web };
                //Sitecore.Globalization.Language[] languages = master.Languages;
                //bool deep = true;
                //bool compareRevisions = true;
                //Sitecore.Publishing.PublishManager.PublishItem(SubcategoryItem, targetDatabases, languages, deep, compareRevisions);
                return GetDownloadItemById(new DownloadByIdModel() { Id = child.ID.ToString(), CultureGroupId = values.CultureGroupId });
            }
            return CreateDowloadItemesponse((int)HttpStatusCode.BadRequest, "This is not a Download Item");
        }

        public HttpResponseMessage CreateDowloadItemesponse(int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;

            var obj = new APIResponse()
            {
                ResponseContent = serializer.Serialize(string.Empty),
                ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
            };
            serializedResult = serializer.Serialize(obj);

            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        public HttpResponseMessage GetDownloadItemByRegion(string Region, string Id)
        {
            GetDownloadByRegionModel OutputModel = new GetDownloadByRegionModel();
            if (string.IsNullOrEmpty(Region.Trim())) return GetDowloadItemByRegionResponse(OutputModel, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);
            if (!ValidateCultureCode(Region.ToLowerInvariant().Trim())) return GetDowloadItemByRegionResponse(OutputModel, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);
            if (string.IsNullOrEmpty(Id.Trim())) return GetDowloadItemByRegionResponse(OutputModel, (int)HttpStatusCode.BadRequest, DownloadConstans.ProvideItemId);

            Language itemLanguage = LanguageManager.GetLanguage(Region, web);
            if (web.GetItem(Id.Trim(), itemLanguage) == null) return GetDowloadItemByRegionResponse(OutputModel, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidId);
            Item DownloadItem = web.GetItem(Id.Trim(), itemLanguage);
            OutputModel.ItemId = DownloadItem.ID.ToString();
            if (DownloadItem.Versions.Count.Equals(0)) return GetDowloadItemByRegionResponse(OutputModel, (int)HttpStatusCode.BadRequest, DownloadConstans.NoLanguageVersion);

            OutputModel.ItemName = DownloadItem.Name;
            OutputModel.Title = DownloadItem["Title"];
            OutputModel.Description = DownloadItem["Description"];
            OutputModel.DownloadFilePath = DownloadItem["FilePath"];
            OutputModel.ReleaseDate = DateUtil.IsoDateToDateTime(DownloadItem["ReleaseDate"]).ToUniversalTime().Date.ToShortDateString();
            Item CategoryItem = DownloadItem.Axes.GetAncestors().Where(x => x.TemplateID.ToString().Equals(DownloadConstans.CategoryTemplate)).FirstOrDefault();
            Item SubCategoryItem = DownloadItem.Axes.GetAncestors().Where(x => x.TemplateID.ToString().Equals(DownloadConstans.SubCategoryTemplate)).FirstOrDefault();
            OutputModel.CategoryType = CategoryItem["Value"];
            OutputModel.SubCategoryType = SubCategoryItem["Value"];
            OutputModel.MenuTitle = DownloadItem["MenuTitle"];
            OutputModel.PageTitle = DownloadItem["PageTitle"];
            OutputModel.Version = DownloadItem["Version"];
            OutputModel.ShowInBreadcrumb = (DownloadItem["ShowInBreadcrumb"].Equals("1") ? true : false);
            OutputModel.FileType = DownloadItem["FileType"];
            OutputModel.Size = DownloadItem["Size"];
            OutputModel.IsNewVersionAvailable = (DownloadItem["IsNewVersionAvailable"].Equals("1") ? true : false);
            OutputModel.LoginRequired = (DownloadItem["LogOnRequired"].Equals("1") ? true : false);
            OutputModel.RedirectToMyAnritsu = (DownloadItem["RedirectToMyAnritsu"].Equals("1") ? true : false);
            OutputModel.MetaDescription = DownloadItem["MetaDescription"];
            OutputModel.MetaKeywords = DownloadItem["Metatags-Other Keywords"];
            OutputModel.ReplacementDownloadPath = DownloadItem["ReplacementDownloadPath"];
            //OutputModel.ItemId = DownloadItem.ID.ToString();
            MultilistField multiselectField = DownloadItem.Fields["RelatedProducts"];
            OutputModel.RelatedProducts = new List<Product>();
            if (multiselectField == null)
            { //TODO: handle case that field does not exist
            }
            else
            {
                Item[] items = multiselectField.GetItems();
                if (items != null && items.Length > 0)
                {
                    for (int i = 0; i < items.Length; i++)
                    { //process items[i] }
                        Product product = new Product();
                        product.ProductName = items[i].Name;
                        OutputModel.RelatedProducts.Add(product);
                    }
                }
            }
            GeneralLinks optionalGeneralLinks = new GeneralLinks(DownloadItem, DownloadItem.Fields["OptionalLinks"]);
            OutputModel.OptionalLinks = new List<OptionalLink>();
            if (optionalGeneralLinks.LinkItems.Count > 0)
            {
                foreach (GeneralLinkItem item in optionalGeneralLinks.LinkItems)
                {
                    OutputModel.OptionalLinks.Add(new OptionalLink() { TargetUrl = item.Url, Text = item.LinkText });
                }
            }


            return GetDowloadItemByRegionResponse(OutputModel, (int)HttpStatusCode.OK, DownloadConstans.FetchSuccessful);
        }

        public HttpResponseMessage GetDowloadItemByRegionResponse(GetDownloadByRegionModel downloadItems, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (downloadItems.ItemId != null)
            {
                var obj = new APIResponse()
                {
                    ResponseContent = downloadItems,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new APIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage }
                };
                serializedResult = serializer.Serialize(obj);
            }
            //return serializedResult;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        /// <summary>
        /// Method to validate Culture code
        /// </summary>
        /// <param name="cultureCode"></param>
        /// <returns></returns>
        public bool ValidateCultureCode(string cultureCode)
        {
            bool IsValid = false;
            switch (cultureCode)
            {
                case "en-us":
                    IsValid = true;
                    break;
                case "ja-jp":
                    IsValid = true;
                    break;
                case "en-gb":
                    IsValid = true;
                    break;
                case "en-au":
                    IsValid = true;
                    break;
                case "zh-cn":
                    IsValid = true;
                    break;
                case "ko-kr":
                    IsValid = true;
                    break;
                case "zh-tw":
                    IsValid = true;
                    break;
                case "ru-ru":
                    IsValid = true;
                    break;
                case "en":
                    IsValid = true;
                    break;
            }
            return IsValid;
        }

        protected string GetDownloadDetailePageUrl(Item item)
        {
            return Request.RequestUri.Scheme + "://" + GetHost() + "/" + item.Language.Name + DownloadConstans.DetailsPagePath + item.Name.Trim();
        }

        private string GetS3ObjectUrl(string s3Path)
        {
            //var bucketPath = "http://" + ConfigurationManager.AppSettings["DownloadDistributionList"] + "/";
            return String.Format("http://{0}/{1}", ConfigurationManager.AppSettings["DownloadDistributionList"], s3Path);
        }

        public string GetHost()
        {
            if (Request.RequestUri.Host.StartsWith("auth")) return DownloadConstans.ProdHost;
            return Request.RequestUri.Host;
        }

        public Database ValidateTargetDb(string targetDb)
        {
            List<string> validDatabases = new List<string>() { master.Name, web.Name };
            Database db;
            if (String.IsNullOrEmpty(targetDb) && (validDatabases.Where(x => x.Contains(targetDb)).Count() < 0)) return null;
            db = Factory.GetDatabase(targetDb);
            if (db == null) return null;
            return db;
        }

        public DataTable GetResponseStatusObject(int StatusCode, string StatusMessage)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ResponseStatusCode", typeof(string));
            dt.Columns.Add("ResponseStatusMessage", typeof(string));
            dt.Rows.Add(StatusCode, StatusMessage);
            return dt;
        }

        /// <summary>
        /// Method to fetch Culture based on ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public string GetCultureCodeById(int ID)
        {
            string cultureCode = string.Empty;
            switch (ID)
            {
                case 1:
                    cultureCode = "en-US";
                    break;

                case 2:
                    cultureCode = "ja-JP";
                    break;
                case 3:
                    cultureCode = "en-GB";
                    break;

                case 4:
                    cultureCode = "en-AU";
                    break;

                case 6:
                    cultureCode = "zh-CN";
                    break;

                case 7:
                    cultureCode = "ko-KR";
                    break;

                case 10:
                    cultureCode = "zh-TW";
                    break;
                case 14:
                    cultureCode = "ru-RU";
                    break;
                case 15:
                    cultureCode = "en-IN";
                    break;
                case 16:
                    cultureCode = "en";
                    break;
                default:
                    cultureCode = "invalid";
                    break;
            }
            return cultureCode;
        }

        private List<string> GetLanagItem(Item item)
        {
            return (from languageVersion in item.Languages
                    let languageSpecificItem = item.Database.GetItem(item.ID, languageVersion)
                    where languageSpecificItem != null && languageSpecificItem.Versions.Count > 0 && languageVersion.Name != String.Empty
                    select languageVersion.Name).ToList();
        }

        private PublishOptions GetPublishOptions(string target, Language itemLang)
        {
            return new PublishOptions(master, Database.GetDatabase(target),
                       PublishMode.Smart, itemLang, DateTime.Now);
        }

        #endregion
    }
    public enum Environment { master, web, production }
}
