﻿using Anritsu.WebApp.GlobalWeb.Models;
using Lucene.Net.Search;
using Newtonsoft.Json;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace Anritsu.WebApp.GlobalWeb.Controllers
{
    public class EmailTemplateController : ApiController
    {
        #region Global Variables
        Database master = Sitecore.Context.Database;
        int DefaultCultureId = 1;
        string indexName = "anritsu_emailtemplates";       
        #endregion

        #region Methods
        /// <summary>
        /// Gets Email Template by key and cultureCode
        /// </summary>
        /// <param name="templateKey"></param>
        /// <param name="cultureGroupId"></param>
        /// <returns></returns>
        public DataTable GetEmailTemplateByKey(string templateKey, int cultureGroupId)
        {
            Item SearchItem;
            EmailTemplateResponse modelTemplate = new EmailTemplateResponse();
            List<Item> lstItems = new List<Item>();
            string responseJson = string.Empty;
            DataTable table = new DataTable();
            using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
            {
                var query = context.GetQueryable<SearchResultItem>();
                int cultureGrpID = (cultureGroupId != 0) ? cultureGroupId : DefaultCultureId;
                foreach (SearchResultItem item in query)
                {
                    lstItems.Add(master.GetItem(item.ItemId));
                }
                if (lstItems.Any())
                {
                    SearchItem = lstItems.Where(x => x.Name == templateKey.Trim()).FirstOrDefault();
                    if (SearchItem != null)
                    {
                        //Language itemLanguage = Language.Parse(cultureGroupId);
                        string cultureCode = GetCultureCodeById(cultureGrpID);
                        Language itemLanguage = LanguageManager.GetLanguage(cultureCode, Sitecore.Context.Database);

                        if (itemLanguage != null)
                        {
                            SearchItem = master.Items.GetItem(SearchItem.ID, itemLanguage);
                            if (!String.IsNullOrEmpty(SearchItem["BodyTemplate"].ToString()))
                            {
                            modelTemplate.FromEmailAddress = SearchItem["FromEmailAddress"].ToString();
                            modelTemplate.MailBodyFormat = SearchItem["MailBodyFormat"].ToString();
                            modelTemplate.SubjectTemplate = SearchItem["SubjectTemplate"].ToString();
                            modelTemplate.BodyTemplate = SearchItem["BodyTemplate"].ToString();
                            modelTemplate.ResponseStatusCode = (int)HttpStatusCode.OK;
                            modelTemplate.ResponseStatusMessage = "Success";                           
                            }
                            else
                            {
                                itemLanguage = LanguageManager.GetLanguage(GetCultureCodeById(DefaultCultureId), Sitecore.Context.Database);
                                SearchItem = master.Items.GetItem(SearchItem.ID, itemLanguage);
                                modelTemplate.FromEmailAddress = SearchItem["FromEmailAddress"].ToString();
                                modelTemplate.MailBodyFormat = SearchItem["MailBodyFormat"].ToString();
                                modelTemplate.SubjectTemplate = SearchItem["SubjectTemplate"].ToString();
                                modelTemplate.BodyTemplate = SearchItem["BodyTemplate"].ToString();
                                modelTemplate.ResponseStatusCode = (int)HttpStatusCode.OK;
                                modelTemplate.ResponseStatusMessage = "Success";                                
                            } 
                            table.Columns.Add("FromEmailAddress", typeof(string));
                            table.Columns.Add("MailBodyFormat", typeof(string));
                            table.Columns.Add("SubjectTemplate", typeof(string));
                            table.Columns.Add("BodyTemplate", typeof(string));
                            table.Columns.Add("ResponseStatusCode", typeof(string));
                            table.Columns.Add("ResponseStatusMessage", typeof(string));
                            table.Rows.Add(modelTemplate.FromEmailAddress, modelTemplate.MailBodyFormat, modelTemplate.SubjectTemplate, modelTemplate.BodyTemplate,modelTemplate.ResponseStatusCode,modelTemplate.ResponseStatusMessage);  
                        }
                        else
                        {
                            return GetResponseStatusObject((int)HttpStatusCode.NotFound, "Not a valid culturecode");
                        }
                    }
                    else
                    {
                        return GetResponseStatusObject((int)HttpStatusCode.NotFound, "No Items Found");
                    }
                }
                else
                {
                    return GetResponseStatusObject((int)HttpStatusCode.NoContent, "No Results Fetched");
                }
            }
            return table;
        }

        /// <summary>
        /// Method to get response codes and messages.
        /// </summary>
        /// <param name="StatusCode"></param>
        /// <param name="StatusMessage"></param>
        /// <returns></returns>
        public DataTable GetResponseStatusObject(int StatusCode, string StatusMessage)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ResponseStatusCode", typeof(string));
            dt.Columns.Add("ResponseStatusMessage", typeof(string));
            dt.Rows.Add(StatusCode, StatusMessage);
            return dt;
        }

        /// <summary>
        /// Method to fetch Culture based on ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public string GetCultureCodeById(int ID)
        {
            string cultureCode = string.Empty;
            switch (ID)
            {
                case 0:
                    cultureCode = "en-US";
                    break;
                case 1:
                    cultureCode = "en-US";
                    break;

                case 2:
                    cultureCode = "ja-JP";
                    break;
                case 3:
                    cultureCode = "en-GB";
                    break;

                case 4:
                    cultureCode = "en-AU";
                    break;

                case 6:
                    cultureCode = "zh-CN";
                    break;

                case 7:
                    cultureCode = "ko-KR";
                    break;

                case 10:
                    cultureCode = "zh-TW";
                    break;
                case 14:
                    cultureCode = "ru-RU";
                    break;
                case 15:
                    cultureCode = "en-IN";
                    break;
                default:
                    cultureCode = "en";
                    break;
            }
            return cultureCode;
        }

    }

        #endregion

}
