﻿using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sitecore.Data.Managers;
using System.Web.Mvc;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using Sitecore.Globalization;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class LinksAdminController : ApiController
    {
        #region GlobalVariables
        public static string productParentID = "{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}";
        Database master = Sitecore.Context.Database;
        string indexName = "anritsu_linksadminproducts";
        #endregion

        #region Methods
        // GET api/linksadmin/
        public List<LinksAdminProduct> GetProducts(string cultureCode)
        {
            //cultureCode = (String.IsNullOrEmpty(cultureCode.Trim())) ? "en-US" : cultureCode.Trim();
            cultureCode = cultureCode.Trim();
            if (!ValidateCultureCode(cultureCode.ToLower()))
            {
                return null;
            }
            List<Item> lstItems = new List<Item>();
            List<LinksAdminProduct> productItems = new List<LinksAdminProduct>();
            Language itemLanguage = LanguageManager.GetLanguage(cultureCode, Sitecore.Context.Database);
            using (var context = ContentSearchManager.GetIndex(indexName).CreateSearchContext())
            {

                var query = context.GetQueryable<SearchResultItem>().Where(i => i.Language == itemLanguage.Name);

                foreach (SearchResultItem item in query)
                {
                    if (master.Items.GetItem(item.ItemId, itemLanguage) != null)
                    {
                         lstItems.Add(master.Items.GetItem(item.ItemId, itemLanguage));
                    }
                }
            }

            lstItems = lstItems.Distinct(new ItemEqualityComparer()).ToList();
            foreach (Item item in lstItems)
            {
                LinksAdminProduct product = new LinksAdminProduct();
                product.Name = item.Name.Trim();
                using (new LanguageSwitcher(itemLanguage))
                {
                    product.Url = Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(item.ID)).ToString();
                }
                productItems.Add(product);
            }

            return productItems.OrderBy(o=>o.Name).ToList();
        }

        public bool ValidateCultureCode(string cultureCode)
        {
            bool IsValid = false;
            switch (cultureCode)
            {
                case "en-us":
                    IsValid = true;
                    break;

                case "ja-jp":
                    IsValid = true;
                    break;
                case "en-gb":
                    IsValid = true;
                    break;

                case "en-au":
                    IsValid = true;
                    break;

                case "zh-cn":
                    IsValid = true;
                    break;

                case "ko-kr":
                    IsValid = true;
                    break;

                case "zh-tw":
                    IsValid = true;
                    break;
                case "ru-ru":
                    IsValid = true;
                    break;
                case "en-in":
                    IsValid = true;
                    break;
            }
            return IsValid;
        }
        #endregion
    } 
}
