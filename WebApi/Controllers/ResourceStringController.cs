﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sitecore;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using WebApi.Models;
using Sitecore.Data.Items;
using System.Data;
using System.Globalization;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Configuration;
using System.Text;

namespace Anritsu.WebApp.GlobalWeb.Controllers
{
    public class ResourceStringController : ApiController
    {
        #region Global Variables
        string cultureEn = "en";
        #endregion

        #region Methods
        /// <summary>
        /// Gets the Resouce key value of the current context language
        /// </summary>
        /// <param name="dictionaryType"></param>
        /// <param name="resourceKey"></param>
        /// <returns></returns>
        public HttpResponseMessage GetResourceValue(string dictionaryType, string resourceKey)
        {
            Language sourceLanguage = Sitecore.Context.Language;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            using (new LanguageSwitcher(sourceLanguage))
            {
                response.Content = new StringContent(Translate.TextByDomain(dictionaryType, resourceKey), Encoding.UTF8, "text/html");
            }
            return response;
        }
              
        /// <summary>
        /// Gets the Resource key value based on below parameters using Sitecore Dictionaries
        /// </summary>
        /// <param name="cultureCode"></param>
        /// <param name="dictionaryType"></param>
        /// <param name="resourceKey"></param>
        /// <returns></returns>
        public HttpResponseMessage GetResourceValueByDictionary(string cultureCode, string dictionaryType, string resourceKey)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                try
                {
                    cultureCode = CultureInfo.CreateSpecificCulture(cultureCode).Name.Trim(); 
                }
                catch
                {
                    cultureCode = cultureEn;
                }
               
                dictionaryType = dictionaryType.Trim(); 
                resourceKey = resourceKey.Trim();
                var sourceLanguage = LanguageManager.GetLanguage(cultureCode, Context.Database);
                var options = new TranslateOptions(); 
                var keyValue = Translate.TextByLanguage(dictionaryType, options, resourceKey, sourceLanguage, string.Empty);
                response.Content = new StringContent(keyValue, Encoding.UTF8, "text/html");
                if (!string.IsNullOrEmpty(keyValue)) return response;
                keyValue = Translate.TextByLanguage("GlobalDictionary", options, resourceKey, sourceLanguage, string.Empty);
                response.Content = new StringContent(keyValue, Encoding.UTF8, "text/html");
                if (!string.IsNullOrEmpty(keyValue)) return response; 
                sourceLanguage = LanguageManager.GetLanguage(cultureEn, Context.Database);
                keyValue = Translate.TextByLanguage(dictionaryType, options, resourceKey, sourceLanguage, string.Empty);
                response.Content = new StringContent(keyValue, Encoding.UTF8, "text/html");
                if (!string.IsNullOrEmpty(keyValue)) return response;
                keyValue = Translate.TextByLanguage("GlobalDictionary", options, resourceKey, sourceLanguage, string.Empty);
                response.Content = new StringContent(keyValue, Encoding.UTF8, "text/html");
                return response;
            }
            catch (Exception ex)
            {
                response.Content = new StringContent(resourceKey); //new StringContent(ex.Message + ex.InnerException.Message, Encoding.UTF8, "text/html");
                return response;
            }
        }

        #endregion
    }
}