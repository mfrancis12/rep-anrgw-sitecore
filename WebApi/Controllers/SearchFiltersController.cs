﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Sitecore;
using Sitecore.Data.Managers;
using WebApi.Constants;

namespace WebApi.Controllers
{
    public class SearchFiltersController : ApiController
    {
        public List<string> GetFilters(string cultureCode)
        {
            var itemLanguage = LanguageManager.GetLanguage((string.IsNullOrEmpty(cultureCode))
                ? "en-US"
                : cultureCode.Trim());
            var searchItem = Context.Database.GetItem(ItemIds.SearchFilterFolder, itemLanguage);
            var searchFilters = searchItem.GetChildren().Select(x => x.Fields["Value"].ToString()).ToList();
            return searchFilters;
        }
    }
}
