﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using WebApi.Models;
using System.Web.Script.Serialization;
using System.Text;
using Anritsu.WebApp.SitecoreUtilities.Extensions;
using WebApi.Constants;
using Sitecore.Globalization;
using Sitecore.Data.Managers;
namespace WebApi.Controllers
{
    public class TrainingController : ApiController
    {
        #region Global Variables
        Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");
        #endregion

        // GET api/training
        public HttpResponseMessage GetTrainingCourses(string CultureCode)
        {
            List<TrainingCourse> lstCourses = new List<TrainingCourse>();
            if (string.IsNullOrEmpty(CultureCode)) return GetTrainingCoursesResponse(lstCourses, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);
            if (!ValidateCultureCode(CultureCode.ToLowerInvariant().Trim())) return GetTrainingCoursesResponse(lstCourses, (int)HttpStatusCode.BadRequest, DownloadConstans.NotValidCulture);

            var query = string.Format("fast:{0}//*[@@templateid='{1}']", StringExtensions.EscapePath("/sitecore/content/GlobalWeb/home/test-measurement/support/training-and-education"), "{F2C578CF-CA8E-44B0-970A-45F446775F81}");
            using (new LanguageSwitcher(LanguageManager.GetLanguage(CultureCode)))
            {
                List<Item> lstItems = masterDb.SelectItems(query).ToList<Item>();
                lstItems = lstItems.Where(x => x.Language.Name.ToLowerInvariant().Equals(CultureCode.ToLowerInvariant()) && x.Versions.Count>0).ToList();
                foreach (var item in lstItems)
                {                   
                        lstCourses.Add(new TrainingCourse() { CourseName = item.Fields["CourseName"].Value, ItemID = item.ID.ToString() });                      
                }
            }
            if (lstCourses.Count <= 0) return GetTrainingCoursesResponse(lstCourses, (int)HttpStatusCode.BadRequest, DownloadConstans.NoResultsFound);
            lstCourses = lstCourses.OrderBy(x => x.CourseName).ToList();
            return GetTrainingCoursesResponse(lstCourses, (int)HttpStatusCode.OK, DownloadConstans.Succesfull);
        }
        public HttpResponseMessage GetTrainingCoursesResponse(List<TrainingCourse> categories, int StatusCode, string StatusMessage)
        {
            var serializer = new JavaScriptSerializer(); var serializedResult = string.Empty;
            if (categories.Count > 0)
            {
                var obj = new GetDownloadsByKeyAPIResponse()
                {
                    ResponseContent = serializer.Serialize(categories),
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage },
                    TotalRowCount=categories.Count
                };
                serializedResult = serializer.Serialize(obj);
            }
            else
            {
                var obj = new GetDownloadsByKeyAPIResponse()
                {
                    ResponseContent = string.Empty,
                    ResponseStatus = new ResponseStatus() { StatusCode = StatusCode, StatusMessage = StatusMessage },
                    TotalRowCount = 0
                };
                serializedResult = serializer.Serialize(obj);
            }
            //return serializedResult;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(serializedResult, Encoding.UTF8, "application/json");
            return response;
        }

        /// <summary>
        /// Method to validate Culture code
        /// </summary>
        /// <param name="cultureCode"></param>
        /// <returns></returns>
        public bool ValidateCultureCode(string cultureCode)
        {
            bool IsValid = false;
            switch (cultureCode)
            {
                case "en": IsValid = true;
                    break;
                case "en-us": IsValid = true;
                    break;
                case "ja-jp": IsValid = true;
                    break;
                case "en-gb": IsValid = true;
                    break;
                case "en-au": IsValid = true;
                    break;
                case "zh-cn": IsValid = true;
                    break;
                case "ko-kr": IsValid = true;
                    break;
                case "zh-tw": IsValid = true;
                    break;
                case "ru-ru": IsValid = true;
                    break;
            }
            return IsValid;
        }
    }

    public class TrainingCourse
    {
        public string CourseName { get; set; }
        public string ItemID { get; set; }
    }
}
