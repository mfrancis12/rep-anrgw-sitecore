﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Http;
using Anritsu.WebApp.GlobalWeb.Models;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Publishing;
using Sitecore.Publishing.Pipelines.Publish;
using Sitecore.SecurityModel;
using WebApi.Constants;
using WebApi.Models;
using Convert = System.Convert;

namespace WebApi.Controllers
{
    public class UniversalFormController : ApiController
    {
        #region Global Variables

        readonly Database _masterDb = Factory.GetDatabase("master");
        readonly Database _webDb = Factory.GetDatabase("web");
        readonly List<Database> _databases = Factory.GetDatabases().Where(x => x.Name == "production").ToList();
        List<Item> _lstSearchResults = new List<Item>();
        #endregion

        #region Methods
        /// <summary>
        /// This method used to get item name for Forms SFTP generation
        /// </summary>
        /// <param name="itemid"></param>
        /// <returns></returns>
        [HttpGet]
        public string GetItemPath(string itemid)
        {
            string itemname = string.Empty;
            try
            {
                
                Sitecore.Data.ID id = new ID(itemid);
                var itemLanguage = LanguageManager.GetLanguage("en");
                var searchItem = Context.Database.GetItem(id, itemLanguage);
                itemname = searchItem.Paths.Path.ToString();
            }
            catch
            {
                itemname = string.Empty;
            }
            return itemname;

        }

        /// <summary>
        /// Ajax Post Method for Creating items
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        [HttpPost]
        public BaseClass CreateEloquaItem(EloquaForm values)
        {
            using (new SecurityDisabler())
            {
                if (!ValidateCultureCode(values.CultureCode.Trim().ToLower()))
                {
                    return new BaseClass
                    {
                        ResponseStatusMessage = ItemIds.NotValidCulture,
                        ResponseStatusCode = (int)HttpStatusCode.BadRequest
                    };
                }

                var itemLanguage = LanguageManager.GetLanguage((string.IsNullOrEmpty(values.CultureCode))
                    ? "en-US" : values.CultureCode.Trim(), _masterDb);

                var parentItem = _masterDb.GetItem(values.ItemPath, itemLanguage);
                if (parentItem == null)
                {
                    return new BaseClass
                    {
                        ResponseStatusMessage = ItemIds.NotValidTargetpath + " " + values.ItemPath,
                        ResponseStatusCode = (int)HttpStatusCode.NotFound
                    };
                }

                var template = _masterDb.GetTemplate(new ID(values.TemplateID.Trim()));
                if (!values.IsEdit)
                {
                    //Now we need to get the template from which the item is created
                    template = _masterDb.GetTemplate(new ID(values.TemplateID.Trim()));
                    if (template == null)
                    {
                        return new BaseClass
                        {
                            ResponseStatusMessage = ItemIds.NoTemplateFound,
                            ResponseStatusCode = (int) HttpStatusCode.NotFound
                        };
                    }
                }


                var isDuplicate = !values.IsEdit &&
                    (parentItem.Children.Any(x1 => x1.Name == values.ItemName.Trim() && x1.Language.Name == itemLanguage.Name));
                     
                var isPublish = Convert.ToBoolean(values.IsPublish);
                if (isDuplicate && !isPublish)
                {
                    return new BaseClass
                    {
                        ResponseStatusMessage = ItemIds.ItemAlreadyExists,
                        ResponseStatusCode = (int)HttpStatusCode.NotAcceptable
                    };
                }


                var child = !values.IsEdit && !isPublish 
                    ? parentItem.Add(ItemUtil.ProposeValidItemName(values.ItemName.Trim()), template)
                    : _masterDb.GetItem(values.ItemPath.Trim() + "/" + values.ItemName.Trim(), itemLanguage);


                using (new LanguageSwitcher(values.CultureCode.ToLower().Trim()))
                {
                    var localizedItem = _masterDb.GetItem(child.ID, itemLanguage);

                    var workflowItem = _masterDb.GetItem("{D7D3D8AB-69F5-4ECC-96DF-597F93D18A1A}");
                    var workflowState = _masterDb.GetItem("{EAD4039A-12C5-411C-B474-14C53D366DEF}");

                    localizedItem.Editing.BeginEdit();
                    localizedItem.Fields["FormId"].Value = values.FormId.Trim();
                    localizedItem.Fields["FormType"].Value = values.FormType.Trim();
                    localizedItem.Fields["PageTitle"].Value = values.PageTitle.Trim();
                    localizedItem.Fields["CreatedBy"].Value = values.CreatedBy.Trim();
                    localizedItem.Fields["EloquaInstance"].Value = values.EloquaInstance.Trim();
                    if (workflowItem != null && workflowState != null)
                    {
                        localizedItem.Fields[FieldIDs.Workflow].Value = workflowItem.ID.ToString();
                        localizedItem.Fields[FieldIDs.WorkflowState].Value = workflowState.ID.ToString();
                    }
                    localizedItem.Editing.EndEdit();

                    if (!isPublish)
                        return new BaseClass
                        {
                            ResponseStatusMessage =
                                values.IsEdit ? ItemIds.UpdatedSuccessfully : ItemIds.CreatedSuccessfully,
                            ResponseStatusCode = (int) HttpStatusCode.OK
                        };
                    var isProdInstance = Convert.ToBoolean(values.IsProdInstance);
                    PublishItem(localizedItem, itemLanguage, isProdInstance);
                    return new BaseClass
                    {
                        ResponseStatusMessage = ItemIds.PublishedSuccessfully,
                        ResponseStatusCode = (int) HttpStatusCode.OK
                    };
                }
            }
        }



        /// <summary>
        /// Method to publish items
        /// </summary>
        /// <param name="item"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public string PublishItem(Item item, Language language, bool isProdInstance)
        {
            // Create a publisher with the publishoptions
            var publishOptions = new PublishOptions(item.Database, Database.GetDatabase("web"), PublishMode.SingleItem,
                item.Language, DateTime.Now.ToUniversalTime());
            var publisher = new Publisher(publishOptions);
            PublishItem(item, publisher);

            if (!isProdInstance) return string.Empty;
            if (_databases.Count <= 0) return string.Empty;
            publishOptions = new PublishOptions(_masterDb, Database.GetDatabase("production"), PublishMode.SingleItem,
                language, DateTime.Now.ToUniversalTime());
            publisher = new Publisher(publishOptions);
            PublishItem(item, publisher);
            return string.Empty;
        }

        /// <summary>
        /// Method to publish items
        /// </summary>
        /// <param name="item"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public PublishResult PublishItem(Item item, Publisher publisher)
        {
            // Choose where to publish from
            publisher.Options.RootItem = item;

            // Publish children as well?
            publisher.Options.Deep = false;

            // Publish related items as well?
            publisher.Options.PublishRelatedItems = false;

            // Do the publish!
            return publisher.PublishWithResult();
        }

        /// <summary>
        /// Method to validate Culture code
        /// </summary>
        /// <param name="cultureCode"></param>
        /// <returns></returns>
        public bool ValidateCultureCode(string cultureCode)
        {
            var isValid = false;
            switch (cultureCode)
            {
                case "en-us": isValid = true;
                    break;
                case "ja-jp": isValid = true;
                    break;
                case "en-gb": isValid = true;
                    break;
                case "en-au": isValid = true;
                    break;
                case "zh-cn": isValid = true;
                    break;
                case "ko-kr": isValid = true;
                    break;
                case "zh-tw": isValid = true;
                    break;
                case "ru-ru": isValid = true;
                    break;
            }
            return isValid;
        }

        /// <summary>
        /// Ajax Post Method for Deleting items
        /// </summary>
        /// <param name="values">template ID, parent path, item id, verification token</param>
        /// <returns></returns>
        [HttpPost]
        public BaseClass DeleteEloquaItem(EloquaForm values)
        {
            using (new SecurityDisabler())
            {
                if (!ValidateCultureCode(values.CultureCode.Trim().ToLower()))
                {
                    return new BaseClass
                    {
                        ResponseStatusMessage = ItemIds.NotValidCulture,
                        ResponseStatusCode = (int)HttpStatusCode.BadRequest
                    };
                }

                var itemLanguage = LanguageManager.GetLanguage((String.IsNullOrEmpty(values.CultureCode))
                    ? "" : values.CultureCode.Trim(), _masterDb);

                if (itemLanguage == null)
                    return new BaseClass
                    {
                        ResponseStatusMessage = ItemIds.ItemNotFound,
                        ResponseStatusCode = (int) HttpStatusCode.NotFound
                    };
                var deletedtobeItem = _masterDb.GetItem(values.ItemID, itemLanguage);
                if (deletedtobeItem == null)
                {
                    return new BaseClass
                    {
                        ResponseStatusMessage = ItemIds.ItemNotFound,
                        ResponseStatusCode = (int) HttpStatusCode.NotFound
                    };
                }
                DeleteWebItem(Environment.master.ToString(), values);

                if (!values.IsProdInstance) return DeleteWebItem(Environment.web.ToString(), values);
                if (_databases.Count <= 0)
                    return new BaseClass
                    {
                        ResponseStatusMessage = ItemIds.ItemDeletedSuccessfully,
                        ResponseStatusCode = (int) HttpStatusCode.OK
                    };
                var prodDb = Factory.GetDatabase("production");
                deletedtobeItem = prodDb.GetItem(values.ItemID, itemLanguage);
                if (deletedtobeItem != null)
                {
                    DeleteWebItem(Environment.production.ToString(), values);
                }
                return DeleteWebItem(Environment.web.ToString(), values);
            }
        }

        public BaseClass DeleteWebItem(string environment, EloquaForm values)
        {
            Item deletedtobeItem = null;
            var id = new ID(values.ItemID);
            TemplateItem template = null;
            if (environment == Environment.master.ToString())
            {
                deletedtobeItem = _masterDb.GetItem(values.ItemID);
                template = (_masterDb.GetTemplate((_masterDb.GetItem(id)).Template.FullName));
            }
            else if (environment == Environment.web.ToString())
            {
                deletedtobeItem = _webDb.GetItem(values.ItemID);
                template = (_webDb.GetTemplate((_webDb.GetItem(id)).Template.FullName));
            }
            else if (environment == Environment.production.ToString())
            {
                if (_databases.Count>0)
                {
                    var prodDb = Factory.GetDatabase("production");
                    deletedtobeItem = prodDb.GetItem(values.ItemID);
                    template = (prodDb.GetTemplate((prodDb.GetItem(id)).Template.FullName));
                }               
            }

            //Now we need to get the template from which the item is created

            //TemplateItem templateItem = (masterDb.GetTemplate((masterDb.GetItem(id)).Template.FullName));
            if (values.VerificationToken != Token.GlobalVerficationToken || template == null || template.ID.ToString() != values.TemplateID)
                return new BaseClass
                {
                    ResponseStatusMessage = ItemIds.ItemNotFound + ", " + environment + ", " + template.ID.ToString() + ", " + values.TemplateID + ", " 
                                                + values.VerificationToken + ", " + Token.GlobalVerficationToken,
                    ResponseStatusCode = (int) HttpStatusCode.BadRequest
                };
            //Item deletedtobeItem = webDb.GetItem(values.ItemID);
            if (deletedtobeItem == null)
                return new BaseClass
                {
                    ResponseStatusMessage = ItemIds.ItemNotFound,
                    ResponseStatusCode = (int) HttpStatusCode.NotFound
                };
            deletedtobeItem.Delete();
            //deletedtobeItem.Versions.RemoveAll(false);
            return new BaseClass
            {
                ResponseStatusMessage = ItemIds.ItemDeletedSuccessfully,
                ResponseStatusCode = (int)HttpStatusCode.OK
            };
        }

        /// <summary>
        /// Eloqua Search Method
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        [HttpPost]
        public DataTable EloquaSearch(EloquaForm values)
        {
            var model = new EloquaForm();
            const int rowsPerPage = 15;
            var filteredItems = new List<EloquaForm>();
            var resultTable = new DataTable();

            if (string.IsNullOrEmpty(values.FormType) || string.IsNullOrEmpty(values.TemplateID) || string.IsNullOrEmpty(values.CultureCode) 
                || String.IsNullOrEmpty(values.EloquaInstance))
            {
                return GetResponseStatusObject((int)HttpStatusCode.BadRequest, "No Proper inputs");
            }
            model.FormType = values.FormType = values.FormType.Trim();
            model.TemplateID = values.TemplateID = values.TemplateID.Trim();
            model.CultureCode = values.CultureCode = values.CultureCode.Trim().ToLower();
            model.EloquaInstance = values.EloquaInstance = values.EloquaInstance.Trim();
            //model.PageIndex = values.PageIndex = ((values.PageIndex != 0)? values.PageIndex : 1);
            if (!string.IsNullOrEmpty(values.ItemName))
            {
                model.ItemName = values.ItemName = values.ItemName.Trim();
            }
            if (!string.IsNullOrEmpty(values.FormId))
            {
                model.FormId = values.FormId = values.FormId.Trim();
            }
            if (!String.IsNullOrEmpty(values.CreatedBy))
            {
                model.CreatedBy = values.CreatedBy = values.CreatedBy.Trim();
            }
            if (!ValidateCultureCode(values.CultureCode))
            {
                return GetResponseStatusObject((int)HttpStatusCode.BadRequest, "No Proper inputs");
            }
            var filterlstItems = new List<Item>();
            var language = Language.Parse(values.CultureCode.Trim().ToLower());
            using (new LanguageSwitcher(language))
            {
                model.CultureCode = values.CultureCode.Trim().ToLower();
                var items = Context.Database.SelectItems("fast:/sitecore/content//*[@@templateid = '" + values.TemplateID + "']");
                var lstItems = items.ToList();
                if (!string.IsNullOrEmpty(values.ItemName))
                {
                    lstItems = lstItems.Where(x => x.Name.ToLowerInvariant().Contains(values.ItemName.ToLowerInvariant())).ToList();                   
                }
                if (values.StartDate != null && values.EndDate != null)
                {
                    filterlstItems.AddRange(lstItems.Where(item => (item.Statistics.Created).Date >= values.StartDate.Date 
                        && (item.Statistics.Created).Date <= values.EndDate.Date));
                }               
            }
            if (filterlstItems.Any())
            {
                foreach (var item in filterlstItems.Where(item => !string.IsNullOrEmpty(item.Fields["EloquaInstance"].Value)))
                {
                    filterlstItems = filterlstItems.Where(x => x.Fields["EloquaInstance"].Value == values.EloquaInstance).ToList();
                }  
            }
            else
            {
                return GetResponseStatusObject((int)HttpStatusCode.BadRequest, "No matches found");
            }
            if (filterlstItems.Any())
            {                             
                var formId = model.FormId;
                var createdBy = model.CreatedBy;
                var formType = model.FormType;
                
                if (!string.IsNullOrEmpty(formId) && !string.IsNullOrEmpty(createdBy) && !string.IsNullOrEmpty(formType))
                {
                    filterlstItems = filterlstItems.Where(x => x.Fields["FormId"].Value == formId).Where(x => x.Fields["CreatedBy"].Value.Contains(createdBy)).Where(x => x.Fields["FormType"].Value == formType).ToList();
                }
                else if (string.IsNullOrEmpty(formId) && !string.IsNullOrEmpty(createdBy) && !string.IsNullOrEmpty(formType))
                {
                    filterlstItems = filterlstItems.Where(x => x.Fields["CreatedBy"].Value.Contains(createdBy)).Where(x => x.Fields["FormType"].Value == formType).ToList();
                }
                else if (!string.IsNullOrEmpty(formId) && string.IsNullOrEmpty(createdBy) && !string.IsNullOrEmpty(formType))
                {
                    filterlstItems = filterlstItems.Where(x => x.Fields["FormId"].Value == formId).Where(x => x.Fields["FormType"].Value == formType).ToList();
                }
                else if (!string.IsNullOrEmpty(formId) && !string.IsNullOrEmpty(createdBy) && string.IsNullOrEmpty(formType))
                {
                    filterlstItems = filterlstItems.Where(x => x.Fields["FormId"].Value == formId).Where(x => x.Fields["CreatedBy"].Value.Contains(createdBy)).ToList();
                }
                else if (!string.IsNullOrEmpty(formId) && string.IsNullOrEmpty(createdBy) && string.IsNullOrEmpty(formType))
                {
                    filterlstItems = filterlstItems.Where(x => x.Fields["FormId"].Value == formId).ToList();
                }
                else if (string.IsNullOrEmpty(formId) && !string.IsNullOrEmpty(createdBy) && string.IsNullOrEmpty(formType))
                {
                    filterlstItems = filterlstItems.Where(x => x.Fields["CreatedBy"].Value.Contains(createdBy)).ToList();
                }
                else if (string.IsNullOrEmpty(formId) && string.IsNullOrEmpty(createdBy) && !string.IsNullOrEmpty(formType))
                {
                    filterlstItems = filterlstItems.Where(x => x.Fields["FormType"].Value == formType).ToList();
                }

                if (filterlstItems.Count > 0)
                {
                    if (values.PageIndex > 0)
                    {
                        if (filterlstItems.Count > (rowsPerPage))
                        {
                            filterlstItems = filterlstItems.Skip((values.PageIndex - 1) * rowsPerPage).Take(rowsPerPage).ToList();
                        }
                    }
                 
                    //lstSearchResults = filterlstItems.OrderByDescending(x => x.Fields["CreatedBy"]).ToList();
                    resultTable.Columns.Add("FormId", typeof(string));
                    resultTable.Columns.Add("CreatedBy", typeof(string));
                    resultTable.Columns.Add("FormType", typeof(string));
                    resultTable.Columns.Add("ItemName", typeof(string));
                    resultTable.Columns.Add("ItemID", typeof(string));
                    resultTable.Columns.Add("ItemPath", typeof(string));
                    resultTable.Columns.Add("PageTitle", typeof(string));
                    resultTable.Columns.Add("TemplateID", typeof(string));
                    resultTable.Columns.Add("CultureCode", typeof(string));
                    resultTable.Columns.Add("EloquaInstance", typeof(string));
                    resultTable.Columns.Add("ResponseStatusCode", typeof(string));
                    resultTable.Columns.Add("ResponseStatusMessage", typeof(string));
                    foreach (Item item in filterlstItems)
                    {
                        EloquaForm listItem = new EloquaForm();
                        listItem.FormId = item["FormId"];
                        listItem.CreatedBy = item["CreatedBy"];
                        listItem.FormType = item["FormType"];
                        listItem.ItemName = item.Name.ToString();
                        listItem.ItemID = item.ID.ToString();
                        listItem.ItemPath = item.Paths.FullPath.ToString();
                        listItem.PageTitle = item["PageTitle"];
                        listItem.TemplateID = model.TemplateID;
                        listItem.CultureCode = model.CultureCode;
                        listItem.EloquaInstance = item["EloquaInstance"].Trim();
                        listItem.ResponseStatusCode = (int)HttpStatusCode.OK;
                        listItem.ResponseStatusMessage = "Successfull";
                        filteredItems.Add(listItem);
                        resultTable.Rows.Add(listItem.FormId, listItem.CreatedBy, listItem.FormType, listItem.ItemName,
                        listItem.ItemID, listItem.ItemPath, listItem.PageTitle, listItem.TemplateID,
                        listItem.CultureCode,listItem.EloquaInstance, listItem.ResponseStatusCode, listItem.ResponseStatusMessage);
                    }
                    
                }
                else
                {
                    return GetResponseStatusObject((int)HttpStatusCode.BadRequest, "No matches found");
                }
            }
            else
            {
                return GetResponseStatusObject((int)HttpStatusCode.BadRequest, "No matches found");
            }
            return resultTable;
        }

        /// <summary>
        /// Method to get response codes and messages.
        /// </summary>
        /// <param name="StatusCode"></param>
        /// <param name="StatusMessage"></param>
        /// <returns></returns>
        public DataTable GetResponseStatusObject(int StatusCode, string StatusMessage)
        {
            var dt = new DataTable();
            dt.Columns.Add("ResponseStatusCode", typeof(string));
            dt.Columns.Add("ResponseStatusMessage", typeof(string));
            dt.Rows.Add(StatusCode, StatusMessage);
            return dt;
        }       

        public enum Environment { master,web,production}
    }

        #endregion
}

