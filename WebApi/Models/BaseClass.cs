﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
namespace Anritsu.WebApp.GlobalWeb.Models
{
    public class BaseClass
    {
        public int ResponseStatusCode { get; set; }
        public string ResponseStatusMessage { get; set; }        
    }
}