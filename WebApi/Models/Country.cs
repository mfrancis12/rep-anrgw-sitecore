﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Country
    {
        public string CountryName { get; set; }
        public string Region { get; set; }
        public string ContextRegion { get; set; }
    }
}