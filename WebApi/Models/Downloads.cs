﻿using Anritsu.WebApp.GlobalWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Downloads
    {
        public string Title { get; set; }
        public string FilePath { get; set; }
        public string DownloadPath { get; set; }
        public string CategoryType { get; set; }
        public string ModelNumber { get; set; }
        public string Version { get; set; }
        public string Size { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string RedirectToMyAnritsu { get; set; }
    }

    public class APIResponse
    {
        public object ResponseContent;
        public ResponseStatus ResponseStatus;
    }

    public class GetDownloadsByKeyAPIResponse
    {
        public String ResponseContent;
        public ResponseStatus ResponseStatus;
        public int TotalRowCount;
    }

    public class ResponseStatus
    {
        public int StatusCode;
        public String StatusMessage;
    }

    public class ProdInfo
    {
        public string GlobalProductItemName;
        public string ModelNumber;
        public string ProductName;
        public string ProductImageURL;
        public string ProductSmallImageURL;
        public string PageURL;

    }

    public class ModelNumber
    {
        public int cultureGroupId { get; set; }
        public string modelNumber { get; set; }
        public string targetDb { get; set; }
    }

    public class DropDownListModel
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }

    public class DownloadItem
    {
        public string ItemName { get; set; }
        public int SkipRows { get; set; }
        public int NoOfRecords { get; set; }
        public int TotalCount { get; set; }
        public Guid ItemId { get; set; }
        public int CultureGroupId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public string DownloadFilePath { get; set; }
        public string ReplacementDownloadPath { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string CategoryType { get; set; }
        public string SubCategoryType { get; set; }
        public List<Product> RelatedProducts { get; set; }

        private List<OptionalLink> _optionalLinks;
        public List<OptionalLink> OptionalLinks
        {
            get { return _optionalLinks; }
            set
            {
                if (value != null && value.Count > 5) throw new ApplicationException("Optional links cannot exceed more than 5");
                _optionalLinks = value;
            }
        }
        public string MenuTitle { get; set; }
        public string PageTitle { get; set; }
        public bool ShowInBreadcrumb { get; set; }
        public string FileType { get; set; }
        public string Size { get; set; }
        public bool IsNewVersionAvailable { get; set; }
        public bool AllowHardCopyRequest { get; set; }
        public bool LoginRequired { get; set; }
        public bool RedirectToMyAnritsu { get; set; }
        //Html page meta tags
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        //S3 object meta tags
        public string Area { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        //S3 meta Data
        public Dictionary<string, string> S3MetaData { get; set; }
        public ReplacementDownload ReplacementDownload { get; set; }
        public List<string> AvailableLanguages { get; set; }
    }

    public class GetDownloadByIDModel
    {
        public string ItemName { get; set; }
        public Guid ItemId { get; set; }
        public int CultureGroupId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public string DownloadFilePath { get; set; }
        public string DownloadUrl { get; set; }
        public ReplacementDownload ReplacementDownloadPath { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string CategoryType { get; set; }
        public string SubCategoryType { get; set; }
        public List<Product> RelatedProducts { get; set; }

        private List<OptionalLink> _optionalLinks;
        public List<OptionalLink> OptionalLinks
        {
            get { return _optionalLinks; }
            set
            {
                if (value != null && value.Count > 5) throw new ApplicationException("Optional links cannot exceed more than 5");
                _optionalLinks = value;
            }
        }
        public string MenuTitle { get; set; }
        public string PageTitle { get; set; }
        public bool ShowInBreadcrumb { get; set; }
        public string FileType { get; set; }
        public string Size { get; set; }
        public bool IsNewVersionAvailable { get; set; }
        public bool AllowHardCopyRequest { get; set; }
        public bool LoginRequired { get; set; }
        public bool RedirectToMyAnritsu { get; set; }
        //Html page meta tags
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        //S3 object meta tags
        public string Area { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        //S3 meta Data
        public Dictionary<string, string> S3MetaData { get; set; }
        public List<string> AvailableLanguages { get; set; }
        public ReplacementDownload ReplacementDownload { get; set; }
    }

    public class Product
    {
        public string ProductName { get; set; }
    }

    public class OptionalLink
    {
        public string Text { get; set; }
        public string TargetUrl { get; set; }
        //public string Target { get; set; }
    }

    public class ProductSearchModel
    {
        public string ProductName { get; set; }
        public int cultureGroupId { get; set; }
    }

    public class DownloadSearchModel
    {
        public Guid ItemId { get; set; }
        public string ItemName { get; set; }
        public int SkipRows { get; set; }
        public int NoOfRecords { get; set; }
        public int TotalCount { get; set; }
        public int CultureGroupId { get; set; }
        public string Title { get; set; }
        // public string PageTitle { get; set; }  
        //public string DownloadFilePath { get; set; }
        // public DateTime ReleaseDate { get; set; }
        //public string OutputReleaseDate { get; set; }
        public string CategoryType { get; set; }
        public string SubCategoryType { get; set; }
        public string AssociatedProduct { get; set; }
    }

    public class DownloadResults
    {
        public Guid ItemId { get; set; }
        public string ItemName { get; set; }
        public string Title { get; set; }
        //  public string PageTitle { get; set; }
        //public string DownloadFilePath { get; set; }
        //  public string ReleaseDate { get; set; }
        public string CategoryType { get; set; }
        public string SubCategoryType { get; set; }
        public List<string> AvailableLanguages { get; set; }
    }

    public class DownloadByIdModel
    {
        public int CultureGroupId { get; set; }
        public string Id { get; set; }
    }

    public class DownloadByRegionModel
    {
        public string Region { get; set; }
        public string Id { get; set; }
    }

    public class PublishDownload
    {
        public string ItemId { get; set; }
        public int CultureGroupId { get; set; }
        public bool PublishToLive { get; set; }
        public bool IsDelete { get; set; }
        public List<string> Languages { get; set; }
    }

    public class GetDownloadByRegionModel
    {
        public string ItemName { get; set; }
        public string ItemId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public string DownloadFilePath { get; set; }
        public string ReplacementDownloadPath { get; set; }
        public string ReleaseDate { get; set; }
        public string CategoryType { get; set; }
        public string SubCategoryType { get; set; }
        public List<Product> RelatedProducts { get; set; }

        private List<OptionalLink> _optionalLinks;
        public List<OptionalLink> OptionalLinks
        {
            get { return _optionalLinks; }
            set
            {
                if (value != null && value.Count > 5) throw new ApplicationException("Optional links cannot exceed more than 5");
                _optionalLinks = value;
            }
        }
        public string MenuTitle { get; set; }
        public string PageTitle { get; set; }
        public bool ShowInBreadcrumb { get; set; }
        public string FileType { get; set; }
        public string Size { get; set; }
        public bool IsNewVersionAvailable { get; set; }
        public bool LoginRequired { get; set; }
        public bool RedirectToMyAnritsu { get; set; }
        //Html page meta tags
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
    }
    public class ReplacementDownload
    {
        public string DownloadName { get; set; }
        public string DownloadId { get; set; }
    }
}