﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.Models
{
    public class EmailTemplateResponse : BaseClass
    {
        public string FromEmailAddress { get; set; }
        public string SubjectTemplate { get; set; }
        public string BodyTemplate { get; set; }
        public string MailBodyFormat { get; set; }
    }   
}