﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.WebApp.GlobalWeb.Models
{
    public class ItemModel
    {
        public string ItemId { get; set; }
        public string Language { get; set; }
        public string Name { get; set; }
    }
}