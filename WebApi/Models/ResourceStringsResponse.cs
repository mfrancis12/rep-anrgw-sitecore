﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Shell.Applications.ContentEditor;
namespace WebApi.Models
{
    public class ResourceStringsResponse
    {
        public string Key { get; set; }
        public string Phrase { get; set; }
    }
}