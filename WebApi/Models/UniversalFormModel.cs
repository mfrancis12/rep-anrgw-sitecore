﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.WebApp.GlobalWeb.Models;
namespace WebApi.Models
{
    public class ItemInfo : BaseClass
    {
        public string TemplateID { get; set; }
        public string ItemName { get; set; }
        public string ItemPath { get; set; }
        public bool IsPublish { get; set; }
        public string CultureCode { get; set; }
        public string CreatedBy { get; set; }
        public string ItemID { get; set; }
        public string PageTitle { get; set; }
    }

    public class EloquaForm : ItemInfo
    {
        public string FormId { get; set; }
        public string FormType { get; set; }
        public bool IsProdInstance { get; set; }
        public string EloquaInstance { get; set; }
        public int PageIndex { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid VerificationToken { get; set; }
        public string ParentPath { get; set; }
        public bool IsEdit { get; set; }
    }    
}